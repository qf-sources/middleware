package io.qiufen.restful.common;

import java.lang.annotation.*;

/**
 * Created by Administrator on 2018/5/4.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.PARAMETER})
@Documented
public @interface Name
{
    String value();
}
