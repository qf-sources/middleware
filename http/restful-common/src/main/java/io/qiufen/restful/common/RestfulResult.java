package io.qiufen.restful.common;

/**
 * restful远程调用结果
 */
public class RestfulResult<T>
{
    public static final String SUCCESS = "0";

    private boolean success = true;
    private String code = SUCCESS;
    private String message = "complete";
    private T object;

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public T getObject()
    {
        return object;
    }

    public void setObject(T object)
    {
        this.object = object;
    }
}
