package io.qiufen.restful.common;

/**
 * restful远程调用异常
 */
public class RestfulException extends RuntimeException
{
    private final String code;
    private final String message;
    private final Object object;

    public RestfulException(String code, String message, Object object)
    {
        this.code = code;
        this.message = message;
        this.object = object;
    }

    public String getCode()
    {
        return code;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    public Object getObject()
    {
        return object;
    }

    @Override
    public Throwable fillInStackTrace()
    {
        return null;
    }

    @Override
    public String toString()
    {
        return "RestfulException{" + "code='" + code + '\'' + ", message='" + message + '\'' + ", object=" + object + "} " + super
                .toString();
    }

    public RestfulResult<Object> toRestfulResult()
    {
        RestfulResult<Object> result = new RestfulResult<Object>();
        result.setSuccess(false);
        result.setCode(getCode());
        result.setMessage(getMessage());
        result.setObject(getObject());
        return result;
    }
}
