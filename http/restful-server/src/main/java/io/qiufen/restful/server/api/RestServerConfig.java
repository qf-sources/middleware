package io.qiufen.restful.server.api;

import io.qiufen.http.server.spi.context.HttpContextSupport;
import io.qiufen.restful.server.spi.RestServiceProvider;

import java.util.List;
import java.util.concurrent.ExecutorService;

public class RestServerConfig
{
    private String name;

    private String host;
    private int port;

    private int ioBossThreads;
    private int ioWorkerThreads;

    private RestServiceProvider provider;

    private List<HttpContextSupport> httpContextSupport;

    private ExecutorService executorService;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getIoBossThreads()
    {
        return ioBossThreads;
    }

    public void setIoBossThreads(int ioBossThreads)
    {
        this.ioBossThreads = ioBossThreads;
    }

    public int getIoWorkerThreads()
    {
        return ioWorkerThreads;
    }

    public void setIoWorkerThreads(int ioWorkerThreads)
    {
        this.ioWorkerThreads = ioWorkerThreads;
    }

    public RestServiceProvider getProvider()
    {
        return provider;
    }

    public void setProvider(RestServiceProvider provider)
    {
        this.provider = provider;
    }

    public ExecutorService getExecutorService()
    {
        return executorService;
    }

    public void setExecutorService(ExecutorService executorService)
    {
        this.executorService = executorService;
    }

    public List<HttpContextSupport> getHttpContextSupport()
    {
        return httpContextSupport;
    }

    public void setHttpContextSupport(List<HttpContextSupport> httpContextSupport)
    {
        this.httpContextSupport = httpContextSupport;
    }
}
