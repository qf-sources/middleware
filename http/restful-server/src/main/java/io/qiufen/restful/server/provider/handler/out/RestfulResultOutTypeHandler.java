package io.qiufen.restful.server.provider.handler.out;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufOutputStream;
import io.qiufen.common.lang.JsonObject;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.restful.common.RestfulResult;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-6-5
 * Time: 下午4:54
 * To change this template use File | Settings | File Templates.
 */
public class RestfulResultOutTypeHandler implements OutTypeHandler
{
    @Override
    public Class<?>[] supportTypes()
    {
        return new Class<?>[]{RestfulResult.class};
    }

    @Override
    public int priority()
    {
        return 0;
    }

    @Override
    public void handle(HttpServiceRequest request, HttpServiceResponse response, Object object) throws IOException
    {
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        try
        {
            JsonObject.toJsonBytes(object, new ByteBufOutputStream(buf));
            response.getChannel().writeFinal(buf.retain());
        }
        finally
        {
            buf.release();
        }
    }
}
