package io.qiufen.restful.server.provider.handler.out;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.qiufen.common.lang.JsonObject;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.restful.common.RestfulResult;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-6-5
 * Time: 下午4:54
 * To change this template use File | Settings | File Templates.
 */
public class VoidOutTypeHandler implements OutTypeHandler
{
    private final ByteBuf DEFAULT;

    public VoidOutTypeHandler()
    {
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        JsonObject.toJsonBytes(new RestfulResult<Object>(), new ByteBufOutputStream(buf));
        DEFAULT = Unpooled.unreleasableBuffer(buf.slice());
    }

    @Override
    public Class<?>[] supportTypes()
    {
        return new Class<?>[]{void.class, Void.class};
    }

    @Override
    public int priority()
    {
        return 0;
    }

    @Override
    public void handle(HttpServiceRequest request, HttpServiceResponse response, Object object) throws IOException
    {
        response.getChannel().writeFinal(DEFAULT.duplicate());
    }
}
