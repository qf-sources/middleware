package io.qiufen.restful.server.provider.filter;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufOutputStream;
import io.qiufen.common.lang.JsonObject;
import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.http.server.spi.http.HttpFilterChain;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.restful.common.RestfulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;

public class RestfulExceptionFilter implements HttpFilter
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RestfulExceptionFilter.class);

    @Override
    public void doFilter(HttpFilterChain chain, HttpServiceRequest request, HttpServiceResponse response)
            throws Throwable
    {
        try
        {
            chain.doNextFilter(request, response);
        }
        catch (InvocationTargetException e)
        {
            Throwable throwable = e.getTargetException();
            if (throwable instanceof RestfulException)
            {
                doException(request, response, (RestfulException) throwable);
                return;
            }
            throw e;
        }
        catch (UndeclaredThrowableException e)
        {
            Throwable throwable = e.getUndeclaredThrowable();
            if (throwable instanceof RestfulException)
            {
                doException(request, response, (RestfulException) throwable);
                return;
            }
            throw e;
        }
        catch (RestfulException e)
        {
            doException(request, response, e);
        }
    }

    private void doException(HttpServiceRequest request, HttpServiceResponse response, RestfulException e)
            throws IOException
    {
        LOGGER.error(e.toString());
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        try
        {
            JsonObject.toJsonBytes(e.toRestfulResult(), new ByteBufOutputStream(buf));
            response.setContentLength(buf.readableBytes());
            response.getChannel().writeFinal(buf.retain());
        }
        finally
        {
            buf.release();
        }
    }
}
