package io.qiufen.restful.server.spi;

public interface RestServiceProvider
{
    void onProvide(RestServiceInfo serviceInfo) throws Throwable;
}
