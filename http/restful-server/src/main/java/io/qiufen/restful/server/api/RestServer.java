package io.qiufen.restful.server.api;

import io.qiufen.http.server.spi.http.HttpFilter;

public interface RestServer
{
    RestServerConfig getConfig();

    RestServer addHttpFilter(String uri, HttpFilter filter);

    RestServer addService(String uri, Object service, Class... interClasses);

    void close();
}
