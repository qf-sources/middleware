package io.qiufen.restful.server.provider.simple;

import io.qiufen.restful.server.api.RestServer;
import io.qiufen.restful.server.api.RestServerConfig;
import io.qiufen.restful.server.api.RestServerFactory;
import io.qiufen.restful.server.provider.handler.TypeHandlerRegistry;
import io.qiufen.restful.server.provider.handler.out.ObjectOutTypeHandler;
import io.qiufen.restful.server.provider.handler.out.OutTypeHandler;
import io.qiufen.restful.server.provider.handler.out.FutureOutTypeHandler;

import java.util.HashMap;
import java.util.Map;
//todo server初始化参数可分离
public class SimpleRestServerFactory implements RestServerFactory
{
    private final Map<String, RestServer> restServiceMap;
    private final TypeHandlerRegistry typeHandlerRegistry;

    public SimpleRestServerFactory()
    {
        this.restServiceMap = new HashMap<String, RestServer>();
        this.typeHandlerRegistry = new SimpleTypeHandlerRegistry().addOutTypeHandler(new ObjectOutTypeHandler())
                .addOutTypeHandler(new FutureOutTypeHandler());
    }

    @Override
    public RestServer createService(String name, RestServerConfig config)
    {
        if (this.restServiceMap.containsKey(name))
        {
            throw new RuntimeException("Exist rest service,name:" + name);
        }
        SimpleRestServer restService = new SimpleRestServer(config, typeHandlerRegistry);
        this.restServiceMap.put(name, restService);
        return restService;
    }

    @Override
    public RestServer getService(String name)
    {
        RestServer restServer = this.restServiceMap.get(name);
        if (restServer == null)
        {
            throw new RuntimeException("Not found rest service,name:" + name);
        }
        return restServer;
    }

    @Override
    public void close()
    {
        for (Map.Entry<String, RestServer> entry : restServiceMap.entrySet())
        {
            entry.getValue().close();
        }
    }

    public SimpleRestServerFactory addOutTypeHandler(OutTypeHandler handler)
    {
        this.typeHandlerRegistry.addOutTypeHandler(handler);
        return this;
    }
}
