package io.qiufen.restful.server.provider.handler.out;

import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.restful.server.provider.handler.TypeHandler;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-6-5
 * Time: 下午12:56
 * To change this template use File | Settings | File Templates.
 */
public interface OutTypeHandler extends TypeHandler
{
    void handle(HttpServiceRequest request, HttpServiceResponse response, Object object) throws Exception;
}
