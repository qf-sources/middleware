package io.qiufen.restful.server.api;

public interface RestServerFactory
{
    RestServer createService(String name, RestServerConfig config);

    RestServer getService(String name);

    void close();
}
