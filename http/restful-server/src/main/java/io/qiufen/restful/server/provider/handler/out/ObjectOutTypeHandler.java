package io.qiufen.restful.server.provider.handler.out;

import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.restful.common.RestfulResult;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-6-5
 * Time: 下午4:54
 * To change this template use File | Settings | File Templates.
 */
public class ObjectOutTypeHandler extends RestfulResultOutTypeHandler
{
    @Override
    public Class<?>[] supportTypes()
    {
        return new Class<?>[]{Object.class};
    }

    @Override
    public int priority()
    {
        return 0;
    }

    @Override
    public void handle(HttpServiceRequest request, HttpServiceResponse response, Object object) throws IOException
    {
        RestfulResult<Object> result = new RestfulResult<Object>();
        result.setObject(object);
        super.handle(request, response, result);
    }
}
