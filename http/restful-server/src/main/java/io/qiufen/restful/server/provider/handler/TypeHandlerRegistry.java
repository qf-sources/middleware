package io.qiufen.restful.server.provider.handler;

import io.qiufen.restful.server.provider.handler.out.OutTypeHandler;

public interface TypeHandlerRegistry
{
    TypeHandlerRegistry addOutTypeHandler(OutTypeHandler handler);

    OutTypeHandler getOutTypeHandler(Class<?> clazz);
}
