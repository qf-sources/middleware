package io.qiufen.restful.server.provider.handler;

/**
 * Created by Administrator on 2018/1/23.
 */
public interface TypeHandler
{
    Class<?>[] supportTypes();

    int priority();
}
