package io.qiufen.restful.server.provider.simple;

import io.netty.handler.codec.http.HttpMethod;
import io.qiufen.common.collection.ArrayUtil;
import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.http.server.provider.HttpServer;
import io.qiufen.http.server.provider.HttpServerConfig;
import io.qiufen.http.server.provider.command.DefaultHttpCommandBuilder;
import io.qiufen.http.server.provider.context.body.BodyHttpContextSupport;
import io.qiufen.http.server.provider.context.body.raw.RawSupport;
import io.qiufen.http.server.provider.dispatcher.SimpleContextDispatcher;
import io.qiufen.http.server.provider.router.SimpleHttpRouter;
import io.qiufen.http.server.spi.command.HttpCommandBuilder;
import io.qiufen.http.server.spi.context.HttpContextSupport;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.http.server.spi.http.HttpService;
import io.qiufen.http.server.spi.router.HttpRouter;
import io.qiufen.restful.common.Name;
import io.qiufen.restful.server.api.RestServer;
import io.qiufen.restful.server.api.RestServerConfig;
import io.qiufen.restful.server.provider.handler.TypeHandlerRegistry;
import io.qiufen.restful.server.spi.RestServiceInfo;
import io.qiufen.restful.server.spi.RestServiceProvider;

import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.ExecutorService;

class SimpleRestServer implements RestServer
{
    private final RestServerConfig config;
    private final TypeHandlerRegistry typeHandlerRegistry;

    private HttpServer server;
    private HttpRouter httpRouter;

    SimpleRestServer(RestServerConfig config, TypeHandlerRegistry typeHandlerRegistry)
    {
        this.config = config;
        this.typeHandlerRegistry = typeHandlerRegistry;

        try
        {
            init();
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
    }

    private static Class<?>[] interClasses(Object service, Class<?>... classes)
    {
        Class<?> serviceClass = service.getClass();
        if (ArrayUtil.isEmpty(classes))
        {
            classes = serviceClass.getInterfaces();
            if (ArrayUtil.isEmpty(classes))
            {
                throw new IllegalArgumentException("Service object not implements any interface.");
            }
            return classes;
        }

        for (Class<?> clazz : classes)
        {
            if (!clazz.isInterface())
            {
                throw new IllegalArgumentException("Found class:" + clazz + " is not an interface.");
            }
            if (!clazz.isAssignableFrom(serviceClass))
            {
                throw new IllegalArgumentException("Found class not implemented by interface:" + clazz);
            }
        }
        return classes;
    }

    @Override
    public RestServerConfig getConfig()
    {
        return this.config;
    }

    @Override
    public RestServer addHttpFilter(String uri, HttpFilter filter)
    {
        httpRouter.addHttpFilter(uri, filter);
        return this;
    }

    @Override
    public RestServer addService(String uri, Object service, Class... interClasses)
    {
        if (uri.endsWith("/")) uri = uri.substring(0, uri.length() - 1);

        interClasses = interClasses(service, interClasses);
        for (Class<?> clazz : interClasses)
        {
            Method[] methods = clazz.getMethods();
            for (Method method : methods)
            {
                String methodName = method.getName();
                if (method.isAnnotationPresent(Name.class))
                {
                    methodName = method.getAnnotation(Name.class).value();
                }
                String requestUri = uri + '/' + methodName;
                HttpService httpService = new SimpleRestHttpService(typeHandlerRegistry, service, method) {};
                httpRouter.addHttpService(requestUri, httpService);

                try
                {
                    triggerProvider(requestUri, clazz, method);
                }
                catch (Throwable e)
                {
                    throw new RuntimeException(e);
                }
            }
        }
        return this;
    }

    @Override
    public void close()
    {
        this.server.close();
    }

    private void init() throws InterruptedException
    {
        HttpServerConfig httpServerConfig = new HttpServerConfig();
        httpServerConfig.setHost(config.getHost());
        httpServerConfig.setPort(config.getPort());

        httpServerConfig.setBossThreads(config.getIoBossThreads());
        httpServerConfig.setWorkerThreads(config.getIoWorkerThreads());

        httpRouter = new SimpleHttpRouter();

        server = new HttpServer(httpServerConfig);
        List<HttpContextSupport> httpContextSupports = config.getHttpContextSupport();
        if (CollectionUtil.isEmpty(httpContextSupports))
        {
            server.addHttpContextSupport(BodyHttpContextSupport.of(HttpMethod.POST)
                    .contentType(RawSupport.of(ContentType.APPLICATION_JSON)));
        }
        else
        {
            for (HttpContextSupport httpContextSupport : httpContextSupports)
            {
                server.addHttpContextSupport(httpContextSupport);
            }
        }

        HttpCommandBuilder commandBuilder = new DefaultHttpCommandBuilder(httpRouter);
        SimpleContextDispatcher dispatcher;
        ExecutorService executorService = config.getExecutorService();
        if (executorService == null)
        {
            dispatcher = new SimpleContextDispatcher(commandBuilder);
        }
        else
        {
            dispatcher = new SimpleContextDispatcher(executorService, commandBuilder);
        }
        server.setHttpContextDispatcher(dispatcher);
        server.start();

        httpServerConfig = server.getConfig();
        this.config.setHost(httpServerConfig.getHost());
        this.config.setPort(httpServerConfig.getPort());
    }

    private void triggerProvider(String uri, Class<?> interClass, Method method) throws Throwable
    {
        RestServiceProvider provider = this.config.getProvider();
        if (provider == null) return;

        RestServiceInfo info = new RestServiceInfo();
        info.setServerName(this.config.getName());
        info.setHost(this.config.getHost());
        info.setPort(this.config.getPort());
        info.setUri(uri);
        info.setInterClass(interClass);
        info.setMethod(method);
        provider.onProvide(info);
    }
}