package io.qiufen.restful.server.provider.handler.out;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.api.Future;
import io.qiufen.common.promise.spi.Rejected;
import io.qiufen.common.promise.spi.Resolved;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.restful.common.RestfulException;
import io.qiufen.restful.common.RestfulResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class FutureOutTypeHandler extends RestfulResultOutTypeHandler
{
    private static final Logger LOGGER = LoggerFactory.getLogger(FutureOutTypeHandler.class);

    @Override
    public Class<?>[] supportTypes()
    {
        return new Class[]{Future.class};
    }

    @Override
    public int priority()
    {
        return 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void handle(HttpServiceRequest request, HttpServiceResponse response, Object object)
    {
        response.setAutoCommit(false);
        RestfulResponse restfulResponse = new RestfulResponse(request, response);
        TailResponse tailResponse = new TailResponse(response);
        ((Future<Object>) object).then(restfulResponse, restfulResponse).then(tailResponse, tailResponse);
    }

    private class RestfulResponse implements Resolved<Object, Object>, Rejected<Exception,Object>
    {
        private final HttpServiceRequest request;
        private final HttpServiceResponse response;

        private RestfulResponse(HttpServiceRequest request, HttpServiceResponse response)
        {
            this.request = request;
            this.response = response;
        }

        @Override
        public void onResolved(Context<Object> context, Object o) throws Exception
        {
            if (o instanceof RestfulResult)
            {
                FutureOutTypeHandler.super.handle(request, response, o);
                context.resolve(o);
            }
            else
            {
                RestfulResult<Object> result = new RestfulResult<Object>();
                result.setObject(o);
                FutureOutTypeHandler.super.handle(request, response, result);
                context.resolve(result);
            }
        }

        @Override
        public void onRejected(Context<Object> context, Exception o) throws IOException
        {
            if (o instanceof RestfulException)
            {
                LOGGER.error(o.toString());
                FutureOutTypeHandler.super.handle(request, response, ((RestfulException) o).toRestfulResult());
                context.resolve(o);
            }
            else
            {
                context.reject(o);
            }
        }
    }

    private static class TailResponse implements Resolved<Object, Object>, Rejected<Exception,Object>
    {
        private final HttpServiceResponse response;

        private TailResponse(HttpServiceResponse response) {this.response = response;}

        @Override
        public void onResolved(Context<Object> context, Object o)
        {
            response.commit();
            context.resolve(o);
        }

        @Override
        public void onRejected(Context context, Exception o)
        {
            LOGGER.error(o.toString(), o);
            response.setStatus(HttpResponseStatus.INTERNAL_SERVER_ERROR.code());
            response.commit();
            context.reject(o);
        }
    }
}
