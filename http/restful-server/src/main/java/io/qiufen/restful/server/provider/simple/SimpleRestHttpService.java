package io.qiufen.restful.server.provider.simple;

import io.netty.buffer.*;
import io.qiufen.common.lang.Arrays;
import io.qiufen.common.lang.JsonObject;
import io.qiufen.common.lang.Strings;
import io.qiufen.http.server.spi.http.HttpService;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.http.server.spi.http.ReadableChannel;
import io.qiufen.restful.common.Name;
import io.qiufen.restful.server.provider.handler.TypeHandlerRegistry;
import io.qiufen.restful.server.provider.handler.out.OutTypeHandler;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 * action请求分发过滤器
 * User: 善为之
 * Date: 17-6-3
 * Time: 下午10:30
 * To change this template use File | Settings | File Templates.
 */
class SimpleRestHttpService implements HttpService
{
    private final Object service;
    private final Method method;
    private final Type[] argTypes;
    private final String[] argNames;
    private final Class<?> returnType;

    private OutTypeHandler outTypeHandler;

    SimpleRestHttpService(TypeHandlerRegistry typeHandlerRegistry, Object service, Method method)
    {
        this.service = service;
        this.method = method;

        this.argTypes = method.getGenericParameterTypes();
        this.argNames = getArgNames(method);
        this.returnType = method.getReturnType();

        initOutTypeHandler(typeHandlerRegistry);
    }

    @Override
    public void doService(HttpServiceRequest request, HttpServiceResponse response) throws Exception
    {
        Object[] args = adaptArguments(request);
        Object obj;
        try
        {
            obj = method.invoke(service, args);
        }
        catch (InvocationTargetException e)
        {
            throw (Exception) e.getTargetException();
        }

        response.setContentType("application/json");
        renderResult(request, response, obj);
    }

    private Object[] adaptArguments(HttpServiceRequest request) throws IOException
    {
        //无参
        if (argTypes.length == 0) return Arrays.EMPTY;

        //尝试按照@Name取值并封装参数
        int matchedNames = 0;
        Object[] args = new Object[argTypes.length];
        JsonObject params = null;
        for (int i = 0; i < argTypes.length; i++)
        {
            String argName = argNames[i];
            if (Strings.isEmpty(argName)) continue;

            matchedNames++;

            if (params == null) params = getParams(request);

            args[i] = params.get(argName).to(argTypes[i]);
        }

        //存在@Name定义参数
        if (matchedNames > 0) return args;

        //按照一个参数类型封装
        args[0] = getParamObject(request, argTypes[0]);
        return args;
    }

    private JsonObject getParams(HttpServiceRequest request) throws IOException
    {
        ByteBuf buf = getBodyBytes(request);
        try
        {
            return buf.readableBytes() <= 0 ? JsonObject.NULL : JsonObject.of(new ByteBufInputStream(buf));
        }
        finally
        {
            buf.release();
        }
    }

    private Object getParamObject(HttpServiceRequest request, Type type) throws IOException
    {
        ByteBuf buf = getBodyBytes(request);
        try
        {
            return buf.readableBytes() <= 0 ? null : JsonObject.to(new ByteBufInputStream(buf), type);
        }
        finally
        {
            buf.release();
        }
    }

    public ByteBuf getBodyBytes(HttpServiceRequest request) throws IOException
    {
        ReadableChannel channel = request.getChannel();
        ByteBuf tempBuf, firstBuf = null;
        CompositeByteBuf all = null;
        while (!channel.isEOF(tempBuf = channel.read(ByteBuf.class)))
        {
            if (firstBuf == null)
            {
                firstBuf = tempBuf;
            }
            else
            {
                if (all == null)
                {
                    all = ByteBufAllocator.DEFAULT.compositeBuffer();
                    all.addComponent(true, firstBuf);
                }
                all.addComponent(true, tempBuf);
            }
        }
        return all == null ? firstBuf == null ? Unpooled.EMPTY_BUFFER : firstBuf : all;
    }

    private void renderResult(HttpServiceRequest request, HttpServiceResponse response, Object obj) throws Exception
    {
        this.outTypeHandler.handle(request, response, obj);
    }

    private void initOutTypeHandler(TypeHandlerRegistry typeHandlerRegistry)
    {
        outTypeHandler = typeHandlerRegistry.getOutTypeHandler(returnType);
        if (outTypeHandler == null)
        {
            //取默认
            outTypeHandler = typeHandlerRegistry.getOutTypeHandler(Object.class);
        }
    }

    private String[] getArgNames(Method method)
    {
        Annotation[][] annotations = method.getParameterAnnotations();
        String[] argNames = new String[annotations.length];
        for (int i = 0; i < annotations.length; i++)
        {
            Annotation[] annotation = annotations[i];
            Name name = null;
            for (Annotation ann : annotation)
            {
                if (ann instanceof Name)
                {
                    name = (Name) ann;
                    break;
                }
            }
            if (null != name)
            {
                argNames[i] = name.value();
            }
        }
        return argNames;
    }
}
