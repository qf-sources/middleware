package io.qiufen.restful.server.spi;

import java.lang.reflect.Method;

public class RestServiceInfo
{
    private String serverName;
    private String host;
    private int port;
    private String uri;
    private Class<?> interClass;
    private Method method;

    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    public Class<?> getInterClass()
    {
        return interClass;
    }

    public void setInterClass(Class<?> interClass)
    {
        this.interClass = interClass;
    }

    public Method getMethod()
    {
        return method;
    }

    public void setMethod(Method method)
    {
        this.method = method;
    }
}
