package io.qiufen.restful.server.provider.simple;

import io.qiufen.restful.server.provider.handler.TypeHandlerRegistry;
import io.qiufen.restful.server.provider.handler.out.OutTypeHandler;

import java.util.*;

class SimpleTypeHandlerRegistry implements TypeHandlerRegistry
{
    private final List<OutTypeHandler> outTypeHandlers;
    private final Map<Class<?>, OutTypeHandler> outTypeHandlerMap;

    SimpleTypeHandlerRegistry()
    {
        this.outTypeHandlers = new ArrayList<OutTypeHandler>();
        this.outTypeHandlerMap = new IdentityHashMap<Class<?>, OutTypeHandler>();
    }

    @Override
    public TypeHandlerRegistry addOutTypeHandler(OutTypeHandler handler)
    {
        this.outTypeHandlers.add(handler);
        initOutTypeHandler();
        return this;
    }

    @Override
    public OutTypeHandler getOutTypeHandler(Class<?> clazz)
    {
        return this.outTypeHandlerMap.get(clazz);
    }

    /**
     * 注册出参类型处理器
     */
    private void initOutTypeHandler()
    {
        Collections.sort(outTypeHandlers, new Comparator<OutTypeHandler>()
        {
            @Override
            public int compare(OutTypeHandler o1, OutTypeHandler o2)
            {
                return o1.priority() - o2.priority();
            }
        });
        for (OutTypeHandler handler : outTypeHandlers)
        {
            for (Class<?> type : handler.supportTypes())
            {
                outTypeHandlerMap.put(type, handler);
            }
        }
    }
}
