package test;

import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.http.server.spi.http.HttpFilterChain;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.restful.server.api.RestServer;
import io.qiufen.restful.server.api.RestServerConfig;
import io.qiufen.restful.server.provider.simple.SimpleRestServerFactory;

import java.util.HashMap;

public class Test1
{
    public static void main(String[] args)
    {
        SimpleRestServerFactory factory = new SimpleRestServerFactory();
        RestServer restServer = factory.createService("test", new RestServerConfig()
        {{
            setPort(8080);
            setHost("0.0.0.0");
        }});

        restServer.addHttpFilter("/", new HttpFilter()
        {
            @Override
            public void doFilter(HttpFilterChain chain, HttpServiceRequest request, HttpServiceResponse response)
                    throws Throwable
            {
                chain.doNextFilter(request, response);
            }
        }).addService("/test", new UserService()
        {
            @Override
            public Object test(final String p1)
            {
                return new HashMap<String, Object>()
                {{
                    put("p1", p1);
                }};
            }
        });
    }
}
