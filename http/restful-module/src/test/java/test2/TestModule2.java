package test2;

import io.qiufen.module.argument.provider.feature.ArgumentBinder;
import io.qiufen.module.argument.provider.feature.ArgumentFeature;
import io.qiufen.module.argument.provider.resolver.SystemPropertiesResolver;
import io.qiufen.module.argument.provider.value.PlaceholderValue;
import io.qiufen.module.binding.provider.feature.BindingBinder;
import io.qiufen.module.binding.provider.feature.BindingFeature;
import io.qiufen.module.binding.provider.feature.ObjectBuilder;
import io.qiufen.module.kernel.api.module.ModuleLifeCycleAdapter;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.launcher.provider.Launcher;
import io.qiufen.module.object.provider.value.RefValue;
import io.qiufen.restful.module.provider.feature.RestServerBinder;
import io.qiufen.restful.module.provider.feature.RestServerFeature;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestModule2 extends ModuleLifeCycleAdapter implements ArgumentFeature, BindingFeature, RestServerFeature
{
    public static void main(String[] args)
    {
        System.setProperty("port", "8080");

        new Launcher().addModule(TestModule2.class).start();
    }

    @Override
    public void bind(ArgumentBinder binder)
    {
        binder.bind(SystemPropertiesResolver.class).scope("test");
    }

    @Override
    public void bind(BindingBinder binder)
    {
        binder.bind(new UserService()
        {
            @Override
            public User create(User user)
            {
                return user;
            }
        }).id(0x100);

        binder.bind(ExecutorService.class, new ObjectBuilder()
        {
            @Override
            public Object build(FacadeFinder finder)
            {
                return Executors.newFixedThreadPool(10);
            }
        }).id("executor");
    }

    @Override
    public void bind(RestServerBinder binder)
    {
        binder.bindServer()
                .port(new PlaceholderValue("${test:port}"))
                .executorService(new RefValue("executor"))
                .id("test");

        binder.bindService("/users", 0x100).to("test");
    }

    @Override
    public void onLoad()
    {
        System.out.println("开始初始化测试模块...");
    }

    @Override
    public void onLoadSuccess()
    {
        System.out.println("测试模块初始化完成");
    }

    @Override
    public void onLoadFailure(Exception e)
    {
        System.out.println("测试模块初始化失败");
    }
}

