package test1;

import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.api.Future;
import io.qiufen.common.promise.api.Promise;
import io.qiufen.common.promise.provider.Promises;
import io.qiufen.common.promise.spi.Resolved;
import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.http.server.spi.http.HttpFilterChain;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.module.argument.provider.feature.ArgumentBinder;
import io.qiufen.module.argument.provider.feature.ArgumentFeature;
import io.qiufen.module.argument.provider.resolver.SystemPropertiesResolver;
import io.qiufen.module.argument.provider.value.PlaceholderValue;
import io.qiufen.module.binding.provider.feature.BindingBinder;
import io.qiufen.module.binding.provider.feature.BindingFeature;
import io.qiufen.module.binding.provider.feature.ObjectBuilder;
import io.qiufen.module.kernel.api.module.ModuleLifeCycleAdapter;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.launcher.provider.Launcher;
import io.qiufen.module.object.provider.value.RefValue;
import io.qiufen.restful.module.provider.feature.RestServerBinder;
import io.qiufen.restful.module.provider.feature.RestServerFeature;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class TestModule1 extends ModuleLifeCycleAdapter implements ArgumentFeature, BindingFeature, RestServerFeature
{
    public static void main(String[] args)
    {
        System.setProperty("port", "8081");

        new Launcher().addModule(TestModule1.class).start();
    }

    @Override
    public void bind(ArgumentBinder binder)
    {
        binder.bind(SystemPropertiesResolver.class).scope("test");
    }

    @Override
    public void bind(BindingBinder binder)
    {
        binder.bind(new HttpFilter()
        {
            @Override
            public void doFilter(HttpFilterChain chain, HttpServiceRequest request, HttpServiceResponse response)
                    throws Throwable
            {
                chain.doNextFilter(request, response);
            }
        }).id(0x000);

        binder.bind(new UserService()
        {
            private final Executor executor = Executors.newFixedThreadPool(10);

            private final Resolved<Map<String, Object>, Map<String, Object>> resolved = new Resolved<Map<String, Object>, Map<String, Object>>()
            {
                @Override
                public void onResolved(Context<Map<String, Object>> context, Map<String, Object> value)
                {
                    context.resolve(value);
                }
            };

            @Override
            public Future test(final String name)
            {
//                final Future<Map<String, Object>> p;
//                Promise<Map<String, Object>> promise = Promises.create();
//                p = promise.future();
//                for (int i = 0; i < 10; i++)
//                {
//                    p = promise.future().then(resolved);
//                }
//                executor.execute(new Command()
//                {
//                    @Override
//                    protected void doExecute()
//                    {
//                        p.resolve(new HashMap<String, Object>()
//                        {{
//                            put("name", name);
//                        }});
//                    }
//                });
//                return promise;
                return null;
            }

            @Override
            public void test1(String name)
            {
                LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(100));
            }

            @Override
            public Future<UUID> test2()
            {
                final Promise<UUID> promise = Promises.create();
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(3));
                        promise.resolve(UUID.randomUUID());
                    }
                }.start();
                return promise.future();
            }
        }).id(0x100);

        binder.bind(ExecutorService.class, new ObjectBuilder()
        {
            @Override
            public Object build(FacadeFinder finder)
            {
                return Executors.newFixedThreadPool(100);
            }
        }).id("executor");
    }

    @Override
    public void bind(RestServerBinder binder)
    {
        binder.bindServer()
                .port(new PlaceholderValue("${test:port}"))
                .executorService(new RefValue("executor"))
                .id("test");

        binder.bindFilter("/", 0x000).to("test");

        binder.bindService("/test", 0x100).to("test");
    }

    @Override
    public void onLoad()
    {
        System.out.println("开始初始化测试模块...");
    }

    @Override
    public void onLoadSuccess()
    {
        System.out.println("测试模块初始化完成");
    }

    @Override
    public void onLoadFailure(Exception e)
    {
        System.out.println("测试模块初始化失败");
    }
}
