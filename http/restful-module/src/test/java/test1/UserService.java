package test1;

import io.qiufen.common.promise.api.Future;
import io.qiufen.restful.common.Name;

public interface UserService
{
    Future test(@Name("name") String name);

    void test1(@Name("name") String name);

    Future test2();
}
