package io.qiufen.restful.module.provider.feature;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;
import io.qiufen.module.kernel.spi.feature.FeatureHandlerAdapter;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import io.qiufen.restful.module.provider.context.RestServerContext;
import io.qiufen.restful.server.api.RestServer;

import java.util.ArrayList;
import java.util.List;

public class RestServerFeatureHandler extends FeatureHandlerAdapter<RestServerFeature>
{
    private List<RestFilterDeclaration> tempFilterDeclarations;
    private List<RestServiceDeclaration> tempServiceDeclarations;

    private FacadeFinder facadeFinder;

    @Override
    public void initFeature(ContextFactory context, FacadeRegistry facadeRegistry)
    {
        this.facadeFinder = facadeRegistry;
    }

    @Override
    public void doHandle(RestServerFeature feature, ContextFactory context)
    {
        List<RestServerDeclaration> restServerDeclarations = new ArrayList<RestServerDeclaration>();
        tempFilterDeclarations = new ArrayList<RestFilterDeclaration>();
        tempServiceDeclarations = new ArrayList<RestServiceDeclaration>();
        feature.bind(new RestServerBinder(restServerDeclarations, tempFilterDeclarations, tempServiceDeclarations));

        for (RestServerDeclaration restServerDeclaration : restServerDeclarations)
        {
            RestServerContext.addDefinition(restServerDeclaration);
        }
        restServerDeclarations.clear();
    }

    @Override
    public void postModule(RestServerFeature feature, ContextFactory contextFactory)
    {
        initServer();
        initFilter(contextFactory);
        initService(contextFactory);

        tempFilterDeclarations.clear();
        tempFilterDeclarations = null;
        tempServiceDeclarations.clear();
        tempServiceDeclarations = null;
    }

    private void initServer()
    {
        RestServerContext.build(facadeFinder);
    }

    private void initFilter(ContextFactory context)
    {
        if (CollectionUtil.isEmpty(tempFilterDeclarations)) return;

        for (RestFilterDeclaration declaration : tempFilterDeclarations)
        {
            RestServer restServer = RestServerContext.getRestServer(declaration.getServerId(), facadeFinder);
            Object object = getObject(context, declaration.getObjectId(), declaration.getType());
            restServer.addHttpFilter(declaration.getUri(), (HttpFilter) object);
        }
    }

    private void initService(ContextFactory context)
    {
        if (CollectionUtil.isEmpty(tempServiceDeclarations)) return;

        for (RestServiceDeclaration declaration : tempServiceDeclarations)
        {
            RestServer restServer = RestServerContext.getRestServer(declaration.getServerId(), facadeFinder);
            Object object = getObject(context, declaration.getObjectId(), declaration.getType());
            restServer.addService(declaration.getUri(), object);
        }
    }

    private Object getObject(ContextFactory context, Object objectId, Class<?> type)
    {
        ObjectContext objectContext = context.getContext(ObjectContext.class);
        //1.先按照ID匹配
        if (objectId != null) return objectContext.getObject(objectId);

        //2.其次按照类型匹配
        if (objectContext.hasDeclaration(type)) return objectContext.getObject(type);

        //3.创建一个
        ClassDeclaration declaration = new ClassDeclaration(type);
        declaration.setScope(ObjectScope.ONCE);
        objectContext.addDeclaration(declaration);
        return objectContext.getObject(declaration.getIdList()[0]);
    }
}
