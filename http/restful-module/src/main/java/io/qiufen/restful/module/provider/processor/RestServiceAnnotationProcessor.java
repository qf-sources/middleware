package io.qiufen.restful.module.provider.processor;

import io.qiufen.common.conversion.TypeConversionService;
import io.qiufen.common.lang.Strings;
import io.qiufen.module.injection.api.qualifier.Id;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.api.declaration.ObjectDeclaration;
import io.qiufen.module.object.spi.declaration.Declaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import io.qiufen.module.processor.spi.ProcessorAdapter;
import io.qiufen.restful.module.api.RestService;
import io.qiufen.restful.module.provider.context.RestServerContext;
import io.qiufen.restful.server.api.RestServer;

public class RestServiceAnnotationProcessor extends ProcessorAdapter
{
    @Override
    public void doPrepare(Declaration declaration)
    {
        boolean hasAnn = false;
        if (declaration instanceof ClassDeclaration)
        {
            hasAnn = ((ClassDeclaration) declaration).getClazz().isAnnotationPresent(RestService.class);
        }
        else if (declaration instanceof ObjectDeclaration)
        {
            hasAnn = ((ObjectDeclaration) declaration).getObject().getClass().isAnnotationPresent(RestService.class);
        }

        if (hasAnn)
        {
            declaration.setScope(ObjectScope.ONCE);
            declaration.setLazy(false);
        }
    }

    @Override
    public void doProcess(Object prototype, Object decorated, FacadeFinder finder)
    {
        Class<?> type = prototype.getClass();
        RestService restServiceAnn = type.getAnnotation(RestService.class);
        if (restServiceAnn == null) return;

        String uri = restServiceAnn.uri();
        Id id = restServiceAnn.to();

        Object serverId = null;
        if (Strings.isNotEmpty(id.value()))
        {
            serverId = TypeConversionService.getInstance().convert(id.value(), id.type());
        }
        RestServer server = RestServerContext.getRestServer(serverId, finder);
        server.addService(uri, decorated);
    }
}
