package io.qiufen.restful.module.provider.feature;

import io.qiufen.http.server.spi.context.HttpContextSupport;
import io.qiufen.module.kernel.provider.value.SimpleValue;
import io.qiufen.module.kernel.spi.value.Value;
import io.qiufen.restful.server.spi.RestServiceProvider;

import java.util.ArrayList;
import java.util.List;

public class RestServerDeclarationOptions
{
    private final RestServerDeclaration declaration;

    RestServerDeclarationOptions(RestServerDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public RestServerDeclarationOptions port(int port)
    {
        return port(new SimpleValue(port));
    }

    public RestServerDeclarationOptions port(Value port)
    {
        this.declaration.setPort(port);
        return this;
    }

    public RestServerDeclarationOptions host(String host) {return host(new SimpleValue(host));}

    public RestServerDeclarationOptions host(Value host)
    {
        this.declaration.setHost(host);
        return this;
    }

    public RestServerDeclarationOptions ioBossThreads(Value host)
    {
        this.declaration.setIoBossThreads(host);
        return this;
    }

    public RestServerDeclarationOptions ioWorkerThreads(Value host)
    {
        this.declaration.setIoWorkerThreads(host);
        return this;
    }

    public RestServerDeclarationOptions executorService(Value host)
    {
        this.declaration.setExecutorService(host);
        return this;
    }

    public RestServerDeclarationOptions addProvider(Class<? extends RestServiceProvider> providerClass)
    {
        List<Class<? extends RestServiceProvider>> classes = this.declaration.getProviderClasses();
        if (classes == null)
        {
            classes = new ArrayList<Class<? extends RestServiceProvider>>();
            this.declaration.setProviderClasses(classes);
        }
        classes.add(providerClass);
        return this;
    }

    public RestServerDeclarationOptions id(Object serverId)
    {
        this.declaration.setServerId(serverId);
        return this;
    }

    public RestServerDeclarationOptions addHttpContextSupport(HttpContextSupport httpContextSupport)
    {
        List<HttpContextSupport> supports = this.declaration.getHttpContextSupport();
        if (supports == null)
        {
            supports = new ArrayList<HttpContextSupport>();
            this.declaration.setHttpContextSupport(supports);
        }
        supports.add(httpContextSupport);
        return this;
    }
}
