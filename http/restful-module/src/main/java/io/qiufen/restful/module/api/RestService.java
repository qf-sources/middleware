package io.qiufen.restful.module.api;

import io.qiufen.module.injection.api.qualifier.Id;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RestService
{
    String uri();

    Id to() default @Id(value = "");
}