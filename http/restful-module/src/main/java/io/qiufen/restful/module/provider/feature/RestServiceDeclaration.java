package io.qiufen.restful.module.provider.feature;

public class RestServiceDeclaration
{
    private final String uri;

    private Object objectId;
    private Class<?> type;

    private Object serverId;

    RestServiceDeclaration(String uri, Object objectId)
    {
        this.uri = uri;
        this.objectId = objectId;
    }

    RestServiceDeclaration(String uri, Class<?> type)
    {
        this.uri = uri;
        this.type = type;
    }

    public RestServiceDeclaration to(Object serverId)
    {
        this.serverId = serverId;
        return this;
    }

    String getUri()
    {
        return uri;
    }

    Object getObjectId()
    {
        return objectId;
    }

    Class<?> getType()
    {
        return type;
    }

    Object getServerId()
    {
        return serverId;
    }
}
