package io.qiufen.restful.module.provider.feature;

import io.qiufen.http.server.spi.context.HttpContextSupport;
import io.qiufen.module.kernel.spi.value.Value;
import io.qiufen.restful.server.spi.RestServiceProvider;

import java.util.List;

public class RestServerDeclaration
{
    private Value port;
    private Value host;
    private Value ioBossThreads;
    private Value ioWorkerThreads;
    private Value executorService;
    private Object serverId;
    private List<Class<? extends RestServiceProvider>> providerClasses;
    private List<HttpContextSupport> httpContextSupport;

    public Value getPort()
    {
        return port;
    }

    void setPort(Value port)
    {
        this.port = port;
    }

    public Value getHost()
    {
        return host;
    }

    void setHost(Value host)
    {
        this.host = host;
    }

    public Value getIoBossThreads()
    {
        return ioBossThreads;
    }

    public void setIoBossThreads(Value ioBossThreads)
    {
        this.ioBossThreads = ioBossThreads;
    }

    public Value getIoWorkerThreads()
    {
        return ioWorkerThreads;
    }

    public void setIoWorkerThreads(Value ioWorkerThreads)
    {
        this.ioWorkerThreads = ioWorkerThreads;
    }

    public Value getExecutorService()
    {
        return executorService;
    }

    public void setExecutorService(Value executorService)
    {
        this.executorService = executorService;
    }

    public Object getServerId()
    {
        return serverId;
    }

    void setServerId(Object serverId)
    {
        this.serverId = serverId;
    }

    public List<Class<? extends RestServiceProvider>> getProviderClasses()
    {
        return providerClasses;
    }

    public void setProviderClasses(List<Class<? extends RestServiceProvider>> providerClasses)
    {
        this.providerClasses = providerClasses;
    }

    public List<HttpContextSupport> getHttpContextSupport()
    {
        return httpContextSupport;
    }

    public void setHttpContextSupport(List<HttpContextSupport> httpContextSupport)
    {
        this.httpContextSupport = httpContextSupport;
    }
}
