package io.qiufen.restful.module.provider.feature;

import io.qiufen.http.server.spi.http.HttpFilter;

import java.util.List;

public class RestServerBinder
{
    private final List<RestServerDeclaration> restServerDeclarationList;
    private final List<RestFilterDeclaration> restFilterDeclarationList;
    private final List<RestServiceDeclaration> restServiceDeclarationList;

    RestServerBinder(List<RestServerDeclaration> restServerDeclarationList,
            List<RestFilterDeclaration> restFilterDeclarationList,
            List<RestServiceDeclaration> restServiceDeclarationList)
    {
        this.restServerDeclarationList = restServerDeclarationList;
        this.restFilterDeclarationList = restFilterDeclarationList;
        this.restServiceDeclarationList = restServiceDeclarationList;
    }

    public RestServerDeclarationOptions bindServer()
    {
        RestServerDeclaration declaration = new RestServerDeclaration();
        this.restServerDeclarationList.add(declaration);
        return new RestServerDeclarationOptions(declaration);
    }

    public RestFilterDeclaration bindFilter(String uri, Object objectId)
    {
        RestFilterDeclaration declaration = new RestFilterDeclaration(uri, objectId);
        this.restFilterDeclarationList.add(declaration);
        return declaration;
    }

    public RestFilterDeclaration bindFilter(String uri, Class<? extends HttpFilter> type)
    {
        RestFilterDeclaration declaration = new RestFilterDeclaration(uri, type);
        this.restFilterDeclarationList.add(declaration);
        return declaration;
    }

    public RestServiceDeclaration bindService(String uri, Object objectId)
    {
        RestServiceDeclaration declaration = new RestServiceDeclaration(uri, objectId);
        this.restServiceDeclarationList.add(declaration);
        return declaration;
    }

    public RestServiceDeclaration bindService(String uri, Class<?> type)
    {
        RestServiceDeclaration declaration = new RestServiceDeclaration(uri, type);
        this.restServiceDeclarationList.add(declaration);
        return declaration;
    }
}
