package io.qiufen.restful.module.provider.feature;


import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

@Handler(RestServerFeatureHandler.class)
public interface RestServerFeature extends Feature
{
    void bind(RestServerBinder binder);
}
