package io.qiufen.restful.module.provider.context;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.collection.MapUtil;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.api.facade.ObjectFacade;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import io.qiufen.restful.module.provider.feature.RestServerDeclaration;
import io.qiufen.restful.server.api.RestServer;
import io.qiufen.restful.server.api.RestServerConfig;
import io.qiufen.restful.server.provider.handler.out.RestfulResultOutTypeHandler;
import io.qiufen.restful.server.provider.handler.out.VoidOutTypeHandler;
import io.qiufen.restful.server.provider.simple.SimpleRestServerFactory;
import io.qiufen.restful.server.spi.RestServiceInfo;
import io.qiufen.restful.server.spi.RestServiceProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

//todo 托管到module
public class RestServerContext
{
    private static final String DEFAULT_ID = "default";

    private static Map<Object, RestServerDeclaration> restServerDeclarations = new HashMap<Object, RestServerDeclaration>();

    private static Map<Object, RestServer> restServerMap = new HashMap<Object, RestServer>();

    private static SimpleRestServerFactory factory;

    static
    {
        factory = new SimpleRestServerFactory();
        factory.addOutTypeHandler(new VoidOutTypeHandler()).addOutTypeHandler(new RestfulResultOutTypeHandler());
    }

    public static void addDefinition(RestServerDeclaration declaration)
    {
        restServerDeclarations.put(declaration.getServerId() == null ? DEFAULT_ID : declaration.getServerId(),
                declaration);
    }

    public static void build(FacadeFinder facadeFinder)
    {
        if (MapUtil.isEmpty(restServerDeclarations))
        {
            return;
        }

        for (Map.Entry<Object, RestServerDeclaration> entry : restServerDeclarations.entrySet())
        {
            Object serverId = entry.getKey();
            RestServerDeclaration declaration = entry.getValue();
            build(serverId, declaration, facadeFinder);
        }
        restServerDeclarations.clear();
    }

    private static RestServer build(Object serverId, RestServerDeclaration declaration, FacadeFinder facadeFinder)
    {
        RestServerConfig config = new RestServerConfig();
        config.setName(serverId.toString());
        if (declaration.getHost() != null)
        {
            config.setHost(declaration.getHost().to(facadeFinder, String.class));
        }
        if (declaration.getPort() != null)
        {
            config.setPort(declaration.getPort().to(facadeFinder, int.class));
        }
        if (declaration.getIoBossThreads() != null)
        {
            config.setIoBossThreads(declaration.getIoBossThreads().to(facadeFinder, int.class));
        }
        if (declaration.getIoWorkerThreads() != null)
        {
            config.setIoWorkerThreads(declaration.getIoWorkerThreads().to(facadeFinder, int.class));
        }
        if (declaration.getExecutorService() != null)
        {
            config.setExecutorService(declaration.getExecutorService().to(facadeFinder, ExecutorService.class));
        }
        if (CollectionUtil.isNotEmpty(declaration.getProviderClasses()))
        {
            config.setProvider(toServiceProvider(facadeFinder.getFacade(ObjectFacade.class), declaration));
        }
        config.setHttpContextSupport(declaration.getHttpContextSupport());
        RestServer restServer = factory.createService(serverId.toString(), config);
        restServerMap.put(serverId, restServer);
        return restServer;
    }

    public static RestServer getRestServer(Object serverId, FacadeFinder facadeFinder)
    {
        serverId = serverId == null ? DEFAULT_ID : serverId;
        RestServer restServer = restServerMap.get(serverId);
        if (restServer != null) return restServer;

        RestServerDeclaration declaration = restServerDeclarations.remove(serverId);
        if (declaration == null) throw new RuntimeException("Rest server not bind,server id:" + serverId);
        restServer = build(serverId, declaration, facadeFinder);
        return restServer;
    }

    private static Object getObject(ObjectFacade facade, Class<?> type)
    {
        //1.其次按照类型匹配
        if (facade.hasDeclaration(type)) return facade.getObject(type);

        //2.创建一个
        ClassDeclaration declaration = new ClassDeclaration(type);
        declaration.setScope(ObjectScope.ONCE);
        facade.addDeclaration(declaration);
        return facade.getObject(declaration.getIdList()[0]);
    }

    private static RestServiceProvider toServiceProvider(ObjectFacade facade, RestServerDeclaration declaration)
    {
        List<Class<? extends RestServiceProvider>> classes = declaration.getProviderClasses();
        if (classes.size() == 1) return (RestServiceProvider) getObject(facade, classes.get(0));

        List<RestServiceProvider> providerList = new ArrayList<RestServiceProvider>();
        for (Class<? extends RestServiceProvider> providerClass : declaration.getProviderClasses())
        {
            providerList.add((RestServiceProvider) getObject(facade, providerClass));
        }
        final RestServiceProvider[] providers = new RestServiceProvider[providerList.size()];
        providerList.toArray(providers);
        return new RestServiceProvider()
        {
            @Override
            public void onProvide(RestServiceInfo serviceInfo) throws Throwable
            {
                for (RestServiceProvider provider : providers)
                {
                    provider.onProvide(serviceInfo);
                }
            }
        };
    }
}
