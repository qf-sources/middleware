package test1;

import java.util.Deque;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class Test
{
    public static void main(String[] args) throws InterruptedException
    {
        final Semaphore semaphore = new Semaphore(0);
        TDispatcher dispatcher = new TDispatcher(Executors.newFixedThreadPool(4), new Operation<UUID>()
        {
            @Override
            public void operate(UUID uuid)
            {
                //                LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(1000));
                //                System.out.println(uuid);
                semaphore.release(1);
            }
        });
        //        Executor executor = ElasticExecutors.create(1, 4, 30000, new QueueDepthElasticPolicy(4));
        UUID uuid = UUID.randomUUID();

        long total = 0;
        int loops = 100;
        int count = 500000;
        for (int k = 0; k < loops; k++)
        {
            for (int i = 0; i < count; i++)
            {
                dispatcher.offer(uuid);
                //                executor.execute(new Runnable() {
                //                    @Override
                //                    public void run()
                //                    {
                //                        semaphore.release(1);
                //                    }
                //                });
            }
            long p0 = System.currentTimeMillis();
            semaphore.acquire(count);
            long p1 = System.currentTimeMillis();
            System.out.println(p1 - p0);
            total += p1 - p0;
        }
        System.out.println(total / loops);
    }
}

class TDispatcher extends ObjectDispatcher<UUID>
{
    private final Deque<UUID> deque = new ConcurrentLinkedDeque<UUID>();

    TDispatcher(Executor executor, Operation<UUID> operation)
    {
        super(executor, operation, false);
    }

    @Override
    protected boolean offer0(UUID uuid)
    {
        return deque.offer(uuid);
    }

    @Override
    protected void offerTail0(UUID uuid)
    {
        deque.offerLast(uuid);
    }

    @Override
    protected UUID poll0()
    {
        return deque.poll();
    }
}