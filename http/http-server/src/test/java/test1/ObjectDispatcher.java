package test1;

import io.qiufen.common.concurrency.executor.api.Command;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class ObjectDispatcher<T>
{
    private final AtomicInteger depth = new AtomicInteger(0);

    private final Executor executor;
    private final Operation<T> operation;
    private final boolean isFair;

    private DispatcherCommand<T> dispatcherCommand;

    private volatile boolean closed = false;

    protected ObjectDispatcher(Executor executor, Operation<T> operation)
    {
        this(executor, operation, true);
    }

    protected ObjectDispatcher(Executor executor, Operation<T> operation, boolean isFair)
    {
        this.executor = executor;
        this.operation = operation;
        this.isFair = isFair;

        init();
    }

    public boolean offer(T obj)
    {
        if (closed) throw new IllegalStateException("Buffered queue is closed.");

        if (!isFair && dispatchDirect(obj)) return true;

        boolean result = offer0(obj);
        //提供成功之后，增加计数，唤醒分发器任务
        if (result)
        {
            depth.getAndIncrement();
            dispatcherCommand.incrementSemaphore();
        }
        return result;
    }

    public void close()
    {
        this.closed = true;
        dispatcherCommand.close();
    }

    T poll()
    {
        T t = poll0();
        if (t == null)
        {
            return null;
        }
        this.depth.decrementAndGet();
        return t;
    }

    /**
     * 初始化缓冲分发器
     */
    private void init()
    {
        dispatcherCommand = new DispatcherCommand<T>(this, executor, operation)
        {
            @Override
            protected void onDispatchSuccess(T obj) throws Exception
            {
                onDispatched(obj);
            }

            @Override
            protected void onDispatchFailure(T obj, Exception exception) throws Exception
            {
                super.onDispatchFailure(obj, exception);
                offerTail(obj);
            }
        };
        Thread thread = new Thread(dispatcherCommand);
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();
    }

    private boolean dispatchDirect(final T obj)
    {
        try
        {
            executor.execute(new Command()
            {
                @Override
                protected void doExecute() throws Throwable
                {
                    operation.operate(obj);
                }
            });
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    private void offerTail(T obj)
    {
        offerTail0(obj);
        depth.getAndIncrement();
        dispatcherCommand.incrementSemaphore();
    }

    protected void onDispatched(T obj) throws Exception
    {
    }

    protected abstract boolean offer0(T t);

    protected abstract void offerTail0(T t);

    protected abstract T poll0();
}
