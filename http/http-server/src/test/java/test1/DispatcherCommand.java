package test1;

import io.qiufen.common.concurrency.executor.api.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executor;
import java.util.concurrent.Semaphore;

class DispatcherCommand<T> implements Runnable
{
    private static final Logger log = LoggerFactory.getLogger(DispatcherCommand.class);

    private final Semaphore semaphore = new Semaphore(0);

    private final ObjectDispatcher<T> dispatcher;
    private final Executor executor;
    private final Operation<T> operation;

    private volatile boolean closed = false;

    DispatcherCommand(ObjectDispatcher<T> dispatcher, Executor executor, Operation<T> operation)
    {
        this.dispatcher = dispatcher;
        this.executor = executor;
        this.operation = operation;
    }

    void incrementSemaphore()
    {
        this.semaphore.release(1);
    }

    @Override
    public void run()
    {
        for (; ; )
        {
            if (closed)
            {
                clear();
                return;
            }
            else
            {
                try
                {
                    semaphore.acquire(1);
                    T obj = dispatcher.poll();
                    if (obj == null) continue;
                    dispatch(obj);
                }
                catch (Exception e)
                {
                    log.error(e.toString(), e);
                }
            }
        }
    }

    private void clear()
    {
        while (true)
        {
            try
            {
                T obj = dispatcher.poll();
                if (obj == null) return;
                dispatch(obj);
            }
            catch (Exception e)
            {
                log.error(e.toString(), e);
            }
        }
    }

    private void dispatch(final T obj)
    {
        Command command = new Command()
        {
            @Override
            protected void doExecute() throws Throwable
            {
                operation.operate(obj);
            }
        };
        Exception exception = null;
        try
        {
            executor.execute(command);
        }
        catch (Exception e)
        {
            exception = e;
        }

        if (exception == null)
        {
            doDispatchSuccess(obj);
        }
        else
        {
            doDispatchFailure(obj, exception);
        }
    }

    void close()
    {
        this.closed = true;
    }

    private void doDispatchSuccess(T obj)
    {
        try
        {
            onDispatchSuccess(obj);
        }
        catch (Exception e)
        {
            log.error("", e);
        }
    }

    private void doDispatchFailure(T obj, Exception exception)
    {
        try
        {
            onDispatchFailure(obj, exception);
        }
        catch (Exception e)
        {
            log.error("", e);
        }
    }

    protected void onDispatchSuccess(T obj) throws Exception
    {
    }

    protected void onDispatchFailure(T obj, Exception exception) throws Exception
    {
        log.error("", exception);
    }
}
