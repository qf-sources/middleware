package test1;

public interface Operation<T>
{
    void operate(T t) throws Exception;
}
