import io.netty.util.ResourceLeakDetector;
import io.qiufen.http.server.provider.HttpServer;
import io.qiufen.http.server.provider.HttpServerConfig;
import io.qiufen.http.server.provider.command.DefaultHttpCommandBuilder;
import io.qiufen.http.server.provider.context.body.BodyHttpContextSupport;
import io.qiufen.http.server.provider.context.body.form.FormDataSupport;
import io.qiufen.http.server.provider.context.body.form.FormURLEncodedSupport;
import io.qiufen.http.server.provider.context.body.multipart.MultipartFormDataSupport;
import io.qiufen.http.server.provider.context.body.multipart.MultipartFormHttpServiceRequest;
import io.qiufen.http.server.provider.context.body.raw.RawHttpServiceRequest;
import io.qiufen.http.server.provider.context.body.raw.RawSupport;
import io.qiufen.http.server.provider.context.common.CommonHttpContextSupport;
import io.qiufen.http.server.provider.dispatcher.SimpleContextDispatcher;
import io.qiufen.http.server.provider.router.SimpleHttpRouter;
import io.qiufen.http.server.spi.http.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;

public class Test1
{
    public static void main(String[] args) throws InterruptedException
    {
        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);

        HttpServerConfig config = new HttpServerConfig()
        {{
            setPort(8080);
        }};
        SimpleHttpRouter httpRouter = new SimpleHttpRouter();
        httpRouter.addHttpService("/", new HttpService1());
        httpRouter.addHttpService("/image.jpg", new HttpService2());
        httpRouter.addHttpService("/upload", new HttpService3());
        httpRouter.addHttpFilter("/", new HttpFilter1());

        new HttpServer(config).addHttpContextSupport(new CommonHttpContextSupport())
                .addHttpContextSupport(new BodyHttpContextSupport(FormDataSupport.of(), FormURLEncodedSupport.of(),
                        MultipartFormDataSupport.of(), new RawSupport()))
                .setHttpContextDispatcher(new SimpleContextDispatcher(Executors.newFixedThreadPool(10),
                        new DefaultHttpCommandBuilder(httpRouter)))
                .start();
    }
}

class HttpService1 implements HttpService
{
    @Override
    public void doService(HttpServiceRequest request, HttpServiceResponse response) throws IOException
    {
        //        System.out.println(request.uri());
        //        System.out.println(request.path());
        //        System.out.println(request.method());
        //        System.out.println(request.headers());
        //        System.out.println(request.params());
        //        System.out.println(request.remoteIp());
        //        System.out.println(request.cookies());
        //        System.out.println("test success!");
        //        if (request instanceof MultipartFormHttpRequest)
        //        {
        //            System.out.println(((MultipartFormHttpRequest) request).files());
        //        }
        if (request instanceof RawHttpServiceRequest)
        {
            String json = ((RawHttpServiceRequest) request).getBody();
            //            System.out.println(json);
            response.setContentType("text/plain");
            byte[] bytes = json.getBytes(Charset.forName("utf8"));
            response.setContentLength(bytes.length);
            response.getChannel().writeFinal(bytes);
        }
    }
}

class HttpService2 implements HttpService
{
    @Override
    public void doService(HttpServiceRequest request, HttpServiceResponse response)
    {
        response.setContentType("image/jpg");
        File file = new File("C:\\Users\\Ruzheng Zhang\\Pictures\\Saved Pictures\\图片20230821165731166.jpg");
        //                response.setContentLength(file.length());
        try
        {
            WritableChannel channel = response.getChannel();
            FileInputStream stream = new FileInputStream(file);
            byte[] buffer = new byte[4096];
            int n;
            while ((n = stream.read(buffer)) != -1)
            {
                channel.write(ByteBuffer.wrap(buffer, 0, n));
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}

class HttpService3 implements HttpService
{
    @Override
    public void doService(HttpServiceRequest request, HttpServiceResponse response)
    {
        MultipartFormHttpServiceRequest multipartFormHttpRequest = (MultipartFormHttpServiceRequest) request;
        System.out.println(multipartFormHttpRequest.files());
    }
}

class HttpFilter1 implements HttpFilter
{
    @Override
    public void doFilter(HttpFilterChain chain, HttpServiceRequest request, HttpServiceResponse response)
            throws Throwable
    {
        chain.doNextFilter(request, response);
    }
}