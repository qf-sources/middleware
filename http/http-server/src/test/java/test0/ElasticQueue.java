package test0;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

class ElasticQueue implements BlockingQueue<Runnable>
{
    private final BlockingQueue<Runnable> queue;
    private final ElasticPolicy policy;

    ElasticQueue(BlockingQueue<Runnable> queue, ElasticPolicy policy)
    {
        this.queue = queue;
        this.policy = policy;
    }

    public boolean offerForce(Runnable e)
    {
        boolean result = queue.offer(e);
        if (result)
        {
            policy.onAdded(e);
        }
        return result;
    }

    @Override
    public boolean add(Runnable e)
    {
        return queue.add(e);
    }

    @Override
    public boolean offer(Runnable e)
    {
        if (policy.isNeedExpansion())
        {
            return false;
        }

        boolean result = queue.offer(e);
        if (result)
        {
            policy.onAdded(e);
        }
        return result;
    }

    @Override
    public Runnable remove()
    {
        return queue.remove();
    }

    @Override
    public Runnable poll()
    {
        return queue.poll();
    }

    @Override
    public Runnable element()
    {
        return queue.element();
    }

    @Override
    public Runnable peek()
    {
        return queue.peek();
    }

    @Override
    public void put(Runnable e) throws InterruptedException
    {
        queue.put(e);
    }

    @Override
    public boolean offer(Runnable e, long timeout, TimeUnit unit) throws InterruptedException
    {
        return queue.offer(e, timeout, unit);
    }

    @Override
    public Runnable take() throws InterruptedException
    {
        Runnable e = queue.take();
        policy.onRemoved(e);
        return e;
    }

    @Override
    public Runnable poll(long timeout, TimeUnit unit) throws InterruptedException
    {
        Runnable e = queue.poll(timeout, unit);
        if (e == null)
        {
            return null;
        }
        policy.onRemoved(e);
        return e;
    }

    @Override
    public int remainingCapacity()
    {
        return queue.remainingCapacity();
    }

    @Override
    public boolean remove(Object o)
    {
        return queue.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c)
    {
        return queue.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Runnable> c)
    {
        return queue.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c)
    {
        return queue.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c)
    {
        return queue.retainAll(c);
    }

    @Override
    public void clear()
    {
        queue.clear();
    }

    @Override
    public int size()
    {
        return queue.size();
    }

    @Override
    public boolean isEmpty()
    {
        return queue.isEmpty();
    }

    @Override
    public boolean contains(Object o)
    {
        return queue.contains(o);
    }

    @Override
    public Iterator<Runnable> iterator()
    {
        return queue.iterator();
    }

    @Override
    public Object[] toArray()
    {
        return queue.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a)
    {
        return queue.toArray(a);
    }

    @Override
    public int drainTo(Collection<? super Runnable> c)
    {
        return queue.drainTo(c);
    }

    @Override
    public int drainTo(Collection<? super Runnable> c, int maxElements)
    {
        return queue.drainTo(c, maxElements);
    }
}
