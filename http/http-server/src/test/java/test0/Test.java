package test0;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class Test
{
    public static void main(String[] args)
    {
        ExecutorService executorService = ElasticExecutors.create(1, 8, 60000, new QueueDepthElasticPolicy(9980));

        for (int i = 0; i < 10000; i++)
        {
            executorService.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(1000));
                }
            });
        }
    }
}

