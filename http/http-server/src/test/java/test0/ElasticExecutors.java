package test0;

import java.util.concurrent.*;

public class ElasticExecutors
{
    private static final RejectedExecutionHandler DEFAULT_HANDLER = new ThreadPoolExecutor.AbortPolicy();

    private ElasticExecutors()
    {
    }

    public static ExecutorService create(int coreSize, int maxSize, int keepAliveMillis, ElasticPolicy policy)
    {
        return create(coreSize, maxSize, keepAliveMillis, TimeUnit.MILLISECONDS, policy, null, null, null);
    }

    public static ExecutorService create(int coreSize, int maxSize, int keepAliveMillis, BlockingQueue<Runnable> queue,
            ElasticPolicy policy)
    {
        return create(coreSize, maxSize, keepAliveMillis, TimeUnit.MILLISECONDS, policy, queue, null, null);
    }

    public static ExecutorService create(int coreSize, int maxSize, int keepAliveMillis, ElasticPolicy policy,
            BlockingQueue<Runnable> queue, RejectedExecutionHandler rejectedExecutionHandler)
    {
        return create(coreSize, maxSize, keepAliveMillis, TimeUnit.MILLISECONDS, policy, queue, null,
                rejectedExecutionHandler);
    }

    public static ExecutorService create(int coreSize, int maxSize, int keepAliveTime, TimeUnit unit,
            ElasticPolicy policy, BlockingQueue<Runnable> queue, ThreadFactory factory,
            RejectedExecutionHandler rejectedExecutionHandler)
    {
        if (queue == null) queue = new LinkedBlockingQueue<Runnable>();
        if (rejectedExecutionHandler == null) rejectedExecutionHandler = DEFAULT_HANDLER;

        ElasticQueue elasticQueue = new ElasticQueue(queue, policy);

        ThreadPoolExecutor executor;
        if (factory == null)
        {
            executor = new ThreadPoolExecutor(coreSize, maxSize, keepAliveTime, unit, elasticQueue,
                    new ElasticRejectedExecutionHandler(elasticQueue, rejectedExecutionHandler));
        }
        else
        {
            executor = new ThreadPoolExecutor(coreSize, maxSize, keepAliveTime, unit, elasticQueue, factory,
                    new ElasticRejectedExecutionHandler(elasticQueue, rejectedExecutionHandler));
        }
        policy.setExecutor(executor);
        return executor;
    }
}
