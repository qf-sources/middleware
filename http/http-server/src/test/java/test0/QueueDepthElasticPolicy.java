package test0;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;

public class QueueDepthElasticPolicy implements ElasticPolicy
{
    private final AtomicLong depth = new AtomicLong(0);

    private final long topDepth;

    public QueueDepthElasticPolicy(long topDepth)
    {
        this.topDepth = topDepth;
    }

    @Override
    public void onAdded(Runnable e)
    {
        depth.getAndIncrement();
    }

    @Override
    public void onRemoved(Runnable e)
    {
        depth.getAndDecrement();
    }

    @Override
    public boolean isNeedExpansion()
    {
        return depth.get() >= topDepth;
    }

    @Override
    public void setExecutor(ThreadPoolExecutor executor)
    {
    }
}
