package test0;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

class ElasticRejectedExecutionHandler implements RejectedExecutionHandler
{
    private final ElasticQueue elasticQueue;
    private final RejectedExecutionHandler rejectedExecutionHandler;

    ElasticRejectedExecutionHandler(ElasticQueue elasticQueue, RejectedExecutionHandler rejectedExecutionHandler)
    {
        this.elasticQueue = elasticQueue;
        this.rejectedExecutionHandler = rejectedExecutionHandler;
    }

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor)
    {
        if (elasticQueue.offerForce(r))
        {
            return;
        }
        rejectedExecutionHandler.rejectedExecution(r, executor);
    }
}
