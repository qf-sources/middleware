package test0;

import java.util.concurrent.ThreadPoolExecutor;

public interface ElasticPolicy
{
    void onAdded(Runnable e);

    void onRemoved(Runnable e);

    boolean isNeedExpansion();

    void setExecutor(ThreadPoolExecutor executor);
}
