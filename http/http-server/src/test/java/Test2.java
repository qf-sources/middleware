import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.util.ResourceLeakDetector;
import io.qiufen.http.server.provider.HttpServer;
import io.qiufen.http.server.provider.HttpServerConfig;
import io.qiufen.http.server.provider.command.DefaultHttpCommandBuilder;
import io.qiufen.http.server.provider.context.body.BodyHttpContextSupport;
import io.qiufen.http.server.provider.context.body.stream.StreamSupport;
import io.qiufen.http.server.provider.context.common.CommonHttpContextSupport;
import io.qiufen.http.server.provider.dispatcher.SimpleContextDispatcher;
import io.qiufen.http.server.provider.router.SimpleHttpRouter;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.http.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class Test2
{
    public static void main(String[] args) throws InterruptedException
    {
        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);

        HttpServerConfig config = new HttpServerConfig()
        {{
            setPort(8081);
        }};
        SimpleHttpRouter httpRouter = new SimpleHttpRouter();
        httpRouter.addHttpService("/download", new Download());
        httpRouter.addHttpService("/upload", new Upload());
        httpRouter.addHttpService("/test/test", new Test());

        new HttpServer(config).addHttpContextSupport(new CommonHttpContextSupport())
                .addHttpContextSupport(new BodyHttpContextSupport(StreamSupport.of(ContentType.APPLICATION_JSON)))
                .setHttpContextDispatcher(new SimpleContextDispatcher(Executors.newFixedThreadPool(8),
                        new DefaultHttpCommandBuilder(httpRouter)))
                .start();
    }

    public static class Download implements HttpService
    {
        @Override
        public void doService(HttpServiceRequest request, HttpServiceResponse response) throws Exception
        {
            File file = new File("E:\\backup\\CLion-2019.1.4.win.zip");
            response.addHeader("content-type", "application/octet-stream");
            response.addHeader("content-length", String.valueOf(file.length()));
            response.addHeader("content-disposition", "attachment;filename=test");
            WritableChannel channel = response.getChannel();
            FileChannel fileChannel = new FileInputStream(file).getChannel();
            int length = 4096;
            long position = 0;
            ByteBuf buf;
            while ((buf = ByteBufAllocator.DEFAULT.buffer(length)).writeBytes(fileChannel, position, length) != -1)
            {
                channel.writeFinal(buf);
                position += length;
            }
            fileChannel.close();
            channel.close();
        }
    }

    private static class Upload implements HttpService
    {
        @Override
        public void doService(HttpServiceRequest request, HttpServiceResponse response) throws Exception
        {
            FileChannel fileChannel = new FileOutputStream(
                    "C:\\Users\\Ruzheng Zhang\\Desktop\\" + UUID.randomUUID()).getChannel();
            ReadableChannel channel = request.getChannel();
            ByteBuf buf;
            long position = 0;
            while (!channel.isEOF(buf = channel.read(ByteBuf.class)))
            {
                int length = buf.readableBytes();
                buf.readBytes(fileChannel, position, length);
                position += length;
                buf.release();
            }
            channel.close();
            fileChannel.close();
        }
    }

    private static class Test implements HttpService
    {
        static Executor executor = Executors.newCachedThreadPool();

        @Override
        public void doService(HttpServiceRequest request, final HttpServiceResponse response) throws Exception
        {
            request.getChannel().read(ByteBuf.class).release();

            response.setAutoCommit(false);
            executor.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(100));
                    response.setContentType("application/json");
                    String msg = "{\"success\":true,\"code\":\"0\",\"message\":\"complete\",\"object\":{\"name\":\"0123456789\"}}";
                    byte[] bytes = msg.getBytes();
                    response.setContentLength(bytes.length);
                    WritableChannel channel = response.getChannel();
                    try
                    {
                        channel.writeFinal(bytes);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    response.commit();
                }
            });
        }
    }
}
