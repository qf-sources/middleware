package test2;

import io.netty.util.ResourceLeakDetector;
import io.qiufen.http.server.provider.HttpServer;
import io.qiufen.http.server.provider.HttpServerConfig;
import io.qiufen.http.server.provider.command.DefaultHttpCommandBuilder;
import io.qiufen.http.server.provider.context.body.BodyHttpContextSupport;
import io.qiufen.http.server.provider.context.body.stream.StreamSupport;
import io.qiufen.http.server.provider.context.common.CommonHttpContextSupport;
import io.qiufen.http.server.provider.dispatcher.SimpleContextDispatcher;
import io.qiufen.http.server.provider.router.SimpleHttpRouter;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.http.HttpService;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;

import java.util.concurrent.Executors;

public class Test2
{
    public static void main(String[] args) throws InterruptedException
    {
        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);

        HttpServerConfig config = new HttpServerConfig()
        {{
            setPort(8080);
        }};
        SimpleHttpRouter httpRouter = new SimpleHttpRouter();
        httpRouter.addHttpService("/users/create", new Test());

        new HttpServer(config).addHttpContextSupport(new CommonHttpContextSupport())
                .addHttpContextSupport(new BodyHttpContextSupport(StreamSupport.of(ContentType.APPLICATION_JSON)))
                .setHttpContextDispatcher(new SimpleContextDispatcher(Executors.newCachedThreadPool(),
                        new DefaultHttpCommandBuilder(httpRouter)))
                .start();
    }

    private static class Test implements HttpService
    {
        @Override
        public void doService(HttpServiceRequest request, final HttpServiceResponse response) throws Exception
        {
        }
    }
}
