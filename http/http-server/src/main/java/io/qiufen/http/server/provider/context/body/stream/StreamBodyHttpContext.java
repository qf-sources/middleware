package io.qiufen.http.server.provider.context.body.stream;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpRequest;
import io.qiufen.common.io.IOUtil;
import io.qiufen.http.server.provider.context.common.CommonHttpServiceResponse;
import io.qiufen.http.server.provider.context.common.StreamReadableChannel;
import io.qiufen.http.server.provider.context.common.StreamWritableChannel;
import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.http.server.spi.net.ChannelContext;

public class StreamBodyHttpContext implements HttpContext
{
    private StreamBodyHttpServiceRequest request;
    private CommonHttpServiceResponse response;

    private StreamReadableChannel readableChannel;
    private StreamWritableChannel writableChannel;

    protected StreamBodyHttpContext()
    {
    }

    @Override
    public HttpServiceRequest getRequest()
    {
        return request;
    }

    @Override
    public HttpServiceResponse getResponse()
    {
        return response;
    }

    @Override
    public void doStart(ChannelContext context, HttpRequest msg)
    {
        ChannelHandlerContext channelHandlerContext = context.getContext();
        readableChannel = new StreamReadableChannel(channelHandlerContext);
        writableChannel = new StreamWritableChannel(channelHandlerContext);
        request = new StreamBodyHttpServiceRequest(context, msg, readableChannel);
        response = new CommonHttpServiceResponse(context, this, writableChannel);
    }

    @Override
    public void doContent(ChannelHandlerContext ctx, HttpContent msg)
    {
        readableChannel.offer(msg.content().retain());
    }

    @Override
    public void doEnd(ChannelHandlerContext ctx)
    {
        readableChannel.finish();
    }

    @Override
    public void close()
    {
        IOUtil.close(readableChannel);
        IOUtil.close(writableChannel);
    }
}
