package io.qiufen.http.server.provider.router;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.qiufen.http.server.provider.exception.HttpResponseStatusException;
import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.http.server.spi.http.HttpService;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.router.HttpRoute;
import io.qiufen.http.server.spi.router.HttpRouter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class SimpleHttpRouter implements HttpRouter<String>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleHttpRouter.class);

    private final Map<String, HttpRoute> routes = new HashMap<String, HttpRoute>();

    private final Map<String, List<HttpFilter>> filters = new LinkedHashMap<String, List<HttpFilter>>();

    @Override
    public void addHttpService(String uri, HttpService httpService)
    {
        if (routes.containsKey(uri))
        {
            throw new RuntimeException("Route has already existed! uri:" + uri);
        }
        SimpleHttpRoute route = new SimpleHttpRoute(uri, httpService);
        this.routes.put(uri, route);
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Add http service,uri:{},type:{}", uri, httpService.getClass().getName());
        }

        route.matchHttpFilters(this.filters);
    }

    @Override
    public void addHttpFilter(String uri, HttpFilter httpFilter)
    {
        List<HttpFilter> filterList = this.filters.get(uri);
        if (filterList == null)
        {
            filterList = new ArrayList<HttpFilter>();
            this.filters.put(uri, filterList);
        }
        filterList.add(httpFilter);
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Add http filter,uri:{},type:{}", uri, httpFilter.getClass().getName());
        }

        for (Map.Entry<String, HttpRoute> entry : routes.entrySet())
        {
            ((SimpleHttpRoute) entry.getValue()).matchHttpFilters(this.filters);
        }
    }

    @Override
    public HttpRoute hitHttpRoute(HttpServiceRequest request)
    {
        String uri = request.path();
        HttpRoute route = this.routes.get(uri);
        if (route == null)
        {
            throw new HttpResponseStatusException(HttpResponseStatus.NOT_FOUND.code(),
                    "Can't hit http service,uri:" + uri);
        }
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Hit http service,uri:{},type:{}", uri, route.getHttpService().getClass().getName());
        }
        return route;
    }

    @Override
    public void removeHttpService(String uri)
    {
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Remove http service,uri:{}", uri);
        }
        this.routes.remove(uri);
    }
}

