package io.qiufen.http.server.spi.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

/**
 * 过滤器链
 */
public class HttpFilterChain
{
    private static final Logger log = LoggerFactory.getLogger(HttpFilterChain.class);

    private final Iterator<HttpFilter> iterator;
    private final HttpService service;

    /**
     * 过滤器组
     *
     * @param iterator 过滤器组
     * @param service  请求服务
     */
    public HttpFilterChain(Iterator<HttpFilter> iterator, HttpService service)
    {
        this.iterator = iterator;
        this.service = service;
    }

    /**
     * 处理下一级过滤器
     *
     * @param request  request
     * @param response response
     */
    public void doNextFilter(HttpServiceRequest request, HttpServiceResponse response) throws Exception
    {
        HttpFilter nextFilter = getNextFilter();
        if (null == nextFilter)
        {
            if (log.isDebugEnabled())
            {
                log.debug("Do http service:{}...", service.getClass().getName());
            }
            service.doService(request, response);
        }
        else
        {
            if (log.isDebugEnabled())
            {
                log.debug("Do http filter:{}...", nextFilter.getClass().getName());
            }
            try
            {
                nextFilter.doFilter(this, request, response);
            }
            catch (Throwable throwable)
            {
                throw new RuntimeException(throwable);
            }
        }
    }

    private HttpFilter getNextFilter()
    {
        return iterator.hasNext() ? iterator.next() : null;
    }
}
