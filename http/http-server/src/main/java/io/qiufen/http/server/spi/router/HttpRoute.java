package io.qiufen.http.server.spi.router;

import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.http.server.spi.http.HttpService;

import java.util.Iterator;

public interface HttpRoute
{
    HttpService getHttpService();

    Iterator<HttpFilter> getHttpFilterList();
}
