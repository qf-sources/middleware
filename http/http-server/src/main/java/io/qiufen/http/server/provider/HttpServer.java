package io.qiufen.http.server.provider;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.qiufen.common.lang.Strings;
import io.qiufen.common.net.ChannelMonitorHandler;
import io.qiufen.http.server.provider.constant.CommonConstant;
import io.qiufen.http.server.provider.context.HttpContextSupportRegistry;
import io.qiufen.http.server.provider.net.ChannelContextBuilder;
import io.qiufen.http.server.provider.net.DefaultChannelContext;
import io.qiufen.http.server.provider.net.HttpChannelEventHandler;
import io.qiufen.http.server.spi.context.HttpContextSupport;
import io.qiufen.http.server.spi.dispatcher.HttpContextDispatcher;
import io.qiufen.http.server.spi.net.ChannelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

/**
 * Created by Administrator on 2018/4/14.
 */
public class HttpServer
{
    private final Logger log = LoggerFactory.getLogger(HttpServer.class);
    private final HttpServerConfig config;
    private final HttpContextSupportRegistry supportRegistry;
    private volatile boolean started = false;
    private HttpContextDispatcher httpContextDispatcher;

    private ChannelContextBuilder contextBuilder;

    private NioEventLoopGroup boss;
    private NioEventLoopGroup worker;

    public HttpServer(HttpServerConfig config)
    {
        this.config = config;

        this.supportRegistry = new HttpContextSupportRegistry();
    }

    public HttpServer start() throws InterruptedException
    {
        if (started)
        {
            throw new RuntimeException("Http server instance has been started.");
        }
        synchronized (this)
        {
            if (started)
            {
                throw new RuntimeException("Http server instance has been started.");
            }
            boss = new NioEventLoopGroup(config.getBossThreads());
            worker = new NioEventLoopGroup(config.getWorkerThreads());

            this.contextBuilder = new ChannelContextBuilder()
            {
                @Override
                public ChannelContext create(ChannelHandlerContext ctx)
                {
                    return new DefaultChannelContext(ctx);
                }
            };

            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.channel(NioServerSocketChannel.class);
            bootstrap.group(boss, worker);
            bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
            bootstrap.childHandler(new ChannelInitializer<SocketChannel>()
            {
                private final ChannelMonitorHandler channelMonitorHandler = new ChannelMonitorHandler(
                        CommonConstant.CHANNEL_MONITOR);

                @Override
                protected void initChannel(final SocketChannel channel)
                {
                    ChannelPipeline pipeline = channel.pipeline();
                    pipeline.addLast(new HttpRequestDecoder());
                    pipeline.addLast(new HttpResponseEncoder());

                    pipeline.addLast(channelMonitorHandler);
                    pipeline.addLast(
                            new HttpChannelEventHandler(supportRegistry, httpContextDispatcher, contextBuilder));
                }
            });

            Channel channel;
            if (Strings.isEmpty(config.getHost()))
            {
                channel = bootstrap.bind(config.getPort()).sync().channel();
            }
            else
            {
                channel = bootstrap.bind(config.getHost(), config.getPort()).sync().channel();
            }
            InetSocketAddress address = (InetSocketAddress) channel.localAddress();
            config.setHost(address.getAddress().getHostAddress());
            config.setPort(address.getPort());
            log.info("Http server started on port@{}:{}.", config.getHost(), config.getPort());
            channel.closeFuture();
            started = true;
        }
        return this;
    }

    public HttpServerConfig getConfig()
    {
        return config;
    }

    public void close()
    {
        worker.shutdownGracefully();
        boss.shutdownGracefully();
        this.httpContextDispatcher.close();
    }

    public HttpServer addHttpContextSupport(HttpContextSupport support)
    {
        supportRegistry.addHttpContextSupport(support);
        return this;
    }

    public HttpServer setHttpContextDispatcher(HttpContextDispatcher httpContextDispatcher)
    {
        this.httpContextDispatcher = httpContextDispatcher;
        return this;
    }
}
