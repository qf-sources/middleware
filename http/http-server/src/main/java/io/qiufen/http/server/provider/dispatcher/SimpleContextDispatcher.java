package io.qiufen.http.server.provider.dispatcher;

import io.netty.util.concurrent.DefaultThreadFactory;
import io.qiufen.http.server.spi.command.HttpCommand;
import io.qiufen.http.server.spi.command.HttpCommandBuilder;
import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.dispatcher.HttpContextDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleContextDispatcher implements HttpContextDispatcher
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleContextDispatcher.class);

    private final ExecutorService executor;
    private final HttpCommandBuilder httpCommandBuilder;

    public SimpleContextDispatcher(HttpCommandBuilder httpCommandBuilder)
    {
        this(Executors.newCachedThreadPool(new DefaultThreadFactory("HttpService")), httpCommandBuilder);
    }

    public SimpleContextDispatcher(ExecutorService executor, HttpCommandBuilder httpCommandBuilder)
    {
        this.executor = executor;
        this.httpCommandBuilder = httpCommandBuilder;
    }

    @Override
    public void dispatch(HttpContext httpContext)
    {
        LOGGER.info("Dispatch http context:{}...", httpContext);
        HttpCommand command = httpCommandBuilder.build(httpContext);
        try
        {
            executor.execute(command);
        }
        catch (RuntimeException e)
        {
            try
            {
                LOGGER.error("Execute failure", e);
                command.doReject();
            }
            finally
            {
                command.doFinally();
            }
        }
    }

    @Override
    public void close()
    {
        this.executor.shutdown();
    }
}
