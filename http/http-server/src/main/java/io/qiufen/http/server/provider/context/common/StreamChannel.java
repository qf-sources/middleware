package io.qiufen.http.server.provider.context.common;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;

public class StreamChannel implements java.nio.channels.Channel
{
    private volatile boolean closed = false;

    @Override
    public final boolean isOpen()
    {
        return !this.closed;
    }

    @Override
    public final void close() throws IOException
    {
        if (this.closed) return;
        synchronized (this)
        {
            if (this.closed) return;
            this.closed = true;
            this.close0();
        }
    }

    protected final boolean isClosed() {return this.closed;}

    protected final void checkState() throws ClosedChannelException
    {
        if (this.closed) throw new ClosedChannelException();
    }

    protected void close0() throws IOException {}
}
