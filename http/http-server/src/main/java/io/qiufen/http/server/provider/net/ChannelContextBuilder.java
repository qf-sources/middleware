package io.qiufen.http.server.provider.net;

import io.netty.channel.ChannelHandlerContext;
import io.qiufen.http.server.spi.net.ChannelContext;

public interface ChannelContextBuilder
{
    ChannelContext create(ChannelHandlerContext ctx);
}
