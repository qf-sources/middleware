package io.qiufen.http.server.provider.context.body;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.qiufen.common.lang.CTString;
import io.qiufen.http.server.provider.exception.HttpResponseStatusException;
import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.context.HttpContextAdapter;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.context.body.ContentTypeSupport;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.http.server.spi.net.ChannelContext;

interface Hit
{
    ContentTypeSupport hit(CTString type);
}

public class BodyHttpContext extends HttpContextAdapter
{
    private final Hit hit;

    private HttpContext httpContext;

    BodyHttpContext(Hit hit)
    {
        this.hit = hit;
    }

    @Override
    public HttpServiceRequest getRequest()
    {
        return this.httpContext.getRequest();
    }

    @Override
    public HttpServiceResponse getResponse()
    {
        return this.httpContext.getResponse();
    }

    @Override
    public void doStart(ChannelContext context, io.netty.handler.codec.http.HttpRequest msg)
    {
        String contentType = msg.headers().get(HttpHeaderNames.CONTENT_TYPE);
        ContentTypeSupport support = hitSupport(contentType);
        this.httpContext = support.create();
        this.httpContext.doStart(context, msg);
    }

    @Override
    public void doContent(ChannelHandlerContext ctx, HttpContent msg)
    {
        this.httpContext.doContent(ctx, msg);
    }

    @Override
    public void doEnd(ChannelHandlerContext ctx)
    {
        this.httpContext.doEnd(ctx);
    }

    @Override
    public void close()
    {
        this.httpContext.close();
    }

    /**
     * 命中受内容类型支持的实现
     *
     * @param contentType contentType
     * @return 受支持的实现
     */
    private ContentTypeSupport hitSupport(String contentType)
    {
        int contentTypeLength;
        if (contentType == null || (contentTypeLength = contentType.length()) == 0)
        {
            ContentTypeSupport support = hit.hit(ContentType.DEFAULT.getValue());
            if (support == null)
            {
                throw new HttpResponseStatusException(HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE.code(),
                        "Not support any content type.");
            }
            return support;
        }

        int index = 0;
        for (; index < contentTypeLength; index++)
        {
            if (contentType.charAt(index) == ';') break;
        }
        return hit.hit(CTString.wrap(contentType, 0, index));
    }
}