package io.qiufen.http.server.provider.context.body.raw;

import io.netty.handler.codec.http.HttpRequest;
import io.qiufen.http.server.provider.context.body.stream.StreamBodyHttpContext;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.net.ChannelContext;

public class RawHttpContext extends StreamBodyHttpContext
{
    private RawHttpServiceRequest request;

    RawHttpContext()
    {
    }

    @Override
    public HttpServiceRequest getRequest()
    {
        return request;
    }

    @Override
    public void doStart(ChannelContext context, HttpRequest msg)
    {
        super.doStart(context, msg);
        request = new RawHttpServiceRequest(super.getRequest());
    }
}
