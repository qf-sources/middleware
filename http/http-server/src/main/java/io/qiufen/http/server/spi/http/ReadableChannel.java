package io.qiufen.http.server.spi.http;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public interface ReadableChannel extends ReadableByteChannel
{
    boolean isEOF(int readResult);

    boolean isEOF(Object readResult);

    /**
     * 存在内存交换
     */
    @Override
    int read(ByteBuffer dst) throws IOException;

    /**
     * 由内部实现决定是否内存交换，netty byte buf默认不存在
     */
    <T> T read(Class<T> expectType) throws IOException;

    /**
     * 由内部实现决定是否内存交换，netty byte buf默认不存在
     */
    <T> T read(int bufferSize, Class<T> expectType) throws IOException;
}

