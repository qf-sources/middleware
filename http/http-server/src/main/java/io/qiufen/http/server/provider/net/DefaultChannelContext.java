package io.qiufen.http.server.provider.net;

import io.netty.channel.ChannelHandlerContext;
import io.qiufen.http.server.spi.net.ChannelContext;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;

public class DefaultChannelContext implements ChannelContext
{
    private final ChannelHandlerContext context;

    private volatile AtomicBoolean lock;

    public DefaultChannelContext(ChannelHandlerContext context)
    {
        this.context = context;
    }

    @Override
    public ChannelHandlerContext getContext()
    {
        return this.context;
    }

    @Override
    public void close()
    {
        if (this.context != null)
        {
            this.context.close();
        }
    }

    public void startWrite()
    {
        while (!this.getWriteLock().compareAndSet(false, true))
        {
            LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(1));
        }
    }

    public void finishWrite()
    {
        this.getWriteLock().set(false);
    }

    private AtomicBoolean getWriteLock()
    {
        if (this.lock != null)
        {
            return this.lock;
        }
        synchronized (this)
        {
            if (this.lock != null)
            {
                return this.lock;
            }
            this.lock = new AtomicBoolean(false);
        }
        return this.lock;
    }
}
