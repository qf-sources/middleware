package io.qiufen.http.server.provider.context;

import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.qiufen.http.server.provider.exception.HttpResponseStatusException;
import io.qiufen.http.server.spi.context.HttpContextSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

interface Hit
{
    HttpContextSupport hit(HttpMethod method);
}

public class HttpContextSupportRegistry
{
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpContextSupportRegistry.class);

    private final Map<HttpMethod, HttpContextSupport> supportMap;

    private Hit hit;

    public HttpContextSupportRegistry()
    {
        this.supportMap = new HashMap<HttpMethod, HttpContextSupport>();
    }

    public void addHttpContextSupport(HttpContextSupport support)
    {
        HttpMethod[] methods = support.support();
        for (HttpMethod method : methods)
        {
            if (this.supportMap.containsKey(method))
            {
                throw new RuntimeException("Has add http context support for method:" + method);
            }
            if (LOGGER.isDebugEnabled())
            {
                LOGGER.debug("Add http context support,method:{},type:{}", method, support.getClass().getName());
            }
            this.supportMap.put(method, support);
        }
        this.hit = initHit(this.supportMap);
    }

    public HttpContextSupport hitHttpContextSupport(HttpMethod method)
    {
        LOGGER.debug("Try hit http context support,method:{}...", method);
        HttpContextSupport support = this.hit.hit(method);
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Hit http context support,method:{},type:{}", method, support.getClass().getName());
        }
        return support;
    }

    private Hit initHit(Map<HttpMethod, HttpContextSupport> supportMap)
    {
        int len = supportMap.size();
        if (len <= 0)
        {
            return new None();
        }
        if (len == 1)
        {
            Hit hit = null;
            for (Map.Entry<HttpMethod, HttpContextSupport> entry : supportMap.entrySet())
            {
                hit = HttpContextSupport.DEFAULT.equals(entry.getKey()) ? new Default(entry.getValue()) : new Single(
                        entry.getKey(), entry.getValue());
            }
            return hit;
        }
        if (len == 2)
        {
            HttpContextSupport defaultSupport = null;
            HttpMethod method = null;
            HttpContextSupport support = null;
            for (Map.Entry<HttpMethod, HttpContextSupport> entry : supportMap.entrySet())
            {
                if (HttpContextSupport.DEFAULT.equals(entry.getKey()))
                {
                    defaultSupport = entry.getValue();
                    continue;
                }
                method = entry.getKey();
                support = entry.getValue();
            }
            return defaultSupport == null ? new Normal(supportMap) : new DefaultSingle(defaultSupport, method, support);
        }

        HttpContextSupport defaultSupport = supportMap.get(HttpContextSupport.DEFAULT);
        return defaultSupport == null ? new Normal(supportMap) : new DefaultNormal(defaultSupport, supportMap);
    }
}

class None implements Hit
{
    @Override
    public HttpContextSupport hit(HttpMethod method)
    {
        throw new HttpResponseStatusException(HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE.code(),
                "Not support any content type.");
    }
}

class Default implements Hit
{
    private final HttpContextSupport support;

    Default(HttpContextSupport support)
    {
        this.support = support;
    }

    @Override
    public HttpContextSupport hit(HttpMethod method)
    {
        return support;
    }
}

class Single implements Hit
{
    private final HttpMethod method;
    private final HttpContextSupport support;

    Single(HttpMethod method, HttpContextSupport support)
    {
        this.method = method;
        this.support = support;
    }

    @Override
    public HttpContextSupport hit(HttpMethod method)
    {
        if (this.method.equals(method))
        {
            return support;
        }
        throw new HttpResponseStatusException(HttpResponseStatus.NOT_IMPLEMENTED.code(),
                "Can't hit http context support!method:" + method);
    }
}

class DefaultSingle implements Hit
{
    private final HttpContextSupport defaultSupport;
    private final HttpMethod method;
    private final HttpContextSupport support;

    DefaultSingle(HttpContextSupport defaultSupport, HttpMethod method, HttpContextSupport support)
    {
        this.defaultSupport = defaultSupport;
        this.method = method;
        this.support = support;
    }

    @Override
    public HttpContextSupport hit(HttpMethod method)
    {
        return this.method.equals(method) ? support : this.defaultSupport;
    }
}

class Normal implements Hit
{
    private final Map<HttpMethod, HttpContextSupport> supportMap;

    Normal(Map<HttpMethod, HttpContextSupport> supportMap)
    {
        this.supportMap = supportMap;
    }

    @Override
    public HttpContextSupport hit(HttpMethod method)
    {
        HttpContextSupport support = supportMap.get(method);
        if (support == null)
        {
            throw new HttpResponseStatusException(HttpResponseStatus.NOT_IMPLEMENTED.code(),
                    "Can't hit http context support!method:" + method);
        }
        return support;
    }
}

class DefaultNormal implements Hit
{
    private final HttpContextSupport defaultSupport;
    private final Map<HttpMethod, HttpContextSupport> supportMap;

    DefaultNormal(HttpContextSupport defaultSupport, Map<HttpMethod, HttpContextSupport> supportMap)
    {
        this.defaultSupport = defaultSupport;
        this.supportMap = supportMap;
    }

    @Override
    public HttpContextSupport hit(HttpMethod method)
    {
        HttpContextSupport support = supportMap.get(method);
        return support == null ? defaultSupport : support;
    }
}