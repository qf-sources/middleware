package io.qiufen.http.server.provider.context.body.multipart;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.multipart.HttpPostMultipartRequestDecoder;
import io.qiufen.common.io.IOUtil;
import io.qiufen.http.server.provider.context.AbstractHttpContext;
import io.qiufen.http.server.provider.context.common.CommonHttpServiceResponse;
import io.qiufen.http.server.provider.context.common.StreamWritableChannel;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.http.server.spi.net.ChannelContext;

public class MultipartFormDataHttpContext extends AbstractHttpContext
{
    private HttpPostMultipartRequestDecoder decoder;

    private MultipartFormHttpServiceRequest request;
    private CommonHttpServiceResponse response;

    private StreamWritableChannel writableChannel;

    MultipartFormDataHttpContext()
    {
    }

    @Override
    public HttpServiceRequest getRequest()
    {
        return request;
    }

    @Override
    public HttpServiceResponse getResponse()
    {
        return response;
    }

    @Override
    public void doStart(ChannelContext context, io.netty.handler.codec.http.HttpRequest msg)
    {
        decoder = new HttpPostMultipartRequestDecoder(msg);

        this.request = new MultipartFormHttpServiceRequest(context, decoder, msg);
        writableChannel = new StreamWritableChannel(context.getContext());
        this.response = new CommonHttpServiceResponse(context, this, writableChannel);
    }

    @Override
    protected void doContent0(ChannelHandlerContext ctx, HttpContent msg)
    {
        decoder.offer(msg);
    }

    @Override
    protected void doEnd0(ChannelHandlerContext ctx)
    {
        this.request.setPrepared();
    }

    @Override
    protected void close0()
    {
        this.decoder.destroy();
        IOUtil.close(writableChannel);
    }
}
