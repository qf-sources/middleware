package io.qiufen.http.server.provider.exception;

public class HttpResponseException extends RuntimeException
{
    public HttpResponseException(String message)
    {
        super(message);
    }
}
