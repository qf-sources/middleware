package io.qiufen.http.server.spi.context;

import io.netty.handler.codec.http.HttpMethod;

public interface HttpContextSupport
{
    HttpMethod DEFAULT = new HttpMethod("*");

    HttpMethod[] support();

    HttpContext create();
}
