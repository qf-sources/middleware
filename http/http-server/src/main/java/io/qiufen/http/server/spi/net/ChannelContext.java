package io.qiufen.http.server.spi.net;

import io.netty.channel.ChannelHandlerContext;

public interface ChannelContext
{
    ChannelHandlerContext getContext();

    void close();
}
