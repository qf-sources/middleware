package io.qiufen.http.server.spi.http;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public interface WritableChannel extends WritableByteChannel
{
    /**
     * 存在内存交换，src改变不影响实际写入数据
     */
    @Override
    int write(ByteBuffer src) throws IOException;

    /**
     * 不存在内存交换，src改变会影响实际写入数据
     */
    void writeFinal(String src) throws IOException;

    /**
     * 不存在内存交换，src改变会影响实际写入数据
     */
    void writeFinal(byte[] src) throws IOException;

    /**
     * 不存在内存交换，src改变会影响实际写入数据
     */
    void writeFinal(byte[] src, int offset, int length) throws IOException;

    /**
     * 不存在内存交换，src改变会影响实际写入数据
     */
    void writeFinal(ByteBuffer src) throws IOException;

    /**
     * 不存在内存交换，src改变会影响实际写入数据
     */
    void writeFinal(ByteBuffer src, int offset, int length) throws IOException;

    /**
     * 不存在内存交换，src改变会影响实际写入数据
     */
    void writeFinal(Object src) throws IOException;
}
