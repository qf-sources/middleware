package io.qiufen.http.server.provider;

/**
 * 服务实例配置
 */
public class HttpServerConfig
{
    /**
     * 主机
     */
    private String host;

    /**
     * 监听端口号
     */
    private int port;

    /**
     * 主线程数量
     */
    private int bossThreads;

    /**
     * 从线程数量
     */
    private int workerThreads;

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getBossThreads()
    {
        return bossThreads;
    }

    public void setBossThreads(int bossThreads)
    {
        this.bossThreads = bossThreads;
    }

    public int getWorkerThreads()
    {
        return workerThreads;
    }

    public void setWorkerThreads(int workerThreads)
    {
        this.workerThreads = workerThreads;
    }
}
