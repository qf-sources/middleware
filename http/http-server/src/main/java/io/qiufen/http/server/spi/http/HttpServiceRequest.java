package io.qiufen.http.server.spi.http;

import io.netty.handler.codec.http.cookie.Cookie;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface HttpServiceRequest
{
    String getRequestURI();

    String path();

    String getMethod();

    String getContentType();

    String getHeader(String name);

    Map<String, String> headers();

    Map<String, List<String>> getParameterMap();

    List<String> getParameterValues(String name);

    String getParameter(String name);

    String getRemoteAddr();

    Cookie cookie(String name);

    Set<Cookie> getCookies();

    ReadableChannel getChannel();
}
