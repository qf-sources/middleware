package io.qiufen.http.server.provider.context.body;

import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.qiufen.common.lang.CTString;
import io.qiufen.http.server.provider.exception.HttpResponseStatusException;
import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.context.HttpContextSupport;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.context.body.ContentTypeSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class BodyHttpContextSupport implements HttpContextSupport
{
    private static final Logger LOGGER = LoggerFactory.getLogger(BodyHttpContextSupport.class);

    private final Map<CTString, ContentTypeSupport> supportMap;

    private final HttpMethod[] methods;

    private Hit hit;

    @Deprecated
    public BodyHttpContextSupport(ContentTypeSupport... supports)
    {
        this.methods = defaultMethods();

        this.supportMap = new HashMap<CTString, ContentTypeSupport>();
        contentType(supports);
    }

    private BodyHttpContextSupport(HttpMethod[] methods)
    {
        this.supportMap = new HashMap<CTString, ContentTypeSupport>();
        this.methods = methods;
    }

    public static BodyHttpContextSupport of(HttpMethod... methods)
    {
        if (methods == null || methods.length == 0) methods = defaultMethods();
        return new BodyHttpContextSupport(methods);
    }

    private static HttpMethod[] defaultMethods() {return new HttpMethod[]{HttpMethod.POST, HttpMethod.PUT};}

    @Override
    public HttpMethod[] support()
    {
        return this.methods;
    }

    @Override
    public HttpContext create()
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("New http context,type:{}.", BodyHttpContext.class.getName());
        }
        return new BodyHttpContext(this.hit);
    }

    public BodyHttpContextSupport contentType(ContentTypeSupport... supports)
    {
        for (ContentTypeSupport support : supports)
        {
            for (ContentType type : support.support())
            {
                this.supportMap.put(type.getValue(), support);
            }
        }
        this.hit = initHit(this.supportMap);
        return this;
    }

    private Hit initHit(Map<CTString, ContentTypeSupport> supportMap)
    {
        int len = supportMap.size();
        if (len <= 0)
        {
            return new None();
        }
        if (len == 1)
        {
            Hit hit = null;
            for (Map.Entry<CTString, ContentTypeSupport> entry : supportMap.entrySet())
            {
                hit = ContentType.DEFAULT.getValue().equals(entry.getKey()) ? new Default(
                        entry.getValue()) : new Single(entry.getKey(), entry.getValue());
            }
            return hit;
        }
        if (len == 2)
        {
            ContentTypeSupport defaultSupport = null;
            CTString type = null;
            ContentTypeSupport support = null;
            for (Map.Entry<CTString, ContentTypeSupport> entry : supportMap.entrySet())
            {
                if (ContentType.DEFAULT.getValue().equals(entry.getKey()))
                {
                    defaultSupport = entry.getValue();
                    continue;
                }
                type = entry.getKey();
                support = entry.getValue();
            }
            return defaultSupport == null ? new Normal(supportMap) : new DefaultSingle(defaultSupport, type, support);
        }

        ContentTypeSupport defaultSupport = supportMap.get(ContentType.DEFAULT.getValue());
        return defaultSupport == null ? new Normal(supportMap) : new DefaultNormal(defaultSupport, supportMap);
    }
}

class None implements Hit
{
    @Override
    public ContentTypeSupport hit(CTString type)
    {
        throw new HttpResponseStatusException(HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE.code(),
                "Not support any content type.");
    }
}

class Default implements Hit
{
    private final ContentTypeSupport support;

    Default(ContentTypeSupport support)
    {
        this.support = support;
    }

    @Override
    public ContentTypeSupport hit(CTString type)
    {
        return support;
    }
}

class Single implements Hit
{
    private final CTString type;
    private final ContentTypeSupport support;

    Single(CTString type, ContentTypeSupport support)
    {
        this.type = type;
        this.support = support;
    }

    @Override
    public ContentTypeSupport hit(CTString type)
    {
        if (this.type.equals(type))
        {
            return support;
        }
        throw new HttpResponseStatusException(HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE.code(),
                "Can't hit content type support:" + type);
    }
}

class DefaultSingle implements Hit
{
    private final ContentTypeSupport defaultSupport;
    private final CTString type;
    private final ContentTypeSupport support;

    DefaultSingle(ContentTypeSupport defaultSupport, CTString type, ContentTypeSupport support)
    {
        this.defaultSupport = defaultSupport;
        this.type = type;
        this.support = support;
    }

    @Override
    public ContentTypeSupport hit(CTString type)
    {
        return this.type.equals(type) ? support : this.defaultSupport;
    }
}

class Normal implements Hit
{
    private final Map<CTString, ContentTypeSupport> supportMap;

    Normal(Map<CTString, ContentTypeSupport> supportMap)
    {
        this.supportMap = supportMap;
    }

    @Override
    public ContentTypeSupport hit(CTString type)
    {
        ContentTypeSupport support = supportMap.get(type);
        if (support == null)
        {
            throw new HttpResponseStatusException(HttpResponseStatus.UNSUPPORTED_MEDIA_TYPE.code(),
                    "Can't hit content type support:" + type);
        }
        return support;
    }
}

class DefaultNormal implements Hit
{
    private final ContentTypeSupport defaultSupport;
    private final Map<CTString, ContentTypeSupport> supportMap;

    DefaultNormal(ContentTypeSupport defaultSupport, Map<CTString, ContentTypeSupport> supportMap)
    {
        this.defaultSupport = defaultSupport;
        this.supportMap = supportMap;
    }

    @Override
    public ContentTypeSupport hit(CTString type)
    {
        ContentTypeSupport support = supportMap.get(type);
        return support == null ? defaultSupport : support;
    }
}