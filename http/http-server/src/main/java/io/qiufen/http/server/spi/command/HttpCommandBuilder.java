package io.qiufen.http.server.spi.command;

import io.qiufen.http.server.spi.context.HttpContext;

public interface HttpCommandBuilder
{
    HttpCommand build(HttpContext httpContext);
}
