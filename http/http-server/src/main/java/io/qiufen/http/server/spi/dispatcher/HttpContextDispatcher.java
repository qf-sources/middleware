package io.qiufen.http.server.spi.dispatcher;

import io.qiufen.http.server.spi.context.HttpContext;

/**
 * http context调度器
 */
public interface HttpContextDispatcher
{
    /**
     * 调度资源
     *
     * @param httpContext http context
     */
    void dispatch(HttpContext httpContext);

    /**
     * 关闭调度器
     */
    void close();
}
