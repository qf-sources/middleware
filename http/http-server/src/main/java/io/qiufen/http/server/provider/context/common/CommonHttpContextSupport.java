package io.qiufen.http.server.provider.context.common;

import io.netty.handler.codec.http.HttpMethod;
import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.context.HttpContextSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonHttpContextSupport implements HttpContextSupport
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonHttpContextSupport.class);

    private final HttpMethod[] methods;

    @Deprecated
    public CommonHttpContextSupport()
    {
        this(defaultMethods());
    }

    private CommonHttpContextSupport(HttpMethod[] methods)
    {
        this.methods = methods;
    }

    public static CommonHttpContextSupport of(HttpMethod... methods)
    {
        if (methods == null || methods.length == 0) methods = defaultMethods();
        return new CommonHttpContextSupport(methods);
    }

    private static HttpMethod[] defaultMethods() {return new HttpMethod[]{HttpMethod.GET, HttpMethod.OPTIONS, HttpMethod.DELETE, HttpMethod.HEAD};}

    @Override
    public HttpMethod[] support()
    {
        return this.methods;
    }

    @Override
    public HttpContext create()
    {
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("New http context,type:{}.", CommonHttpContext.class.getName());
        }
        return new CommonHttpContext();
    }
}
