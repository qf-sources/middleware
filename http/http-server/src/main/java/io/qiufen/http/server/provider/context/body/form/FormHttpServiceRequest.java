package io.qiufen.http.server.provider.context.body.form;

import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.HttpPostStandardRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.qiufen.http.server.provider.context.body.BodyHttpServiceRequest;
import io.qiufen.http.server.spi.net.ChannelContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormHttpServiceRequest extends BodyHttpServiceRequest
{
    private final HttpPostStandardRequestDecoder decoder;

    private Map<String, List<String>> params;

    private boolean init = false;

    public FormHttpServiceRequest(ChannelContext ctx, HttpPostStandardRequestDecoder decoder, HttpRequest msg)
    {
        super(ctx, msg);
        this.decoder = decoder;
    }

    @Override
    public Map<String, List<String>> getParameterMap()
    {
        try
        {
            initDecode();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        this.params.putAll(super.getParameterMap());
        return this.params;
    }

    private void initDecode() throws IOException
    {
        if (this.init)
        {
            return;
        }

        //初始化变量：请求参数 & 文件参数
        if (this.params == null)
        {
            this.params = new HashMap<String, List<String>>();
        }

        //检查是否准备好，并进行解析数据
        checkReady();
        for (InterfaceHttpData data : decoder.getBodyHttpDatas())
        {
            if (data.getHttpDataType() == InterfaceHttpData.HttpDataType.Attribute)
            {
                Attribute attribute = (Attribute) data;
                List<String> valueList = this.params.get(attribute.getName());
                if (null == valueList)
                {
                    valueList = new ArrayList<String>(1);
                    this.params.put(attribute.getName(), valueList);
                }
                valueList.add(attribute.getValue());
            }
        }
        this.init = true;
    }
}
