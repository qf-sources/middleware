package io.qiufen.http.server.spi.http;

public interface HttpService
{
    void doService(HttpServiceRequest request, HttpServiceResponse response) throws Exception;
}
