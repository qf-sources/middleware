package io.qiufen.http.server.provider.context.body;

import io.netty.handler.codec.http.HttpRequest;
import io.qiufen.http.server.provider.context.common.CommonHttpServiceRequest;
import io.qiufen.http.server.spi.net.ChannelContext;

public class BodyHttpServiceRequest extends CommonHttpServiceRequest
{
    private volatile boolean prepared = false;

    protected BodyHttpServiceRequest(ChannelContext ctx, HttpRequest msg)
    {
        super(ctx, msg);
    }

    protected void checkReady()
    {
        if (this.prepared)
        {
            return;
        }
        synchronized (this)
        {
            if (this.prepared)
            {
                return;
            }
            try
            {
                this.wait();
            }
            catch (InterruptedException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    public void setPrepared()
    {
        this.prepared = true;
        synchronized (this)
        {
            this.notifyAll();
        }
    }
}
