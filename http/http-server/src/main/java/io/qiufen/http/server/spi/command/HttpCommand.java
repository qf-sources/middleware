package io.qiufen.http.server.spi.command;

import io.qiufen.common.concurrency.executor.api.Command;

public abstract class HttpCommand extends Command
{
    public abstract void doReject();

    @Override
    public void doFinally() { }
}
