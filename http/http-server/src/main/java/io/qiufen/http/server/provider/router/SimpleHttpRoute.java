package io.qiufen.http.server.provider.router;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.http.server.spi.http.HttpService;
import io.qiufen.http.server.spi.router.HttpRoute;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SimpleHttpRoute implements HttpRoute
{
    private final String uri;
    private final HttpService httpService;

    private List<HttpFilter> filterList;

    SimpleHttpRoute(String uri, HttpService httpService)
    {
        this.uri = uri;
        this.httpService = httpService;
    }

    @Override
    public HttpService getHttpService()
    {
        return httpService;
    }

    @Override
    public Iterator<HttpFilter> getHttpFilterList()
    {
        return filterList == null ? null : filterList.iterator();
    }

    void matchHttpFilters(Map<String, List<HttpFilter>> filters)
    {
        if (CollectionUtil.isNotEmpty(this.filterList))
        {
            this.filterList.clear();
        }

        for (Map.Entry<String, List<HttpFilter>> entry : filters.entrySet())
        {
            String filterUri = entry.getKey();
            List<HttpFilter> httpFilterList = entry.getValue();
            if (!this.uri.startsWith(filterUri))
            {
                continue;
            }

            if (this.filterList == null)
            {
                this.filterList = new ArrayList<HttpFilter>();
            }
            this.filterList.addAll(httpFilterList);
        }
    }
}
