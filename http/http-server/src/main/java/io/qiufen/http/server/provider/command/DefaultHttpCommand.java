package io.qiufen.http.server.provider.command;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.qiufen.http.server.provider.exception.HttpResponseStatusException;
import io.qiufen.http.server.spi.command.HttpCommand;
import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.http.server.spi.http.HttpFilterChain;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.http.server.spi.router.HttpRoute;
import io.qiufen.http.server.spi.router.HttpRouter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Iterator;

public class DefaultHttpCommand extends HttpCommand
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultHttpCommand.class);

    private final HttpRouter<?> httpRouter;
    private final HttpContext httpContext;

    DefaultHttpCommand(HttpRouter<?> httpRouter, HttpContext httpContext)
    {
        this.httpRouter = httpRouter;
        this.httpContext = httpContext;
    }

    @Override
    protected void doExecute() throws Throwable
    {
        HttpServiceRequest request = httpContext.getRequest();
        HttpServiceResponse response = httpContext.getResponse();
        // uri <-> http service
        HttpRoute httpRoute = httpRouter.hitHttpRoute(request);
        Iterator<HttpFilter> httpFilterIterator = httpRoute.getHttpFilterList();
        if (httpFilterIterator == null)
        {
            httpRoute.getHttpService().doService(request, response);
        }
        else
        {
            new HttpFilterChain(httpFilterIterator, httpRoute.getHttpService()).doNextFilter(request, response);
        }
    }

    @Override
    protected void doException(Throwable throwable)
    {
        if (throwable instanceof InvocationTargetException)
        {
            doException(((InvocationTargetException) throwable).getTargetException());
        }
        else if (throwable instanceof UndeclaredThrowableException)
        {
            doException(((UndeclaredThrowableException) throwable).getUndeclaredThrowable());
        }
        else if (throwable instanceof Exception)
        {
            doException((Exception) throwable);
        }
        else
        {
            LOGGER.error(throwable.toString(), throwable);
        }
    }

    @Override
    public void doFinally()
    {
        HttpServiceResponse response = this.httpContext.getResponse();
        if (response.isAutoCommit()) response.commit();
    }

    @Override
    public void doReject()
    {
        doException(new HttpResponseStatusException(HttpResponseStatus.FORBIDDEN.code(), "Reject request"));
    }

    private void doException(Exception e)
    {
        LOGGER.error(e.toString(), e);
        if (e instanceof HttpResponseStatusException)
        {
            this.httpContext.getResponse().setStatus(((HttpResponseStatusException) e).getStatusCode());
        }
        else
        {
            this.httpContext.getResponse().setStatus(HttpResponseStatus.INTERNAL_SERVER_ERROR.code());
        }
    }
}
