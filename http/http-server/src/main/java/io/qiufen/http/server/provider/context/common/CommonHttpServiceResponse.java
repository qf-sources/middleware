package io.qiufen.http.server.provider.context.common;

import io.netty.handler.codec.http.*;
import io.qiufen.common.collection.MapUtil;
import io.qiufen.common.lang.Strings;
import io.qiufen.http.server.provider.net.DefaultChannelContext;
import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.http.server.spi.http.WritableChannel;
import io.qiufen.http.server.spi.net.ChannelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class CommonHttpServiceResponse implements HttpServiceResponse
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonHttpServiceResponse.class);

    private final DefaultChannelContext context;
    private final HttpContext httpContext;
    private final WritableChannel writableChannel;

    private boolean writeHeader = false;

    private boolean autoCommit = true;

    private Map<String, String> headerMap;

    private int statusCode = HttpResponseStatus.OK.code();

    public CommonHttpServiceResponse(ChannelContext context, HttpContext httpContext, WritableChannel writableChannel)
    {
        this.context = (DefaultChannelContext) context;
        this.httpContext = httpContext;
        this.writableChannel = writableChannel;

        initHeaders();
    }

    @Override
    public void setStatus(int code)
    {
        checkWriteHeader();
        this.statusCode = code;
    }

    @Override
    public void addHeader(String name, String value)
    {
        checkWriteHeader();
        this.headerMap.put(name, value);
    }

    @Override
    public void setContentType(String contentType)
    {
        addHeader(HttpHeaderNames.CONTENT_TYPE.toString(), contentType);
    }

    @Override
    public void setContentLength(long contentLength)
    {
        //以content-length响应，移出默认chunk方式
        addHeader(HttpHeaderNames.CONTENT_LENGTH.toString(), Long.toString(contentLength));
        this.headerMap.remove("transfer-encoding");
    }

    @Override
    public WritableChannel getChannel()
    {
        writeHeader();
        return this.writableChannel;
    }

    public void commit()
    {
        try
        {
            finish();
        }
        finally
        {
            this.httpContext.close();
        }
    }

    public void finish()
    {
        try
        {
            writeHeader();
            this.context.getContext().writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
        }
        finally
        {
            this.context.finishWrite();
        }
    }

    @Override
    public boolean isAutoCommit()
    {
        return autoCommit;
    }

    @Override
    public void setAutoCommit(boolean autoCommit)
    {
        this.autoCommit = autoCommit;
    }

    private void initHeaders()
    {
        this.headerMap = new HashMap<String, String>();
        //增加默认头
        this.headerMap.put("transfer-encoding", "chunked");
    }

    private void writeHeader()
    {
        if (this.writeHeader)
        {
            return;
        }
        this.writeHeader = true;
        DefaultHttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1,
                HttpResponseStatus.valueOf(this.statusCode));

        HttpHeaders headers = response.headers();
        //写入自定义头
        if (!MapUtil.isEmpty(this.headerMap))
        {
            for (Map.Entry<String, String> entry : headerMap.entrySet())
            {
                String value = entry.getValue();
                if (Strings.isEmpty(value))
                {
                    continue;
                }
                String name = entry.getKey();
                LOGGER.debug("Add header:{},{}", name, value);
                headers.set(name, value);
            }
        }

        this.context.startWrite();
        this.context.getContext().write(response);
    }

    private void checkWriteHeader()
    {
        if (this.writeHeader)
        {
            LOGGER.warn("Header has been written!");
        }
    }
}
