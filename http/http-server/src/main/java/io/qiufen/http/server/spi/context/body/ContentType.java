package io.qiufen.http.server.spi.context.body;

import io.netty.handler.codec.http.HttpHeaderValues;
import io.qiufen.common.lang.CTString;

public class ContentType
{
    @Deprecated
    public static final ContentType ANY = new ContentType(CTString.wrap("*"));

    public static final ContentType DEFAULT = new ContentType(CTString.wrap("*"));

    public static final ContentType APPLICATION_JSON = new ContentType(
            CTString.wrap(HttpHeaderValues.APPLICATION_JSON));
    public static final ContentType TEXT_PLAIN = new ContentType(CTString.wrap(HttpHeaderValues.TEXT_PLAIN));
    public static final ContentType FORM_DATA = new ContentType(CTString.wrap(HttpHeaderValues.FORM_DATA));
    public static final ContentType MULTIPART_FORM_DATA = new ContentType(
            CTString.wrap(HttpHeaderValues.MULTIPART_FORM_DATA));
    public static final ContentType APPLICATION_X_WWW_FORM_URLENCODED = new ContentType(
            CTString.wrap(HttpHeaderValues.APPLICATION_X_WWW_FORM_URLENCODED));
    public static final ContentType APPLICATION_OCTET_STREAM = new ContentType(
            CTString.wrap(HttpHeaderValues.APPLICATION_OCTET_STREAM));

    private final CTString value;

    public ContentType(CTString value) {this.value = value;}

    public CTString getValue()
    {
        return value;
    }
}
