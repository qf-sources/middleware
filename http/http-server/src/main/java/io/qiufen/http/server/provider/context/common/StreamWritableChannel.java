package io.qiufen.http.server.provider.context.common;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.qiufen.common.net.SendResult;
import io.qiufen.http.server.provider.constant.CommonConstant;
import io.qiufen.http.server.spi.http.WritableChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;

public class StreamWritableChannel extends StreamChannel implements WritableChannel
{
    private static final Logger LOGGER = LoggerFactory.getLogger(StreamWritableChannel.class);

    private final Channel channel;
    private final ByteBufAllocator allocator;

    public StreamWritableChannel(ChannelHandlerContext context)
    {
        this.channel = context.channel();
        this.allocator = context.alloc();
    }

    @Override
    public int write(ByteBuffer src) throws IOException
    {
        ByteBuf buf = allocator.buffer().writeBytes(src);
        int n = buf.readableBytes();
        write0(buf);
        return n;
    }

    @Override
    public void writeFinal(String src) throws IOException
    {
        writeFinal(src.getBytes());
    }

    @Override
    public void writeFinal(byte[] src) throws IOException
    {
        write0(allocator.buffer(src.length).writeBytes(src));
    }

    @Override
    public void writeFinal(byte[] src, int offset, int length) throws IOException
    {
        write0(allocator.buffer(src.length).writeBytes(src, 0, length));
    }

    @Override
    public void writeFinal(ByteBuffer src) throws IOException
    {
        write0(Unpooled.wrappedBuffer(src));
    }

    @Override
    public void writeFinal(ByteBuffer src, int offset, int length) throws IOException
    {
        write0(Unpooled.wrappedBuffer(src).slice(offset, length));
    }

    @Override
    public void writeFinal(Object src) throws ClosedChannelException
    {
        if (src instanceof ByteBuf)
        {
            write0((ByteBuf) src);
            return;
        }
        throw new IllegalArgumentException("unsupported type:" + src.getClass().getName());
    }

    private void write0(ByteBuf buf) throws ClosedChannelException
    {
        try
        {
            checkState();
            SendResult result = CommonConstant.CHANNEL_MONITOR.send(channel, buf.retain());
            if (result == SendResult.SUCCESS)
            {
                LOGGER.debug("Send success:{}", channel);
            }
            else if (result == SendResult.CH_INACTIVE)
            {
                throw new ClosedChannelException();
            }
            else
            {
                throw new IllegalStateException("write failure,channel state:" + result + ",channel:" + channel);
            }
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            buf.release();
        }
    }
}
