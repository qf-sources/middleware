package io.qiufen.http.server.spi.http;

/**
 * http过滤器
 * todo 顺序问题
 */
public interface HttpFilter
{
    /**
     * 处理过滤
     *
     * @param chain    过滤链
     * @param request  request
     * @param response response
     * @throws Exception e
     */
    void doFilter(HttpFilterChain chain, HttpServiceRequest request, HttpServiceResponse response) throws Throwable;
}
