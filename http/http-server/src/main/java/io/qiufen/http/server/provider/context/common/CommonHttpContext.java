package io.qiufen.http.server.provider.context.common;

import io.qiufen.common.io.IOUtil;
import io.qiufen.http.server.spi.context.HttpContextAdapter;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.http.server.spi.net.ChannelContext;

public class CommonHttpContext extends HttpContextAdapter
{
    private HttpServiceRequest request;
    private HttpServiceResponse response;

    private StreamWritableChannel writableChannel;

    protected CommonHttpContext()
    {
    }

    @Override
    public HttpServiceRequest getRequest()
    {
        return request;
    }

    @Override
    public HttpServiceResponse getResponse()
    {
        return response;
    }

    @Override
    public void doStart(ChannelContext context, io.netty.handler.codec.http.HttpRequest msg)
    {
        request = new CommonHttpServiceRequest(context, msg);
        writableChannel = new StreamWritableChannel(context.getContext());
        response = new CommonHttpServiceResponse(context, this, writableChannel);
    }

    @Override
    public void close()
    {
        IOUtil.close(writableChannel);
    }
}
