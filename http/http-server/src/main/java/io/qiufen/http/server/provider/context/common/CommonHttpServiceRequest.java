package io.qiufen.http.server.provider.context.common;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.handler.codec.http.cookie.Cookie;
import io.netty.handler.codec.http.cookie.ServerCookieDecoder;
import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.lang.StringUtil;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.ReadableChannel;
import io.qiufen.http.server.spi.net.ChannelContext;

import java.net.InetSocketAddress;
import java.util.*;

public class CommonHttpServiceRequest implements HttpServiceRequest
{
    private final ChannelContext ctx;
    private final io.netty.handler.codec.http.HttpRequest request;

    private final String path;
    private final Map<String, List<String>> params;

    private Map<String, String> headers;
    private Set<Cookie> cookies;

    protected CommonHttpServiceRequest(ChannelContext ctx, io.netty.handler.codec.http.HttpRequest msg)
    {
        this.ctx = ctx;
        request = msg;

        QueryStringDecoder decoder = new QueryStringDecoder(request.uri());//todo charset
        this.path = decoder.path();
        this.params = new HashMap<String, List<String>>();
        this.params.putAll(decoder.parameters());
    }

    @Override
    public String getRequestURI()
    {
        return request.uri();
    }

    @Override
    public String path()
    {
        return this.path;
    }

    @Override
    public String getMethod()
    {
        return request.method().name();
    }

    @Override
    public String getContentType()
    {
        return getHeader(HttpHeaderNames.CONTENT_TYPE.toString());
    }

    @Override
    public String getHeader(String name)
    {
        name = name.toLowerCase();
        if (this.headers != null)
        {
            return this.headers.get(name);
        }
        return request.headers().get(name);
    }

    @Override
    public Map<String, String> headers()
    {
        if (this.headers != null)
        {
            return this.headers;
        }
        this.headers = new HashMap<String, String>();
        List<Map.Entry<String, String>> list = request.headers().entries();
        if (CollectionUtil.isNotEmpty(list))
        {
            for (Map.Entry<String, String> entry : list)
            {
                this.headers.put(entry.getKey().toLowerCase(), entry.getValue());
            }
        }
        return this.headers;
    }

    @Override
    public Map<String, List<String>> getParameterMap()
    {
        return this.params;
    }

    @Override
    public List<String> getParameterValues(String name)
    {
        return this.getParameterMap().get(name);
    }

    @Override
    public String getParameter(String name)
    {
        List<String> values = getParameterValues(name);
        return CollectionUtil.isEmpty(values) ? null : values.size() == 1 ? values.get(0) : values.toString();
    }

    @Override
    public String getRemoteAddr()
    {
        String ip = getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
        {
            ip = getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
        {
            ip = getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
        {
            ip = ((InetSocketAddress) ctx.getContext().channel().remoteAddress()).getHostName();
        }
        return ip;
    }

    @Override
    public Cookie cookie(String name)
    {
        Set<Cookie> cookies = getCookies();
        if (CollectionUtil.isEmpty(cookies))
        {
            return null;
        }
        for (Cookie cookie : cookies)
        {
            if (name.equals(cookie.name()))
            {
                return cookie;
            }
        }
        return null;
    }

    @Override
    public Set<Cookie> getCookies()
    {
        if (this.cookies != null)
        {
            return this.cookies;
        }

        this.cookies = new HashSet<Cookie>();
        String cookieStr = request.headers().get(HttpHeaderNames.COOKIE);
        if (StringUtil.isEmpty(cookieStr))
        {
            return this.cookies;
        }
        Set<Cookie> cookies = ServerCookieDecoder.STRICT.decode(cookieStr);
        this.cookies.addAll(cookies);
        return this.cookies;
    }

    @Override
    public ReadableChannel getChannel()
    {
        throw new UnsupportedOperationException();
    }
}
