package io.qiufen.http.server.provider.context.body.stream;

import io.netty.handler.codec.http.HttpRequest;
import io.qiufen.http.server.provider.context.common.CommonHttpServiceRequest;
import io.qiufen.http.server.spi.http.ReadableChannel;
import io.qiufen.http.server.spi.net.ChannelContext;

public class StreamBodyHttpServiceRequest extends CommonHttpServiceRequest
{
    private final ReadableChannel channel;

    StreamBodyHttpServiceRequest(ChannelContext ctx, HttpRequest msg, ReadableChannel channel)
    {
        super(ctx, msg);
        this.channel = channel;
    }

    @Override
    public ReadableChannel getChannel()
    {
        return channel;
    }
}
