package io.qiufen.http.server.provider.exception;

public class HttpResponseStatusException extends HttpResponseException
{
    private final int statusCode;

    public HttpResponseStatusException(int statusCode, String message)
    {
        super(message + ",statusCode:" + statusCode);
        this.statusCode = statusCode;
    }

    public int getStatusCode()
    {
        return statusCode;
    }
}
