package io.qiufen.http.server.provider.context;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.qiufen.http.server.spi.context.HttpContext;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;

public abstract class AbstractHttpContext implements HttpContext
{
    private final AtomicBoolean busy = new AtomicBoolean(false);

    private volatile boolean closed = false;

    @Override
    public final void doContent(ChannelHandlerContext ctx, HttpContent msg)
    {
        if (this.closed || !this.busy.compareAndSet(false, true))
        {
            return;
        }
        try
        {
            this.doContent0(ctx, msg);
        }
        finally
        {
            this.busy.set(false);
        }
    }

    @Override
    public final void doEnd(ChannelHandlerContext ctx)
    {
        if (this.closed || !this.busy.compareAndSet(false, true))
        {
            return;
        }
        try
        {
            this.doEnd0(ctx);
        }
        finally
        {
            this.busy.set(false);
        }
    }

    @Override
    public final void close()
    {
        if (this.closed) return;
        while (!this.busy.compareAndSet(false, true))
        {
            LockSupport.parkNanos(1);
        }
        try
        {
            this.closed = true;
            this.close0();
        }
        finally
        {
            this.busy.set(false);
        }
    }

    protected void doContent0(ChannelHandlerContext ctx, HttpContent msg)
    {
    }

    protected void doEnd0(ChannelHandlerContext ctx)
    {
    }

    protected void close0()
    {
    }
}
