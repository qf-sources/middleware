package io.qiufen.http.server.provider.context.body.raw;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.cookie.Cookie;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.ReadableChannel;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RawHttpServiceRequest implements HttpServiceRequest
{
    private final HttpServiceRequest request;

    private byte[] content;

    RawHttpServiceRequest(HttpServiceRequest request)
    {
        this.request = request;
    }

    public byte[] getBodyBytes() throws IOException
    {
        if (this.content != null)
        {
            return this.content;
        }

        this.content = new byte[0];
        ReadableChannel channel = getChannel();
        ByteBuf buf;
        while (!channel.isEOF(buf = channel.read(ByteBuf.class)))
        {
            byte[] newArray = new byte[this.content.length + buf.readableBytes()];
            ByteBuf byteBuf = Unpooled.wrappedBuffer(newArray).writerIndex(0);
            byteBuf.writeBytes(this.content);
            byteBuf.writeBytes(buf);
            byteBuf.release();
            buf.release();
            this.content = newArray;
        }
        return this.content;
    }

    public String getBody() throws IOException
    {
        return new String(getBodyBytes(), Charset.forName("utf-8"));
    }

    @Override
    public String getRequestURI()
    {
        return request.getRequestURI();
    }

    @Override
    public String path()
    {
        return request.path();
    }

    @Override
    public String getMethod()
    {
        return request.getMethod();
    }

    @Override
    public String getContentType()
    {
        return request.getContentType();
    }

    @Override
    public String getHeader(String name)
    {
        return request.getHeader(name);
    }

    @Override
    public Map<String, String> headers()
    {
        return request.headers();
    }

    @Override
    public Map<String, List<String>> getParameterMap()
    {
        return request.getParameterMap();
    }

    @Override
    public List<String> getParameterValues(String name)
    {
        return request.getParameterValues(name);
    }

    @Override
    public String getParameter(String name)
    {
        return request.getParameter(name);
    }

    @Override
    public String getRemoteAddr()
    {
        return request.getRemoteAddr();
    }

    @Override
    public Cookie cookie(String name)
    {
        return request.cookie(name);
    }

    @Override
    public Set<Cookie> getCookies()
    {
        return request.getCookies();
    }

    @Override
    public ReadableChannel getChannel()
    {
        return request.getChannel();
    }
}
