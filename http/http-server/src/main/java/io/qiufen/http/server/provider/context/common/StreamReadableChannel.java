package io.qiufen.http.server.provider.context.common;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.CompositeByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelConfig;
import io.netty.channel.ChannelHandlerContext;
import io.qiufen.http.server.spi.http.ReadableChannel;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;
//todo 性能优化？
public class StreamReadableChannel extends StreamChannel implements ReadableChannel
{
    private static final int EOF_LEN = -1;
    private static final Object EOF = null;

    private static final Map<Class, BufferExchange> EXCHANGE_MAP;

    static
    {
        EXCHANGE_MAP = new IdentityHashMap<Class, BufferExchange>();
        EXCHANGE_MAP.put(ByteBuf.class, new BufferExchange1());
    }

    private final Queue<ByteBuf> queue = new ConcurrentLinkedQueue<ByteBuf>();
    private final AtomicInteger queueDepth = new AtomicInteger(0);

    private final AtomicBoolean op_lock_offer = new AtomicBoolean(false);
    private final AtomicBoolean op_lock_read_0 = new AtomicBoolean(false);
    private final AtomicBoolean op_lock_read_1 = new AtomicBoolean(false);

    private final Semaphore semaphore = new Semaphore(0);

    private final ChannelConfig channelConfig;

    private volatile boolean finished = false;

    private volatile ByteBuf halfBuf;

    public StreamReadableChannel(ChannelHandlerContext context)
    {
        this.channelConfig = context.channel().config();
    }

    public void offer(ByteBuf buf)
    {
        try
        {
            //已关闭
            if (this.isClosed()) return;

            //未拿到锁，说明锁已被关闭占用，直接退出即可
            if (!this.op_lock_offer.compareAndSet(false, true)) return;

            try
            {
                this.queue.offer(buf.retain());
                setAutoRead(this.queueDepth.incrementAndGet());
                semaphore.release(1);
            }
            finally
            {
                this.op_lock_offer.set(false);
            }
        }
        finally
        {
            buf.release();
        }
    }

    public void finish()
    {
        this.finished = true;
        this.semaphore.release(1);
    }

    @Override
    public boolean isEOF(int readResult)
    {
        return readResult == EOF_LEN;
    }

    @Override
    public boolean isEOF(Object readResult)
    {
        return readResult == EOF;
    }

    @Override
    public int read(ByteBuffer dst) throws IOException
    {
        //目标缓冲区已填满
        int writableLength = dst.remaining();
        if (writableLength <= 0) return EOF_LEN;
        final ByteBuf destination = Unpooled.wrappedBuffer(dst);
        BufferReader bufferReader = new BufferReader()
        {
            @Override
            public void read(ByteBuf src, int length)
            {
                src.readBytes(destination, length);
            }
        };
        try
        {
            return read(writableLength, bufferReader);
        }
        finally
        {
            destination.release();
        }
    }

    @Override
    public <T> T read(Class<T> expectType) throws IOException
    {
        //未拿到锁，说明锁已被关闭占用，直接退出即可
        if (!this.op_lock_read_1.compareAndSet(false, true)) return expectType.cast(EOF);

        try
        {
            for (; ; )
            {
                checkState();

                ByteBuf buf = queue.poll();
                //未取到数据
                if (buf == null)
                {
                    //已结束
                    if (this.finished) return expectType.cast(EOF);

                    await();
                    continue;
                }
                setAutoRead(this.queueDepth.decrementAndGet());

                return toExpectType(buf, expectType);
            }
        }
        finally
        {
            this.op_lock_read_1.set(false);
        }
    }

    @Override
    public <T> T read(int bufferSize, Class<T> expectType) throws IOException
    {
        final List<ByteBuf> destinationList = new ArrayList<ByteBuf>();
        BufferReader bufferReader = new BufferReader()
        {
            @Override
            public void read(ByteBuf src, int length)
            {
                destinationList.add(src.readRetainedSlice(length));
            }
        };
        int result = read(bufferSize, bufferReader);
        if (isEOF(result))
        {
            return expectType.cast(EOF);
        }
        CompositeByteBuf buf = channelConfig.getAllocator().compositeBuffer(destinationList.size());
        for (ByteBuf destination : destinationList)
        {
            buf.addComponent(true, destination);
        }
        return toExpectType(buf, expectType);
    }

    @Override
    protected void close0()
    {
        //先标记offer操作锁为关闭
        while (!this.op_lock_offer.compareAndSet(false, true))
        {
            LockSupport.parkNanos(10);
        }
        //再标记read操作锁为关闭
        while (!this.op_lock_read_0.compareAndSet(false, true))
        {
            LockSupport.parkNanos(10);
            //唤醒可能同步等待read的线程
            finish();
        }
        while (!this.op_lock_read_1.compareAndSet(false, true))
        {
            LockSupport.parkNanos(10);
            //唤醒可能同步等待read的线程
            finish();
        }

        //清空半包
        if (this.halfBuf != null)
        {
            this.halfBuf.release();
        }
        //清空队列
        for (; ; )
        {
            ByteBuf buf = this.queue.poll();
            if (buf == null)
            {
                break;
            }
            buf.release();
        }
        this.queueDepth.set(0);
        setAutoRead(0);

        //唤醒可能读休眠读线程
        this.semaphore.release(1);
    }

    private int read(int writableLength, BufferReader bufferReader) throws IOException
    {
        int originLength = writableLength;

        //未拿到锁，说明锁已被关闭占用，直接退出即可
        if (!this.op_lock_read_0.compareAndSet(false, true)) return EOF_LEN;

        try
        {
            //处理半包
            if (this.halfBuf != null)
            {
                checkState();

                int readableLength = this.halfBuf.readableBytes();
                if (readableLength >= writableLength)
                {
                    bufferReader.read(this.halfBuf, writableLength);
                    //正好读完
                    if (readableLength == writableLength) this.halfBuf.release();
                    //返回，下次继续读半包
                    //可用空间均被使用，因此返回原始长度
                    return originLength;
                }

                bufferReader.read(this.halfBuf, readableLength);
                writableLength -= readableLength;
                this.halfBuf.release();
            }

            //处理队列
            return readFromQueue(originLength, writableLength, bufferReader);
        }
        finally
        {
            this.op_lock_read_0.set(false);
        }
    }

    private int readFromQueue(int originLength, int writableLength, BufferReader bufferReader)
            throws ClosedChannelException
    {
        for (; ; )
        {
            checkState();

            ByteBuf buf = queue.poll();
            //未取到数据
            if (buf == null)
            {
                //已结束
                if (this.finished) return originLength - writableLength;

                await();
                continue;
            }
            setAutoRead(this.queueDepth.decrementAndGet());

            int readableLength = buf.readableBytes();
            if (readableLength >= writableLength)
            {
                bufferReader.read(buf, writableLength);
                if (readableLength == writableLength)
                {
                    //正好读完
                    buf.release();
                }
                else
                {
                    //存在部分未读，放入半包
                    this.halfBuf = buf;
                }
                //可用空间均被使用，因此返回原始长度
                return originLength;
            }

            //读完数据
            bufferReader.read(buf, readableLength);
            buf.release();
            writableLength -= readableLength;
        }
    }

    private void setAutoRead(int queueDepth)
    {
        if (queueDepth >= 100)
        {
            channelConfig.setAutoRead(false);
        }
        else if (queueDepth <= 10)
        {
            channelConfig.setAutoRead(true);
        }
    }

    private void await()
    {
        try
        {
            this.semaphore.acquire(1);
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
    }

    private <T> T toExpectType(ByteBuf buf, Class<T> clazz)
    {
        try
        {
            BufferExchange<T> exchange = EXCHANGE_MAP.get(clazz);
            if (exchange == null)
            {
                throw new RuntimeException("Not support buffer exchange for expect type:" + clazz.getName());
            }
            return exchange.toBuffer(buf.retain());
        }
        finally
        {
            buf.release();
        }
    }

    interface BufferExchange<T>
    {
        T toBuffer(ByteBuf origin);
    }

    interface BufferReader
    {
        void read(ByteBuf src, int length);
    }

    private static class BufferExchange1 implements BufferExchange<ByteBuf>
    {
        @Override
        public ByteBuf toBuffer(ByteBuf origin)
        {
            return origin;
        }
    }
}