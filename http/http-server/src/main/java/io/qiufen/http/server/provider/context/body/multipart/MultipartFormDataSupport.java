package io.qiufen.http.server.provider.context.body.multipart;

import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.context.body.ContentTypeSupport;

public final class MultipartFormDataSupport implements ContentTypeSupport
{
    private static final MultipartFormDataSupport instance = new MultipartFormDataSupport();

    @Deprecated
    public MultipartFormDataSupport() { }

    public static MultipartFormDataSupport of()
    {
        return instance;
    }

    @Override
    public ContentType[] support()
    {
        return new ContentType[]{ContentType.MULTIPART_FORM_DATA};
    }

    @Override
    public HttpContext create()
    {
        return new MultipartFormDataHttpContext();
    }
}
