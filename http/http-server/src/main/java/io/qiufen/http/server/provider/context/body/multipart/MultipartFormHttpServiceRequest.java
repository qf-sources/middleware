package io.qiufen.http.server.provider.context.body.multipart;

import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.FileUpload;
import io.netty.handler.codec.http.multipart.HttpPostMultipartRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.qiufen.http.server.provider.context.body.BodyHttpServiceRequest;
import io.qiufen.http.server.spi.net.ChannelContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultipartFormHttpServiceRequest extends BodyHttpServiceRequest
{
    private final HttpPostMultipartRequestDecoder decoder;

    private Map<String, List<String>> params;

    private Map<String, List<FileUpload>> files;

    private boolean init = false;

    MultipartFormHttpServiceRequest(ChannelContext ctx, HttpPostMultipartRequestDecoder decoder, HttpRequest msg)
    {
        super(ctx, msg);
        this.decoder = decoder;
    }

    @Override
    public Map<String, List<String>> getParameterMap()
    {
        try
        {
            initDecode();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        this.params.putAll(super.getParameterMap());
        return this.params;
    }

    public boolean isMultipart()
    {
        return this.decoder.isMultipart();
    }

    public List<FileUpload> files(String name)
    {
        return files().get(name);
    }

    public Map<String, List<FileUpload>> files()
    {
        try
        {
            initDecode();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        return this.files;
    }

    private void initDecode() throws IOException
    {
        if (this.init)
        {
            return;
        }

        //初始化变量：请求参数 & 文件参数
        if (this.params == null)
        {
            this.params = new HashMap<String, List<String>>();
        }
        boolean multipart = isMultipart();
        if (multipart)
        {
            if (this.files == null)
            {
                this.files = new HashMap<String, List<FileUpload>>();
            }
        }

        //检查是否准备好，并进行解析数据
        checkReady();
        for (InterfaceHttpData data : decoder.getBodyHttpDatas())
        {
            if (data.getHttpDataType() == InterfaceHttpData.HttpDataType.Attribute)
            {
                Attribute attribute = (Attribute) data;
                List<String> valueList = this.params.get(attribute.getName());
                if (null == valueList)
                {
                    valueList = new ArrayList<String>(1);
                    this.params.put(attribute.getName(), valueList);
                }
                valueList.add(attribute.getValue());
                continue;
            }

            if (multipart && data.getHttpDataType() == InterfaceHttpData.HttpDataType.FileUpload)
            {
                FileUpload upload = (FileUpload) data;
                List<FileUpload> uploads = this.files.get(upload.getName());
                if (uploads == null)
                {
                    uploads = new ArrayList<FileUpload>(1);
                    this.files.put(upload.getName(), uploads);
                }
                uploads.add(upload);
            }
        }
        this.init = true;
    }
}
