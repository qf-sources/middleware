package io.qiufen.http.server.provider.context.body.stream;

import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.context.body.ContentTypeSupport;

public class StreamSupport implements ContentTypeSupport
{
    private final ContentType[] types;

    @Deprecated
    public StreamSupport()
    {
        this(defaultTypes());
    }

    private StreamSupport(ContentType[] types)
    {
        this.types = types;
    }

    public static StreamSupport of(ContentType... types)
    {
        if (types == null || types.length == 0) types = defaultTypes();
        return new StreamSupport(types);
    }

    private static ContentType[] defaultTypes()
    {
        return new ContentType[]{ContentType.APPLICATION_OCTET_STREAM};
    }

    @Override
    public ContentType[] support()
    {
        return types;
    }

    @Override
    public HttpContext create()
    {
        return new StreamBodyHttpContext();
    }
}
