package io.qiufen.http.server.spi.context.body;

import io.qiufen.http.server.spi.context.HttpContext;

/**
 * 受支持的内容类型
 */
public interface ContentTypeSupport
{
    /**
     * 受支持的内容类型
     *
     * @return 列表
     * @see io.netty.handler.codec.http.HttpHeaderValues
     */
    ContentType[] support();

    /**
     * 创建http context
     *
     * @return http context
     */
    HttpContext create();
}
