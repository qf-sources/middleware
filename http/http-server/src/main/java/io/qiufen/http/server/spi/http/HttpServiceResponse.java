package io.qiufen.http.server.spi.http;

public interface HttpServiceResponse
{
    void setStatus(int status);

    void addHeader(String name, String value);

    void setContentType(String contentType);

    void setContentLength(long contentLength);

    WritableChannel getChannel();

    void commit();

    boolean isAutoCommit();

    void setAutoCommit(boolean autoCommit);
}
