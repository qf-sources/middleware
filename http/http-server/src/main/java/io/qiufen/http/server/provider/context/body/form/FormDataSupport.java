package io.qiufen.http.server.provider.context.body.form;

import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.context.body.ContentTypeSupport;

public final class FormDataSupport implements ContentTypeSupport
{
    private static final FormDataSupport instance = new FormDataSupport();

    @Deprecated
    public FormDataSupport() { }

    public static FormDataSupport of()
    {
        return instance;
    }

    @Override
    public ContentType[] support()
    {
        return new ContentType[]{ContentType.FORM_DATA};
    }

    @Override
    public HttpContext create()
    {
        return new FormDataHttpContext();
    }
}
