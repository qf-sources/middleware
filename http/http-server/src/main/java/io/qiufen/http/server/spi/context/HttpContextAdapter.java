package io.qiufen.http.server.spi.context;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpRequest;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.http.server.spi.net.ChannelContext;

public class HttpContextAdapter implements HttpContext
{
    @Override
    public HttpServiceRequest getRequest()
    {
        return null;
    }

    @Override
    public HttpServiceResponse getResponse()
    {
        return null;
    }

    @Override
    public void doStart(ChannelContext context, HttpRequest msg)
    {
    }

    @Override
    public void doContent(ChannelHandlerContext ctx, HttpContent msg)
    {
        msg.release();
    }

    @Override
    public void doEnd(ChannelHandlerContext ctx)
    {
    }

    @Override
    public void close()
    {
    }
}
