package io.qiufen.http.server.provider.context.body.form;

import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.context.body.ContentTypeSupport;

public final class FormURLEncodedSupport implements ContentTypeSupport
{
    private static final FormURLEncodedSupport instance = new FormURLEncodedSupport();

    @Deprecated
    public FormURLEncodedSupport() { }

    public static FormURLEncodedSupport of()
    {
        return instance;
    }

    @Override
    public ContentType[] support()
    {
        return new ContentType[]{ContentType.APPLICATION_X_WWW_FORM_URLENCODED};
    }

    @Override
    public HttpContext create()
    {
        return new FormURLEncodedHttpContext();
    }
}
