package io.qiufen.http.server.provider.net;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.util.ReferenceCountUtil;
import io.qiufen.http.server.provider.context.HttpContextSupportRegistry;
import io.qiufen.http.server.provider.exception.HttpResponseStatusException;
import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.dispatcher.HttpContextDispatcher;
import io.qiufen.http.server.spi.net.ChannelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * http通道事件处理器
 */
public class HttpChannelEventHandler extends ChannelInboundHandlerAdapter
{
    private static final Logger log = LoggerFactory.getLogger(HttpChannelEventHandler.class);
    private final HttpContextSupportRegistry supportRegistry;
    private final HttpContextDispatcher dispatcher;
    private final ChannelContextBuilder contextBuilder;
    private HttpContext httpContext;
    private ChannelContext context;

    public HttpChannelEventHandler(HttpContextSupportRegistry supportRegistry, HttpContextDispatcher dispatcher,
            ChannelContextBuilder contextBuilder)
    {
        this.supportRegistry = supportRegistry;
        this.dispatcher = dispatcher;
        this.contextBuilder = contextBuilder;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx)
    {
        this.context = contextBuilder.create(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
    {
        if (msg instanceof HttpRequest)
        {
            try
            {
                HttpRequest request = (HttpRequest) msg;
                httpContext = this.supportRegistry.hitHttpContextSupport(request.method()).create();
                httpContext.doStart(context, request);

                dispatcher.dispatch(httpContext);
            }
            finally
            {
                ReferenceCountUtil.release(msg);
            }
        }
        else if (msg instanceof HttpContent)
        {
            if (httpContext == null)
            {
                ReferenceCountUtil.release(msg);
            }
            else
            {
                try
                {
                    httpContext.doContent(ctx, (HttpContent) msg);
                }
                finally
                {
                    ReferenceCountUtil.release(msg);
                }

                if (msg instanceof LastHttpContent)
                {
                    httpContext.doEnd(ctx);
                    httpContext = null;
                }
            }
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx)
    {
        try
        {
            if (null != httpContext)
            {
                httpContext.close();
                httpContext = null;
            }
        }
        finally
        {
            context.close();
            this.context = null;
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    {
        try
        {
            log.error(cause.toString(), cause);
            if (null != httpContext)
            {
                httpContext.close();
                httpContext = null;
            }
        }
        finally
        {
            if (cause instanceof HttpResponseStatusException)
            {
                HttpResponseStatusException e = (HttpResponseStatusException) cause;
                HttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1,
                        HttpResponseStatus.valueOf(e.getStatusCode()));
                response.headers().set(HttpHeaderNames.CONTENT_LENGTH, 0);
                ctx.write(response);
                ctx.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
            }
        }
    }
}
