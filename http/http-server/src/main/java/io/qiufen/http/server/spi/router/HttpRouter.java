package io.qiufen.http.server.spi.router;

import io.qiufen.http.server.spi.http.HttpFilter;
import io.qiufen.http.server.spi.http.HttpService;
import io.qiufen.http.server.spi.http.HttpServiceRequest;

/**
 * http 路由
 */
public interface HttpRouter<U>
{
    /**
     * 添加http服务
     *
     * @param uri         uri
     * @param httpService httpService
     */
    void addHttpService(U uri, HttpService httpService);

    /**
     * 添加http过滤器
     *
     * @param uri        uri
     * @param httpFilter httpFilter
     */
    void addHttpFilter(U uri, HttpFilter httpFilter);

    /**
     * 命中http 路由
     *
     * @param request 请求
     * @return http服务
     */
    HttpRoute hitHttpRoute(HttpServiceRequest request);

    /**
     * 移除http服务
     */
    void removeHttpService(U uri);
}
