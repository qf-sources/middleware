package io.qiufen.http.server.provider.command;

import io.qiufen.http.server.spi.command.HttpCommand;
import io.qiufen.http.server.spi.command.HttpCommandBuilder;
import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.router.HttpRouter;

public class DefaultHttpCommandBuilder implements HttpCommandBuilder
{
    private final HttpRouter<?> httpRouter;

    public DefaultHttpCommandBuilder(HttpRouter<?> httpRouter)
    {
        this.httpRouter = httpRouter;
    }

    @Override
    public HttpCommand build(HttpContext httpContext)
    {
        return new DefaultHttpCommand(httpRouter, httpContext);
    }
}
