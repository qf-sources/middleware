package io.qiufen.http.server.spi.context;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpContent;
import io.qiufen.http.server.spi.http.HttpServiceRequest;
import io.qiufen.http.server.spi.http.HttpServiceResponse;
import io.qiufen.http.server.spi.net.ChannelContext;

public interface HttpContext
{
    HttpServiceRequest getRequest();

    HttpServiceResponse getResponse();

    void doStart(ChannelContext context, io.netty.handler.codec.http.HttpRequest msg);

    void doContent(ChannelHandlerContext ctx, HttpContent msg);

    void doEnd(ChannelHandlerContext ctx);

    void close();
}
