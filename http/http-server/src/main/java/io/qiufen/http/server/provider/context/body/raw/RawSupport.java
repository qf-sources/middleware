package io.qiufen.http.server.provider.context.body.raw;

import io.qiufen.http.server.spi.context.HttpContext;
import io.qiufen.http.server.spi.context.body.ContentType;
import io.qiufen.http.server.spi.context.body.ContentTypeSupport;

public class RawSupport implements ContentTypeSupport
{
    private final ContentType[] types;

    @Deprecated
    public RawSupport()
    {
        this(defaultTypes());
    }

    private RawSupport(ContentType[] types)
    {
        this.types = types;
    }

    public static RawSupport of(ContentType... types)
    {
        if (types == null || types.length == 0) types = defaultTypes();
        return new RawSupport(types);
    }

    private static ContentType[] defaultTypes()
    {
        return new ContentType[]{ContentType.APPLICATION_JSON, ContentType.TEXT_PLAIN};
    }

    @Override
    public ContentType[] support()
    {
        return types;
    }

    @Override
    public HttpContext create()
    {
        return new RawHttpContext();
    }
}
