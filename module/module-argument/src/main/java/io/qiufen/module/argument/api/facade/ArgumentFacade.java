package io.qiufen.module.argument.api.facade;

import io.qiufen.module.kernel.spi.facade.Facade;

/**
 * 实参门面
 *
 * @version 1.0
 */
public interface ArgumentFacade extends Facade
{
    Object hitArgument(String scope, Object[] keys);

    Object hitArgument(Object[] keys);

    //place holder属于特殊能力，由place holder value实现
    @Deprecated
    String of(String placeHolder);

    boolean hasArgument(String scope, Object[] keys);

    boolean hasArgument(Object[] keys);
}
