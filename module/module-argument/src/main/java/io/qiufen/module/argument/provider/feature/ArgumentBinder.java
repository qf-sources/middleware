package io.qiufen.module.argument.provider.feature;

import io.qiufen.module.argument.spi.resolver.ArgumentResolver;
import io.qiufen.module.kernel.spi.feature.Binder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangruzheng on 2021/11/9.
 */
public class ArgumentBinder extends Binder
{
    private List<ArgumentDeclaration> declarations;

    ArgumentBinder()
    {
    }

    public ArgumentDeclarationOptions bind(Object resolverId)
    {
        ArgumentDeclaration declaration = new ArgumentDeclaration(resolverId, null);
        this.useList().add(declaration);
        return new ArgumentDeclarationOptions(declaration);
    }

    public ArgumentDeclarationOptions bind(Class<? extends ArgumentResolver> resolverClass)
    {
        ArgumentDeclaration declaration = new ArgumentDeclaration(null, resolverClass);
        this.useList().add(declaration);
        return new ArgumentDeclarationOptions(declaration);
    }

    List<ArgumentDeclaration> getDeclarations()
    {
        return declarations;
    }

    protected void clear()
    {
        if (this.declarations != null)
        {
            this.declarations.clear();
        }
    }

    private List<ArgumentDeclaration> useList()
    {
        if (this.declarations == null)
        {
            this.declarations = new ArrayList<ArgumentDeclaration>();
        }
        return this.declarations;
    }
}
