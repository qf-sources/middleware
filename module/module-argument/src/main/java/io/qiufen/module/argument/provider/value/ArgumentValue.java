package io.qiufen.module.argument.provider.value;

import io.qiufen.module.argument.provider.common.Util;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.value.Value;

public class ArgumentValue implements Value
{
    private final String scope;
    private final Object[] keys;

    public ArgumentValue(Object key)
    {
        this(null, key);
    }

    public ArgumentValue(String scope, Object... keys)
    {
        this.scope = scope;
        this.keys = keys;
    }

    @Override
    public <T> T to(FacadeFinder finder, Class<T> clazz)
    {
        if (scope == null)
        {
            return Util.to(finder, keys, clazz);
        }
        return Util.to(finder, scope, keys, clazz);
    }
}
