package io.qiufen.module.argument.spi;

public abstract class SingleKeyArgument implements Argument
{
    @Override
    public Object hit(Object[] keys)
    {
        Object key = keys[0];
        if (key == null)
        {
            throw new IllegalArgumentException("key not allow be null");
        }

        return hit(key);
    }

    @Override
    public boolean has(Object[] keys)
    {
        Object key = keys[0];
        if (key == null)
        {
            throw new IllegalArgumentException("key not allow be null");
        }

        return has(key);
    }

    protected abstract Object hit(Object key);

    protected abstract boolean has(Object key);
}
