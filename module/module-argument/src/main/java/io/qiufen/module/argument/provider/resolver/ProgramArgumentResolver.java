package io.qiufen.module.argument.provider.resolver;

import io.qiufen.module.argument.spi.Argument;
import io.qiufen.module.argument.spi.resolver.ArgumentResolver;

/**
 * 程序执行参数解析器
 */
public class ProgramArgumentResolver implements ArgumentResolver
{
    @Override
    public Argument resolve()
    {
        return new Argument()
        {
            @Override
            public Object hit(Object[] keys)
            {
                return null;
            }

            @Override
            public boolean has(Object[] keys)
            {
                return false;
            }
        };
    }
}
