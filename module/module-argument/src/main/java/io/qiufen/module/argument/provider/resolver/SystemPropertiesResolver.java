package io.qiufen.module.argument.provider.resolver;

import io.qiufen.module.argument.spi.Argument;
import io.qiufen.module.argument.spi.SingleKeyArgument;
import io.qiufen.module.argument.spi.resolver.ArgumentResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;

/**
 * 系统jvm参数解析器
 */
public class SystemPropertiesResolver implements ArgumentResolver
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SystemPropertiesResolver.class);

    public SystemPropertiesResolver()
    {
        Properties properties = System.getProperties();
        for (Map.Entry<Object, Object> entry : properties.entrySet())
        {
            LOGGER.info("Found argument{{}={}}", entry.getKey(), entry.getValue());
        }
    }

    @Override
    public Argument resolve()
    {
        return new SingleKeyArgument()
        {
            @Override
            public Object hit(Object key)
            {
                return System.getProperties().get(key);
            }

            @Override
            protected boolean has(Object key)
            {
                return System.getProperties().containsKey(key);
            }
        };
    }
}
