package io.qiufen.module.argument.provider.feature;

import io.qiufen.module.argument.api.facade.ArgumentFacade;
import io.qiufen.module.argument.provider.context.ArgumentContext;
import io.qiufen.module.argument.spi.resolver.ArgumentResolver;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;
import io.qiufen.module.kernel.spi.feature.BindableFeatureHandler;
import io.qiufen.module.kernel.spi.priority.Priority;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import io.qiufen.module.processor.spi.Processor;

import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArgumentFeatureHandler extends BindableFeatureHandler<ArgumentBinder, ArgumentFeature>
{
    private ArgumentContext context;

    @Override
    public void initFeature(ContextFactory contextFactory, FacadeRegistry facadeRegistry)
    {
        //注册实参上下内容环境
        this.context = new ArgumentContext();
        contextFactory.addContext(ArgumentContext.class, this.context);
        //注册私有化实参门面
        facadeRegistry.addFacade(ArgumentFacade.class, new PrivateArgumentFacade());
    }

    @Override
    protected ArgumentBinder initBinder(ArgumentFeature feature, ContextFactory contextFactory)
    {
        return new ArgumentBinder();
    }

    @Override
    protected void doHandle(ArgumentFeature feature, ContextFactory contextFactory, ArgumentBinder binder)
    {
        feature.bind(binder);
        List<ArgumentDeclaration> declarations = binder.getDeclarations();
        if (declarations != null && !declarations.isEmpty())
        {
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            for (ArgumentDeclaration declaration : declarations)
            {
                Object resolverId = declaration.getResolverId();
                Class<? extends ArgumentResolver> clazz = declaration.getResolverClass();
                if (resolverId != null)
                {
                    objectContext.getObject(resolverId, Processor.class);
                    continue;
                }

                if (clazz != null)
                {
                    if (!objectContext.hasDeclaration(clazz))
                    {
                        ClassDeclaration classDeclaration = new ClassDeclaration(clazz);
                        classDeclaration.setScope(ObjectScope.ONCE);
                        objectContext.addDeclaration(classDeclaration);
                    }
                    continue;
                }

                throw new IllegalArgumentException("Must provide processor id or processor class at least");
            }
        }
    }

    @Override
    protected void postModule(ArgumentFeature feature, ContextFactory contextFactory, ArgumentBinder binder)
    {
        List<ArgumentDeclaration> argumentDeclarations = binder.getDeclarations();
        if (argumentDeclarations != null && !argumentDeclarations.isEmpty())
        {
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            Map<Object, Priority> priorityMap = new IdentityHashMap<Object, Priority>();
            for (ArgumentDeclaration declaration : argumentDeclarations)
            {
                Object resolverId = declaration.getResolverId();
                Class<? extends ArgumentResolver> clazz = declaration.getResolverClass();
                ArgumentResolver resolver = null;
                if (resolverId != null)
                {
                    resolver = objectContext.getObject(resolverId, ArgumentResolver.class);
                }
                else if (clazz != null)
                {
                    resolver = objectContext.getObject(clazz);
                }
                if (resolver == null) continue;

                String scope = declaration.getScope();
                this.context.addArgumentResolver(scope, resolver);
                if (declaration.getPriority() != null) priorityMap.put(resolver, declaration.getPriority());
            }
            this.context.priority(priorityMap);
        }
    }

    /**
     * 私有化的实参门面实现
     */
    private class PrivateArgumentFacade implements ArgumentFacade
    {
        @Override
        public Object hitArgument(String scope, Object[] keys)
        {
            return context.hitArgument(scope, keys);
        }

        public Object hitArgument(Object[] keys)
        {
            return context.hitArgument(keys);
        }

        @Override
        public String of(String placeHolder)
        {
            return parse(placeHolder);
        }

        @Override
        public boolean hasArgument(String scope, Object[] keys)
        {
            return context.hasArgument(scope, keys);
        }

        @Override
        public boolean hasArgument(Object[] keys)
        {
            return context.hasArgument(keys);
        }

        private String parse(String placeHolder)
        {
            Matcher matcher = Pattern.compile("\\$\\{(.+?)\\}").matcher(placeHolder);
            boolean matched = false;
            while (matcher.find())
            {
                String var = matcher.group(1);
                if ("".equals(var))
                {
                    throw new RuntimeException("none argument in expression.");
                }
                String[] args = var.split(":");
                String scope = args.length > 1 ? args[0] : null;
                String key = args.length > 1 ? args[1] : args[0];
                String value;
                if (scope == null)
                {
                    value = hitArgument(new Object[]{key}).toString();
                }
                else
                {
                    value = hitArgument(scope, new Object[]{key}).toString();
                }
                placeHolder = matcher.replaceFirst(of(value));
                matcher = Pattern.compile("\\$\\{(.+?)\\}").matcher(placeHolder);
                matched = true;
            }
            if (matched)
            {
                placeHolder = parse(placeHolder);
            }
            return placeHolder;
        }
    }
}
