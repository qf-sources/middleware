package io.qiufen.module.argument.provider.qualifier;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.module.argument.api.qualifier.Argument;
import io.qiufen.module.argument.provider.common.Util;
import io.qiufen.module.injection.spi.qualifier.ObjectCondition;
import io.qiufen.module.injection.spi.qualifier.QualifierProcessorAdapter;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;

public class ArgumentQualifierProcessor extends QualifierProcessorAdapter<Argument>
{
    @Override
    public Class<Argument> supportType()
    {
        return Argument.class;
    }

    @Override
    public void doProcess(Argument qualifier, Class<?> type, FacadeFinder finder, ObjectCondition condition)
    {
        Object[] keys = qualifier.value();
        String scope = qualifier.scope();
        Object value;
        if (CharSequences.isEmpty(scope))
        {
            value = Util.to(finder, keys, type);
        }
        else
        {
            value = Util.to(finder, scope, keys, type);
        }
        condition.putObjectIntersection(value);
    }
}
