package io.qiufen.module.argument.provider.value;

import io.qiufen.common.conversion.TypeConversionService;
import io.qiufen.common.lang.CharSequences;
import io.qiufen.module.argument.api.facade.ArgumentFacade;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.value.Value;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlaceholderValue implements Value
{
//    private static final Parser parser = new Parser();

    private final String placeholder;

    public PlaceholderValue(String placeholder)
    {
        this.placeholder = placeholder;
    }

    @Override
    public <T> T to(FacadeFinder finder, Class<T> clazz)
    {
        CharSequence finalValue = parse(placeholder, null, finder.getFacade(ArgumentFacade.class));
        return TypeConversionService.getInstance().convert(finalValue, clazz);
    }

    //todo 整合到char sequences
    private <T> String parse(String placeHolder, CharSequences.VariableParser<T> parser, ArgumentFacade args)
    {
        Matcher matcher = Pattern.compile("\\$\\{(.+?)\\}").matcher(placeHolder);
        boolean matched = false;
        while (matcher.find())
        {
            String var = matcher.group(1);
            if ("".equals(var))
            {
                throw new RuntimeException("none argument in expression.");
            }
            placeHolder = matcher.replaceFirst(parse(args, var).toString());
            matcher = Pattern.compile("\\$\\{(.+?)\\}").matcher(placeHolder);
            matched = true;
        }
        if (matched)
        {
            placeHolder = parse(placeHolder, parser, args);
        }
        return placeHolder;
    }

    //    private static class Parser implements CharSequences.VariableParser<ArgumentFacade>
//    {
//        @Override
    private Object parse(ArgumentFacade o, String var)
    {
        int index = -1;
        for (int i = 0; i < var.length(); i++)
        {
            char ch = var.charAt(i);
            if (ch == ':')
            {
                index = i;
                break;
            }
        }
        String scope = index < 0 ? null : var.substring(0, index);
        String key = index < 0 ? var : var.substring(index + 1);
        Object[] keyArray = new Object[]{key};
        return scope == null ? o.hitArgument(keyArray) : o.hitArgument(scope, keyArray);
    }
//    }
}
