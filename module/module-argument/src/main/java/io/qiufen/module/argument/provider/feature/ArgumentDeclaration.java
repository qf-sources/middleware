package io.qiufen.module.argument.provider.feature;

import io.qiufen.module.argument.spi.resolver.ArgumentResolver;
import io.qiufen.module.kernel.spi.priority.Priority;

public class ArgumentDeclaration
{
    private final Object resolverId;

    private final Class<? extends ArgumentResolver> resolverClass;

    private String scope;

    private Priority priority;

    ArgumentDeclaration(Object resolverId, Class<? extends ArgumentResolver> resolverClass)
    {
        this.resolverId = resolverId;
        this.resolverClass = resolverClass;
    }

    public Object getResolverId()
    {
        return resolverId;
    }

    public Class<? extends ArgumentResolver> getResolverClass()
    {
        return resolverClass;
    }

    public String getScope()
    {
        return scope;
    }

    public void setScope(String scope)
    {
        this.scope = scope;
    }

    public Priority getPriority()
    {
        return priority;
    }

    public void setPriority(Priority priority)
    {
        this.priority = priority;
    }
}
