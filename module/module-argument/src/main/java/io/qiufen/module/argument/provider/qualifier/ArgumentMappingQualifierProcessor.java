package io.qiufen.module.argument.provider.qualifier;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.module.argument.api.facade.ArgumentFacade;
import io.qiufen.module.argument.api.qualifier.ArgumentMapping;
import io.qiufen.module.argument.provider.common.Util;
import io.qiufen.module.injection.spi.qualifier.ObjectCondition;
import io.qiufen.module.injection.spi.qualifier.QualifierProcessorAdapter;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ArgumentMappingQualifierProcessor extends QualifierProcessorAdapter<ArgumentMapping>
{
    @Override
    public Class<ArgumentMapping> supportType()
    {
        return ArgumentMapping.class;
    }

    @Override
    public void doProcess(ArgumentMapping qualifier, Class<?> type, FacadeFinder finder, ObjectCondition condition)
            throws Exception
    {
        Object object = type.newInstance();
        PropertyDescriptor[] descriptors = Introspector.getBeanInfo(type).getPropertyDescriptors();
        if (descriptors.length == 0)
        {
            condition.putObjectIntersection(object);
            return;
        }

        ArgumentFacade facade = finder.getFacade(ArgumentFacade.class);
        String scope = qualifier.scope();
        for (PropertyDescriptor descriptor : descriptors)
        {
            Method method = descriptor.getWriteMethod();
            if (method == null)
            {
                continue;
            }

            Object[] keys = {qualifier.prefix() + descriptor.getName()};
            try
            {
                if (CharSequences.isEmpty(scope))
                {
                    if (facade.hasArgument(keys))
                    {
                        method.invoke(object, Util.to(finder, keys, descriptor.getPropertyType()));
                    }
                }
                else
                {
                    if (facade.hasArgument(scope, keys))
                    {
                        method.invoke(object, Util.to(finder, scope, keys, descriptor.getPropertyType()));
                    }
                }
            }
            catch (InvocationTargetException e)
            {
                throw (Exception) e.getTargetException();
            }
        }
        condition.putObjectIntersection(object);
    }
}
