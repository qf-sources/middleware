package io.qiufen.module.argument.spi.resolver;

import io.qiufen.module.argument.spi.Argument;

/**
 * 参数解析器接口：定义参数解析方法
 */
public interface ArgumentResolver
{
    /**
     * 解析实参
     */
    Argument resolve();
}
