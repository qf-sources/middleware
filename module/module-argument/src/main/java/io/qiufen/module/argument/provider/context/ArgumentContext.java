package io.qiufen.module.argument.provider.context;

import io.qiufen.module.argument.api.exception.ArgumentException;
import io.qiufen.module.argument.spi.Argument;
import io.qiufen.module.argument.spi.resolver.ArgumentResolver;
import io.qiufen.module.kernel.provider.priority.PriorityUtil;
import io.qiufen.module.kernel.spi.context.Context;
import io.qiufen.module.kernel.spi.priority.Priority;

import java.util.*;

public class ArgumentContext implements Context
{
    private final List<ArgumentResolver> allResolvers = new ArrayList<ArgumentResolver>();

    private final Map<String, List<ArgumentResolver>> scopeResolvers = new HashMap<String, List<ArgumentResolver>>();

    private final Map<ArgumentResolver, Argument> arguments = new IdentityHashMap<ArgumentResolver, Argument>();

    public void addArgumentResolver(String scope, ArgumentResolver resolver)
    {
        if (this.arguments.containsKey(resolver))
        {
            throw new ArgumentException("Duplication resolver:" + resolver.getClass().getName());
        }
        this.arguments.put(resolver, resolver.resolve());
        this.allResolvers.add(resolver);

        if (scope != null && !"".equals(scope))
        {
            List<ArgumentResolver> resolvers = this.scopeResolvers.get(scope);
            if (resolvers == null)
            {
                resolvers = new ArrayList<ArgumentResolver>();
                this.scopeResolvers.put(scope, resolvers);
            }
            resolvers.add(resolver);
        }
    }

    public void priority(Map<Object, Priority> priorityMap)
    {
        PriorityUtil.sort(allResolvers, priorityMap);

        for (Map.Entry<String, List<ArgumentResolver>> entry : scopeResolvers.entrySet())
        {
            PriorityUtil.sort(entry.getValue(), priorityMap);
        }
    }

    public Object hitArgument(String scope, Object[] keys)
    {
        return hitArgument(hitScope(scope), keys);
    }

    public Object hitArgument(Object[] keys)
    {
        return hitArgument(this.allResolvers, keys);
    }

    private Object hitArgument(List<ArgumentResolver> resolvers, Object[] keys)
    {
        checkParams(keys);
        for (ArgumentResolver resolver : resolvers)
        {
            Argument argument = arguments.get(resolver);
            if (argument != null && argument.has(keys))
            {
                return argument.hit(keys);
            }
        }
        throw new ArgumentException("Can't hit any argument for keys:" + Arrays.asList(keys));
    }

    public boolean hasArgument(String scope, Object[] keys)
    {
        return hasArgument(hitScope(scope), keys);
    }

    public boolean hasArgument(Object[] keys)
    {
        return hasArgument(this.allResolvers, keys);
    }

    private boolean hasArgument(List<ArgumentResolver> resolvers, Object[] keys)
    {
        checkParams(keys);
        for (ArgumentResolver resolver : resolvers)
        {
            Argument argument = arguments.get(resolver);
            if (argument != null && argument.has(keys))
            {
                return true;
            }
        }
        return false;
    }

    private void checkParams(Object[] keys)
    {
        if (keys == null || keys.length == 0)
        {
            throw new IllegalArgumentException();
        }
    }

    private List<ArgumentResolver> hitScope(String scope)
    {
        List<ArgumentResolver> resolvers = this.scopeResolvers.get(scope);
        if (resolvers == null)
        {
            throw new ArgumentException("Unknown scope:" + scope);
        }
        return resolvers;
    }
}
