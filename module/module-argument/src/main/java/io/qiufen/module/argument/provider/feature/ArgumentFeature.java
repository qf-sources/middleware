package io.qiufen.module.argument.provider.feature;

import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

/**
 * 模块实参特性
 */
@Handler(ArgumentFeatureHandler.class)
public interface ArgumentFeature extends Feature
{
    void bind(ArgumentBinder binder);
}
