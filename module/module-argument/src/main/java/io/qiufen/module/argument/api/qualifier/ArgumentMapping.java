package io.qiufen.module.argument.api.qualifier;

import jakarta.inject.Qualifier;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Qualifier
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ArgumentMapping
{
    /**
     * scope
     */
    String scope() default "";

    /**
     * prefix
     */
    String prefix() default "";
}
