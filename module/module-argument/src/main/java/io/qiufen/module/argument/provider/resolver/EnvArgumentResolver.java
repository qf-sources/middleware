package io.qiufen.module.argument.provider.resolver;

import io.qiufen.module.argument.spi.Argument;
import io.qiufen.module.argument.spi.SingleKeyArgument;
import io.qiufen.module.argument.spi.resolver.ArgumentResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 环境变量解析器
 */
public class EnvArgumentResolver implements ArgumentResolver
{
    private static final Logger LOGGER = LoggerFactory.getLogger(EnvArgumentResolver.class);

    public EnvArgumentResolver()
    {
        Map<String, String> map = System.getenv();
        for (Map.Entry<String, String> entry : map.entrySet())
        {
            LOGGER.info("Found argument{{}={}}", entry.getKey(), entry.getValue());
        }
    }

    @Override
    public Argument resolve()
    {
        return new SingleKeyArgument()
        {
            @Override
            protected Object hit(Object key)
            {
                return System.getenv().get(key instanceof String ? key : key.toString());
            }

            @Override
            protected boolean has(Object key)
            {
                return System.getenv().containsKey(key instanceof String ? key : key.toString());
            }
        };
    }
}
