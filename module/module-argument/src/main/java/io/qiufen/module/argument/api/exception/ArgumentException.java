package io.qiufen.module.argument.api.exception;

import io.qiufen.module.kernel.spi.exception.ModuleException;

public class ArgumentException extends ModuleException
{
    public ArgumentException(String message)
    {
        super(message);
    }
}
