package io.qiufen.module.argument.provider.common;

import io.qiufen.common.conversion.TypeConversionService;
import io.qiufen.module.argument.api.facade.ArgumentFacade;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;

public class Util
{
    public static <T> T to(FacadeFinder finder, Object[] keys, Class<T> clazz)
    {
        Object value = finder.getFacade(ArgumentFacade.class).hitArgument(keys);
        return TypeConversionService.getInstance().convert(value, clazz);
    }

    public static <T> T to(FacadeFinder finder, String scope, Object[] keys, Class<T> clazz)
    {
        Object value = finder.getFacade(ArgumentFacade.class).hitArgument(scope, keys);
        return TypeConversionService.getInstance().convert(value, clazz);
    }
}
