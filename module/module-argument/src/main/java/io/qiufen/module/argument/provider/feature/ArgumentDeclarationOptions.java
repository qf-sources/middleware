package io.qiufen.module.argument.provider.feature;

import io.qiufen.module.kernel.spi.priority.Priority;

public class ArgumentDeclarationOptions
{
    private final ArgumentDeclaration declaration;

    ArgumentDeclarationOptions(ArgumentDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public ArgumentDeclarationOptions scope(String scope)
    {
        declaration.setScope(scope);
        return this;
    }

    public ArgumentDeclarationOptions after(Class<?>... classes)
    {
        usePriority().after(classes);
        return this;
    }

    public ArgumentDeclarationOptions before(Class<?>... classes)
    {
        usePriority().before(classes);
        return this;
    }

    private Priority usePriority()
    {
        Priority priority = this.declaration.getPriority();
        if (priority == null)
        {
            priority = new Priority();
            this.declaration.setPriority(priority);
        }
        return priority;
    }
}
