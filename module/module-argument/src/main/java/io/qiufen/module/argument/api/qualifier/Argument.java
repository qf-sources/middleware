package io.qiufen.module.argument.api.qualifier;

import jakarta.inject.Qualifier;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Documented
@Qualifier
public @interface Argument
{
    /**
     * scope
     */
    String scope() default "";

    /**
     * keys
     */
    String[] value();
}
