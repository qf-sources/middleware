package io.qiufen.module.argument.spi;

/**
 * 参数接口，定义数据存取方法
 */
public interface Argument
{
    /**
     * 命中实参
     */
    Object hit(Object[] keys);

    boolean has(Object[] keys);
}
