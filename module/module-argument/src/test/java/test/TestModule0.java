package test;

import io.qiufen.module.argument.api.facade.ArgumentFacade;
import io.qiufen.module.argument.provider.feature.ArgumentBinder;
import io.qiufen.module.argument.provider.feature.ArgumentFeature;
import io.qiufen.module.argument.provider.resolver.EnvArgumentResolver;
import io.qiufen.module.argument.provider.resolver.ProgramArgumentResolver;
import io.qiufen.module.argument.provider.resolver.SystemPropertiesResolver;
import io.qiufen.module.binding.provider.feature.BindingBinder;
import io.qiufen.module.binding.provider.feature.BindingFeature;
import io.qiufen.module.launcher.provider.Launcher;

public class TestModule0 implements ArgumentFeature, BindingFeature
{
    public static void main(String[] args)
    {
        ArgumentFacade facade = new Launcher().addModule(TestModule0.class)
                .start()
                .getFacadeFinder()
                .getFacade(ArgumentFacade.class);
        System.out.println(facade.hitArgument(new Object[]{"sun.cpu.isalist"}));
    }

    @Override
    public void bind(ArgumentBinder binder)
    {
        binder.bind(EnvArgumentResolver.class).scope("env");
        binder.bind(SystemPropertiesResolver.class).scope("prop");
        binder.bind(ProgramArgumentResolver.class).scope("program");
    }

    @Override
    public void bind(BindingBinder binder)
    {
        //        binder.bind();
    }
}
