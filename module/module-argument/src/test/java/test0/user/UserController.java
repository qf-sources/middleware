package test0.user;

import io.qiufen.module.argument.api.qualifier.Argument;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import test0.MyDAO;

import javax.annotation.PostConstruct;

/**
 * Created by zhangruzheng on 2021/11/9.
 */
public class UserController
{
    @Inject
    private UserService userService;

    @Inject
    @MyDAO
    private UserDao userDao;

    @Inject
    @Argument({"os.arch"})
    private String arch;

    @Inject
    UserController(@Named("userService") UserService userService,
            @Argument(scope = "prop", value = {"os.arch"}) String arch)
    {
        System.out.println("constructor:" + userService);
        System.out.println("constructor:" + arch);
    }

    @Inject
    void setParams(@Named("userService") UserService userService, @Argument({"os.arch"}) String arch)
    {
        System.out.println("setParams:" + userService);
        System.out.println("setParams:" + arch);
    }

    @PostConstruct
    public void init()
    {
        System.out.println("init:" + userService);
        System.out.println("init:" + arch);
    }
}
