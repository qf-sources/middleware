package test0.user;

import io.qiufen.module.injection.api.qualifier.Id;
import jakarta.inject.Inject;
import jakarta.inject.Provider;
import test0.MyDAO;

import javax.annotation.PostConstruct;

/**
 * Created by zhangruzheng on 2021/11/9.
 */
public class UserService
{
    private final Provider<UserDao> userDaoProvider;

    @Inject
    UserService(@MyDAO @Id(value = "000", type = int.class) Provider<UserDao> userDaoProvider)
    {
        this.userDaoProvider = userDaoProvider;
    }

    @PostConstruct
    public void init()
    {
        System.out.println("user dao:" + userDaoProvider.get());
    }
}
