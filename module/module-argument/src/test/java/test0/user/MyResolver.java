package test0.user;

import io.qiufen.module.argument.spi.Argument;
import io.qiufen.module.argument.spi.SingleKeyArgument;
import io.qiufen.module.argument.spi.resolver.ArgumentResolver;
import jakarta.inject.Inject;
import test0.MyDAO;

import javax.annotation.PostConstruct;
import java.util.Date;

public class MyResolver implements ArgumentResolver
{
    private final UserDao userDao;

    @Inject
    MyResolver(@MyDAO UserDao userDao)
    {
        this.userDao = userDao;
    }

    @Override
    public Argument resolve()
    {
        System.out.println("resolving:" + userDao);
        return new SingleKeyArgument()
        {
            @Override
            protected Object hit(Object key)
            {
                return new Date();
            }

            @Override
            protected boolean has(Object key)
            {
                return true;
            }
        };
    }

    @PostConstruct
    public void init()
    {
        System.out.println("resolver:" + userDao);
    }
}
