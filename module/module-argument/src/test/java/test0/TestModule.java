package test0;

import io.qiufen.module.annotation.provider.processor.PostConstructAnnotationProcessor;
import io.qiufen.module.annotation.provider.processor.PreDestroyAnnotationProcessor;
import io.qiufen.module.argument.api.facade.ArgumentFacade;
import io.qiufen.module.argument.provider.feature.ArgumentBinder;
import io.qiufen.module.argument.provider.feature.ArgumentFeature;
import io.qiufen.module.argument.provider.qualifier.ArgumentQualifierProcessor;
import io.qiufen.module.argument.provider.resolver.EnvArgumentResolver;
import io.qiufen.module.argument.provider.resolver.ProgramArgumentResolver;
import io.qiufen.module.argument.provider.resolver.SystemPropertiesResolver;
import io.qiufen.module.binding.provider.feature.BindingBinder;
import io.qiufen.module.binding.provider.feature.BindingFeature;
import io.qiufen.module.binding.provider.feature.ObjectBuilder;
import io.qiufen.module.injection.provider.feature.InjectionBinder;
import io.qiufen.module.injection.provider.feature.InjectionFeature;
import io.qiufen.module.injection.provider.processor.DeclarationPreparedProcessor;
import io.qiufen.module.injection.provider.processor.InjectConstructorProcessor;
import io.qiufen.module.injection.provider.processor.InjectFieldProcessor;
import io.qiufen.module.injection.provider.processor.InjectMethodProcessor;
import io.qiufen.module.injection.provider.qualifier.IdQualifierProcessor;
import io.qiufen.module.injection.provider.qualifier.NamedQualifierProcessor;
import io.qiufen.module.kernel.api.module.ModuleLifeCycleAdapter;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.launcher.provider.Launcher;
import io.qiufen.module.object.api.declaration.ObjectDeclaration;
import io.qiufen.module.object.api.facade.ObjectFacade;
import io.qiufen.module.processor.provider.feature.ProcessorBinder;
import io.qiufen.module.processor.provider.feature.ProcessorFeature;
import io.qiufen.module.processor.provider.feature.Registered;
import test0.user.MyResolver;
import test0.user.UserController;
import test0.user.UserDao;
import test0.user.UserService;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.concurrent.locks.LockSupport;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/10 11:16
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class TestModule extends ModuleLifeCycleAdapter implements ProcessorFeature, InjectionFeature, ArgumentFeature, BindingFeature
{
    public static void main(String[] args)
    {
        System.setProperty("a", "A");
        System.setProperty("b", "B");
        System.setProperty("test", "tester${a}2222!${b}abc");

        Launcher launcher = new Launcher().addModule(TestModule.class).start();
        ObjectFacade objectFacade = launcher.getFacadeFinder().getObjectFacade();
        objectFacade.addDeclaration(new ObjectDeclaration(new TestService()));

        System.out.println(objectFacade.getObject(TestService.class));

        LockSupport.park();
    }

    @Override
    public void bind(BindingBinder binder)
    {
        binder.bind(UserDao.class).id(0x000);
        binder.bind(UserService.class).id("userService");
        binder.bind(UserController.class);

        binder.bind(DataSource.class, new ObjectBuilder()
        {
            @Override
            public Object build(FacadeFinder finder)
            {
                ArgumentFacade facade = finder.getFacade(ArgumentFacade.class);
                org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();
                dataSource.setDriverClassName(facade.of("${prop:a}"));
                dataSource.setUrl(facade.of("${prop:b}"));
                dataSource.setUsername(facade.of("${prop:test}"));
                dataSource.setPassword(facade.of("${test}"));
                return dataSource;
            }
        }).lazy(false);
    }

    @Override
    public void bind(ProcessorBinder binder)
    {
        binder.bind(DeclarationPreparedProcessor.class).at(Registered.PRE);
        binder.bind(InjectConstructorProcessor.class);
        binder.bind(InjectFieldProcessor.class);
        binder.bind(InjectMethodProcessor.class);
        binder.bind(PostConstructAnnotationProcessor.class);
        binder.bind(PreDestroyAnnotationProcessor.class);
    }

    @Override
    public void bind(InjectionBinder binder)
    {
        binder.bind(ArgumentQualifierProcessor.class);
        binder.bind(IdQualifierProcessor.class);
        binder.bind(NamedQualifierProcessor.class);
    }

    @Override
    public void bind(ArgumentBinder binder)
    {
        binder.bind(EnvArgumentResolver.class).scope("env");
        binder.bind(SystemPropertiesResolver.class).scope("prop").before(EnvArgumentResolver.class);
        binder.bind(ProgramArgumentResolver.class).scope("program").before(SystemPropertiesResolver.class);
        binder.bind(MyResolver.class).before(EnvArgumentResolver.class);
    }

    @Override
    public void onLoad()
    {
        System.out.println("开始加载test模块...");
    }

    @Override
    public void onLoadSuccess()
    {
        System.out.println("test模块加载成功");
    }
}

class TestService
{
    @Resource
    private UserDao userDao;

    @Resource
    private FacadeFinder facadeFinder;
}