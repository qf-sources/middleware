package test0;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class TestAdvice implements MethodInterceptor
{
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable
    {
        System.out.println("doBefore");
        try
        {
            Object o = methodInvocation.proceed();
            System.out.println("doAfter");
            return o;
        }
        catch (Exception e)
        {
            System.out.println("doThrowing");
            throw e;
        }
        finally
        {
            System.out.println("doFinal");
        }
    }
}
