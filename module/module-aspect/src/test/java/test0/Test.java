package test0;

import io.qiufen.module.aspect.provider.feature.AspectBinder;
import io.qiufen.module.aspect.provider.feature.AspectFeature;
import io.qiufen.module.binding.provider.feature.BindingBinder;
import io.qiufen.module.binding.provider.feature.BindingFeature;
import io.qiufen.module.launcher.provider.Launcher;

import javax.annotation.Resource;

public class Test implements AspectFeature, BindingFeature
{
    public static void main(String[] args)
    {
        Launcher launcher = new Launcher().addModule(Test.class).start();
        launcher.getFacadeFinder().getObjectFacade().getObject(UserService.class).test();
    }

    @Override
    public void bind(AspectBinder binder)
    {
        //单个规则匹配
        binder.bindPointcut("test0Aspect", "..*.test0*(..)");
        binder.bindPointcut("test1Aspect", Resource.class);
        //        binder.bindPointcut("test1Aspect", ImportModule.class);自定义匹配器

        //todo 复合规则匹配
        //        binder.bindPointcut("testAspect",Mode.INSECT, "test0Aspect", "test1Aspect");//模式：交集复合
        //        binder.bindPointcut("testAspect",Mode.INSECT, "test0Aspect", "test1Aspect");//模式：并集复合
        //        binder.bindPointcut("testAspect",Mode.INSECT, "test0Aspect", "test1Aspect");//模式：差集复合
        //        binder.bindPointcut("testAspect",Mode.INSECT, "test0Aspect", "test1Aspect");//模式：自定义复合

        binder.bindAdvice(TestAdvice.class, "test1Aspect");
        binder.bindAdvice(MyLog.class, "test1Aspect");//todo 优先级
    }

    @Override
    public void bind(BindingBinder binder)
    {
        binder.bind(UserServiceImpl.class);
    }
}
