package test0;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class MyLog implements MethodInterceptor
{
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable
    {
        System.out.println("MyLog:before");
        try
        {
            return invocation.proceed();
            //            return null;
        }
        finally
        {
            System.out.println("MyLog:after");
        }
    }
}
