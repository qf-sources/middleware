package io.qiufen.module.aspect.provider.context;

import io.qiufen.module.kernel.spi.context.Context;

import java.util.HashMap;
import java.util.Map;

public class AspectContext implements Context
{
    private Map<Object, PointcutEntry> pointcutEntries;

    public Map<Object, PointcutEntry> usePointcutEntries()
    {
        return pointcutEntries == null ? (pointcutEntries = new HashMap<Object, PointcutEntry>()) : pointcutEntries;
    }

    public Map<Object, PointcutEntry> getPointcutEntries()
    {
        return pointcutEntries;
    }
}
