package io.qiufen.module.aspect.provider.pointcut;

import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.ConstructorInterceptor;
import org.aopalliance.intercept.ConstructorInvocation;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;

class ConstructorInvocationImpl implements ConstructorInvocation
{
    private final Object target;
    private final Constructor constructor;
    private final Iterator<Advice> adviceIterator;
    private final Object[] args;

    ConstructorInvocationImpl(Object target, Constructor constructor, Iterator<Advice> adviceIterator, Object[] args)
    {
        this.target = target;
        this.constructor = constructor;
        this.adviceIterator = adviceIterator;
        this.args = args;
    }

    @Override
    public Constructor getConstructor()
    {
        return constructor;
    }

    @Override
    public Object[] getArguments()
    {
        return args;
    }

    @Override
    public Object proceed() throws Throwable
    {
        if (adviceIterator.hasNext())
        {
            ConstructorInterceptor interceptor = (ConstructorInterceptor) adviceIterator.next();
            return interceptor.construct(this);
        }
        try
        {
            return constructor.newInstance(args);
        }
        catch (InvocationTargetException e)
        {
            throw e.getTargetException();
        }
    }

    @Override
    public Object getThis()
    {
        return target;
    }

    @Override
    public AccessibleObject getStaticPart()
    {
        return constructor;
    }
}
