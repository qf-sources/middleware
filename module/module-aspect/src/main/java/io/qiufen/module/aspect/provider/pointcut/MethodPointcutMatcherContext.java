package io.qiufen.module.aspect.provider.pointcut;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.module.aspect.spi.pointcut.PointcutMatcherContext;
import org.aopalliance.aop.Advice;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;

public class MethodPointcutMatcherContext implements PointcutMatcherContext
{
    public static final List<Advice> EMPTY = Collections.emptyList();

    private final Object prototype;

    private Object decoratedObject;

    private Map<Method, List<Advice>> advices;

    public MethodPointcutMatcherContext(Object prototype)
    {
        this.prototype = prototype;
    }

    public Object getPrototype()
    {
        return this.prototype;
    }

    public Object getDecoratedObject()
    {
        return this.decoratedObject == null ? this.prototype : this.decoratedObject;
    }

    public List<Advice> initMethodAdvice(Method targetMethod)
    {
        initDecoratedObject();
        if (advices == null) advices = new HashMap<Method, List<Advice>>();

        List<Advice> adviceList = advices.get(targetMethod);
        if (adviceList == null)
        {
            adviceList = new ArrayList<Advice>(1);
            advices.put(targetMethod, adviceList);
        }
        return adviceList;
    }

    private void initDecoratedObject()
    {
        if (this.decoratedObject == null)
        {
            this.decoratedObject = Proxy.newProxyInstance(getClass().getClassLoader(),
                    prototype.getClass().getInterfaces(), new DD());
        }
    }

    private class DD implements InvocationHandler
    {
        private volatile Map<Method, List<Advice>> cachedAdvices;

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
        {
            List<Advice> adviceList = useAdvices(method);
            if (CollectionUtil.isEmpty(adviceList))
            {
                try
                {
                    return method.invoke(prototype, args);
                }
                catch (InvocationTargetException e)
                {
                    Throwable target = e.getTargetException();
                    throw (target instanceof RuntimeException) ? (RuntimeException) target : e;
                }
            }

            return new MethodInvocationImpl(prototype, method, adviceList.iterator(), args).proceed();
        }

        private Map<Method, List<Advice>> useCachedAdvices()
        {
            if (cachedAdvices != null)
            {
                return cachedAdvices;
            }
            synchronized (this)
            {
                if (cachedAdvices != null)
                {
                    return cachedAdvices;
                }
                cachedAdvices = new IdentityHashMap<Method, List<Advice>>(advices);
            }
            return cachedAdvices;
        }

        private List<Advice> useAdvices(Method method)
        {
            Map<Method, List<Advice>> cachedAdvices = useCachedAdvices();
            List<Advice> adviceList = cachedAdvices.get(method);
            if (adviceList != null) return adviceList;

            synchronized (this)
            {
                adviceList = cachedAdvices.get(method);
                if (adviceList != null) return adviceList;

                adviceList = advices.get(method);
                adviceList = adviceList == null ? EMPTY : adviceList;
                cachedAdvices.put(method, adviceList);
            }
            return adviceList;
        }
    }

}
