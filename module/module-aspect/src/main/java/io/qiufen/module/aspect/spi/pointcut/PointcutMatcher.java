package io.qiufen.module.aspect.spi.pointcut;

import java.util.List;

public interface PointcutMatcher<E>
{
    List<Pointcut> doMatch(PointcutMatcherContext context, E expression) throws Exception;
}
