package io.qiufen.module.aspect.provider.pointcut;

import io.qiufen.module.aspect.spi.pointcut.Pointcut;
import org.aopalliance.aop.Advice;

import java.util.List;

public class SimplePointcut implements Pointcut
{
    private final List<Advice> adviceList;

    public SimplePointcut(List<Advice> adviceList)
    {
        this.adviceList = adviceList;
    }

    @Override
    public void weave(Advice advice)
    {
        this.adviceList.add(advice);
    }
}
