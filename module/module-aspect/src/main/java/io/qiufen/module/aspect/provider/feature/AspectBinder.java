package io.qiufen.module.aspect.provider.feature;

import io.qiufen.module.aspect.provider.pointcut.AnnotationPointcutMatcher;
import io.qiufen.module.aspect.provider.pointcut.PatternPointcutMatcher;
import io.qiufen.module.aspect.spi.pointcut.PointcutMatcher;
import io.qiufen.module.kernel.spi.feature.Binder;
import org.aopalliance.aop.Advice;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

public class AspectBinder extends Binder
{
    private List<PointcutMatcherOptions> matcherOptionsList;
    private List<AdviceOptions> adviceOptionsList;

    public void bindPointcut(Object id, String pattern)
    {
        bindPointcut(PatternPointcutMatcher.class).bind(id, pattern);
    }

    public void bindPointcut(Object id, Class<? extends Annotation> annClass)
    {
        bindPointcut(AnnotationPointcutMatcher.class).bind(id, annClass);
    }

    public <E, M extends PointcutMatcher<E>> PointcutMatcherOptions<E> bindPointcut(Class<M> matcherClass)
    {
        return new PointcutMatcherOptions<E>(useMatcherOptionsList(), matcherClass);
    }

    public void bindAdvice(Object adviceId, Object pointcutId)
    {
        useAdviceOptionsList().add(new AdviceOptions(adviceId, pointcutId));
    }

    public void bindAdvice(Class<? extends Advice> adviceClass, Object pointcutId)
    {
        useAdviceOptionsList().add(new AdviceOptions(adviceClass, pointcutId));
    }

    public List<PointcutMatcherOptions> getMatcherOptionsList()
    {
        return matcherOptionsList;
    }

    public List<AdviceOptions> getAdviceOptionsList()
    {
        return adviceOptionsList;
    }

    private List<PointcutMatcherOptions> useMatcherOptionsList()
    {
        return matcherOptionsList == null ? (matcherOptionsList = new ArrayList<PointcutMatcherOptions>()) : matcherOptionsList;
    }

    private List<AdviceOptions> useAdviceOptionsList()
    {
        return adviceOptionsList == null ? (adviceOptionsList = new ArrayList<AdviceOptions>()) : adviceOptionsList;
    }

    @Override
    protected void clear()
    {
        if (this.adviceOptionsList != null)
        {
            this.adviceOptionsList.clear();
        }
        if (this.matcherOptionsList != null)
        {
            this.matcherOptionsList.clear();
        }
    }
}
