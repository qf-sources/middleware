package io.qiufen.module.aspect.provider.context;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.module.aspect.spi.pointcut.Pointcut;
import io.qiufen.module.aspect.spi.pointcut.PointcutMatcher;
import org.aopalliance.aop.Advice;

import java.util.ArrayList;
import java.util.List;

public class PointcutEntry
{
    private final Object id;

    private final Object expression;

    private final PointcutMatcher matcher;

    private List<Pointcut> pointcutList;

    private List<Advice> adviceList;

    public PointcutEntry(Object id, Object expression, PointcutMatcher matcher)
    {
        this.id = id;
        this.expression = expression;
        this.matcher = matcher;
    }

    public Object getId()
    {
        return id;
    }

    public Object getExpression()
    {
        return expression;
    }

    public PointcutMatcher getMatcher()
    {
        return matcher;
    }

    public void addPointcut(Pointcut pointcut)
    {
        if (pointcutList == null) pointcutList = new ArrayList<Pointcut>(1);
        pointcutList.add(pointcut);

        if (CollectionUtil.isNotEmpty(adviceList))
        {
            for (Advice advice : adviceList)
            {
                pointcut.weave(advice);
            }
        }
    }

    public void addAdvice(Advice advice)
    {
        if (adviceList == null) adviceList = new ArrayList<Advice>(1);
        adviceList.add(advice);

        if (CollectionUtil.isNotEmpty(pointcutList))
        {
            for (Pointcut pointcut : pointcutList)
            {
                pointcut.weave(advice);
            }
        }
    }
}
