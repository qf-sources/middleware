package io.qiufen.module.aspect.provider.pointcut;

import io.qiufen.module.aspect.spi.pointcut.Pointcut;
import io.qiufen.module.aspect.spi.pointcut.PointcutMatcher;
import io.qiufen.module.aspect.spi.pointcut.PointcutMatcherContext;
import org.aopalliance.aop.Advice;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PatternPointcutMatcher implements PointcutMatcher<String>
{
    @Override
    public List<Pointcut> doMatch(PointcutMatcherContext context, String expression)
    {
        Pattern pattern = Pattern.compile(expression);

        Class<?>[] inters = context.getPrototype().getClass().getInterfaces();
        List<Pointcut> pointcutList = new ArrayList<Pointcut>(1);
        for (Class<?> inter : inters)
        {
            Method[] methods = inter.getMethods();
            if (methods.length == 0) continue;

            StringBuilder builder = new StringBuilder(inter.getName()).append('.');
            int start = builder.length();
            int end = start;
            for (Method method : methods)
            {
                builder.replace(start, end, method.getName());
                end = builder.length();
                if (pattern.matcher(builder).find())
                {
                    List<Advice> adviceList = context.initMethodAdvice(method);
                    pointcutList.add(new SimplePointcut(adviceList));
                }
            }
        }
        return pointcutList;
    }
}
