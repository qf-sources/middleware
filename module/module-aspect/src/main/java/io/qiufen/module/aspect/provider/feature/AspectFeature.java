package io.qiufen.module.aspect.provider.feature;

import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

@Handler(AspectFeatureHandler.class)
public interface AspectFeature extends Feature
{
    void bind(AspectBinder binder);
}
