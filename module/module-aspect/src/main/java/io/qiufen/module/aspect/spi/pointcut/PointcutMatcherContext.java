package io.qiufen.module.aspect.spi.pointcut;

import org.aopalliance.aop.Advice;

import java.lang.reflect.Method;
import java.util.List;

public interface PointcutMatcherContext
{
    Object getPrototype();

    Object getDecoratedObject();

    List<Advice> initMethodAdvice(Method targetMethod);
}
