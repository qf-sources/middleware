package io.qiufen.module.aspect.provider.feature;

import io.qiufen.module.aspect.spi.pointcut.PointcutMatcher;

import java.util.List;

public class PointcutMatcherOptions<E>
{
    private final List<PointcutMatcherOptions> options;
    private final Class<? extends PointcutMatcher<E>> matcherClass;

    private Object id;
    private E expression;

    PointcutMatcherOptions(List<PointcutMatcherOptions> options, Class<? extends PointcutMatcher<E>> matcherClass)
    {
        this.options = options;
        this.matcherClass = matcherClass;
    }

    public void bind(Object id, E expression)
    {
        this.id = id;
        this.expression = expression;
        this.options.add(this);
    }

    public Class<? extends PointcutMatcher<E>> getMatcherClass()
    {
        return matcherClass;
    }

    public Object getId()
    {
        return id;
    }

    public E getExpression()
    {
        return expression;
    }
}
