package io.qiufen.module.aspect.provider.feature;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.collection.MapUtil;
import io.qiufen.module.aspect.provider.context.AspectContext;
import io.qiufen.module.aspect.provider.context.PointcutEntry;
import io.qiufen.module.aspect.provider.pointcut.MethodPointcutMatcherContext;
import io.qiufen.module.aspect.spi.pointcut.Pointcut;
import io.qiufen.module.aspect.spi.pointcut.PointcutMatcher;
import io.qiufen.module.aspect.spi.pointcut.PointcutMatcherContext;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;
import io.qiufen.module.kernel.spi.feature.BindableFeatureHandler;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.provider.support.event.ObjectConstructEvent;
import io.qiufen.module.object.spi.event.EventHandlerAdapter;
import org.aopalliance.aop.Advice;

import java.util.List;
import java.util.Map;

public class AspectFeatureHandler extends BindableFeatureHandler<AspectBinder, AspectFeature>
{
    @Override
    public void initFeature(ContextFactory contextFactory, FacadeRegistry facadeRegistry)
    {
        AspectContext context = new AspectContext();
        contextFactory.addContext(AspectContext.class, context);
        contextFactory.getContext(ObjectContext.class).addEventHandler(new OO(context));
    }

    @Override
    protected AspectBinder initBinder(AspectFeature feature, ContextFactory contextFactory)
    {
        return new AspectBinder();
    }

    @Override
    protected void doHandle(AspectFeature feature, ContextFactory contextFactory, AspectBinder binder)
    {
        feature.bind(binder);

        List<PointcutMatcherOptions> matcherOptionsList = binder.getMatcherOptionsList();
        if (CollectionUtil.isNotEmpty(matcherOptionsList))
        {
            AspectContext context = contextFactory.getContext(AspectContext.class);
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            Map<Object, PointcutEntry> entries = context.usePointcutEntries();
            for (PointcutMatcherOptions options : matcherOptionsList)
            {
                Object id = options.getId();
                if (entries.containsKey(id))
                {
                    throw new RuntimeException("Duplicate pointcut id:" + id);
                }
                Class<? extends PointcutMatcher> clazz = options.getMatcherClass();
                if (!objectContext.hasDeclaration(clazz))
                {
                    objectContext.addDeclaration(new ClassDeclaration(clazz));
                }
                PointcutMatcher matcher = objectContext.getObject(clazz);
                entries.put(id, new PointcutEntry(id, options.getExpression(), matcher));
            }
            matcherOptionsList.clear();
        }
    }

    @Override
    protected void postModule(AspectFeature feature, ContextFactory contextFactory, AspectBinder binder)
    {
        List<AdviceOptions> adviceOptionsList = binder.getAdviceOptionsList();
        if (CollectionUtil.isEmpty(adviceOptionsList))
        {
            return;
        }

        AspectContext context = contextFactory.getContext(AspectContext.class);
        ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
        Map<Object, PointcutEntry> entries = context.getPointcutEntries();
        for (AdviceOptions options : adviceOptionsList)
        {
            if (entries == null) throw new RuntimeException("Hasn't bind any pointcut.");

            Object pointcutId = options.getPointcutId();
            PointcutEntry entry = entries.get(pointcutId);
            if (entry == null) throw new RuntimeException("Can't find pointcut,id:" + pointcutId);

            Advice advice = getAdvice(objectContext, options.getAdviceId(), options.getAdviceClass());
            entry.addAdvice(advice);
        }
    }

    private Advice getAdvice(ObjectContext objectContext, Object id, Class<? extends Advice> clazz)
    {
        if (id != null) return objectContext.getObject(id, Advice.class);

        if (!objectContext.hasDeclaration(clazz))
        {
            objectContext.addDeclaration(new ClassDeclaration(clazz));
        }
        return objectContext.getObject(clazz);
    }

    private static class OO extends EventHandlerAdapter
    {
        private final AspectContext context;

        private OO(AspectContext context)
        {
            this.context = context;
        }

        @Override
        public void onConstruct(ObjectConstructEvent event) throws Throwable
        {
            super.onConstruct(event);
        }

        @Override
        public Object onDecorate(Object prototype, Object decoratedObject) throws Exception
        {
            Map<Object, PointcutEntry> entries = context.getPointcutEntries();
            if (MapUtil.isEmpty(entries)) return prototype;

            PointcutMatcherContext context = new MethodPointcutMatcherContext(prototype);
            for (Map.Entry<Object, PointcutEntry> mapEntry : entries.entrySet())
            {
                PointcutEntry entry = mapEntry.getValue();
                PointcutMatcher matcher = entry.getMatcher();
                List<Pointcut> pointcutList = matcher.doMatch(context, entry.getExpression());
                if (CollectionUtil.isEmpty(pointcutList)) continue;

                for (Pointcut pointcut : pointcutList)
                {
                    entry.addPointcut(pointcut);
                }
            }
            return context.getDecoratedObject();
        }
    }
}