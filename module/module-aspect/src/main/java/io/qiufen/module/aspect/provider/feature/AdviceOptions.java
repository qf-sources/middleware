package io.qiufen.module.aspect.provider.feature;

import org.aopalliance.aop.Advice;

public class AdviceOptions
{
    private final Object adviceId;
    private final Class<? extends Advice> adviceClass;
    private final Object pointcutId;

    AdviceOptions(Object adviceId, Object pointcutId)
    {
        this(adviceId, null, pointcutId);
    }

    AdviceOptions(Class<? extends Advice> adviceClass, Object pointcutId)
    {
        this(null, adviceClass, pointcutId);
    }

    public AdviceOptions(Object adviceId, Class<? extends Advice> adviceClass, Object pointcutId)
    {
        this.adviceId = adviceId;
        this.adviceClass = adviceClass;
        this.pointcutId = pointcutId;
    }

    public Object getAdviceId()
    {
        return adviceId;
    }

    public Class<? extends Advice> getAdviceClass()
    {
        return adviceClass;
    }

    public Object getPointcutId()
    {
        return pointcutId;
    }
}
