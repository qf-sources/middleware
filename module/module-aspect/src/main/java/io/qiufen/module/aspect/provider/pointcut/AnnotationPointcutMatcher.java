package io.qiufen.module.aspect.provider.pointcut;

import io.qiufen.module.aspect.spi.pointcut.Pointcut;
import io.qiufen.module.aspect.spi.pointcut.PointcutMatcher;
import io.qiufen.module.aspect.spi.pointcut.PointcutMatcherContext;
import org.aopalliance.aop.Advice;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class AnnotationPointcutMatcher implements PointcutMatcher<Class<? extends Annotation>>
{
    @Override
    public List<Pointcut> doMatch(PointcutMatcherContext context, Class<? extends Annotation> expression)
            throws NoSuchMethodException
    {
        Class<?> prototypeClass = context.getPrototype().getClass();
        Class<?>[] inters = prototypeClass.getInterfaces();
        List<Pointcut> pointcutList = new ArrayList<Pointcut>(1);
        boolean markedClass = prototypeClass.isAnnotationPresent(expression);
        for (Class<?> inter : inters)
        {
            Method[] methods = inter.getMethods();
            for (Method method : methods)
            {
                if (markedClass || method.isAnnotationPresent(expression) || implMethod(prototypeClass,
                        method).isAnnotationPresent(expression))
                {
                    List<Advice> adviceList = context.initMethodAdvice(method);
                    pointcutList.add(new SimplePointcut(adviceList));
                }
            }
        }
        return pointcutList;
    }

    private Method implMethod(Class<?> prototypeClass, Method interMethod) throws NoSuchMethodException
    {
        return prototypeClass.getMethod(interMethod.getName(), interMethod.getParameterTypes());
    }
}
