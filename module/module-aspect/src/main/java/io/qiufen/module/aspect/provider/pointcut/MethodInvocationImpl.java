package io.qiufen.module.aspect.provider.pointcut;

import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;

class MethodInvocationImpl implements MethodInvocation
{
    private final Object target;
    private final Method method;
    private final Iterator<Advice> adviceIterator;
    private final Object[] args;

    MethodInvocationImpl(Object target, Method method, Iterator<Advice> adviceIterator, Object[] args)
    {
        this.target = target;
        this.method = method;
        this.adviceIterator = adviceIterator;
        this.args = args;
    }

    @Override
    public Method getMethod()
    {
        return method;
    }

    @Override
    public Object[] getArguments()
    {
        return args;
    }

    @Override
    public Object proceed() throws Throwable
    {
        if (adviceIterator.hasNext())
        {
            MethodInterceptor interceptor = (MethodInterceptor) adviceIterator.next();
            return interceptor.invoke(this);
        }
        try
        {
            return method.invoke(target, args);
        }
        catch (InvocationTargetException e)
        {
            throw e.getTargetException();
        }
    }

    @Override
    public Object getThis()
    {
        return target;
    }

    @Override
    public AccessibleObject getStaticPart()
    {
        return method;
    }
}
