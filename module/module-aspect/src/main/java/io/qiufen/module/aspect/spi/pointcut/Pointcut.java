package io.qiufen.module.aspect.spi.pointcut;

import org.aopalliance.aop.Advice;

public interface Pointcut
{
    void weave(Advice advice);
}
