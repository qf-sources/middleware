package io.qiufen.module.launcher.provider.context;

import io.qiufen.module.kernel.spi.context.Context;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.launcher.provider.exception.ContextException;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * 上下内容环境工厂默认实现
 */
public class SimpleContextFactory implements ContextFactory
{
    private final Map<Class<?>, Object> contexts;

    protected SimpleContextFactory()
    {
        this.contexts = new IdentityHashMap<Class<?>, Object>();
    }

    @Override
    public <C extends Context> void addContext(Class<C> clazz, C context)
    {
        if (this.contexts.containsKey(clazz))
        {
            throw new ContextException("Duplicate add context,class:" + clazz.getName());
        }
        this.contexts.put(clazz, context);
    }

    @Override
    public <C extends Context> C getContext(Class<C> clazz)
    {
        Object o = this.contexts.get(clazz);
        if (o == null) throw new ContextException("Not found context,class:" + clazz.getName());
        return clazz.cast(o);
    }
}
