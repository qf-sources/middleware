package io.qiufen.module.launcher.provider;

import io.qiufen.module.kernel.api.module.ModuleLifeCycle;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.Facade;
import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.FeatureHandler;
import io.qiufen.module.kernel.spi.feature.Handler;
import io.qiufen.module.launcher.api.facade.FacadeFinder;
import io.qiufen.module.launcher.provider.common.Constants;
import io.qiufen.module.launcher.provider.context.SimpleContextFactory;
import io.qiufen.module.launcher.provider.exception.LauncherException;
import io.qiufen.module.launcher.provider.facade.FacadeManager;
import io.qiufen.module.launcher.provider.loader.AnnotationModuleLoader;
import io.qiufen.module.launcher.provider.loader.BuiltinModuleData;
import io.qiufen.module.launcher.provider.loader.BuiltinModuleLoader;
import io.qiufen.module.launcher.provider.loader.ModuleStarter;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.api.declaration.ObjectDeclaration;
import io.qiufen.module.object.api.facade.ObjectFacade;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.spi.declaration.Declaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * 启动器
 */
@SuppressWarnings("rawtypes")
public class Launcher
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);

    /**
     * 上下内容环境工厂
     */
    private ContextFactory contextFactory;

    /**
     * 对象上下内容环境
     */
    private ObjectContext objectContext;

    /**
     * 门面管理器
     */
    private FacadeManager facadeManager;

    private volatile boolean initialized = false;

    private Map<Class<?>, FeatureHandler> tempFeatureHandlers;

    private List<Class<?>> tempModuleClasses;

    private Map<Object, Object> tempModuleMap = new HashMap<Object, Object>();

    private AnnotationModuleLoader annotationModuleLoader;
    private BuiltinModuleLoader builtinModuleLoader;

    public Launcher() {}

    public String getVersion()
    {
        return Constants.VERSION_VALUE;
    }

    /**
     * 添加模块
     */
    public final Launcher addModule(Class<?> moduleClass) throws LauncherException
    {
        if (this.initialized)
        {
            throw new LauncherException("Launcher has started, not allow to add addition module.", null);
        }

        if (tempModuleClasses == null) tempModuleClasses = new ArrayList<Class<?>>();
        tempModuleClasses.add(moduleClass);
        return this;
    }

    /**
     * 启动
     */
    public Launcher start() throws LauncherException
    {
        if (this.initialized) throw new LauncherException("Module system has been started.", null);

        synchronized (this)
        {
            if (this.initialized) throw new LauncherException("Module system has been started.", null);

            LOGGER.info("Starting module system, version:{}", getVersion());
            System.setProperty(Constants.VERSION_NAME, getVersion());

            initLauncher();

            tempFeatureHandlers = new IdentityHashMap<Class<?>, FeatureHandler>();

            ModuleStarter moduleStarter = new PrivateModuleStarter();
            //加载内建模块
            builtinModuleLoader = new BuiltinModuleLoader(moduleStarter);
            Iterator<BuiltinModuleData> builtinDependencies = builtinModuleLoader.buildDependencies();
            while (builtinDependencies.hasNext())
            {
                builtinModuleLoader.load(builtinDependencies.next());
            }

            annotationModuleLoader = new AnnotationModuleLoader(moduleStarter);
            if (tempModuleClasses != null)
            {
                for (Class<?> clazz : tempModuleClasses)
                {
                    annotationModuleLoader.load(clazz);
                }
            }

            LOGGER.info("Module system started.");
            this.initialized = true;

            clearTemp();
        }
        return this;
    }

    public final FacadeFinder getFacadeFinder()
    {
        return this.objectContext.getObject(FacadeFinder.class);
    }

    private void initLauncher()
    {
        //初始化上下内容环境工厂
        this.contextFactory = new SimpleContextFactory() {};
        //注册对象内容环境
        this.objectContext = new ObjectContext();
        this.contextFactory.addContext(ObjectContext.class, this.objectContext);

        //初始化门面管理器
        this.facadeManager = new FacadeManager();
        //注册私有化对象门面
        this.facadeManager.addFacade(ObjectFacade.class, new PrivateObjectFacade());
        //注册私有化门面搜索器
        this.objectContext.addDeclaration(new ObjectDeclaration(new PrivateFacadeFinder()));
    }

    private void clearTemp()
    {
        if (tempFeatureHandlers != null)
        {
            tempFeatureHandlers.clear();
            tempFeatureHandlers = null;
        }
        if (tempModuleClasses != null)
        {
            tempModuleClasses.clear();
            tempModuleClasses = null;
        }
        tempModuleMap.clear();
        tempModuleMap = null;

        annotationModuleLoader = null;
        builtinModuleLoader = null;
    }

    @SuppressWarnings({"unchecked"})
    private void startModule0(Object module) throws Exception
    {
        List<FeatureHandler> featureHandlerList = new ArrayList<FeatureHandler>();
        //check features
        Class<?>[] interClasses = module.getClass().getInterfaces();
        for (Class<?> interClazz : interClasses)
        {
            if (!Feature.class.isAssignableFrom(interClazz))
            {
                continue;
            }
            Handler handlerAnn = interClazz.getAnnotation(Handler.class);
            if (handlerAnn == null)
            {
                continue;
            }
            FeatureHandler featureHandler = getFeatureHandler(handlerAnn.value());
            featureHandler.doHandle((Feature) module, contextFactory);
            featureHandlerList.add(featureHandler);
        }
        //post module
        for (FeatureHandler featureHandler : featureHandlerList)
        {
            featureHandler.postModule((Feature) module, contextFactory);
        }
    }

    private FeatureHandler getFeatureHandler(Class<? extends FeatureHandler> clazz)
    {
        if (this.tempFeatureHandlers.containsKey(clazz))
        {
            return this.tempFeatureHandlers.get(clazz);
        }

        FeatureHandler featureHandler;
        if (!objectContext.hasDeclaration(clazz))
        {
            ClassDeclaration declaration = new ClassDeclaration(clazz);
            declaration.setScope(ObjectScope.ONCE);
            objectContext.addDeclaration(declaration);
        }
        featureHandler = objectContext.getObject(clazz);
        featureHandler.initFeature(this.contextFactory, facadeManager);
        this.tempFeatureHandlers.put(clazz, featureHandler);
        return featureHandler;
    }

    /**
     * 私有化的门面搜索器实现
     */
    private class PrivateFacadeFinder implements FacadeFinder
    {
        @Override
        public ObjectFacade getObjectFacade()
        {
            return Launcher.this.facadeManager.getObjectFacade();
        }

        @Override
        public <F extends Facade> F getFacade(Class<F> clazz)
        {
            return Launcher.this.facadeManager.getFacade(clazz);
        }
    }

    /**
     * 私有化的对象门面实现
     */
    private class PrivateObjectFacade implements ObjectFacade
    {
        @Override
        public Object getObject(Object id)
        {
            return objectContext.getObject(id);
        }

        @Override
        public <T> T getObject(Object id, Class<T> clazz)
        {
            return objectContext.getObject(id, clazz);
        }

        @Override
        public <T> T getObject(Class<T> clazz)
        {
            return objectContext.getObject(clazz);
        }

        @Override
        public <T> Set<T> getObjectOfType(Class<T> clazz)
        {
            return objectContext.getObjectOfType(clazz);
        }

        @Override
        public void addDeclaration(Declaration declaration)
        {
            objectContext.addDeclaration(declaration);
        }

        @Override
        public boolean hasDeclaration(Class<?> clazz)
        {
            return objectContext.hasDeclaration(clazz);
        }
    }

    private class PrivateModuleStarter implements ModuleStarter
    {
        @Override
        public boolean hasLoad(Class<?> moduleClazz)
        {
            return tempModuleMap.containsKey(moduleClazz);
        }

        @Override
        public void start(Class<?> moduleClazz)
        {
            ClassDeclaration declaration = new ClassDeclaration(moduleClazz);
            declaration.setScope(ObjectScope.ONCE);
            objectContext.addDeclaration(declaration);
            Object module;
            try
            {
                module = objectContext.getObject(moduleClazz);
            }
            catch (Exception e)
            {
                throw new LauncherException(e, moduleClazz);
            }
            tempModuleMap.put(moduleClazz, module);

            if (module instanceof ModuleLifeCycle)
            {
                ((ModuleLifeCycle) module).onLoad();
            }

            try
            {
                startModule0(module);
            }
            catch (LauncherException e)
            {
                if (module instanceof ModuleLifeCycle)
                {
                    ((ModuleLifeCycle) module).onLoadFailure(e);
                }
                throw e;
            }
            catch (Exception e)
            {
                if (module instanceof ModuleLifeCycle)
                {
                    ((ModuleLifeCycle) module).onLoadFailure(e);
                }
                throw new LauncherException(e, moduleClazz);
            }

            if (module instanceof ModuleLifeCycle)
            {
                ((ModuleLifeCycle) module).onLoadSuccess();
            }
        }
    }
}
