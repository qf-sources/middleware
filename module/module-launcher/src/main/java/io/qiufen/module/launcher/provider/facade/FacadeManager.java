package io.qiufen.module.launcher.provider.facade;

import io.qiufen.module.kernel.spi.facade.Facade;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;
import io.qiufen.module.launcher.api.facade.FacadeFinder;
import io.qiufen.module.object.api.facade.ObjectFacade;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * Created by zhangruzheng on 2021/11/13.
 */
public class FacadeManager implements FacadeFinder, FacadeRegistry
{
    private final Map<Class<?>, Facade> facades = new IdentityHashMap<Class<?>, Facade>();

    @Override
    public ObjectFacade getObjectFacade()
    {
        return getFacade(ObjectFacade.class);
    }

    @Override
    public <F extends Facade> F getFacade(Class<F> clazz)
    {
        Object o = facades.get(clazz);
        if (o == null)
        {
            throw new FacadeException("Maybe feature not implemented!type=" + clazz.getName());
        }
        return clazz.cast(o);
    }

    @Override
    public <F extends Facade> void addFacade(Class<F> clazz, F facade)
    {
        this.facades.put(clazz, facade);
    }
}
