package io.qiufen.module.launcher.provider.exception;

import io.qiufen.module.kernel.spi.exception.ModuleException;

public class ContextException extends ModuleException
{
    public ContextException(String message)
    {
        super(message);
    }
}
