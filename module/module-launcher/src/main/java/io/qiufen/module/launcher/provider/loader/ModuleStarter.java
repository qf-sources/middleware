package io.qiufen.module.launcher.provider.loader;

public interface ModuleStarter
{
    boolean hasLoad(Class<?> moduleClazz);

    void start(Class<?> moduleClazz);
}
