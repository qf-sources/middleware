package io.qiufen.module.launcher.provider.exception;

import io.qiufen.module.kernel.spi.exception.ModuleException;

public class LauncherException extends ModuleException
{
    private final Class<?> moduleClass;

    public LauncherException(String message, Class<?> moduleClass)
    {
        super(message);
        this.moduleClass = moduleClass;
    }

    public LauncherException(Exception e, Class<?> moduleClass)
    {
        super(e);
        this.moduleClass = moduleClass;
    }

    @Override
    public String getMessage()
    {
        if (this.moduleClass == null) return super.getMessage();

        return '[' + this.moduleClass.getName() + ']' + super.getMessage();
    }

    public Class<?> getModuleClass()
    {
        return moduleClass;
    }
}
