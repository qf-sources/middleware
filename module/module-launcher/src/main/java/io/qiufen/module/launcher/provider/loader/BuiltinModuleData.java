package io.qiufen.module.launcher.provider.loader;

import java.util.ArrayList;
import java.util.List;

public class BuiltinModuleData
{
    private final Class<?> clazz;
    private boolean rootNode = true;
    private List<BuiltinModuleData> dependencies;

    public BuiltinModuleData(Class<?> clazz)
    {
        this.clazz = clazz;
    }

    public Class<?> getClazz()
    {
        return clazz;
    }

    public boolean isRootNode()
    {
        return rootNode;
    }

    public void setRootNode(boolean rootNode)
    {
        this.rootNode = rootNode;
    }

    public List<BuiltinModuleData> getDependencies()
    {
        return dependencies;
    }

    public void addDependency(BuiltinModuleData m)
    {
        if (dependencies == null) dependencies = new ArrayList<BuiltinModuleData>();
        dependencies.add(m);
    }
}
