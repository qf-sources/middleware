package io.qiufen.module.launcher.provider.facade;

import io.qiufen.module.kernel.spi.exception.ModuleException;

public class FacadeException extends ModuleException
{
    public FacadeException(String message)
    {
        super(message);
    }
}
