package io.qiufen.module.launcher.provider.loader;

import io.qiufen.module.kernel.api.module.RequireModule;

public class AnnotationModuleLoader extends ModuleLoader<Class<?>>
{
    public AnnotationModuleLoader(ModuleStarter moduleStarter)
    {
        super(moduleStarter);
    }

    @Override
    protected void doDependencies(Class<?> d, ModuleLoader<Class<?>> moduleLoader)
    {
        RequireModule ann0 = d.getAnnotation(RequireModule.class);
        if (ann0 == null) return;

        Class<?>[] importModuleClasses = ann0.value();
        for (Class<?> importModuleClass : importModuleClasses)
        {
            moduleLoader.load(importModuleClass);
        }
    }

    @Override
    protected Class<?> getModuleClass(Class<?> d)
    {
        return d;
    }
}
