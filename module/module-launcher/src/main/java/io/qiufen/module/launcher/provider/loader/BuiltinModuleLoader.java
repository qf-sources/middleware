package io.qiufen.module.launcher.provider.loader;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.lang.JsonObject;
import io.qiufen.module.launcher.provider.exception.BuiltinException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

public class BuiltinModuleLoader extends ModuleLoader<BuiltinModuleData>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(BuiltinModuleLoader.class);

    private static final String BUILTIN_CONFIG_FILE = "META-INF/module/buildin.json";

    public BuiltinModuleLoader(ModuleStarter moduleStarter)
    {
        super(moduleStarter);
    }

    private static JsonObject parseConfigFile(URL url)
    {
        try
        {
            LOGGER.debug("Use config file:{}", url.getPath());
            InputStream stream = url.openStream();
            JsonObject config = JsonObject.of(new DataInputStream(stream));
            stream.close();
            return config;
        }
        catch (Exception e)
        {
            throw new BuiltinException(url, e);
        }
    }

    @Override
    protected void doDependencies(BuiltinModuleData builtinModuleData, ModuleLoader<BuiltinModuleData> moduleLoader)
    {
        List<BuiltinModuleData> dependencies = builtinModuleData.getDependencies();
        if (CollectionUtil.isEmpty(dependencies)) return;

        for (BuiltinModuleData dependency : dependencies)
        {
            moduleLoader.load(dependency);
        }
    }

    @Override
    protected Class<?> getModuleClass(BuiltinModuleData builtinModuleData)
    {
        return builtinModuleData.getClazz();
    }

    public Iterator<BuiltinModuleData> buildDependencies()
    {
        Map<String, BuiltinModuleData> mapping = new HashMap<String, BuiltinModuleData>();
        try
        {
            Enumeration<URL> configs = getClass().getClassLoader().getResources(BUILTIN_CONFIG_FILE);
            while (configs.hasMoreElements())
            {
                JsonObject config = parseConfigFile(configs.nextElement());
                putConfigIntoMapping(mapping, config);
            }
        }
        catch (BuiltinException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new BuiltinException(e);
        }
        return new PrivateIterator(mapping);
    }

    private void putConfigIntoMapping(Map<String, BuiltinModuleData> mapping, JsonObject config)
            throws ClassNotFoundException
    {
        String mainModule = config.getString("mainModule");
        BuiltinModuleData builtinModuleData = mapping.get(mainModule);
        List<JsonObject> requireModules = config.getArray("requireModules");
        if (builtinModuleData == null)
        {
            builtinModuleData = new BuiltinModuleData(Class.forName(mainModule));
            mapping.put(mainModule, builtinModuleData);
        }
        for (JsonObject o : requireModules)
        {
            String requireModule = o.to(String.class);
            BuiltinModuleData require = mapping.get(requireModule);
            if (require == null)
            {
                require = new BuiltinModuleData(Class.forName(requireModule));
                mapping.put(requireModule, require);
            }
            require.setRootNode(false);
            builtinModuleData.addDependency(require);
        }
    }

    private static class PrivateIterator implements Iterator<BuiltinModuleData>
    {
        private final Iterator<Map.Entry<String, BuiltinModuleData>> iterator;

        private BuiltinModuleData current;

        private PrivateIterator(Map<String, BuiltinModuleData> mapping)
        {
            this.iterator = mapping.entrySet().iterator();
        }

        @Override
        public boolean hasNext()
        {
            while (true)
            {
                if (!iterator.hasNext())
                {
                    return false;
                }
                BuiltinModuleData data = iterator.next().getValue();
                if (data.isRootNode())
                {
                    this.current = data;
                    return true;
                }
            }
        }

        @Override
        public BuiltinModuleData next() {return current;}

        @Override
        public void remove() {}
    }
}

