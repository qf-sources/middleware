package io.qiufen.module.launcher.api.facade;

import io.qiufen.module.object.api.facade.ObjectFacade;

/**
 * 门面接口搜查器
 *
 * @since 1.0
 */
public interface FacadeFinder extends io.qiufen.module.kernel.spi.facade.FacadeFinder
{
    /**
     * 取得对象门面接口
     */
    ObjectFacade getObjectFacade();
}
