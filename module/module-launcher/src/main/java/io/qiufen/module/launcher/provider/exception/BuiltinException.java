package io.qiufen.module.launcher.provider.exception;

import io.qiufen.module.kernel.spi.exception.ModuleException;

import java.net.URL;

public class BuiltinException extends ModuleException
{
    private final URL configFileURL;

    public BuiltinException(Throwable cause)
    {
        this(null, cause);
    }

    public BuiltinException(URL configFileURL, Throwable cause)
    {
        super(cause);
        this.configFileURL = configFileURL;
    }

    @Override
    public String getMessage()
    {
        if (this.configFileURL == null) return super.getMessage();

        return "Buildin failure in file:" + this.configFileURL.getPath() + ", detail: " + super.getMessage();
    }

    public URL getConfigFileURL()
    {
        return configFileURL;
    }
}
