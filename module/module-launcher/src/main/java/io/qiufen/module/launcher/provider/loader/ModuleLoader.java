package io.qiufen.module.launcher.provider.loader;

public abstract class ModuleLoader<D>
{
    private final ModuleStarter moduleStarter;

    protected ModuleLoader(ModuleStarter moduleStarter)
    {
        this.moduleStarter = moduleStarter;
    }

    public final void load(D d)
    {
        Class<?> moduleClazz = getModuleClass(d);
        if (moduleStarter.hasLoad(moduleClazz)) return;

        doDependencies(d, this);

        moduleStarter.start(moduleClazz);
    }

    protected abstract void doDependencies(D d, ModuleLoader<D> moduleLoader);

    protected abstract Class<?> getModuleClass(D d);
}
