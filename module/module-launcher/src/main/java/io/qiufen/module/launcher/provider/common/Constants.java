package io.qiufen.module.launcher.provider.common;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:22
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Constants
{
    public static final String VERSION_NAME = "qiufen.module.version";

    public static final String VERSION_VALUE = "0.0.1";
}