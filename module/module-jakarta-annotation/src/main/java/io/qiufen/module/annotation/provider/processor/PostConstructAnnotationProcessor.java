package io.qiufen.module.annotation.provider.processor;

import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.processor.spi.ProcessorAdapter;
import jakarta.annotation.PostConstruct;

import java.lang.reflect.Method;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/11 15:27
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class PostConstructAnnotationProcessor extends ProcessorAdapter
{
    @Override
    public void doProcess(Object prototype, Object decorated, FacadeFinder finder) throws Throwable
    {
        Method[] methods = prototype.getClass().getMethods();
        if (methods.length <= 0)
        {
            return;
        }

        for (Method method : methods)
        {
            if (method.isAnnotationPresent(PostConstruct.class))
            {
                method.invoke(prototype);
            }
        }
    }
}