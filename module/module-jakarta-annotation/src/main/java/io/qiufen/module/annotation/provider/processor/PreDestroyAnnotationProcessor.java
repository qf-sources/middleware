package io.qiufen.module.annotation.provider.processor;

import io.qiufen.module.processor.spi.ProcessorAdapter;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/11 15:27
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class PreDestroyAnnotationProcessor extends ProcessorAdapter
{
    @Override
    public void doDestroy()
    {
        //todo:待完善
    }
}