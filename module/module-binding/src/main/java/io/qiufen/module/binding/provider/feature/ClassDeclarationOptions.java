package io.qiufen.module.binding.provider.feature;

import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;

public class ClassDeclarationOptions
{
    private final ClassDeclaration declaration;

    ClassDeclarationOptions(ClassDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public ClassDeclarationOptions id(Object... id)
    {
        this.declaration.setIdList(id);
        return this;
    }

    public ClassDeclarationOptions type(Class<?>... type)
    {
        this.declaration.setTypeList(type);
        return this;
    }

    public ClassDeclarationOptions lazy(boolean lazy)
    {
        this.declaration.setLazy(lazy);
        return this;
    }

    public ClassDeclarationOptions scope(ObjectScope scope)
    {
        this.declaration.setScope(scope);
        return this;
    }
}
