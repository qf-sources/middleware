package io.qiufen.module.binding.provider.feature;

import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.api.declaration.Builder;

/**
 * Created by zhangruzheng on 2021/11/13.
 */
public class BuilderImpl implements Builder
{
    private final ObjectBuilder builder;

    private FacadeFinder finder;

    BuilderImpl(ObjectBuilder builder)
    {
        this.builder = builder;
    }

    @Override
    public Object build() throws Exception
    {
        return builder.build(this.finder);
    }

    public void setFinder(FacadeFinder finder)
    {
        this.finder = finder;
    }
}
