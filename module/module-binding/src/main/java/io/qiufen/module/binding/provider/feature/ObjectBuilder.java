package io.qiufen.module.binding.provider.feature;

import io.qiufen.module.kernel.spi.facade.FacadeFinder;

/**
 * Created by zhangruzheng on 2021/11/13.
 */
public interface ObjectBuilder
{
    Object build(FacadeFinder finder) throws Exception;
}
