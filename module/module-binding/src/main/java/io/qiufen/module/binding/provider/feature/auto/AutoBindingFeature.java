package io.qiufen.module.binding.provider.feature.auto;

import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

@Handler(AutoBindingFeatureHandler.class)
public interface AutoBindingFeature extends Feature {}
