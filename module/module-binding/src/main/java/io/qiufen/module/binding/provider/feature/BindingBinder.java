package io.qiufen.module.binding.provider.feature;

import io.qiufen.module.kernel.spi.feature.Binder;
import io.qiufen.module.object.api.declaration.BuilderDeclaration;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.api.declaration.ObjectDeclaration;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.ArrayList;
import java.util.List;

public class BindingBinder extends Binder
{
    private List<Declaration> declarations;

    BindingBinder()
    {
    }

    public ObjectDeclarationOptions bind(Object object)
    {
        ObjectDeclaration declaration = new ObjectDeclaration(object);
        this.useList().add(declaration);
        return new ObjectDeclarationOptions(declaration);
    }

    public ClassDeclarationOptions bind(Class<?> clazz)
    {
        ClassDeclaration declaration = new ClassDeclaration(clazz);
        this.useList().add(declaration);
        return new ClassDeclarationOptions(declaration);
    }

    public BuilderDeclarationOptions bind(Class<?> clazz, ObjectBuilder builder)
    {
        BuilderDeclaration declaration = new BuilderDeclaration(clazz, new BuilderImpl(builder));
        this.useList().add(declaration);
        return new BuilderDeclarationOptions(declaration);
    }

    List<Declaration> getDeclarations()
    {
        return this.declarations;
    }

    protected void clear()
    {
        if (this.declarations != null)
        {
            this.declarations.clear();
        }
    }

    private List<Declaration> useList()
    {
        if (this.declarations == null)
        {
            this.declarations = new ArrayList<Declaration>();
        }
        return this.declarations;
    }
}
