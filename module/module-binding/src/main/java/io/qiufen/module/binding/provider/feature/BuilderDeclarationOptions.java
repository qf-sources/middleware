package io.qiufen.module.binding.provider.feature;

import io.qiufen.module.object.api.declaration.BuilderDeclaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;

public class BuilderDeclarationOptions
{
    private final BuilderDeclaration declaration;

    BuilderDeclarationOptions(BuilderDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public BuilderDeclarationOptions id(Object... id)
    {
        this.declaration.setIdList(id);
        return this;
    }

    public BuilderDeclarationOptions type(Class<?>... type)
    {
        this.declaration.setTypeList(type);
        return this;
    }

    public BuilderDeclarationOptions lazy(boolean lazy)
    {
        this.declaration.setLazy(lazy);
        return this;
    }

    public BuilderDeclarationOptions scope(ObjectScope scope)
    {
        this.declaration.setScope(scope);
        return this;
    }
}
