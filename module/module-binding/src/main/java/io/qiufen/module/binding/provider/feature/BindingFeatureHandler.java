package io.qiufen.module.binding.provider.feature;

import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;
import io.qiufen.module.kernel.spi.feature.BindableFeatureHandler;
import io.qiufen.module.object.api.declaration.BuilderDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.List;

/**
 * Created by zhangruzheng on 2021/11/9.
 */
public class BindingFeatureHandler extends BindableFeatureHandler<BindingBinder, BindingFeature>
{
    private FacadeFinder finder;

    @Override
    public void initFeature(ContextFactory contextFactory, FacadeRegistry facadeRegistry)
    {
        this.finder = facadeRegistry;
    }

    @Override
    protected BindingBinder initBinder(BindingFeature feature, ContextFactory contextFactory)
    {
        return new BindingBinder();
    }

    @Override
    protected void doHandle(BindingFeature feature, ContextFactory contextFactory, BindingBinder binder)
    {
        feature.bind(binder);
        List<Declaration> declarations = binder.getDeclarations();
        if (declarations != null && !declarations.isEmpty())
        {
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            for (Declaration declaration : declarations)
            {
                if (declaration instanceof BuilderDeclaration)
                {
                    BuilderImpl impl = (BuilderImpl) ((BuilderDeclaration) declaration).getBuilder();
                    impl.setFinder(this.finder);
                }
                objectContext.addDeclaration(declaration);
            }
        }
    }

    @Override
    protected void postModule(BindingFeature feature, ContextFactory contextFactory, BindingBinder binder)
    {
        List<Declaration> declarations = binder.getDeclarations();
        if (declarations != null)
        {
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            for (Declaration declaration : declarations)
            {
                objectContext.build(declaration);
            }
        }
    }
}
