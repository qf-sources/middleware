package io.qiufen.module.binding.provider.auto;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.module.binding.spi.auto.Resource;
import io.qiufen.module.binding.spi.auto.ResourceProcessor;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.spi.declaration.Declaration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class AnnotationAutoBindingProcessor implements ResourceProcessor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AnnotationAutoBindingProcessor.class);

    private static final String CLASS_SUFFIX = ".class";
    private static final Pattern ANONYMOUS_INNER_PATTERN = Pattern.compile("\\$(\\d+).", Pattern.CASE_INSENSITIVE);

    private final Class<? extends Annotation>[] annotations;

    private List<Declaration> declarations;

    public AnnotationAutoBindingProcessor(Class<? extends Annotation>... annotations)
    {
        this.annotations = annotations;
    }

    @Override
    public void doProcess(Resource resource, ContextFactory contextFactory) throws Exception
    {
        String name = resource.getName();
        LOGGER.debug("Process resource:{}", name);
        int suffixIndex = name.indexOf(CLASS_SUFFIX);
        if (suffixIndex < 0) return;

        StringBuilder builder = new StringBuilder().append(name, 0, suffixIndex);
        int classesIndex = builder.indexOf("classes");
        if (classesIndex >= 0)
        {
            builder.delete(0, classesIndex + 8);
            for (int i = 0; i < builder.length(); i++)
            {
                char ch = builder.charAt(i);
                if (ch == '/' || ch == '\\')
                {
                    builder.setCharAt(i, '.');
                }
            }
        }
        else
        {
            for (int i = 0; i < builder.length(); i++)
            {
                if (builder.charAt(i) == '/')
                {
                    builder.setCharAt(i, '.');
                }
            }
        }

        if (ANONYMOUS_INNER_PATTERN.matcher(builder).find()) return;
        String className = builder.toString();
        Class<?> clazz = Class.forName(className);
        for (Class<? extends Annotation> annotation : annotations)
        {
            if (clazz.isAnnotationPresent(annotation))
            {
                ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
                Declaration declaration = new ClassDeclaration(clazz);
                objectContext.addDeclaration(declaration);

                if (this.declarations == null) this.declarations = new ArrayList<Declaration>();
                this.declarations.add(declaration);
                break;
            }
        }
    }

    @Override
    public void doPostProcess(ContextFactory contextFactory)
    {
        if (CollectionUtil.isEmpty(this.declarations)) return;

        ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
        for (Declaration declaration : this.declarations)
        {
            objectContext.build(declaration);
        }
        this.declarations.clear();
    }
}
