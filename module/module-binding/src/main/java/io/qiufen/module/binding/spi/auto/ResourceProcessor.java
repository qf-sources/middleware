package io.qiufen.module.binding.spi.auto;

import io.qiufen.module.kernel.spi.context.ContextFactory;

public interface ResourceProcessor
{
    void doProcess(Resource resource, ContextFactory contextFactory) throws Exception;

    void doPostProcess(ContextFactory contextFactory);
}
