package io.qiufen.module.binding.provider.feature.auto;

import io.qiufen.module.binding.spi.auto.ResourceProcessor;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.feature.FeatureHandlerAdapter;
import io.qiufen.module.object.provider.context.ObjectContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutoBindingFeatureHandler extends FeatureHandlerAdapter<AutoBindingFeature>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AutoBindingFeatureHandler.class);

    private final ClassPathResourceScanner scanner = new ClassPathResourceScanner();

    @Override
    public void doHandle(AutoBindingFeature feature, ContextFactory contextFactory) throws Exception
    {
        ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
        ResourceProcessor processor = objectContext.getObject(ResourceProcessor.class);

        String basePackage = feature.getClass().getName().replaceAll("\\.\\w+$", "");
        LOGGER.info("Start binding for package:{}", basePackage);
        scanner.findResource(basePackage, processor, contextFactory);
    }

    @Override
    public void postModule(AutoBindingFeature feature, ContextFactory contextFactory)
    {
        ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
        ResourceProcessor processor = objectContext.getObject(ResourceProcessor.class);
        processor.doPostProcess(contextFactory);
    }
}

