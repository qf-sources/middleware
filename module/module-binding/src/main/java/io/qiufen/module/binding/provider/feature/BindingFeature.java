package io.qiufen.module.binding.provider.feature;

import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

/**
 * 模块对象绑定特性
 */
@Handler(BindingFeatureHandler.class)
public interface BindingFeature extends Feature
{
    void bind(BindingBinder binder);
}
