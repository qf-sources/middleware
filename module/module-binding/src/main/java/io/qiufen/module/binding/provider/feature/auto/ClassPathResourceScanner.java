package io.qiufen.module.binding.provider.feature.auto;

import io.qiufen.module.binding.spi.auto.Resource;
import io.qiufen.module.binding.spi.auto.ResourceProcessor;
import io.qiufen.module.kernel.spi.context.ContextFactory;

import java.io.*;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

class ClassPathResourceScanner
{
    void findResource(String packageName, ResourceProcessor processor, ContextFactory contextFactory) throws IOException
    {
        if (packageName.endsWith(".")) packageName = packageName.substring(0, packageName.length() - 1);

        String path = packageName.replace(".", "/");
        Enumeration<URL> urls = findAllClassPathResources(path);
        while (urls.hasMoreElements())
        {
            URL url = urls.nextElement();
            String protocol = url.getProtocol();
            if ("file".equals(protocol))
            {
                String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                File file = new File(filePath);
                listFile(file, processor, contextFactory);
            }
            else if ("jar".equals(protocol))
            {
                listJarFile(url, processor, contextFactory);
            }
        }
    }

    private void listFile(File file, ResourceProcessor processor, ContextFactory contextFactory)
    {
        if (file.isFile())
        {
            try
            {
                processor.doProcess(new FileResource(file), contextFactory);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Process failure:" + file.getPath(), e);
            }
            return;
        }

        if (file.isDirectory())
        {
            File[] subFiles = file.listFiles();
            if (subFiles == null || subFiles.length <= 0) return;
            for (File subFile : subFiles)
            {
                listFile(subFile, processor, contextFactory);
            }
        }
    }

    private void listJarFile(URL url, ResourceProcessor processor, ContextFactory contextFactory) throws IOException
    {
        JarFile jar = ((JarURLConnection) url.openConnection()).getJarFile();
        Enumeration<JarEntry> entries = jar.entries();
        while (entries.hasMoreElements())
        {
            JarEntry entry = entries.nextElement();
            if (entry.isDirectory())
            {
                continue;
            }
            try
            {
                processor.doProcess(new JarFileEntryResource(jar, entry), contextFactory);
            }
            catch (Exception e)
            {
                throw new RuntimeException("Process failure:" + entry.getName(), e);
            }
        }
    }

    private Enumeration<URL> findAllClassPathResources(String path) throws IOException
    {
        if (path.startsWith("/")) path = path.substring(1);
        return getClass().getClassLoader().getResources(path);
    }

    private static class FileResource implements Resource
    {
        private final File file;

        private FileResource(File file)
        {
            this.file = file;
        }

        @Override
        public String getName()
        {
            return file.getPath();
        }

        @Override
        public InputStream openStream() throws FileNotFoundException
        {
            return new FileInputStream(file);
        }
    }

    private static class JarFileEntryResource implements Resource
    {
        private final JarFile jarFile;
        private final JarEntry jarEntry;

        private JarFileEntryResource(JarFile jarFile, JarEntry jarEntry)
        {
            this.jarFile = jarFile;
            this.jarEntry = jarEntry;
        }

        @Override
        public String getName()
        {
            return jarEntry.getName();
        }

        @Override
        public InputStream openStream() throws IOException
        {
            return jarFile.getInputStream(jarEntry);
        }
    }
}
