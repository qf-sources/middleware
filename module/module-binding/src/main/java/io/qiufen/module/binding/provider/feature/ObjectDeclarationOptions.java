package io.qiufen.module.binding.provider.feature;

import io.qiufen.module.object.api.declaration.ObjectDeclaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ObjectDeclarationOptions
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectDeclarationOptions.class);

    private final ObjectDeclaration declaration;

    ObjectDeclarationOptions(ObjectDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public ObjectDeclarationOptions id(Object... id)
    {
        this.declaration.setIdList(id);
        return this;
    }

    public ObjectDeclarationOptions type(Class<?>... type)
    {
        this.declaration.setTypeList(type);
        return this;
    }

    public ObjectDeclarationOptions scope(ObjectScope scope)
    {
        if (scope == ObjectScope.PROTOTYPE)
        {
            LOGGER.warn("Unsupported operation,scope:{}", scope.name());
        }
        this.declaration.setScope(scope);
        return this;
    }
}
