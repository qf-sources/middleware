package io.qiufen.module.binding.spi.auto;

import java.io.IOException;
import java.io.InputStream;

public interface Resource
{
    String getName();

    InputStream openStream() throws IOException;
}
