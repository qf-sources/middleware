package io.qiufen.module.processor.provider.feature;

import io.qiufen.module.kernel.provider.priority.PriorityUtil;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;
import io.qiufen.module.kernel.spi.feature.BindableFeatureHandler;
import io.qiufen.module.kernel.spi.priority.Priority;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.provider.support.event.ObjectConstructEvent;
import io.qiufen.module.object.spi.declaration.Declaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import io.qiufen.module.object.spi.event.EventHandler;
import io.qiufen.module.object.spi.event.EventHandlerAdapter;
import io.qiufen.module.processor.spi.Processor;

import java.util.*;

public class ProcessorFeatureHandler extends BindableFeatureHandler<ProcessorBinder, ProcessorFeature>
{
    private List<Processor> processorList;

    private FacadeFinder finder;

    @Override
    public void initFeature(ContextFactory contextFactory, FacadeRegistry facadeRegistry)
    {
        this.finder = facadeRegistry;

        EventHandler eventHandler = new EventHandler0();
        ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
        objectContext.addEventHandler(eventHandler);
    }

    @Override
    protected ProcessorBinder initBinder(ProcessorFeature feature, ContextFactory contextFactory)
    {
        return new ProcessorBinder();
    }

    @Override
    protected void doHandle(ProcessorFeature feature, ContextFactory contextFactory, ProcessorBinder binder)
    {
        feature.bind(binder);
        List<ProcessorDeclaration> declarations = binder.getDeclarations();
        if (declarations != null && !declarations.isEmpty())
        {
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            List<ProcessorDeclaration> preList = new ArrayList<ProcessorDeclaration>();
            Iterator<ProcessorDeclaration> iterator = declarations.iterator();
            while (iterator.hasNext())
            {
                ProcessorDeclaration declaration = iterator.next();
                Object processorId = declaration.getProcessorId();
                Class<? extends Processor> clazz = declaration.getProcessorClass();
                if (processorId != null)
                {
                    objectContext.getObject(processorId, Processor.class);
                    if (declaration.getRegistered() == Registered.PRE)
                    {
                        preList.add(declaration);
                        iterator.remove();
                    }
                    continue;
                }

                if (clazz != null)
                {
                    if (!objectContext.hasDeclaration(clazz))
                    {
                        ClassDeclaration classDeclaration = new ClassDeclaration(clazz);
                        classDeclaration.setScope(ObjectScope.ONCE);
                        objectContext.addDeclaration(classDeclaration);
                    }
                    if (declaration.getRegistered() == Registered.PRE)
                    {
                        preList.add(declaration);
                        iterator.remove();
                    }
                    continue;
                }

                throw new IllegalArgumentException("Must provide processor id or processor class at least");
            }

            init(objectContext, preList);
            preList.clear();
        }
    }

    @Override
    protected void postModule(ProcessorFeature feature, ContextFactory contextFactory, ProcessorBinder binder)
    {
        init(contextFactory.getContext(ObjectContext.class), binder.getDeclarations());
    }

    private void init(ObjectContext objectContext, List<ProcessorDeclaration> tempProcessorDeclarations)
    {
        if (tempProcessorDeclarations == null || tempProcessorDeclarations.isEmpty()) return;

        List<Processor> tempProcessorList = new ArrayList<Processor>();
        if (this.processorList != null)
        {
            tempProcessorList.addAll(this.processorList);
        }
        Map<Object, Priority> priorityMap = new HashMap<Object, Priority>();
        for (ProcessorDeclaration declaration : tempProcessorDeclarations)
        {
            Object processorId = declaration.getProcessorId();
            Class<? extends Processor> clazz = declaration.getProcessorClass();
            if (processorId != null)
            {
                Processor processor = objectContext.getObject(processorId, Processor.class);
                tempProcessorList.add(processor);
                if (declaration.getPriority() != null) priorityMap.put(processor, declaration.getPriority());
            }
            else if (clazz != null)
            {
                Processor processor = objectContext.getObject(clazz);
                tempProcessorList.add(processor);
                if (declaration.getPriority() != null) priorityMap.put(processor, declaration.getPriority());
            }
        }

        //根据priority排序
        List<Processor> processors = new ArrayList<Processor>();
        //先自排序
        PriorityUtil.sort(tempProcessorList, processors);

        //按照定义排序
        if (!processors.isEmpty())
        {
            PriorityUtil.sort(processors, priorityMap);
        }
        this.processorList = processors;
    }

    class EventHandler0 extends EventHandlerAdapter
    {
        @Override
        public void onPrepare(Declaration declaration)
        {
            if (processorList == null || processorList.isEmpty())
            {
                return;
            }
            for (Processor processor : processorList)
            {
                processor.doPrepare(declaration);
            }
        }

        @Override
        public void onConstruct(ObjectConstructEvent event) throws Exception
        {
            if (processorList == null || processorList.isEmpty())
            {
                return;
            }
            for (Processor processor : processorList)
            {
                processor.doCreate(event, finder);
            }
        }

        @Override
        public void onCreated(Object prototype, Object decorated) throws Exception
        {
            if (processorList == null || processorList.isEmpty())
            {
                return;
            }
            for (Processor processor : processorList)
            {
                try
                {
                    processor.doProcess(prototype, decorated, finder);
                }
                catch (Throwable throwable)
                {
                    throw new RuntimeException(throwable);
                }
            }
        }
    }
}