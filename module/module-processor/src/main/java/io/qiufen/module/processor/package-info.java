/**
 * 对象处理服务模块
 * feature  - 模块特性<br/>
 * spi      - 客户端扩展接口<br/>
 * priority - 定义优先级<br/>
 */
package io.qiufen.module.processor;