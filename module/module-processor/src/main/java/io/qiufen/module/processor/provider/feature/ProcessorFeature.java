package io.qiufen.module.processor.provider.feature;

import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

/**
 * 模块对象反射特性
 */
@Handler(ProcessorFeatureHandler.class)
public interface ProcessorFeature extends Feature
{
    void bind(ProcessorBinder binder);
}