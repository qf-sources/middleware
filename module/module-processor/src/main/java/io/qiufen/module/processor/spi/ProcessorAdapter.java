package io.qiufen.module.processor.spi;

import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.priority.Priority;
import io.qiufen.module.object.provider.support.event.ObjectConstructEvent;
import io.qiufen.module.object.spi.declaration.Declaration;

/**
 * Created by zhangruzheng on 2021/11/14.
 */
public class ProcessorAdapter implements Processor
{
    @Override
    public void doPrepare(Declaration declaration)
    {
    }

    @Override
    public void doCreate(ObjectConstructEvent event, FacadeFinder finder) throws Exception
    {
    }

    @Override
    public void doProcess(Object prototype, Object decorated, FacadeFinder finder) throws Throwable
    {
    }

    @Override
    public void doDestroy()
    {
    }

    @Override
    public Priority priority()
    {
        return null;
    }
}
