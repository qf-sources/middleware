package io.qiufen.module.processor.spi;

import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.priority.Prioritized;
import io.qiufen.module.object.provider.support.event.ObjectConstructEvent;
import io.qiufen.module.object.spi.declaration.Declaration;

/**
 * 对象处理器
 */
public interface Processor extends Prioritized
{
    void doPrepare(Declaration declaration);

    void doCreate(ObjectConstructEvent event, FacadeFinder finder) throws Exception;

    /**
     * 处理动作
     *
     * @param prototype 对象原型
     * @param decorated
     * @param finder    门面搜索器
     */
    void doProcess(Object prototype, Object decorated, FacadeFinder finder) throws Throwable;

    /**
     * 处理销毁
     */
    void doDestroy();
}