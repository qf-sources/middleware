package io.qiufen.module.processor.provider.feature;

import io.qiufen.module.kernel.spi.priority.Priority;
import io.qiufen.module.processor.spi.Processor;

public class ProcessorDeclaration
{
    private final Object processorId;

    private final Class<? extends Processor> processorClass;

    private Priority priority;

    private Registered registered;

    ProcessorDeclaration(Object processorId, Class<? extends Processor> processorClass)
    {
        this.processorId = processorId;
        this.processorClass = processorClass;
    }

    public Object getProcessorId()
    {
        return processorId;
    }

    public Class<? extends Processor> getProcessorClass()
    {
        return processorClass;
    }

    public Priority getPriority()
    {
        return priority;
    }

    public void setPriority(Priority priority)
    {
        this.priority = priority;
    }

    public Registered getRegistered()
    {
        return registered;
    }

    public void setRegistered(Registered registered)
    {
        this.registered = registered;
    }
}
