package io.qiufen.module.processor.provider.feature;

import io.qiufen.module.kernel.spi.priority.Priority;

public class ProcessorDeclarationOptions
{
    private final ProcessorDeclaration declaration;

    ProcessorDeclarationOptions(ProcessorDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public ProcessorDeclarationOptions after(Class<?>... classes)
    {
        usePriority().after(classes);
        return this;
    }

    public ProcessorDeclarationOptions before(Class<?>... classes)
    {
        usePriority().before(classes);
        return this;
    }

    public ProcessorDeclarationOptions at(Registered registered)
    {
        this.declaration.setRegistered(registered);
        return this;
    }

    private Priority usePriority()
    {
        Priority priority = this.declaration.getPriority();
        if (priority == null)
        {
            priority = new Priority();
            this.declaration.setPriority(priority);
        }
        return priority;
    }
}
