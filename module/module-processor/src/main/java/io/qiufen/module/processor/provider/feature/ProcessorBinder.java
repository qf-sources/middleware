package io.qiufen.module.processor.provider.feature;

import io.qiufen.module.kernel.spi.feature.Binder;
import io.qiufen.module.processor.spi.Processor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangruzheng on 2021/11/11.
 */
public class ProcessorBinder extends Binder
{
    private List<ProcessorDeclaration> declarations;

    ProcessorBinder()
    {
    }

    public ProcessorDeclarationOptions bind(Object processorId)
    {
        ProcessorDeclaration declaration = new ProcessorDeclaration(processorId, null);
        useList().add(declaration);
        return new ProcessorDeclarationOptions(declaration);
    }

    public ProcessorDeclarationOptions bind(Class<? extends Processor> processorClass)
    {
        ProcessorDeclaration declaration = new ProcessorDeclaration(null, processorClass);
        useList().add(declaration);
        return new ProcessorDeclarationOptions(declaration);
    }

    List<ProcessorDeclaration> getDeclarations()
    {
        return declarations;
    }

    protected void clear()
    {
        if (this.declarations != null)
        {
            this.declarations.clear();
        }
    }

    private List<ProcessorDeclaration> useList()
    {
        if (this.declarations == null)
        {
            this.declarations = new ArrayList<ProcessorDeclaration>();
        }
        return this.declarations;
    }
}
