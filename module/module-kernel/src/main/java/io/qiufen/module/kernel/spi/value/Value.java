package io.qiufen.module.kernel.spi.value;

import io.qiufen.module.kernel.spi.facade.FacadeFinder;

/**
 * 值接口
 *
 * @since 1.0
 */
public interface Value
{
    /**
     * 转为目标类型的值
     */
    <T> T to(FacadeFinder finder, Class<T> clazz);
}
