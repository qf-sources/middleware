package io.qiufen.module.kernel.api.module;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/10 9:36
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RequireModule
{
    Class<?>[] value();
}