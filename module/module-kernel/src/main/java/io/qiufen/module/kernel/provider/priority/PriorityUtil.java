package io.qiufen.module.kernel.provider.priority;

import io.qiufen.module.kernel.spi.priority.Prioritized;
import io.qiufen.module.kernel.spi.priority.Priority;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by zhangruzheng on 2021/11/14.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class PriorityUtil
{
    public static void sort(Collection<? extends Prioritized> input, List output)
    {
        output.addAll(input);
        for (Prioritized prioritized : input)
        {
            Priority priority = prioritized.priority();
            if (priority != null)
            {
                sort(output, prioritized, priority);
            }
        }
    }

    public static void sort(List list, Map<Object, Priority> priorityMap)
    {
        Set set = priorityMap.entrySet();
        for (Object o : set)
        {
            Map.Entry entry = (Map.Entry) o;
            sort(list, entry.getKey(), (Priority) entry.getValue());
        }
    }

    private static void sort(List output, Object o, Priority priority)
    {
        if (priority.getAfterList() != null && !priority.getAfterList().isEmpty())
        {
            int oldIndex = output.indexOf(o);
            //不存在该元素，退出
            if (oldIndex < 0) return;

            //执行迁移
            for (Class<?> clazz : priority.getAfterList())
            {
                int index = findIndex(output, clazz);
                //未发现相对元素
                if (index < 0) continue;
                //比较该元素与相对元素位置
                //该元素已经后置（或就是自身），无需调整
                if (oldIndex >= index) continue;
                //该元素前置，需要调整
                output.remove(oldIndex);//移出该元素
                index = findIndex(output, clazz);//重新获取相对元素位置
                output.add(index + 1, o);
            }
        }

        if (priority.getBeforeList() != null && !priority.getBeforeList().isEmpty())
        {
            int oldIndex = output.indexOf(o);
            //不存在该元素，退出
            if (oldIndex < 0) return;

            for (Class<?> clazz : priority.getBeforeList())
            {
                int index = findIndex(output, clazz);
                //相对元素不存在
                if (index < 0) continue;
                //比较相对元素位置
                //该元素已经置前（或就是自身），无需调整
                if (oldIndex <= index) continue;
                //该元素后置，需要调整
                output.remove(oldIndex);//移出该元素
                index = findIndex(output, clazz);//重新获取相对元素位置
                output.add(index, o);
            }
        }
    }

    private static int findIndex(List<?> list, Class clazz)
    {
        for (int i = 0; i < list.size(); i++)
        {
            if (list.get(i).getClass() == clazz) return i;
        }
        return -1;
    }
}
