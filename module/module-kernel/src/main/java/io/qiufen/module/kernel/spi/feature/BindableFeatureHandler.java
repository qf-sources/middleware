package io.qiufen.module.kernel.spi.feature;

import io.qiufen.module.kernel.spi.context.ContextFactory;

/**
 * 支持绑定类特性的抽象处理器
 */
public abstract class BindableFeatureHandler<B extends Binder, F extends Feature> extends FeatureHandlerAdapter<F>
{
    private B binder;

    @Override
    public final void doHandle(F feature, ContextFactory contextFactory) throws Exception
    {
        if (binder == null)
        {
            this.binder = initBinder(feature, contextFactory);
        }
        this.doHandle(feature, contextFactory, this.binder);
    }

    @Override
    public final void postModule(F feature, ContextFactory contextFactory) throws Exception
    {
        this.postModule(feature, contextFactory, this.binder);
        this.binder.clear();
    }

    protected abstract B initBinder(F feature, ContextFactory contextFactory);

    protected abstract void doHandle(F feature, ContextFactory contextFactory, B binder);

    protected abstract void postModule(F feature, ContextFactory contextFactory, B binder) throws Exception;
}
