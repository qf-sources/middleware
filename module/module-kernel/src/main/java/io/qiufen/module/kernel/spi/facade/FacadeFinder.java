package io.qiufen.module.kernel.spi.facade;

/**
 * 门面接口查找器
 */
public interface FacadeFinder
{
    /**
     * 根据门面类型查找门面接口
     *
     * @param clazz 门面接口类型
     */
    <F extends Facade> F getFacade(Class<F> clazz);
}
