package io.qiufen.module.kernel.spi.priority;

/**
 * 优先级接口
 *
 * @since 1.0
 */
public interface Prioritized
{
    /**
     * 声明优先级
     */
    Priority priority();
}
