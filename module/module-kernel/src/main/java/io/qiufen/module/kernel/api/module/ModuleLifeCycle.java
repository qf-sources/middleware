package io.qiufen.module.kernel.api.module;

public interface ModuleLifeCycle extends Module
{
    void onLoad();

    void onLoadSuccess();

    void onLoadFailure(Exception e);
}
