package io.qiufen.module.kernel.api.module;

public class ModuleLifeCycleAdapter implements ModuleLifeCycle
{
    @Override
    public void onLoad()
    {

    }

    @Override
    public void onLoadSuccess()
    {

    }

    @Override
    public void onLoadFailure(Exception e)
    {

    }
}
