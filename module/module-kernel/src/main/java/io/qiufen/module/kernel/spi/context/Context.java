package io.qiufen.module.kernel.spi.context;

/**
 * 上下内容接口，可根据预期特性做扩展
 *
 * @since 1.0
 */
public interface Context {}
