package io.qiufen.module.kernel.spi.feature;

import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;

/**
 * 模块特性处理器
 *
 * @since 1.0
 */
public interface FeatureHandler<F extends Feature>
{
    /**
     * 初始化模块特性
     *
     * @param contextFactory context
     * @param facadeRegistry 门面注册器
     */
    void initFeature(ContextFactory contextFactory, FacadeRegistry facadeRegistry);

    void doHandle(F feature, ContextFactory contextFactory) throws Exception;

    void postModule(F feature, ContextFactory contextFactory) throws Exception;
}
