/**
 * 模块化内核
 * context   - 定义上下内容，模块内部使用，方法不公开<br/>
 * exception - 定义异常<br/>
 * facade    - 定义门面，用于开放内部上下内容指定方法<br/>
 * feature   - 定义特性<br/>
 * object    - 对象实现<br/>
 * util      - 定义工具<br/>
 * spi       - 扩展接口<br/>
 */
package io.qiufen.module.kernel;