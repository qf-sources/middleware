package io.qiufen.module.kernel.spi.feature;

import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;

/**
 * 模块特性处理器适配器
 */
public class FeatureHandlerAdapter<F extends Feature> implements FeatureHandler<F>
{
    @Override
    public void initFeature(ContextFactory contextFactory, FacadeRegistry facadeRegistry)
    {

    }

    @Override
    public void doHandle(F feature, ContextFactory contextFactory) throws Exception
    {

    }

    @Override
    public void postModule(F feature, ContextFactory contextFactory) throws Exception
    {

    }
}