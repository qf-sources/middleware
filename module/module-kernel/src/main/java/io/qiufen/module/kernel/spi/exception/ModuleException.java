package io.qiufen.module.kernel.spi.exception;

/**
 * Created by zhangruzheng on 2021/11/14.
 */
public abstract class ModuleException extends RuntimeException
{
    protected ModuleException(String message)
    {
        super(message);
    }

    protected ModuleException(Throwable cause)
    {
        super(cause);
    }

    protected ModuleException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
