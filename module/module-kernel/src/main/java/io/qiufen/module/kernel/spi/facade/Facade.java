package io.qiufen.module.kernel.spi.facade;

/**
 * 门面接口，作为各子业务服务统一接口
 *
 * @since 1.0
 */
public interface Facade {}
