package io.qiufen.module.kernel.provider.value;

import io.qiufen.common.conversion.TypeConversionService;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.value.Value;

public class SimpleValue implements Value
{
    private final Object value;

    public SimpleValue(Object value)
    {
        this.value = value;
    }

    @Override
    public <T> T to(FacadeFinder finder, Class<T> clazz)
    {
        return TypeConversionService.getInstance().convert(this.value, clazz);
    }
}
