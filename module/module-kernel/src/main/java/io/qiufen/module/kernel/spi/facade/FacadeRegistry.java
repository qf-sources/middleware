package io.qiufen.module.kernel.spi.facade;

/**
 * 门面接口注册器
 *
 * @since 1.0
 */
public interface FacadeRegistry extends FacadeFinder
{
    /**
     * 注册门面
     */
    <F extends Facade> void addFacade(Class<F> clazz, F facade);
}
