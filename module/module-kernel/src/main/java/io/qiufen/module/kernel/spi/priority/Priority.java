package io.qiufen.module.kernel.spi.priority;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by zhangruzheng on 2021/11/14.
 */
public class Priority
{
    private List<Class<?>> afterList;
    private List<Class<?>> beforeList;

    public Priority after(Class<?>... clazz)
    {
        if (this.afterList == null)
        {
            this.afterList = new ArrayList<Class<?>>();
        }
        Collections.addAll(this.afterList, clazz);
        return this;
    }

    public Priority before(Class<?>... clazz)
    {
        if (this.beforeList == null)
        {
            this.beforeList = new ArrayList<Class<?>>();
        }
        Collections.addAll(this.beforeList, clazz);
        return this;
    }

    public List<Class<?>> getAfterList()
    {
        return afterList;
    }

    public void setAfterList(List<Class<?>> afterList)
    {
        this.afterList = afterList;
    }

    public List<Class<?>> getBeforeList()
    {
        return beforeList;
    }

    public void setBeforeList(List<Class<?>> beforeList)
    {
        this.beforeList = beforeList;
    }
}
