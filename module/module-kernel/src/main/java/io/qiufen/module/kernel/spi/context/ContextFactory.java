package io.qiufen.module.kernel.spi.context;

/**
 * 上下内容环境工厂
 *
 * @since 1.0
 */
public interface ContextFactory
{
    <C extends Context> void addContext(Class<C> clazz, C context);

    <C extends Context> C getContext(Class<C> clazz);
}
