package io.qiufen.module.kernel.spi.feature;

/**
 * 绑定器，可被扩展为绑定任意配置的具体实现
 */
public abstract class Binder
{
    protected abstract void clear();
}
