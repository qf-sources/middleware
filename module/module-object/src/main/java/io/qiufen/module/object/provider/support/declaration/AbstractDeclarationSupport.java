package io.qiufen.module.object.provider.support.declaration;

import io.qiufen.module.object.provider.common.Constants;
import io.qiufen.module.object.spi.declaration.Declaration;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:25
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public abstract class AbstractDeclarationSupport<D extends Declaration> implements DeclarationSupport<D>
{
    @Override
    public Object build(D declaration) throws Throwable
    {
        //lazy = true
        if (declaration.isLazy())
        {
            return Constants.EMPTY;
        }
        return create(declaration);
    }
}