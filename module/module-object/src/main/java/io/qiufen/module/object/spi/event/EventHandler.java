package io.qiufen.module.object.spi.event;

import io.qiufen.module.object.provider.support.event.ObjectConstructEvent;
import io.qiufen.module.object.spi.declaration.Declaration;

/**
 * 事件处理器
 */
public interface EventHandler
{
    void onPrepare(Declaration declaration);

    void onConstruct(ObjectConstructEvent event) throws Throwable;

    /**
     * 事件：创建完对象
     *
     * @param prototype 对象原型
     * @param decorated decorated
     */
    void onCreated(Object prototype, Object decorated) throws Throwable;

    /**
     * 事件：装饰对象原型
     *
     * @param prototype       对象原型
     * @param decoratedObject 修饰对象
     * @return 装饰的对象
     */
    Object onDecorate(Object prototype, Object decoratedObject) throws Throwable;

    /**
     * 事件：销毁对象
     * todo:待完善
     */
    void onDestroy();
}