package io.qiufen.module.object.provider.context;

import io.qiufen.module.kernel.spi.context.Context;
import io.qiufen.module.kernel.spi.exception.ModuleException;
import io.qiufen.module.object.api.declaration.BuilderDeclaration;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.api.declaration.ObjectDeclaration;
import io.qiufen.module.object.provider.exception.BuildObjectException;
import io.qiufen.module.object.provider.exception.GetObjectException;
import io.qiufen.module.object.provider.exception.NoSuchDeclarationException;
import io.qiufen.module.object.provider.exception.NoUniqueDeclarationException;
import io.qiufen.module.object.provider.support.ScopeSupportFactory;
import io.qiufen.module.object.provider.support.declaration.BuilderDeclarationSupport;
import io.qiufen.module.object.provider.support.declaration.ClassDeclarationSupport;
import io.qiufen.module.object.provider.support.declaration.ObjectDeclarationSupport;
import io.qiufen.module.object.provider.support.scope.OnceScopeSupport;
import io.qiufen.module.object.provider.support.scope.PrototypeScopeSupport;
import io.qiufen.module.object.provider.support.scope.SingletonScopeSupport;
import io.qiufen.module.object.provider.support.scope.UnusedScopeSupport;
import io.qiufen.module.object.spi.declaration.Declaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import io.qiufen.module.object.spi.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 对象上下内容环境
 *
 * @since 1.0
 */
public class ObjectContext implements Context
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectContext.class);

    // id -> declaration
    // id -> type list

    private final Map<Object, Declaration> idDeclarationMap;
    private final Map<Object, Class<?>[]> idTypeMap;

    private final ScopeSupportFactory factory;

    public ObjectContext()
    {
        this.idDeclarationMap = new ConcurrentHashMap<Object, Declaration>();
        this.idTypeMap = new ConcurrentHashMap<Object, Class<?>[]>();

        this.factory = new ScopeSupportFactory();

        init();
    }

    public void addDeclaration(Declaration declaration)
    {
        Object[] idList = declaration.getIdList();
        if (idList == null || idList.length == 0) throw new IllegalArgumentException("Must provide id.");
        factory.getEventManager().fireDeclarationPrepareEvent(declaration);
        LOGGER.info("Add declaration:{}...", declaration);
        for (Object id : idList)
        {
            if (this.idTypeMap.containsKey(id))
            {
                throw new RuntimeException("Duplication id:" + id);
            }

            Class<?>[] typeList = declaration.getTypeList();
            if (typeList == null || typeList.length == 0) throw new IllegalArgumentException("Must provide type.");
            this.idDeclarationMap.put(id, declaration);
            this.idTypeMap.put(id, typeList);

            //提前分配空位
            this.factory.createEmptyObject(declaration);
        }
    }

    /**
     * 构建声明
     */
    public void build(Declaration declaration)
    {
        try
        {
            factory.build(declaration.getScope(), declaration);
        }
        catch (ModuleException e)
        {
            throw e;
        }
        catch (Throwable e)
        {
            throw new BuildObjectException(e);
        }
    }

    public <T> T getObject(Object id, Class<T> clazz)
    {
        return clazz.cast(getObject(id));
    }

    public Object getObject(Object id)
    {
        Declaration declaration = this.idDeclarationMap.get(id);
        if (declaration == null)
        {
            throw new NoSuchDeclarationException("No such declaration,id:(" + id.getClass().getName() + ")" + id);
        }
        return getObject(declaration);
    }

    public <T> T getObject(Class<T> clazz)
    {
        List<Declaration> declarationList = searchDeclaration(clazz);
        if (declarationList.isEmpty())
        {
            throw new NoSuchDeclarationException("No such declaration,class:" + clazz.getName());
        }
        if (declarationList.size() > 1)
        {
            throw new NoUniqueDeclarationException("No unique declaration,class:" + clazz.getName());
        }

        Object object = getObject(declarationList.get(0));
        try
        {
            return clazz.cast(object);
        }
        catch (Exception throwable)
        {
            throw new GetObjectException(throwable);
        }
    }

    public <T> Set<T> getObjectOfType(Class<T> clazz)
    {
        Set<T> objectList = new HashSet<T>();
        List<Declaration> declarationList = searchDeclaration(clazz);
        if (declarationList.isEmpty())
        {
            return objectList;
        }

        for (Declaration declaration : declarationList)
        {
            Object o = getObject(declaration);
            T object;
            try
            {
                object = clazz.cast(o);
            }
            catch (Exception throwable)
            {
                throw new GetObjectException(throwable);
            }
            objectList.add(object);
        }
        return objectList;
    }

    public void addEventHandler(EventHandler eventHandler)
    {
        this.factory.addEventHandler(eventHandler);
    }

    public boolean hasDeclaration(Class<?> clazz)
    {
        for (Map.Entry<Object, Class<?>[]> entry : this.idTypeMap.entrySet())
        {
            Class<?>[] typeList = entry.getValue();
            for (Class<?> type : typeList)
            {
                if (clazz == type || clazz.isAssignableFrom(type))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private Object getObject(Declaration declaration)
    {
        try
        {
            return factory.get(declaration.getScope(), declaration);
        }
        catch (ModuleException e)
        {
            throw e;
        }
        catch (Throwable throwable)
        {
            throw new GetObjectException(throwable);
        }
    }

    private List<Declaration> searchDeclaration(Class<?> clazz)
    {
        List<Declaration> declarations = new ArrayList<Declaration>();
        Set<Map.Entry<Object, Class<?>[]>> set = this.idTypeMap.entrySet();
        for (Map.Entry<Object, Class<?>[]> entry : set)
        {
            Class<?>[] typeList = entry.getValue();
            for (Class<?> type : typeList)
            {
                if (clazz == type || clazz.isAssignableFrom(type))
                {
                    declarations.add(this.idDeclarationMap.get(entry.getKey()));
                    break;
                }
            }
        }
        return declarations;
    }

    private void init()
    {
        this.factory.addScopeSupport(ObjectScope.PROTOTYPE, new PrototypeScopeSupport());
        this.factory.addScopeSupport(ObjectScope.SINGLETON, new SingletonScopeSupport());
        this.factory.addScopeSupport(ObjectScope.ONCE, new OnceScopeSupport(this.idDeclarationMap, this.idTypeMap));
        this.factory.addScopeSupport(ObjectScope.UNUSED, new UnusedScopeSupport(this.idDeclarationMap, this.idTypeMap));

        ObjectDeclarationSupport objectDeclarationSupport = new ObjectDeclarationSupport();
        ClassDeclarationSupport classDeclarationSupport = new ClassDeclarationSupport(factory.getEventManager());
        BuilderDeclarationSupport builderDeclarationSupport = new BuilderDeclarationSupport();

        this.factory.addDeclarationSupport(ObjectScope.PROTOTYPE, ClassDeclaration.class, classDeclarationSupport);
        this.factory.addDeclarationSupport(ObjectScope.PROTOTYPE, BuilderDeclaration.class, builderDeclarationSupport);

        this.factory.addDeclarationSupport(ObjectScope.SINGLETON, ObjectDeclaration.class, objectDeclarationSupport);
        this.factory.addDeclarationSupport(ObjectScope.SINGLETON, ClassDeclaration.class, classDeclarationSupport);
        this.factory.addDeclarationSupport(ObjectScope.SINGLETON, BuilderDeclaration.class, builderDeclarationSupport);

        this.factory.addDeclarationSupport(ObjectScope.ONCE, ObjectDeclaration.class, objectDeclarationSupport);
        this.factory.addDeclarationSupport(ObjectScope.ONCE, ClassDeclaration.class, classDeclarationSupport);
        this.factory.addDeclarationSupport(ObjectScope.ONCE, BuilderDeclaration.class, builderDeclarationSupport);

        this.factory.addDeclarationSupport(ObjectScope.UNUSED, ObjectDeclaration.class, objectDeclarationSupport);
        this.factory.addDeclarationSupport(ObjectScope.UNUSED, ClassDeclaration.class, classDeclarationSupport);
        this.factory.addDeclarationSupport(ObjectScope.UNUSED, BuilderDeclaration.class, builderDeclarationSupport);
    }
}

