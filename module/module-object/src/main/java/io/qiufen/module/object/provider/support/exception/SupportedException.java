package io.qiufen.module.object.provider.support.exception;

public abstract class SupportedException extends RuntimeException
{
    protected SupportedException(String message)
    {
        super(message);
    }
}
