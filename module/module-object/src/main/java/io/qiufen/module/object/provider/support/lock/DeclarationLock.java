package io.qiufen.module.object.provider.support.lock;

import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.LockSupport;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class DeclarationLock
{
    private final Lock operationLock;

    private final Map<Declaration, Lock> lockMap;

    public DeclarationLock()
    {
        this.operationLock = new LightLock();
        this.lockMap = new ConcurrentHashMap<Declaration, Lock>();
    }

    public Lock getLock(Declaration declaration)
    {
        Lock lock = this.lockMap.get(declaration);
        if (lock != null)
        {
            return lock;
        }

        try
        {
            operationLock.lock();
            lock = this.lockMap.get(declaration);
            if (lock == null)
            {
                lock = new LightLock();
                this.lockMap.put(declaration, lock);
            }
        }
        finally
        {
            operationLock.unlock();
        }
        return lock;
    }

    public void removeLock(Declaration declaration)
    {
        this.lockMap.remove(declaration);
    }

    /**
     * 轻量级锁（不支持重入）
     */
    private static class LightLock implements Lock
    {
        private final AtomicBoolean state = new AtomicBoolean(false);

        private volatile Thread current;

        @Override
        public void lock()
        {
            int tryTimes = 0;
            Thread thread = Thread.currentThread();
            while (!tryLock())
            {
                tryTimes++;
                if (tryTimes <= 1000)
                {
                    Thread.yield();
                }
                else if (tryTimes <= 5000)
                {
                    LockSupport.parkNanos(thread, 100);
                }
                else
                {
                    LockSupport.parkNanos(thread, TimeUnit.MILLISECONDS.toNanos(1));
                }
            }
        }

        @Override
        public void lockInterruptibly()
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean tryLock()
        {
            if (this.current == Thread.currentThread()) return true;

            boolean result = this.state.compareAndSet(false, true);
            if (result) this.current = Thread.currentThread();
            return result;
        }

        @Override
        public boolean tryLock(long time, TimeUnit unit)
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public void unlock()
        {
            this.current = null;
            state.set(false);
        }

        @Override
        public Condition newCondition()
        {
            throw new UnsupportedOperationException();
        }
    }
}
