package io.qiufen.module.object.provider.support.event;

import io.qiufen.module.object.api.declaration.ClassDeclaration;

import java.lang.reflect.Constructor;

public class ObjectConstructEvent
{
    private final ClassDeclaration declaration;
    private Constructor<?> constructor;
    private Object[] arguments;

    public ObjectConstructEvent(ClassDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public ClassDeclaration getDeclaration()
    {
        return declaration;
    }

    public Constructor<?> getConstructor() {return this.constructor;}

    public void setConstructor(Constructor<?> constructor)
    {
        this.constructor = constructor;
    }

    public Object[] getArguments() {return this.arguments;}

    public void setArguments(Object[] arguments)
    {
        this.arguments = arguments;
    }
}
