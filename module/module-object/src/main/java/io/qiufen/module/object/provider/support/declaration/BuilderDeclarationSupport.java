package io.qiufen.module.object.provider.support.declaration;

import io.qiufen.module.object.api.declaration.BuilderDeclaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:26
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class BuilderDeclarationSupport extends AbstractDeclarationSupport<BuilderDeclaration>
{
    @Override
    public Object create(BuilderDeclaration declaration) throws Throwable
    {
        Object prototype = declaration.getBuilder().build();
        //单例情况下，创建对象后，即可清除
        if (declaration.getScope() == ObjectScope.SINGLETON)
        {
            declaration.setBuilder(null);
        }
        return prototype;
    }
}