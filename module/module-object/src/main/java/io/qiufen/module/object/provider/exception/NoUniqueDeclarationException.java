package io.qiufen.module.object.provider.exception;

import io.qiufen.module.kernel.spi.exception.ModuleException;

/**
 * Created by zhangruzheng on 2021/11/14.
 */
public class NoUniqueDeclarationException extends ModuleException
{
    public NoUniqueDeclarationException(String message)
    {
        super(message);
    }
}
