package io.qiufen.module.object.api.declaration;

import io.qiufen.module.object.spi.declaration.Declaration;

/**
 * 构造类对象声明
 *
 * @since 1.0
 */
public class BuilderDeclaration extends Declaration
{
    /**
     * 单例模式下，不会再次构建对象，因此对象构建成功之后即销毁
     */
    private Builder builder;

    public BuilderDeclaration(Class<?> clazz, Builder builder)
    {
        super(clazz);
        this.builder = builder;
    }

    public Builder getBuilder()
    {
        return builder;
    }

    public void setBuilder(Builder builder)
    {
        this.builder = builder;
    }

    @Override
    public String toString()
    {
        return "BuilderDeclaration{" + "builder=" + builder + "} " + super.toString();
    }
}