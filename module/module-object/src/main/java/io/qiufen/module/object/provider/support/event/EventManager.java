package io.qiufen.module.object.provider.support.event;

import io.qiufen.module.object.spi.declaration.Declaration;
import io.qiufen.module.object.spi.event.EventHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 11:04
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class EventManager
{
    private final List<EventHandler> eventHandlerList;

    public EventManager()
    {
        this.eventHandlerList = new ArrayList<EventHandler>();
    }

    /**
     * 触发对象创建事件
     */
    public void fireObjectConstructEvent(ObjectConstructEvent event) throws Throwable
    {
        for (EventHandler eventHandler : eventHandlerList)
        {
            eventHandler.onConstruct(event);
        }
    }

    /**
     * 触发对象创建事件
     */
    public void fireObjectCreatedEvent(Object prototype, Object decorated) throws Throwable
    {
        for (EventHandler eventHandler : eventHandlerList)
        {
            eventHandler.onCreated(prototype, decorated);
        }
    }

    /**
     * 触发对象装饰事件
     */
    public Object fireObjectDecorateEvent(Object prototype) throws Throwable
    {
        Object object = prototype;
        for (EventHandler eventHandler : eventHandlerList)
        {
            object = eventHandler.onDecorate(prototype, object);
        }
        return object;
    }

    public void addEventHandler(EventHandler eventHandler)
    {
        this.eventHandlerList.add(eventHandler);
    }

    public void fireDeclarationPrepareEvent(Declaration declaration)
    {
        for (EventHandler eventHandler : eventHandlerList)
        {
            eventHandler.onPrepare(declaration);
        }
    }
}