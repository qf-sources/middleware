package io.qiufen.module.object.spi.declaration;

/**
 * 对象实例类型
 */
public enum ObjectScope
{
    /**
     * 原型
     */
    PROTOTYPE,

    /**
     * 单例
     */
    SINGLETON,

    /**
     * 使用后失效
     */
    ONCE,

    /**
     * 未被使用
     */
    UNUSED
}
