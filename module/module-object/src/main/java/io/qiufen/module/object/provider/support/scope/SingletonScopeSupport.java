package io.qiufen.module.object.provider.support.scope;

import io.qiufen.module.object.provider.common.Constants;
import io.qiufen.module.object.provider.support.declaration.DeclarationSupport;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:28
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class SingletonScopeSupport extends AbstractScopeSupport
{
    public SingletonScopeSupport()
    {
        super(new ConcurrentHashMap<Declaration, Object>());
    }

    @Override
    protected void build0(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        Object prototype = getPrototype(declaration);
        if (prototype != Constants.EMPTY)
        {
            return;
        }

        Lock lock = getLock(declaration);
        try
        {
            lock.lock();
            prototype = getPrototype(declaration);
            if (prototype != Constants.EMPTY)
            {
                return;
            }

            prototype = support.build(declaration);
            if (prototype == Constants.EMPTY)
            {
                return;
            }
            getDeclarationPrototypeMap().put(declaration, prototype);
            Object decorated = fireObjectDecoratedEvent(prototype);
            useDeclarationDecoratedMap().put(declaration, decorated);
            fireObjectCreatedEvent(prototype, decorated);

            removeLock(declaration);
        }
        finally
        {
            lock.unlock();
        }
    }

    @Override
    protected Object get0(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        Object prototype = getPrototype(declaration);
        if (prototype != Constants.EMPTY)
        {
            return getDecorated(declaration, prototype);
        }

        Object decorated;
        Lock lock = getLock(declaration);
        try
        {
            lock.lock();
            prototype = getPrototype(declaration);
            if (prototype != Constants.EMPTY)
            {
                //防止二次脏锁问题
                removeLock(declaration);
                return getDecorated(declaration, prototype);
            }

            prototype = support.create(declaration);
            getDeclarationPrototypeMap().put(declaration, prototype);
            decorated = fireObjectDecoratedEvent(prototype);
            useDeclarationDecoratedMap().put(declaration, decorated);
            fireObjectCreatedEvent(prototype, decorated);

            removeLock(declaration);
        }
        finally
        {
            lock.unlock();
        }
        return decorated;
    }
}