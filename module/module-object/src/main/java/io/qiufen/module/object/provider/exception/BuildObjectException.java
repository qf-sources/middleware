package io.qiufen.module.object.provider.exception;

import io.qiufen.module.kernel.spi.exception.ModuleException;

/**
 * Created by zhangruzheng on 2021/11/14.
 */
public class BuildObjectException extends ModuleException
{
    public BuildObjectException(String message)
    {
        super(message);
    }

    public BuildObjectException(Throwable cause)
    {
        super(cause);
    }
}
