package io.qiufen.module.object.api.declaration;

/**
 * Created by zhangruzheng on 2021/11/9.
 */
public interface Builder
{
    Object build() throws Throwable;
}
