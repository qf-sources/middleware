package io.qiufen.module.object.provider.exception;

import io.qiufen.module.kernel.spi.exception.ModuleException;

/**
 * Created by zhangruzheng on 2021/11/14.
 */
public class NoSuchDeclarationException extends ModuleException
{
    public NoSuchDeclarationException(String message)
    {
        super(message);
    }
}
