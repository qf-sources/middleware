package io.qiufen.module.object.provider.support.scope;

import io.qiufen.module.object.provider.common.Constants;
import io.qiufen.module.object.provider.exception.NoSuchDeclarationException;
import io.qiufen.module.object.provider.support.declaration.DeclarationSupport;
import io.qiufen.module.object.provider.support.event.EventManager;
import io.qiufen.module.object.provider.support.lock.DeclarationLock;
import io.qiufen.module.object.spi.declaration.Declaration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:30
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public abstract class AbstractScopeSupport implements ScopeSupport
{
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    // declaration -> object
    private final Map<Declaration, Object> declarationPrototypeMap;
    // declaration -> decorated
    private volatile Map<Declaration, Object> declarationDecoratedMap;

    private DeclarationLock declarationLock;
    private EventManager eventManager;

    protected AbstractScopeSupport(Map<Declaration, Object> declarationPrototypeMap)
    {
        this.declarationPrototypeMap = declarationPrototypeMap;
    }

    @Override
    public void build(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        //检查是否存在注册该声明
        if (!declarationPrototypeMap.containsKey(declaration))
        {
            LOGGER.warn("No such declaration:{}", declaration);
            return;
        }
        build0(support, declaration);
    }

    @Override
    public Object get(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        checkDeclaration(declaration);
        return get0(support, declaration);
    }

    public Map<Declaration, Object> getDeclarationPrototypeMap()
    {
        return declarationPrototypeMap;
    }

    public void setDeclarationLock(DeclarationLock declarationLock)
    {
        this.declarationLock = declarationLock;
    }

    public void setEventManager(EventManager eventManager)
    {
        this.eventManager = eventManager;
    }

    @Override
    public void createEmptyObject(Declaration declaration)
    {
        this.declarationPrototypeMap.put(declaration, Constants.EMPTY);
    }

    protected Object getPrototype(Declaration declaration)
    {
        return declarationPrototypeMap.get(declaration);
    }

    protected Object getDecorated(Declaration declaration, Object prototype)
    {
        if (this.declarationDecoratedMap == null) return prototype;

        Object decorated = this.declarationDecoratedMap.get(declaration);
        return decorated == null ? prototype : decorated;
    }

    protected final Lock getLock(Declaration declaration)
    {
        return this.declarationLock.getLock(declaration);
    }

    protected final void removeLock(Declaration declaration)
    {
        this.declarationLock.removeLock(declaration);
    }

    protected final Object fireObjectDecoratedEvent(Object prototype) throws Throwable
    {
        return this.eventManager.fireObjectDecorateEvent(prototype);
    }

    protected final void fireObjectCreatedEvent(Object prototype, Object decorated) throws Throwable
    {
        this.eventManager.fireObjectCreatedEvent(prototype, decorated);
    }

    protected final void checkDeclaration(Declaration declaration)
    {
        if (!declarationPrototypeMap.containsKey(declaration))
        {
            throw new NoSuchDeclarationException("No such declaration:" + declaration);
        }
    }

    protected Map<Declaration, Object> useDeclarationDecoratedMap()
    {
        if (this.declarationDecoratedMap != null)
        {
            return this.declarationDecoratedMap;
        }
        synchronized (this)
        {
            if (this.declarationDecoratedMap != null)
            {
                return this.declarationDecoratedMap;
            }
            this.declarationDecoratedMap = new ConcurrentHashMap<Declaration, Object>();
        }
        return this.declarationDecoratedMap;
    }

    protected void build0(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        LOGGER.warn("None implementation for build!");
    }

    protected Object get0(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        LOGGER.warn("None implementation for get!");
        return null;
    }
}