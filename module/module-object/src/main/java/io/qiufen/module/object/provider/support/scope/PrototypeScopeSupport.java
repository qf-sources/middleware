package io.qiufen.module.object.provider.support.scope;

import io.qiufen.module.object.provider.common.Constants;
import io.qiufen.module.object.provider.support.declaration.DeclarationSupport;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:28
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class PrototypeScopeSupport extends AbstractScopeSupport
{
    public PrototypeScopeSupport()
    {
        super(new ConcurrentHashMap<Declaration, Object>());
    }

    @Override
    protected void build0(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        Lock lock = getLock(declaration);
        try
        {
            lock.lock();
            Object prototype = support.build(declaration);
            if (prototype == Constants.EMPTY)
            {
                return;
            }
            getDeclarationPrototypeMap().put(declaration, prototype);
            Object decorated = fireObjectDecoratedEvent(prototype);
            useDeclarationDecoratedMap().put(declaration, decorated);
            fireObjectCreatedEvent(prototype, decorated);
        }
        finally
        {
            lock.unlock();
        }
    }

    @Override
    protected Object get0(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        Object decorated;
        Lock lock = getLock(declaration);
        try
        {
            lock.lock();
            Object prototype = getPrototype(declaration);
            if (prototype != Constants.EMPTY)
            {
                decorated = getDecorated(declaration, prototype);
                //置空对象位
                getDeclarationPrototypeMap().put(declaration, Constants.EMPTY);
                useDeclarationDecoratedMap().remove(declaration);
                return decorated;
            }

            prototype = support.create(declaration);
            decorated = fireObjectDecoratedEvent(prototype);

            fireObjectCreatedEvent(prototype, decorated);
        }
        finally
        {
            lock.unlock();
        }
        return decorated;
    }
}