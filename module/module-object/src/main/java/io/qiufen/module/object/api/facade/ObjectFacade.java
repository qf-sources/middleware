package io.qiufen.module.object.api.facade;

import io.qiufen.module.kernel.spi.facade.Facade;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.Set;

/**
 * 对象门面接口
 *
 * @since 1.0
 */
public interface ObjectFacade extends Facade
{
    /**
     * 根据id获取对象
     *
     * @param id ID
     * @return 对象
     */
    Object getObject(Object id);

    /**
     * 根据id获取对象，并根据类型引用
     *
     * @param id    ID
     * @param clazz 对象类型
     * @return 对象
     */
    <T> T getObject(Object id, Class<T> clazz);

    /**
     * 根据类型获取对象
     *
     * @param clazz 类型
     * @return 对象
     */
    <T> T getObject(Class<T> clazz);

    /**
     * 根据类型查找符合类型的对象
     *
     * @param clazz 类型
     * @return 对象集
     */
    <T> Set<T> getObjectOfType(Class<T> clazz);

    /**
     * 添加声明
     *
     * @param declaration 声明
     */
    void addDeclaration(Declaration declaration);

    boolean hasDeclaration(Class<?> clazz);
}
