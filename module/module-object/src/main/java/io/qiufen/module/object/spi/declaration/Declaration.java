package io.qiufen.module.object.spi.declaration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 对象声明
 *
 * @since 1.0
 */
public abstract class Declaration
{
    private Object[] idList;
    private Class<?>[] typeList;

    private boolean lazy;

    /**
     * 对象作用范围
     *
     * @see ObjectScope
     */
    private ObjectScope scope = ObjectScope.SINGLETON;

    protected Declaration(Class<?> clazz)
    {
        setIdList(new Object[]{clazz.getName()});
        setTypeList0(clazz);
    }

    public Object[] getIdList()
    {
        return idList;
    }

    public void setIdList(Object[] idList)
    {
        this.idList = idList;
    }

    public Class<?>[] getTypeList()
    {
        return typeList;
    }

    public void setTypeList(Class<?>[] typeList)
    {
        this.typeList = typeList;
    }

    public boolean isLazy()
    {
        return lazy;
    }

    public void setLazy(boolean lazy)
    {
        this.lazy = lazy;
    }

    public ObjectScope getScope()
    {
        return scope;
    }

    public void setScope(ObjectScope scope)
    {
        this.scope = scope;
    }

    @Override
    public String toString()
    {
        return "Declaration{" + "idList=" + Arrays.toString(idList) + ", typeList=" + Arrays.toString(
                typeList) + ", lazy=" + lazy + ", scope=" + scope + '}';
    }

    private void setTypeList0(Class<?> clazz)
    {
        Class<?>[] interClasses = clazz.getInterfaces();
        if (interClasses.length == 0)
        {
            setTypeList(new Class[]{clazz});
            return;
        }

        List<Class<?>> interClassList = new ArrayList<Class<?>>();
        getInterfaceClasses(clazz, interClassList);
        Class<?>[] classes = new Class<?>[interClassList.size() + 1];
        interClassList.toArray(classes);
        classes[classes.length - 1] = clazz;
        setTypeList(classes);
    }

    private void getInterfaceClasses(Class<?> type, List<Class<?>> interClassList)
    {
        Class<?>[] classes = type.getInterfaces();
        if (classes.length == 0) return;

        for (Class<?> clazz : classes)
        {
            getInterfaceClasses(clazz, interClassList);
            interClassList.add(clazz);
        }
    }
}