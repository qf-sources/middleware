package io.qiufen.module.object.api.declaration;

import io.qiufen.module.object.spi.declaration.Declaration;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/10 20:38
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ClassDeclaration extends Declaration
{
    private final Class<?> clazz;

    public ClassDeclaration(Class<?> clazz)
    {
        super(clazz);
        this.clazz = clazz;
    }

    public Class<?> getClazz()
    {
        return clazz;
    }

    @Override
    public String toString()
    {
        return "ClassDeclaration{" + "clazz=" + clazz + "} " + super.toString();
    }
}