package io.qiufen.module.object.provider.support.exception;

public class NoneSuitableConstructorException extends SupportedException
{
    private final Class<?> clazz;

    public NoneSuitableConstructorException(Class<?> clazz, String s)
    {
        super("[" + clazz.getName() + "]" + s);
        this.clazz = clazz;
    }

    public Class<?> getClazz()
    {
        return clazz;
    }
}
