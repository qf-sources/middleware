package io.qiufen.module.object.provider.support.scope;

import io.qiufen.module.object.provider.exception.GetObjectException;
import io.qiufen.module.object.provider.support.declaration.DeclarationSupport;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.Iterator;
import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:27
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class UnusedScopeSupport extends AbstractScopeSupport
{
    private final Map<Object, Declaration> idDeclarationMap;
    private final Map<Object, Class<?>[]> idTypeMap;

    public UnusedScopeSupport(Map<Object, Declaration> idDeclarationMap, Map<Object, Class<?>[]> idTypeMap)
    {
        super(null);
        this.idDeclarationMap = idDeclarationMap;
        this.idTypeMap = idTypeMap;
    }

    @Override
    public void createEmptyObject(Declaration declaration) {}

    @Override
    public void build(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        //延迟创建设置为false，此类对象立即创建
        declaration.setLazy(false);
        Object prototype = support.build(declaration);
        Object decorated = fireObjectDecoratedEvent(prototype);
        fireObjectCreatedEvent(prototype, decorated);
        removeDeclaration(declaration);
    }

    @Override
    public Object get(DeclarationSupport support, Declaration declaration)
    {
        throw new GetObjectException("Can't get object for unused declaration!");
    }

    private void removeDeclaration(Declaration declaration)
    {
        Iterator<Map.Entry<Object, Declaration>> iterator = this.idDeclarationMap.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<Object, Declaration> entry = iterator.next();
            if (entry.getValue() == declaration)
            {
                Object id = entry.getKey();
                this.idTypeMap.remove(id);
                iterator.remove();
            }
        }
    }
}