package io.qiufen.module.object.provider.support.declaration;

import io.qiufen.module.object.api.declaration.ObjectDeclaration;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:26
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ObjectDeclarationSupport implements DeclarationSupport<ObjectDeclaration>
{
    @Override
    public Object build(ObjectDeclaration declaration)
    {
        //构建时
        //lazy 选项不使用
        //scope 选项仅支持 SINGLETON,ONCE
        return create(declaration);
    }

    @Override
    public Object create(ObjectDeclaration declaration)
    {
        return declaration.getObject();
    }
}