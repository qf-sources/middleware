package io.qiufen.module.object.provider.support;

import io.qiufen.module.object.provider.support.declaration.DeclarationSupport;
import io.qiufen.module.object.provider.support.event.EventManager;
import io.qiufen.module.object.provider.support.lock.DeclarationLock;
import io.qiufen.module.object.provider.support.scope.AbstractScopeSupport;
import io.qiufen.module.object.provider.support.scope.ScopeSupport;
import io.qiufen.module.object.spi.declaration.Declaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import io.qiufen.module.object.spi.event.EventHandler;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:54
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
@SuppressWarnings("rawtypes")
public class ScopeSupportFactory
{
    private final EventManager eventManager;
    private final DeclarationLock declarationLock;

    private final Map<ObjectScope, ScopeSupport> scopeSupportMap;
    private final Map<ObjectScope, Map<Class, DeclarationSupport>> declarationSupportMap;

    public ScopeSupportFactory()
    {
        this.eventManager = new EventManager();
        this.declarationLock = new DeclarationLock();

        this.scopeSupportMap = new IdentityHashMap<ObjectScope, ScopeSupport>();
        this.declarationSupportMap = new IdentityHashMap<ObjectScope, Map<Class, DeclarationSupport>>();
    }

    public void build(ObjectScope scope, Declaration declaration) throws Throwable
    {
        ScopeSupport scopeSupport = getScopeSupport(scope);
        DeclarationSupport declarationSupport = getDeclarationSupport(scope, declaration.getClass());
        scopeSupport.build(declarationSupport, declaration);
    }

    public Object get(ObjectScope scope, Declaration declaration) throws Throwable
    {
        ScopeSupport scopeSupport = getScopeSupport(scope);
        DeclarationSupport declarationSupport = getDeclarationSupport(scope, declaration.getClass());
        return scopeSupport.get(declarationSupport, declaration);
    }

    public void createEmptyObject(Declaration declaration)
    {
        ObjectScope scope = declaration.getScope();
        ScopeSupport scopeSupport = getScopeSupport(scope);
        scopeSupport.createEmptyObject(declaration);
    }

    public <T extends AbstractScopeSupport> void addScopeSupport(ObjectScope scope, T support)
    {
        support.setDeclarationLock(declarationLock);
        support.setEventManager(eventManager);
        this.scopeSupportMap.put(scope, support);
    }

    public <T extends Declaration> void addDeclarationSupport(ObjectScope scope, Class<T> clazz,
            DeclarationSupport<T> support)
    {
        Map<Class, DeclarationSupport> map = this.declarationSupportMap.get(scope);
        if (map == null)
        {
            map = new IdentityHashMap<Class, DeclarationSupport>();
            this.declarationSupportMap.put(scope, map);
        }
        map.put(clazz, support);
    }

    public void addEventHandler(EventHandler eventHandler)
    {
        this.eventManager.addEventHandler(eventHandler);
    }

    private ScopeSupport getScopeSupport(ObjectScope scope)
    {
        ScopeSupport scopeSupport = this.scopeSupportMap.get(scope);
        if (scopeSupport == null)
        {
            throw new UnsupportedOperationException("Unsupported declaration scope:" + scope.name());
        }
        return scopeSupport;
    }

    private DeclarationSupport getDeclarationSupport(ObjectScope scope, Class clazz)
    {
        Map<Class, DeclarationSupport> map = this.declarationSupportMap.get(scope);
        if (map == null)
        {
            throw new UnsupportedOperationException(
                    "Unsupported declaration,scope:" + scope.name() + ",class:" + clazz.getName());
        }
        DeclarationSupport declarationSupport = map.get(clazz);
        if (declarationSupport == null)
        {
            throw new UnsupportedOperationException(
                    "Unsupported declaration,scope:" + scope.name() + ",class:" + clazz.getName());
        }
        return declarationSupport;
    }

    public EventManager getEventManager()
    {
        return this.eventManager;
    }
}