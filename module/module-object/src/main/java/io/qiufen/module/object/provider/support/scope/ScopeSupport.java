package io.qiufen.module.object.provider.support.scope;

import io.qiufen.module.object.provider.support.declaration.DeclarationSupport;
import io.qiufen.module.object.spi.declaration.Declaration;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:24
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface ScopeSupport
{
    void createEmptyObject(Declaration declaration);

    void build(DeclarationSupport support, Declaration declaration) throws Throwable;

    Object get(DeclarationSupport support, Declaration declaration) throws Throwable;
}