package io.qiufen.module.object.provider.value;

import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.value.Value;
import io.qiufen.module.object.api.facade.ObjectFacade;

public class RefValue implements Value
{
    private final Object id;

    public RefValue(Object id)
    {
        this.id = id;
    }

    @Override
    public <T> T to(FacadeFinder finder, Class<T> clazz)
    {
        if (this.id == null)
        {
            return finder.getFacade(ObjectFacade.class).getObject(clazz);
        }
        return finder.getFacade(ObjectFacade.class).getObject(this.id, clazz);
    }
}
