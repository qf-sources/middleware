package io.qiufen.module.object.spi.event;

import io.qiufen.module.object.provider.support.event.ObjectConstructEvent;
import io.qiufen.module.object.spi.declaration.Declaration;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/10 21:18
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class EventHandlerAdapter implements EventHandler
{
    @Override
    public void onPrepare(Declaration declaration)
    {
    }

    @Override
    public void onConstruct(ObjectConstructEvent event) throws Throwable
    {
    }

    @Override
    public void onCreated(Object prototype, Object decorated) throws Throwable
    {
    }

    @Override
    public Object onDecorate(Object prototype, Object decoratedObject) throws Throwable
    {
        return decoratedObject;
    }

    @Override
    public void onDestroy()
    {
    }
}