package io.qiufen.module.object.provider.support.declaration;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.lang.Arrays;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.provider.exception.BuildObjectException;
import io.qiufen.module.object.provider.support.event.EventManager;
import io.qiufen.module.object.provider.support.event.ObjectConstructEvent;
import io.qiufen.module.object.provider.support.exception.NoneSuitableConstructorException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:26
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ClassDeclarationSupport extends AbstractDeclarationSupport<ClassDeclaration>
{
    private final EventManager eventManager;

    public ClassDeclarationSupport(EventManager eventManager)
    {
        this.eventManager = eventManager;
    }

    private final Set<ClassDeclaration> mayLockedDeclarations = new HashSet<ClassDeclaration>();

    @Override
    public Object create(ClassDeclaration declaration) throws Throwable
    {
        if (CollectionUtil.contain(mayLockedDeclarations, declaration))
        {
            throw new BuildObjectException("Constructor circular dependency detected!");
        }

        ObjectConstructEvent event;
        try
        {
            mayLockedDeclarations.add(declaration);

            event = new ObjectConstructEvent(declaration);
            eventManager.fireObjectConstructEvent(event);
        }
        finally
        {
            mayLockedDeclarations.remove(declaration);
        }

        Constructor<?> constructor = event.getConstructor();
        Object[] args = Arrays.EMPTY;
        Class<?> clazz = declaration.getClazz();
        if (constructor == null)
        {
            //未提供构造方法，则尝试匹配默认无参构造方法
            constructor = matchDefaultConstructor(clazz, constructor);
        }
        else
        {
            //使用提供的构造方法，实例化对象
            args = event.getArguments();
        }
        return newInstance(clazz, constructor, args);
    }

    private Constructor<?> matchDefaultConstructor(Class<?> clazz, Constructor<?> constructor)
    {
        Constructor<?>[] constructors = clazz.getDeclaredConstructors();
        for (Constructor<?> c : constructors)
        {
            if (c.getGenericParameterTypes().length == 0)
            {
                constructor = c;
                break;
            }
        }
        if (constructor == null)
        {
            throw new NoneSuitableConstructorException(clazz, "Can't find suitable constructor!");
        }
        return constructor;
    }

    private Object newInstance(Class<?> clazz, Constructor<?> constructor, Object[] args) throws Throwable
    {
        int modifier = constructor.getModifiers();
        //public scope
        if (Modifier.isPublic(modifier))
        {
            return newInstance0(constructor, args);
        }

        //package scope
        if ((modifier << 31) == 0)
        {
            constructor.setAccessible(true);
            try
            {
                return newInstance0(constructor, args);
            }
            finally
            {
                constructor.setAccessible(false);
            }
        }

        throw new BuildObjectException("Can't instance class:" + clazz.getName());
    }

    private static Object newInstance0(Constructor<?> constructor, Object[] args) throws Throwable
    {
        try
        {
            return constructor.newInstance(args);
        }
        catch (InvocationTargetException e)
        {
            throw e.getTargetException();
        }
    }
}