package io.qiufen.module.object.api.declaration;

import io.qiufen.module.object.spi.declaration.Declaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/10 20:36
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ObjectDeclaration extends Declaration
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectDeclaration.class);

    private final Object object;

    public ObjectDeclaration(Object object)
    {
        super(object.getClass());
        this.object = object;
    }

    public Object getObject()
    {
        return object;
    }

    @Override
    public void setScope(ObjectScope scope)
    {
        if (scope == ObjectScope.PROTOTYPE)
        {
            LOGGER.warn("Unsupported operation,scope:{}", scope.name());
            return;
        }
        super.setScope(scope);
    }

    @Override
    public String toString()
    {
        return "ObjectDeclaration{" + "object=" + object + "} " + super.toString();
    }
}