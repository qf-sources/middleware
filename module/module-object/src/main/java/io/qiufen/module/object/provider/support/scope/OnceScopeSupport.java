package io.qiufen.module.object.provider.support.scope;

import io.qiufen.module.object.provider.common.Constants;
import io.qiufen.module.object.provider.support.declaration.DeclarationSupport;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/11/16 10:27
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class OnceScopeSupport extends AbstractScopeSupport
{
    private final Map<Object, Declaration> idDeclarationMap;
    private final Map<Object, Class<?>[]> idTypeMap;

    public OnceScopeSupport(Map<Object, Declaration> idDeclarationMap, Map<Object, Class<?>[]> idTypeMap)
    {
        super(new ConcurrentHashMap<Declaration, Object>());
        this.idDeclarationMap = idDeclarationMap;
        this.idTypeMap = idTypeMap;
    }

    @Override
    protected void build0(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        Object prototype = getPrototype(declaration);
        if (prototype != Constants.EMPTY)
        {
            return;
        }

        Lock lock = getLock(declaration);
        try
        {
            lock.lock();
            //检查是否存在注册该声明：防止对象已被使用，声明已被移出
            if (!getDeclarationPrototypeMap().containsKey(declaration))
            {
                LOGGER.warn("No such declaration:{}", declaration);
                return;
            }
            prototype = getPrototype(declaration);
            if (prototype != Constants.EMPTY)
            {
                return;
            }

            prototype = support.build(declaration);
            if (prototype == Constants.EMPTY)
            {
                return;
            }
            getDeclarationPrototypeMap().put(declaration, prototype);
            Object decorated = fireObjectDecoratedEvent(prototype);
            useDeclarationDecoratedMap().put(declaration, decorated);
            fireObjectCreatedEvent(prototype, decorated);

            removeLock(declaration);
        }
        finally
        {
            lock.unlock();
        }
    }

    @Override
    protected Object get0(DeclarationSupport support, Declaration declaration) throws Throwable
    {
        Object decorated;
        Lock lock = getLock(declaration);
        try
        {
            lock.lock();
            checkDeclaration(declaration);
            Object prototype = getPrototype(declaration);
            if (prototype != Constants.EMPTY)
            {
                decorated = getDecorated(declaration, prototype);
                //清除声明及相关引用：防止二次脏锁问题
                removeDeclaration(declaration);
                removeLock(declaration);
                return decorated;
            }

            prototype = support.create(declaration);
            decorated = fireObjectDecoratedEvent(prototype);
            fireObjectCreatedEvent(prototype, decorated);

            //清除声明及相关引用
            removeDeclaration(declaration);
            removeLock(declaration);
        }
        finally
        {
            lock.unlock();
        }
        return decorated;
    }

    private void removeDeclaration(Declaration declaration)
    {
        getDeclarationPrototypeMap().remove(declaration);
        useDeclarationDecoratedMap().remove(declaration);
        Iterator<Map.Entry<Object, Declaration>> iterator = this.idDeclarationMap.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<Object, Declaration> entry = iterator.next();
            if (entry.getValue() == declaration)
            {
                Object id = entry.getKey();
                this.idTypeMap.remove(id);
                iterator.remove();
            }
        }
    }
}