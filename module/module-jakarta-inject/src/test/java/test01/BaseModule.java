package test01;

import io.qiufen.module.injection.provider.feature.InjectionBinder;
import io.qiufen.module.injection.provider.feature.InjectionFeature;
import io.qiufen.module.injection.provider.processor.ContributeMethodProcessor;
import io.qiufen.module.injection.provider.qualifier.NamedQualifierProcessor;
import io.qiufen.module.processor.provider.feature.ProcessorBinder;
import io.qiufen.module.processor.provider.feature.ProcessorFeature;

public class BaseModule implements ProcessorFeature, InjectionFeature
{
    @Override
    public void bind(ProcessorBinder binder)
    {
        binder.bind(ContributeMethodProcessor.class);
    }

    @Override
    public void bind(InjectionBinder binder)
    {
        binder.bind(NamedQualifierProcessor.class);
    }
}
