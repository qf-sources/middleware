package test01;

import io.qiufen.module.binding.provider.feature.BindingBinder;
import io.qiufen.module.binding.provider.feature.BindingFeature;
import io.qiufen.module.injection.provider.processor.InjectConstructorProcessor;
import io.qiufen.module.injection.provider.processor.InjectFieldProcessor;
import io.qiufen.module.kernel.api.module.RequireModule;
import io.qiufen.module.launcher.provider.Launcher;
import io.qiufen.module.processor.provider.feature.ProcessorBinder;
import io.qiufen.module.processor.provider.feature.ProcessorFeature;
import jakarta.inject.Inject;
import jakarta.inject.Provider;

import java.util.Date;

@RequireModule(BaseModule.class)
public class TestModule2 implements ProcessorFeature, BindingFeature
{
    public static void main(String[] args)
    {
        Launcher launcher = new Launcher().addModule(TestModule2.class).start();
        Service service = launcher.getFacadeFinder().getObjectFacade().getObject(Service.class);
        service.test();
    }

    @Override
    public void bind(BindingBinder binder)
    {
        binder.bind(UserDao1.class).id("userDao").lazy(true);
        binder.bind(DefaultService1.class).lazy(true);
    }

    @Override
    public void bind(ProcessorBinder binder)
    {
        binder.bind(InjectConstructorProcessor.class);
        binder.bind(InjectFieldProcessor.class);
    }
}

class DefaultService1 implements Service
{
    private final UserDao1 userDao;

    @Inject
    DefaultService1(UserDao1 userDao)
    {
        this.userDao = userDao;
        System.out.println(DefaultService1.class.getName());
    }

    @Override
    public void test()
    {
        this.userDao.test();
    }
}

class UserDao1
{
    //    private final Provider<DefaultService1> service;
    @Inject
    private Provider<DefaultService1> service;

//    @Inject
//    UserDao1(Provider<DefaultService1> service)
//    {
//        this.service = service;
//        System.out.println(UserDao1.class.getName());
//    }

    void test()
    {
        System.out.println(service.get() + ":" + new Date());
//        System.out.println(service + ":" + new Date());
    }
}