package test01;

import io.qiufen.module.binding.provider.feature.BindingBinder;
import io.qiufen.module.binding.provider.feature.BindingFeature;
import io.qiufen.module.injection.api.Contribute;
import io.qiufen.module.injection.api.Scope;
import io.qiufen.module.kernel.api.module.RequireModule;
import io.qiufen.module.launcher.provider.Launcher;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import jakarta.inject.Named;

import java.util.Date;

@RequireModule(BaseModule.class)
public class TestModule implements BindingFeature
{
    public static void main(String[] args)
    {
        Launcher launcher = new Launcher().addModule(TestModule.class).start();
        launcher.getFacadeFinder().getObjectFacade().getObject(Service.class);
        Service service = launcher.getFacadeFinder().getObjectFacade().getObject(Service.class);
        service.test();
    }

    @Contribute
    @Scope(ObjectScope.PROTOTYPE)
    public Service createService(@Named("userDao") UserDao userDao)
    {
        return new DefaultService(userDao);
    }

    @Override
    public void bind(BindingBinder binder)
    {
        binder.bind(UserDao.class).id("userDao");
    }
}

class DefaultService implements Service
{
    private final UserDao userDao;

    DefaultService(UserDao userDao) {this.userDao = userDao;}

    @Override
    public void test()
    {
        this.userDao.test();
    }
}

class UserDao
{
    void test()
    {
        System.out.println(new Date());
    }
}