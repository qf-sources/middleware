package io.qiufen.module.injection.provider.processor;

import io.qiufen.module.injection.api.Contribute;
import io.qiufen.module.injection.api.Scope;
import io.qiufen.module.injection.provider.common.DataRegistry;
import io.qiufen.module.injection.provider.common.QualifierObjectEntry;
import io.qiufen.module.injection.provider.exception.ContributeException;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.api.declaration.Builder;
import io.qiufen.module.object.api.declaration.BuilderDeclaration;
import io.qiufen.module.object.api.facade.ObjectFacade;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import jakarta.inject.Singleton;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.List;

public class ContributeMethodProcessor extends InjectProcessor
{
    private final DataRegistry dataRegistry = DataRegistry.getInstance();

    @Override
    public void doProcess(Object prototype, Object decorated, FacadeFinder finder) throws Throwable
    {
        t(prototype.getClass(), prototype, finder);
    }

    @SuppressWarnings("rawtypes")
    private void t(Class clazz, Object prototype, FacadeFinder finder)
    {
        Class parentClass = clazz.getSuperclass();
        if (parentClass != null)
        {
            t(parentClass, prototype, finder);
        }

        Method[] methods = clazz.getDeclaredMethods();
        List<QualifierObjectEntry> entries = dataRegistry.getQualifierObjectEntries();
        for (Method method : methods)
        {
            Contribute ann = method.getAnnotation(Contribute.class);
            if (ann == null)
            {
                continue;
            }

            Class<?> returnType = method.getReturnType();
            if (returnType == Object.class)
            {
                throw new ContributeException("Can't use Object as return type!");
            }

            Builder builder = new PrivateBuilder(clazz, prototype, finder, method);
            BuilderDeclaration declaration = new BuilderDeclaration(returnType, builder);
            prepareQualifier(entries, method.getAnnotations(), method.getReturnType(), declaration);

            //处理scope声明
            if (method.isAnnotationPresent(Singleton.class))
            {
                declaration.setScope(ObjectScope.SINGLETON);
            }
            else if (method.isAnnotationPresent(Scope.class))
            {
                declaration.setScope(method.getAnnotation(Scope.class).value());
            }
            declaration.setLazy(true);
            finder.getFacade(ObjectFacade.class).addDeclaration(declaration);//todo 需要关联到模块到post上，即build
        }
    }

    private Object createObject(Class<?> clazz, Object prototype, FacadeFinder finder, Method method) throws Throwable
    {
        Type[] argTypes = method.getGenericParameterTypes();
        Annotation[][] anns = method.getParameterAnnotations();
        Object[] args = new Object[argTypes.length];
        for (int i = 0; i < argTypes.length; i++)
        {
            try
            {
                List<Annotation> qualifiers = getQualifiers(anns[i]);
                args[i] = getObject(qualifiers, argTypes[i], finder);
            }
            catch (Exception e)
            {
                throw new ContributeException(
                        "Class:" + clazz.getName() + ",method:" + method.getName() + ",argType:" + argTypes[i], e);
            }
        }

        try
        {
            int modifier = method.getModifiers();
            //public scope
            if (Modifier.isPublic(modifier))
            {
                return method.invoke(prototype, args);
            }

            //package scope
            if ((modifier << 31) == 0)
            {
                method.setAccessible(true);
                try
                {
                    return method.invoke(prototype, args);
                }
                finally
                {
                    method.setAccessible(false);
                }
            }
        }
        catch (InvocationTargetException e)
        {
            throw e.getTargetException();
        }

        throw new ContributeException("Can't contribute:" + method.getName());
    }

    private class PrivateBuilder implements Builder
    {
        private final Class<?> clazz;
        private final Object prototype;
        private final FacadeFinder finder;
        private final Method method;

        private PrivateBuilder(Class<?> clazz, Object prototype, FacadeFinder finder, Method method)
        {
            this.clazz = clazz;
            this.prototype = prototype;
            this.finder = finder;
            this.method = method;
        }

        @Override
        public Object build() throws Throwable
        {
            return createObject(clazz, prototype, finder, method);
        }
    }
}
