package io.qiufen.module.injection.provider.common;

import io.qiufen.module.injection.spi.qualifier.QualifierProcessor;
import jakarta.inject.Qualifier;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataRegistry
{
    private static final DataRegistry instance = new DataRegistry();

    private final List<QualifierObjectEntry> qualifierObjectEntries = new ArrayList<QualifierObjectEntry>();

    private final Map<Class, QualifierProcessor> processorMap = new HashMap<Class, QualifierProcessor>();

    private DataRegistry()
    {
    }

    public static DataRegistry getInstance()
    {
        return instance;
    }

    public List<QualifierObjectEntry> getQualifierObjectEntries()
    {
        return qualifierObjectEntries;
    }

    public QualifierProcessor findQualifierProcessor(Class<? extends Annotation> qualifierClass)
    {
        return this.processorMap.get(qualifierClass);
    }

    public void putQualifierProcessor(QualifierProcessor processor)
    {
        Class<? extends Annotation> qualifierClass = processor.supportType();
        if (!qualifierClass.isAnnotationPresent(Qualifier.class))
        {
            throw new IllegalArgumentException(
                    "Must qualified by @" + Qualifier.class.getName() + ",annotation type:" + qualifierClass.getName());
        }

        if (this.processorMap.containsKey(processor.supportType()))
        {
            throw new IllegalStateException("Duplication add qualifier @" + qualifierClass.getName());
        }
        this.processorMap.put(processor.supportType(), processor);
    }
}
