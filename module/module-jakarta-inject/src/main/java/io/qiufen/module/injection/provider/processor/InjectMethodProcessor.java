package io.qiufen.module.injection.provider.processor;

import io.qiufen.module.injection.provider.exception.InjectionException;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import jakarta.inject.Inject;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.List;

public class InjectMethodProcessor extends InjectProcessor
{
    @Override
    public void doProcess(Object prototype, Object decorated, FacadeFinder finder) throws Throwable
    {
        t(prototype.getClass(), prototype, finder);
    }

    @SuppressWarnings("rawtypes")
    private void t(Class clazz, Object prototype, FacadeFinder finder) throws Throwable
    {
        Class parentClass = clazz.getSuperclass();
        if (parentClass != null)
        {
            t(parentClass, prototype, finder);
        }
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods)
        {
            Inject ann = method.getAnnotation(Inject.class);
            if (ann == null)
            {
                continue;
            }

            Type[] argTypes = method.getGenericParameterTypes();
            Annotation[][] anns = method.getParameterAnnotations();
            Object[] args = new Object[argTypes.length];
            for (int i = 0; i < argTypes.length; i++)
            {
                try
                {
                    List<Annotation> qualifiers = getQualifiers(anns[i]);
                    args[i] = getObject(qualifiers, argTypes[i], finder);
                }
                catch (Exception e)
                {
                    throw new InjectionException(
                            "Class:" + clazz.getName() + ",method:" + method.getName() + ",argType:" + argTypes[i], e);
                }
            }

            try
            {
                int modifier = method.getModifiers();
                //public scope
                if (Modifier.isPublic(modifier))
                {
                    method.invoke(prototype, args);
                    continue;
                }

                //package scope
                if ((modifier << 31) == 0)
                {
                    method.setAccessible(true);
                    try
                    {
                        method.invoke(prototype, args);
                    }
                    finally
                    {
                        method.setAccessible(false);
                    }
                    continue;
                }
            }
            catch (InvocationTargetException e)
            {
                throw e.getTargetException();
            }

            throw new InjectionException("Can't invoke method:" + method.getName());
        }
    }
}