package io.qiufen.module.injection.provider.feature;

import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

@Handler(InjectionFeatureHandler.class)
public interface InjectionFeature extends Feature
{
    void bind(InjectionBinder binder);
}
