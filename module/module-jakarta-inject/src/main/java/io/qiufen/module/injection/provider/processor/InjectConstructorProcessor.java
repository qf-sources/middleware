package io.qiufen.module.injection.provider.processor;

import io.qiufen.module.injection.provider.exception.InjectionException;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.provider.support.event.ObjectConstructEvent;
import jakarta.inject.Inject;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.List;

public class InjectConstructorProcessor extends InjectProcessor
{
    @Override
    public void doCreate(ObjectConstructEvent event, FacadeFinder finder)
    {
        ClassDeclaration classDeclaration = event.getDeclaration();
        Class<?> clazz = classDeclaration.getClazz();
        Constructor<?>[] constructors = clazz.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors)
        {
            Type[] argTypes = constructor.getGenericParameterTypes();
            if (argTypes.length == 0 || !constructor.isAnnotationPresent(Inject.class))
            {
                continue;
            }

            Object[] args = new Object[argTypes.length];
            Annotation[][] anns = constructor.getParameterAnnotations();
            for (int i = 0; i < argTypes.length; i++)
            {
                try
                {
                    List<Annotation> qualifiers = getQualifiers(anns[i]);
                    args[i] = getObject(qualifiers, argTypes[i], finder);
                }
                catch (Exception e)
                {
                    throw new InjectionException("Constructor:" + constructor.getName() + ",argType:" + argTypes[i], e);
                }
            }
            event.setConstructor(constructor);
            event.setArguments(args);
            break;
        }
    }
}