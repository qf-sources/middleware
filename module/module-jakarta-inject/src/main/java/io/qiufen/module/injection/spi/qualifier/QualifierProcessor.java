package io.qiufen.module.injection.spi.qualifier;

import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.lang.annotation.Annotation;

public interface QualifierProcessor<Q extends Annotation>
{
    Class<Q> supportType();

    void doPrepare(Q qualifier, Declaration declaration);

    void doProcess(Q qualifier, Class<?> type, FacadeFinder finder, ObjectCondition condition) throws Exception;
}
