package io.qiufen.module.injection.provider.processor;

import io.qiufen.module.injection.api.Scope;
import io.qiufen.module.injection.provider.common.DataRegistry;
import io.qiufen.module.injection.provider.common.QualifierObjectEntry;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.api.declaration.ObjectDeclaration;
import io.qiufen.module.object.spi.declaration.Declaration;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import jakarta.inject.Singleton;

import java.util.List;

public class DeclarationPreparedProcessor extends InjectProcessor
{
    private final DataRegistry dataRegistry = DataRegistry.getInstance();

    @Override
    public void doPrepare(Declaration declaration)
    {
        List<QualifierObjectEntry> entries = dataRegistry.getQualifierObjectEntries();
        if (declaration instanceof ClassDeclaration)
        {
            prepareForClassDeclaration((ClassDeclaration) declaration, entries);
        }
        else if (declaration instanceof ObjectDeclaration)
        {
            Class<?> clazz = ((ObjectDeclaration) declaration).getObject().getClass();
            prepareQualifier(entries, clazz.getAnnotations(), clazz, declaration);
        }
    }

    private void prepareForClassDeclaration(ClassDeclaration declaration, List<QualifierObjectEntry> entries)
    {
        Class<?> clazz = declaration.getClazz();
        prepareQualifier(entries, clazz.getAnnotations(), clazz, declaration);

        //处理scope声明
        if (clazz.isAnnotationPresent(Singleton.class))
        {
            declaration.setScope(ObjectScope.SINGLETON);
        }
        else if (clazz.isAnnotationPresent(Scope.class))
        {
            declaration.setScope(clazz.getAnnotation(Scope.class).value());
        }
    }
}