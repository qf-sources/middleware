package io.qiufen.module.injection.spi.qualifier;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ObjectCondition
{
    private Set<Object> idSet;
    private Set<Class<?>> typeSet;
    private Set<Object> objectSet;

    public void putIdIntersection(Object id0)
    {
        this.idSet = putObjectIntersection(this.idSet, id0);
    }

    public void putTypeIntersection(Class<?> type0)
    {
        if (this.typeSet == null)
        {
            this.typeSet = new HashSet<Class<?>>();
            this.typeSet.add(type0);
            return;
        }
        Iterator<Class<?>> iterator = this.typeSet.iterator();
        while (iterator.hasNext())
        {
            Class<?> type = iterator.next();
            if (type0.isAssignableFrom(type))
            {
                continue;
            }
            iterator.remove();
        }
    }

    public void putObjectIntersection(Object object0)
    {
        this.objectSet = putObjectIntersection(this.objectSet, object0);
    }

    public void putIdSetIntersection(Collection<?> idSet0)
    {
        this.idSet = putObjectSetIntersection(this.idSet, idSet0);
    }

    public void putTypeSetIntersection(Collection<Class<?>> typeSet0)
    {
        if (this.typeSet == null)
        {
            this.typeSet = new HashSet<Class<?>>(typeSet0);
            return;
        }
        Iterator<Class<?>> iterator = this.typeSet.iterator();
        while (iterator.hasNext())
        {
            Class<?> type = iterator.next();
            boolean has = false;
            for (Class<?> type0 : typeSet0)
            {
                if (type0.isAssignableFrom(type))
                {
                    has = true;
                    break;
                }
            }
            if (!has) iterator.remove();
        }
    }

    public void putObjectSetIntersection(Collection<?> objectSet0)
    {
        this.objectSet = putObjectSetIntersection(this.objectSet, objectSet0);
    }

    public Set<Object> getIdSet()
    {
        return idSet;
    }

    public Set<Class<?>> getTypeSet()
    {
        return typeSet;
    }

    public Set<Object> getObjectSet()
    {
        return objectSet;
    }

    public void clear()
    {
        if (this.idSet != null)
        {
            this.idSet.clear();
            this.idSet = null;
        }

        if (this.typeSet != null)
        {
            this.typeSet.clear();
            this.typeSet = null;
        }

        if (this.objectSet != null)
        {
            this.objectSet.clear();
            this.objectSet = null;
        }
    }

    private Set<Object> putObjectIntersection(Set<Object> originObjSet, Object object0)
    {
        if (originObjSet == null)
        {
            Set<Object> set = new HashSet<Object>();
            set.add(object0);
            return set;
        }

        Iterator<Object> iterator = originObjSet.iterator();
        while (iterator.hasNext())
        {
            Object o = iterator.next();
            if (o.equals(object0))
            {
                continue;
            }
            iterator.remove();
        }
        return originObjSet;
    }

    private Set<Object> putObjectSetIntersection(Set<Object> originObjSet, Collection<?> objectSet)
    {
        if (originObjSet == null) return new HashSet<Object>(objectSet);

        Iterator<Object> iterator = originObjSet.iterator();
        while (iterator.hasNext())
        {
            Object o = iterator.next();
            if (objectSet.contains(o))
            {
                continue;
            }
            iterator.remove();
        }
        return originObjSet;
    }
}
