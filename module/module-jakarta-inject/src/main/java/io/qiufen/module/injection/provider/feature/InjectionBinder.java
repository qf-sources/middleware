package io.qiufen.module.injection.provider.feature;

import io.qiufen.module.injection.spi.qualifier.QualifierProcessor;
import io.qiufen.module.kernel.spi.feature.Binder;

import java.util.ArrayList;
import java.util.List;

public class InjectionBinder extends Binder
{
    private List<QualifierProcessorDeclaration> qualifierProcessorDeclarations;

    InjectionBinder()
    {
    }

    public void bind(Object processorId)
    {
        useList().add(new QualifierProcessorDeclaration(processorId, null));
    }

    public void bind(Class<? extends QualifierProcessor> processorClass)
    {
        useList().add(new QualifierProcessorDeclaration(null, processorClass));
    }

    List<QualifierProcessorDeclaration> getQualifierDeclarations()
    {
        return qualifierProcessorDeclarations;
    }

    protected void clear()
    {
        if (this.qualifierProcessorDeclarations != null)
        {
            this.qualifierProcessorDeclarations.clear();
        }
    }

    private List<QualifierProcessorDeclaration> useList()
    {
        if (this.qualifierProcessorDeclarations == null)
        {
            this.qualifierProcessorDeclarations = new ArrayList<QualifierProcessorDeclaration>();
        }
        return this.qualifierProcessorDeclarations;
    }
}
