package io.qiufen.module.injection.provider.exception;

import io.qiufen.module.kernel.spi.exception.ModuleException;

public class ContributeException extends ModuleException
{
    public ContributeException(String message)
    {
        super(message);
    }

    public ContributeException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
