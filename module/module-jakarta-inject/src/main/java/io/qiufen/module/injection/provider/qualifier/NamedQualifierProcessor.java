package io.qiufen.module.injection.provider.qualifier;

import io.qiufen.common.lang.Strings;
import io.qiufen.module.injection.spi.qualifier.ObjectCondition;
import io.qiufen.module.injection.spi.qualifier.QualifierProcessorAdapter;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.spi.declaration.Declaration;
import jakarta.inject.Named;

import java.util.Arrays;

public class NamedQualifierProcessor extends QualifierProcessorAdapter<Named>
{
    @Override
    public Class<Named> supportType()
    {
        return Named.class;
    }

    @Override
    public void doPrepare(Named qualifier, Declaration declaration)
    {
        if (Strings.isEmpty(qualifier.value())) return;

        Object[] idList = declaration.getIdList();
        int length = idList.length;
        Object[] newIdList = Arrays.copyOf(idList, length + 1);
        newIdList[length] = qualifier.value();
        declaration.setIdList(newIdList);
    }

    @Override
    public void doProcess(Named qualifier, Class<?> type, FacadeFinder finder, ObjectCondition condition)
    {
        condition.putIdIntersection(qualifier.value());
    }
}
