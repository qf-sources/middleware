package io.qiufen.module.injection.provider.qualifier;

import io.qiufen.common.conversion.TypeConversionService;
import io.qiufen.module.injection.api.qualifier.Id;
import io.qiufen.module.injection.spi.qualifier.ObjectCondition;
import io.qiufen.module.injection.spi.qualifier.QualifierProcessorAdapter;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.Arrays;

public class IdQualifierProcessor extends QualifierProcessorAdapter<Id>
{
    @Override
    public Class<Id> supportType()
    {
        return Id.class;
    }

    @Override
    public void doPrepare(Id qualifier, Declaration declaration)
    {
        Object id = TypeConversionService.getInstance().convert(qualifier.value(), qualifier.type());
        Object[] idList = declaration.getIdList();
        int length = idList.length;
        Object[] newIdList = Arrays.copyOf(idList, length + 1);
        newIdList[length] = id;
        declaration.setIdList(newIdList);
    }

    @Override
    public void doProcess(Id qualifier, Class<?> type, FacadeFinder finder, ObjectCondition condition)
    {
        condition.putIdIntersection(TypeConversionService.getInstance().convert(qualifier.value(), qualifier.type()));
    }
}
