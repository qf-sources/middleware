package io.qiufen.module.injection.provider.feature;

import io.qiufen.module.injection.spi.qualifier.QualifierProcessor;

public class QualifierProcessorDeclaration
{
    private final Object processorId;

    private final Class<? extends QualifierProcessor> processorClass;

    QualifierProcessorDeclaration(Object processorId, Class<? extends QualifierProcessor> processorClass)
    {
        this.processorId = processorId;
        this.processorClass = processorClass;
    }

    Object getProcessorId()
    {
        return processorId;
    }

    Class<? extends QualifierProcessor> getProcessorClass()
    {
        return processorClass;
    }
}
