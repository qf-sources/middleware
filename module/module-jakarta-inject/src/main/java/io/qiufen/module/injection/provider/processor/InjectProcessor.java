package io.qiufen.module.injection.provider.processor;

import io.qiufen.common.lang.Arrays;
import io.qiufen.module.injection.provider.common.DataRegistry;
import io.qiufen.module.injection.provider.common.QualifierObjectEntry;
import io.qiufen.module.injection.provider.exception.InjectionException;
import io.qiufen.module.injection.spi.qualifier.ObjectCondition;
import io.qiufen.module.injection.spi.qualifier.QualifierProcessor;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.api.facade.ObjectFacade;
import io.qiufen.module.object.spi.declaration.Declaration;
import io.qiufen.module.processor.spi.ProcessorAdapter;
import jakarta.inject.Provider;
import jakarta.inject.Qualifier;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public abstract class InjectProcessor extends ProcessorAdapter
{
    protected Object getObject(List<Annotation> qualifiers, Type baseType, FacadeFinder finder) throws Exception
    {
        if (baseType instanceof Class)
        {
            return getObject(qualifiers, (Class<?>) baseType, finder);
        }

        if (baseType instanceof ParameterizedType)
        {
            return getObject(qualifiers, (ParameterizedType) baseType, finder);
        }

        throw new UnsupportedOperationException();
    }

    private Object getObject(List<Annotation> qualifiers, Class<?> baseType, FacadeFinder finder) throws Exception
    {
        //1.Provider类型
        if (Provider.class.isAssignableFrom(baseType))
        {
            Class<?> actualType = Object.class;
            Type[] inters = baseType.getGenericInterfaces();
            for (Type inter : inters)
            {
                if (inter instanceof ParameterizedType)
                {
                    ParameterizedType type = ((ParameterizedType) inter);
                    if (Provider.class.isAssignableFrom((Class<?>) type.getRawType()))
                    {
                        Type[] actualTypes = type.getActualTypeArguments();
                        actualType = actualTypes.length == 0 ? actualType : (Class<?>) actualTypes[0];
                        break;
                    }
                }
            }
            return getProvider(qualifiers, baseType, actualType, finder);
        }
        //2.非Provider类型，返回对象
        return getObject0(qualifiers, baseType, finder);
    }

    private Object getObject(List<Annotation> qualifiers, ParameterizedType type, FacadeFinder finder) throws Exception
    {
        Class<?> rawType = (Class<?>) type.getRawType();
        //1.Provider类型
        if (Provider.class.isAssignableFrom(rawType))
        {
            Type[] actualTypes = type.getActualTypeArguments();
            Class<?> actualType = actualTypes.length == 0 ? Object.class : (Class<?>) actualTypes[0];
            return getProvider(qualifiers, rawType, actualType, finder);
        }
        //2.非Provider类型，返回对象
        return getObject0(qualifiers, rawType, finder);
    }

    private Object getProvider(List<Annotation> qualifiers, Class<?> baseType, Class<?> actualType, FacadeFinder finder)
    {
        //1.1.检查存在Provider类型声明
        if (finder.getFacade(ObjectFacade.class).hasDeclaration(Provider.class))
        {
            try
            {
                //1.1.1.尝试按照Provider类型取得对象
                return getObject0(qualifiers, baseType, finder);
            }
            catch (Exception e)
            {
                //1.1.2.尝试失败，返回构造私有Provider对象
                return new PrivateProvider(qualifiers, actualType, finder);
            }
        }
        //1.2.检查不存在Provider类型声明，返回构造私有Provider对象
        return new PrivateProvider(qualifiers, actualType, finder);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected Object getObject0(List<Annotation> qualifiers, Class<?> baseType, FacadeFinder finder) throws Exception
    {
        ObjectFacade objectFacade = finder.getFacade(ObjectFacade.class);
        if (qualifiers == null)
        {
            //按照类型
            return objectFacade.getObject(baseType);
        }

        ObjectCondition condition = new ObjectCondition();
        for (Annotation qualifier : qualifiers)
        {
            //检查是否注册修饰器处理器
            QualifierProcessor qualifierProcessor = DataRegistry.getInstance()
                    .findQualifierProcessor(qualifier.annotationType());
            if (qualifierProcessor == null)
            {
                //映射表匹配对象引用
                List<Object> idList = matchId(qualifier, baseType, objectFacade);
                condition.putIdSetIntersection(idList);
                continue;
            }
            //按照修饰器处理
            qualifierProcessor.doProcess(qualifier, baseType, finder, condition);
        }
        try
        {
            return getObject(condition, baseType, objectFacade);
        }
        finally
        {
            condition.clear();
        }
    }

    protected List<Annotation> getQualifiers(Annotation[] annotations)
    {
        List<Annotation> qualifiers = null;
        for (Annotation annotation : annotations)
        {
            if (annotation.annotationType().isAnnotationPresent(Qualifier.class))
            {
                if (qualifiers == null) qualifiers = new ArrayList<Annotation>();
                qualifiers.add(annotation);
            }
        }
        return qualifiers;
    }

    protected List<Object> matchId(Annotation qualifier, Class<?> type, ObjectFacade facade)
    {
        List<Object> idList = null;
        List<QualifierObjectEntry> entries = DataRegistry.getInstance().getQualifierObjectEntries();
        for (QualifierObjectEntry entry : entries)
        {
            if (equals(entry.getQualifier(), qualifier) && type.isAssignableFrom(entry.getType()))
            {
                (idList == null ? (idList = new ArrayList<Object>()) : idList).add(entry.getId());
            }
        }

        if (idList == null)
        {
            throw new InjectionException("Can't match qualifier:" + qualifier.annotationType().getName());
        }
        return idList;
    }

    protected Object getObject(ObjectCondition condition, Class<?> baseType, ObjectFacade objectFacade)
    {
        //候选
        List<Object> list = new ArrayList<Object>();
        tObject(list, condition.getObjectSet(), baseType);
        tId(list, condition.getIdSet(), baseType, objectFacade);
        tType(list, condition.getTypeSet(), baseType, objectFacade);
        if (list.isEmpty()) throw new InjectionException("None object injected!");
        if (list.size() > 1) throw new InjectionException("None unique object injected!");
        return list.get(0);
    }

    protected boolean equals(Annotation ann0, Annotation ann1)
    {
        return ann0.equals(ann1);
    }

    private void tObject(List<Object> list, Set<Object> objectSet, Class<?> baseType)
    {
        if (objectSet == null || objectSet.isEmpty()) return;

        for (Object o : objectSet)
        {
            //如果是原型，直接add即可，非原型则做一次cast
            list.add(baseType.isPrimitive() ? o : baseType.cast(o));
        }
    }

    private void tId(List<Object> list, Set<Object> idSet, Class<?> baseType, ObjectFacade objectFacade)
    {
        if (idSet == null || idSet.isEmpty()) return;

        if (list.isEmpty())
        {
            for (Object id : idSet)
            {
                list.add(objectFacade.getObject(id, baseType));
            }
            return;
        }

        List<Object> contains = new ArrayList<Object>();
        for (Object id : idSet)
        {
            Object o = objectFacade.getObject(id, baseType);
            if (list.contains(o))
            {
                contains.add(o);
            }
        }
        if (contains.isEmpty()) throw new InjectionException("None object injected!");

        list.clear();
        list.add(contains);
        contains.clear();
    }

    private void tType(List<Object> list, Set<Class<?>> typeSet, Class<?> baseType, ObjectFacade objectFacade)
    {
        if (typeSet == null || typeSet.isEmpty()) return;

        if (list.isEmpty())
        {
            for (Class<?> type : typeSet)
            {
                list.add(baseType.cast(objectFacade.getObject(type)));
            }
            return;
        }

        //排除
        Iterator<Object> iterator = list.iterator();
        while (iterator.hasNext())
        {
            Object o = iterator.next();
            boolean has = false;
            for (Class<?> type : typeSet)
            {
                if (type.isInstance(o))
                {
                    has = true;
                    break;
                }
            }
            if (!has)
            {
                iterator.remove();
            }
        }
        if (list.isEmpty()) throw new InjectionException("None object injected!");
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    protected void prepareQualifier(List<QualifierObjectEntry> entries, Annotation[] annotations, Class<?> targetType,
            Declaration declaration)
    {
        Object[] defaultIds = declaration.getIdList();
        //置空id，修饰注解可提供id
        declaration.setIdList(Arrays.EMPTY);
        for (Annotation annotation : annotations)
        {
            Class<? extends Annotation> qualifierAnnType = annotation.annotationType();
            if (qualifierAnnType.isAnnotationPresent(Qualifier.class))
            {
                QualifierProcessor qualifierProcessor = DataRegistry.getInstance()
                        .findQualifierProcessor(qualifierAnnType);
                if (qualifierProcessor == null)
                {
                    entries.add(new QualifierObjectEntry(annotation, defaultIds[0], targetType));
                }
                else
                {
                    qualifierProcessor.doPrepare(annotation, declaration);
                }
            }
        }
        //未获得id，则设置默认id
        if (declaration.getIdList() == Arrays.EMPTY)
        {
            declaration.setIdList(defaultIds);
        }
    }

    /**
     * 私有化的对象提供者实现
     */
    @SuppressWarnings("rawtypes")
    private class PrivateProvider implements Provider
    {
        private final List<Annotation> qualifiers;
        private final Class<?> actualType;
        private final FacadeFinder finder;

        private PrivateProvider(List<Annotation> qualifiers, Class<?> actualType, FacadeFinder finder)
        {
            this.qualifiers = qualifiers;
            this.actualType = actualType;
            this.finder = finder;
        }

        @Override
        public Object get()
        {
            try
            {
                return getObject0(this.qualifiers, this.actualType, this.finder);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
    }
}