package io.qiufen.module.injection.provider.feature;

import io.qiufen.module.injection.provider.common.DataRegistry;
import io.qiufen.module.injection.spi.qualifier.QualifierProcessor;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.feature.BindableFeatureHandler;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.spi.declaration.ObjectScope;

import java.util.List;

public class InjectionFeatureHandler extends BindableFeatureHandler<InjectionBinder, InjectionFeature>
{
    @Override
    protected InjectionBinder initBinder(InjectionFeature feature, ContextFactory contextFactory)
    {
        return new InjectionBinder();
    }

    @Override
    protected void doHandle(InjectionFeature feature, ContextFactory contextFactory, InjectionBinder binder)
    {
        feature.bind(binder);
        List<QualifierProcessorDeclaration> qualifierProcessorDeclarations = binder.getQualifierDeclarations();
        if (qualifierProcessorDeclarations != null)
        {
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            for (QualifierProcessorDeclaration declaration : qualifierProcessorDeclarations)
            {
                Object processorId = declaration.getProcessorId();
                Class<? extends QualifierProcessor> processorClass = declaration.getProcessorClass();
                if (processorId != null)
                {
                    objectContext.getObject(processorId, QualifierProcessor.class);
                    continue;
                }
                if (processorClass != null)
                {
                    if (!objectContext.hasDeclaration(processorClass))
                    {
                        ClassDeclaration classDeclaration = new ClassDeclaration(processorClass);
                        classDeclaration.setScope(ObjectScope.ONCE);
                        objectContext.addDeclaration(classDeclaration);
                    }
                }
            }
        }
    }

    @Override
    protected void postModule(InjectionFeature feature, ContextFactory contextFactory, InjectionBinder binder)
    {
        List<QualifierProcessorDeclaration> qualifierProcessorDeclarations = binder.getQualifierDeclarations();
        if (qualifierProcessorDeclarations != null && !qualifierProcessorDeclarations.isEmpty())
        {
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            for (QualifierProcessorDeclaration declaration : qualifierProcessorDeclarations)
            {
                Object processorId = declaration.getProcessorId();
                Class<? extends QualifierProcessor> processorClass = declaration.getProcessorClass();
                QualifierProcessor processor;
                if (processorId != null)
                {
                    processor = objectContext.getObject(processorId, QualifierProcessor.class);
                }
                else if (processorClass != null)
                {
                    processor = objectContext.getObject(processorClass);
                }
                else
                {
                    throw new IllegalArgumentException();
                }
                DataRegistry.getInstance().putQualifierProcessor(processor);
            }
        }
    }
}
