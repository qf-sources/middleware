package io.qiufen.module.injection.provider.common;

import java.lang.annotation.Annotation;

public class QualifierObjectEntry
{
    private final Annotation qualifier;
    private final Object id;
    private final Class<?> type;

    public QualifierObjectEntry(Annotation qualifier, Object id, Class<?> type)
    {
        this.qualifier = qualifier;
        this.id = id;
        this.type = type;
    }

    public Annotation getQualifier()
    {
        return qualifier;
    }

    public Object getId()
    {
        return id;
    }

    public Class<?> getType()
    {
        return type;
    }
}
