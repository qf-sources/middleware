package io.qiufen.module.injection.provider.processor;

import io.qiufen.module.injection.provider.exception.InjectionException;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import jakarta.inject.Inject;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;

public class InjectFieldProcessor extends InjectProcessor
{
    @Override
    public void doProcess(Object prototype, Object decorated, FacadeFinder finder) throws Throwable
    {
        t(prototype.getClass(), prototype, finder);
    }

    @SuppressWarnings("rawtypes")
    private void t(Class clazz, Object prototype, FacadeFinder finder)
    {
        Class parentClass = clazz.getSuperclass();
        if (parentClass != null)
        {
            t(parentClass, prototype, finder);
        }
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields)
        {
            try
            {
                doField(prototype, field, finder);
            }
            catch (Exception e)
            {
                throw new InjectionException("class:" + clazz.getName() + ",field:" + field.getName(), e);
            }
        }
    }

    private void doField(Object prototype, Field field, FacadeFinder finder) throws Exception
    {
        Inject ann = field.getAnnotation(Inject.class);
        if (ann == null)
        {
            return;
        }

        List<Annotation> qualifiers = getQualifiers(field.getAnnotations());
        Object obj = getObject(qualifiers, field.getGenericType(), finder);
        if (field.isAccessible())
        {
            field.set(prototype, obj);
        }
        else
        {
            field.setAccessible(true);
            field.set(prototype, obj);
            field.setAccessible(false);
        }
    }
}