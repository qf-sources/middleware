/**
 * 注入服务模块
 * spi      -   客户端扩展接口<br/>
 * processor  - 对象处理器实现<br/>
 */
package io.qiufen.module.injection;