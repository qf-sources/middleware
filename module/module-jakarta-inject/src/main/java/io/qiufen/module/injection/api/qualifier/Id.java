package io.qiufen.module.injection.api.qualifier;

import jakarta.inject.Qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface Id
{
    String value();

    Class<?> type() default String.class;
}
