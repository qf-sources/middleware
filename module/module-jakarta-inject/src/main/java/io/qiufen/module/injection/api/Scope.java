package io.qiufen.module.injection.api;

import io.qiufen.module.object.spi.declaration.ObjectScope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@jakarta.inject.Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface Scope
{
    ObjectScope value() default ObjectScope.SINGLETON;
}
