package io.qiufen.module.injection.provider.exception;

import io.qiufen.module.kernel.spi.exception.ModuleException;

public class InjectionException extends ModuleException
{
    public InjectionException(String message)
    {
        super(message);
    }

    public InjectionException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
