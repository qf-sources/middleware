package io.qiufen.module.injection.spi.qualifier;

import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.lang.annotation.Annotation;

public class QualifierProcessorAdapter<Q extends Annotation> implements QualifierProcessor<Q>
{
    @Override
    public Class<Q> supportType()
    {
        return null;
    }

    @Override
    public void doPrepare(Q qualifier, Declaration declaration)
    {
    }

    @Override
    public void doProcess(Q qualifier, Class<?> type, FacadeFinder finder, ObjectCondition condition) throws Exception
    {
    }
}
