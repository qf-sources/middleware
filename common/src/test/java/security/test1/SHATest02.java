package security.test1;

import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHATest02
{
    public static void main(String[] args) throws NoSuchAlgorithmException
    {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.update("0".getBytes());
        System.out.println(Hex.encodeHexString(digest.digest()));
        digest.update("0".getBytes());
        System.out.println(Hex.encodeHexString(digest.digest()));
    }
}
