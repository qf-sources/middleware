package security.test1;

import io.qiufen.common.security.codec.Encoder;
import io.qiufen.common.security.codec.HexDecoder;
import io.qiufen.common.security.codec.Output;
import io.qiufen.common.security.security.SHA256Security;
import io.qiufen.common.security.security.Security;
import security.blake.Blake3Test01;

import java.io.IOException;

public class SHA256Test01
{
    public static void main(String[] args) throws IOException
    {
        byte[][] data = Blake3Test01.getData();
        Security s = new SHA256Security();

        Encoder<byte[][]> encoder = new Encoder<byte[][]>()
        {
            @Override
            public void encode(byte[][] data, Output output)
            {
                for (byte[] bytes : data)
                {
                    output.write(bytes, 0, bytes.length);
                }
            }
        };

        s.encrypt(data, encoder, HexDecoder.DEFAULT);

        long p0 = System.currentTimeMillis();
        System.out.println(s.encrypt(data, encoder, HexDecoder.DEFAULT));
        long p1 = System.currentTimeMillis();
        System.out.println(p1 - p0);
    }
}
