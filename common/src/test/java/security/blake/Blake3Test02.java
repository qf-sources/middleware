package security.blake;

import io.qiufen.common.security.provider.blake.Blake3;
import org.apache.commons.codec.binary.Hex;

//2aa7361175b011f21a3a98a2f1c7f0fa09cb9b31f7b64196a7c0cd4bfedabe69
public class Blake3Test02
{
    public static void main(String[] args)
    {
        byte[] bytes = "Vector API (Sixth Incubator) 是 Java 平台的一个项目，旨在提供一种简单且高效的方式来执行向量化计算。 它基于 SIMD 指令集，并通过引入新的类和接口来支持并行计算。 使用 Vector API (Sixth Incubator) 可以提高应用程序的性能，但需要注意硬件兼容性和适当的性能优化。".getBytes();
        Blake3 blake3 = Blake3.newInstance();
        blake3.update(bytes, 0, bytes.length);
        System.out.println(Hex.encodeHexString(blake3.digest(16)));
        System.out.println(Hex.encodeHexString(blake3.digest(32)));
        System.out.println(Hex.encodeHexString(blake3.digest(64)));
    }
}
