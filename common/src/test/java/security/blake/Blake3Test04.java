package security.blake;

import io.qiufen.common.security.provider.blake.Blake3;
import org.apache.commons.codec.binary.Hex;

import java.io.IOException;
import java.nio.ByteBuffer;

public class Blake3Test04
{
    public static void main(String[] args) throws IOException
    {
        byte[][] data = getData();
        test2(data);
        long p0 = System.currentTimeMillis();
        test2(data);
        long p1 = System.currentTimeMillis();
        System.out.println(p1 - p0);
    }

    static void test2(byte[][] data)
    {
        Blake3 blake3 = Blake3.newInstance();
        for (byte[] bytes : data)
        {
            blake3.update(ByteBuffer.wrap(bytes));
        }
        byte[] out = blake3.digest(32);
        System.out.println(Hex.encodeHexString(out));
    }

    public static byte[][] getData()
    {
        byte[][] buffer = new byte[2][];
        byte[] buffer0 = new byte[1 << 30];
        byte[] buffer1 = new byte[1 << 30];
        buffer[0] = buffer0;
        buffer[1] = buffer1;
        return buffer;
    }
}
