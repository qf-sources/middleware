package security.blake;

import io.qiufen.common.lang.JsonObject;
import io.qiufen.common.security.provider.blake.Blake3;
import io.qiufen.common.security.provider.blake.Blake3State;
import org.apache.commons.codec.binary.Hex;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Blake3Test06
{
    public static void main(String[] args) throws IOException
    {
        test();
        long p0 = System.currentTimeMillis();
        test();
        long p1 = System.currentTimeMillis();
        System.out.println(p1 - p0);
    }

    static void test() throws IOException
    {
        Blake3 blake3 = Blake3.newInstance();
        //        String name = "/Users/zhangruzheng/Downloads/SW_DVD9_Win_Pro_11_21H2_64ARM_ChnSimp_Pro_Ent_EDU_N_MLF_-2_X22-82731.ISO";
        String name = "C:\\Users\\Ruzheng Zhang\\Desktop\\HDC.vmdk";
        FileChannel channel = new FileInputStream(name).getChannel();
        ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 1024);
        while (channel.read(buffer) != -1)
        {
            buffer.flip();
            blake3.update(buffer);
            buffer.flip();
            blake3 = Blake3.of(
                    JsonObject.<Blake3State>to(JsonObject.toJsonBytes(blake3.getState()), Blake3State.class));
        }
        System.out.println(Hex.encodeHexString(blake3.digest()));
    }
}
