package security.blake;

import io.qiufen.common.security.provider.blake.Blake3;
import org.apache.commons.codec.binary.Hex;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Blake3Test03
{
    public static void main(String[] args) throws IOException
    {
        test();
        long p0 = System.currentTimeMillis();
        test();
        long p1 = System.currentTimeMillis();
        System.out.println(p1 - p0);
    }

    static void test() throws IOException
    {
        Blake3 blake3 = Blake3.newInstance();
        String name = "/Users/zhangruzheng/Downloads/SW_DVD9_Win_Pro_11_21H2_64ARM_ChnSimp_Pro_Ent_EDU_N_MLF_-2_X22-82731.ISO";
//        String name = "C:\\Users\\Ruzheng Zhang\\Desktop\\HDC.vmdk";
        InputStream stream = new FileInputStream(name);
        byte[] buffer = new byte[1024 * 1024];
        int n;
        while ((n = stream.read(buffer)) != -1)
        {
            blake3.update(buffer, 0, n);
        }
        System.out.println(Hex.encodeHexString(blake3.digest()));
    }
}
