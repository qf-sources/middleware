package security;

import io.qiufen.common.security.codec.HexDecoder;
import io.qiufen.common.security.codec.StringEncoder;
import io.qiufen.common.security.security.MD5Security;
import io.qiufen.common.security.security.SecurityTemplate;

public class TemplateTest
{
    public static void main(String[] args)
    {
        SecurityTemplate<String, String> template = new SecurityTemplate<String, String>(new MD5Security());
        template.setEncryptionCodec(StringEncoder.DEFAULT, HexDecoder.DEFAULT);
        System.out.println(template.encrypt(""));
        System.out.println(template.decrypt(""));
    }
}
