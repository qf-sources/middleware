package security;

import io.qiufen.common.security.codec.Callback;
import io.qiufen.common.security.codec.HexDecoder;
import io.qiufen.common.security.codec.Output;
import io.qiufen.common.security.codec.StringEncoder;

public class HexTest
{
    public static void main(String[] args)
    {
        final StringEncoder encoder = StringEncoder.DEFAULT;
        HexDecoder decoder = HexDecoder.DEFAULT;
        Callback callback = new Callback()
        {
            @Override
            public void call(final Output output)
            {
                encoder.encode("老张见老张", new Output()
                {
                    @Override
                    public void write(byte[] bytes, int offset, int length)
                    {
                        output.write(bytes, offset, length);
                    }
                });
            }
        };
        String hex = decoder.decode(callback);
        System.out.println(hex);
    }
}
