package filter;

import io.qiufen.common.filter.*;

interface TextContext extends Context
{
    void text();
}

interface TestContext2 extends Context
{
    void test();
}

public class TestFilter
{
    public static void main(String[] args) throws Throwable
    {
        FilterRegistry registry = new FilterRegistry("test");
        registry.addFilter(new Filter2());
        registry.addFilter(new Filter1());
        registry.setService(new Service()
        {
            @Override
            public void doService(Context context)
            {
                System.out.println("service");
            }
        });
        registry.submit(new TestContext());
    }
}

class TestContext implements TextContext, TestContext2
{
    private final TextContextDto textContextDto = new TextContextDto();
    private final TestContext2Dto testContext2Dto = new TestContext2Dto();

    @Override
    public void text()
    {
        textContextDto.text();
    }

    @Override
    public void test()
    {
        testContext2Dto.test();
    }
}

class TextContextDto implements TextContext
{
    @Override
    public void text()
    {
        System.out.println("text");
    }
}

class Filter1 implements Filter<TextContext>
{
    @Override
    public void doFilter(FilterChain chain, TextContext context) throws Throwable
    {
        context.text();
        chain.doFilter(context);
    }
}

class TestContext2Dto implements TestContext2
{
    @Override
    public void test()
    {
        System.out.println("test");
    }
}

class Filter2 extends AcceptableFilter<TestContext2>
{
    @Override
    public void doFilter(FilterChain chain, TestContext2 context) throws Throwable
    {
        context.test();
        chain.doFilter(context);
    }
}
