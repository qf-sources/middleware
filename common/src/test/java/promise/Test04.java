package promise;

import io.qiufen.common.promise.api.Promise;
import io.qiufen.common.promise.provider.Promises;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Test04
{
    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException
    {
        final Promise<Date> promise = Promises.create();
        new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e)
                {
                    throw new RuntimeException(e);
                }
                promise.resolve(new Date());
            }
        }.start();
        System.out.println("1 = " + promise.future().get(2000, TimeUnit.MILLISECONDS));
        System.out.println("2 = " + promise.future().get());
    }
}
