//package promise;
//
//import io.qiufen.common.promise.api.Context;
//import io.qiufen.common.promise.api.Promise;
//import io.qiufen.common.promise.provider.BatchValue;
//import io.qiufen.common.promise.provider.Promises;
//import io.qiufen.common.promise.spi.Rejected;
//import io.qiufen.common.promise.spi.Resolved;
//
//import java.util.Date;
//import java.util.Map;
//import java.util.Properties;
//import java.util.UUID;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.locks.LockSupport;
//
//public class Test02
//{
//    public static void main(String[] args)
//    {
//        final Promise<Date> p0 = func0();
//        final Promise<UUID> p1 = func1();
//        final Promise<Properties> p2 = func2();
//        Promises.both(p0, p1, p2).future().then(new Resolved<Map<Promise<?>, BatchValue>, Void>()
//        {
//            @Override
//            public void onResolved(Context<Void> context, Map<Promise<?>, BatchValue> values)
//            {
//                System.out.println(Thread.currentThread().getName() + ":batchValue = " + values.get(p0));
//                System.out.println(Thread.currentThread().getName() + ":batchValue = " + values.get(p1));
//                System.out.println(Thread.currentThread().getName() + ":batchValue = " + values.get(p2));
//            }
//        }, new Rejected<Void>()
//        {
//            @Override
//            public void onRejected(Context<Void> context, Object value)
//            {
//                ((Exception) value).printStackTrace();
//            }
//        });
//    }
//
//    static Promise<Date, Object> func0()
//    {
//        final Promise<Date, Object> promise = Promises.create();
//        new Thread()
//        {
//            @Override
//            public void run()
//            {
//                LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(1));
//                promise.resolve(new Date());
//            }
//        }.start();
//        return promise;
//    }
//
//    static Promise<UUID> func1()
//    {
//        final Promise<UUID> promise = Promises.create();
//        new Thread()
//        {
//            @Override
//            public void run()
//            {
//                LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(2));
//                promise.resolve(UUID.randomUUID());
//            }
//        }.start();
//        return promise;
//    }
//
//    static Promise<Properties> func2()
//    {
//        final Promise<Properties> promise = Promises.create();
//        new Thread()
//        {
//            @Override
//            public void run()
//            {
//                LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(2));
//                promise.resolve(System.getProperties());
//            }
//        }.start();
//        return promise;
//    }
//}
//
