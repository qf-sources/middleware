//package promise;
//
//import io.qiufen.common.promise.api.Context;
//import io.qiufen.common.promise.api.Future;
//import io.qiufen.common.promise.api.Promise;
//import io.qiufen.common.promise.provider.Promises;
//import io.qiufen.common.promise.spi.Rejected;
//import io.qiufen.common.promise.spi.Resolved;
//
//import java.util.Date;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.locks.LockSupport;
//
//public class Test01
//{
//    public static void main(String[] args)
//    {
//        Promise<Date> promise = Promises.create();
//        Future promise1 = promise.future().then(new Resolved<Date, Date>()
//        {
//            @Override
//            public void onResolved(Context<Date> context, final Date value)
//            {
//                final Promise<Date> promise0 = Promises.create();
//                new Thread()
//                {
//                    @Override
//                    public void run()
//                    {
//                        LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(3));
//                        promise0.resolve(value);
//                    }
//                }.start();
//                context.resolve(promise0.future());
//            }
//        }, new Rejected<Date>()
//        {
//            @Override
//            public void onRejected(Context<Date> context, Object value) throws Exception
//            {
//            }
//        }).then(new Resolved<Date, Date>()
//        {
//            @Override
//            public void onResolved(Context<Date> context, Date value)
//            {
//                context.resolve(value);
//            }
//        }, new Rejected<Date>()
//        {
//            @Override
//            public void onRejected(Context<Date> context, Object value) throws Exception
//            {
//            }
//        }).then(new Resolved<Date, Date>()
//        {
//            @Override
//            public void onResolved(Context<Date> context, Date value)
//            {
//                context.resolve(value);
//                System.out.println(value);
//            }
//        }, new Rejected<Date>()
//        {
//            @Override
//            public void onRejected(Context<Date> context, Object value)
//            {
//                ((Exception) value).printStackTrace();
//            }
//        });
//        promise1.then(new Resolved<Object, Object>()
//        {
//            @Override
//            public void onResolved(Context<Object> context, Object value)
//            {
//                System.out.println(value);
//                context.resolve(value);
//            }
//        });
//        promise.resolve(new Date());
//    }
//}
//
