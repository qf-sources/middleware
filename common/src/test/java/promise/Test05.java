package promise;

import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.api.Promise;
import io.qiufen.common.promise.provider.Promises;
import io.qiufen.common.promise.spi.Resolved;

import java.util.Date;

public class Test05
{
    public static void main(String[] args)
    {
        Promise promise = Promises.create();
        promise.future().then(new Resolved()
        {
            @Override
            public void onResolved(Context context, Object value) throws Exception
            {
                System.out.println("value1 = " + value);
                Promise promise1 = Promises.create();
                context.resolve(promise1.future());
                Thread.sleep(1000);
                promise1.resolve(new Date());
            }
        }).then(new Resolved()
        {
            @Override
            public void onResolved(Context context, Object value) throws Exception
            {
                System.out.println("value2 = " + value);
                context.resolve(value);
            }
        });
        promise.resolve(new Date());
    }
}
