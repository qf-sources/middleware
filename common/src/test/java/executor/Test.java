package executor;

import io.qiufen.common.concurrency.executor.api.BusyPriority;
import io.qiufen.common.concurrency.executor.api.ExecutorConfig;
import io.qiufen.common.concurrency.executor.api.ExecutorServiceFactory;
import io.qiufen.common.concurrency.executor.api.PoolConfig;
import io.qiufen.common.concurrency.executor.provider.DefaultExecutorServiceFactoryBuilder;
import io.qiufen.common.concurrency.executor.spi.Predictable;
import io.qiufen.common.concurrency.executor.spi.PredictionHandler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class Test
{
    public static void main(String[] args)
    {
        PoolConfig poolConfig = new PoolConfig();
        poolConfig.setMinThreads(4);
        poolConfig.setMaxThreads(20);
        poolConfig.setIdleDuration(30000);
        final ExecutorServiceFactory context = new DefaultExecutorServiceFactoryBuilder().setPoolConfig(poolConfig)
                .build();

        for (int i = 0; i < 20; i++)
        {
            test_0(context, i);
        }

        new Thread()
        {
            @Override
            public void run()
            {
                while (true)
                {
                    LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(1500));
                    System.out.println("concurrent:" + context.watcher().concurrent());
                    System.out.println("queueDepth:" + context.watcher().queueDepth());
                    System.out.println("countRecycles:" + context.watcher().countRecycles());
                    System.out.println("countWorkers:" + context.watcher().countWorkers());
                }
            }
        }.start();

        for (int i = 0; i < 20; i++)
        {
            final int finalI = i;
            new Thread()
            {
                @Override
                public void run()
                {
                    test0(context, finalI);
                }
            }.start();
        }
    }

    private static void test_0(ExecutorServiceFactory context, int group)
    {
        ExecutorConfig executorConfig = new ExecutorConfig();
        executorConfig.setMinConcurrent(5);
        executorConfig.setMaxConcurrent(30);
        executorConfig.setMaxQueueDepth(500);
        if (group == 6)
        {
            executorConfig.setBusyPriority(BusyPriority.P6);
        }
        //        executorConfig.setRejectionPolicy(RETRY);
        executorConfig.setPredictionHandler(new PredictionHandler()
        {
            @Override
            public void onTimeout(Predictable command)
            {
                System.out.println(command);
            }
        });
        context.initService("executor" + group, executorConfig);
    }

    private static void test0(ExecutorServiceFactory context, int group)
    {
        ExecutorService service0 = context.getService("executor" + group);

        LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(10));

        for (int i = 0; i < 10000; i++)
        {
            service0.execute(new Test0("GROUP" + group));
            LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(50));
        }
    }
}

class Test0 implements Predictable
{
    //    @Override
    //    public int maxWait()
    //    {
    //        return 3000;
    //    }
    private final String group;

    public Test0(String group)
    {
        this.group = group;
    }

    @Override
    public void run()
    {
        //        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(100));
        f(28);
        //        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(1500));
        //        System.out.println(group + " >>>>> " + Thread.currentThread().getName());
    }

    private long f(long n)
    {
        if (n <= 1)
        {
            return 0;
        }
        if (n <= 2)
        {
            return 1;
        }
        return f(n - 2) + f(n - 1);
    }

    @Override
    public int maxWait()
    {
        return 100;
    }
}
