import io.qiufen.common.lang.JsonObject;

import java.util.HashMap;
import java.util.Map;

public class Test1
{
    public static void main(String[] args)
    {
        Map map = new HashMap();
        map.put("a", "a");
        map.put("b", "b");
        map.put("c", 1234567890);
        byte[] bytes = JsonObject.toJsonBytes(map);
        JsonObject jsonObject = JsonObject.of("{\"a\":\"a\",\"b\":\"b\",\"p1\":123456789}".getBytes());
        System.out.println(jsonObject);
    }
}
