//package interceptor;
//
//import io.qiufen.common.interceptor.AggregatedAsyncInterceptor;
//import io.qiufen.common.interceptor.InterceptorChainService;
//
///**
// * Created by zhangruzheng on 2021/6/26.
// */
//public class OrderService
//{
//    private InterceptorChainService interceptorChainService;
//
//    public void init()
//    {
//        interceptorChainService.init("Order_create")
//                               .add(new LogInterceptor())
//                               .add(new ParamValidationInterceptor())
//                               .add(new TransactionInterceptor())
//                               .add(new InventoryLockInterceptor())
//                               .add(new OrderCreateInterceptor());
//
//        interceptorChainService.init("order_query")
//                               .add(new LogInterceptor())
//                               .add(new ParamValidationInterceptor())
//                               .add(new OrderQueryInterceptor())
//                               .add(new AggregatedAsyncInterceptor(new OrderDetailQueryInterceptor(),
//                                       new PaymentQueryInterceptor(), new CustomerQueryInterceptor()));
//    }
//
//    public void createOrder()
//    {
//        interceptorChainService.get("order_create").runSync();
//    }
//
//    public OrderDto queryOrder(String orderNo)
//    {
//        interceptorChainService.get("order_query").runAsync();
//    }
//}
//
//class OrderCreateRequestDto {}
//
//class OrderCreateResponseDto {}