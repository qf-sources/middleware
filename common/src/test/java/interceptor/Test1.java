package interceptor;

import io.qiufen.common.interceptor.api.AsyncInterceptorChain;
import io.qiufen.common.interceptor.api.InvocationFeedback;
import io.qiufen.common.interceptor.impl.AggregatedAsyncInterceptor;
import io.qiufen.common.interceptor.impl.AggregatedSyncInterceptor;
import io.qiufen.common.interceptor.impl.DefaultAsyncInterceptorChain;
import io.qiufen.common.interceptor.impl.DefaultInterceptorContext;
import io.qiufen.common.interceptor.spi.*;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/22 16:01
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Test1
{
    public static void main(String[] args)
    {
        //        InterceptorChain interceptorChainService = new DefaultInterceptorChainService().setGlobalExecutor(
        //                Executors.newCachedThreadPool()).build();
        ExecutorService executor = Executors.newCachedThreadPool();
        AsyncInterceptorChain registry = new DefaultAsyncInterceptorChain(executor);
        registry.add(new MyInterceptor(1))
                .add(new MyInterceptor(2))
                .add(new AggregatedAsyncInterceptor(executor).add(new MyInterceptor(31))
                        .add(new MyInterceptor(32))
                        .add(new AggregatedSyncInterceptor(new MyInterceptor(33)).add(new MyInterceptor(34))
                                .add(new MyInterceptor(35)
                                {
                                    @Override
                                    public boolean doBefore(InterceptorContext context) throws Exception
                                    {
                                        throw new RuntimeException("测试下");
                                        //                                                             context.require("abc");
                                        //                                                             return true;
                                    }
                                })))
                .add(new AggregatedSyncInterceptor().add(new MyInterceptor(4)).add(new MyInterceptor(5)))
                .add(new MyInterceptor(6)
                {
                    @Override
                    public void doFinal(InterceptorContext context) throws Exception
                    {
                        throw new RuntimeException("test");
                    }
                });

        InterceptorContext context = new DefaultInterceptorContext();
        context.export("time", new Date());
        registry.invoke(context, new InvocationFeedback()
        {
            @Override
            public void onComplete(InterceptorContext context)
            {
                Date time = (Date) context.require("time");
                System.out.println(time);
                System.out.println("complete");
            }

            @Override
            public void onException(InterceptorContext context, Exception e)
            {
                Date time = (Date) context.require("time");
                System.out.println(time);
                e.printStackTrace();
            }
        });
    }
}

class MyInterceptor implements OnBefore, OnAfter, OnException, OnFinal
{
    private final int num;

    public MyInterceptor(int num)
    {
        this.num = num;
    }

    @Override
    public boolean doBefore(InterceptorContext context) throws Exception
    {
        System.out.println(Thread.currentThread().getName() + " doBefore:" + num);
        return true;
    }

    @Override
    public void doAfter(InterceptorContext context) throws Exception
    {
        System.out.println(Thread.currentThread().getName() + " doAfter:" + num);
    }

    @Override
    public void doFinal(InterceptorContext context) throws Exception
    {
        System.out.println(Thread.currentThread().getName() + " doFinal:" + num);
    }

    @Override
    public void doException(InterceptorContext context, Exception source) throws Exception
    {
        System.out.println(Thread.currentThread().getName() + " doException:" + num);
        throw source;
    }
}