package lang;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;

public class TestReplace02
{
    public static void main(String[] args)
    {
        StringBuilder builder = new StringBuilder().append(
                "SELECT data_id,partition_id,`index`,segment_name,`offset`,data_size,flag,create_user,create_time,update_user,update_time FROM storage_data_segment WHERE flag=? AND data_id=? ORDER BY $sortList[0].sortField$ $sortList[0].sortDir$ LIMIT ?,? ");
        CharSequences.replaceArgs(builder, '$', '$', new CharSequences.VariableParser<Object>()
        {
            @Override
            public int parse(Object args, CharSequences.Var var, CharSequences.Resolver resolver)
            {
                return 0;
            }
        }, null);
        System.out.println(builder);
    }
}
