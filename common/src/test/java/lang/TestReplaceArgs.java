package lang;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;

public class TestReplaceArgs
{
    public static void main(String[] args)
    {
        StringBuilder builder0 = new StringBuilder().append("jopdfsa{0}ldkjfa{1};sdfjasdf{2}");
        CharSequences.replaceArgs(builder0, '{', '}', new Object[]{'a', 'b', 'c'},
                new CharSequences.VariableParser<Object[]>()
                {
                    @Override
                    public int parse(Object[] args, CharSequences.Var var, CharSequences.Resolver resolver)
                    {
                        return 0;
                    }
                });
        System.out.println(builder0);

        StringBuilder builder1 = new StringBuilder().append("jopdfsa{0ldkjfa{1;sdfjasdf{2");
        CharSequences.replaceArgs(builder1, '{', '}', new Object[]{'a', 'b', 'c'},
                new CharSequences.VariableParser<Object>()
                {
                    @Override
                    public int parse(Object args, CharSequences.Var var, CharSequences.Resolver resolver)
                    {
                        return 0;
                    }
                });
        System.out.println(builder1);

        StringBuilder builder2 = new StringBuilder().append("#idList[0]# ");
        CharSequences.replaceArgs(builder2, '#', '#', new Object[]{'a', 'b', 'c'},
                new CharSequences.VariableParser<Object>()
                {
                    @Override
                    public int parse(Object args, CharSequences.Var var, CharSequences.Resolver resolver)
                    {
                        return 0;
                    }
                });
        System.out.println(builder2);
    }
}
