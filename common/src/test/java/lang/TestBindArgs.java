package lang;

import io.qiufen.common.lang.CharSequences;

public class TestBindArgs
{
    public static void main(String[] args)
    {
        System.out.println(CharSequences.bindArgs("jopdfsa{0}ldkjfa{1};sdfjasdf{2}", new Object[]{'a', 'b', 'c'}));
        System.out.println(CharSequences.bindArgs("jopdfsa{0ldkjfa{1;sdfjasdf{2", new Object[]{'a', 'b', 'c'}));
        System.out.println(CharSequences.bindArgs("#idList[0]# ", '#', '#', new Object[]{'a', 'b', 'c'},
                new CharSequences.VariableParser<Object>()
                {
                    @Override
                    public int parse(Object args, CharSequences.Var var, CharSequences.Resolver resolver)
                    {
                        return 0;
                    }
                }));
    }
}
