package lang;

import io.qiufen.common.lang.CTString;

public class CTStringTest0
{
    public static void main(String[] args)
    {
        String str = "1234567890";
        CTString s0 = CTString.of(str, 0, 5);
        CTString s1 = CTString.of("12345");
        System.out.println(s0.equals(s1));
    }
}
