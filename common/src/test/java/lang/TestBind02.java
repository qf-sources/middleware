package lang;

import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.CharSequences;

public class TestBind02
{
    public static void main(String[] args)
    {
        CharSequences.VariableParser<Object> parser = new CharSequences.VariableParser<Object>()
        {
            @Override
            public int parse(Object args, CharSequences.Var var, CharSequences.Resolver resolver)
            {
                return 0;
            }
        };
        System.out.println(
                CharSequences.bindArgs(CTString.wrap(new StringBuilder("test like '%$test$%' ")), '$', '$', null,
                        parser));
    }
}
