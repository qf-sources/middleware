package lang;

import io.qiufen.common.lang.CTString;

import java.util.UUID;

public class Test01
{
    public static void main(String[] args)
    {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < 100; i++)
        {
            str.append(UUID.randomUUID());
        }
        String str0 = str.toString();
        CTString ctString = CTString.wrap(str0);
        CTString ctString1 = CTString.wrap(str0);

        System.out.println(ctString);
        System.out.println(ctString.equals(ctString1));

        int count = 1000000, loops = 20;

        for (int i = 0; i < count; i++)
        {
            test(ctString, ctString1);
        }

        for (int loop = 0; loop < loops; loop++)
        {
            long p0 = System.currentTimeMillis();
            for (int i = 0; i < count; i++)
            {
                test(ctString, ctString1);
            }
            long p1 = System.currentTimeMillis();
            System.out.println(p1 - p0);
        }
    }

    static void test(CTString ctString, CTString ctString1)
    {
        ctString.equals(ctString1);
    }
}
