package lang;

import io.qiufen.common.lang.CharSequences;

import java.util.Enumeration;

public class TestSplit
{
    public static void main(String[] args)
    {
        Enumeration<CharSequences.Var> result = CharSequences.split("0,1,2,3,4,5,6,7,89", ',');
        while (result.hasMoreElements())
        {
            System.out.println(result.nextElement());
        }
    }
}
