package lang;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;

public class TestPerformance
{
    static String str = "SELECT data_id,partition_id,`index`,segment_name,`offset`,data_size,flag,create_user,create_time,update_user,update_time FROM storage_data_segment WHERE flag=? AND data_id=? ORDER BY $sortList[0].sortField$ $sortList[0].sortDir$ LIMIT ?,? ";

    static Object[] args = {'C', 'B', 'A'};

    static CharSequences.VariableParser<Object[]> parser = new CharSequences.VariableParser<Object[]>()
    {
        @Override
        public int parse(Object[] args, CharSequences.Var var, CharSequences.Resolver resolver)
        {
            return 0;
        }
    };

    static char left = '$';
    static char right = '$';

    static int times = 100;

    static int loops = 1000000;

    public static void main(String[] args)
    {
        replace();
        bind();
    }

    static void bind()
    {
        for (int i = 0; i < times; i++)
        {
            long sum = 0;
            for (int j = 0; j < loops; j++)
            {
                long p0 = System.nanoTime();
                CharSequences.bindArgs(str, left, right, args, parser);
                long p1 = System.nanoTime();
                sum += p1 - p0;
            }
            System.out.println("bind:" + (i + 1) + " " + sum / loops);
        }
    }

    static void replace()
    {
        for (int i = 0; i < times; i++)
        {
            long sum = 0;
            for (int j = 0; j < loops; j++)
            {
                long p0 = System.nanoTime();
                CharSequences.replaceArgs(new StringBuilder().append(str), left, right, args, parser);
                long p1 = System.nanoTime();
                sum += p1 - p0;
            }
            System.out.println("replace:" + (i + 1) + " " + sum / loops);
        }
    }
}
