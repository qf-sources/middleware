package event.test0;

import java.util.Date;
import java.util.UUID;

interface Sub
{
    void request(Object param, Object extra);
}

interface Listener
{
    void doHandle(Object obj, Object extra);
}

public class Test
{
    UserService userService = new UserService();

    Sub subAdd;
    Sub subQuery;

    public Test()
    {
        subAdd = userService.addAddListener(new ResponseListener());
        subQuery = userService.addQueryListener(new ResponseListener());
    }

    public static void main(String[] args)
    {
        Test test = new Test();
        test.test(new Request("add"), new Response());
        test.test(new Request("query"), new Response());
    }

    public void test(Request request, Response response)
    {
        String method = request.method();
        if ("add".equals(method))
        {
            subAdd.request(request.param(), response);
        }
        else if ("query".equals(method))
        {
            subQuery.request(request.param(), response);
        }
    }
}

class Request
{
    private final String method;

    public Request(String method)
    {
        this.method = method;
    }

    String method()
    {
        return method;
    }

    String param()
    {
        return UUID.randomUUID().toString();
    }
}

class Response
{
    void handle(Object obj)
    {
        System.out.println(obj);
    }
}

class ResponseListener implements Listener
{
    @Override
    public void doHandle(Object obj, Object extra)
    {
        Response response = (Response) extra;
        response.handle(obj);
    }
}

class UserDao
{
    Date select()
    {
        return new Date();
    }

    int insert()
    {
        return 1;
    }
}

class UserService
{
    UserDao userDao = new UserDao();

    Sub addQueryListener(final Listener listener)
    {
        return new DefaultSub(listener)
        {
            @Override
            protected Object execute(Object param, Object extra)
            {
                return userDao.select();
            }
        };
    }

    Sub addAddListener(final Listener listener)
    {
        return new DefaultSub(listener)
        {
            @Override
            protected Object execute(Object param, Object extra)
            {
                return userDao.insert();
            }
        };
    }
}

class DefaultSub implements Sub
{
    private final Listener listener;

    public DefaultSub(Listener listener)
    {
        this.listener = listener;
    }

    @Override
    public final void request(Object param, Object extra)
    {
        listener.doHandle(execute(param, extra), extra);
    }

    protected Object execute(Object param, Object extra)
    {
        return null;
    }
}