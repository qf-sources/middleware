package event.test1;

import io.qiufen.common.concurrency.executor.spi.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

enum UserEventType implements EventType
{
    ADD,
    QUERY
}

interface Listener
{
    void doHandle(Object obj, Object extra);
}

interface Sub
{
    void request(Object param, Object extra);
}

interface Handler
{
    Object handle(Object param);
}

interface EventType {}

public class Test
{
    UserService userService = new UserService();

    Sub subAdd;
    Sub subQuery;

    public Test()
    {
        subAdd = userService.addListener(UserEventType.ADD, new ResponseListener());
        subQuery = userService.addListener(UserEventType.QUERY, new ResponseListener());
    }

    public static void main(String[] args)
    {
        Test test = new Test();
        for (int i = 0; i < 10; i++)
        {
            test.test(new Request("add"), new Response());
            test.test(new Request("query"), new Response());
        }
        System.out.println("end.");
    }

    public void test(Request request, Response response)
    {
        String method = request.method();
        if ("add".equals(method))
        {
            subAdd.request(request.param(), response);
        }
        else if ("query".equals(method))
        {
            subQuery.request(request.param(), response);
        }
    }
}

class Request
{
    private final String method;

    public Request(String method)
    {
        this.method = method;
    }

    String method()
    {
        return method;
    }

    String param()
    {
        return UUID.randomUUID().toString();
    }
}

class Response
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Response.class);

    void handle(Object obj)
    {
        LOGGER.info("{}", obj);
    }
}

class ResponseListener implements Listener
{
    @Override
    public void doHandle(Object obj, Object extra)
    {
        Response response = (Response) extra;
        response.handle(obj);
    }
}

class UserDao
{
    Date select()
    {
        LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(new Random().nextInt(10)));
        return new Date();
    }

    int insert()
    {
        LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(new Random().nextInt(10)));
        return 1;
    }
}

class UserService extends BaseService<UserEventType>
{
    UserDao userDao = new UserDao();

    @Override
    protected void init(Map<UserEventType, Handler> handlerMap)
    {
        handlerMap.put(UserEventType.ADD, new Handler()
        {
            @Override
            public Object handle(Object param)
            {
                return add();
            }
        });

        handlerMap.put(UserEventType.QUERY, new Handler()
        {
            @Override
            public Object handle(Object param)
            {
                return query();
            }
        });
    }

    private int add()
    {
        return userDao.insert();
    }

    private Date query()
    {
        return userDao.select();
    }
}

class Sub0 implements Sub
{
    private static final Executor executor = Executors.newCachedThreadPool();

    private final Handler handler;
    private final Listener listener;

    public Sub0(Handler handler, Listener listener)
    {
        this.handler = handler;
        this.listener = listener;
    }

    @Override
    public final void request(final Object param, final Object extra)
    {
        //todo 拒绝异常
        executor.execute(new Command()
        {
            @Override
            protected void doExecute()
            {
                listener.doHandle(handler.handle(param), extra);
            }
            //todo 处理异常
        });
    }
}

class BaseService<E extends EventType>
{
    Map<E, Handler> handlerMap = new IdentityHashMap<E, Handler>();

    protected BaseService()
    {
        init(handlerMap);
    }

    public Sub addListener(E eventType, Listener listener)
    {
        return new Sub0(handlerMap.get(eventType), listener);
    }

    protected void init(Map<E, Handler> handlerMap)
    {
    }
}