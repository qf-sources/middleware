package eventbus;

import io.qiufen.common.eventbus.api.EventBusService;
import io.qiufen.common.eventbus.provider.DefaultEventBusService;
import io.qiufen.common.eventbus.provider.SimpleEventDispatcherFactory;
import io.qiufen.common.eventbus.provider.SimpleEventPublisherFactory;
import io.qiufen.common.eventbus.spi.*;
import io.qiufen.common.lang.JsonObject;

import java.util.Date;
import java.util.UUID;

public class Test
{
    private static final EventType<TestEvent> TEST = new SimpleEventType();

    public static void main(String[] args)
    {
        EventBusService service = new DefaultEventBusService();
        EventDispatcherFactory eventDispatcherFactory = new SimpleEventDispatcherFactory();
        EventPublisherFactory eventPublisherFactory = new SimpleEventPublisherFactory(eventDispatcherFactory);
        service.setEventDispatcherFactory(eventDispatcherFactory);
        service.setEventPublisherFactory(eventPublisherFactory);
        service.addListener(TEST, new Listener<TestEvent>()
        {
            @Override
            public void handle(TestEvent event)
            {
                System.out.println(JsonObject.toJsonString(event));
            }
        });
        EventPublisher publisher = service.createEventPublisher(TEST);
        publisher.publish(new TestEvent(new Date(), UUID.randomUUID().toString()));
    }
}

class SimpleEventType implements EventType<TestEvent> {}

class TestEvent extends Event
{
    private final String name;

    public TestEvent(Object source, String name)
    {
        super(source);
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}