package io.qiufen.common.filter;

/**
 * 过滤器接口
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface Filter<C extends Context>
{
    /**
     * 处理器过滤器
     *
     * @param chain   过滤器链
     * @param context 过滤器上下参数
     */
    void doFilter(FilterChain chain, C context) throws Throwable;
}
