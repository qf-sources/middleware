package io.qiufen.common.filter;

public enum AcceptancePolicy
{
    ACCEPT,
    CONTINUE,
    BREAK
}
