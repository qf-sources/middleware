package io.qiufen.common.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 过滤器注册表
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
@SuppressWarnings("rawtypes")
public class FilterRegistry
{
    private static final Logger log = LoggerFactory.getLogger(FilterRegistry.class);

    private final String name;
    private final List<FilterNode> nodeList;
    private final List<Filter> filterList;

    private Filter[] filters;
    private short endIndex;

    private boolean acceptable = false;

    private Service service = EmptyService.EMPTY;

    public FilterRegistry(String name)
    {
        this.name = name;
        this.nodeList = new ArrayList<FilterNode>();
        this.filterList = new LinkedList<Filter>();
    }

    public boolean isEmpty()
    {
        return this.filterList.isEmpty();
    }

    public FilterBinder addFilter(Filter filter)
    {
        if (null == filter)
        {
            throw new IllegalArgumentException("need filter.");
        }
        log.info("[{}]Registry filter, class:{}...", this.name, filter.getClass().getName());
        filterList.add(filter);
        Class<?> clazz = filter.getClass();
        String name = clazz.getName();
        for (FilterNode node : nodeList)
        {
            if (node.getName().equals(name))
            {
                throw new FilterException("[" + this.name + "]Filter:" + name + " has registered.");
            }
        }
        if (filter instanceof Acceptable)
        {
            this.acceptable = true;
        }

        FilterNode node = new FilterNode(name, filter);
        nodeList.add(node);
        FilterBinder binder = new FilterBinder(nodeList, filterList, node);

        //初始化注解配置
        loadAnnotation(clazz, binder);

        int count = this.filterList.size();
        Filter[] filters = new Filter[count];
        filterList.toArray(filters);
        this.filters = filters;
        this.endIndex = (short) (count - 1);
        return binder;
    }

    public String getName()
    {
        return name;
    }

    public void setService(Service service)
    {
        if (service != null)
        {
            this.service = service;
        }
    }

    public void submit(Context context) throws Throwable
    {
        FilterChain filterChain;
        if (this.acceptable)
        {
            filterChain = new AcceptableFilterChain(this.filters, endIndex, this.service);
        }
        else
        {
            filterChain = new SimpleFilterChain(this.filters, endIndex, this.service);
        }
        filterChain.doFilter(context);
    }

    private void loadAnnotation(Class<?> clazz, FilterBinder binder)
    {
        FilterName filterName = clazz.getAnnotation(FilterName.class);
        if (filterName != null)
        {
            binder.withName(filterName.value());
        }
        FilterPosition filterPosition = clazz.getAnnotation(FilterPosition.class);
        if (filterPosition != null)
        {
            switch (filterPosition.position())
            {
                case BEFORE:
                    binder.before(filterPosition.relativeName());
                    break;
                case AFTER:
                    binder.after(filterPosition.relativeName());
                    break;
                default:
            }
        }
    }
}
