package io.qiufen.common.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 过滤器链
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class SimpleFilterChain extends AbstractFilterChain
{
    private static final Logger log = LoggerFactory.getLogger(SimpleFilterChain.class);

    SimpleFilterChain(Filter[] filters, short endIndex, Service service)
    {
        super(filters, endIndex, service);
    }

    @Override
    protected void doFilter(Filter filter, FilterChain filterChain, Context context) throws Throwable
    {
        if (log.isDebugEnabled())
        {
            log.debug("Use filter:{}...", filter.getClass().getName());
        }
        filter.doFilter(this, context);
    }
}
