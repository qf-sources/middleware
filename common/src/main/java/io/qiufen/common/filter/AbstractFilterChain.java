package io.qiufen.common.filter;

/**
 * 过滤器链
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
@SuppressWarnings("rawtypes")
public abstract class AbstractFilterChain implements FilterChain
{
    private final Filter[] filters;
    private final Service service;
    private final short endIndex;

    private short index = 0;

    protected AbstractFilterChain(Filter[] filters, short endIndex, Service service)
    {
        this.filters = filters;
        this.service = service;
        this.endIndex = endIndex;
    }

    /**
     * 处理下一级过滤
     *
     * @param context 上下参数
     * @throws FilterException e
     */
    @Override
    public final void doFilter(Context context) throws Throwable
    {
        if (index <= endIndex)
        {
            doFilter(filters[index++], this, context);
        }
        else
        {
            this.service.doService(context);
        }
    }

    protected abstract void doFilter(Filter filter, FilterChain filterChain, Context context) throws Throwable;

}
