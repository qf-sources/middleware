package io.qiufen.common.filter;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/3/11 9:13
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class FilterException extends RuntimeException
{
    public FilterException(String message)
    {
        super(message);
    }

    public FilterException(Throwable cause)
    {
        super(cause);
    }
}
