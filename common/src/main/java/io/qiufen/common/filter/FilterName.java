package io.qiufen.common.filter;

import java.lang.annotation.*;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/7 10:05
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FilterName
{
    String value();
}
