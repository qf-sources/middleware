package io.qiufen.common.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 过滤器链
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class AcceptableFilterChain extends AbstractFilterChain
{
    private static final Logger log = LoggerFactory.getLogger(AcceptableFilterChain.class);

    AcceptableFilterChain(Filter[] filters, short endIndex, Service service)
    {
        super(filters, endIndex, service);
    }

    @Override
    protected void doFilter(Filter filter, FilterChain filterChain, Context context) throws Throwable
    {
        if (filter instanceof Acceptable)
        {
            Acceptable acceptable = (Acceptable) filter;
            switch (acceptable.accept(context))
            {
                case ACCEPT:
                    if (log.isDebugEnabled())
                    {
                        log.debug("Accept filter:{}...", filter.getClass().getName());
                    }
                    filter.doFilter(this, context);
                    break;
                case CONTINUE:
                    if (log.isDebugEnabled())
                    {
                        log.debug("Can't accept filter:{},continue...", filter.getClass().getName());
                    }
                    this.doFilter(context);
                    break;
                case BREAK:
                    if (log.isDebugEnabled())
                    {
                        log.debug("Can't accept filter:{},break...", filter.getClass().getName());
                    }
                default:
            }
            return;
        }

        if (log.isDebugEnabled())
        {
            log.debug("Use filter:{}...", filter.getClass().getName());
        }
        filter.doFilter(this, context);
    }
}
