package io.qiufen.common.filter;

import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/7 9:51
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class FilterBinder
{
    private final List<FilterNode> nodeList;
    private final List<Filter> filterList;
    private final FilterNode node;

    FilterBinder(List<FilterNode> nodeList, List<Filter> filterList, FilterNode node)
    {
        this.nodeList = nodeList;
        this.filterList = filterList;
        this.node = node;
    }

    public FilterBinder withName(String name)
    {
        if (this.node.getName().equals(name))
        {
            return this;
        }
        for (FilterNode node0 : nodeList)
        {
            if (node0.getName().equals(name))
            {
                throw new FilterException("Filter:" + name + " has registered.");
            }
        }
        this.node.setName(name);
        return this;
    }

    public FilterBinder before(String targetName)
    {
        return adjust(targetName, RelativePosition.BEFORE);
    }

    public FilterBinder after(String targetName)
    {
        return adjust(targetName, RelativePosition.AFTER);
    }

    private FilterBinder adjust(String targetName, RelativePosition position)
    {
        if (this.node.getName().equals(targetName))
        {
            throw new FilterException("Can't adjust self node:" + targetName);
        }
        FilterNode node1 = null;
        for (FilterNode node : nodeList)
        {
            if (node.getName().equals(targetName))
            {
                node1 = node;
                break;
            }
        }
        if (node1 == null)
        {
            throw new FilterException("Can't find filter named:" + targetName);
        }
        nodeList.remove(node);
        int index = nodeList.indexOf(node1);
        if (position == RelativePosition.BEFORE)
        {
            nodeList.add(index, node);
        }
        else if (position == RelativePosition.AFTER)
        {
            nodeList.add(index + 1, node);
        }

        filterList.clear();
        for (FilterNode node : nodeList)
        {
            filterList.add(node.getFilter());
        }
        return this;
    }
}
