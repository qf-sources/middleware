package io.qiufen.common.filter;

class EmptyService implements Service
{
    static final Service EMPTY = new EmptyService();

    @Override
    public void doService(Context context) {}
}
