package io.qiufen.common.filter;

import java.lang.reflect.ParameterizedType;

public abstract class AcceptableFilter<C extends Context> implements Filter<C>, Acceptable
{
    private final Class<?> type;

    protected AcceptableFilter()
    {
        ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
        this.type = (Class<?>) parameterizedType.getActualTypeArguments()[0];
    }

    @Override
    public AcceptancePolicy accept(Context context)
    {
        return this.type.isInstance(context) ? AcceptancePolicy.ACCEPT : AcceptancePolicy.CONTINUE;
    }
}
