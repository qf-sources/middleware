package io.qiufen.common.filter;

public interface Acceptable
{
    AcceptancePolicy accept(Context context);
}
