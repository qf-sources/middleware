package io.qiufen.common.filter;

/**
 * 过滤器链
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface FilterChain
{
    /**
     * 处理下一级过滤
     *
     * @param context 上下参数
     * @throws FilterException
     */
    void doFilter(Context context) throws Throwable;
}
