package io.qiufen.common.filter;

public interface Service
{
    void doService(Context context) throws Throwable;
}
