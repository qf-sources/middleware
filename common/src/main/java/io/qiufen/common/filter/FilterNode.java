package io.qiufen.common.filter;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/7 11:16
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class FilterNode
{
    private final Filter filter;

    private String name;

    FilterNode(String name, Filter filter)
    {
        this.name = name;
        this.filter = filter;
    }

    Filter getFilter()
    {
        return this.filter;
    }

    String getName()
    {
        return name;
    }

    void setName(String name)
    {
        this.name = name;
    }
}
