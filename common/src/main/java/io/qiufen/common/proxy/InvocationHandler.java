package io.qiufen.common.proxy;

import java.lang.reflect.Method;

/**
 * Created by Administrator on 2018/4/2.
 */
public interface InvocationHandler
{
    Object invoke(Object prototype, Method method, Object[] args) throws Throwable;
}
