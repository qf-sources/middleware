package io.qiufen.common.proxy;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/4/2.
 */
public class Proxy
{
    public static Object newProxyInstance(Class<?>[] interClasses, final ProviderInvocationHandler handler)
    {
        Object obj;
        try
        {
            ProxyFactory factory = new ProxyFactory();
            factory.setInterfaces(filterInterfaces(interClasses));
            obj = factory.createClass().newInstance();
        }
        catch (Exception e)
        {
            throw new ProxyBuildException(e);
        }
        ((ProxyObject) obj).setHandler(new MethodHandler()
        {
            @Override
            public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable
            {
                Object source = handler.prototype();
                try
                {
                    return handler.invoke(source, thisMethod, args);
                }
                catch (InvocationTargetException e)
                {
                    throw e.getTargetException();
                }
            }
        });
        return obj;
    }

    public static Object newProxyInstance(Class<?>[] interClasses, final InvocationHandler handler)
    {
        final Object obj;
        try
        {
            ProxyFactory factory = new ProxyFactory();
            factory.setInterfaces(filterInterfaces(interClasses));
            obj = factory.createClass().newInstance();
        }
        catch (Exception e)
        {
            throw new ProxyBuildException(e);
        }
        ((ProxyObject) obj).setHandler(new MethodHandler()
        {
            @Override
            public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable
            {
                try
                {
                    return handler.invoke(obj, thisMethod, args);
                }
                catch (InvocationTargetException e)
                {
                    throw e.getTargetException();
                }
            }
        });
        return obj;
    }

    public static Object newProxyInstance(Class<?> implClass, InvocationHandler handler)
    {
        Object prototype;
        try
        {
            prototype = implClass.newInstance();
        }
        catch (Exception e)
        {
            throw new ProxyBuildException(e);
        }
        return newProxyInstance(prototype, handler);
    }

    public static Object newProxyInstance(final Object source, final InvocationHandler handler)
    {
        Object obj;
        try
        {
            ProxyFactory factory = new ProxyFactory();
            factory.setSuperclass(source.getClass());
            obj = factory.createClass().newInstance();
        }
        catch (Exception e)
        {
            throw new ProxyBuildException(e);
        }
        ((ProxyObject) obj).setHandler(new MethodHandler()
        {
            @Override
            public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable
            {
                try
                {
                    return handler.invoke(source, thisMethod, args);
                }
                catch (InvocationTargetException e)
                {
                    throw e.getTargetException();
                }
            }
        });
        return obj;
    }

    public static Object newProxyInstance(Object source, InvocationHandler handler, Class<?> interClass)
    {
        return newProxyInstance(source, handler, new Class<?>[]{interClass});
    }

    public static Object newProxyInstance(final Object source, final InvocationHandler handler, Class<?>[] interClasses)
    {
        Object obj;
        try
        {
            ProxyFactory factory = new ProxyFactory();
            factory.setInterfaces(filterInterfaces(interClasses));
            factory.setSuperclass(source.getClass());
            obj = factory.createClass().newInstance();
        }
        catch (Exception e)
        {
            throw new ProxyBuildException(e);
        }
        ((ProxyObject) obj).setHandler(new MethodHandler()
        {
            @Override
            public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable
            {
                try
                {
                    return handler.invoke(source, thisMethod, args);
                }
                catch (InvocationTargetException e)
                {
                    throw e.getTargetException();
                }
            }
        });
        return obj;
    }

    private static Class[] filterInterfaces(Class[] interClasses)
    {
        List<Class> list = new ArrayList<Class>();
        for (Class interClass : interClasses)
        {
            if (ProxyObject.class.isAssignableFrom(interClass))
            {
                continue;
            }
            list.add(interClass);
        }
        Class[] array = new Class[list.size()];
        return list.toArray(array);
    }
}