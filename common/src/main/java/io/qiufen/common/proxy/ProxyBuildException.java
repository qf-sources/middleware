package io.qiufen.common.proxy;

/**
 * Created by Administrator on 2018/4/2.
 */
public class ProxyBuildException extends RuntimeException
{
    public ProxyBuildException(Throwable cause)
    {
        super(cause);
    }
}
