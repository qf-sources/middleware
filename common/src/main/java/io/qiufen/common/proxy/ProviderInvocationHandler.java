package io.qiufen.common.proxy;

/**
 * Created by Administrator on 2018/4/2.
 */
public interface ProviderInvocationHandler extends InvocationHandler
{
    Object prototype() throws Throwable;
}
