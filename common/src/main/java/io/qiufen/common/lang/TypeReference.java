package io.qiufen.common.lang;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/2 14:03
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public abstract class TypeReference<T>
{
    private final Type type;

    protected TypeReference()
    {
        ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
        this.type = type.getActualTypeArguments()[0];
    }

    public final Type getType()
    {
        return type;
    }
}
