package io.qiufen.common.lang;

import java.nio.CharBuffer;

public final class HeapCTString extends CTString
{
    private final char[] value;

    @Deprecated
    HeapCTString(CharSequence value, AcceptPolicy acceptPolicy)
    {
        int length = value.length();
        char[] chars = new char[length];
        for (int i = 0, index = 0; i < length; i++)
        {
            char ch = value.charAt(i);
            if (acceptPolicy.accept(ch)) chars[index++] = ch;
        }
        this.value = chars;

        super.hash();
    }

    HeapCTString(CharSequence value)
    {
        this(value, 0, value.length());
    }

    HeapCTString(CharSequence value, int offset, int length)
    {
        char[] chars = new char[length];
        for (int i = offset; i < offset + length; i++)
        {
            chars[i] = value.charAt(i);
        }
        this.value = chars;

        super.hash();
    }

    @Override
    public int length()
    {
        return this.value.length;
    }

    @Override
    public char charAt(int index)
    {
        return this.value[index];
    }

    @Override
    public CharSequence subSequence(int start, int end)
    {
        return CharBuffer.wrap(this.value, start, end - start);
    }

    @Override
    public String toString()
    {
        return new String(this.value);
    }

    @Override
    public void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)
    {
        System.arraycopy(this.value, srcBegin, dst, dstBegin, srcEnd - srcBegin);
    }
}