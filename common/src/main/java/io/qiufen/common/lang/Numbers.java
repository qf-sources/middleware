package io.qiufen.common.lang;

public class Numbers
{
    private Numbers()
    {
    }

    public static String toFullHex(int num)
    {
        return "0x" + Strings.leftPadding(Integer.toHexString(num), '0', 8);
    }

    public static long toUnsigned(int value)
    {
        return value & 0xffffffffL;
    }

    public static int toUnsigned(short value)
    {
        return value & 0xffff;
    }

    public static short toUnsigned(byte value)
    {
        return (short) (value & 0xff);
    }
}
