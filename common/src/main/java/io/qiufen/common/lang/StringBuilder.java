package io.qiufen.common.lang;

import java.nio.CharBuffer;
import java.util.Arrays;

public class StringBuilder implements CharSequence
{
    public static final CharCopier<char[]> COPIER_CHAR_ARRAY = new CharArrayCopier();
    public static final CharCopier<String> COPIER_STRING = new StringCopier();
    public static final CharCopier<java.lang.StringBuilder> COPIER_J_STRING_BUILDER = new JavaStringBuilderCopier();
    public static final CharCopier<StringBuilder> COPIER_STRING_BUILDER = new StringBuilderCopier();
    public static final CharCopier<CTString> COPIER_CT_STRING = new CTStringCopier();

    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    private char[] value;
    private int length;

    public StringBuilder()
    {
        this(16);
    }

    public StringBuilder(int initCapacity)
    {
        this.value = new char[initCapacity];
        this.length = 0;
    }

    public <S> StringBuilder append(S src, CharCopier<S> copier)
    {
        int srcLen = copier.length(src);
        int totalLen = this.length + srcLen;
        ensureCapacityInternal(totalLen);
        copier.copy(src, 0, this.value, this.length, srcLen);
        this.length = totalLen;
        return this;
    }

    public StringBuilder append(char[] src)
    {
        return append(src, COPIER_CHAR_ARRAY);
    }

    public StringBuilder append(String src)
    {
        return append(src, COPIER_STRING);
    }

    public StringBuilder append(CharBuffer src)
    {
        src = src.duplicate();
        int srcLen = src.length();
        int totalLen = this.length + srcLen;
        ensureCapacityInternal(totalLen);
        src.get(this.value, this.length, srcLen);
        this.length = totalLen;
        return this;
    }

    public StringBuilder append(java.lang.StringBuilder src)
    {
        return append(src, COPIER_J_STRING_BUILDER);
    }

    public StringBuilder append(StringBuffer src)
    {
        int srcLen = src.length();
        int totalLen = this.length + srcLen;
        ensureCapacityInternal(totalLen);
        src.getChars(0, srcLen, this.value, this.length);
        this.length = totalLen;
        return this;
    }

    public StringBuilder append(StringBuilder src)
    {
        return this.append(src, COPIER_STRING_BUILDER);
    }

    public StringBuilder append(CTString src)
    {
        return append(src, COPIER_CT_STRING);
    }

    public <S> StringBuilder replace(int start, int end, S src, CharCopier<S> copier)
    {
        if (end > this.length) end = this.length;
        int srcLen = copier.length(src);
        int totalLen = this.length + srcLen - (end - start);
        ensureCapacityInternal(totalLen);
        System.arraycopy(value, end, value, start + srcLen, this.length - end);
        copier.copy(src, 0, this.value, start, srcLen);
        this.length = totalLen;
        return this;
    }

    public <S> StringBuilder insert(int index, S src, CharCopier<S> copier)
    {
        int srcLen = copier.length(src);
        int totalLen = this.length + srcLen;
        ensureCapacityInternal(totalLen);
        System.arraycopy(value, index, value, index + srcLen, length - index);
        copier.copy(src, 0, this.value, index, srcLen);
        this.length = totalLen;
        return this;
    }

    @Override
    public int length()
    {
        return this.length;
    }

    @Override
    public char charAt(int index)
    {
        return this.value[index];
    }

    @Override
    public CharSequence subSequence(int start, int end)
    {
        return CharBuffer.wrap(this.value, start, end - start);
    }

    @Override
    public String toString()
    {
        return new String(this.value, 0, this.length);
    }

    private void ensureCapacityInternal(int minimumCapacity)
    {
        // overflow-conscious code
        int oldCapacity = value.length;
        if (minimumCapacity - oldCapacity > 0)
        {
            value = Arrays.copyOf(value, newCapacity(minimumCapacity, oldCapacity));
        }
    }

    private int newCapacity(int minCapacity, int oldCapacity)
    {
        // overflow-conscious code
        int newCapacity = (oldCapacity << 1) + 2;
        if (newCapacity - minCapacity < 0)
        {
            newCapacity = minCapacity;
        }
        return (newCapacity <= 0 || MAX_ARRAY_SIZE - newCapacity < 0) ? hugeCapacity(minCapacity) : newCapacity;
    }

    private int hugeCapacity(int minCapacity)
    {
        if (Integer.MAX_VALUE - minCapacity < 0)
        { // overflow
            throw new OutOfMemoryError();
        }
        return Math.max(minCapacity, MAX_ARRAY_SIZE);
    }

    public interface CharCopier<S>
    {
        int length(S src);

        void copy(S src, int srcPos, char[] dest, int destPos, int length);
    }

    private static class CharArrayCopier implements CharCopier<char[]>
    {
        @Override
        public int length(char[] src)
        {
            return src.length;
        }

        @Override
        public void copy(char[] src, int srcPos, char[] dest, int destPos, int length)
        {
            System.arraycopy(src, srcPos, dest, destPos, length);
        }
    }

    private static class StringCopier implements CharCopier<String>
    {
        @Override
        public int length(String src)
        {
            return src.length();
        }

        @Override
        public void copy(String src, int srcPos, char[] dest, int destPos, int length)
        {
            src.getChars(srcPos, length, dest, destPos);
        }
    }

    private static class JavaStringBuilderCopier implements CharCopier<java.lang.StringBuilder>
    {
        @Override
        public int length(java.lang.StringBuilder src)
        {
            return src.length();
        }

        @Override
        public void copy(java.lang.StringBuilder src, int srcPos, char[] dest, int destPos, int length)
        {
            src.getChars(srcPos, length, dest, destPos);
        }
    }

    private static class StringBuilderCopier implements CharCopier<StringBuilder>
    {
        @Override
        public int length(StringBuilder src)
        {
            return src.length;
        }

        @Override
        public void copy(StringBuilder src, int srcPos, char[] dest, int destPos, int length)
        {
            System.arraycopy(src.value, srcPos, dest, destPos, length);
        }
    }

    private static class CTStringCopier implements CharCopier<CTString>
    {
        @Override
        public int length(CTString src)
        {
            return src.length();
        }

        @Override
        public void copy(CTString src, int srcPos, char[] dest, int destPos, int length)
        {
            src.getChars(srcPos, length, dest, destPos);
        }
    }
}
