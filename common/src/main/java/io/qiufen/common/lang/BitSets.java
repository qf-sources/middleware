package io.qiufen.common.lang;

import io.qiufen.common.collection.ArrayUtil;
import io.qiufen.common.collection.CollectionUtil;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-8-2
 * Time: 上午11:09
 * To change this template use File | Settings | File Templates.
 */
public class BitSets
{
    public static List<Property> toProperties(BitSet set)
    {
        if (null == set)
        {
            return null;
        }
        List<Property> properties = new ArrayList<Property>();
        for (int i = 0; i < set.length(); i++)
        {
            Property property = new Property();
            property.setIndex(i);
            property.setFlag(set.get(i));
            properties.add(property);
        }
        return properties;
    }

    public static BitSet bitSetOfTrue(int... indexes)
    {
        return toBitSet(true, indexes);
    }

    public static BitSet bitSetOfFalse(int... indexes)
    {
        return toBitSet(false, indexes);
    }

    public static BitSet toBitSet(boolean flag, int... indexes)
    {
        if (null == indexes || indexes.length <= 0)
        {
            return null;
        }
        BitSet set = new BitSet();
        for (int index : indexes)
        {
            set.set(index, flag);
        }
        return set;
    }

    public static BitSet toBitSet(Property... properties)
    {
        if (ArrayUtil.isEmpty(properties))
        {
            return null;
        }
        BitSet set = new BitSet();
        for (Property property : properties)
        {
            set.set(property.getIndex(), property.getFlag());
        }
        return set;
    }

    public static BitSet toBitSet(Collection<Property> properties)
    {
        if (CollectionUtil.isEmpty(properties))
        {
            return null;
        }
        BitSet set = new BitSet();
        for (Property property : properties)
        {
            set.set(property.getIndex(), property.getFlag());
        }
        return set;
    }

    public static BitSet toBitSet(Long num)
    {
        if (null == num || num <= 0)
        {
            return new BitSet();
        }
        BitSet bitSet = new BitSet();
        int bit = 0;
        while (num > 0)
        {
            bitSet.set(bit++, num % 2 > 0);
            num = num >> 1;
        }
        return bitSet;
    }

    public static BitSet merge(BitSet... sets)
    {
        if (ArrayUtil.isEmpty(sets))
        {
            return null;
        }
        BitSet bigSet = new BitSet();
        for (BitSet set : sets)
        {
            if (null == set || set.length() <= 0)
            {
                continue;
            }
            for (int i = 0; i < set.length(); i++)
            {
                bigSet.set(i, set.get(i));
            }
        }
        return bigSet;
    }

    public static boolean isTrue(BitSet set, int bit)
    {
        return null != set && set.get(bit);
    }

    public static boolean isFalse(BitSet set, int bit)
    {
        return !isTrue(set, bit);
    }

    public static boolean isTrue(Number num, int bit)
    {
        return num != null && (num.longValue() & (1L << bit)) > 0;
    }

    public static boolean isFalse(Long num, int bit)
    {
        return !isTrue(num, bit);
    }

    public static long toLong(BitSet bitSet)
    {
        if (null == bitSet)
        {
            return 0;
        }
        long num = 0;
        for (int i = 0; i < bitSet.length(); i++)
        {
            if (bitSet.get(i))
            {
                num += 1L << i;
            }
        }
        return num;
    }
}
