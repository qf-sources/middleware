package io.qiufen.common.lang;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Enumeration;

public class CharSequences
{
    public static boolean isEmpty(CharSequence sequence)
    {
        return sequence == null || sequence.length() <= 0;
    }

    public static boolean isBlank(CharSequence sequence)
    {
        for (int i = 0; i < sequence.length(); i++)
        {
            if (sequence.charAt(i) > ' ') return false;
        }
        return true;
    }

    public static boolean isNotBlank(CharSequence sequence)
    {
        return !isBlank(sequence);
    }

    public static ByteBuffer toBytes(CharSequence sequence)
    {
        return toBytes(Charset.defaultCharset(), sequence);
    }

    public static ByteBuffer toBytes(Charset charset, CharSequence sequence)
    {
        return charset.encode(CharBuffer.wrap(sequence));
    }

    public static <T> void replaceArgs(StringBuilder builder, char left, char right, T args, VariableParser<T> parser)
    {
        if (isEmpty(builder)) return;

        int length = builder.length();
        Var var = null;
        ReplacementResolver resolver = new ReplacementResolver(builder);
        for (int i = 0; i < length; i++)
        {
            char ch = builder.charAt(i);
            if (var == null) // 变量为null
            {
                if (ch == left) var = new Var(builder, i + 1, 0);
                continue;
            }

            var.setEnd(i); //变量不为null
            //左边界
            if (ch == left)
            {
                if (left == right)
                {
                    int leftStartIndex = var.start - 1;
                    resolver.setRange(leftStartIndex, var.end + 1);
                    int varLen = parser.parse(args, var, resolver);
                    var = null;
                    i = leftStartIndex + varLen;
                    length = builder.length();
                    continue;
                }
                var = new Var(builder, i + 1, 0);
                continue;
            }
            //右边界
            if (ch == right)
            {
                int leftStartIndex = var.start - 1;
                resolver.setRange(leftStartIndex, var.end + 1);
                int varLen = parser.parse(args, var, resolver);
                var = null;
                i = leftStartIndex + varLen;
                length = builder.length();
            }
        }
    }

    public static <CS extends CharSequence, T> StringBuilder bindArgs(CS charSequence,
            StringBuilder.CharCopier<CS> copier, char left, char right, VariableParser<T> parser, T args)
    {
        StringBuilder builder = new StringBuilder();
        if (isEmpty(charSequence)) return builder;

        builder.append(charSequence, copier);
        replaceArgs(builder, left, right, args, parser);
        return builder;
    }

    public static <T> StringBuilder bindArgs(String charSequence, char left, char right, T args,
            VariableParser<T> parser)
    {
        return bindArgs(charSequence, StringBuilder.COPIER_STRING, left, right, parser, args);
    }

    public static <T> StringBuilder bindArgs(CTString charSequence, char left, char right, T args,
            VariableParser<T> parser)
    {
        return bindArgs(charSequence, StringBuilder.COPIER_CT_STRING, left, right, parser, args);
    }

    public static StringBuilder bindArgs(String charSequence, Object[] args)
    {
        return bindArgs(charSequence, StringBuilder.COPIER_STRING, '{', '}', ArrayVariableParser.INSTANCE, args);
    }

    public static int indexOf(CharSequence charSequence, char ch, int offset)
    {
        for (int i = offset; i < charSequence.length(); i++)
        {
            if (charSequence.charAt(i) == ch) return i;
        }
        return -1;
    }

    public static Enumeration<Var> split(CharSequence charSequence, char ch)
    {
        return new SplitResult(charSequence, ch);
    }

    public interface VariableParser<T>
    {
        int parse(T args, Var var, Resolver resolver);
    }

    public interface Resolver
    {
        <T> int resolve(T t, StringBuilder.CharCopier<T> copier);
    }

    public static class Var implements CharSequence
    {
        private final CharSequence origin;
        private final int start;
        private int end;

        private Var(CharSequence origin, int start, int end)
        {
            this.origin = origin;
            this.start = start;
            this.end = end;
        }

        private void setEnd(int end)
        {
            this.end = end;
        }

        @Override
        public int length()
        {
            return end - start;
        }

        @Override
        public char charAt(int index)
        {
            return origin.charAt(start + index);
        }

        @Override
        public CharSequence subSequence(int start, int end)
        {
            return new Var(origin, this.start + start, this.start + end);
        }

        @Override
        public String toString()
        {
            return origin.subSequence(start, end).toString();
        }
    }

    private static class ArrayVariableParser implements VariableParser<Object[]>
    {
        private static final ArrayVariableParser INSTANCE = new ArrayVariableParser();

        @Override
        public int parse(Object[] args, Var var, Resolver resolver)
        {
            Object value = args[Integer.parseInt(var.toString())];
            return resolver.resolve(value == null ? "null" : value.toString(), StringBuilder.COPIER_STRING);
        }
    }

    private static class SplitResult implements Enumeration<Var>
    {
        private final CharSequence charSequence;
        private final char ch;
        private final int length;
        private int position = 0;
        private Var var;

        private SplitResult(CharSequence charSequence, char ch)
        {
            this.charSequence = charSequence;
            this.ch = ch;
            this.length = charSequence.length();
        }

        @Override
        public boolean hasMoreElements()
        {
            if (position >= length)
            {
                var = null;
                return false;
            }

            var = new Var(charSequence, position, position);
            while (position < length)
            {
                var.setEnd(position);
                char ch = charSequence.charAt(position);
                position++;
                if (this.ch == ch) return true;
            }
            var.setEnd(length);
            return true;
        }

        @Override
        public Var nextElement()
        {
            Var var = this.var;
            this.var = null;
            return var;
        }
    }

    private static class ReplacementResolver implements Resolver
    {
        private final StringBuilder builder;
        private int start;
        private int end;

        private ReplacementResolver(StringBuilder builder)
        {
            this.builder = builder;
        }

        private void setRange(int start, int end)
        {
            this.start = start;
            this.end = end;
        }

        public <T> int resolve(T t, StringBuilder.CharCopier<T> copier)
        {
            builder.replace(start, end, t, copier);
            return copier.length(t);
        }
    }
}