package io.qiufen.common.lang;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-8-2
 * Time: 上午11:05
 * To change this template use File | Settings | File Templates.
 */
public class Property
{
    private int index;
    private boolean flag;

    public Property()
    {
    }

    public Property(int index, boolean flag)
    {
        this.index = index;
        this.flag = flag;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public boolean getFlag()
    {
        return flag;
    }

    public void setFlag(boolean flag)
    {
        this.flag = flag;
    }
}
