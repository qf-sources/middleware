package io.qiufen.common.lang;

import java.lang.StringBuilder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Strings
{
    private Strings()
    {
    }

    public static boolean isEmpty(String arg)
    {
        return null == arg || "".equals(arg);
    }

    public static boolean isNotEmpty(String arg)
    {
        return !isEmpty(arg);
    }

    public static String leftPadding(String origin, char ch, int totalLength)
    {
        int paddingLength = totalLength - origin.length();
        if (paddingLength <= 0)
        {
            return origin;
        }
        java.lang.StringBuilder builder = new StringBuilder();
        for (int i = 0; i < paddingLength; i++)
        {
            builder.append(ch);
        }
        builder.append(origin);
        return builder.toString();
    }

    @SuppressWarnings("unchecked")
    @Deprecated
    public static String bindArgs(String str, Object args)
    {
        if (isEmpty(str) || null == args)
        {
            return str;
        }
        //匹配站位名称
        List<String> nameList = new ArrayList<String>();
        Matcher matcher = Pattern.compile("\\{(\\w+?)\\}").matcher(str);
        while (matcher.find())
        {
            nameList.add(matcher.group(1));
        }
        if (nameList.isEmpty())
        {
            return str;
        }

        //构建参数MAP
        Map<String, Object> paramMap = new HashMap<String, Object>();
        if (args instanceof Object[])
        {
            Object[] collection = (Object[]) args;
            for (int index = 0; index < collection.length; index++)
            {
                paramMap.put(String.valueOf(index), collection[index]);
            }
        }
        else if (args instanceof Collection)
        {
            Collection var10 = (Collection) args;
            int index = 0;
            for (Object arg1 : var10)
            {
                paramMap.put(String.valueOf(index++), arg1);
            }
        }
        else if (args instanceof Map)
        {
            paramMap.putAll((Map<String, Object>) args);
        }
        else
        {
            paramMap.put("0", args);
        }

        //替换站位名称
        for (String var12 : nameList)
        {
            Object var13 = paramMap.get(var12);
            if (null != var13)
            {
                str = str.replaceAll("\\{" + var12 + "\\}", var13.toString());
            }
        }
        return str;
    }
}
