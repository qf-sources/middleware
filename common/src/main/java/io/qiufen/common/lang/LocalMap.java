package io.qiufen.common.lang;

import java.util.HashMap;

public class LocalMap extends HashMap<String, Object>
{
    public LocalMap set(String key, Object value)
    {
        put(key, value);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key)
    {
        return (T) super.get(key);
    }

    public Boolean getBoolean(String key)
    {
        Object v = get(key);
        if (v == null)
        {
            return Boolean.FALSE;
        }
        if (v instanceof Boolean)
        {
            return (Boolean) v;
        }
        return Boolean.valueOf(v.toString());
    }

    public boolean booleanValue(String key)
    {
        Boolean v = getBoolean(key);
        return null != v && v;
    }

    public String getString(String key)
    {
        Object v = get(key);
        if (v == null)
        {
            return null;
        }
        if (v instanceof String)
        {
            return (String) v;
        }
        return v.toString();
    }

    public Byte getByte(String key)
    {
        Number number = getNumber(key);
        return null == number ? null : number.byteValue();
    }

    public byte byteValue(String key)
    {
        Byte v = getByte(key);
        return null == v ? 0 : v;
    }

    public Short getShort(String key)
    {
        Number number = getNumber(key);
        return null == number ? null : number.shortValue();
    }

    public short shortValue(String key)
    {
        Short v = getShort(key);
        return null == v ? 0 : v;
    }

    public Integer getInteger(String key)
    {
        Number number = getNumber(key);
        return null == number ? null : number.intValue();
    }

    public int intValue(String key)
    {
        Integer v = getInteger(key);
        return null == v ? 0 : v;
    }

    public Long getLong(String key)
    {
        Number number = getNumber(key);
        return null == number ? null : number.longValue();
    }

    public long longValue(String key)
    {
        Long v = getLong(key);
        return null == v ? 0 : v;
    }

    public Float getFloat(String key)
    {
        Number number = getNumber(key);
        return null == number ? null : number.floatValue();
    }

    public float floatValue(String key)
    {
        Float v = getFloat(key);
        return null == v ? 0 : v;
    }

    public Double getDouble(String key)
    {
        Number number = getNumber(key);
        return null == number ? null : number.doubleValue();
    }

    public double doubleValue(String key)
    {
        Double v = getDouble(key);
        return null == v ? 0 : v;
    }

    public Number getNumber(String key)
    {
        Object v = get(key);
        if (v == null)
        {
            return null;
        }
        if (v instanceof Number)
        {
            return (Number) v;
        }
        if (v instanceof String)
        {
            return new DefaultNumber((String) v);
        }
        return new DefaultNumber(v.toString());
    }

    static class DefaultNumber extends Number
    {
        private final String number;

        DefaultNumber(String number)
        {
            this.number = number;
        }

        @Override
        public int intValue()
        {
            return Integer.parseInt(number);
        }

        @Override
        public long longValue()
        {
            return Long.parseLong(number);
        }

        @Override
        public float floatValue()
        {
            return Float.parseFloat(number);
        }

        @Override
        public double doubleValue()
        {
            return Double.parseDouble(number);
        }
    }
}
