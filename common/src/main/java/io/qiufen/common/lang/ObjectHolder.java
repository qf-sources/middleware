package io.qiufen.common.lang;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/1/18.
 */
public class ObjectHolder
{
    private static final ThreadLocal<Map<Object, Object>> local = new ThreadLocal<Map<Object, Object>>();

    public static void hold(Object key, Object obj)
    {
        Map<Object, Object> map = local.get();
        if (null == map)
        {
            map = new HashMap<Object, Object>();
            local.set(map);
        }
        map.put(key, obj);
    }

    public static Object get(Object key)
    {
        Map<Object, Object> map = local.get();
        if (null == map)
        {
            return null;
        }
        return map.get(key);
    }

    public static boolean contain(Object key)
    {
        Map<Object, Object> map = local.get();
        if (null == map)
        {
            return false;
        }
        return map.containsKey(key);
    }

    public static void clear(Object key)
    {
        Map<Object, Object> map = local.get();
        if (null == map)
        {
            return;
        }
        map.remove(key);
        if (map.isEmpty())
        {
            local.remove();
        }
    }

    public static void clearAll()
    {
        Map<Object, Object> map = local.get();
        local.remove();
        if (null != map)
        {
            map.clear();
        }
    }
}
