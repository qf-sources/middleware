package io.qiufen.common.lang;

public interface LazyReference<T>
{
    T get();
}
