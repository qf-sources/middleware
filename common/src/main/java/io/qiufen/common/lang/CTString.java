package io.qiufen.common.lang;

public abstract class CTString implements CharSequence
{
    private static final int THRESHOLD_LENGTH_USE_DIRECT;

    static
    {
        String value = System.getProperty("thresholdLengthUseDirect", "32");
        THRESHOLD_LENGTH_USE_DIRECT = Integer.parseInt(value);
    }

    private int hash;

    public static CTString of(CharSequence value)
    {
        return value.length() < THRESHOLD_LENGTH_USE_DIRECT ? wrap(value) : new DirectCTString(value);
    }

    public static CTString of(CharSequence value, int offset, int length)
    {
        return value.length() < THRESHOLD_LENGTH_USE_DIRECT ? wrap(value, offset, length) : new DirectCTString(value,
                offset, length);
    }

    public static CTString of(String value)
    {
        return value.length() < THRESHOLD_LENGTH_USE_DIRECT ? wrap(value) : new DirectCTString(value);
    }

    public static CTString of(String value, int offset, int length)
    {
        return value.length() < THRESHOLD_LENGTH_USE_DIRECT ? wrap(value, offset, length) : new DirectCTString(value,
                offset, length);
    }

    public static CTString of(CharSequence... values)
    {
        return of(new BigCharSequence(values));
    }

    public static CTString wrap(CharSequence value)
    {
        return new HeapCTString(value);
    }

    public static CTString wrap(CharSequence value, int offset, int limit)
    {
        return new HeapCTString(value, offset, limit);
    }

    @Deprecated
    public static CTString wrap(CharSequence value, AcceptPolicy acceptPolicy)
    {
        return new HeapCTString(value, acceptPolicy);
    }

    public static CTString wrap(CharSequence... values)
    {
        return wrap(new BigCharSequence(values));
    }

    public static CTString wrap(String value, int offset, int length)
    {
        return new WrappedCTString0(value, offset, length);
    }

    public static CTString wrap(String value)
    {
        return new WrappedCTString1(value);
    }

    @Override
    public final int hashCode()
    {
        return this.hash;
    }

    @Override
    public final boolean equals(Object anObject)
    {
        if (this == anObject) return true;

        if (!(anObject instanceof CTString)) return false;

        CTString anSeq = (CTString) anObject;
        if (anSeq.hash != this.hash) return false;

        int length;
        if ((length = this.length()) != anSeq.length()) return false;

        for (int i = 0; i < length; i++)
        {
            if (this.charAt(i) != anSeq.charAt(i)) return false;
        }
        return true;
    }

    public abstract void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin);

    protected final void hash()
    {
        int length = this.length();
        if (length <= 0)
        {
            this.hash = 0;
            return;
        }

        int h = 0;
        for (int i = 0; i < length; i++)
        {
            h = 31 * h + this.charAt(i);
        }
        this.hash = h;
    }

    private static class BigCharSequence implements CharSequence
    {
        private final CharSequence[] charSequences;
        private final int[] lengths;

        private BigCharSequence(CharSequence... charSequences)
        {
            this.charSequences = charSequences;

            int len = charSequences.length;
            this.lengths = new int[len];
            for (int i = 0; i < len; i++)
            {
                this.lengths[i] = charSequences[i].length();
            }
        }

        @Override
        public int length()
        {
            int len = 0;
            for (int i : lengths)
            {
                len += i;
            }
            return len;
        }

        @Override
        public char charAt(int index)
        {
            int n = 0, endIndex = this.lengths[n] - 1;
            while (endIndex < index)
            {
                endIndex += this.lengths[++n];
            }
            int realIndex = index - (endIndex - this.lengths[n]) - 1;
            return this.charSequences[n].charAt(realIndex);
        }

        @Override
        public CharSequence subSequence(int start, int end)
        {
            throw new UnsupportedOperationException();
        }
    }
}