package io.qiufen.common.lang;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;

public final class DirectCTString extends CTString
{
    private final CharBuffer value;

    DirectCTString(CharSequence value)
    {
        this(value, 0, value.length());
    }

    DirectCTString(CharSequence value, int offset, int length)
    {
        this.value = createValue(length);
        for (int i = offset; i < offset + length; i++)
        {
            this.value.put(value.charAt(i));
        }
        this.value.flip();

        super.hash();
    }

    @Override
    public int length()
    {
        return value.length();
    }

    @Override
    public char charAt(int index)
    {
        return value.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end)
    {
        return value.subSequence(start, end);
    }

    @Override
    public String toString()
    {
        return this.value.toString();
    }

    @Override
    public void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)
    {
        CharBuffer buffer = this.value.duplicate();
        buffer.position(srcBegin);
        buffer.get(dst, dstBegin, srcEnd - srcBegin);
    }

    private CharBuffer createValue(int length)
    {
        return ByteBuffer.allocateDirect(length << 1).asCharBuffer();
    }
}