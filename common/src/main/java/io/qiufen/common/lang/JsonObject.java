package io.qiufen.common.lang;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import io.qiufen.common.io.IOUtil;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public final class JsonObject
{
    public static final JsonObject NULL = new JsonObject(null, null);

    private static final ObjectMapper DEFAULT = JsonMapper.builder().build();

    private final ObjectMapper mapper;
    private final JsonNode jsonNode;

    private JsonObject(ObjectMapper mapper, JsonNode jsonNode)
    {
        this.mapper = mapper;
        this.jsonNode = jsonNode;
    }

    public static JsonObject of(JsonNode json)
    {
        return new JsonObject(DEFAULT, json);
    }

    public static JsonObject of(ObjectMapper mapper, JsonNode json)
    {
        return new JsonObject(check(mapper), json);
    }

    public static JsonObject of(Object javaObject)
    {
        return javaObject == null ? NULL : of0(DEFAULT, javaObject);
    }

    public static JsonObject of(ObjectMapper mapper, Object javaObject)
    {
        return javaObject == null ? NULL : of0(check(mapper), javaObject);
    }

    private static JsonObject of0(ObjectMapper mapper0, Object javaObject)
    {
        if (javaObject instanceof JsonNode)
        {
            return new JsonObject(mapper0, (JsonNode) javaObject);
        }
        else
        {
            return new JsonObject(mapper0, mapper0.valueToTree(javaObject));
        }
    }

    public static JsonObject of(String json)
    {
        return Strings.isEmpty(json) ? NULL : of0(DEFAULT, json);
    }

    public static JsonObject of(ObjectMapper mapper, String json)
    {
        return Strings.isEmpty(json) ? NULL : of0(check(mapper), json);
    }

    private static JsonObject of0(ObjectMapper mapper0, String json)
    {
        try
        {
            return new JsonObject(mapper0, mapper0.readTree(json));
        }
        catch (JsonProcessingException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    public static JsonObject of(byte[] json)
    {
        return json == null ? NULL : of0(DEFAULT, json);
    }

    public static JsonObject of(ObjectMapper mapper, byte[] json)
    {
        return json == null ? NULL : of0(check(mapper), json);
    }

    private static JsonObject of0(ObjectMapper mapper0, byte[] json)
    {
        try
        {
            return new JsonObject(mapper0, mapper0.readTree(json));
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    public static JsonObject of(DataInput input)
    {
        return input == null ? NULL : of0(DEFAULT, input);
    }

    public static JsonObject of(ObjectMapper mapper, DataInput input)
    {
        return input == null ? NULL : of0(check(mapper), input);
    }

    private static JsonObject of0(ObjectMapper mapper0, DataInput input)
    {
        JsonParser parser = null;
        try
        {
            parser = mapper0.getFactory().createParser(input);
            return new JsonObject(mapper0, (JsonNode) mapper0.readTree(parser));
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException(e);
        }
        finally
        {
            IOUtil.close(parser);
        }
    }

    public static String toJsonString(Object value)
    {
        return null == value ? null : toJsonString0(DEFAULT, value);
    }

    public static String toJsonString(ObjectMapper mapper, Object value)
    {
        return null == value ? null : toJsonString0(check(mapper), value);
    }

    private static String toJsonString0(ObjectMapper mapper, Object value)
    {
        try
        {
            return mapper.writeValueAsString(value);
        }
        catch (JsonProcessingException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    public static byte[] toJsonBytes(Object value)
    {
        return value == null ? Arrays.EMPTY_BYTE : toJsonBytes0(DEFAULT, value);
    }

    public static byte[] toJsonBytes(ObjectMapper mapper, Object value)
    {
        return value == null ? Arrays.EMPTY_BYTE : toJsonBytes0(check(mapper), value);
    }

    private static byte[] toJsonBytes0(ObjectMapper mapper, Object value)
    {
        try
        {
            return mapper.writeValueAsBytes(value);
        }
        catch (JsonProcessingException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    public static void toJsonBytes(Object value, DataOutput output)
    {
        if (value != null) toJsonBytes0(DEFAULT, value, output);
    }

    public static void toJsonBytes(ObjectMapper mapper, Object value, DataOutput output)
    {
        if (value != null) toJsonBytes0(check(mapper), value, output);
    }

    private static void toJsonBytes0(ObjectMapper mapper, Object value, DataOutput output)
    {
        try
        {
            mapper.writeValue(output, value);
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    public static <T> T to(byte[] bytes, Type type)
    {
        return to(DEFAULT, bytes, type);
    }

    public static <T> T to(String json, Type type)
    {
        return to(DEFAULT, json, type);
    }

    public static <T> T to(DataInput input, Type type)
    {
        return to(DEFAULT, input, type);
    }

    @SuppressWarnings("unchecked")
    public static <T> T to(ObjectMapper mapper, DataInput input, Type type)
    {
        if (input == null) return (T) defaultValue(type);
        check(mapper);
        JavaType javaType = mapper.getTypeFactory().constructType(type);
        try
        {
            return mapper.readValue(input, javaType);
        }
        catch (JsonProcessingException e)
        {
            throw new IllegalArgumentException(e);
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T to(ObjectMapper mapper, byte[] bytes, Type type)
    {
        if (null == bytes || bytes.length <= 0) return (T) defaultValue(type);
        check(mapper);
        JavaType javaType = mapper.getTypeFactory().constructType(type);
        try
        {
            return mapper.readValue(bytes, javaType);
        }
        catch (JsonProcessingException e)
        {
            throw new IllegalArgumentException(e);
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T to(ObjectMapper mapper, String json, Type type)
    {
        if (Strings.isEmpty(json)) return (T) defaultValue(type);
        check(mapper);
        JavaType javaType = mapper.getTypeFactory().constructType(type);
        try
        {
            return mapper.readValue(json, javaType);
        }
        catch (JsonProcessingException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    private static ObjectMapper check(ObjectMapper mapper)
    {
        if (null == mapper)
        {
            throw new IllegalArgumentException("need object mapper.");
        }
        return mapper;
    }

    private static Object defaultValue(Type type)
    {
        if (type == byte.class) return (byte) 0;
        if (type == short.class) return (short) 0;
        if (type == int.class) return 0;
        if (type == long.class) return 0L;
        if (type == float.class) return 0.0f;
        if (type == double.class) return 0.0;
        if (type == boolean.class) return false;
        return null;
    }

    public boolean hasValue()
    {
        return jsonNode != null;
    }

    public JsonObject get(String key)
    {
        if (null == jsonNode) return NULL;
        return new JsonObject(this.mapper, jsonNode.get(key));
    }

    public String getString(String key)
    {
        return get(key).to(String.class);
    }

    public List<JsonObject> getArray(String key)
    {
        List<JsonObject> list = new ArrayList<JsonObject>();
        if (null == jsonNode) return list;
        JsonNode child = jsonNode.get(key);
        if (null == child) return list;
        if (child.isArray())
        {
            for (JsonNode node : child)
            {
                list.add(new JsonObject(this.mapper, node));
            }
            return list;
        }
        throw new UnsupportedOperationException("Not an array value, key:" + key);
    }

    @SuppressWarnings("unchecked")
    public <T> T to(Class<T> clazz)
    {
        if (!hasValue()) return (T) defaultValue(clazz);
        try
        {
            return this.mapper.readValue(this.mapper.treeAsTokens(this.jsonNode), clazz);
        }
        catch (JsonProcessingException e)
        {
            throw new IllegalArgumentException(e);
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T to(Type type)
    {
        if (!hasValue()) return (T) defaultValue(type);
        JavaType javaType = this.mapper.getTypeFactory().constructType(type);
        try
        {
            return this.mapper.readValue(this.mapper.treeAsTokens(this.jsonNode), javaType);
        }
        catch (JsonProcessingException e)
        {
            throw new IllegalArgumentException(e);
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    public <T> T to(TypeReference<T> reference)
    {
        return to(reference.getType());
    }

    public String toJsonString()
    {
        return hasValue() ? toJsonString0(this.mapper, this.jsonNode) : null;
    }

    public byte[] toJsonBytes()
    {
        return hasValue() ? toJsonBytes(this.mapper, this.jsonNode) : Arrays.EMPTY_BYTE;
    }

    public void toJsonBytes(DataOutput output)
    {
        if (hasValue()) toJsonBytes0(this.mapper, this.jsonNode, output);
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
}
