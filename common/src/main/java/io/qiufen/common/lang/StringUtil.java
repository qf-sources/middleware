package io.qiufen.common.lang;

import java.lang.StringBuilder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-6-5
 * Time: 下午6:24
 * To change this template use File | Settings | File Templates.
 */
@Deprecated
public class StringUtil
{
    private static final Random random0 = new Random();

    public static boolean isEmpty(String arg)
    {
        return null == arg || "".equals(arg);
    }

    public static boolean isNotEmpty(String arg)
    {
        return !isEmpty(arg);
    }

    public static boolean bothEmpty(String... args)
    {
        if (null == args)
        {
            return true;
        }
        for (String arg : args)
        {
            if (isNotEmpty(arg))
            {
                return false;
            }
        }
        return true;
    }

    public static boolean bothNotEmpty(String... args)
    {
        if (null == args || args.length <= 0)
        {
            return false;
        }
        for (String arg : args)
        {
            if (isEmpty(arg))
            {
                return false;
            }
        }
        return true;
    }

    public static boolean contain(String str, char ch)
    {
        if (isEmpty(str))
        {
            return false;
        }
        for (int i = 0; i < str.length(); i++)
        {
            if (ch == str.charAt(i))
            {
                return true;
            }
        }
        return false;
    }

    public static String replaceFirst(String str, CharSequence replacement)
    {
        return str;//todo
    }

    public static boolean startWith(String str, String prefix)
    {
        return isNotEmpty(str) && str.startsWith(prefix);
    }

    public static boolean startWith(String str, char prefix)
    {
        return isNotEmpty(str) && str.charAt(0) == prefix;
    }

    public static boolean startWithAny(String str, Collection<? extends CharSequence> collection)
    {
        return true;//todo
    }

    public static boolean equal(String arg0, String arg1)
    {
        return null == arg0 ? null == arg1 : arg0.equals(arg1);
    }

    public static String randomNum(int bytes)
    {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < bytes; i++)
        {
            builder.append(random0.nextInt(10));
        }
        return builder.toString();
    }

    public static String toString(Object obj)
    {
        return toString(obj, "");
    }

    public static String toString(Object obj, Object defaultValue)
    {
        if (null == obj)
        {
            return defaultValue.toString();
        }
        if (obj instanceof String)
        {
            return (String) obj;
        }
        return obj.toString();
    }

    @SuppressWarnings("unchecked")
    public static String bindArgs(String str, Object args)
    {
        if (isEmpty(str) || null == args)
        {
            return str;
        }
        //匹配站位名称
        List<String> nameList = new ArrayList<String>();
        Matcher matcher = Pattern.compile("\\{(\\w+?)\\}").matcher(str);
        while (matcher.find())
        {
            nameList.add(matcher.group(1));
        }
        if (nameList.isEmpty())
        {
            return str;
        }

        //构建参数MAP
        Map<String, Object> paramMap = new HashMap<String, Object>();
        if (args instanceof Object[])
        {
            Object[] collection = (Object[]) args;
            for (int index = 0; index < collection.length; index++)
            {
                paramMap.put(String.valueOf(index), collection[index]);
            }
        }
        else if (args instanceof Collection)
        {
            Collection var10 = (Collection) args;
            int index = 0;
            for (Object arg1 : var10)
            {
                paramMap.put(String.valueOf(index++), arg1);
            }
        }
        else if (args instanceof Map)
        {
            paramMap.putAll((Map<String, Object>) args);
        }
        else
        {
            paramMap.put("0", args);
        }

        //替换站位名称
        for (String var12 : nameList)
        {
            Object var13 = paramMap.get(var12);
            if (null != var13)
            {
                str = str.replaceAll("\\{" + var12 + "\\}", var13.toString());
            }
        }
        return str;
    }
}
