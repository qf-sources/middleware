package io.qiufen.common.lang;

public interface AcceptPolicy
{
    boolean accept(char ch);
}
