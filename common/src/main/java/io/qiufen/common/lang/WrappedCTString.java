package io.qiufen.common.lang;

final class WrappedCTString0 extends CTString
{
    private final String value;
    private final int offset;
    private final int length;

    WrappedCTString0(String value, int offset, int length)
    {
        this.value = value;
        this.offset = offset;
        this.length = length;
        hash();
    }

    @Override
    public int length()
    {
        return this.length;
    }

    @Override
    public char charAt(int index)
    {
        return this.value.charAt(this.offset + index);
    }

    @Override
    public CharSequence subSequence(int start, int end)
    {
        return this.value.subSequence(this.offset + start, this.offset + end);
    }

    @Override
    public String toString()
    {
        return this.value.substring(this.offset, this.offset + this.length);
    }

    public void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)
    {
        this.value.getChars(this.offset + srcBegin, this.offset + srcEnd, dst, dstBegin);
    }
}

final class WrappedCTString1 extends CTString
{
    private final String value;

    WrappedCTString1(String value)
    {
        this.value = value;
        hash();
    }

    @Override
    public int length()
    {
        return this.value.length();
    }

    @Override
    public char charAt(int index)
    {
        return this.value.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end)
    {
        return this.value.subSequence(start, end);
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    public void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)
    {
        this.value.getChars(srcBegin, srcEnd, dst, dstBegin);
    }
}
