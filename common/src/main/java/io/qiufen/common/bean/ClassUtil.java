package io.qiufen.common.bean;

import io.qiufen.common.collection.ArrayUtil;
import io.qiufen.common.collection.CollectionUtil;
import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-6-6
 * Time: 下午3:41
 * To change this template use File | Settings | File Templates.
 */
public class ClassUtil
{
    static
    {
        ClassPool.getDefault().insertClassPath(new ClassClassPath(ClassUtil.class));
    }

    public static String getClassName(Class<?> clazz)
    {
        return getClassName0(clazz).toString();
    }

    private static StringBuilder getClassName0(Class<?> clazz)
    {
        if (clazz.isArray())
        {
            return getClassName0(clazz.getComponentType()).append("[]");
        }
        return new StringBuilder(clazz.getName());
    }

    public static CtClass toCtClass(Class clazz)
    {
        return toCtClass(new Class[]{clazz})[0];
    }

    public static CtClass[] toCtClass(Class[] clazzArray)
    {
        if (ArrayUtil.isEmpty(clazzArray))
        {
            return new CtClass[0];
        }
        CtClass[] ctClasses = new CtClass[clazzArray.length];
        try
        {
            for (int i = 0; i < clazzArray.length; i++)
            {
                ctClasses[i] = ClassPool.getDefault().get(clazzArray[i].getName());

            }
        }
        catch (NotFoundException e)
        {
            throw new RuntimeException(e);
        }
        return ctClasses;
    }

    public static <T> List<Class<T>> search(Class<T> superClass) throws ClassNotFoundException
    {
        return scanClasses(superClass);
    }

    private static <T> List<Class<T>> scanClasses(final Class<T> superClass) throws ClassNotFoundException
    {
        URL url = ClassUtil.class.getClassLoader().getResource("");
        if (null == url)
        {
            return null;
        }
        File root = new File(url.getFile());
        return scanClasses(root.getPath(), root, new Filter()
        {
            @Override
            public boolean match(Class<?> clazz)
            {
                return clazz != superClass && superClass.isAssignableFrom(clazz);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private static <T> List<Class<T>> scanClasses(String rootPath, File file, Filter filter)
            throws ClassNotFoundException
    {
        List<Class<T>> classes = new ArrayList<Class<T>>();
        if (file.isFile())
        {
            String name = file.getName();
            if (!name.contains("$") && name.endsWith(".class"))
            {
                String path = file.getPath();
                int offset = rootPath.length();
                String className = path.substring(offset + 1, path.length() - 6);
                className = className.replace('\\', '.');
                Class clazz = Class.forName(className);
                if (filter.match(clazz))
                {
                    classes.add(clazz);
                }
            }
            return classes;
        }
        File[] files = file.listFiles();
        if (null == files || files.length == 0)
        {
            return null;
        }
        for (File file1 : files)
        {
            List<Class<T>> cls = scanClasses(rootPath, file1, filter);
            if (CollectionUtil.isNotEmpty(cls))
            {
                classes.addAll(cls);
            }
        }
        return classes;
    }

    public interface Filter
    {
        boolean match(Class<?> clazz);
    }
}
