package io.qiufen.common.bean;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class BeanUtil
{
    private static final BeanUtil instance = new BeanUtil();

    private final Map<Integer, Method> setterInvokerMap;
    private final Map<Integer, Method> getterInvokerMap;

    private BeanUtil()
    {
        setterInvokerMap = new HashMap<Integer, Method>();
        getterInvokerMap = new HashMap<Integer, Method>();
    }

    private static BeanUtil getInstance()
    {
        return instance;
    }

    public static void setProperty(Object target, String propertyName, Object obj)
    {
        try
        {
            getInstance().getSetterInvoker(target.getClass(), propertyName).invoke(target, obj);
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    public static Object getProperty(Object target, String propertyName)
    {
        try
        {
            return getInstance().getGetterInvoker(target.getClass(), propertyName).invoke(target);
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    private Method getSetterInvoker(Class<?> clazz, String propertyName) throws Exception
    {
        int key = (clazz.getName() + '.' + propertyName).hashCode();
        Method invoker = getterInvokerMap.get(key);
        if (null != invoker)
        {
            return invoker;
        }
        synchronized (getterInvokerMap)
        {
            invoker = getterInvokerMap.get(key);
            if (null != invoker)
            {
                return invoker;
            }

            invoker = new PropertyDescriptor(propertyName, clazz).getWriteMethod();
            getterInvokerMap.put(key, invoker);
        }
        return invoker;
    }

    private Method getGetterInvoker(Class<?> clazz, String propertyName) throws Exception
    {
        int key = (clazz.getName() + '.' + propertyName).hashCode();
        Method invoker = setterInvokerMap.get(key);
        if (null != invoker)
        {
            return invoker;
        }
        synchronized (setterInvokerMap)
        {
            invoker = setterInvokerMap.get(key);
            if (null != invoker)
            {
                return invoker;
            }

            invoker = new PropertyDescriptor(propertyName, clazz).getReadMethod();
            setterInvokerMap.put(key, invoker);
        }
        return invoker;
    }
}