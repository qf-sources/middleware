package io.qiufen.common.eventbus.provider;

import io.qiufen.common.concurrency.executor.spi.Command;
import io.qiufen.common.eventbus.spi.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class SimpleEventDispatcherFactory extends AbstractEventDispatcherFactory
{
    private final Executor executor = Executors.newCachedThreadPool();

    @Override
    public EventDispatcher create(EventType<? extends Event> eventType)
    {
        return new SimpleEventDispatcher(executor);
    }
}

class SimpleEventDispatcher implements EventDispatcher
{
    private final Executor executor;
    private final List<Listener> listeners;

    public SimpleEventDispatcher(Executor executor)
    {
        this.executor = executor;
        this.listeners = new ArrayList<Listener>();
    }

    @Override
    public void dispatch(final Event event)
    {
        for (final Listener listener : listeners)
        {
            executor.execute(new Command()
            {
                @Override
                protected void doExecute()
                {
                    listener.handle(event);
                }
            });
        }
    }

    @Override
    public void addListener(Listener<? extends Event> listener)
    {
        this.listeners.add(listener);
    }
}