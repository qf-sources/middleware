package io.qiufen.common.eventbus.api;

import io.qiufen.common.eventbus.spi.*;

public interface EventBusService
{
    void setEventPublisherFactory(EventPublisherFactory factory);

    void setEventDispatcherFactory(EventDispatcherFactory factory);

    <T extends Event> void addListener(EventType<T> eventType, Listener<T> listener);

    EventPublisher createEventPublisher(EventType<? extends Event> eventType);
}
