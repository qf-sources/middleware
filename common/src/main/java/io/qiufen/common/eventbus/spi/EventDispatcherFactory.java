package io.qiufen.common.eventbus.spi;

public interface EventDispatcherFactory
{
    EventDispatcher create(EventType<? extends Event> eventType);

    /**
     * 获取事件分发器
     * 1.autoCreate = true,若不存在则创建(create())一个并返回
     * 2.autoCreate = false,若不存在直接返回null
     *
     * @param eventType  事件类型
     * @param autoCreate 自动创建
     */
    EventDispatcher get(EventType<? extends Event> eventType, boolean autoCreate);
}
