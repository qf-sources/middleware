package io.qiufen.common.eventbus.provider;

import io.qiufen.common.eventbus.spi.*;

public class SimpleEventPublisherFactory implements EventPublisherFactory
{
    private final EventDispatcherFactory eventDispatcherFactory;

    public SimpleEventPublisherFactory(EventDispatcherFactory eventDispatcherFactory)
    {
        this.eventDispatcherFactory = eventDispatcherFactory;
    }

    @Override
    public EventPublisher create(EventType eventType)
    {
        return new SimpleEventPublisher(eventDispatcherFactory.get(eventType, true));
    }
}

class SimpleEventPublisher implements EventPublisher
{
    private final EventDispatcher eventDispatcher;

    public SimpleEventPublisher(EventDispatcher eventDispatcher)
    {
        this.eventDispatcher = eventDispatcher;
    }

    @Override
    public void publish(Event event)
    {
        eventDispatcher.dispatch(event);
    }
}