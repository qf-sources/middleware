package io.qiufen.common.eventbus.spi;

public interface EventPublisher
{
    void publish(Event event);
}
