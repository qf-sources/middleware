package io.qiufen.common.eventbus.spi;

import java.util.EventListener;

public interface Listener<T extends Event> extends EventListener
{
    void handle(T event);
}
