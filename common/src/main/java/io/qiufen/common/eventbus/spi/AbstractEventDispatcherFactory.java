package io.qiufen.common.eventbus.spi;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractEventDispatcherFactory implements EventDispatcherFactory
{
    private final Map<EventType<? extends Event>, EventDispatcher> dispatchers;

    public AbstractEventDispatcherFactory()
    {
        dispatchers = new HashMap<EventType<? extends Event>, EventDispatcher>();
    }

    @Override
    public final EventDispatcher get(EventType<? extends Event> eventType, boolean autoCreate)
    {
        EventDispatcher dispatcher = this.dispatchers.get(eventType);
        if (!autoCreate)
        {
            return dispatcher;
        }

        if (dispatcher == null)
        {
            synchronized (this.dispatchers)
            {
                dispatcher = this.dispatchers.get(eventType);
                if (dispatcher == null)
                {
                    dispatcher = this.create(eventType);
                    this.dispatchers.put(eventType, dispatcher);
                }
            }
        }
        return dispatcher;
    }
}