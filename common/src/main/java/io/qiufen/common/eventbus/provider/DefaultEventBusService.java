package io.qiufen.common.eventbus.provider;

import io.qiufen.common.eventbus.api.EventBusService;
import io.qiufen.common.eventbus.spi.*;

public class DefaultEventBusService implements EventBusService
{
    private EventPublisherFactory eventPublisherFactory;
    private EventDispatcherFactory eventDispatcherFactory;

    @Override
    public void setEventPublisherFactory(EventPublisherFactory factory)
    {
        this.eventPublisherFactory = factory;
    }

    @Override
    public void setEventDispatcherFactory(EventDispatcherFactory factory)
    {
        this.eventDispatcherFactory = factory;
    }

    @Override
    public <T extends Event> void addListener(EventType<T> eventType, Listener<T> listener)
    {
        eventDispatcherFactory.get(eventType, true).addListener(listener);
    }

    @Override
    public EventPublisher createEventPublisher(EventType<? extends Event> eventType)
    {
        return this.eventPublisherFactory.create(eventType);
    }
}