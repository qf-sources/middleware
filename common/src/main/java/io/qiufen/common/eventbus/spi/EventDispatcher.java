package io.qiufen.common.eventbus.spi;

public interface EventDispatcher
{
    void dispatch(Event event);

    void addListener(Listener<? extends Event> listener);
}
