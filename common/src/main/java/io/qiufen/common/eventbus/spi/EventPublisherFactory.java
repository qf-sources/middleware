package io.qiufen.common.eventbus.spi;

public interface EventPublisherFactory
{
    EventPublisher create(EventType eventType);
}
