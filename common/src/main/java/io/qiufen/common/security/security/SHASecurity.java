package io.qiufen.common.security.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHASecurity extends HashSecurity
{
    protected MessageDigest getMessageDigest() throws NoSuchAlgorithmException
    {
        return MessageDigest.getInstance("SHA1");
    }
}
