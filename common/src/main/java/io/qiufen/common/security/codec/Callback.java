package io.qiufen.common.security.codec;

public interface Callback
{
    void call(Output output);
}
