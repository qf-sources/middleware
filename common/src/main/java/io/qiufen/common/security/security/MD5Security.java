package io.qiufen.common.security.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Deprecated
public class MD5Security extends HashSecurity
{
    protected MessageDigest getMessageDigest() throws NoSuchAlgorithmException
    {
        return MessageDigest.getInstance("MD5");
    }
}
