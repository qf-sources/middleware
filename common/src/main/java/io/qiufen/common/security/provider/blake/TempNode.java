package io.qiufen.common.security.provider.blake;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Node of the Blake3 hash tree
 * Is either chained into the next node using chainingValue()
 * Or used to calculate the hash digest using rootOutputBytes()
 */
class TempNode
{
    private static final int ROOT = 8;
    private static final int OUT_LEN = 32;

    private final Encoder _encoder;

    private final int[] inputChainingValue;
    private final int[] blockWords;

    private final long counter;
    private final int blockLen;
    private final int flags;

    TempNode(Encoder _encoder, int[] inputChainingValue, int[] blockWords, long counter, int blockLen, int flags)
    {
        this._encoder = _encoder;
        this.inputChainingValue = inputChainingValue;
        this.blockWords = blockWords;
        this.counter = counter;
        this.blockLen = blockLen;
        this.flags = flags;
    }

    // Return the 8 int CV
    int[] chainingValue()
    {
        return _encoder.compress(inputChainingValue, blockWords, counter, blockLen, flags);
    }

    byte[] rootOutputBytes(int outLen)
    {
        byte[] hash = new byte[outLen];
        int outputsNeeded = floorDiv(outLen, OUT_LEN << 1) + 1;
        int flags = this.flags | ROOT;
        for (int outputCounter = 0, i = 0; outputCounter < outputsNeeded; outputCounter++)
        {
            int[] words = _encoder.compress(inputChainingValue, blockWords, outputCounter, blockLen, flags);
            for (int word : words)
            {
                byte[] bytes = new byte[4];
                ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).putInt(word);
                for (byte b : bytes)
                {
                    hash[i++] = b;
                    if (i == outLen)
                    {
                        return hash;
                    }
                }
            }
        }
        throw new IllegalStateException("Uh oh something has gone horribly wrong");
    }

    private int floorDiv(int x, int y)
    {
        int r = x / y;
        if ((x ^ y) < 0 && (r * y != x))
        {
            r--;
        }
        return r;
    }
}
