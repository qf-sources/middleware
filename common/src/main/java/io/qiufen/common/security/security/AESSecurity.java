package io.qiufen.common.security.security;

import io.qiufen.common.security.codec.Callback;
import io.qiufen.common.security.codec.Decoder;
import io.qiufen.common.security.codec.Encoder;
import io.qiufen.common.security.codec.Output;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

//todo 待改进
public class AESSecurity implements Security
{
    private final SecretKey secretKey;

    public AESSecurity(byte[] key)
    {
        secretKey = new SecretKeySpec(key, "AES");
    }

    @Override
    public <E, D> D encrypt(E input, Encoder<E> encoder, Decoder<D> decoder) throws EncryptionException
    {
        try
        {
            final Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            encoder.encode(input, new Output()
            {
                @Override
                public void write(byte[] bytes, int offset, int length)
                {
                    cipher.update(bytes, offset, length);
                }
            });
            return decoder.decode(new Callback()
            {
                @Override
                public void call(Output output)
                {
                    //                    byte[] bytes = cipher.doFinal();
                    //                    output.write(bytes, 0, bytes.length);
                }
            });
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new EncryptionException(e);
        }
        catch (NoSuchPaddingException e)
        {
            throw new EncryptionException(e);
        }
        catch (InvalidKeyException e)
        {
            throw new EncryptionException(e);
        }
    }

    @Override
    public <E, D> D decrypt(E input, Encoder<E> encoder, Decoder<D> decoder) throws DecryptionException
    {
        try
        {
            final Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            encoder.encode(input, new Output()
            {
                @Override
                public void write(byte[] bytes, int offset, int length)
                {
                    cipher.update(bytes, offset, length);
                }
            });
            return decoder.decode(new Callback()
            {
                @Override
                public void call(Output output)
                {
                    //                    byte[] bytes = cipher.doFinal();
                    //                    output.write(bytes, 0, bytes.length);
                }
            });
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new DecryptionException(e);
        }
        catch (NoSuchPaddingException e)
        {
            throw new DecryptionException(e);
        }
        catch (InvalidKeyException e)
        {
            throw new DecryptionException(e);
        }
    }
}
