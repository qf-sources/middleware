package io.qiufen.common.security.codec;

import io.qiufen.common.io.IOUtil;

import java.io.*;

public class FileEncoder implements Encoder<File>
{
    public static final FileEncoder DEFAULT = new FileEncoder();

    private FileEncoder() {}

    @Override
    public void encode(File file, Output output)
    {
        InputStream stream;
        try
        {
            stream = new FileInputStream(file);
        }
        catch (FileNotFoundException e)
        {
            throw new CodecException(e);
        }

        try
        {
            byte[] buffer = new byte[1024 * 1024];
            int n;
            while ((n = stream.read(buffer)) != -1)
            {
                output.write(buffer, 0, n);
            }
        }
        catch (IOException e)
        {
            throw new CodecException(e);
        }
        finally
        {
            IOUtil.close(stream);
        }
    }
}
