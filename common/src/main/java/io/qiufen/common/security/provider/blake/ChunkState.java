package io.qiufen.common.security.provider.blake;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Helper object for creating new Nodes and chaining them
 */
class ChunkState
{
    static final int CHUNK_LEN = 1024;
    static final int CHUNK_CV_LEN = 16;
    static final int CHUNK_CHILD_CV_LEN = CHUNK_CV_LEN >> 1;
    static final int BLOCK_LEN = 64;
    static final int BLOCK_WORDS_LEN = BLOCK_LEN >> 2;

    private static final int CHUNK_START = 1;
    private static final int CHUNK_END = 2;
    private static final byte NUM_ZERO = (byte) 0;

    private final Encoder _encoder;

    private final byte[] block = new byte[BLOCK_LEN];
    private final int flags;

    private int[] chainingValue;
    private long chunkCounter = 0;
    private byte blockLen = 0;
    private byte blocksCompressed = 0;

    ChunkState(Encoder _encoder, int[] key, int flags)
    {
        this._encoder = _encoder;
        this.chainingValue = key;
        this.flags = flags;
    }

    ChunkState(Encoder _encoder, Blake3State state)
    {
        this._encoder = _encoder;
        this.flags = state.getFlags();
        System.arraycopy(state.getBlock(), 0, this.block, 0, this.block.length);
        this.chainingValue = state.getChainingValue();
        this.chunkCounter = state.getChunkCounter();
        this.blockLen = state.getBlockLen();
        this.blocksCompressed = state.getBlocksCompressed();
    }

    int length()
    {
        return BLOCK_LEN * blocksCompressed + blockLen;
    }

    long getChunkCounter()
    {
        return chunkCounter;
    }

    void update(byte[] input, int from, int to)
    {
        for (int currPos = from, canTake; currPos < to; currPos += canTake)
        {
            // Chain the next 64 byte block into this chunk/node
            if (blockLen == BLOCK_LEN)
            {
                this.chainingValue = _encoder.compress(this.chainingValue, _encoder.wordsFromLEBytes(block),
                        this.chunkCounter, BLOCK_LEN, this.flags | this.startFlag());
                blocksCompressed += 1;
                Arrays.fill(this.block, NUM_ZERO);
                this.blockLen = 0;
            }

            // Take bytes out of the input and update
            int want = BLOCK_LEN - this.blockLen; // How many bytes we need to fill up the current block
            canTake = Math.min(want, to - currPos);
            System.arraycopy(input, currPos, block, blockLen, canTake);
            blockLen += canTake;
        }
    }

    void update(ByteBuffer input, int from, int to)
    {
        for (int currPos = from, canTake; currPos < to; currPos += canTake)
        {
            // Chain the next 64 byte block into this chunk/node
            if (blockLen == BLOCK_LEN)
            {
                this.chainingValue = _encoder.compress(this.chainingValue, _encoder.wordsFromLEBytes(block),
                        this.chunkCounter, BLOCK_LEN, this.flags | this.startFlag());
                blocksCompressed += 1;
                Arrays.fill(this.block, NUM_ZERO);
                this.blockLen = 0;
            }

            // Take bytes out of the input and update
            int want = BLOCK_LEN - this.blockLen; // How many bytes we need to fill up the current block
            canTake = Math.min(want, to - currPos);
            input.get(block, blockLen, canTake);
            blockLen += canTake;
        }
    }

    TempNode createNode()
    {
        return new TempNode(_encoder, chainingValue, _encoder.wordsFromLEBytes(block), chunkCounter, blockLen,
                flags | startFlag() | CHUNK_END);
    }

    int[] endAndResetChunk(int[] key)
    {
        int[] chunkCV = _encoder.compress(chainingValue, _encoder.wordsFromLEBytes(block), chunkCounter, blockLen,
                flags | startFlag() | CHUNK_END);

        /* reset */
        Arrays.fill(block, NUM_ZERO);
        this.chainingValue = key;
        this.chunkCounter++;
        this.blockLen = 0;
        this.blocksCompressed = 0;
        return chunkCV;
    }

    void fillState(Blake3State state)
    {
        state.setBlock(block);
        state.setChainingValue(chainingValue);
        state.setChunkCounter(chunkCounter);
        state.setBlockLen(blockLen);
        state.setBlocksCompressed(blocksCompressed);
    }

    private int startFlag()
    {
        return blocksCompressed == 0 ? CHUNK_START : 0;
    }
}
