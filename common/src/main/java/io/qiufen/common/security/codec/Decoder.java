package io.qiufen.common.security.codec;

/**
 * 解码器
 *
 * @since 1.0
 */
public interface Decoder<T>
{
    T decode(Callback callback);
}
