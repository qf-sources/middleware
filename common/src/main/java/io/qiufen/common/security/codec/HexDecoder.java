package io.qiufen.common.security.codec;

import org.apache.commons.codec.binary.Hex;

public class HexDecoder extends ByteArrayDecoder<String>
{
    public static final HexDecoder DEFAULT = new HexDecoder();

    private HexDecoder() {}

    @Override
    protected String decode(byte[] bytes)
    {
        return Hex.encodeHexString(bytes);
    }
}