package io.qiufen.common.security.security;

import io.qiufen.common.security.codec.Decoder;
import io.qiufen.common.security.codec.Encoder;

/**
 * 安全接口
 */
public interface Security
{
    /**
     * 加密
     */
    <E, D> D encrypt(E input, Encoder<E> encoder, Decoder<D> decoder) throws EncryptionException;

    /**
     * 解密
     */
    <E, D> D decrypt(E input, Encoder<E> encoder, Decoder<D> decoder) throws DecryptionException;
}
