package io.qiufen.common.security.provider.blake;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

class Encoder
{
    static final int[] IV = {0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19};

    private final int[] _words = new int[ChunkState.BLOCK_WORDS_LEN];
    private final int[] _permuted0 = new int[ChunkState.BLOCK_WORDS_LEN];
    private final int[] _permuted1 = new int[ChunkState.BLOCK_WORDS_LEN];

    int[] compress(int[] chainingValue, int[] blockWords, long counter, int blockLen, int flags)
    {
        int counterInt = (int) (counter & 0xffffffffL);
        int counterShift = (int) ((counter >> 32) & 0xffffffffL);
        int[] state = {chainingValue[0], chainingValue[1], chainingValue[2], chainingValue[3], chainingValue[4], chainingValue[5], chainingValue[6], chainingValue[7], IV[0], IV[1], IV[2], IV[3], counterInt, counterShift, blockLen, flags};
        roundFn(state, blockWords);         // Round 1
        permute(blockWords, _permuted0);
        roundFn(state, _permuted0);         // Round 2
        permute(_permuted0, _permuted1);
        roundFn(state, _permuted1);         // Round 3
        permute(_permuted1, _permuted0);
        roundFn(state, _permuted0);         // Round 4
        permute(_permuted0, _permuted1);
        roundFn(state, _permuted1);         // Round 5
        permute(_permuted1, _permuted0);
        roundFn(state, _permuted0);         // Round 6
        permute(_permuted0, _permuted1);
        roundFn(state, _permuted1);         // Round 7

        for (int i = 0; i < 8; i++)
        {
            state[i] ^= state[i + 8];
        }

        for (int i = 0; i < 8; i++)
        {
            state[i + 8] ^= chainingValue[i];
        }

        return state;
    }

    private void roundFn(int[] state, int[] m)
    {
        // Mix columns
        g(state, 0, 4, 8, 12, m[0], m[1]);
        g(state, 1, 5, 9, 13, m[2], m[3]);
        g(state, 2, 6, 10, 14, m[4], m[5]);
        g(state, 3, 7, 11, 15, m[6], m[7]);
        // todo 以上4行可视作4条原子指令并行执行（不存在冲突数组位置）

        // Mix diagonals
        g(state, 0, 5, 10, 15, m[8], m[9]);
        g(state, 1, 6, 11, 12, m[10], m[11]);
        g(state, 2, 7, 8, 13, m[12], m[13]);
        g(state, 3, 4, 9, 14, m[14], m[15]);
        // todo 以上4行可视作4条原子指令并行执行（不存在冲突数组位置）
    }

    private void g(int[] state, int a, int b, int c, int d, int mx, int my)
    {
        int xb = state[b];
        int xa = wrappingAdd(wrappingAdd(state[a], xb), mx);
        int xd = rotateRight(state[d] ^ xa, 16, 16);
        int xc = wrappingAdd(state[c], xd);

        xb = rotateRight(xb ^ xc, 12, 20);
        xa = wrappingAdd(wrappingAdd(xa, xb), my);
        xd = rotateRight(xd ^ xa, 8, 24);
        xc = wrappingAdd(xc, xd);

        state[a] = xa;
        state[c] = xc;
        state[d] = xd;
        state[b] = rotateRight(xb ^ xc, 7, 25);
    }

    private int wrappingAdd(int a, int b)
    {
        return a + b;
    }

    private int rotateRight(int x, int len, int toLeftBits)
    {
        return (x >>> len) | (x << toLeftBits);
    }

    private void permute(int[] m, int[] out)
    {
        /*
         * MSG_PERMUTATION = {2, 6, 3, 10, 7, 0, 4, 13, 1, 11, 12, 5, 9, 14, 15, 8};
         * for (int i = 0; i < ChunkState.BLOCK_WORDS_LEN; i++)
         * {
         *     out[i] = m[MSG_PERMUTATION[i]];
         * }
         */

        out[0] = m[2];
        out[1] = m[6];
        out[2] = m[3];
        out[3] = m[10];
        out[4] = m[7];
        out[5] = m[0];
        out[6] = m[4];
        out[7] = m[13];
        out[8] = m[1];
        out[9] = m[11];
        out[10] = m[12];
        out[11] = m[5];
        out[12] = m[9];
        out[13] = m[14];
        out[14] = m[15];
        out[15] = m[8];
    }

    int[] wordsFromLEBytes(byte[] bytes)
    {
        ByteBuffer buf = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
        for (int i = 0; i < ChunkState.BLOCK_WORDS_LEN; i++)
        {
            _words[i] = buf.getInt();
        }
        return _words;
    }

    int[] mergeToWords(int[] leftChildCV, int[] rightChildCV)
    {
        System.arraycopy(leftChildCV, 0, _words, 0, ChunkState.CHUNK_CHILD_CV_LEN);
        System.arraycopy(rightChildCV, 0, _words, ChunkState.CHUNK_CHILD_CV_LEN, ChunkState.CHUNK_CHILD_CV_LEN);
        return _words;
    }
}
