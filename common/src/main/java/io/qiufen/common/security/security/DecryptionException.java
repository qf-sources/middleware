package io.qiufen.common.security.security;

public class DecryptionException extends RuntimeException
{
    public DecryptionException(Throwable cause)
    {
        super(cause);
    }
}
