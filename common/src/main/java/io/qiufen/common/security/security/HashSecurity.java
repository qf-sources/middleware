package io.qiufen.common.security.security;

import io.qiufen.common.security.codec.Callback;
import io.qiufen.common.security.codec.Decoder;
import io.qiufen.common.security.codec.Encoder;
import io.qiufen.common.security.codec.Output;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public abstract class HashSecurity implements Security
{
    @Override
    public final <E, D> D encrypt(E input, Encoder<E> encoder, Decoder<D> decoder) throws EncryptionException
    {
        try
        {
            return encrypt0(input, encoder, decoder);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new EncryptionException(e);
        }
    }

    @Override
    public final <E, D> D decrypt(E input, Encoder<E> encoder, Decoder<D> decoder) throws DecryptionException
    {
        throw new UnsupportedOperationException();
    }

    private <E, D> D encrypt0(E input, Encoder<E> encoder, Decoder<D> decoder) throws NoSuchAlgorithmException
    {
        final MessageDigest digest = getMessageDigest();
        encoder.encode(input, new Output()
        {
            @Override
            public void write(byte[] bytes, int offset, int length)
            {
                digest.update(bytes, offset, length);
            }
        });
        return decoder.decode(new Callback()
        {
            @Override
            public void call(Output output)
            {
                byte[] bytes = digest.digest();
                output.write(bytes, 0, bytes.length);
            }
        });
    }

    protected abstract MessageDigest getMessageDigest() throws NoSuchAlgorithmException;
}
