package io.qiufen.common.security.provider.blake;

public class Blake3State
{
    private int[][] cvStack;
    private int[] key;
    private byte cvStackLen;
    private int flags;

    private byte[] block;
    private int[] chainingValue;
    private long chunkCounter;
    private byte blockLen;
    private byte blocksCompressed;

    public int[][] getCvStack()
    {
        return cvStack;
    }

    public void setCvStack(int[][] cvStack)
    {
        this.cvStack = cvStack;
    }

    public int[] getKey()
    {
        return key;
    }

    public void setKey(int[] key)
    {
        this.key = key;
    }

    public byte getCvStackLen()
    {
        return cvStackLen;
    }

    public void setCvStackLen(byte cvStackLen)
    {
        this.cvStackLen = cvStackLen;
    }

    public int getFlags()
    {
        return flags;
    }

    public void setFlags(int flags)
    {
        this.flags = flags;
    }

    public byte[] getBlock()
    {
        return block;
    }

    public void setBlock(byte[] block)
    {
        this.block = block;
    }

    public int[] getChainingValue()
    {
        return chainingValue;
    }

    public void setChainingValue(int[] chainingValue)
    {
        this.chainingValue = chainingValue;
    }

    public long getChunkCounter()
    {
        return chunkCounter;
    }

    public void setChunkCounter(long chunkCounter)
    {
        this.chunkCounter = chunkCounter;
    }

    public byte getBlockLen()
    {
        return blockLen;
    }

    public void setBlockLen(byte blockLen)
    {
        this.blockLen = blockLen;
    }

    public byte getBlocksCompressed()
    {
        return blocksCompressed;
    }

    public void setBlocksCompressed(byte blocksCompressed)
    {
        this.blocksCompressed = blocksCompressed;
    }
}
