package io.qiufen.common.security.codec;

public interface Output
{
    void write(byte[] bytes, int offset, int length);
}
