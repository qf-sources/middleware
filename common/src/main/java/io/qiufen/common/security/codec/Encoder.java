package io.qiufen.common.security.codec;

/**
 * 编码器
 *
 * @since 1.0
 */
public interface Encoder<T>
{
    void encode(T t, Output output);
}
