package io.qiufen.common.security.codec;

import java.util.Arrays;

public abstract class ByteArrayDecoder<T> implements Decoder<T>
{
    @Override
    public final T decode(Callback callback)
    {
        ByteArrayOutput output = new ByteArrayOutput();
        callback.call(output);
        return decode(output.bytes());
    }

    protected abstract T decode(byte[] bytes);

    private static class ByteArrayOutput implements Output
    {
        private byte[] bytes = new byte[0];

        @Override
        public void write(byte[] bytes, int offset, int length)
        {
            int position = this.bytes.length;
            this.bytes = Arrays.copyOf(this.bytes, position + length);
            System.arraycopy(bytes, offset, this.bytes, position, length);
        }

        public byte[] bytes()
        {
            return this.bytes;
        }
    }

}