package io.qiufen.common.security.security;

import io.qiufen.common.security.codec.Decoder;
import io.qiufen.common.security.codec.Encoder;

/**
 * 安全模板
 */
public class SecurityTemplate<E, D>
{
    private final Security security;

    private Encoder<E> encoder0;
    private Decoder<D> decoder0;

    private Encoder<E> encoder1;
    private Decoder<D> decoder1;

    public SecurityTemplate(Security security)
    {
        this.security = security;
    }

    public void setEncryptionCodec(Encoder<E> encoder, Decoder<D> decoder)
    {
        this.encoder0 = encoder;
        this.decoder0 = decoder;
    }

    public void setDecryptionCodec(Encoder<E> encoder, Decoder<D> decoder)
    {
        this.encoder1 = encoder;
        this.decoder1 = decoder;
    }

    public D encrypt(E input)
    {
        if (encoder0 == null || decoder0 == null)
        {
            throw new IllegalArgumentException("Need encoder and decoder for encryption.");
        }
        return security.encrypt(input, encoder0, decoder0);
    }

    public D decrypt(E input)
    {
        if (encoder1 == null || decoder1 == null)
        {
            throw new IllegalArgumentException("Need encoder and decoder for decryption.");
        }
        return security.decrypt(input, encoder1, decoder1);
    }
}
