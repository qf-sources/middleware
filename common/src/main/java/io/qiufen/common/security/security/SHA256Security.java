package io.qiufen.common.security.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA256Security extends HashSecurity
{
    protected MessageDigest getMessageDigest() throws NoSuchAlgorithmException
    {
        return MessageDigest.getInstance("SHA-256");
    }
}
