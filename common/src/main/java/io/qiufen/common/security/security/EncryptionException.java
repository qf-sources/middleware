package io.qiufen.common.security.security;

public class EncryptionException extends RuntimeException
{
    public EncryptionException(Throwable cause)
    {
        super(cause);
    }
}
