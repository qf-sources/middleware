package io.qiufen.common.security.codec;

import java.nio.charset.Charset;

public class StringEncoder implements Encoder<String>
{
    public static final StringEncoder DEFAULT = new StringEncoder(Charset.defaultCharset());

    private final Charset charset;

    public StringEncoder(Charset charset)
    {
        this.charset = charset;
    }

    @Override
    public void encode(String s, Output output)
    {
        byte[] bytes = s.getBytes(charset);
        output.write(bytes, 0, bytes.length);
    }
}
