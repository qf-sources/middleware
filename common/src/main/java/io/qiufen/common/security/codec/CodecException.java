package io.qiufen.common.security.codec;

public class CodecException extends RuntimeException
{
    public CodecException(Throwable cause)
    {
        super(cause);
    }
}
