package io.qiufen.common.security.codec;

import org.apache.commons.codec.binary.Base64;

public class Base64Decoder extends ByteArrayDecoder<String>
{
    public static final Base64Decoder DEFAULT = new Base64Decoder();

    private Base64Decoder() {}

    @Override
    protected String decode(byte[] bytes)
    {
        return Base64.encodeBase64String(bytes);
    }
}