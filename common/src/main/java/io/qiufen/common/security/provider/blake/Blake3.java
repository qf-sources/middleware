package io.qiufen.common.security.provider.blake;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Translation of the Blake3 reference implementation from Rust to Java
 * BLAKE3 Source: https://github.com/BLAKE3-team/BLAKE3
 * todo 序列化/反序列化
 */
public class Blake3
{
    private static final int DEFAULT_HASH_LEN = 32;
    private static final int KEY_LEN = 32;

    private static final int PARENT = 4;
    private static final int KEYED_HASH = 16;
    private static final int DERIVE_KEY_CONTEXT = 32;
    private static final int DERIVE_KEY_MATERIAL = 64;
    private final Encoder _encoder = new Encoder();
    private final int[][] cvStack = new int[54][];
    private ChunkState chunkState;
    private int[] key;
    private byte cvStackLen = 0;
    private int flags;

    private Blake3()
    {
        initialize(Encoder.IV, 0);
    }

    private Blake3(byte[] key)
    {
        initialize(_encoder.wordsFromLEBytes(key).clone(), KEYED_HASH);
    }

    private Blake3(String context)
    {
        Blake3 instance = new Blake3();
        instance.initialize(Encoder.IV, DERIVE_KEY_CONTEXT);
        byte[] bytes = context.getBytes(Charset.forName("UTF-8"));
        instance.update(bytes, 0, bytes.length);
        int[] contextKey = _encoder.wordsFromLEBytes(instance.digest()).clone();
        initialize(contextKey, DERIVE_KEY_MATERIAL);
    }

    private Blake3(Blake3State state)
    {
        System.arraycopy(state.getCvStack(), 0, this.cvStack, 0, this.cvStack.length);
        this.key = state.getKey().clone();
        this.cvStackLen = state.getCvStackLen();
        this.flags = state.getFlags();

        this.chunkState = new ChunkState(_encoder, state);
    }

    /**
     * Construct a BLAKE3 blake3 hasher
     */
    public static Blake3 newInstance()
    {
        return new Blake3();
    }

    public static Blake3 of(Blake3State state)
    {
        return new Blake3(state);
    }

    /**
     * Construct a new BLAKE3 keyed mode hasher
     *
     * @param key The 32 byte key
     * @throws IllegalStateException If the key is not 32 bytes
     */
    public static Blake3 newKeyed(byte[] key)
    {
        if (key.length != KEY_LEN) throw new IllegalStateException("Invalid key length");
        return new Blake3(key);
    }

    /**
     * Construct a new BLAKE3 key derivation mode hasher
     * The context string should be hardcoded, globally unique, and application-specific. <br><br>
     * A good default format is <i>"[application] [commit timestamp] [purpose]"</i>, <br>
     * eg "example.com 2019-12-25 16:18:03 session tokens v1"
     *
     * @param context Context string used to derive keys.
     */
    public static Blake3 newKeyDerivation(String context)
    {
        return new Blake3(context);
    }

    private void initialize(int[] key, int flags)
    {
        this.chunkState = new ChunkState(_encoder, key, flags);
        this.key = key;
        this.flags = flags;
    }

    /**
     * Appends new data to the hash tree
     *
     * @param input Data to be added
     */
    public void update(byte[] input, int offset, int length)
    {
        for (int currPos = offset, take; currPos < length; currPos += take)
        {
            // If this chunk has chained in 16 64 bytes of input, add its CV to the stack
            int chunkLen = chunkState.length();
            if (chunkLen == ChunkState.CHUNK_LEN)
            {
                chunkLen = 0;
                int[] chunkCV = chunkState.endAndResetChunk(key);
                addChunkChainingValue(chunkCV, chunkState.getChunkCounter());
            }

            int want = ChunkState.CHUNK_LEN - chunkLen;
            take = Math.min(want, length - currPos);
            chunkState.update(input, currPos, currPos + take);
        }
    }

    public void update(ByteBuffer input)
    {
        int length = input.limit();
        for (int currPos = input.position(), take; currPos < length; currPos += take)
        {
            // If this chunk has chained in 16 64 bytes of input, add its CV to the stack
            int chunkLen = chunkState.length();
            if (chunkLen == ChunkState.CHUNK_LEN)
            {
                chunkLen = 0;
                int[] chunkCV = chunkState.endAndResetChunk(key);
                addChunkChainingValue(chunkCV, chunkState.getChunkCounter());
            }

            int want = ChunkState.CHUNK_LEN - chunkLen;
            take = Math.min(want, length - currPos);
            chunkState.update(input, currPos, currPos + take);
        }
    }

    /**
     * Generate the blake3 hash for the current tree with the default byte length of 32
     *
     * @return The byte array representing the hash
     */
    public byte[] digest()
    {
        return digest(DEFAULT_HASH_LEN);
    }

    /**
     * Generate the blake3 hash for the current tree with the given byte length
     *
     * @param hashLen The number of bytes of hash to return
     * @return The byte array representing the hash
     */
    public byte[] digest(int hashLen)
    {
        TempNode node = this.chunkState.createNode();
        int parentNodesRemaining = cvStackLen;
        while (parentNodesRemaining > 0)
        {
            parentNodesRemaining -= 1;
            node = parentNode(cvStack[parentNodesRemaining], node.chainingValue(), key, flags);
        }
        return node.rootOutputBytes(hashLen);
    }

    public Blake3State getState()
    {
        Blake3State state = new Blake3State();
        state.setCvStack(cvStack);
        state.setKey(key);
        state.setCvStackLen(cvStackLen);
        state.setFlags(flags);
        chunkState.fillState(state);
        return state;
    }

    /**
     * Combines the chaining values of two children to create the parent node
     */
    private TempNode parentNode(int[] leftChildCV, int[] rightChildCV, int[] key, int flags)
    {
        int[] blockWords = _encoder.mergeToWords(leftChildCV, rightChildCV);
        return new TempNode(_encoder, key, blockWords, 0, ChunkState.BLOCK_LEN, PARENT | flags);
    }

    private void addChunkChainingValue(int[] newCV, long totalChunks)
    {
        for (; (totalChunks & 1) == 0; totalChunks >>= 1)
        {
            newCV = parentCV(popStack(), newCV, key, flags);
        }
        pushStack(newCV);
    }

    private int[] parentCV(int[] leftChildCV, int[] rightChildCV, int[] key, int flags)
    {
        int[] blockWords = _encoder.mergeToWords(leftChildCV, rightChildCV);
        return _encoder.compress(key, blockWords, 0, ChunkState.BLOCK_LEN, PARENT | flags);
    }

    private void pushStack(int[] cv)
    {
        this.cvStack[this.cvStackLen++] = cv;
    }

    private int[] popStack()
    {
        return cvStack[--cvStackLen];
    }
}
