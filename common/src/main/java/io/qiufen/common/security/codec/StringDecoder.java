package io.qiufen.common.security.codec;

import java.nio.charset.Charset;

public class StringDecoder extends ByteArrayDecoder<String>
{
    public static final StringDecoder DEFAULT = new StringDecoder(Charset.defaultCharset());

    private final Charset charset;

    public StringDecoder(Charset charset)
    {
        this.charset = charset;
    }

    @Override
    protected String decode(byte[] bytes)
    {
        return new String(bytes, charset);
    }
}
