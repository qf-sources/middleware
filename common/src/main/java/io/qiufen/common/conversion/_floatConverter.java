package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class _floatConverter extends FloatConverter
{
    _floatConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return float.class;
    }

    @Override
    public Object convert(Object source)
    {
        source = super.convert(source);
        return source == null ? 0.0f : source;
    }
}
