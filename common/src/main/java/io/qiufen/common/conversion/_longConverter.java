package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class _longConverter extends LongConverter
{
    _longConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return long.class;
    }

    @Override
    public Object convert(Object source)
    {
        source = super.convert(source);
        return source == null ? 0L : source;
    }
}
