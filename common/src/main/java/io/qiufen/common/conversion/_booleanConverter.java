package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class _booleanConverter extends BooleanConverter
{
    _booleanConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return boolean.class;
    }

    @Override
    public Object convert(Object source)
    {
        source = super.convert(source);
        return source == null ? false : source;
    }
}
