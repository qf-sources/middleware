package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class _byteConverter extends ByteConverter
{
    _byteConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return byte.class;
    }

    @Override
    public Object convert(Object source)
    {
        source = super.convert(source);
        return source == null ? (byte) 0 : source;
    }
}
