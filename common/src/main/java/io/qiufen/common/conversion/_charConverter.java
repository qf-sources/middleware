package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class _charConverter extends CharConverter
{
    _charConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return char.class;
    }

    @Override
    public Object convert(Object source)
    {
        Object value = super.convert(source);
        return value == null ? '\u0000' : value;
    }
}
