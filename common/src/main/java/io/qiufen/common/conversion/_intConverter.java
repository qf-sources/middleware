package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class _intConverter extends IntegerConverter
{
    _intConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return int.class;
    }

    @Override
    public Object convert(Object source)
    {
        source = super.convert(source);
        return source == null ? 0 : source;
    }
}
