package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class ByteConverter extends NumberConverter
{
    ByteConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return Byte.class;
    }

    @Override
    public Object convert(Object source)
    {
        if (source == null)
        {
            return null;
        }
        if (source instanceof Byte)
        {
            return source;
        }
        return toNumber(source).byteValue();
    }
}
