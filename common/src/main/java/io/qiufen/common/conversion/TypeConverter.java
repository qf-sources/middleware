package io.qiufen.common.conversion;

/**
 * 类型转换器
 *
 * @version 1.0
 * @since 1.0
 */
public interface TypeConverter
{
    /**
     * 目标类型
     *
     * @return 类型
     */
    Class<?> targetType();

    /**
     * 转换类型
     *
     * @param source 原始对象
     * @return 目标对象
     */
    Object convert(Object source);
}
