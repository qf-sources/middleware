package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class _doubleConverter extends DoubleConverter
{
    _doubleConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return double.class;
    }

    @Override
    public Object convert(Object source)
    {
        source = super.convert(source);
        return source == null ? 0.0 : source;
    }
}
