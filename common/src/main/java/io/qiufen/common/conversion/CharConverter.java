package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class CharConverter implements TypeConverter
{
    CharConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return Character.class;
    }

    @Override
    public Object convert(Object source)
    {
        if (source == null)
        {
            return null;
        }
        if (source instanceof Character)
        {
            return source;
        }
        if (source instanceof CharSequence)
        {
            return ((CharSequence) source).charAt(0);
        }
        return source.toString().charAt(0);
    }
}
