package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class ShortConverter extends NumberConverter
{
    ShortConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return Short.class;
    }

    @Override
    public Object convert(Object source)
    {
        if (source == null)
        {
            return null;
        }
        if (source instanceof Short)
        {
            return source;
        }
        return toNumber(source).shortValue();
    }
}
