package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class StringConverter implements TypeConverter
{
    StringConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return String.class;
    }

    @Override
    public Object convert(Object source)
    {
        if (source == null)
        {
            return null;
        }
        return (source instanceof String) ? source : source.toString();
    }
}
