package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class BooleanConverter implements TypeConverter
{
    BooleanConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return Boolean.class;
    }

    @Override
    public Object convert(Object source)
    {
        if (source == null)
        {
            return null;
        }
        if (source instanceof Boolean)
        {
            return source;
        }
        if (source instanceof String)
        {
            return Boolean.valueOf((String) source);
        }
        return Boolean.valueOf(source.toString());
    }
}
