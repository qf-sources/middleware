package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public abstract class NumberConverter implements TypeConverter
{
    NumberConverter()
    {
    }

    protected Number toNumber(Object source)
    {
        if (source instanceof Number)
        {
            return (Number) source;
        }
        return new Number0(source.toString());
    }

    private static class Number0 extends Number
    {
        private final String value;

        private Number0(Object obj)
        {
            value = obj.toString();
        }

        @Override
        public int intValue()
        {
            return Integer.parseInt(value);
        }

        @Override
        public long longValue()
        {
            return Long.parseLong(value);
        }

        @Override
        public float floatValue()
        {
            return Float.parseFloat(value);
        }

        @Override
        public double doubleValue()
        {
            return Double.parseDouble(value);
        }
    }
}
