package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class IntegerConverter extends NumberConverter
{
    IntegerConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return Integer.class;
    }

    @Override
    public Object convert(Object source)
    {
        if (source == null)
        {
            return null;
        }
        if (source instanceof Integer)
        {
            return source;
        }
        return toNumber(source).intValue();
    }
}
