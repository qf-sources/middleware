package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class FloatConverter extends NumberConverter
{
    FloatConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return Float.class;
    }

    @Override
    public Object convert(Object source)
    {
        if (source == null)
        {
            return null;
        }
        if (source instanceof Float)
        {
            return source;
        }
        return toNumber(source).floatValue();
    }
}
