package io.qiufen.common.conversion;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * 类型转换服务
 *
 * @version 1.0
 * @since 1.0
 */
public class TypeConversionService
{
    private static volatile TypeConversionService service;
    private final Map<Class<?>, TypeConverter> context;

    private TypeConversionService()
    {
        context = new IdentityHashMap<Class<?>, TypeConverter>();
    }

    public static TypeConversionService getInstance()
    {
        if (service == null)
        {
            synchronized (TypeConversionService.class)
            {
                if (service == null)
                {
                    service = new TypeConversionService();
                    service.addBasic();
                }
            }
        }
        return service;
    }

    /**
     * 转换对象类型
     *
     * @param source     原对象
     * @param targetType 目标类型
     * @param <T>        类型参数
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public final <T> T convert(Object source, Class<T> targetType)
    {
        TypeConverter converter = context.get(targetType);
        if (null == converter)
        {
            throw new RuntimeException("Can't find type converter for target:" + targetType);
        }
        try
        {
            return (T) converter.convert(source);
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    /**
     * 注册类型转换器
     *
     * @param converter 转换器
     */
    public final void addConverter(TypeConverter converter)
    {
        context.put(converter.targetType(), converter);
    }

    private void addBasic()
    {
        this.addConverter(new _charConverter());
        this.addConverter(new _byteConverter());
        this.addConverter(new _shortConverter());
        this.addConverter(new _intConverter());
        this.addConverter(new _longConverter());
        this.addConverter(new _booleanConverter());
        this.addConverter(new _floatConverter());
        this.addConverter(new _doubleConverter());
        this.addConverter(new CharConverter());
        this.addConverter(new ByteConverter());
        this.addConverter(new ShortConverter());
        this.addConverter(new IntegerConverter());
        this.addConverter(new LongConverter());
        this.addConverter(new FloatConverter());
        this.addConverter(new DoubleConverter());
        this.addConverter(new BooleanConverter());
        this.addConverter(new StringConverter());
    }
}