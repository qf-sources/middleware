package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class _shortConverter extends ShortConverter
{
    _shortConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return short.class;
    }

    @Override
    public Object convert(Object source)
    {
        source = super.convert(source);
        return source == null ? (short) 0 : source;
    }
}
