package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class LongConverter extends NumberConverter
{
    LongConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return Long.class;
    }

    @Override
    public Object convert(Object source)
    {
        if (source == null)
        {
            return null;
        }
        if (source instanceof Long)
        {
            return source;
        }
        return toNumber(source).longValue();
    }
}
