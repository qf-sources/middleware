package io.qiufen.common.conversion;

/**
 * Created by Administrator on 2017/11/24.
 */
public class DoubleConverter extends NumberConverter
{
    DoubleConverter()
    {
    }

    @Override
    public Class<?> targetType()
    {
        return Double.class;
    }

    @Override
    public Object convert(Object source)
    {
        if (source == null)
        {
            return null;
        }
        if (source instanceof Double)
        {
            return source;
        }
        return toNumber(source).doubleValue();
    }
}
