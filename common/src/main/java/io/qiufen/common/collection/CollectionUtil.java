package io.qiufen.common.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-6-6
 * Time: 下午4:04
 * To change this template use File | Settings | File Templates.
 */
public class CollectionUtil
{
    public static boolean isEmpty(Collection collection)
    {
        return null == collection || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection collection)
    {
        return null != collection && !collection.isEmpty();
    }

    public static boolean contain(Collection collection, Object element)
    {
        return isNotEmpty(collection) && collection.contains(element);
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> mergeToList(Collection... collections)
    {
        if (ArrayUtil.isEmpty(collections))
        {
            return null;
        }
        List<T> list = new ArrayList<T>();
        for (Collection collection : collections)
        {
            if (isEmpty(collection))
            {
                continue;
            }
            list.addAll(collection);
        }
        return list;
    }
}
