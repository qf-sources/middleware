package io.qiufen.common.collection;

/**
 * Created by Administrator on 2017/12/15.
 * instead by Arrays
 */
@Deprecated
public class ArrayUtil
{
    public static boolean contain(Object[] array, Object o)
    {
        if (null == array)
        {
            return false;
        }
        for (Object e : array)
        {
            if (e == o)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean isEmpty(Object[] args)
    {
        return null == args || args.length <= 0;
    }

    public static boolean isNotEmpty(Object[] args)
    {
        return !isEmpty(args);
    }
}
