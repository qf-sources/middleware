package io.qiufen.common.net;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

@ChannelHandler.Sharable
public class ChannelMonitorHandler extends ChannelInboundHandlerAdapter
{
    private final ChannelMonitor monitor;

    public ChannelMonitorHandler(ChannelMonitor monitor)
    {
        this.monitor = monitor;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception
    {
        monitor.add(ctx.channel());
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception
    {
        monitor.remove(ctx.channel());
        super.channelInactive(ctx);
    }

    @Override
    public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception
    {
        Channel channel = ctx.channel();
        if (channel.isWritable())
        {
            monitor.wakeup(channel);
        }
        super.channelWritabilityChanged(ctx);
    }
}
