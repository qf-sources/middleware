package io.qiufen.common.net;

import io.qiufen.common.lang.Strings;

import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class NetUtil
{
    public static List<String> localIP() throws SocketException
    {
        List<String> ipList = new ArrayList<String>();

        Enumeration<NetworkInterface> enumeration = NetworkInterface.getNetworkInterfaces();
        while (enumeration.hasMoreElements())
        {
            NetworkInterface networkInterface = enumeration.nextElement();
            List<InterfaceAddress> interfaceAddresses = networkInterface.getInterfaceAddresses();
            if (interfaceAddresses.isEmpty())
            {
                continue;
            }
            if (Strings.isNotEmpty(networkInterface.getName()) && networkInterface.getName().startsWith("lo"))
            {
                continue;
            }
            for (InterfaceAddress interfaceAddress : interfaceAddresses)
            {
                InetAddress address = interfaceAddress.getAddress();
                if (address instanceof Inet4Address)
                {
                    ipList.add(address.getHostAddress());
                }
            }
        }

        return ipList;
    }

    public static List<String> localIP(String referenceIP) throws SocketException
    {
        if ("0.0.0.0".equals(referenceIP) || "0:0:0:0:0:0:0:0".equals(referenceIP))
        {
            return localIP();
        }
        return Collections.singletonList(referenceIP);
    }
}
