package io.qiufen.common.net;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.util.AttributeKey;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ChannelMonitor
{
    private static final AttributeKey<MonitorEntry> KEY = AttributeKey.valueOf(MonitorEntry.class.getName());

    private final boolean fair;

    public ChannelMonitor()
    {
        this(false);
    }

    public ChannelMonitor(boolean fair)
    {
        this.fair = fair;
    }

    void add(Channel channel)
    {
        channel.attr(KEY).set(new MonitorEntry());
    }

    void remove(Channel channel)
    {
        wakeup0(channel.attr(KEY).getAndSet(null));
    }

    void wakeup(Channel channel)
    {
        wakeup0(channel.attr(KEY).get());
    }

    public SendResult send(Channel channel, ByteBuf buf) throws InterruptedException
    {
        try
        {
            MonitorEntry entry = channel.attr(KEY).get();
            if (entry == null) return SendResult.CH_INACTIVE;

            //并发发送
            Lock lock = entry.getLock();
            for (; ; )
            {
                try
                {
                    lock.lock();
                    //失活，退出
                    if (!channel.isActive()) return SendResult.CH_INACTIVE;

                    //可写，退出
                    if (channel.isWritable())
                    {
                        channel.writeAndFlush(buf.retain());
                        return SendResult.SUCCESS;
                    }

                    //不可写，加入队列
                    entry.getCondition().await();
                }
                finally
                {
                    lock.unlock();
                }
            }
        }
        finally
        {
            buf.release();
        }
    }

    private void wakeup0(MonitorEntry entry)
    {
        if (entry == null) return;

        //排他遍历队列，唤醒
        Lock lock = entry.getLock();
        try
        {
            lock.lock();
            entry.getCondition().signalAll();
        }
        finally
        {
            lock.unlock();
        }
    }

    private class MonitorEntry
    {
        private final Lock lock = new ReentrantLock(fair);
        private final Condition condition = lock.newCondition();

        Lock getLock()
        {
            return lock;
        }

        Condition getCondition()
        {
            return condition;
        }
    }
}
