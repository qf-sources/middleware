package io.qiufen.common.interceptor.impl;

import io.qiufen.common.interceptor.spi.*;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * 同步聚合的拦截器
 */
public class AggregatedSyncInterceptor implements AggregatedInterceptor, OnBefore, OnAfter, OnException
{
    private final List<InterceptorWrapper> interceptorList = new ArrayList<InterceptorWrapper>();

    private final Deque<InterceptorWrapper> deque = new LinkedList<InterceptorWrapper>();

    public AggregatedSyncInterceptor(Interceptor... interceptors)
    {
        for (Interceptor interceptor : interceptors)
        {
            add(interceptor);
        }
    }

    @Override
    public boolean doBefore(InterceptorContext context) throws Exception
    {
        for (InterceptorWrapper interceptor : interceptorList)
        {
            deque.offerLast(interceptor);
            if (interceptor.getOnBefore() == null || interceptor.getOnBefore().doBefore(context))
            {
                continue;
            }
            return false;
        }
        return true;
    }

    @Override
    public void doAfter(InterceptorContext context) throws Exception
    {
        InterceptorWrapper interceptor;
        while ((interceptor = deque.pollLast()) != null)
        {
            OnAfter onAfter = interceptor.getOnAfter();
            OnFinal onFinal = interceptor.getOnFinal();
            boolean success = false;
            try
            {
                if (onAfter != null)
                {
                    onAfter.doAfter(context);
                }
                success = true;
            }
            catch (Exception e)
            {
                //after处理异常，将当前拦截器重新放入队列尾部待异常处理
                deque.offerLast(interceptor);
                throw e;
            }
            finally
            {
                //after处理成功，处理final
                if (success)
                {
                    if (onFinal != null)
                    {
                        onFinal.doFinal(context);
                    }
                }
            }
        }
    }

    @Override
    public void doException(InterceptorContext context, Exception source) throws Exception
    {
        InterceptorWrapper interceptor = deque.pollLast();
        if (interceptor == null)
        {
            throw source;
        }
        Exception ex = null;
        try
        {
            OnException onException = interceptor.getOnException();
            OnFinal onFinal = interceptor.getOnFinal();
            try
            {
                if (onException == null)
                {
                    throw source;
                }
                onException.doException(context, source);
            }
            finally
            {
                if (onFinal != null)
                {
                    onFinal.doFinal(context);
                }
            }
        }
        catch (Exception e1)
        {
            ex = e1;
        }

        if (ex == null)
        {
            try
            {
                doAfter(context);
            }
            catch (Exception e)
            {
                doException(context, e);
            }
        }
        else
        {
            doException(context, ex);
        }
    }

    @Override
    public AggregatedInterceptor add(Interceptor interceptor)
    {
        interceptorList.add(new InterceptorWrapper(null, interceptor));
        return this;
    }
}