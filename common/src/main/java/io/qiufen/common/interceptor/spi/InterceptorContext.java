package io.qiufen.common.interceptor.spi;

import io.qiufen.common.interceptor.api.InterceptorName;
import io.qiufen.common.interceptor.exception.InterceptorException;

/**
 * 拦截器上下内容
 */
public interface InterceptorContext
{
    /**
     * 设置即将流转的下一个拦截器
     */
    void next(InterceptorName name);

    /**
     * 获取流转下一个拦截器
     */
    InterceptorName next();

    /**
     * 导出K - V到上下内容
     */
    void export(Object key, Object value);

    /**
     * 从上下内容引用K，不存在则抛出异常
     */
    Object require(Object key) throws InterceptorException;

    /**
     * 从上下内容尝试引用K，不存在则返回NULL
     */
    Object tryRequire(Object key);
}