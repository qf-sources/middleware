package io.qiufen.common.interceptor.spi;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/9/9 16:47
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface OnException extends Interceptor
{
    void doException(InterceptorContext context, Exception source) throws Exception;
}