package io.qiufen.common.interceptor.impl;

import io.qiufen.common.interceptor.api.InterceptorName;
import io.qiufen.common.interceptor.spi.*;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/9/9 17:01
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class InterceptorWrapper
{
    private final InterceptorName name;

    private final Interceptor interceptor;
    private final OnBefore onBefore;
    private final OnAfter onAfter;
    private final OnException onException;
    private final OnFinal onFinal;

    InterceptorWrapper(InterceptorName name, Interceptor interceptor)
    {
        this.name = name;

        this.interceptor = interceptor;
        this.onBefore = interceptor instanceof OnBefore ? (OnBefore) interceptor : null;
        this.onAfter = interceptor instanceof OnAfter ? (OnAfter) interceptor : null;
        this.onException = interceptor instanceof OnException ? (OnException) interceptor : null;
        this.onFinal = interceptor instanceof OnFinal ? (OnFinal) interceptor : null;
    }

    InterceptorName getName()
    {
        return name;
    }

    Interceptor getInterceptor()
    {
        return interceptor;
    }

    OnBefore getOnBefore()
    {
        return onBefore;
    }

    OnAfter getOnAfter()
    {
        return onAfter;
    }

    OnException getOnException()
    {
        return onException;
    }

    OnFinal getOnFinal()
    {
        return onFinal;
    }
}