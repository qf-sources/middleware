package io.qiufen.common.interceptor.api;

import io.qiufen.common.interceptor.spi.InterceptorContext;

/**
 * 拦截器链
 */
public interface InterceptorChain extends InterceptorRegister
{
    /**
     * 调用拦截器链
     */
    void invoke(InterceptorContext context);
}