package io.qiufen.common.interceptor.impl;

import io.qiufen.common.interceptor.api.InterceptorChain;
import io.qiufen.common.interceptor.api.InterceptorName;
import io.qiufen.common.interceptor.api.InterceptorRegister;
import io.qiufen.common.interceptor.exception.InterceptorException;
import io.qiufen.common.interceptor.spi.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/22 16:01
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class DefaultInterceptorChain implements InterceptorChain
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAsyncInterceptorChain.class);

    private final List<InterceptorWrapper> interceptorList = new ArrayList<InterceptorWrapper>();

    @Override
    public InterceptorRegister add(Interceptor interceptor)
    {
        return this.add(null, interceptor);
    }

    @Override
    public InterceptorRegister add(InterceptorName name, Interceptor interceptor)
    {
        this.interceptorList.add(new InterceptorWrapper(name, interceptor));
        return this;
    }

    @Override
    public void invoke(InterceptorContext context)
    {
        try
        {
            runSync(new InterceptorMatcher(interceptorList, context), context);
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new InterceptorException(e);
        }
    }

    private void runSync(InterceptorMatcher matcher, InterceptorContext context) throws Exception
    {
        InterceptorWrapper interceptorWrapper = matcher.match();
        if (interceptorWrapper == null)
        {
            return;
        }
        Interceptor interceptor = interceptorWrapper.getInterceptor();
        OnBefore onBefore = interceptorWrapper.getOnBefore();
        OnAfter onAfter = interceptorWrapper.getOnAfter();
        OnException onException = interceptorWrapper.getOnException();
        OnFinal onFinal = interceptorWrapper.getOnFinal();
        try
        {
            if (LOGGER.isInfoEnabled())
            {
                LOGGER.info("Using interceptor,name:{},class:{}", interceptorWrapper.getName(),
                        interceptor.getClass().getName());
            }

            if (onBefore == null || onBefore.doBefore(context))
            {
                runSync(matcher, context);
            }
            if (onAfter != null)
            {
                onAfter.doAfter(context);
            }
        }
        catch (Exception e)
        {
            if (onException == null)
            {
                throw e;
            }
            onException.doException(context, e);
        }
        finally
        {
            if (onFinal != null)
            {
                onFinal.doFinal(context);
            }
        }
    }
}