package io.qiufen.common.interceptor.exception;

import java.util.List;

/**
 * Created by zhangruzheng on 2021/6/26.
 */
public class AggregatedAsyncException extends RuntimeException
{
    private final List<Exception> sourceList;

    public AggregatedAsyncException(List<Exception> sourceList)
    {
        this.sourceList = sourceList;
    }

    @Override
    public String toString()
    {
        return "AggregatedAsyncException{" + "sourceList=" + sourceList + '}';
    }
}
