package io.qiufen.common.interceptor.api;

import io.qiufen.common.interceptor.spi.InterceptorContext;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/22 16:04
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface InvocationFeedback
{
    void onComplete(InterceptorContext context);

    void onException(InterceptorContext context, Exception e);
}