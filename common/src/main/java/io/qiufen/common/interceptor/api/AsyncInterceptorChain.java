package io.qiufen.common.interceptor.api;

import io.qiufen.common.interceptor.spi.Interceptor;
import io.qiufen.common.interceptor.spi.InterceptorContext;

import java.util.concurrent.ExecutorService;

/**
 * 异步拦截器链
 */
public interface AsyncInterceptorChain extends InterceptorRegister
{
    /**
     * 注册拦截器
     */
    AsyncInterceptorChain add(InterceptorName name, Interceptor interceptor, ExecutorService executor);

    /**
     * 调用拦截器链
     */
    void invoke(InterceptorContext context, InvocationFeedback feedback);
}