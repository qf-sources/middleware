package io.qiufen.common.interceptor.api;

import io.qiufen.common.interceptor.spi.Interceptor;

/**
 * 拦截器注册者
 * todo interceptor支持指定位置
 */
public interface InterceptorRegister
{
    /**
     * 注册无名拦截器
     */
    InterceptorRegister add(Interceptor interceptor);

    /**
     * 注册有名拦截器
     */
    InterceptorRegister add(InterceptorName name, Interceptor interceptor);
}