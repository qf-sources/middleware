package io.qiufen.common.interceptor.spi;

/**
 * 可聚合的拦截器
 *
 * @author Ruzheng Zhang
 * @date 2021/6/23 11:14
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface AggregatedInterceptor extends Interceptor
{
    AggregatedInterceptor add(Interceptor interceptor);
}