package io.qiufen.common.interceptor.impl;

import io.qiufen.common.interceptor.api.InterceptorName;
import io.qiufen.common.interceptor.exception.InterceptorException;
import io.qiufen.common.interceptor.spi.Interceptor;

import java.util.concurrent.ExecutorService;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/23 19:08
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class InterceptorInner extends InterceptorWrapper
{
    private final ExecutorService executor;

    InterceptorInner(InterceptorName name, Interceptor interceptor, ExecutorService executor)
    {
        super(name, interceptor);
        this.executor = executor;
    }

    ExecutorService getExecutor()
    {
        if (executor == null)
        {
            throw new InterceptorException("None executor service bind!");
        }
        return executor;
    }
}