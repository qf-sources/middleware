package io.qiufen.common.interceptor.api;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/23 17:58
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class InterceptorName
{
    private final String name;

    public InterceptorName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InterceptorName that = (InterceptorName) o;

        return !(name != null ? !name.equals(that.name) : that.name != null);

    }

    @Override
    public int hashCode()
    {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString()
    {
        return "InterceptorName{" + "name='" + name + '\'' + '}';
    }
}