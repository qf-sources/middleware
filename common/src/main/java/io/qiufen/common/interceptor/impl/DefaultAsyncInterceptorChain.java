package io.qiufen.common.interceptor.impl;

import io.qiufen.common.interceptor.api.AsyncInterceptorChain;
import io.qiufen.common.interceptor.api.InterceptorName;
import io.qiufen.common.interceptor.api.InterceptorRegister;
import io.qiufen.common.interceptor.api.InvocationFeedback;
import io.qiufen.common.interceptor.spi.Interceptor;
import io.qiufen.common.interceptor.spi.InterceptorContext;
import io.qiufen.common.interceptor.spi.OnBefore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/22 16:01
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class DefaultAsyncInterceptorChain implements AsyncInterceptorChain
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAsyncInterceptorChain.class);

    private final List<InterceptorWrapper> interceptorList = new ArrayList<InterceptorWrapper>();

    private final ExecutorService executor;

    public DefaultAsyncInterceptorChain(ExecutorService executor)
    {
        this.executor = executor;
    }

    @Override
    public InterceptorRegister add(Interceptor interceptor)
    {
        return this.add(null, interceptor);
    }

    @Override
    public InterceptorRegister add(InterceptorName name, Interceptor interceptor)
    {
        return this.add(name, interceptor, null);
    }

    @Override
    public AsyncInterceptorChain add(InterceptorName name, Interceptor interceptor, ExecutorService executor)
    {
        this.interceptorList.add(new InterceptorInner(name, interceptor, executor == null ? this.executor : executor));
        return this;
    }

    @Override
    public void invoke(InterceptorContext context, InvocationFeedback feedback)
    {
        runAsync(new InterceptorMatcher(interceptorList, context), context, new FeedbackInner(feedback, null));
    }

    private void runAsync(final InterceptorMatcher matcher, final InterceptorContext context,
            final FeedbackInner promise)
    {
        InterceptorWrapper interceptorWrapper = matcher.match();
        if (interceptorWrapper == null)
        {
            promise.onComplete(context);
            return;
        }

        final InterceptorInner interceptorInner = (InterceptorInner) interceptorWrapper;
        final OnBefore onBefore = interceptorInner.getOnBefore();

        final FeedbackInner currentFeedback = new FeedbackInner(promise, interceptorInner);
        if (onBefore == null)
        {
            DefaultAsyncInterceptorChain.this.runAsync(matcher, context, currentFeedback);
            return;
        }

        interceptorInner.getExecutor().execute(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    if (LOGGER.isInfoEnabled())
                    {
                        LOGGER.info("Using interceptor,name:{},class:{}", interceptorInner.getName(),
                                interceptorInner.getInterceptor().getClass().getName());
                    }

                    if (onBefore.doBefore(context))
                    {
                        DefaultAsyncInterceptorChain.this.runAsync(matcher, context, currentFeedback);
                        return;
                    }
                }
                catch (Exception e)
                {
                    currentFeedback.onException(context, e);
                    return;
                }

                currentFeedback.onComplete(context);
            }
        });
    }
}