package io.qiufen.common.interceptor.impl;

import io.qiufen.common.interceptor.api.InterceptorName;
import io.qiufen.common.interceptor.exception.InterceptorException;
import io.qiufen.common.interceptor.spi.InterceptorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/29 10:29
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class DefaultInterceptorContext implements InterceptorContext
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultInterceptorContext.class);

    private final Map<Object, Object> valueMap = new ConcurrentHashMap<Object, Object>();

    private volatile InterceptorName name;

    @Override
    public void next(InterceptorName name)
    {
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Set next interceptor name:{}", name);
        }
        this.name = name;
    }

    @Override
    public InterceptorName next()
    {
        return this.name;
    }

    @Override
    public void export(Object key, Object value)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Export value into context,key:({}){},value:({}){}", getTypeName(key), key, getTypeName(value),
                    value);
        }
        this.valueMap.put(key, value);
    }

    @Override
    public Object require(Object key) throws InterceptorException
    {
        Object value = this.valueMap.get(key);
        if (value == null)
        {
            throw new InterceptorException("Require value failure,key:" + key);
        }
        return value;
    }

    @Override
    public Object tryRequire(Object key)
    {
        return this.valueMap.get(key);
    }

    private String getTypeName(Object o)
    {
        if (o == null)
        {
            return "NULL";
        }
        return o.getClass().getTypeName();
    }
}