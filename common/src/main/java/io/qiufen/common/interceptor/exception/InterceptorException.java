package io.qiufen.common.interceptor.exception;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/23 11:06
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class InterceptorException extends RuntimeException
{
    public InterceptorException(String message)
    {
        super(message);
    }

    public InterceptorException(Throwable cause)
    {
        super(cause);
    }
}