package io.qiufen.common.interceptor.impl;

import io.qiufen.common.interceptor.exception.AggregatedAsyncException;
import io.qiufen.common.interceptor.spi.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;

/**
 * 异步聚合的拦截器
 */
public class AggregatedAsyncInterceptor implements AggregatedInterceptor, OnBefore
{
    private final List<InterceptorWrapper> interceptorList = new ArrayList<InterceptorWrapper>();

    private final ExecutorService executor;

    public AggregatedAsyncInterceptor(ExecutorService executor, Interceptor... interceptors)
    {
        this.executor = executor;

        for (Interceptor interceptor : interceptors)
        {
            add(interceptor);
        }
    }

    @Override
    public boolean doBefore(final InterceptorContext context) throws Exception
    {
        final Semaphore semaphore = new Semaphore(0);
        int count = 0;
        final Queue<Exception> exceptions = new ConcurrentLinkedQueue<Exception>();
        for (final InterceptorWrapper interceptor : interceptorList)
        {
            count++;
            executor.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    try
                    {
                        OnBefore onBefore = interceptor.getOnBefore();
                        OnAfter onAfter = interceptor.getOnAfter();
                        OnException onException = interceptor.getOnException();
                        OnFinal onFinal = interceptor.getOnFinal();
                        try
                        {
                            if (onBefore != null)
                            {
                                onBefore.doBefore(context);
                            }
                            if (onAfter != null)
                            {
                                onAfter.doAfter(context);
                            }
                        }
                        catch (Exception e)
                        {
                            if (onException == null)
                            {
                                throw e;
                            }
                            onException.doException(context, e);
                        }
                        finally
                        {
                            if (onFinal != null)
                            {
                                onFinal.doFinal(context);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        exceptions.add(e);
                    }
                    finally
                    {
                        semaphore.release(1);
                    }
                }
            });
        }

        try
        {
            semaphore.acquire(count);
        }
        catch (InterruptedException e)
        {
            exceptions.add(e);
        }
        if (exceptions.isEmpty())
        {
            return true;
        }
        throw new AggregatedAsyncException(new ArrayList<Exception>(exceptions));
    }

    @Override
    public AggregatedInterceptor add(Interceptor interceptor)
    {
        interceptorList.add(new InterceptorWrapper(null, interceptor));
        return this;
    }
}