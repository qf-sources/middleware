package io.qiufen.common.interceptor.impl;

import io.qiufen.common.interceptor.api.InterceptorName;
import io.qiufen.common.interceptor.exception.InterceptorException;
import io.qiufen.common.interceptor.spi.InterceptorContext;

import java.util.Iterator;
import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/23 17:03
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class InterceptorMatcher
{
    private final Iterator<InterceptorWrapper> iterator;
    private final InterceptorContext context;

    public InterceptorMatcher(List<InterceptorWrapper> interceptors, InterceptorContext context)
    {
        this.iterator = interceptors.iterator();
        this.context = context;
    }

    public InterceptorWrapper match()
    {
        if (!iterator.hasNext())
        {
            return null;
        }

        InterceptorName next = context.next();
        if (next == null)
        {
            return iterator.next();
        }

        InterceptorWrapper interceptorWrapper = null;
        try
        {
            interceptorWrapper = match0(next);
        }
        finally
        {
            context.next(null);
        }
        if (interceptorWrapper == null)
        {
            throw new InterceptorException("Can't match any interceptor for name:" + next);
        }

        return interceptorWrapper;
    }

    private InterceptorWrapper match0(InterceptorName next)
    {
        InterceptorWrapper interceptorWrapper = iterator.next();
        if (next == interceptorWrapper.getName())
        {
            return interceptorWrapper;
        }
        if (iterator.hasNext())
        {
            return match0(next);
        }
        return null;
    }
}