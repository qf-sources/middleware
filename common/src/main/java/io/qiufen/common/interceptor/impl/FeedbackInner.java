package io.qiufen.common.interceptor.impl;

import io.qiufen.common.interceptor.api.InvocationFeedback;
import io.qiufen.common.interceptor.spi.InterceptorContext;
import io.qiufen.common.interceptor.spi.OnAfter;
import io.qiufen.common.interceptor.spi.OnException;
import io.qiufen.common.interceptor.spi.OnFinal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2021/6/25 10:44
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class FeedbackInner implements InvocationFeedback
{
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackInner.class);

    final InvocationFeedback parent;
    final InterceptorInner interceptorInner;

    FeedbackInner(InvocationFeedback parent, InterceptorInner interceptorInner)
    {
        this.parent = parent;
        this.interceptorInner = interceptorInner;
    }

    @Override
    public void onComplete(final InterceptorContext context)
    {

        if (interceptorInner == null)
        {
            doParent(context, null);
            return;
        }

        final OnAfter onAfter = interceptorInner.getOnAfter();
        final OnFinal onFinal = interceptorInner.getOnFinal();
        if (onAfter == null && onFinal == null)
        {
            doParent(context, null);
            return;
        }
        interceptorInner.getExecutor().execute(new Runnable()
        {
            @Override
            public void run()
            {
                Exception ex = null;
                try
                {
                    if (LOGGER.isInfoEnabled())
                    {
                        LOGGER.info("Using interceptor,name:{},class:{}", interceptorInner.getName(),
                                interceptorInner.getInterceptor().getClass().getName());
                    }

                    try
                    {
                        if (onAfter != null)
                        {
                            onAfter.doAfter(context);
                        }
                    }
                    finally
                    {
                        if (onFinal != null)
                        {
                            onFinal.doFinal(context);
                        }
                    }
                }
                catch (Exception e)
                {
                    ex = e;
                }
                doParent(context, ex);
            }
        });
    }

    @Override
    public void onException(final InterceptorContext context, final Exception e)
    {
        if (interceptorInner == null)
        {
            doParent(context, e);
            return;
        }

        final OnException onException = interceptorInner.getOnException();
        final OnFinal onFinal = interceptorInner.getOnFinal();
        if (onException == null && onFinal == null)
        {
            doParent(context, e);
            return;
        }
        interceptorInner.getExecutor().execute(new Runnable()
        {
            @Override
            public void run()
            {
                Exception ex = null;
                try
                {
                    if (LOGGER.isInfoEnabled())
                    {
                        LOGGER.info("Using interceptor,name:{},class:{}", interceptorInner.getName(),
                                interceptorInner.getInterceptor().getClass().getName());
                    }

                    try
                    {
                        if (onException == null)
                        {
                            throw e;
                        }
                        onException.doException(context, e);
                    }
                    finally
                    {
                        if (onFinal != null)
                        {
                            onFinal.doFinal(context);
                        }
                    }
                }
                catch (Exception e1)
                {
                    ex = e1;
                }
                doParent(context, ex);
            }
        });
    }

    private void doParent(final InterceptorContext context, final Exception e)
    {
        if (parent == null)
        {
            return;
        }
        if (e == null)
        {
            parent.onComplete(context);
            return;
        }
        parent.onException(context, e);
    }
}