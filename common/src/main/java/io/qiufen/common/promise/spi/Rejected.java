package io.qiufen.common.promise.spi;

import io.qiufen.common.promise.api.Context;

public interface Rejected<I, ORV>
{
    void onRejected(Context<ORV> context, I value) throws Exception;
}
