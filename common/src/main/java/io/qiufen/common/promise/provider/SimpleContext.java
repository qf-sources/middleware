package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.api.Future;
import io.qiufen.common.promise.api.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("rawtypes")
class SimpleContext implements Context
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleContext.class);

    ContextHandler handler = EmptyContextHandler.INSTANCE;

    State targetState;
    Object value;

    @Override
    public void resolve(Object value)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Resolve:{}...", value);
        }
        this.value = value;
        this.targetState = State.RESOLVED;
        this.handler = ResolvedObjectContextHandler.INSTANCE;
    }

    @Override
    public void resolve(Future future)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Resolve[future:{}]...", future.getClass().getName());
        }
        this.value = future;
        this.targetState = State.RESOLVED;
        this.handler = ResolvedFutureContextHandler.INSTANCE;
    }

    @Override
    public void reject(Object value)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Reject");
        }
        this.value = value;
        this.targetState = State.REJECTED;
        this.handler = RejectedContextHandler.INSTANCE;
    }

    void reset()
    {
        targetState = State.PENDING;
        value = null;
        handler = EmptyContextHandler.INSTANCE;
    }
}
