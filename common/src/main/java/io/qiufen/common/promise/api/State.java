package io.qiufen.common.promise.api;

public enum State
{
    PENDING,
    RESOLVED,
    REJECTED
}
