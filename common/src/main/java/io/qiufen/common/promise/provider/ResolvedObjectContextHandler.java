package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Promise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"unchecked", "rawtypes"})
class ResolvedObjectContextHandler extends ContextHandler<Object>
{
    static final ContextHandler<Object> INSTANCE = new ResolvedObjectContextHandler();
    private static final Logger LOGGER = LoggerFactory.getLogger(ResolvedObjectContextHandler.class);

    @Override
    public void doHandle(Promise nextPromise, SimpleContext context, Object value)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Resolve[promise:{}]", nextPromise.getClass().getName());
        }
        nextPromise.resolve(value);
    }

    @Override
    public void doHandle(Then then, SimpleContext context, Object value)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Resolve[{}]", then.resolved.getClass().getName());
        }
        Promise nextPromise = then.nextPromise;
        try
        {
            context.reset();
            then.resolved.onResolved(context, value);
        }
        catch (Exception e)
        {
            nextPromise.reject(e);
            return;
        }

        context.handler.doHandle(nextPromise, context, context.value);
    }
}
