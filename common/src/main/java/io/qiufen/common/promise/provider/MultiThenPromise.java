package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Promise;

class MultiThenPromise<IRV> extends AbstractPromise<IRV>
{
    private final ThenNode rootThen = new ThenNode(null);

    private ThenNode currentThen = rootThen;

    MultiThenPromise(SimpleContext context)
    {
        super(context);
    }

    @Override
    <ORV> Promise<ORV> createPromise(SimpleContext context)
    {
        return new MultiThenPromise<ORV>(context);
    }

    @Override
    void preparePendingThen(Then then, SimpleContext context)
    {
        this.currentThen.next = new ThenNode(then);
        this.currentThen = this.currentThen.next;
    }

    @Override
    <V> void doHandleValue(ContextHandler<V> contextHandler, SimpleContext context, V value)
    {
        ThenNode node = this.rootThen;
        while ((node = node.next) != null)
        {
            contextHandler.doHandle(node.then, context, value);
        }
    }

    private static class ThenNode
    {
        private final Then then;
        private ThenNode next;

        private ThenNode(Then then)
        {
            this.then = then;
        }
    }
}
