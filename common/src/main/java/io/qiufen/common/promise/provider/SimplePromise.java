package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Promise;

@SuppressWarnings({"rawtypes"})
class SimplePromise<IRV> extends AbstractPromise<IRV>
{
    private ValueHandler valueHandler = ValueHandler.EMPTY;

    private Then currentThen;

    SimplePromise()
    {
        this(new SimpleContext());
    }

    private SimplePromise(SimpleContext context)
    {
        super(context);
    }

    @Override
    <ORV> Promise<ORV> createPromise(SimpleContext context)
    {
        return new SimplePromise<ORV>(context);
    }

    @Override
    void preparePendingThen(Then then, SimpleContext context)
    {
        Then currentThen = this.currentThen;
        if (currentThen == null)
        {
            this.currentThen = then;
            this.valueHandler = PrivateValueHandler.PRIVATE;
            return;
        }

        if (currentThen.nextPromise instanceof MultiThenPromise)
        {
            ((MultiThenPromise) currentThen.nextPromise).then(then);
            return;
        }

        MultiThenPromise multiThenPromise = new MultiThenPromise(context);
        Then multiThen = new Then(EmptyResponse.INSTANCE, EmptyResponse.INSTANCE, multiThenPromise);
        multiThenPromise.then(new Then(currentThen.resolved, currentThen.rejected, currentThen.nextPromise));
        multiThenPromise.then(then);
        this.currentThen = multiThen;
    }

    @Override
    <V> void doHandleValue(ContextHandler<V> contextHandler, SimpleContext context, V value)
    {
        this.valueHandler.doHandle(this.currentThen, contextHandler, context, value);
    }

    private static class ValueHandler
    {
        private static final ValueHandler EMPTY = new ValueHandler();

        <V> void doHandle(Then then, ContextHandler<V> contextHandler, SimpleContext context, V value) {}
    }

    private static class PrivateValueHandler extends ValueHandler
    {
        private static final ValueHandler PRIVATE = new PrivateValueHandler();

        @Override
        <V> void doHandle(Then then, ContextHandler<V> contextHandler, SimpleContext context, V value)
        {
            contextHandler.doHandle(then, context, value);
        }
    }
}

