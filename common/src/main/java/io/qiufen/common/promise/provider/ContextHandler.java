package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Promise;

@SuppressWarnings("rawtypes")
class ContextHandler<V>
{
    void doHandle(Promise nextPromise, SimpleContext context, V value) {}

    void doHandle(Then then, SimpleContext context, V value) {}
}
