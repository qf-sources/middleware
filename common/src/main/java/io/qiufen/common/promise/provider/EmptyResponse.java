package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.spi.Rejected;
import io.qiufen.common.promise.spi.Resolved;

@SuppressWarnings({"rawtypes", "unchecked"})
class EmptyResponse implements Resolved, Rejected
{
    static final EmptyResponse INSTANCE = new EmptyResponse();

    @Override
    public void onResolved(Context context, Object value) {context.resolve(value);}

    @Override
    public void onRejected(Context context, Object value) {context.reject(value);}
}
