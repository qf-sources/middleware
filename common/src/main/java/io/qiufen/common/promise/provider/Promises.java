package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.api.Promise;
import io.qiufen.common.promise.api.State;
import io.qiufen.common.promise.spi.Rejected;
import io.qiufen.common.promise.spi.Resolved;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Promises
{
    public static <IRV> Promise<IRV> create()
    {
        return new SimplePromise<IRV>();
    }

    @SuppressWarnings("unchecked")
    public static Promise<Map<Promise<?>, BatchValue>> both(Collection<? extends Promise<?>> promises)
    {
        int size = promises.size();
        Map<Promise<?>, BatchValue> values = new ConcurrentHashMap<Promise<?>, BatchValue>(size);
        Promise<Map<Promise<?>, BatchValue>> promise = create();
        AtomicInteger count = new AtomicInteger(0);
        for (Promise<?> p : promises)
        {
            BothResponse response = new BothResponse(promise, values, size, count, p);
            p.future().then(response, response);
        }
        return promise;
    }

    public static Promise<Map<Promise<?>, BatchValue>> both(Promise<?>... promises)
    {
        return both(Arrays.asList(promises));
    }

    @SuppressWarnings("rawtypes")
    private static class BothResponse implements Resolved, Rejected
    {
        private final Promise<Map<Promise<?>, BatchValue>> promise;
        private final Map<Promise<?>, BatchValue> values;
        private final int size;
        private final AtomicInteger count;
        private final Promise<?> p;

        private BothResponse(Promise<Map<Promise<?>, BatchValue>> promise, Map<Promise<?>, BatchValue> values, int size,
                AtomicInteger count, Promise<?> p)
        {
            this.promise = promise;
            this.p = p;
            this.values = values;
            this.size = size;
            this.count = count;
        }

        @Override
        public void onResolved(Context context, Object value)
        {
            values.put(p, new BatchValue(State.RESOLVED, value));
            resolve();
        }

        @Override
        public void onRejected(Context context, Object value)
        {
            values.put(p, new BatchValue(State.REJECTED, value));
            resolve();
        }

        private void resolve()
        {
            if (count.incrementAndGet() >= size)
            {
                promise.resolve(values);
            }
        }
    }
}
