package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.State;

public class BatchValue
{
    private final State state;
    private final Object value;

    BatchValue(State state, Object value)
    {
        this.state = state;
        this.value = value;
    }

    public State getState()
    {
        return state;
    }

    public Object getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return "BatchValue{" + "state=" + state + ", value=" + value + '}';
    }
}
