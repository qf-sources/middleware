package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Future;
import io.qiufen.common.promise.api.Promise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"rawtypes"})
class ResolvedFutureContextHandler extends ContextHandler<Future>
{
    static final ContextHandler<Future> INSTANCE = new ResolvedFutureContextHandler();
    private static final Logger LOGGER = LoggerFactory.getLogger(ResolvedFutureContextHandler.class);

    @Override
    public void doHandle(Promise nextPromise, SimpleContext context, Future value)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Resolve[promise:{}]", nextPromise.getClass().getName());
        }
        nextPromise.after(value);
    }

    @Override
    public void doHandle(Then then, SimpleContext context, Future value)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Resolve[{}]", then.resolved.getClass().getName());
        }
        then.nextPromise.after(value);
    }
}
