package io.qiufen.common.promise.spi;

import io.qiufen.common.promise.api.Context;

public interface Resolved<I, ORV>
{
    void onResolved(Context<ORV> context, I value) throws Exception;
}
