package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Promise;
import io.qiufen.common.promise.spi.Rejected;
import io.qiufen.common.promise.spi.Resolved;

@SuppressWarnings("rawtypes")
class Then
{
    final Resolved resolved;
    final Rejected rejected;
    final Promise nextPromise;

    Then(Resolved resolved, Rejected rejected, Promise nextPromise)
    {
        this.resolved = resolved;
        this.rejected = rejected;
        this.nextPromise = nextPromise;
    }
}
