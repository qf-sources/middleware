package io.qiufen.common.promise.api;

import io.qiufen.common.promise.spi.Rejected;
import io.qiufen.common.promise.spi.Resolved;

public interface Future<IRV> extends java.util.concurrent.Future<IRV>
{
    <IRT, ORV> Future<ORV> then(Resolved<IRV, ORV> resolved, Rejected<IRT, ORV> rejected);

    <ORV> Future<ORV> then(Resolved<IRV, ORV> resolved);

    <IRT, ORV> Future<ORV> then(Rejected<IRT, ORV> rejected);
}
