package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.spi.Rejected;
import io.qiufen.common.promise.spi.Resolved;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

class SyncResponse<IRV> implements Resolved<IRV, Object>, Rejected<Object, Object>
{
    private final Semaphore semaphore = new Semaphore(0);
    private volatile IRV value;
    private volatile Throwable failure;

    @Override
    public void onRejected(Context<Object> context, Object value)
    {
        if (value == null)
        {
            this.failure = new Exception("Null");
        }
        else if (value instanceof Throwable)
        {
            this.failure = (Throwable) value;
        }
        else
        {
            this.failure = new Exception(value.toString());
        }
        semaphore.release(1);
        context.reject(value);
    }

    @Override
    public void onResolved(Context<Object> context, IRV value)
    {
        this.value = value;
        semaphore.release(1);
        context.resolve(value);
    }

    IRV get() throws InterruptedException, ExecutionException
    {
        semaphore.acquire(1);
        if (failure == null)
        {
            return this.value;
        }
        throw new ExecutionException(failure);
    }

    IRV get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException
    {
        if (semaphore.tryAcquire(1, timeout, unit))
        {
            if (failure == null)
            {
                return this.value;
            }
            throw new ExecutionException(failure);
        }
        throw new TimeoutException();
    }
}
