package io.qiufen.common.promise.api;

@SuppressWarnings("rawtypes")
public interface Promise<IRV> extends Context<IRV>
{
    State state();

    void after(Future future);

    Future<IRV> future();
}
