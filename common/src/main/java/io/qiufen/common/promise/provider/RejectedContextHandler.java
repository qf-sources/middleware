package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Promise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"rawtypes", "unchecked"})
class RejectedContextHandler extends ContextHandler
{
    static final ContextHandler INSTANCE = new RejectedContextHandler();
    private static final Logger LOGGER = LoggerFactory.getLogger(RejectedContextHandler.class);

    @Override
    void doHandle(Promise nextPromise, SimpleContext context, Object value)
    {
        nextPromise.reject(value);
    }

    @Override
    void doHandle(Then then, SimpleContext context, Object value)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Reject:{}...", value);
        }
        Promise nextPromise = then.nextPromise;
        try
        {
            context.reset();
            then.rejected.onRejected(context, value);
        }
        catch (Exception e)
        {
            nextPromise.reject(e);
            return;
        }
        context.handler.doHandle(nextPromise, context, context.value);
    }
}
