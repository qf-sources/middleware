package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Promise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("rawtypes")
class EmptyContextHandler extends ContextHandler<Object>
{
    static final ContextHandler INSTANCE = new EmptyContextHandler();
    private static final Logger LOGGER = LoggerFactory.getLogger(EmptyContextHandler.class);

    @Override
    public void doHandle(Promise nextPromise, SimpleContext context, Object value)
    {
        LOGGER.error("Target state is:{}, won't do anything more!", context.targetState);
    }
}
