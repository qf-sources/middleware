package io.qiufen.common.promise.provider;

import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.api.Future;
import io.qiufen.common.promise.api.Promise;
import io.qiufen.common.promise.api.State;
import io.qiufen.common.promise.spi.Rejected;
import io.qiufen.common.promise.spi.Resolved;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

@SuppressWarnings({"unchecked", "rawtypes"})
abstract class AbstractPromise<IRV> implements Promise<IRV>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPromise.class);
    private final AtomicBoolean stateLock = new AtomicBoolean(false);

    private final PrivateFuture future = new PrivateFuture();

    private final SimpleContext context;

    private volatile State state = State.PENDING;
    private volatile Object value;
    private volatile ContextHandler contextHandler;

    AbstractPromise(SimpleContext context)
    {
        this.context = context;
    }

    @Override
    public State state()
    {
        return state;
    }

    @Override
    public void resolve(Object value)
    {
        resolve(ResolvedObjectContextHandler.INSTANCE, value);
    }

    @Override
    public void resolve(Future<IRV> future)
    {
        resolve(ResolvedFutureContextHandler.INSTANCE, future);
    }

    @Override
    public void reject(Object value)
    {
        if (this.state != State.PENDING)
        {
            throw new IllegalStateException("Not pending state.");
        }

        this.value = value;
        while (!stateLock.compareAndSet(false, true))
        {
            LOGGER.debug("Lock state failure.");
        }
        try
        {
            this.state = State.REJECTED;
            this.contextHandler = RejectedContextHandler.INSTANCE;
            doHandleValue(this.contextHandler, this.context, value);
        }
        finally
        {
            stateLock.set(false);
        }
    }

    @Override
    public void after(Future future)
    {
        future.then(new Resolved()
        {
            @Override
            public void onResolved(Context context, Object value)
            {
                AbstractPromise.this.resolve(value);
                context.resolve(value);
            }
        }, new Rejected()
        {
            @Override
            public void onRejected(Context context, Object value)
            {
                AbstractPromise.this.reject(value);
                context.reject(value);
            }
        });
    }

    @Override
    public Future<IRV> future()
    {
        return this.future;
    }

    void then(Then then)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Then:{}", then.resolved.getClass().getName());
        }
        while (!stateLock.compareAndSet(false, true))
        {
            LOGGER.debug("Lock state failure.");
        }
        try
        {
            switch (state)
            {
                case PENDING:
                    preparePendingThen(then, this.context);
                    break;
                case RESOLVED:
                case REJECTED:
                    this.contextHandler.doHandle(then, this.context, this.value);
                    break;
                default:
            }
        }
        finally
        {
            stateLock.set(false);
        }
    }

    private <V> void resolve(ContextHandler<V> contextHandler, V value)
    {
        if (this.state != State.PENDING)
        {
            throw new IllegalStateException("Not pending state.");
        }

        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Resolve[contextHandler:{}]", contextHandler.getClass().getName());
        }
        this.value = value;
        while (!stateLock.compareAndSet(false, true))
        {
            LOGGER.debug("Lock state failure.");
        }
        try
        {
            this.state = State.RESOLVED;
            this.contextHandler = contextHandler;
            doHandleValue(contextHandler, this.context, value);
        }
        finally
        {
            stateLock.set(false);
        }
    }

    abstract <ORV> Promise<ORV> createPromise(SimpleContext context);

    abstract void preparePendingThen(Then then, SimpleContext context);

    abstract <V> void doHandleValue(ContextHandler<V> contextHandler, SimpleContext context, V value);

    private class PrivateFuture implements Future<IRV>
    {
        @Override
        public <IRT, ORV> Future<ORV> then(Resolved<IRV, ORV> resolved, Rejected<IRT, ORV> rejected)
        {
            Promise<ORV> promise = createPromise(context);
            AbstractPromise.this.then(new Then(resolved, rejected, promise));
            return promise.future();
        }

        @Override
        public <ORV> Future<ORV> then(Resolved<IRV, ORV> resolved)
        {
            return then(resolved, EmptyResponse.INSTANCE);
        }

        @Override
        public <IRT, ORV> Future<ORV> then(Rejected<IRT, ORV> rejected)
        {
            return then(EmptyResponse.INSTANCE, rejected);
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning)
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean isCancelled()
        {
            return false;
        }

        @Override
        public boolean isDone()
        {
            return state == State.RESOLVED || state == State.REJECTED;
        }

        @Override
        public IRV get() throws InterruptedException, ExecutionException
        {
            SyncResponse<IRV> response = new SyncResponse<IRV>();
            then(response, response);
            return response.get();
        }

        @Override
        public IRV get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException
        {
            SyncResponse<IRV> response = new SyncResponse<IRV>();
            then(response, response);
            return response.get(timeout, unit);
        }
    }
}