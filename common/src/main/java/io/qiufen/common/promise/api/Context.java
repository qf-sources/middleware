package io.qiufen.common.promise.api;

/**
 * promise响应上下环境
 *
 * @param <O> 预期输出类型
 */
public interface Context<O>
{
    void resolve(O value);

    void resolve(Future<O> future);

    void reject(Object value);
}
