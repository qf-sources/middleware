package io.qiufen.common.concurrency.executor.spi;

import io.qiufen.common.concurrency.executor.api.ExecutorConfig;

public interface CommandQueue
{
    boolean offer(Runnable command);

    Runnable poll();

    ExecutorConfig config();
}
