package io.qiufen.common.concurrency.executor.spi;

public interface RejectionPolicy
{
    void onReject(Runnable command, CommandQueue commandQueue);
}
