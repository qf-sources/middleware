package io.qiufen.common.concurrency.executor.api;

public class ThreadInfo
{
    private final String name;
    private final Thread.State state;

    public ThreadInfo(String name, Thread.State state)
    {
        this.name = name;
        this.state = state;
    }

    public String getName()
    {
        return name;
    }

    public Thread.State getState()
    {
        return state;
    }
}
