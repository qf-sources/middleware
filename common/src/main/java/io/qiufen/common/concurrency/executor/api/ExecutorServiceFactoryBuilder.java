package io.qiufen.common.concurrency.executor.api;

import io.qiufen.common.concurrency.executor.spi.CommandPollingFactory;
import io.qiufen.common.concurrency.executor.spi.CommandQueueFactory;

public interface ExecutorServiceFactoryBuilder
{
    ExecutorServiceFactoryBuilder setPoolConfig(PoolConfig config);

    ExecutorServiceFactoryBuilder setCommandPollingFactory(CommandPollingFactory factory);

    ExecutorServiceFactoryBuilder setCommandQueueFactory(CommandQueueFactory factory);

    ExecutorServiceFactory build();
}
