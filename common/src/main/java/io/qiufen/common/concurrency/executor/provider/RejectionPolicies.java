package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.spi.CommandQueue;
import io.qiufen.common.concurrency.executor.spi.RejectionPolicy;

public class RejectionPolicies
{
    public static final RejectionPolicy THROWING = new ThrowingRejectionPolicy();

    private RejectionPolicies()
    {
    }
}

class ThrowingRejectionPolicy implements RejectionPolicy
{
    @Override
    public void onReject(Runnable command, CommandQueue commandQueue)
    {
        throw new RuntimeException(command.toString());
    }
}