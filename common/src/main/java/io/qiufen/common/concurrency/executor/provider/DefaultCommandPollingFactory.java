package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.spi.CommandPolling;
import io.qiufen.common.concurrency.executor.spi.CommandPollingFactory;

class DefaultCommandPollingFactory implements CommandPollingFactory
{
    private static volatile CommandPollingFactory instance;

    private DefaultCommandPollingFactory()
    {
    }

    static CommandPollingFactory getInstance()
    {
        if (instance == null)
        {
            synchronized (DefaultCommandPollingFactory.class)
            {
                if (instance == null)
                {
                    instance = new DefaultCommandPollingFactory();
                }
            }
        }
        return instance;
    }

    @Override
    public CommandPolling create()
    {
        return new DefaultCommandPolling();
    }
}
