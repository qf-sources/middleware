package io.qiufen.common.concurrency.executor.spi;

import org.slf4j.LoggerFactory;

/**
 * Created by 善为之 on 2019/3/2.
 */
public abstract class Command implements Runnable
{
    @Override
    public final void run()
    {
        try
        {
            try
            {
                doExecute();
            }
            catch (Throwable throwable)
            {
                doException(throwable);
            }
            finally
            {
                doFinally();
            }
        }
        catch (Throwable throwable)
        {
            LoggerFactory.getLogger(getClass()).error(throwable.toString(), throwable);
        }
    }

    protected void doExecute() throws Throwable
    {
    }

    protected void doException(Throwable throwable) throws Throwable
    {
        LoggerFactory.getLogger(getClass()).error(throwable.toString(), throwable);
    }

    protected void doFinally() throws Throwable
    {
    }
}
