package io.qiufen.common.concurrency.executor.spi;

/**
 * 任务轮询器
 */
public interface CommandPolling
{
    /**
     * 增加队列
     */
    void addCommandQueue(CommandQueue commandQueue);

    /**
     * 轮询任务
     */
    Runnable poll(int sequence);

    /**
     * 是否繁忙模式
     */
    boolean isBusyMode();

    /**
     * 设置繁忙模式，以便使用不同的轮询策略
     */
    void setBusyMode(boolean busyMode);
}
