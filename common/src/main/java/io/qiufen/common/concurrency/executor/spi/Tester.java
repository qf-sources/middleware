package io.qiufen.common.concurrency.executor.spi;

import io.qiufen.common.concurrency.executor.api.Watcher;

/**
 * 监视器测试者
 */
public interface Tester
{
    /**
     * 测试是否需要扩容线程
     *
     * @param watcher 监视器
     */
    boolean testNeedExpand(Watcher watcher);

    /**
     * 测试可预测的任务
     *
     * @param watcher   监视器
     * @param command   可预测的任务
     * @param timestamp 任务时间戳
     */
    boolean testPredictableCommandTimeout(Watcher watcher, Predictable command, long timestamp);

    /**
     * 测试是否繁忙
     *
     * @param watcher 监视器
     */
    boolean testWorking(Watcher watcher);

    /**
     * 测试是否繁忙模式
     *
     * @param watcher 监视器
     */
    boolean testBusyMode(Watcher watcher);
}
