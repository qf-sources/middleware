package io.qiufen.common.concurrency.executor.api;

public class ThreadRecycleInfo extends ThreadInfo
{
    private final long lifeTime;

    public ThreadRecycleInfo(String name, Thread.State state, long lifeTime)
    {
        super(name, state);
        this.lifeTime = lifeTime;
    }

    public long getLifeTime()
    {
        return lifeTime;
    }
}
