package io.qiufen.common.concurrency.executor.spi;

import org.slf4j.LoggerFactory;

/**
 * Created by 善为之 on 2019/3/2.
 */
public abstract class ScheduledCommand implements Runnable
{
    private final boolean concurrent;

    private volatile boolean running = false;

    protected ScheduledCommand(boolean concurrent)
    {
        this.concurrent = concurrent;
    }

    @Override
    public final void run()
    {
        if (!concurrent)
        {
            if (running) return;
            synchronized (this)
            {
                if (running) return;
                running = true;
            }
        }

        try
        {
            try
            {
                doExecute();
            }
            catch (Throwable throwable)
            {
                doException(throwable);
            }
            finally
            {
                doFinally();
            }
        }
        catch (Throwable throwable)
        {
            LoggerFactory.getLogger(getClass()).error(throwable.toString(), throwable);
        }
        finally
        {
            if (!concurrent)
            {
                running = false;
            }
        }
    }

    protected void doExecute() throws Throwable
    {
    }

    protected void doException(Throwable throwable)
    {
        LoggerFactory.getLogger(getClass()).error(throwable.toString(), throwable);
    }

    protected void doFinally()
    {
    }
}
