package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.ExecutorConfig;
import io.qiufen.common.concurrency.executor.api.ExecutorServiceInfo;

public class DefaultExecutorServiceInfo implements ExecutorServiceInfo
{
    private final String id;
    private final DefaultExecutorService service;

    DefaultExecutorServiceInfo(String id, DefaultExecutorService service)
    {
        this.id = id;
        this.service = service;
    }

    @Override
    public String id()
    {
        return this.id;
    }

    @Override
    public ExecutorConfig config()
    {
        return service.config();
    }

    @Override
    public int concurrent()
    {
        return service.concurrent();
    }

    @Override
    public int queueDepth()
    {
        return service.queueDepth();
    }
}