package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.ExecutorServiceInfo;
import io.qiufen.common.concurrency.executor.api.Watcher;
import io.qiufen.common.concurrency.executor.spi.Predictable;
import io.qiufen.common.concurrency.executor.spi.Tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class DefaultTester implements Tester
{
    private static final Logger log = LoggerFactory.getLogger(DefaultTester.class);

    @Override
    public boolean testNeedExpand(Watcher watcher)
    {
        int concurrent = watcher.concurrent();
        int currentThreads = watcher.countWorkers();
        //存在系统线程空闲，系统线程数大于并发量，无需扩容
        if (currentThreads > concurrent)
        {
            return false;
        }
        //系统线程均被使用
        for (Map.Entry<String, ExecutorServiceInfo> entry : watcher.listExecutorServiceInfo().entrySet())
        {
            ExecutorServiceInfo info = entry.getValue();
            int concurrent0 = info.concurrent(), queueDepth = info.queueDepth();
            //用户线程池并发量 小于 最低并发量
            if (concurrent0 < info.config().getMinConcurrent())
            {
                //且有任务排队，需要扩容
                if (queueDepth > 0)
                {
                    return true;
                }
                continue;
            }
            //用户线程并发量 小于 最大并发量
            if (concurrent0 < info.config().getMaxConcurrent())
            {
                //且超过最大队列深度，需要扩容
                if (queueDepth > info.config().getMaxQueueDepth())
                {
                    return true;
                }
            }
            //用户线程并发量 达到 最大并发量，无需扩容
        }
        return false;
    }

    @Override
    public boolean testPredictableCommandTimeout(Watcher watcher, Predictable command, long timestamp)
    {
        return timestamp + command.maxWait() <= System.currentTimeMillis();
    }

    @Override
    public boolean testWorking(Watcher watcher)
    {
        for (Map.Entry<String, ExecutorServiceInfo> entry : watcher.listExecutorServiceInfo().entrySet())
        {
            if (entry.getValue().queueDepth() > 0)
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean testBusyMode(Watcher watcher)
    {
        boolean busyMode = watcher.concurrent() == watcher.poolConfig().getMaxThreads();
        log.info("Test busy mode:{}", busyMode);
        return busyMode;
    }
}
