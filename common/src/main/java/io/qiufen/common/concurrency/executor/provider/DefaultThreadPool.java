package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.PoolConfig;
import io.qiufen.common.concurrency.executor.spi.CommandPolling;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class DefaultThreadPool implements SystemThreadPool
{
    private final PoolConfig config;
    private final CommandPolling polling;
    private final SystemRecyclePool recyclePool;

    private final Map<Thread, Worker> threads;

    private final AtomicInteger workerCount;
    private final Queue<Worker> workers;

    private volatile boolean working = false;

    public DefaultThreadPool(PoolConfig config, CommandPolling polling, SystemRecyclePool recyclePool)
    {
        this.config = config;
        this.polling = polling;
        this.recyclePool = recyclePool;

        this.threads = new ConcurrentHashMap<Thread, Worker>();
        this.workerCount = new AtomicInteger(0);
        this.workers = new ConcurrentLinkedQueue<Worker>();

        ((DefaultRecyclePool) this.recyclePool).setThreads(threads);

        for (int i = 0; i < config.getMinThreads(); i++)
        {
            Worker worker = new Worker(polling, false);
            this.threads.put(worker.getThread(), worker);
            this.workers.offer(worker);
        }
        this.workerCount.set(config.getMinThreads());
    }


    @Override
    public int threads()
    {
        return this.workerCount.get();
    }

    @Override
    public List<Thread> getThreads()
    {
        List<Thread> threads = new ArrayList<Thread>();
        for (Worker worker : workers)
        {
            threads.add(worker.getThread());
        }
        return threads;
    }

    @Override
    public void tryExpand()
    {
        if (workers.size() >= config.getMaxThreads())
        {
            return;
        }
        synchronized (this)
        {
            if (workers.size() >= config.getMaxThreads())
            {
                return;
            }
            Thread thread = recyclePool.recover();
            if (thread == null)
            {
                Worker worker = new Worker(polling, working);
                this.threads.put(worker.getThread(), worker);
                this.workers.offer(worker);
            }
            else
            {
                Worker worker = threads.get(thread);
                worker.setWorking(this.working);
                this.workers.offer(worker);
            }
            this.workerCount.incrementAndGet();
        }
    }

    @Override
    public void tryShrink()
    {
        if (workers.size() <= config.getMinThreads())
        {
            return;
        }
        synchronized (this)
        {
            if (workers.size() <= config.getMinThreads())
            {
                return;
            }
            Worker worker = this.workers.poll();
            if (worker == null)
            {
                return;
            }
            this.workerCount.decrementAndGet();
            recyclePool.put(worker.getThread());
            worker.setWorking(false);
        }
    }

    @Override
    public void close()
    {
        for (Worker worker = workers.poll(); worker != null; worker = workers.poll())
        {
            this.workerCount.decrementAndGet();
            threads.remove(worker.getThread());
            worker.close();
        }
    }

    @Override
    public void setWorking(boolean working)
    {
        if (this.working == working)
        {
            return;
        }
        this.working = working;
        for (Worker worker : workers)
        {
            worker.setWorking(working);
        }
    }
}

