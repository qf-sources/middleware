package io.qiufen.common.concurrency.executor.provider;

public class Recycle
{
    private final Thread thread;
    private final long timestamp;

    public Recycle(Thread thread)
    {
        this.thread = thread;
        this.timestamp = System.currentTimeMillis();
    }

    public Thread getThread()
    {
        return thread;
    }

    public long getTimestamp()
    {
        return timestamp;
    }
}
