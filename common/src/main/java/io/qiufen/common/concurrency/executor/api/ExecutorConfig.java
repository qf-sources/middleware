package io.qiufen.common.concurrency.executor.api;

import io.qiufen.common.concurrency.executor.spi.PredictionHandler;
import io.qiufen.common.concurrency.executor.spi.RejectionPolicy;

/**
 * 用户线程池配置
 */
public class ExecutorConfig
{
    /**
     * 最低并发量
     */
    private int minConcurrent;

    /**
     * 最大并发量
     */
    private int maxConcurrent;

    /**
     * 队列最大深度
     */
    private int maxQueueDepth;

    /**
     * 排斥策略
     */
    private RejectionPolicy rejectionPolicy;

    /**
     * 可预测的任务回调处理器
     */
    private PredictionHandler predictionHandler;

    /**
     * 繁忙模式下，用户线程池执行优先级
     */
    private BusyPriority busyPriority;

    public int getMinConcurrent()
    {
        return minConcurrent;
    }

    public void setMinConcurrent(int minConcurrent)
    {
        this.minConcurrent = minConcurrent;
    }

    public int getMaxConcurrent()
    {
        return maxConcurrent;
    }

    public void setMaxConcurrent(int maxConcurrent)
    {
        this.maxConcurrent = maxConcurrent;
    }

    public int getMaxQueueDepth()
    {
        return maxQueueDepth;
    }

    public void setMaxQueueDepth(int maxQueueDepth)
    {
        this.maxQueueDepth = maxQueueDepth;
    }

    public RejectionPolicy getRejectionPolicy()
    {
        return rejectionPolicy;
    }

    public void setRejectionPolicy(RejectionPolicy rejectionPolicy)
    {
        this.rejectionPolicy = rejectionPolicy;
    }

    public PredictionHandler getPredictionHandler()
    {
        return predictionHandler;
    }

    public void setPredictionHandler(PredictionHandler predictionHandler)
    {
        this.predictionHandler = predictionHandler;
    }

    public BusyPriority getBusyPriority()
    {
        return busyPriority;
    }

    public void setBusyPriority(BusyPriority busyPriority)
    {
        this.busyPriority = busyPriority;
    }
}
