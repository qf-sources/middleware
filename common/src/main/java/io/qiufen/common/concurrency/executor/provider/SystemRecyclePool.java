package io.qiufen.common.concurrency.executor.provider;

import java.util.List;

/**
 * 系统层线程回收池
 */
public interface SystemRecyclePool
{
    /**
     * 当前待回收线程数
     */
    int threads();

    /**
     * 当前待回收线程
     */
    List<Recycle> getThreads();

    /**
     * 放入系统线程，待释放
     */
    void put(Thread thread);

    /**
     * 移出系统线程，恢复正常
     */
    Thread recover();

    /**
     * 尝试释放线程资源
     */
    void tryRelease();

    /**
     * 关闭回收池
     */
    void close();
}
