package io.qiufen.common.concurrency.executor.api;

import java.util.List;
import java.util.Map;

/**
 * 监视器
 */
public interface Watcher
{
    /**
     * 系统线程池配置
     */
    PoolConfig poolConfig();

    /**
     * 工作线程数量
     */
    int countWorkers();

    /**
     * 待回收线程数量
     */
    int countRecycles();

    /**
     * 工作线程数量
     */
    List<ThreadInfo> workers();

    /**
     * 待回收线程数量
     */
    List<ThreadRecycleInfo> recycles();

    /**
     * 并发量
     */
    int concurrent();

    /**
     * 并发量
     *
     * @param id 线程池ID
     */
    int concurrent(String id);

    /**
     * 用户线程池队列深度
     */
    int queueDepth();

    /**
     * 用户线程池队列深度
     *
     * @param id 线程池ID
     */
    int queueDepth(String id);

    /**
     * 是否繁忙模式
     */
    boolean isBusyMode();

    /**
     * 所有用户线程池信息
     *
     * @return 用户线程池信息
     */
    Map<String, ExecutorServiceInfo> listExecutorServiceInfo();
}
