package io.qiufen.common.concurrency.dispatcher;

import io.qiufen.common.concurrency.executor.spi.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executor;
import java.util.concurrent.Semaphore;

class DispatcherCommand<T> implements Runnable
{
    private static final Logger log = LoggerFactory.getLogger(DispatcherCommand.class);

    private final Semaphore semaphore = new Semaphore(0);

    private final DispatcherQueue<T> dispatcher;
    private final Executor executor;
    private final Operation<T> operation;

    private volatile boolean closed = false;

    DispatcherCommand(DispatcherQueue<T> dispatcher, Executor executor, Operation<T> operation)
    {
        this.dispatcher = dispatcher;
        this.executor = executor;
        this.operation = operation;
    }

    void incrementSemaphore()
    {
        this.semaphore.release(1);
    }

    @Override
    public void run()
    {
        for (; ; )
        {
            if (closed)
            {
                clear();
                return;
            }
            else
            {
                try
                {
                    semaphore.acquire(1);
                    T obj = dispatcher.poll();
                    if (obj == null) continue;
                    dispatch(obj);
                }
                catch (Exception e)
                {
                    log.error(e.toString(), e);
                }
            }
        }
    }

    private void clear()
    {
        while (true)
        {
            try
            {
                T obj = dispatcher.poll();
                if (obj == null) return;
                dispatch(obj);
            }
            catch (Exception e)
            {
                log.error(e.toString(), e);
            }
        }
    }

    private void dispatch(T obj)
    {
        try
        {
            executor.execute(createCommand(obj));
        }
        catch (Exception e)
        {
            doDispatchFailure(obj, e);
            return;
        }
        doDispatchSuccess(obj);
    }

    Command createCommand(final T obj)
    {
        return new Command()
        {
            @Override
            protected void doExecute() throws Throwable
            {
                operation.operate(obj);
            }

            @Override
            protected void doFinally()
            {
                doCommandCompleted(obj);
            }
        };
    }

    void close()
    {
        this.closed = true;
    }

    private void doDispatchSuccess(T obj)
    {
        try
        {
            onDispatchSuccess(obj);
        }
        catch (Exception e)
        {
            log.error("", e);
        }
    }

    private void doDispatchFailure(T obj, Exception exception)
    {
        try
        {
            onDispatchFailure(obj, exception);
        }
        catch (Exception e)
        {
            log.error("", e);
        }
    }

    private void doCommandCompleted(T obj)
    {
        try
        {
            onCommandCompleted(obj);
        }
        catch (Exception e)
        {
            log.error("", e);
        }
    }

    protected void onDispatchSuccess(T obj) throws Exception
    {
    }

    protected void onDispatchFailure(T obj, Exception exception) throws Exception
    {
        log.error("", exception);
    }

    protected void onCommandCompleted(T obj) throws Exception
    {
    }
}
