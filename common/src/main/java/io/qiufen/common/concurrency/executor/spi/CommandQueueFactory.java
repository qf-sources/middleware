package io.qiufen.common.concurrency.executor.spi;

public interface CommandQueueFactory
{
    CommandQueue create();
}
