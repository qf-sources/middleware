package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.PoolConfig;

import java.util.*;

public class DefaultRecyclePool implements SystemRecyclePool
{
    private final PoolConfig config;

    private final Deque<Recycle> recycles;

    private Map<Thread, Worker> threads;

    public DefaultRecyclePool(PoolConfig config)
    {
        this.config = config;

        this.recycles = new LinkedList<Recycle>();
    }

    @Override
    public void put(Thread thread)
    {
        Worker worker = this.threads.get(thread);
        if (worker == null)
        {
            return;
        }
        worker.suspend();
        synchronized (this.recycles)
        {
            this.recycles.offer(new Recycle(thread));
        }
    }

    @Override
    public Thread recover()
    {
        Recycle recycle;
        synchronized (this.recycles)
        {
            recycle = recycles.pollLast();
        }
        if (recycle == null)
        {
            return null;
        }
        Thread thread = recycle.getThread();
        Worker worker = this.threads.get(thread);
        if (worker != null)
        {
            worker.resume();
        }
        return thread;
    }

    @Override
    public void tryRelease()
    {
        synchronized (this.recycles)
        {
            Iterator<Recycle> iterator = recycles.iterator();
            while (iterator.hasNext())
            {
                Recycle recycle = iterator.next();
                if (recycle.getTimestamp() + config.getIdleDuration() > System.currentTimeMillis())
                {
                    continue;
                }
                Worker worker = this.threads.remove(recycle.getThread());
                if (worker != null)
                {
                    worker.close();
                }
                iterator.remove();
            }
        }
    }

    @Override
    public void close()
    {
        synchronized (this.recycles)
        {
            for (Recycle recycle; (recycle = recycles.poll()) != null; )
            {
                Worker worker = threads.remove(recycle.getThread());
                if (worker != null)
                {
                    worker.close();
                }
            }
        }
    }

    @Override
    public int threads()
    {
        return this.recycles.size();
    }

    @Override
    public List<Recycle> getThreads()
    {
        return new ArrayList<Recycle>(this.recycles);
    }

    void setThreads(Map<Thread, Worker> threads)
    {
        this.threads = threads;
    }
}

