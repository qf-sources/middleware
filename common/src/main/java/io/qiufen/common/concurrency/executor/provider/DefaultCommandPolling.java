package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.BusyPriority;
import io.qiufen.common.concurrency.executor.spi.CommandPolling;
import io.qiufen.common.concurrency.executor.spi.CommandQueue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

class DefaultCommandPolling implements CommandPolling
{
    private final List<CommandQueue> normalList = new ArrayList<CommandQueue>();
    private final List<CommandQueue> busyList = new ArrayList<CommandQueue>();

    private volatile List<CommandQueue> pollingList;

    private volatile boolean busyMode = false;

    DefaultCommandPolling()
    {
        this.pollingList = normalList;
    }

    @Override
    public void addCommandQueue(CommandQueue commandQueue)
    {
        this.normalList.add(commandQueue);
        arrange();
    }

    @Override
    public Runnable poll(int sequence)
    {
        int len = pollingList.size();
        if (len <= 0)
        {
            return null;
        }

        int mod = sequence % len;
        if (mod < 0) mod = -mod;
        return pollingList.get(mod).poll();
    }

    @Override
    public boolean isBusyMode()
    {
        return busyMode;
    }

    @Override
    public void setBusyMode(boolean busyMode)
    {
        this.busyMode = busyMode;
        this.pollingList = busyMode ? busyList : normalList;
    }

    private void arrange()
    {
        List<CommandQueue> list = new ArrayList<CommandQueue>();
        for (CommandQueue commandQueue : this.normalList)
        {
            BusyPriority priority = commandQueue.config().getBusyPriority();
            if (priority == null)
            {
                list.add(commandQueue);
                continue;
            }

            //填补
            for (int i = 0; i < calcWeight(priority); i++)
            {
                list.add(commandQueue);
            }
        }

        SecureRandom random = new SecureRandom();
        this.busyList.clear();//先清空
        //再随机混淆添加
        for (CommandQueue commandQueue : list)
        {
            if (this.busyList.isEmpty())
            {
                this.busyList.add(commandQueue);
                continue;
            }
            this.busyList.add(random.nextInt(this.busyList.size()), commandQueue);
        }
        list.clear();
    }

    private int calcWeight(BusyPriority priority)
    {
        int total = this.normalList.size() - 1; //总数，不包含当前
        int o = priority.ordinal();
        int f = BusyPriority.values().length;
        int weight;
        if (total < f)
        {
            weight = o + 1;
        }
        else
        {
            // ( total * ( o + 1 ) ) / f，四舍五入
            weight = new BigDecimal(total).multiply(new BigDecimal(o + 1))
                    .divide(new BigDecimal(f), 0, RoundingMode.HALF_UP)
                    .intValue();
        }
        return weight;
    }
}
