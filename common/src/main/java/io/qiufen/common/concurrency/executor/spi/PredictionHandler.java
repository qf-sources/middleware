package io.qiufen.common.concurrency.executor.spi;

public interface PredictionHandler
{
    void onTimeout(Predictable command);
}
