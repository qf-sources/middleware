package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.ExecutorServiceFactory;
import io.qiufen.common.concurrency.executor.api.ExecutorServiceFactoryBuilder;
import io.qiufen.common.concurrency.executor.api.PoolConfig;
import io.qiufen.common.concurrency.executor.spi.CommandPollingFactory;
import io.qiufen.common.concurrency.executor.spi.CommandQueueFactory;
import io.qiufen.common.lang.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultExecutorServiceFactoryBuilder implements ExecutorServiceFactoryBuilder
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultExecutorServiceFactoryBuilder.class);

    private PoolConfig poolConfig;
    private CommandPollingFactory commandPollingFactory;
    private CommandQueueFactory commandQueueFactory;

    @Override
    public ExecutorServiceFactoryBuilder setPoolConfig(PoolConfig config)
    {
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Use pool config:{}", JsonObject.toJsonString(config));
        }
        this.poolConfig = config;
        return this;
    }

    @Override
    public ExecutorServiceFactoryBuilder setCommandPollingFactory(CommandPollingFactory factory)
    {
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Use command polling factory:{}", factory.getClass());
        }
        this.commandPollingFactory = factory;
        return this;
    }

    @Override
    public ExecutorServiceFactoryBuilder setCommandQueueFactory(CommandQueueFactory factory)
    {
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Use command queue factory:{}", factory.getClass());
        }
        this.commandQueueFactory = factory;
        return this;
    }

    @Override
    public ExecutorServiceFactory build()
    {
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Build executor service factory...");
        }
        if (this.commandPollingFactory == null) this.commandPollingFactory = DefaultCommandPollingFactory.getInstance();
        //        if (this.commandQueueFactory == null) this.commandQueueFactory = DefaultCommandPollingFactory.getInstance();
        return new DefaultExecutorServiceFactory(this.poolConfig, this.commandPollingFactory, this.commandQueueFactory);
    }
}
