package io.qiufen.common.concurrency.dispatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executor;

public abstract class DispatcherQueue<T>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DispatcherQueue.class);

    private final Executor executor;
    private final Operation<T> operation;
    private final boolean isFair;

    private DispatcherCommand<T> dispatcherCommand;

    private volatile boolean closed = false;

    protected DispatcherQueue(Executor executor, Operation<T> operation)
    {
        this(executor, operation, true);
    }

    protected DispatcherQueue(Executor executor, Operation<T> operation, boolean isFair)
    {
        this.executor = executor;
        this.operation = operation;
        this.isFair = isFair;

        init();
    }

    public boolean offer(T obj)
    {
        if (closed) throw new IllegalStateException("Buffered queue is closed.");

        if (!isFair && dispatchDirect(obj))
        {
            doQueued(obj);
            return true;
        }

        boolean result = offer0(obj);
        //提供成功之后，增加计数，唤醒分发器任务
        if (result)
        {
            dispatcherCommand.incrementSemaphore();
            doQueued(obj);
        }
        return result;
    }

    public void close()
    {
        this.closed = true;
        dispatcherCommand.close();
    }

    T poll()
    {
        return poll0();
    }

    /**
     * 初始化缓冲分发器
     */
    private void init()
    {
        dispatcherCommand = new DispatcherCommand<T>(this, executor, operation)
        {
            @Override
            protected void onDispatchSuccess(T obj) throws Exception
            {
                onDispatched(obj);
            }

            @Override
            protected void onDispatchFailure(T obj, Exception exception) throws Exception
            {
                super.onDispatchFailure(obj, exception);
                offerTail(obj);
            }

            @Override
            protected void onCommandCompleted(T obj) throws Exception
            {
                DispatcherQueue.this.onCommandCompleted(obj);
            }
        };
        Thread thread = new Thread(dispatcherCommand);
        thread.setName("Dispatcher-" + DispatcherQueue.this.getClass().getSimpleName());
        thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();
    }

    private boolean dispatchDirect(T obj)
    {
        try
        {
            executor.execute(dispatcherCommand.createCommand(obj));
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    private void offerTail(T obj)
    {
        offerTail0(obj);
        dispatcherCommand.incrementSemaphore();
    }

    private void doQueued(T obj)
    {
        try
        {
            onQueued(obj);
        }
        catch (Exception e)
        {
            LOGGER.error("", e);
        }
    }

    protected void onQueued(T obj) throws Exception
    {
    }

    protected void onDispatched(T obj) throws Exception
    {
    }

    protected void onCommandCompleted(T obj) throws Exception
    {
    }

    protected abstract boolean offer0(T t);

    protected abstract void offerTail0(T t);

    protected abstract T poll0();
}
