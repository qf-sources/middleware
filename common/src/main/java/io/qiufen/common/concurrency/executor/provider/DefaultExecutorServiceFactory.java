package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.*;
import io.qiufen.common.concurrency.executor.spi.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class DefaultExecutorServiceFactory implements ExecutorServiceFactory
{
    private final Map<String, DefaultExecutorService> services;
    private final List<DefaultExecutorService> normalList;

    private final Map<String, ExecutorServiceInfo> infoList;

    private final PoolConfig poolConfig;
    private final CommandQueueFactory commandQueueFactory;

    private final CommandPolling commandPolling;
    private final Tester tester;
    private final Watcher watcher;

    DefaultExecutorServiceFactory(PoolConfig poolConfig, CommandPollingFactory commandPollingFactory,
            CommandQueueFactory commandQueueFactory)
    {
        this.poolConfig = poolConfig;
        this.commandQueueFactory = commandQueueFactory;

        this.services = new HashMap<String, DefaultExecutorService>();
        this.normalList = new ArrayList<DefaultExecutorService>();
        this.infoList = new HashMap<String, ExecutorServiceInfo>();

        this.tester = provideTester();
        this.commandPolling = commandPollingFactory.create();
        SystemRecyclePool recyclePool = provideRecyclePool(this.poolConfig);
        SystemThreadPool threadPool = provideThreadPool(this.poolConfig, this.commandPolling, recyclePool);
        this.watcher = provideWatcher(threadPool, recyclePool);
    }

    @Override
    public ExecutorService initService(String id, ExecutorConfig config)
    {
        if (this.services.containsKey(id))
        {
            throw new RuntimeException("User executor service existed already, id:" + id);
        }
        DefaultExecutorService service = new DefaultExecutorService(config)
        {
            @Override
            protected boolean testPredictableCommandTimeout(Predictable command, long timestamp)
            {
                return tester.testPredictableCommandTimeout(watcher, command, timestamp);
            }
        };
        this.services.put(id, service);
        this.commandPolling.addCommandQueue(service.getCommandQueue());
        this.normalList.add(service);
        this.infoList.put(id, new DefaultExecutorServiceInfo(id, service));
        return service;
    }

    @Override
    public ExecutorService getService(String id)
    {
        return this.getService0(id);
    }

    @Override
    public Watcher watcher()
    {
        return this.watcher;
    }

    @Override
    public void close()
    {
        //todo
    }

    private DefaultExecutorService getService0(String id)
    {
        DefaultExecutorService service = services.get(id);
        if (service == null)
        {
            throw new RuntimeException("not found executor service,id:" + id);
        }
        return service;
    }

    protected Tester provideTester()
    {
        return new DefaultTester();
    }

    protected SystemRecyclePool provideRecyclePool(PoolConfig poolConfig)
    {
        return new DefaultRecyclePool(poolConfig);
    }

    protected SystemThreadPool provideThreadPool(PoolConfig poolConfig, CommandPolling commandPolling,
            SystemRecyclePool recyclePool)
    {
        return new DefaultThreadPool(poolConfig, commandPolling, recyclePool);
    }

    protected Watcher provideWatcher(SystemThreadPool threadPool, SystemRecyclePool recyclePool)
    {
        return new UserDefaultWatcher(threadPool, recyclePool);
    }

    private class UserDefaultWatcher extends DefaultWatcher
    {
        private UserDefaultWatcher(SystemThreadPool threadPool, SystemRecyclePool recyclePool)
        {
            super(poolConfig, threadPool, recyclePool, commandPolling, tester);
        }

        @Override
        public Map<String, ExecutorServiceInfo> listExecutorServiceInfo()
        {
            return new HashMap<String, ExecutorServiceInfo>(infoList);
        }

        @Override
        protected void testPredictableCommandTimeout()
        {
            for (DefaultExecutorService service : normalList)
            {
                service.checkPredictableCommand();
            }
        }
    }
}
