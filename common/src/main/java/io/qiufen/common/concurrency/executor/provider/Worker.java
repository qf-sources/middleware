package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.spi.CommandPolling;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;

class Worker implements Runnable
{
    private static final Logger log = LoggerFactory.getLogger(Worker.class);

    private static final int NORMAL = 1;
    private static final int SUSPEND = 2;
    private static final int CLOSED = 3;

    private static final AtomicInteger INDEX = new AtomicInteger(0);

    private final CommandPolling polling;

    private final Thread thread;

    private volatile int state = NORMAL;

    private volatile boolean working;

    Worker(CommandPolling polling, boolean working)
    {
        this.polling = polling;
        this.working = working;

        this.thread = new Thread(this);
        this.thread.setName("system-executor-" + INDEX.getAndIncrement());
        this.thread.start();
    }

    Thread getThread()
    {
        return thread;
    }

    void suspend()
    {
        if (this.state == CLOSED)
        {
            throw new IllegalStateException("Thread has closed.");
        }
        this.state = SUSPEND;
    }

    void resume()
    {
        if (this.state == CLOSED)
        {
            throw new IllegalStateException("Thread has closed.");
        }
        this.state = NORMAL;
        LockSupport.unpark(this.thread);
    }

    void close()
    {
        this.state = CLOSED;
        LockSupport.unpark(this.thread);
    }

    @Override
    public final void run()
    {
        int sequence = 0, failureTimes = 0;
        for (; ; sequence++)
        {
            try
            {
                switch (this.state)
                {
                    case NORMAL:
                        Runnable runnable = this.polling.poll(sequence);
                        if (runnable == null)
                        {
                            if (failureTimes < 20000)
                            {
                                failureTimes++;
                            }
                            else
                            {
                                failureTimes = 20000;
                            }
                            await(failureTimes);
                            continue;
                        }
                        failureTimes = 0;
                        runnable.run();
                        break;
                    case SUSPEND:
                        LockSupport.park(this);
                        break;
                    case CLOSED:
                        return;
                    default:
                }
            }
            catch (Throwable throwable)
            {
                log.error(throwable.toString(), throwable);
            }
        }
    }

    private void await(int failureTimes)
    {
        if (this.working)
        {
            if (failureTimes <= 10000)
            {
                Thread.yield();
            }
            else
            {
                LockSupport.parkNanos(1);
            }
        }
        else
        {
            LockSupport.park();
        }
    }

    void setWorking(boolean working)
    {
        if (this.working == working)
        {
            return;
        }
        this.working = working;
        if (working)
        {
            LockSupport.unpark(thread);
        }
    }
}
