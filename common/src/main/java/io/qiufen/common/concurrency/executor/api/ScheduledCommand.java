package io.qiufen.common.concurrency.executor.api;

/**
 * Created by 善为之 on 2019/3/2.
 */
@Deprecated
public abstract class ScheduledCommand extends io.qiufen.common.concurrency.executor.spi.ScheduledCommand
{
    protected ScheduledCommand(boolean concurrent)
    {
        super(concurrent);
    }
}
