package io.qiufen.common.concurrency.executor.api;

/**
 * 用户线程池信息
 */
public interface ExecutorServiceInfo
{
    String id();

    /**
     * 用户线程池配置
     */
    ExecutorConfig config();

    /**
     * 并发量
     */
    int concurrent();

    /**
     * 用户线程池队列深度
     */
    int queueDepth();
}
