package io.qiufen.common.concurrency.dispatcher;

public interface Operation<T>
{
    void operate(T t) throws Exception;
}
