package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.ExecutorConfig;
import io.qiufen.common.concurrency.executor.spi.Predictable;
import io.qiufen.common.concurrency.executor.spi.RejectionPolicy;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public abstract class DefaultExecutorService extends AbstractExecutorService
{
    private final DefaultExecutorState executorState;
    private final DefaultCommandQueue commandQueue;

    DefaultExecutorService(ExecutorConfig config)
    {
        this.executorState = new DefaultExecutorState(config);
        this.commandQueue = new DefaultCommandQueue(config, this.executorState)
        {
            @Override
            protected boolean testPredictableCommandTimeout(Predictable predictable, long timestamp)
            {
                return DefaultExecutorService.this.testPredictableCommandTimeout(predictable, timestamp);
            }
        };
    }

    @Override
    public void shutdown()
    {
        executorState.setState(State.CLOSING);
    }

    @Override
    public List<Runnable> shutdownNow()
    {
        executorState.setState(State.CLOSED);
        return new ArrayList<Runnable>(this.commandQueue.getQueue());
    }

    @Override
    public boolean isShutdown()
    {
        return executorState.getState() == State.CLOSED;
    }

    @Override
    public boolean isTerminated()
    {
        return isShutdown() && concurrent() == 0;
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit)
    {
        LockSupport.parkNanos(unit.toNanos(timeout));
        return isTerminated();
    }

    ExecutorConfig config()
    {
        return this.executorState.config();
    }

    int concurrent()
    {
        return executorState.concurrent();
    }

    int queueDepth()
    {
        return commandQueue.queueDepth();
    }

    @Override
    public void execute(Runnable command)
    {
        if (executorState.getState() != State.NORMAL)
        {
            throw new IllegalStateException("User executor service is not in normal.");
        }
        if (!this.commandQueue.offer(command))
        {
            //排斥策略
            RejectionPolicy policy = this.executorState.config().getRejectionPolicy();
            if (policy == null)
            {
                policy = RejectionPolicies.THROWING;
            }
            policy.onReject(command, this.commandQueue);
        }
    }

    void checkPredictableCommand()
    {
        this.commandQueue.checkPredictableCommand();
    }

    protected abstract boolean testPredictableCommandTimeout(Predictable command, long timestamp);

    DefaultCommandQueue getCommandQueue()
    {
        return commandQueue;
    }
}