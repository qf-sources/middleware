package io.qiufen.common.concurrency.executor.spi;

/**
 * 可预测的任务
 */
public interface Predictable extends Runnable
{
    /**
     * 最大等待执行时长
     */
    int maxWait();
}
