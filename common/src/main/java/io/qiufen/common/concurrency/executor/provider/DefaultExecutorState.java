package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.ExecutorConfig;

import java.util.concurrent.atomic.AtomicInteger;

enum State
{
    NORMAL,
    CLOSING,
    CLOSED
}

public class DefaultExecutorState
{
    private final ExecutorConfig config;

    private final AtomicInteger concurrent;

    private volatile State state = State.NORMAL;

    DefaultExecutorState(ExecutorConfig config)
    {
        this.config = config;

        this.concurrent = new AtomicInteger(0);
    }

    State getState()
    {
        return this.state;
    }

    void setState(State state)
    {
        this.state = state;
    }

    void addConcurrent(int nums)
    {
        this.concurrent.addAndGet(nums);
    }

    ExecutorConfig config()
    {
        return config;
    }

    int concurrent()
    {
        return concurrent.get();
    }
}