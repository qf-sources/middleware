package io.qiufen.common.concurrency.executor.api;

/**
 * Created by 善为之 on 2019/3/2.
 * instead of io.qiufen.common.concurrency.executor.spi.Command
 */
@Deprecated
public abstract class Command extends io.qiufen.common.concurrency.executor.spi.Command {}
