package io.qiufen.common.concurrency.executor.api;

import java.util.concurrent.ExecutorService;

/**
 * 线程池context
 */
public interface ExecutorServiceFactory
{
    /**
     * 初始化用户线程池
     *
     * @param id     ID
     * @param config 配置
     * @return 用户线程池
     */
    ExecutorService initService(String id, ExecutorConfig config);

    /**
     * 获取用户线程池
     *
     * @param id ID
     * @return 用户线程池
     */
    ExecutorService getService(String id);

    /**
     * 监视器
     */
    Watcher watcher();

    void close();
}
