package io.qiufen.common.concurrency.executor.api;

/**
 * 繁忙模式下优先级
 */
public enum BusyPriority
{
    /**
     * 最低
     */
    P0,

    P1,
    P2,
    P3,
    P4,
    P5,
    P6,
    P7,
    P8,

    /**
     * 最高
     */
    P9
}
