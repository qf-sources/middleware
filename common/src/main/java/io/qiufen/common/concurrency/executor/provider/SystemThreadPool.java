package io.qiufen.common.concurrency.executor.provider;

import java.util.List;

/**
 * 系统层线程池
 */
public interface SystemThreadPool
{
    /**
     * 当前线程数量
     */
    int threads();

    /**
     * 获取当前线程
     */
    List<Thread> getThreads();

    void setWorking(boolean working);

    /**
     * 尝试扩容线程
     */
    void tryExpand();

    /**
     * 尝试缩减线程
     */
    void tryShrink();

    /**
     * 关闭线程池
     */
    void close();
}
