package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.ExecutorConfig;
import io.qiufen.common.concurrency.executor.spi.CommandQueue;
import io.qiufen.common.concurrency.executor.spi.Predictable;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class DefaultCommandQueue implements CommandQueue
{
    private final ExecutorConfig config;

    private final DefaultExecutorState userExecutorState;

    private final AtomicBoolean pollingLock;

    private final AtomicInteger queueDepth;
    private final Queue<Runnable> queue;

    private volatile Queue<PredictableEntry> predictableQueue;

    DefaultCommandQueue(ExecutorConfig config, DefaultExecutorState userExecutorState)
    {
        this.config = config;
        this.userExecutorState = userExecutorState;

        this.pollingLock = new AtomicBoolean(false);

        this.queueDepth = new AtomicInteger(0);
        this.queue = new ConcurrentLinkedQueue<Runnable>();
    }

    Queue<Runnable> getQueue()
    {
        return this.queue;
    }

    int queueDepth()
    {
        return queueDepth.get();
    }

    public Runnable poll()
    {
        if (!this.pollingLock.compareAndSet(false, true))
        {
            return null;
        }
        Runnable runnable;
        try
        {
            runnable = poll1();
        }
        finally
        {
            this.pollingLock.set(false);
        }
        return runnable;
    }

    /**
     * 轮询取出任务
     * 并发操作的场景：由于并发数量会上下浮动，无需做到精确控制，仅需保证最终兜底并发量即可（无锁化）
     */
    private Runnable poll1()
    {
        //队列深度
        int queueDepth = this.queueDepth();
        //无元素
        if (queueDepth <= 0) return null;

        int concurrent = userExecutorState.concurrent();
        //未达到最小并发量，正常poll
        if (concurrent < this.config.getMinConcurrent())
        {
            return poll0();
        }

        //未达到最大并发量
        if (concurrent < this.config.getMaxConcurrent())
        {
            //不足最大队列深度，返回null
            if (queueDepth <= this.config.getMaxQueueDepth())
            {
                return null;
            }
            //达到最大队列深度，正常poll
            return poll0();
        }

        //达到最大并发量
        return null;
    }

    @Override
    public boolean offer(Runnable command)
    {
        //排斥策略
        if (this.queueDepth() >= this.config.getMaxConcurrent() - userExecutorState.concurrent() + this.config.getMaxQueueDepth())
        {
            return false;
        }

        this.queueDepth.incrementAndGet();
        this.queue.offer(command);

        if (command instanceof Predictable)
        {
            this.usePredictableQueue().offer(new PredictableEntry((Predictable) command));
        }
        return true;
    }

    private Runnable poll0()
    {
        //先检查最大并发量（兜底，防止超出边界）
        if (userExecutorState.concurrent() >= this.config.getMaxConcurrent())
        {
            return null;
        }

        final Runnable runnable = this.queue.poll();
        if (runnable == null)
        {
            return null;
        }
        this.queueDepth.decrementAndGet();
        if (userExecutorState.getState() == State.CLOSING && this.queueDepth() == 0)
        {
            userExecutorState.setState(State.CLOSED);
        }
        return new Runnable()
        {
            @Override
            public void run()
            {
                //并发量+1
                userExecutorState.addConcurrent(1);
                try
                {
                    runnable.run();
                }
                finally
                {
                    //并发量-1
                    userExecutorState.addConcurrent(-1);
                }
            }
        };
    }

    @Override
    public ExecutorConfig config()
    {
        return userExecutorState.config();
    }

    void checkPredictableCommand()
    {
        Iterator<PredictableEntry> iterator = usePredictableQueue().iterator();
        while (iterator.hasNext())
        {
            PredictableEntry entry = iterator.next();
            Predictable predictable = entry.getPredictable();
            if (!testPredictableCommandTimeout(predictable, entry.getTimestamp()))
            {
                continue;
            }
            if (entry.getTimestamp() + predictable.maxWait() > System.currentTimeMillis())
            {
                continue;
            }
            if (this.queue.remove(predictable))
            {
                this.queueDepth.decrementAndGet();
                if (this.config.getPredictionHandler() != null)
                {
                    this.config.getPredictionHandler().onTimeout(predictable);
                }
            }
            iterator.remove();
        }
        if (userExecutorState.getState() == State.CLOSING && this.queueDepth() == 0)
        {
            userExecutorState.setState(State.CLOSED);
        }
    }

    private Queue<PredictableEntry> usePredictableQueue()
    {
        if (this.predictableQueue != null) return this.predictableQueue;
        synchronized (this)
        {
            if (this.predictableQueue != null) return this.predictableQueue;
            this.predictableQueue = new ConcurrentLinkedQueue<PredictableEntry>();
        }
        return this.predictableQueue;
    }

    protected abstract boolean testPredictableCommandTimeout(Predictable predictable, long timestamp);

}

class PredictableEntry
{
    private final Predictable predictable;
    private final long timestamp;

    PredictableEntry(Predictable predictable)
    {
        this.predictable = predictable;
        this.timestamp = System.currentTimeMillis();
    }

    Predictable getPredictable()
    {
        return predictable;
    }

    long getTimestamp()
    {
        return timestamp;
    }
}
