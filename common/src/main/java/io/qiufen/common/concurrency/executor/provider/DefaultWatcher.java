package io.qiufen.common.concurrency.executor.provider;

import io.qiufen.common.concurrency.executor.api.*;
import io.qiufen.common.concurrency.executor.spi.CommandPolling;
import io.qiufen.common.concurrency.executor.spi.Tester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public abstract class DefaultWatcher implements Watcher
{
    private static final Logger log = LoggerFactory.getLogger(DefaultWatcher.class);

    private final PoolConfig poolConfig;
    private final SystemThreadPool threadPool;
    private final SystemRecyclePool recyclePool;
    private final CommandPolling commandPolling;
    private final Tester tester;
    private final int period = 50;
    private volatile boolean watching = true;
    private long shrinkCount = System.currentTimeMillis();
    private long busyModeCount = System.currentTimeMillis();

    public DefaultWatcher(PoolConfig poolConfig, SystemThreadPool threadPool, SystemRecyclePool recyclePool,
            CommandPolling commandPolling, Tester tester)
    {
        this.poolConfig = poolConfig;
        this.threadPool = threadPool;
        this.recyclePool = recyclePool;
        this.commandPolling = commandPolling;
        this.tester = tester;

        init();
    }

    @Override
    public PoolConfig poolConfig()
    {
        return poolConfig;
    }

    @Override
    public int countWorkers()
    {
        return threadPool.threads();
    }

    @Override
    public int countRecycles()
    {
        return recyclePool.threads();
    }

    @Override
    public List<ThreadInfo> workers()
    {
        List<Thread> threads = threadPool.getThreads();
        List<ThreadInfo> infoList = new ArrayList<ThreadInfo>();
        for (Thread thread : threads)
        {
            infoList.add(new ThreadInfo(thread.getName(), thread.getState()));
        }
        return infoList;
    }

    @Override
    public List<ThreadRecycleInfo> recycles()
    {
        List<Recycle> recycles = recyclePool.getThreads();
        List<ThreadRecycleInfo> infoList = new ArrayList<ThreadRecycleInfo>();
        for (Recycle recycle : recycles)
        {
            Thread thread = recycle.getThread();
            long lifeTime = recycle.getTimestamp() + poolConfig().getIdleDuration() - System.currentTimeMillis();
            if (lifeTime < 0)
            {
                lifeTime = 0;
            }
            infoList.add(new ThreadRecycleInfo(thread.getName(), thread.getState(), lifeTime));
        }
        return infoList;
    }

    @Override
    public int concurrent()
    {
        int concurrent = 0;
        for (Map.Entry<String, ExecutorServiceInfo> entry : listExecutorServiceInfo().entrySet())
        {
            concurrent += entry.getValue().concurrent();
        }
        return concurrent;
    }

    @Override
    public int queueDepth()
    {
        int queueDepth = 0;
        for (Map.Entry<String, ExecutorServiceInfo> entry : listExecutorServiceInfo().entrySet())
        {
            queueDepth += entry.getValue().queueDepth();
        }
        return queueDepth;
    }

    @Override
    public int concurrent(String id)
    {
        return listExecutorServiceInfo().get(id).concurrent();
    }

    @Override
    public int queueDepth(String id)
    {
        return listExecutorServiceInfo().get(id).queueDepth();
    }

    @Override
    public boolean isBusyMode()
    {
        return commandPolling.isBusyMode();
    }

    private void init()
    {
        new Thread()
        {
            @Override
            public void run()
            {
                setName("executor-watcher");


                while (watching)
                {
                    try
                    {
                        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(period));

                        testWorking();

                        log.info("Test predictable command...");
                        testPredictableCommandTimeout();

                        testNeedExpand();

                        log.info("Try release...");
                        recyclePool.tryRelease();

                        tryShrink();

                        testBusyMode();
                    }
                    catch (Throwable throwable)
                    {
                        log.error(throwable.toString(), throwable);
                    }
                }
            }
        }.start();
    }

    public void close()
    {
        this.watching = false;
    }

    private void testWorking()
    {
        log.info("Test working...");
        threadPool.setWorking(tester.testWorking(this));
    }

    private void testNeedExpand()
    {
        log.info("Test need expand...");
        if (tester.testNeedExpand(this))
        {
            log.info("Try expand...");
            threadPool.tryExpand();
        }
    }

    private void testBusyMode()
    {
        int busyMode = 5000;
        if (System.currentTimeMillis() - busyModeCount >= busyMode)
        {
            log.info("Test busy mode...");
            commandPolling.setBusyMode(tester.testBusyMode(this));
            busyModeCount = System.currentTimeMillis();
        }
    }

    private void tryShrink()
    {
        int shrink = 1000;
        if (System.currentTimeMillis() - shrinkCount >= shrink)
        {
            log.info("Try shrink...");
            threadPool.tryShrink();
            shrinkCount = System.currentTimeMillis();
        }
    }

    protected void testPredictableCommandTimeout()
    {
    }
}
