package io.qiufen.common.concurrency.executor.api;

/**
 * 系统线程池配置
 */
public class PoolConfig
{
    /**
     * 最低线程数量
     */
    private int minThreads;

    /**
     * 最多线程数量
     */
    private int maxThreads;

    /**
     * 多余线程的存活时间，单位ms
     */
    private int idleDuration;

    public int getMinThreads()
    {
        return minThreads;
    }

    public void setMinThreads(int minThreads)
    {
        this.minThreads = minThreads;
    }

    public int getMaxThreads()
    {
        return maxThreads;
    }

    public void setMaxThreads(int maxThreads)
    {
        this.maxThreads = maxThreads;
    }

    public int getIdleDuration()
    {
        return idleDuration;
    }

    public void setIdleDuration(int idleDuration)
    {
        this.idleDuration = idleDuration;
    }
}
