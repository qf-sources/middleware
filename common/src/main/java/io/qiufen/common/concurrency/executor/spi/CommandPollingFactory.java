package io.qiufen.common.concurrency.executor.spi;

public interface CommandPollingFactory
{
    CommandPolling create();
}
