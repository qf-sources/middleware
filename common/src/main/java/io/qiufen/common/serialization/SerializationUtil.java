package io.qiufen.common.serialization;


import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.pool.KryoFactory;
import com.esotericsoftware.kryo.pool.KryoPool;
import com.esotericsoftware.kryo.serializers.CompatibleFieldSerializer;
import io.qiufen.common.io.serializer.BitSetSerializer;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * 序列化工具
 * User: Administrator
 * Date: 16-4-6
 * Time: 上午11:27
 * To change this template use File | Settings | File Templates.
 */
public class SerializationUtil
{
    private static final KryoPool pool;

    static
    {
        KryoFactory factory = new KryoFactory()
        {
            @Override
            public Kryo create()
            {
                Kryo kryo = new Kryo();
                //---------- 注册基础序列化解析器 -----------
                kryo.register(ArrayList.class);
                kryo.register(LinkedList.class);
                kryo.register(HashSet.class);
                kryo.register(LinkedHashSet.class);
                kryo.register(HashMap.class);
                kryo.register(LinkedHashMap.class);
                kryo.register(Date.class);
                kryo.addDefaultSerializer(BitSet.class, new BitSetSerializer());
                kryo.setDefaultSerializer(CompatibleFieldSerializer.class);

                kryo.setReferences(false);
                kryo.setCopyReferences(false);
                kryo.setAutoReset(true);
                return kryo;
            }
        };
        pool = new KryoPool.Builder(factory).build();
    }

    /**
     * 序列化
     *
     * @param object 对象
     * @return 数据流
     */
    public static byte[] serialize(Object object)
    {
        if (null == object)
        {
            return new byte[0];
        }
        Kryo kryo = pool.borrow();
        try
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Output output = new Output(stream);
            kryo.writeClassAndObject(output, object);
            output.flush();
            output.close();
            return stream.toByteArray();
        }
        finally
        {
            pool.release(kryo);
        }
    }

    /**
     * 序列化
     *
     * @param object 对象
     * @return 数据流
     */
    public static byte[] serializeContent(Object object)
    {
        if (null == object)
        {
            return new byte[0];
        }
        Kryo kryo = pool.borrow();
        try
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Output output = new Output(stream);
            kryo.writeObject(output, object);
            output.flush();
            output.close();
            return stream.toByteArray();
        }
        finally
        {
            pool.release(kryo);
        }
    }

    /**
     * 反序列化
     *
     * @param bytes 数据流
     * @return 对象
     */
    public static <T> T deSerialize(byte[] bytes)
    {
        return deSerialize(bytes, 0, bytes.length);
    }

    /**
     * 反序列化
     *
     * @param bytes 数据流
     * @return 对象
     */
    public static <T> T deSerializeContent(byte[] bytes, Class<T> clazz)
    {
        return deSerializeContent(bytes, 0, bytes.length, clazz);
    }

    /**
     * 反序列化
     *
     * @param bytes  数据流
     * @param offset offset
     * @param length length
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static <T> T deSerialize(byte[] bytes, int offset, int length)
    {
        if (null == bytes || bytes.length <= 0)
        {
            return null;
        }
        Kryo kryo = pool.borrow();
        T obj = null;
        try
        {
            Input input = new Input(bytes, offset, length);
            obj = (T) kryo.readClassAndObject(input);
            input.close();
        }
        finally
        {
            pool.release(kryo);
        }
        return obj;
    }

    /**
     * 反序列化
     *
     * @param bytes  数据流
     * @param offset offset
     * @param length length
     * @param clazz  类型
     * @return 对象
     */
    public static <T> T deSerializeContent(byte[] bytes, int offset, int length, Class<T> clazz)
    {
        if (null == bytes || bytes.length <= 0)
        {
            return null;
        }
        Kryo kryo = pool.borrow();
        T obj = null;
        try
        {
            Input input = new Input(bytes, offset, length);
            obj = kryo.readObject(input, clazz);
            input.close();
        }
        finally
        {
            pool.release(kryo);
        }
        return obj;
    }
}
