package io.qiufen.common.serialization;

/**
 * 序列号支持
 */
public interface Serializable
{
    int id();
}
