package io.qiufen.common.io.resource;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/14 17:28
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface Resource {}
