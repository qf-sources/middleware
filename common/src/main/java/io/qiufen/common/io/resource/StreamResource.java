package io.qiufen.common.io.resource;

import java.io.IOException;
import java.io.InputStream;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/14 18:12
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface StreamResource extends Resource
{
    InputStream open() throws IOException;
}
