package io.qiufen.common.io;

import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/1/3 10:47
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class FileUtil
{
    public static void deleteFileQuietly(Object monitor, File file)
    {
        if (!file.exists())
        {
            return;
        }
        if (file.delete())
        {
            LoggerFactory.getLogger(monitor.getClass()).info("Delete file successfully, file:{}.", file.getPath());
        }
        else
        {
            LoggerFactory.getLogger(monitor.getClass()).error("Delete file failure, file:{}.", file.getPath());
        }
    }
}
