package io.qiufen.common.io.resource;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/14 19:19
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ClassPathResource extends URLStreamResource
{
    private static final ClassLoader defaultClassLoader;

    static
    {
        defaultClassLoader = ClassPathResource.class.getClassLoader();
    }

    private final String path;

    public ClassPathResource(String path)
    {
        this(path, defaultClassLoader);
    }

    public ClassPathResource(String path, ClassLoader classLoader)
    {
        super(classLoader.getResource(path));
        this.path = path;
    }

    @Override
    public String toString()
    {
        return "ClassPathResource:" + path;
    }
}
