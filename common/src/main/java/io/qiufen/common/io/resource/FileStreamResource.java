package io.qiufen.common.io.resource;

import java.io.*;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/14 18:13
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class FileStreamResource implements StreamResource
{
    private final File file;

    public FileStreamResource(String path) throws IOException
    {
        if (path == null)
        {
            throw new IllegalArgumentException("Need path.");
        }
        this.file = new File(path);
    }

    public FileStreamResource(File file)
    {
        if (file == null)
        {
            throw new IllegalArgumentException("Need file.");
        }
        this.file = file;
    }

    @Override
    public InputStream open() throws FileNotFoundException
    {
        return new FileInputStream(this.file);
    }

    @Override
    public String toString()
    {
        return "FileStreamResource:" + file.getPath();
    }
}
