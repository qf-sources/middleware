package io.qiufen.common.io.serializer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.util.BitSet;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/7/22 20:28
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class BitSetSerializer extends Serializer<BitSet>
{
    @Override
    public void write(Kryo kryo, Output output, BitSet object)
    {
        if (null != object)
        {
            byte[] data = object.toByteArray();
            output.writeByte(data.length);//长度
            output.write(data);
        }
        else
        {
            output.writeByte(0);//长度为0
        }
    }

    @Override
    public BitSet read(Kryo kryo, Input input, Class type)
    {
        int len = input.readByte();//长度
        byte[] data = input.readBytes(len);//读取数据
        return BitSet.valueOf(data);
    }
}
