package io.qiufen.common.io.resource;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/14 19:22
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class URLStreamResource implements StreamResource
{
    private final URL url;

    public URLStreamResource(URL url)
    {
        if (url == null)
        {
            throw new IllegalArgumentException("need url");
        }
        this.url = url;
    }

    @Override
    public InputStream open() throws IOException
    {
        return url.openStream();
    }

    @Override
    public String toString()
    {
        return "URLStreamResource:" + url;
    }
}
