package io.qiufen.common.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-6-5
 * Time: 下午5:07
 * To change this template use File | Settings | File Templates.
 */
public class IOUtil
{
    private static final Logger log = LoggerFactory.getLogger(IOUtil.class);

    public static byte[] read(InputStream stream) throws IOException
    {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int n = stream.read(buffer);
        while (-1 != n)
        {
            output.write(buffer, 0, n);
            n = stream.read(buffer);
        }
        return output.toByteArray();
    }

    public static String toString(URL url) throws IOException
    {
        return toString(url.openStream());
    }

    public static String toString(InputStream stream) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream, "utf-8"));
        StringBuilder builder = new StringBuilder();
        String str = br.readLine();
        while (null != str)
        {
            builder.append(str).append('\n');
            str = br.readLine();
        }
        return builder.toString();
    }

    public static void close(Closeable stream)
    {
        if (null != stream)
        {
            try
            {
                stream.close();
            }
            catch (IOException e)
            {
                log.error(e.toString(), e);
            }
        }
    }

    public static void close(HttpURLConnection connection)
    {
        if (null != connection)
        {
            try
            {
                connection.disconnect();
            }
            catch (Exception e)
            {
                log.error(e.toString(), e);
            }
        }
    }

    public static void write(File file, String content) throws IOException
    {
        FileWriter writer = null;
        try
        {
            writer = new FileWriter(file);
            writer.write(content);
            writer.flush();
        }
        finally
        {
            close(writer);
        }
    }
}
