package io.qiufen.common.provider;

public interface ProviderContext
{
    <P extends Provider, D extends Provider & ProviderDecorator<P>> void setProvider0(Class<P> type, D provider);

    <T extends Provider> T getProvider(Class<T> type);

    <P extends Provider> void addDecorator(Class<P> type, P provider);
}
