package io.qiufen.common.provider;

public interface ProviderCaller<P extends Provider>
{
    Object call(P provider, Object... args);
}
