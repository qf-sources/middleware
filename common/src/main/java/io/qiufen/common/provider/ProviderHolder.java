package io.qiufen.common.provider;

public class ProviderHolder
{
    private static final ThreadLocal<Entry> LOCAL = new ThreadLocal<Entry>();

    public static <T> T next(Object... args)
    {
        Entry entry = LOCAL.get();
        Node node;
        if (entry == null || (node = entry.currentNode) == null) return null;

        Provider provider = node.provider;
        entry.currentNode = node.next;
        return (T) entry.caller.call(provider, args);
    }

    static void prepare(Node node0, ProviderCaller x0)
    {
        LOCAL.set(new Entry(x0, node0));
    }

    static void clean()
    {
        LOCAL.remove();
    }

    private static class Entry
    {
        private final ProviderCaller caller;

        private Node currentNode;

        private Entry(ProviderCaller caller, Node node)
        {
            this.caller = caller;
            this.currentNode = node;
        }
    }
}
