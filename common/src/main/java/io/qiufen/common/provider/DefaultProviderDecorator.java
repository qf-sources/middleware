package io.qiufen.common.provider;

public class DefaultProviderDecorator<P extends Provider> implements ProviderDecorator<P>
{
    private Node<P> root;

    public Object call(ProviderCaller<P> x, Object... args)
    {
        ProviderHolder.prepare(root, x);
        try
        {
            return ProviderHolder.next(args);
        }
        finally
        {
            ProviderHolder.clean();
        }
    }

    @Override
    public void add(P provider)
    {
        if (root == null)
        {
            root = new Node<P>();
            root.provider = provider;
            return;
        }
        Node<P> node = new Node<P>();
        node.provider = provider;
        Node<P> pre = root;
        while (pre.next != null)
        {
            pre = pre.next;
        }
        pre.next = node;
    }
}

class Node<P extends Provider>
{
    P provider;
    Node<P> next;
}