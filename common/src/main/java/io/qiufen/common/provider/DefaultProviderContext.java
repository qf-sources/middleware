package io.qiufen.common.provider;

import java.util.IdentityHashMap;
import java.util.Map;

public class DefaultProviderContext implements ProviderContext
{
    private final Map<Class, Provider> providers = new IdentityHashMap<Class, Provider>();

    public <P extends Provider> void setProvider(Class<P> type, P provider)
    {
        this.providers.put(type, provider);
    }

    @Override
    public <P extends Provider, D extends Provider & ProviderDecorator<P>> void setProvider0(Class<P> type, D provider)
    {

    }

    @Override
    public <T extends Provider> T getProvider(Class<T> type)
    {
        return type.cast(providers.get(type));
    }

    @Override
    public <P extends Provider> void addDecorator(Class<P> type, P provider)
    {
        ((ProviderDecorator) this.providers.get(type)).add(provider);
    }
}
