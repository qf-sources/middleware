package io.qiufen.common.provider;

public interface ProviderDecorator<P extends Provider>
{
    void add(P provider);
}