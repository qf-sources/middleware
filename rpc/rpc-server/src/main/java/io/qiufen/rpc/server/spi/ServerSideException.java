package io.qiufen.rpc.server.spi;

import io.qiufen.rpc.common.exception.EndpointException;

public class ServerSideException extends EndpointException
{
    public ServerSideException(String message)
    {
        super(message);
    }

    public ServerSideException(Throwable cause)
    {
        super(cause);
    }
}
