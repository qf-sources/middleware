package io.qiufen.rpc.server.provider.router;

import io.qiufen.common.filter.FilterRegistry;
import io.qiufen.rpc.server.spi.router.RPCServiceRoute;
import io.qiufen.rpc.server.spi.rpc.RPCService;
import io.qiufen.rpc.server.spi.rpc.RPCServiceRequest;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;

class DefaultRPCServiceRoute implements RPCServiceRoute
{
    private final RPCService rpcService;
    private final FilterRegistry filterRegistry;

    DefaultRPCServiceRoute(RPCService rpcService, FilterRegistry filterRegistry)
    {
        this.rpcService = rpcService;
        this.filterRegistry = filterRegistry;
    }

    @Override
    public void doRPCService(RPCServiceRequest request, RPCServiceResponse response) throws Throwable
    {
        if (filterRegistry.isEmpty())
        {
            rpcService.doService(request, response);
        }
        else
        {
            filterRegistry.submit(new DefaultRPCServiceFilterContext(rpcService, request, response));
        }
    }
}
