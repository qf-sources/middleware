package io.qiufen.rpc.server.spi.rpc;

import io.qiufen.common.filter.Filter;

public interface RPCServiceFilter extends Filter<RPCServiceFilterContext> {}
