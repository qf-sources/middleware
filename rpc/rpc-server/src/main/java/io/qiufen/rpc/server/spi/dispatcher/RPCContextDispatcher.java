package io.qiufen.rpc.server.spi.dispatcher;

import io.netty.channel.Channel;
import io.qiufen.common.provider.Provider;
import io.qiufen.rpc.server.spi.context.RPCContext;

public interface RPCContextDispatcher extends Provider
{
    void init(Channel channel);

    void dispatch(RPCContext context);

    void close(Channel channel);
}
