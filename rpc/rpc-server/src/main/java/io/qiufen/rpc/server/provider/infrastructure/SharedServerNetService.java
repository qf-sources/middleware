package io.qiufen.rpc.server.provider.infrastructure;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.util.ReferenceCountUtil;
import io.qiufen.common.lang.Strings;
import io.qiufen.common.net.ChannelMonitorHandler;
import io.qiufen.rpc.server.provider.constant.CommonConstant;
import io.qiufen.rpc.server.spi.infrastructure.ServerNetService;

import java.util.IdentityHashMap;
import java.util.Map;

public class SharedServerNetService implements ServerNetService
{
    private final Map<Channel, ChannelInboundHandler> eventHandlerMap;

    private final ServerBootstrap bootstrap;

    public SharedServerNetService(int bosses, int workers)
    {
        eventHandlerMap = new IdentityHashMap<Channel, ChannelInboundHandler>();

        bootstrap = init(bosses, workers);
    }

    @Override
    public Channel bind(String host, int port, ChannelInboundHandler handler) throws InterruptedException
    {
        //监听端口
        ChannelFuture future = Strings.isEmpty(host) ? bootstrap.bind(port) : bootstrap.bind(host, port);
        Channel channel = future.sync().channel();
        eventHandlerMap.put(channel, handler);
        return channel;
    }

    private ServerBootstrap init(int bosses, int workers)
    {
        NioEventLoopGroup boss = new NioEventLoopGroup(bosses);
        NioEventLoopGroup worker = new NioEventLoopGroup(workers);

        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(boss, worker);
        bootstrap.channel(NioServerSocketChannel.class);
        bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
        final ChannelInboundHandler eventHandler = new DelegateChannelEventHandler(eventHandlerMap);
        bootstrap.childHandler(new ChannelInitializer<Channel>()
        {
            private final LengthFieldPrepender lengthFieldPrepender = new LengthFieldPrepender(2, 0);
            private final ChannelMonitorHandler channelMonitorHandler = new ChannelMonitorHandler(
                    CommonConstant.CHANNEL_MONITOR);
            @Override
            protected void initChannel(Channel ch)
            {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(new LengthFieldBasedFrameDecoder(4096, 0, 2, 0, 2));
                pipeline.addLast(lengthFieldPrepender);

                pipeline.addLast(channelMonitorHandler);
                pipeline.addLast(eventHandler);
            }
        });
        return bootstrap;
    }
}

@ChannelHandler.Sharable
class DelegateChannelEventHandler extends ChannelInboundHandlerAdapter
{
    private final Map<Channel, ChannelInboundHandler> eventHandlerMap;

    DelegateChannelEventHandler(Map<Channel, ChannelInboundHandler> eventHandlerMap)
    {
        this.eventHandlerMap = eventHandlerMap;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception
    {
        ChannelInboundHandler handler = eventHandlerMap.get(ctx.channel().parent());
        if (null != handler) handler.channelActive(ctx);
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) throws Exception
    {
        ChannelInboundHandler handler = eventHandlerMap.get(ctx.channel().parent());
        if (null == handler)
        {
            ReferenceCountUtil.release(msg);
        }
        else
        {
            handler.channelRead(ctx, msg);
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception
    {
        ChannelInboundHandler handler = eventHandlerMap.get(ctx.channel().parent());
        if (null != handler) handler.channelInactive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
    {
        ChannelInboundHandler handler = eventHandlerMap.get(ctx.channel().parent());
        if (null != handler) handler.exceptionCaught(ctx, cause);
    }
}