package io.qiufen.rpc.server.provider.net;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.common.net.Command;
import io.qiufen.rpc.common.net.DataPacket;
import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.context.RPCContext;
import io.qiufen.rpc.server.spi.context.RPCContextProvider;
import io.qiufen.rpc.server.spi.dispatcher.RPCContextDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Administrator on 2019/1/22.
 */
@ChannelHandler.Sharable
public class LogicEventHandler extends ChannelInboundHandlerAdapter
{
    private static final Logger LOGGER = LoggerFactory.getLogger(LogicEventHandler.class);

    private final RPCContextProvider rpcContextProvider;
    private final RPCContextDispatcher rpcContextDispatcher;

    public LogicEventHandler(ProviderContext providerContext)
    {
        this.rpcContextProvider = providerContext.getProvider(RPCContextProvider.class);
        this.rpcContextDispatcher = providerContext.getProvider(RPCContextDispatcher.class);
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) throws ServerSideException
    {
        DataPacket packet;
        try
        {
            packet = DataPacket.fromBuffer(((ByteBuf) msg).retain());
        }
        finally
        {
            ReferenceCountUtil.release(msg);
        }

        try
        {
            Channel channel = ctx.channel();
            if (packet.getCmd() == Command.PREPARE.getCode())
            {
                long sessionId = packet.getData().readLong();
                LOGGER.info("Prepare channel for session,sessionId:{},channel:{}", sessionId, channel);

                //响应就绪
                byte cmd = Command.READY.getCode();
                int seq = packet.getSequence();
                DataPacket response = new DataPacket(cmd, seq, Unpooled.EMPTY_BUFFER, Unpooled.EMPTY_BUFFER,
                        Unpooled.EMPTY_BUFFER);
                ctx.writeAndFlush(response.toBuffer());
            }
            else if (packet.getCmd() == Command.END.getCode())
            {
                LOGGER.info("End channel for session,channel:{}", channel);

                //响应结束
                byte cmd = Command.END.getCode();
                int seq = packet.getSequence();
                DataPacket response = new DataPacket(cmd, seq, Unpooled.EMPTY_BUFFER, Unpooled.EMPTY_BUFFER,
                        Unpooled.EMPTY_BUFFER);
                ctx.writeAndFlush(response.toBuffer());
            }
            else if (packet.getCmd() == Command.REQUEST.getCode())
            {
                RPCContext rpcContext = rpcContextProvider.provide(channel, packet.retain());
                rpcContextDispatcher.dispatch(rpcContext);
            }
        }
        finally
        {
            packet.release();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    {
        LOGGER.error(cause.toString(), cause);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception
    {
        rpcContextDispatcher.init(ctx.channel());
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception
    {
        Channel channel = ctx.channel();
        rpcContextDispatcher.close(channel);
        super.channelInactive(ctx);
    }
}