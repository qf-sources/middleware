package io.qiufen.rpc.server.spi.infrastructure;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInboundHandler;
import io.qiufen.common.provider.Provider;

public interface ServerNetService extends Provider
{
    Channel bind(String host, int port, ChannelInboundHandler handler) throws InterruptedException;
}
