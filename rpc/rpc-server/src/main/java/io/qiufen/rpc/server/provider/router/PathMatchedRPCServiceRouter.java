package io.qiufen.rpc.server.provider.router;

import io.netty.buffer.ByteBufUtil;
import io.qiufen.common.filter.Context;
import io.qiufen.common.filter.FilterRegistry;
import io.qiufen.common.filter.Service;
import io.qiufen.rpc.common.net.DataPacket;
import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.context.RPCContext;
import io.qiufen.rpc.server.spi.router.RPCServiceRoute;
import io.qiufen.rpc.server.spi.router.RPCServiceRouter;
import io.qiufen.rpc.server.spi.rpc.RPCService;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class PathMatchedRPCServiceRouter implements RPCServiceRouter<String>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PathMatchedRPCServiceRouter.class);

    private final Map<String, RPCServiceRoute> routers = new HashMap<String, RPCServiceRoute>();

    private final FilterRegistry filterRegistry = new FilterRegistry("RPCFilter");

    public PathMatchedRPCServiceRouter()
    {
        filterRegistry.setService(new FilterService());
    }

    @Override
    public RPCServiceRouter<String> addRoute(String uri, RPCService rpcService) throws ServerSideException
    {
        if (routers.containsKey(uri)) throw new ServerSideException("Duplication route:" + uri);

        LOGGER.info("Add route,uri:{},rpc service:{}", uri, rpcService.getClass().getName());
        routers.put(uri, new DefaultRPCServiceRoute(rpcService, filterRegistry));
        return this;
    }

    @Override
    public RPCServiceRoute hitRoute(RPCContext rpcContext) throws ServerSideException
    {
        DataPacket packet = rpcContext.getPacket();
        String uri = new String(ByteBufUtil.getBytes(packet.getUri()));
        RPCServiceRoute route = routers.get(uri);
        LOGGER.debug("Hit route,uri:{}", uri);
        if (route == null) throw new ServerSideException("None route:" + uri);
        return route;
    }

    @Override
    public void addFilter(RPCServiceFilter filter)
    {
        filterRegistry.addFilter(filter);
    }
}

class FilterService implements Service
{
    @Override
    public void doService(Context context) throws Throwable
    {
        DefaultRPCServiceFilterContext rpcServiceFilterContext = (DefaultRPCServiceFilterContext) context;
        rpcServiceFilterContext.getRpcService()
                .doService(rpcServiceFilterContext.getRequest(), rpcServiceFilterContext.getResponse());
    }
}