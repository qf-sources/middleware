package io.qiufen.rpc.server.spi.rpc;

import io.qiufen.common.filter.Context;

public interface RPCServiceFilterContext extends Context
{
    RPCServiceRequest getRequest();

    RPCServiceResponse getResponse();
}
