package io.qiufen.rpc.server.spi.router;

import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.context.RPCContext;
import io.qiufen.rpc.server.spi.rpc.RPCService;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilter;

public interface RPCServiceRouter<U>
{
    RPCServiceRouter<U> addRoute(U uri, RPCService rpcService) throws ServerSideException;

    RPCServiceRoute hitRoute(RPCContext rpcContext) throws ServerSideException;

    void addFilter(RPCServiceFilter filter);
}
