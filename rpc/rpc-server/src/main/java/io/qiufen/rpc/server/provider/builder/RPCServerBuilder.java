package io.qiufen.rpc.server.provider.builder;

import io.qiufen.common.provider.DefaultProviderContext;
import io.qiufen.rpc.server.api.RPCServer;
import io.qiufen.rpc.server.api.RPCServerConfig;
import io.qiufen.rpc.server.provider.context.DefaultRPCContextProvider;
import io.qiufen.rpc.server.provider.infrastructure.DefaultServerNetService;
import io.qiufen.rpc.server.spi.context.RPCContextProvider;
import io.qiufen.rpc.server.spi.dispatcher.RPCContextDispatcher;
import io.qiufen.rpc.server.spi.infrastructure.ServerNetService;

/**
 * RPC服务器构建器
 */
public class RPCServerBuilder
{
    private final DefaultProviderContext providerContext = new DefaultProviderContext();

    private final RPCServerConfig config = defaultNetConfig();

    private RPCContextProvider rpcContextProvider;

    private RPCContextDispatcher rpcContextDispatcher;

    private ServerNetService serverNetService;

    private volatile boolean init = false;

    public RPCServerConfig getConfig()
    {
        return config;
    }

    public RPCServerBuilder setRpcContextProvider(RPCContextProvider rpcContextProvider)
    {
        this.rpcContextProvider = rpcContextProvider;
        return this;
    }

    public RPCServerBuilder setRpcContextDispatcher(RPCContextDispatcher rpcContextDispatcher)
    {
        this.rpcContextDispatcher = rpcContextDispatcher;
        return this;
    }

    public RPCServerBuilder setServerNetService(ServerNetService serverNetService)
    {
        this.serverNetService = serverNetService;
        return this;
    }

    public RPCServer build() throws InterruptedException
    {
        if (init) throw new IllegalStateException("Rpc server has been built.");
        init = true;

        if (rpcContextProvider == null) rpcContextProvider = DefaultRPCContextProvider.INSTANCE;
        providerContext.setProvider(RPCContextProvider.class, rpcContextProvider);

        if (rpcContextDispatcher == null) throw new IllegalArgumentException("Need rpc context dispatcher");
        providerContext.setProvider(RPCContextDispatcher.class, rpcContextDispatcher);

        if (serverNetService == null)
        {
            int cpus = Runtime.getRuntime().availableProcessors();
            int boss = cpus < 2 ? 1 : cpus >> 1;
            int workers = cpus << 2;
            serverNetService = new DefaultServerNetService(boss, workers);
        }
        providerContext.setProvider(ServerNetService.class, serverNetService);

        providerContext.setProvider(RPCServerConfig.class, config);

        return new DefaultRPCServer(providerContext).start();
    }

    private RPCServerConfig defaultNetConfig()
    {
        return new RPCServerConfig();
    }
}
