package io.qiufen.rpc.server.provider.infrastructure;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.qiufen.common.lang.Strings;
import io.qiufen.common.net.ChannelMonitorHandler;
import io.qiufen.rpc.server.provider.constant.CommonConstant;
import io.qiufen.rpc.server.spi.infrastructure.ServerNetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultServerNetService implements ServerNetService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultServerNetService.class);

    private final ServerBootstrap bootstrap;

    private volatile ChannelInboundHandler handler;

    public DefaultServerNetService(int bosses, int workers)
    {
        bootstrap = init(bosses, workers);
    }

    @Override
    public Channel bind(String host, int port, ChannelInboundHandler handler) throws InterruptedException
    {
        setHandler(handler);

        LOGGER.info("Connect, host:" + host + ", port:" + port);
        //监听端口
        ChannelFuture future = Strings.isEmpty(host) ? bootstrap.bind(port) : bootstrap.bind(host, port);
        return future.sync().channel();
    }

    private ServerBootstrap init(int bosses, int workers)
    {
        NioEventLoopGroup boss = new NioEventLoopGroup(bosses);
        NioEventLoopGroup worker = new NioEventLoopGroup(workers);

        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(boss, worker);
        bootstrap.channel(NioServerSocketChannel.class);
        bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
        bootstrap.childHandler(new ChannelInitializer<Channel>()
        {
            private final LengthFieldPrepender lengthFieldPrepender = new LengthFieldPrepender(2, 0);
            private final ChannelMonitorHandler channelMonitorHandler = new ChannelMonitorHandler(
                    CommonConstant.CHANNEL_MONITOR);
            @Override
            protected void initChannel(Channel ch)
            {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(new LengthFieldBasedFrameDecoder(4096, 0, 2, 0, 2));
                pipeline.addLast(lengthFieldPrepender);

                pipeline.addLast(channelMonitorHandler);
                pipeline.addLast(getHandler());
            }
        });
        return bootstrap;
    }

    private void checkHandler(ChannelInboundHandler handler)
    {
        if (this.handler != handler)
        {
            throw new IllegalArgumentException(
                    "Not allow set different handler,you can use " + SharedServerNetService.class.getName() + " on necessary");
        }
    }

    private ChannelInboundHandler getHandler()
    {
        return handler;
    }

    private void setHandler(ChannelInboundHandler handler)
    {
        if (this.handler != null)
        {
            checkHandler(handler);
            return;
        }
        synchronized (this)
        {
            if (this.handler != null)
            {
                checkHandler(handler);
                return;
            }
            this.handler = handler;
        }
    }
}
