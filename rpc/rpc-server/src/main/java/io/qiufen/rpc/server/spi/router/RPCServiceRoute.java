package io.qiufen.rpc.server.spi.router;

import io.qiufen.rpc.server.spi.rpc.RPCServiceRequest;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;

public interface RPCServiceRoute
{
    void doRPCService(RPCServiceRequest request, RPCServiceResponse response) throws Throwable;
}
