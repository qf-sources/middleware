package io.qiufen.rpc.server.spi.rpc;

import io.qiufen.rpc.common.rpc.BufferWriter;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/11/9 11:00
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface RPCServiceResponse
{
    <T> void writeHeader(BufferWriter<T> writer, T t);

    <T> void writeData(BufferWriter<T> writer, T t);
}
