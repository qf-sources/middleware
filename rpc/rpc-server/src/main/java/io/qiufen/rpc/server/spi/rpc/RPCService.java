package io.qiufen.rpc.server.spi.rpc;

/**
 * [功能描述]
 *
 * @author Forany
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface RPCService
{
    /**
     * 处理服务
     *
     * @param request  request
     * @param response response
     */
    void doService(RPCServiceRequest request, RPCServiceResponse response) throws Throwable;
}
