package io.qiufen.rpc.server.spi.rpc;

import io.qiufen.rpc.common.rpc.BufferLoader;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/11/9 11:00
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface RPCServiceRequest
{
    <T> T getUri(BufferLoader<T> loader);

    <T> T getHeader(BufferLoader<T> loader);

    <T> T getData(BufferLoader<T> loader);
}
