package io.qiufen.rpc.server.spi.context;

import io.netty.channel.Channel;
import io.qiufen.rpc.common.net.DataPacket;
import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.rpc.RPCServiceRequest;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;

import java.io.Closeable;

public interface RPCContext extends Closeable
{
    Channel getChannel();

    DataPacket getPacket();

    RPCServiceRequest getRequest();

    RPCServiceResponse getResponse();

    void finish() throws Exception;
}
