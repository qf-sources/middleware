package io.qiufen.rpc.server.provider.context;

import io.netty.buffer.ByteBuf;
import io.qiufen.rpc.common.rpc.BufferWriter;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;

import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/24 10:49
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class DefaultRPCServiceResponse implements RPCServiceResponse
{
    private final List<ByteBuf> sendingHeader;
    private final List<ByteBuf> sendingData;

    DefaultRPCServiceResponse(List<ByteBuf> sendingHeader, List<ByteBuf> sendingData)
    {
        this.sendingHeader = sendingHeader;
        this.sendingData = sendingData;
    }

    @Override
    public <T> void writeHeader(BufferWriter<T> writer, T t)
    {
        sendingHeader.add((ByteBuf) writer.to(t));
    }

    @Override
    public <T> void writeData(BufferWriter<T> writer, T t)
    {
        sendingData.add((ByteBuf) writer.to(t));
    }
}