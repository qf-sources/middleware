package io.qiufen.rpc.server.provider.constant;

import io.qiufen.common.net.ChannelMonitor;

public class CommonConstant
{
    public static final ChannelMonitor CHANNEL_MONITOR = new ChannelMonitor();

    private CommonConstant() {}
}
