package io.qiufen.rpc.server.provider.context;

import io.qiufen.rpc.common.net.DataPacket;
import io.qiufen.rpc.common.rpc.BufferLoader;
import io.qiufen.rpc.server.spi.rpc.RPCServiceRequest;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/24 10:49
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class DefaultRPCServiceRequest implements RPCServiceRequest
{
    private final DataPacket packet;

    DefaultRPCServiceRequest(DataPacket packet)
    {
        this.packet = packet;
    }

    @Override
    public <T> T getUri(BufferLoader<T> loader)
    {
        return loader.to(packet.getUri().retain());
    }

    @Override
    public <T> T getHeader(BufferLoader<T> loader)
    {
        return loader.to(packet.getHeader().retain());
    }

    @Override
    public <T> T getData(BufferLoader<T> loader)
    {
        return loader.to(packet.getData().retain());
    }
}
