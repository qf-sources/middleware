package io.qiufen.rpc.server.api;

import io.qiufen.common.provider.Provider;

/**
 * [功能描述]
 *
 * @author Forany
 * @date 2020/8/23 23:23
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class RPCServerConfig implements Provider
{
    private String host;

    private int port;

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }
}
