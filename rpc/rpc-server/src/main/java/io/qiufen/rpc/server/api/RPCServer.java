package io.qiufen.rpc.server.api;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/11/5 19:25
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface RPCServer
{
    RPCServerConfig getConfig();
}
