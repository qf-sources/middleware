package io.qiufen.rpc.server.provider.context;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.CompositeByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.net.SendResult;
import io.qiufen.rpc.common.net.Command;
import io.qiufen.rpc.common.net.DataPacket;
import io.qiufen.rpc.server.provider.constant.CommonConstant;
import io.qiufen.rpc.server.spi.context.RPCContext;
import io.qiufen.rpc.server.spi.rpc.RPCServiceRequest;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.ClosedChannelException;
import java.util.ArrayList;
import java.util.List;

class DefaultRPCContext implements RPCContext
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultRPCContext.class);
    private final RPCServiceRequest request;
    private final RPCServiceResponse response;

    private final Channel channel;
    private final DataPacket packet;
    private final List<ByteBuf> sendingHeader;
    private final List<ByteBuf> sendingData;

    DefaultRPCContext(Channel channel, DataPacket packet)
    {
        this.channel = channel;
        this.packet = packet;

        this.request = new DefaultRPCServiceRequest(packet);

        this.sendingHeader = new ArrayList<ByteBuf>();
        this.sendingData = new ArrayList<ByteBuf>();
        this.response = new DefaultRPCServiceResponse(sendingHeader, sendingData);
    }

    @Override
    public Channel getChannel()
    {
        return channel;
    }

    @Override
    public DataPacket getPacket()
    {
        return packet;
    }

    @Override
    public RPCServiceRequest getRequest()
    {
        return request;
    }

    @Override
    public RPCServiceResponse getResponse()
    {
        return response;
    }

    @Override
    public void finish() throws ClosedChannelException, InterruptedException
    {
        byte cmd = Command.RESPONSE.getCode();
        ByteBufAllocator allocator = channel.alloc();
        ByteBuf header = toBuf(allocator, sendingHeader);
        ByteBuf data = toBuf(allocator, sendingData);
        ByteBuf buf = new DataPacket(cmd, packet.getSequence(), Unpooled.EMPTY_BUFFER, header, data).toBuffer();
        send(buf);
    }

    private void send(ByteBuf buf) throws ClosedChannelException, InterruptedException
    {
        try
        {
            SendResult result = CommonConstant.CHANNEL_MONITOR.send(channel, buf.retain());
            if (result == SendResult.SUCCESS)
            {
                LOGGER.debug("Send success:{}", channel);
            }
            else if (result == SendResult.CH_INACTIVE)
            {
                throw new ClosedChannelException();
            }
            else
            {
                throw new IllegalStateException("write failure,channel state:" + result + ",channel:" + channel);
            }
        }
        finally
        {
            buf.release();
        }
    }

    @Override
    public void close()
    {
        packet.release();

        for (ByteBuf buf : sendingHeader)
        {
            buf.release();
        }
        sendingHeader.clear();

        for (ByteBuf buf : sendingData)
        {
            buf.release();
        }
        sendingData.clear();
    }

    private ByteBuf toBuf(ByteBufAllocator allocator, List<ByteBuf> bufList)
    {
        if (CollectionUtil.isEmpty(bufList)) return Unpooled.EMPTY_BUFFER;

        if (bufList.size() == 1) return bufList.get(0).retain();

        CompositeByteBuf compositeByteBuf = allocator.compositeBuffer(bufList.size());
        for (ByteBuf buf : bufList)
        {
            compositeByteBuf.addComponent(true, buf.retain());
        }
        return compositeByteBuf;
    }
}
