package io.qiufen.rpc.server.provider.context;

import io.netty.channel.Channel;
import io.qiufen.rpc.common.net.DataPacket;
import io.qiufen.rpc.server.spi.context.RPCContext;
import io.qiufen.rpc.server.spi.context.RPCContextProvider;

public class DefaultRPCContextProvider implements RPCContextProvider
{
    public static final RPCContextProvider INSTANCE = new DefaultRPCContextProvider();

    private DefaultRPCContextProvider()
    {
    }

    @Override
    public RPCContext provide(Channel channel, DataPacket packet)
    {
        return new DefaultRPCContext(channel, packet);
    }
}
