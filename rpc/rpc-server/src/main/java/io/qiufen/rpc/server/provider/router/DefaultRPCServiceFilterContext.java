package io.qiufen.rpc.server.provider.router;

import io.qiufen.rpc.server.spi.rpc.RPCService;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilterContext;
import io.qiufen.rpc.server.spi.rpc.RPCServiceRequest;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;

class DefaultRPCServiceFilterContext implements RPCServiceFilterContext
{
    private final RPCService rpcService;
    private final RPCServiceRequest request;
    private final RPCServiceResponse response;

    DefaultRPCServiceFilterContext(RPCService rpcService, RPCServiceRequest request, RPCServiceResponse response)
    {
        this.rpcService = rpcService;
        this.request = request;
        this.response = response;
    }

    @Override
    public RPCServiceRequest getRequest()
    {
        return request;
    }

    @Override
    public RPCServiceResponse getResponse()
    {
        return response;
    }

    RPCService getRpcService()
    {
        return rpcService;
    }
}
