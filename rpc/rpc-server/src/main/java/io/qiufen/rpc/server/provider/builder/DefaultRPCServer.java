package io.qiufen.rpc.server.provider.builder;

import io.netty.channel.Channel;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.server.api.RPCServer;
import io.qiufen.rpc.server.api.RPCServerConfig;
import io.qiufen.rpc.server.provider.net.LogicEventHandler;
import io.qiufen.rpc.server.spi.infrastructure.ServerNetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

/**
 * Created by Administrator on 2018/11/10.
 */
class DefaultRPCServer implements RPCServer
{
    private static final Logger log = LoggerFactory.getLogger(DefaultRPCServer.class);

    private final ProviderContext providerContext;
    private final RPCServerConfig serverConfig;
    private final ServerNetService serverNetService;

    DefaultRPCServer(ProviderContext providerContext)
    {
        this.providerContext = providerContext;
        this.serverConfig = providerContext.getProvider(RPCServerConfig.class);
        this.serverNetService = providerContext.getProvider(ServerNetService.class);
    }

    DefaultRPCServer start() throws InterruptedException
    {
        LogicEventHandler eventHandler = new LogicEventHandler(providerContext);
        //监听端口
        Channel channel = serverNetService.bind(serverConfig.getHost(), serverConfig.getPort(), eventHandler);
        InetSocketAddress socketAddress = (InetSocketAddress) channel.localAddress();
        serverConfig.setHost(socketAddress.getAddress().getHostAddress());
        serverConfig.setPort(socketAddress.getPort());
        log.info("Rpc server started on port@{}:{}.", serverConfig.getHost(), serverConfig.getPort());
        //同步等待
        channel.closeFuture();
        return this;
    }

    @Override
    public RPCServerConfig getConfig()
    {
        return serverConfig;
    }
}