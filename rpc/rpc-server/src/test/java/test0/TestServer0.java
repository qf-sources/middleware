package test0;

import io.netty.buffer.ByteBuf;
import io.qiufen.rpc.common.rpc.ByteBufLoader;
import io.qiufen.rpc.common.rpc.ByteBufWriter;
import io.qiufen.rpc.server.provider.builder.RPCServerBuilder;
import io.qiufen.rpc.server.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.server.provider.router.PathMatchedRPCServiceRouter;
import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.router.RPCServiceRouter;
import io.qiufen.rpc.server.spi.rpc.RPCService;
import io.qiufen.rpc.server.spi.rpc.RPCServiceRequest;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class TestServer0
{
    public static void main(String[] args) throws InterruptedException, ServerSideException
    {
        RPCServiceRouter<String> router = new PathMatchedRPCServiceRouter();
        router.addRoute("/test", new FirstRPCService());

        RPCServerBuilder builder = new RPCServerBuilder().setRpcContextDispatcher(
                new DefaultRPCContextDispatcher(Executors.newCachedThreadPool(), router, 1024, 1024 * 1024));
        builder.getConfig().setPort(50001);
        builder.build();
    }
}

class FirstRPCService implements RPCService
{
    @Override
    public void doService(RPCServiceRequest request, RPCServiceResponse response)
    {
        ByteBuf buf = request.getData(ByteBufLoader.INSTANCE);
        response.writeData(ByteBufWriter.INSTANCE, buf);
        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(50));
    }
}