package test1;

import io.netty.buffer.ByteBuf;
import io.qiufen.rpc.common.rpc.ByteBufLoader;
import io.qiufen.rpc.common.rpc.ByteBufWriter;
import io.qiufen.rpc.server.provider.builder.RPCServerBuilder;
import io.qiufen.rpc.server.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.server.provider.router.PathMatchedRPCServiceRouter;
import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.router.RPCServiceRouter;
import io.qiufen.rpc.server.spi.rpc.RPCService;
import io.qiufen.rpc.server.spi.rpc.RPCServiceRequest;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;

import java.util.concurrent.Executors;

public class TestServer1
{
    public static void main(String[] args) throws InterruptedException, ServerSideException
    {
        RPCServiceRouter<String> router = new PathMatchedRPCServiceRouter();
        router.addRoute("/first/get", new FirstRPCService());

        RPCServerBuilder builder = new RPCServerBuilder().setRpcContextDispatcher(
                new DefaultRPCContextDispatcher(Executors.newFixedThreadPool(16), router, 1024 * 1024,
                        1024 * 1024 * 5));
        builder.getConfig().setPort(8080);
        builder.build();
    }
}

class FirstRPCService implements RPCService
{
    @Override
    public void doService(RPCServiceRequest request, RPCServiceResponse response)
    {
        ByteBuf buf = request.getData(ByteBufLoader.INSTANCE);
        response.writeData(ByteBufWriter.INSTANCE, buf);
//        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(50));
    }
}