package test0;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.util.ResourceLeakDetector;
import io.qiufen.common.concurrency.executor.spi.Command;
import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.provider.builder.SingleRouteRPCClientBuilder;
import io.qiufen.rpc.client.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.rpc.*;
import io.qiufen.rpc.common.rpc.ByteBufLoader;
import io.qiufen.rpc.common.rpc.ByteBufWriter;

import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;

public class TestClient0
{
    static AtomicInteger total = new AtomicInteger(0);
    static AtomicInteger thought = new AtomicInteger(0);
    static AtomicInteger totalTimes = new AtomicInteger(0);

    static AtomicInteger r0_10 = new AtomicInteger(0), r10_50 = new AtomicInteger(0), r50_100 = new AtomicInteger(
            0), r100 = new AtomicInteger(0);

    public static void main(String[] args) throws Exception
    {
        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);

        int users = 1000;
        int connections = 1;

        SingleRouteRPCClientBuilder builder = new SingleRouteRPCClientBuilder();
        builder.getClientConfig().setInitConnections(1);
        builder.getClientConfig().setMaxConnections(connections);
        builder.setRpcContextDispatcher(
                new DefaultRPCContextDispatcher(Executors.newCachedThreadPool(), 10240, 1024 * 1024));
        final RPCClient client = builder.setHost(new Host("localhost", 50001)).build();

        final String str = UUID.randomUUID().toString();
        final byte[] uri = "/test".getBytes();
        final byte[] data = str.getBytes();
        Executor executor = Executors.newFixedThreadPool(users);

        final RPCRequestSetter setter = new RPCRequestSetter()
        {
            @Override
            public void set(RPCRequest request)
            {
                request.writeUri(ByteBufWriter.INSTANCE, ByteBufAllocator.DEFAULT.buffer(uri.length).writeBytes(uri));
                request.writeData(ByteBufWriter.INSTANCE,
                        ByteBufAllocator.DEFAULT.buffer(data.length).writeBytes(data));
            }
        };
        final Feedback feedback = new FeedbackAdapter()
        {
            @Override
            public void onResponse(RPCResponse response) throws Exception
            {
                ByteBuf buf = response.getData(ByteBufLoader.INSTANCE);
                //                    System.out.println(new String(ByteBufUtil.getBytes(buf)));
                buf.release();
                total.incrementAndGet();
            }
        };
        new Thread()
        {
            @Override
            public void run()
            {
                while (true)
                {
                    LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(1));
                    int count = total.get();
                    int times = totalTimes.get();
                    int th = thought.get();
                    thought.addAndGet(-th);
                    if (count <= 0)
                    {
                        continue;
                    }
                    System.out.println(
                            "total:" + count + ",r0_10:" + r0_10 + ",r10_50:" + r10_50 + ",r50_100:" + r50_100 + ",r100:" + r100 + ",avg cost:" + (times / count) + ",thought:" + th);
                }
            }
        }.start();

        for (int i = 0; i < 5000000; i++)
        {
            executor.execute(new Command()
            {
                @Override
                protected void doExecute() throws Throwable
                {
                    final Semaphore lock = new Semaphore(0);
                    client.send(setter, 100000, new FeedbackAdapter()
                    {
                        final long p0 = System.currentTimeMillis();

                        @Override
                        public void onResponse(RPCResponse response)
                        {
                            try
                            {
                                //                            ByteBuf buf = response.getData(ByteBufLoader.INSTANCE);
                                //                    System.out.println(new String(ByteBufUtil.getBytes(buf)));
                                //                            buf.release();

                                //                                ByteBuf buf0 = response.getHeader(ByteBufLoader.INSTANCE);
                                //                                long p0 = buf0.readLong();
                                //                                buf0.release();

                                long p1 = System.currentTimeMillis();
                                int time = (int) (p1 - p0);
                                totalTimes.addAndGet(time);
                                total.incrementAndGet();
                                thought.incrementAndGet();
                                if (time >= 100)
                                {
                                    r100.incrementAndGet();
                                    return;
                                }
                                if (time >= 50)
                                {
                                    r50_100.incrementAndGet();
                                    return;
                                }
                                if (time >= 10)
                                {
                                    r10_50.incrementAndGet();
                                    return;
                                }
                                r0_10.incrementAndGet();
                            }
                            finally
                            {
                                lock.release(1);
                            }
                        }
                    });
                    lock.acquire(1);
                }
            });
        }
    }
}
