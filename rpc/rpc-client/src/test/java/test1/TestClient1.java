package test1;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.util.ResourceLeakDetector;
import io.qiufen.common.lang.JsonObject;
import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.provider.builder.SingleRouteRPCClientBuilder;
import io.qiufen.rpc.client.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.rpc.FeedbackAdapter;
import io.qiufen.rpc.client.spi.rpc.RPCRequest;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;
import io.qiufen.rpc.client.spi.rpc.RPCResponse;
import io.qiufen.rpc.common.rpc.ByteBufLoader;
import io.qiufen.rpc.common.rpc.ByteBufWriter;

import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

public class TestClient1
{
    static AtomicLong count = new AtomicLong(0);

    public static void main(String[] args) throws Exception
    {
        new Timer().scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                long c = count.get();
                count.addAndGet(-c);
                System.out.println("count = " + c);
            }
        }, 1000, 1000);

        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);

        int connections = 1;

        SingleRouteRPCClientBuilder builder = new SingleRouteRPCClientBuilder();
        builder.getClientConfig().setInitConnections(1);
        builder.getClientConfig().setMaxConnections(connections);
        builder.setRpcContextDispatcher(
                new DefaultRPCContextDispatcher(Executors.newCachedThreadPool(), 10240, 1024 * 1024));
        final RPCClient client = builder.setHost(new Host("localhost", 8080)).build();

        String adding = UUID.randomUUID().toString();
        final byte[] uri = "/first/get".getBytes();
        final byte[] data = JsonObject.toJsonBytes(adding);

        while (true)
        {
//            LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(1));
            final RPCRequestSetter setter = new RPCRequestSetter()
            {
                @Override
                public void set(RPCRequest request)
                {
                    request.writeUri(ByteBufWriter.INSTANCE,
                            ByteBufAllocator.DEFAULT.buffer(uri.length).writeBytes(uri));
                    request.writeData(ByteBufWriter.INSTANCE,
                            ByteBufAllocator.DEFAULT.buffer(data.length + 4).writeInt(data.length).writeBytes(data));
                }
            };

            client.send(setter, 100000, new FeedbackAdapter()
            {
                @Override
                public void onResponse(RPCResponse response) throws Exception
                {
                    ByteBuf buf = response.getData(ByteBufLoader.INSTANCE);
//                    System.out.println(new String(ByteBufUtil.getBytes(buf)));

                    buf.release();
                    count.incrementAndGet();
                }
            });
        }
    }
}
