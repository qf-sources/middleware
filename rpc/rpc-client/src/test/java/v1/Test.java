package v1;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.util.ResourceLeakDetector;
import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.provider.builder.MultipleRouteRPCClientBuilder;
import io.qiufen.rpc.client.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.rpc.FeedbackAdapter;
import io.qiufen.rpc.client.spi.rpc.RPCRequest;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;
import io.qiufen.rpc.client.spi.rpc.RPCResponse;
import io.qiufen.rpc.common.rpc.ByteBufLoader;
import io.qiufen.rpc.common.rpc.ByteBufWriter;

import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Test
{
    static AtomicInteger counter = new AtomicInteger(0);

    public static void main(String[] args) throws Exception
    {
        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);

        RPCClient client = new MultipleRouteRPCClientBuilder().setHostSet(
                        Collections.singleton(new Host("localhost", 50001)))
                .setRpcContextDispatcher(
                        new DefaultRPCContextDispatcher(Executors.newCachedThreadPool(), 1024, 1024 * 1024))
                .build();

        final String str = UUID.randomUUID().toString();
        for (int i = 0; i < 10000; i++)
        {
            client.send(new RPCRequestSetter()
            {
                @Override
                public void set(RPCRequest request)
                {
                    request.writeUri(ByteBufWriter.INSTANCE,
                            ByteBufAllocator.DEFAULT.buffer().writeBytes("/test".getBytes()));
                    request.writeData(ByteBufWriter.INSTANCE,
                            ByteBufAllocator.DEFAULT.buffer().writeBytes(str.getBytes()));
                }
            }, new FeedbackAdapter()
            {
                @Override
                public void onResponse(RPCResponse response)
                {
                    ByteBuf buf = response.getData(ByteBufLoader.INSTANCE);
                    //                    System.out.println(new String(ByteBufUtil.getBytes(buf)));
                    buf.release();
                    System.out.println(counter.incrementAndGet());
                }
            });
        }
    }
}
