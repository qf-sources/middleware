package io.qiufen.rpc.client.provider.builder;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.exception.ClientSideException;
import io.qiufen.rpc.client.spi.route.ClientRoute;
import io.qiufen.rpc.client.spi.route.ClientRouteTable;
import io.qiufen.rpc.client.spi.route.ClientRouteTableProvider;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class MultipleRouteRPCClient extends AbstractRPCClient
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MultipleRouteRPCClient.class);

    private final ProviderContext providerContext;

    private Set<Host> hostSet;

    private ClientRouteTable clientRouteTable;

    MultipleRouteRPCClient(ProviderContext providerContext)
    {
        this.providerContext = providerContext;
    }

    MultipleRouteRPCClient init()
    {
        ClientRouteTableProvider clientRouteTableProvider = this.providerContext.getProvider(
                ClientRouteTableProvider.class);
        this.clientRouteTable = clientRouteTableProvider.provide(this.providerContext);
        if (CollectionUtil.isNotEmpty(this.hostSet)) updateRoute(this.hostSet);

        this.hostSet = null;

        return this;
    }

    @Override
    public RPCClient addListener(NetEventListener listener)
    {
        clientRouteTable.addListener(listener);
        return this;
    }

    @Override
    public void send(RPCRequestSetter setter, long readTimeout, Feedback feedback) throws Exception
    {
        int failures = 0;
        for (; ; )
        {
            //命中路由
            ClientRoute clientRoute = clientRouteTable.hitRoute();

            //获取链接，发送请求
            Connection connection = null;
            try
            {
                connection = clientRoute.getConnectionPool().getConnection();
                connection.send(setter, readTimeout, feedback);
                break;
            }
            catch (ClientSideException e)
            {
                failures++;
                if (failures >= clientRouteTable.getRouteCount()) throw e;

                LOGGER.error("Send failures:{}", failures);
                LOGGER.error(e.toString(), e);
            }
            finally
            {
                //关闭链接
                if (connection != null) connection.close();
            }
        }
    }

    public MultipleRouteRPCClient updateRoute(Set<Host> hostSet)
    {
        clientRouteTable.setHost(hostSet);
        return this;
    }

    MultipleRouteRPCClient setHostSet(Set<Host> hostSet)
    {
        this.hostSet = hostSet;
        return this;
    }
}
