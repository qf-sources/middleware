package io.qiufen.rpc.client.spi.pool;

import io.qiufen.common.provider.Provider;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.spi.Host;

public interface ConnectionPoolProvider extends Provider
{
    ConnectionPool provide(ProviderContext providerContext, Host host);
}
