package io.qiufen.rpc.client.spi.rpc;

import io.qiufen.common.provider.Provider;

import java.util.List;

public interface RPCRequestProvider<ET> extends Provider
{
    RPCRequest create(List<ET> sendingUri, List<ET> sendingHeader, List<ET> sendingData);
}
