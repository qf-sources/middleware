package io.qiufen.rpc.client.spi.exception;

import io.qiufen.rpc.common.exception.RPCException;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/21 10:33
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class RPCTimeoutException extends RPCException
{
    public RPCTimeoutException(String message)
    {
        super(message);
    }
}
