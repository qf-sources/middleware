package io.qiufen.rpc.client.spi.rpc;

import io.qiufen.rpc.common.rpc.BufferWriter;

public interface RPCRequest
{
    <T> void writeUri(BufferWriter<T> writer, T t);

    <T> void writeHeader(BufferWriter<T> writer, T t);

    <T> void writeData(BufferWriter<T> writer, T t);
}
