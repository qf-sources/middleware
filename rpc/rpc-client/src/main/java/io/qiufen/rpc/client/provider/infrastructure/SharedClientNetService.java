package io.qiufen.rpc.client.provider.infrastructure;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.util.ReferenceCountUtil;
import io.qiufen.common.net.ChannelMonitorHandler;
import io.qiufen.rpc.client.provider.constant.BaseConstants;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.IdentityHashMap;
import java.util.Map;

public class SharedClientNetService implements ClientNetService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SharedClientNetService.class);

    private final Map<Channel, ChannelInboundHandler> eventHandlerMap;

    private final Bootstrap bootstrap;

    public SharedClientNetService(int workers)
    {
        eventHandlerMap = new IdentityHashMap<Channel, ChannelInboundHandler>();
        bootstrap = init(workers);
    }

    @Override
    public Channel connect(String host, int port, ChannelInboundHandler handler) throws InterruptedException
    {
        LOGGER.info("Connect, host:" + host + ", port:" + port);
        ChannelFuture future = bootstrap.connect(host, port);
        Channel channel = future.sync().channel();
        eventHandlerMap.put(channel, handler);
        return channel;
    }

    @Override
    public void disconnect(Channel channel)
    {
        LOGGER.info("Disconnect! remote:" + channel.remoteAddress());
        eventHandlerMap.remove(channel);
        if (channel.isActive()) channel.close();
    }

    private Bootstrap init(int workers)
    {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(new NioEventLoopGroup(workers));
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.option(ChannelOption.TCP_NODELAY, true);
        final ChannelInboundHandler eventHandler = new DelegateChannelEventHandler(eventHandlerMap);
        bootstrap.handler(new ChannelInitializer<Channel>()
        {
            private final LengthFieldPrepender lengthFieldPrepender = new LengthFieldPrepender(2, 0);
            private final ChannelMonitorHandler channelMonitorHandler = new ChannelMonitorHandler(
                    BaseConstants.CHANNEL_MONITOR);
            @Override
            protected void initChannel(Channel ch)
            {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(new LengthFieldBasedFrameDecoder(BaseConstants.MAX_FRAME_LENGTH, 0, 2, 0, 2));
                pipeline.addLast(lengthFieldPrepender);

                pipeline.addLast(channelMonitorHandler);
                pipeline.addLast(eventHandler);
            }
        });
        return bootstrap;
    }
}

@ChannelHandler.Sharable
class DelegateChannelEventHandler extends ChannelInboundHandlerAdapter
{
    private final Map<Channel, ChannelInboundHandler> eventHandlerMap;

    DelegateChannelEventHandler(Map<Channel, ChannelInboundHandler> eventHandlerMap)
    {
        this.eventHandlerMap = eventHandlerMap;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception
    {
        ChannelInboundHandler handler = eventHandlerMap.get(ctx.channel());
        if (null != handler) handler.channelActive(ctx);
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) throws Exception
    {
        ChannelInboundHandler handler = eventHandlerMap.get(ctx.channel());
        if (null == handler)
        {
            ReferenceCountUtil.release(msg);
        }
        else
        {
            handler.channelRead(ctx, msg);
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception
    {
        ChannelInboundHandler handler = eventHandlerMap.get(ctx.channel());
        if (null != handler) handler.channelInactive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
    {
        ChannelInboundHandler handler = eventHandlerMap.get(ctx.channel());
        if (null != handler) handler.exceptionCaught(ctx, cause);
    }
}