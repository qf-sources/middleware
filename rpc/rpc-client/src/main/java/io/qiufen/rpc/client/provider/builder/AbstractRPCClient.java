package io.qiufen.rpc.client.provider.builder;

import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;
import io.qiufen.rpc.client.spi.rpc.RPCResponse;
import io.qiufen.rpc.common.exception.RPCException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Semaphore;

abstract class AbstractRPCClient implements RPCClient
{
    @Override
    public void send(RPCRequestSetter setter, Feedback feedback) throws Exception
    {
        send(setter, 0, feedback);
    }

    @Override
    public RPCResponse send(RPCRequestSetter setter, long readTimeout) throws Exception
    {
        SyncFeedback feedback = new SyncFeedback();
        send(setter, readTimeout, feedback);
        return feedback.getResponse();
    }

    private static class SyncFeedback implements Feedback
    {
        private static final Logger LOGGER = LoggerFactory.getLogger(SyncFeedback.class);

        private final Semaphore lock = new Semaphore(0);

        private volatile RPCResponse response;
        private volatile RPCException exception;

        private SyncFeedback()
        {
        }

        public void onResponse(RPCResponse response)
        {
            LOGGER.debug("Return data from remote server...");
            this.response = response;
            lock.release(1);
        }

        public void onResponse(RPCException e)
        {
            this.exception = e;
            lock.release(1);
        }

        RPCResponse getResponse() throws RPCException, InterruptedException
        {
            lock.acquire(1);
            if (null == exception) return response;
            throw exception;
        }
    }
}
