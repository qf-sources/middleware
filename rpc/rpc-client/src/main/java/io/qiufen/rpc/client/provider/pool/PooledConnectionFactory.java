package io.qiufen.rpc.client.provider.pool;

import io.netty.channel.Channel;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.provider.connection.ConnectionFactory;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.connection.NetEventListenerAdapter;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class PooledConnectionFactory extends BasePooledObjectFactory<PooledConnection>
{
    private static final Logger log = LoggerFactory.getLogger(PooledConnectionFactory.class);

    private final Host host;
    private final ConnectionFactory factory;
    private final Map<Connection, PooledConnection> map = new HashMap<Connection, PooledConnection>();
    private ObjectPool<PooledConnection> pool;

    PooledConnectionFactory(ProviderContext providerContext, Host host)
    {
        this.factory = new ConnectionFactory(providerContext);

        this.host = host;

        this.factory.addListener(new NetEventListenerAdapter()
        {
            @Override
            public void onDisconnect(Channel channel) throws Exception
            {
                Connection connection = channel.attr(ConnectionFactory.KEY_CONN).get();
                synchronized (map)
                {
                    PooledConnection pooledConnection = map.remove(connection);
                    if (pooledConnection != null)
                    {
                        pool.invalidateObject(pooledConnection);
                    }
                }
            }
        });
    }

    void setPool(GenericObjectPool<PooledConnection> pool)
    {
        this.pool = pool;
    }

    @Override
    public PooledConnection create() throws Exception
    {
        for (; ; )
        {
            Connection connection = factory.create(host.getIp(), host.getPort());
            PooledConnection pooledConnection = new PooledConnection(pool, connection);
            synchronized (map)
            {
                if (connection.isClosed()) continue;

                map.put(connection, pooledConnection);
                return pooledConnection;
            }
        }
    }

    @Override
    public PooledObject<PooledConnection> wrap(PooledConnection obj)
    {
        return new DefaultPooledObject<PooledConnection>(obj);
    }

    @Override
    public void destroyObject(PooledObject<PooledConnection> p) throws Exception
    {
        Connection connection = p.getObject().getOriginConnection();
        synchronized (map)
        {
            if (map.remove(connection) != null && !connection.isClosed())
            {
                connection.close();
            }
        }
    }

    void addListener(NetEventListener listener)
    {
        factory.addListener(listener);
    }
}