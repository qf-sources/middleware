package io.qiufen.rpc.client.provider.builder;

import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.provider.route.DefaultClientRouteTableProvider;
import io.qiufen.rpc.client.provider.route.PollingLoadBalancePolicy;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.route.ClientRouteTableProvider;
import io.qiufen.rpc.client.spi.route.LoadBalancePolicy;

import java.util.Set;

public class MultipleRouteRPCClientBuilder extends AbstractRPCClientBuilder
{
    private Set<Host> hostSet;

    private ClientRouteTableProvider clientRouteTableProvider;

    private LoadBalancePolicy loadBalancePolicy;

    public MultipleRouteRPCClientBuilder setHostSet(Set<Host> hostSet)
    {
        this.hostSet = hostSet;
        return this;
    }

    public MultipleRouteRPCClientBuilder setClientRouteTableProvider(ClientRouteTableProvider clientRouteTableProvider)
    {
        this.clientRouteTableProvider = clientRouteTableProvider;
        return this;
    }

    public AbstractRPCClientBuilder setLoadBalancePolicy(LoadBalancePolicy loadBalancePolicy)
    {
        this.loadBalancePolicy = loadBalancePolicy;
        return this;
    }

    @Override
    protected RPCClient build0()
    {
        if (clientRouteTableProvider == null) clientRouteTableProvider = DefaultClientRouteTableProvider.INSTANCE;
        providerContext.setProvider(ClientRouteTableProvider.class, clientRouteTableProvider);

        if (loadBalancePolicy == null) loadBalancePolicy = PollingLoadBalancePolicy.INSTANCE;
        providerContext.setProvider(LoadBalancePolicy.class, loadBalancePolicy);

        return new MultipleRouteRPCClient(providerContext).setHostSet(hostSet).init();
    }
}
