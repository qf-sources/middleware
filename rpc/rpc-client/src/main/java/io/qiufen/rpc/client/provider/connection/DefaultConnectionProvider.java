package io.qiufen.rpc.client.provider.connection;

import io.netty.channel.Channel;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.connection.ConnectionProvider;

public class DefaultConnectionProvider implements ConnectionProvider
{
    public static final ConnectionProvider INSTANCE = new DefaultConnectionProvider();

    private DefaultConnectionProvider()
    {
    }

    @Override
    public Connection provide(ProviderContext providerContext, Channel channel)
    {
        return new DefaultConnection(providerContext, channel);
    }
}
