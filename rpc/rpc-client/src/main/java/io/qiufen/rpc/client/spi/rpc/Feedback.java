package io.qiufen.rpc.client.spi.rpc;

import io.qiufen.rpc.common.exception.RPCException;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface Feedback
{
    void onResponse(RPCResponse response) throws Exception;

    void onResponse(RPCException e) throws Exception;
}
