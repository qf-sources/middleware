package io.qiufen.rpc.client.spi.connection;

import io.netty.channel.Channel;
import io.qiufen.rpc.client.spi.Host;

/**
 * 网络客户端事件监听
 */
public interface NetEventListener
{
    /**
     * 断开链接事件
     *
     * @param local  本地主机
     * @param remote 远程主机
     */
    void onDisconnect(Host local, Host remote) throws Exception;

    /**
     * 创建链接事件
     *
     * @param local  本地主机
     * @param remote 远程主机
     */
    void onConnect(Host local, Host remote) throws Exception;

    void onConnect(Channel channel) throws Exception;

    void onDisconnect(Channel channel) throws Exception;

    void onReady(Channel channel) throws Exception;
}
