package io.qiufen.rpc.client.provider.builder;

import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.spi.Host;

public class SingleRouteRPCClientBuilder extends AbstractRPCClientBuilder
{
    private Host host;

    public SingleRouteRPCClientBuilder setHost(Host host)
    {
        this.host = host;
        return this;
    }

    @Override
    protected RPCClient build0()
    {
        if (host == null) throw new IllegalArgumentException("Need host.");

        return new SingleRouteRPCClient(providerContext).setHost(host).init();
    }
}
