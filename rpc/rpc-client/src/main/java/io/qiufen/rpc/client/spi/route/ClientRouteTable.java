package io.qiufen.rpc.client.spi.route;

import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.exception.ClientSideException;

import java.util.Set;

/**
 * 网络客户端路由表
 */
public interface ClientRouteTable
{
    /**
     * 命中客户端路由
     */
    ClientRoute hitRoute() throws ClientSideException;

    void addListener(NetEventListener listener);

    void setHost(Set<Host> hostSet);

    int getRouteCount();
}
