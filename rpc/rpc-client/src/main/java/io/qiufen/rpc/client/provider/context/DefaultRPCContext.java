package io.qiufen.rpc.client.provider.context;

import io.netty.channel.Channel;
import io.qiufen.rpc.client.spi.context.RPCContext;
import io.qiufen.rpc.client.spi.rpc.RPCResponse;
import io.qiufen.rpc.common.net.DataPacket;

class DefaultRPCContext implements RPCContext
{
    private final Channel channel;
    private final DataPacket packet;

    private final RPCResponse response;

    DefaultRPCContext(Channel channel, DataPacket packet)
    {
        this.channel = channel;
        this.packet = packet;

        this.response = new DefaultRPCResponse(packet);
    }

    @Override
    public Channel getChannel()
    {
        return channel;
    }

    @Override
    public DataPacket getPacket()
    {
        return packet;
    }

    @Override
    public RPCResponse getResponse()
    {
        return response;
    }

    @Override
    public void close()
    {
        this.packet.release();
    }
}
