package io.qiufen.rpc.client.provider.infrastructure;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.qiufen.common.net.ChannelMonitorHandler;
import io.qiufen.rpc.client.provider.constant.BaseConstants;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultClientNetService implements ClientNetService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultClientNetService.class);

    private final Bootstrap bootstrap;

    private volatile ChannelInboundHandler handler;

    public DefaultClientNetService(int workers)
    {
        bootstrap = init(workers);
    }

    @Override
    public Channel connect(String host, int port, ChannelInboundHandler handler) throws InterruptedException
    {
        setHandler(handler);

        LOGGER.info("Connect, host:" + host + ", port:" + port);
        ChannelFuture future = bootstrap.connect(host, port);
        return future.sync().channel();
    }

    @Override
    public void disconnect(Channel channel)
    {
        LOGGER.info("Disconnect! remote:" + channel.remoteAddress());
        if (channel.isActive()) channel.close();
    }

    private Bootstrap init(int workers)
    {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(new NioEventLoopGroup(workers));
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.option(ChannelOption.TCP_NODELAY, true);
        bootstrap.handler(new ChannelInitializer<Channel>()
        {
            private final LengthFieldPrepender lengthFieldPrepender = new LengthFieldPrepender(2, 0);
            private final ChannelMonitorHandler channelMonitorHandler = new ChannelMonitorHandler(
                    BaseConstants.CHANNEL_MONITOR);
            @Override
            protected void initChannel(Channel ch)
            {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast(new LengthFieldBasedFrameDecoder(BaseConstants.MAX_FRAME_LENGTH, 0, 2, 0, 2));
                pipeline.addLast(lengthFieldPrepender);

                pipeline.addLast(channelMonitorHandler);
                pipeline.addLast(getHandler());
            }
        });
        return bootstrap;
    }

    private void checkHandler(ChannelInboundHandler handler)
    {
        if (this.handler != handler)
        {
            throw new IllegalArgumentException(
                    "Not allow set different handler,you can use " + SharedClientNetService.class.getName() + " on necessary");
        }
    }

    private ChannelInboundHandler getHandler()
    {
        return handler;
    }

    private void setHandler(ChannelInboundHandler handler)
    {
        if (this.handler != null)
        {
            checkHandler(handler);
            return;
        }
        synchronized (this)
        {
            if (this.handler != null)
            {
                checkHandler(handler);
                return;
            }
            this.handler = handler;
        }
    }
}
