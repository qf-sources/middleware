package io.qiufen.rpc.client.spi.connection;

import io.netty.channel.Channel;
import io.qiufen.common.provider.Provider;
import io.qiufen.common.provider.ProviderContext;

public interface ConnectionProvider extends Provider
{
    Connection provide(ProviderContext providerContext, Channel channel);
}
