package io.qiufen.rpc.client.provider.connection;

/**
 * 序列器
 */
public interface Sequence
{
    int get();
}
