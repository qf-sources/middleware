package io.qiufen.rpc.client.provider.pool;

import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.pool.ConnectionPool;
import io.qiufen.rpc.client.spi.pool.ConnectionPoolProvider;

public class DefaultConnectionPoolProvider implements ConnectionPoolProvider
{
    public static final ConnectionPoolProvider INSTANCE = new DefaultConnectionPoolProvider();

    private DefaultConnectionPoolProvider()
    {
    }

    @Override
    public ConnectionPool provide(ProviderContext providerContext, Host host)
    {
        return new DefaultConnectionPool(providerContext, host).applyConfig();
    }
}
