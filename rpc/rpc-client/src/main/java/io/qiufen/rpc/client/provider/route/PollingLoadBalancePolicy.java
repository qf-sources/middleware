package io.qiufen.rpc.client.provider.route;

import io.qiufen.rpc.client.spi.route.LoadBalancePolicy;

import java.util.concurrent.atomic.AtomicInteger;

public class PollingLoadBalancePolicy implements LoadBalancePolicy
{
    public static final LoadBalancePolicy INSTANCE = new PollingLoadBalancePolicy();

    /**
     * 计数器
     */
    private final AtomicInteger indexer = new AtomicInteger(0);

    private PollingLoadBalancePolicy()
    {
    }

    @Override
    public int use()
    {
        return indexer.getAndIncrement();
    }
}
