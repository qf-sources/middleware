package io.qiufen.rpc.client.spi.route;

import io.qiufen.common.provider.Provider;

public interface LoadBalancePolicy extends Provider
{
    int use();
}
