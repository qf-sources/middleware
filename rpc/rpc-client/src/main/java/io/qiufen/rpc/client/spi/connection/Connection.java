package io.qiufen.rpc.client.spi.connection;

import io.qiufen.rpc.client.spi.exception.ClientSideException;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;

public interface Connection
{
    void send(RPCRequestSetter setter, long readTimeout, Feedback feedback) throws ClientSideException;

    void close() throws ClientSideException;

    boolean isClosed();
}
