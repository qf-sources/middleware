package io.qiufen.rpc.client.spi.pool;

import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.exception.ClientSideException;

public interface ConnectionPool
{
    Connection getConnection() throws ClientSideException;

    void close();

    void addListener(NetEventListener listener);
}
