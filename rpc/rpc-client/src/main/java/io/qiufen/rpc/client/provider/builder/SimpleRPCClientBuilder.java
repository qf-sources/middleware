package io.qiufen.rpc.client.provider.builder;

import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.spi.Host;

public class SimpleRPCClientBuilder extends AbstractRPCClientBuilder
{
    private Host host;

    private int maxUsage;

    public SimpleRPCClientBuilder setHost(Host host)
    {
        this.host = host;
        return this;
    }

    public SimpleRPCClientBuilder setMaxUsage(int maxUsage)
    {
        this.maxUsage = maxUsage;
        return this;
    }

    @Override
    protected RPCClient build0()
    {
        if (host == null) throw new IllegalArgumentException("Need host.");

        return new SimpleRPCClient(providerContext).setHost(host).setMaxUsage(maxUsage).init();
    }
}
