package io.qiufen.rpc.client.provider.connection;

import io.netty.channel.Channel;
import io.qiufen.common.collection.MapUtil;
import io.qiufen.rpc.client.spi.exception.ClientSideException;
import io.qiufen.rpc.client.spi.exception.RPCInterruptedException;
import io.qiufen.rpc.client.spi.exception.RPCTimeoutException;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class FeedbackRegistry
{
    public static final FeedbackRegistry INSTANCE = new FeedbackRegistry();

    private static final Logger log = LoggerFactory.getLogger(FeedbackRegistry.class);

    private final Map<Channel, Map<Integer, FeedbackEntry>> feedbackMap;

    private final Sequence sequence = new DefaultSequence();

    private FeedbackRegistry()
    {
        feedbackMap = new ConcurrentHashMap<Channel, Map<Integer, FeedbackEntry>>();
        initMonitor();
    }

    public int getSequence() {return sequence.get();}

    public void initChannel(Channel channel)
    {
        this.feedbackMap.put(channel, new ConcurrentHashMap<Integer, FeedbackEntry>());
    }

    public void removeChannel(Channel channel)
    {
        Map<Integer, FeedbackEntry> feedbackEntryMap = this.feedbackMap.remove(channel);
        if (MapUtil.isEmpty(feedbackEntryMap)) return;

        for (Map.Entry<Integer, FeedbackEntry> entry : feedbackEntryMap.entrySet())
        {
            try
            {
                entry.getValue().getFeedback().onResponse(new RPCInterruptedException("sequence:" + entry.getKey()));
            }
            catch (Exception e)
            {
                log.error(e.toString(), e);
            }
        }
    }

    public void addFeedback(Channel channel, int sequence, long readTimeout, Feedback feedback)
            throws ClientSideException
    {
        Map<Integer, FeedbackEntry> feedbackEntryMap = this.feedbackMap.get(channel);
        if (feedbackEntryMap == null) throw new ClientSideException("Connection maybe closed!");

        long timestamp = readTimeout <= 0 ? 0 : System.currentTimeMillis();
        feedbackEntryMap.put(sequence, new FeedbackEntry(readTimeout, timestamp, feedback));
    }

    public Feedback getFeedback(Channel channel, int sequence)
    {
        Map<Integer, FeedbackEntry> feedbackEntryMap = this.feedbackMap.get(channel);
        if (feedbackEntryMap == null) return null;

        FeedbackEntry entry = feedbackEntryMap.remove(sequence);
        return entry == null ? null : entry.getFeedback();
    }

    private void notifyTimeout()
    {
        for (Map.Entry<Channel, Map<Integer, FeedbackEntry>> entry : feedbackMap.entrySet())
        {
            Iterator<Map.Entry<Integer, FeedbackEntry>> iterator = entry.getValue().entrySet().iterator();
            while (iterator.hasNext())
            {
                Map.Entry<Integer, FeedbackEntry> entry0 = iterator.next();
                FeedbackEntry feedbackEntry = entry0.getValue();
                long readTimeout = feedbackEntry.getReadTimeout();
                if (readTimeout <= 0) continue;
                if (feedbackEntry.getTimestamp() + readTimeout < System.currentTimeMillis())
                {
                    iterator.remove();
                    String message = "Max read timeout:" + readTimeout + "ms,sequence:" + entry0.getKey();
                    try
                    {
                        feedbackEntry.getFeedback().onResponse(new RPCTimeoutException(message));
                    }
                    catch (Exception e)
                    {
                        log.error(e.toString(), e);
                    }
                }
            }
        }
    }

    private void initMonitor()
    {
        //todo 检查是否有关闭标识，存在则发送close指令
        new Timer("monitor-feedback-timeout").scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                try
                {
                    FeedbackRegistry.this.notifyTimeout();
                }
                catch (Exception e)
                {
                    log.error(e.toString(), e);
                }
            }
        }, 500, 500);
    }

    private static class FeedbackEntry
    {
        private final long readTimeout;
        private final long timestamp;
        private final Feedback feedback;

        private FeedbackEntry(long readTimeout, long timestamp, Feedback feedback)
        {
            this.readTimeout = readTimeout;
            this.timestamp = timestamp;
            this.feedback = feedback;
        }

        private long getReadTimeout()
        {
            return readTimeout;
        }

        private long getTimestamp()
        {
            return timestamp;
        }

        private Feedback getFeedback()
        {
            return feedback;
        }
    }

    private static class DefaultSequence implements Sequence
    {
        private final AtomicInteger seq = new AtomicInteger(0);

        private DefaultSequence()
        {
        }

        @Override
        public int get()
        {
            return seq.getAndIncrement();
        }
    }
}
