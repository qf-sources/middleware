package io.qiufen.rpc.client.spi.rpc;

import io.qiufen.rpc.common.rpc.BufferLoader;

public interface RPCResponse
{
    <T> T getHeader(BufferLoader<T> loader);

    <T> T getData(BufferLoader<T> loader);
}
