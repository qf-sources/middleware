package io.qiufen.rpc.client.spi.rpc;

public interface RPCRequestSetter
{
    void set(RPCRequest request);
}
