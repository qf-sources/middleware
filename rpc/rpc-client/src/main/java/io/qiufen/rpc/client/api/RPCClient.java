package io.qiufen.rpc.client.api;

import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;
import io.qiufen.rpc.client.spi.rpc.RPCResponse;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/11/4 10:49
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface RPCClient
{
    RPCClient addListener(NetEventListener listener);

    void send(RPCRequestSetter setter, long readTimeout, Feedback feedback) throws Exception;

    void send(RPCRequestSetter setter, Feedback feedback) throws Exception;

    RPCResponse send(RPCRequestSetter setter, long readTimeout) throws Exception;
}
