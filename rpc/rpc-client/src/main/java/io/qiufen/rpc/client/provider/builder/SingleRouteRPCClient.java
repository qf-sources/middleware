package io.qiufen.rpc.client.provider.builder;

import io.qiufen.common.provider.DefaultProviderContext;
import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.pool.ConnectionPool;
import io.qiufen.rpc.client.spi.pool.ConnectionPoolProvider;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;

class SingleRouteRPCClient extends AbstractRPCClient
{
    private final DefaultProviderContext providerContext;

    private Host host;

    private ConnectionPool connectionPool;

    SingleRouteRPCClient(DefaultProviderContext providerContext)
    {
        this.providerContext = providerContext;
    }

    SingleRouteRPCClient init()
    {
        ConnectionPoolProvider connectionPoolProvider = providerContext.getProvider(ConnectionPoolProvider.class);
        this.connectionPool = connectionPoolProvider.provide(providerContext, host);
        return this;
    }

    @Override
    public RPCClient addListener(NetEventListener listener)
    {
        connectionPool.addListener(listener);
        return this;
    }

    @Override
    public void send(RPCRequestSetter setter, long readTimeout, Feedback feedback) throws Exception
    {
        Connection connection = connectionPool.getConnection();
        try
        {
            connection.send(setter, readTimeout, feedback);
        }
        finally
        {
            connection.close();
        }
    }

    SingleRouteRPCClient setHost(Host host)
    {
        this.host = host;
        return this;
    }
}
