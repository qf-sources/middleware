package io.qiufen.rpc.client.spi.infrastructure;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInboundHandler;
import io.qiufen.common.provider.Provider;

public interface ClientNetService extends Provider
{
    Channel connect(String host, int port, ChannelInboundHandler handler) throws InterruptedException;

    void disconnect(Channel channel);
}
