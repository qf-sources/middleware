package io.qiufen.rpc.client.provider.constant;

import io.qiufen.common.net.ChannelMonitor;

public class BaseConstants
{
    /**
     * 报文最大帧：默认2048KB
     */
    public static final int MAX_FRAME_LENGTH = 2097152;
    public static final ChannelMonitor CHANNEL_MONITOR = new ChannelMonitor();
    private BaseConstants()
    {
    }
}
