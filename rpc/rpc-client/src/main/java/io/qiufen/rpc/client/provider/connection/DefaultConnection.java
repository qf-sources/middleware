package io.qiufen.rpc.client.provider.connection;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.CompositeByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.net.SendResult;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.provider.constant.BaseConstants;
import io.qiufen.rpc.client.spi.RPCClientConfig;
import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.exception.ClientSideException;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import io.qiufen.rpc.client.spi.rpc.RPCRequest;
import io.qiufen.rpc.client.spi.rpc.RPCRequestProvider;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;
import io.qiufen.rpc.common.exception.RPCException;
import io.qiufen.rpc.common.net.Command;
import io.qiufen.rpc.common.net.DataPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.ClosedChannelException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.LockSupport;

/**
 * 链接
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class DefaultConnection implements Connection
{
    private static final Logger log = LoggerFactory.getLogger(Connection.class);

    private final FeedbackRegistry registry = FeedbackRegistry.INSTANCE;

    private final Channel channel;

    private final RPCRequestProvider<ByteBuf> rpcRequestProvider;

    private final RPCClientConfig config;

    private volatile boolean closed = false;

    DefaultConnection(ProviderContext providerContext, Channel channel)
    {
        this.channel = channel;

        this.rpcRequestProvider = providerContext.getProvider(RPCRequestProvider.class);

        this.config = providerContext.getProvider(RPCClientConfig.class);
    }

    public void send(RPCRequestSetter setter, long readTimeout, Feedback feedback) throws ClientSideException
    {
        if (log.isTraceEnabled())
        {
            log.trace("Send data...");
        }
        List<ByteBuf> sendingUri = new ArrayList<ByteBuf>();
        List<ByteBuf> sendingHeader = new ArrayList<ByteBuf>();
        List<ByteBuf> sendingData = new ArrayList<ByteBuf>();
        RPCRequest request = rpcRequestProvider.create(sendingUri, sendingHeader, sendingData);
        setter.set(request);

        int seq = registry.getSequence();
        byte cmd = Command.REQUEST.getCode();

        ByteBufAllocator allocator = channel.alloc();
        ByteBuf uri = toBuf(allocator, sendingUri);
        ByteBuf header = toBuf(allocator, sendingHeader);
        ByteBuf data = toBuf(allocator, sendingData);

        sendingUri.clear();
        sendingHeader.clear();
        sendingData.clear();

        DataPacket packet = new DataPacket(cmd, seq, uri, header, data);
        log.debug("Send data packet{length:{},cmd:{},sequence:{}}", packet.getLength(), packet.getCmd(),
                packet.getSequence());
        readTimeout = readTimeout <= 0 ? config.getReadTimeoutMills() : readTimeout;
        registry.addFeedback(channel, seq, readTimeout, feedback);

        try
        {
            send(packet.toBuffer());
        }
        catch (ClosedChannelException e)
        {
            throw new ClientSideException(e);
        }
        catch (InterruptedException e)
        {
            throw new ClientSideException(e);
        }
    }

    private void send(ByteBuf buf) throws ClosedChannelException, InterruptedException
    {
        try
        {
            SendResult result = BaseConstants.CHANNEL_MONITOR.send(channel, buf.retain());
            if (result == SendResult.SUCCESS)
            {
                log.debug("Send success:{}", channel);
            }
            else if (result == SendResult.CH_INACTIVE)
            {
                throw new ClosedChannelException();
            }
            else
            {
                throw new IllegalStateException("write failure,channel state:" + result + ",channel:" + channel);
            }
        }
        finally
        {
            buf.release();
        }
    }

    @Override
    public void close()
    {
        if (isClosed())
        {
            throw new RPCException("Connection closed already,channel:" + channel);
        }

        this.closed = true;
        log.debug("Start ending connection:{}", this);
        byte cmd = Command.END.getCode();
        int seq = 0;
        DataPacket packet = new DataPacket(cmd, seq, Unpooled.EMPTY_BUFFER, Unpooled.EMPTY_BUFFER,
                Unpooled.EMPTY_BUFFER);
        channel.writeAndFlush(packet.toBuffer());
    }

    @Override
    public boolean isClosed()
    {
        return this.closed || !this.channel.isActive();
    }

    @Override
    public String toString()
    {
        return "Connection{" + "channel=" + channel + '}';
    }

    private ByteBuf toBuf(ByteBufAllocator allocator, List<ByteBuf> bufList)
    {
        if (CollectionUtil.isEmpty(bufList)) return Unpooled.EMPTY_BUFFER;

        if (bufList.size() == 1) return bufList.get(0);

        CompositeByteBuf compositeByteBuf = allocator.compositeBuffer(bufList.size());
        for (ByteBuf buf : bufList)
        {
            compositeByteBuf.addComponent(true, buf);
        }
        return compositeByteBuf;
    }
}
