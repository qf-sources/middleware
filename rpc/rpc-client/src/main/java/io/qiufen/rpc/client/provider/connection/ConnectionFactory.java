package io.qiufen.rpc.client.provider.connection;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.util.AttributeKey;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.connection.ConnectionProvider;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.connection.NetEventListenerAdapter;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import io.qiufen.rpc.common.net.Command;
import io.qiufen.rpc.common.net.DataPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ConnectionFactory
{
    public static final AttributeKey<Connection> KEY_CONN = AttributeKey.valueOf(Connection.class.getName());

    private static final Logger log = LoggerFactory.getLogger(ConnectionFactory.class);

    private static final AtomicInteger SESSION_SEQUENCE = new AtomicInteger(0);

    private static final FeedbackRegistry REGISTRY = FeedbackRegistry.INSTANCE;

    /**
     * 进程ID
     */
    private static final int PROCESS_ID;

    static
    {
        String processName = ManagementFactory.getRuntimeMXBean().getName();
        PROCESS_ID = Integer.parseInt(processName.substring(0, processName.indexOf('@')));
    }

    private final short sessionSequence;
    private final ProviderContext providerContext;
    private final ClientEventHandler handler;
    private final ConnectionProvider connectionProvider;
    private final ClientNetService clientNetService;

    private final Map<Channel, Semaphore> locks;

    public ConnectionFactory(ProviderContext providerContext)
    {
        this.providerContext = providerContext;

        this.handler = new ClientEventHandler(providerContext);
        this.connectionProvider = providerContext.getProvider(ConnectionProvider.class);
        this.clientNetService = providerContext.getProvider(ClientNetService.class);

        this.sessionSequence = (short) SESSION_SEQUENCE.getAndIncrement();

        this.locks = new ConcurrentHashMap<Channel, Semaphore>();

        this.handler.addListener(new ReadyEventListener(this.locks));
        //监听网络断开事件，移除注册回调
        this.handler.addListener(new FeedbackRegistryEventListener(REGISTRY));
    }

    public Connection create(String ip, int port) throws InterruptedException
    {
        // step1. build channel between client and server
        Channel channel = clientNetService.connect(ip, port, handler);
        REGISTRY.initChannel(channel);

        // step2. prepare channel
        Semaphore lock = new Semaphore(0);
        this.locks.put(channel, lock);
        prepare(channel);
        lock.acquire(1);//todo 异常情况，比如超时

        // step3. build connection
        Connection connection = connectionProvider.provide(providerContext, channel);
        channel.attr(KEY_CONN).set(connection);
        return connection;
    }

    private void prepare(Channel channel)
    {
        log.info("Request preparing channel:{}", channel);
        byte cmd = Command.PREPARE.getCode();
        int seq = 0;
        // 会话ID = 进程ID+会话序列
        ByteBuf sessionIdBuffer = channel.alloc()
                .buffer(8)
                .writeShort(0)
                .writeInt(PROCESS_ID)
                .writeShort(sessionSequence);
        if (log.isInfoEnabled())
        {
            long sessionId = sessionIdBuffer.readLong();
            sessionIdBuffer.resetReaderIndex();
            log.info("Prepare channel for session,sessionId:{},channel:{}", sessionId, channel);
        }

        DataPacket packet = new DataPacket(cmd, seq, Unpooled.EMPTY_BUFFER, Unpooled.EMPTY_BUFFER, sessionIdBuffer);
        channel.writeAndFlush(packet.toBuffer());
    }

    public void addListener(NetEventListener listener)
    {
        handler.addListener(listener);
    }

    private static class ReadyEventListener extends NetEventListenerAdapter
    {
        private final Map<Channel, Semaphore> locks;

        private ReadyEventListener(Map<Channel, Semaphore> locks) {this.locks = locks;}

        @Override
        public void onReady(Channel channel)
        {
            locks.get(channel).release(1);
        }

        @Override
        public void onDisconnect(Channel channel)
        {
            Semaphore lock = locks.remove(channel);
            if (lock != null) lock.release(1);
        }
    }

    private static class FeedbackRegistryEventListener extends NetEventListenerAdapter
    {
        private final FeedbackRegistry registry;

        private FeedbackRegistryEventListener(FeedbackRegistry registry) {this.registry = registry;}

        @Override
        public void onDisconnect(Channel channel)
        {
            registry.removeChannel(channel);
        }
    }
}