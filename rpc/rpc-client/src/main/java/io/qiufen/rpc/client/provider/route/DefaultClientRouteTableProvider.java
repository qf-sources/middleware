package io.qiufen.rpc.client.provider.route;

import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.spi.route.ClientRouteTable;
import io.qiufen.rpc.client.spi.route.ClientRouteTableProvider;

public class DefaultClientRouteTableProvider implements ClientRouteTableProvider
{
    public static final ClientRouteTableProvider INSTANCE = new DefaultClientRouteTableProvider();

    private DefaultClientRouteTableProvider()
    {
    }

    @Override
    public ClientRouteTable provide(ProviderContext providerContext)
    {
        return new DefaultClientRouteTable(providerContext);
    }
}
