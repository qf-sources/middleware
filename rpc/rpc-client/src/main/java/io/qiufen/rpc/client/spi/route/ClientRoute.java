package io.qiufen.rpc.client.spi.route;

import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.pool.ConnectionPool;

/**
 * 网络客户端路由
 */
public class ClientRoute
{
    private final Host host;
    private final ConnectionPool connectionPool;

    public ClientRoute(Host host, ConnectionPool connectionPool)
    {
        this.host = host;
        this.connectionPool = connectionPool;
    }

    public Host getHost()
    {
        return host;
    }

    public ConnectionPool getConnectionPool()
    {
        return connectionPool;
    }

    public void close()
    {
        this.connectionPool.close();
    }
}
