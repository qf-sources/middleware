package io.qiufen.rpc.client.spi.route;


import io.qiufen.common.provider.Provider;
import io.qiufen.common.provider.ProviderContext;

public interface ClientRouteTableProvider extends Provider
{
    ClientRouteTable provide(ProviderContext providerContext);
}
