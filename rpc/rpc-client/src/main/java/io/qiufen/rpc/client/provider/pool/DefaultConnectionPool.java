package io.qiufen.rpc.client.provider.pool;

import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.RPCClientConfig;
import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.exception.ClientSideException;
import io.qiufen.rpc.client.spi.pool.ConnectionPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

/**
 * 链接池
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class DefaultConnectionPool implements ConnectionPool
{
    private final ProviderContext providerContext;

    private final Host host;
    private final RPCClientConfig config;
    private GenericObjectPoolConfig poolConfig;
    private PooledConnectionFactory factory;
    private GenericObjectPool<PooledConnection> pool;

    DefaultConnectionPool(ProviderContext providerContext, Host host)
    {
        this.providerContext = providerContext;
        this.config = providerContext.getProvider(RPCClientConfig.class);
        this.host = host;
    }

    public Host getHost()
    {
        return host;
    }

    public Connection getConnection() throws ClientSideException
    {
        try
        {
            return pool.borrowObject();
        }
        catch (ClientSideException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new ClientSideException(e);
        }
    }

    public void close()
    {
        this.pool.close();
    }

    public DefaultConnectionPool applyConfig()
    {
        if (null == this.poolConfig)
        {
            this.poolConfig = new GenericObjectPoolConfig();
            setPoolConfig();
            this.factory = new PooledConnectionFactory(providerContext, host);
            this.pool = new GenericObjectPool<PooledConnection>(factory, this.poolConfig);
            factory.setPool(this.pool);
            return this;
        }
        setPoolConfig();
        return this;
    }

    public void addListener(NetEventListener listener)
    {
        this.factory.addListener(listener);
    }

    private void setPoolConfig()
    {
        this.poolConfig.setMaxTotal(this.config.getMaxConnections());
        this.poolConfig.setMaxIdle(this.config.getMaxConnections());
        this.poolConfig.setMinIdle(this.config.getInitConnections());
        this.poolConfig.setTimeBetweenEvictionRunsMillis(1000 * 60 * 5);//5分钟
        this.poolConfig.setMinEvictableIdleTimeMillis(1000 * 60 * 5);//5分钟
        this.poolConfig.setNumTestsPerEvictionRun(5);
    }

    @Override
    public String toString()
    {
        return "ConnectionPool{" + "host=" + host + '}';
    }
}
