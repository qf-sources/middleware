package io.qiufen.rpc.client.provider.builder;

import io.qiufen.common.provider.DefaultProviderContext;
import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.provider.connection.ConnectionFactory;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.connection.NetEventListenerAdapter;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;
import io.qiufen.rpc.client.spi.rpc.RPCResponse;
import io.qiufen.rpc.common.exception.RPCException;

class SimpleRPCClient extends AbstractRPCClient
{
    private final DefaultProviderContext providerContext;

    private Host host;

    private ConnectionFactory factory;

    private int maxUsage = 0;

    private int usage = 0;

    private volatile Connection connection = null;

    SimpleRPCClient(DefaultProviderContext providerContext)
    {
        this.providerContext = providerContext;
    }

    SimpleRPCClient init()
    {
        factory = new ConnectionFactory(providerContext);

        factory.addListener(new NetEventListenerAdapter()
        {
            @Override
            public void onDisconnect(Host local, Host remote)
            {
                connection = null;
                usage = 0;
            }
        });
        return this;
    }

    @Override
    public RPCClient addListener(NetEventListener listener)
    {
        factory.addListener(listener);
        return this;
    }

    @Override
    public void send(RPCRequestSetter setter, long readTimeout, Feedback feedback) throws Exception
    {
        Connection connection = getConnection();
        if (this.maxUsage <= 0)
        {
            connection.send(setter, readTimeout, feedback);
            return;
        }

        synchronized (this)
        {
            this.usage++;
            if (usage < this.maxUsage)
            {
                connection.send(setter, readTimeout, feedback);
            }
            else if (usage == this.maxUsage)
            {
                connection.send(setter, readTimeout, new ClosableFeedback(connection, feedback));
                this.usage = 0;
                this.connection = null;
            }
        }
    }

    private Connection getConnection() throws InterruptedException
    {
        if (connection != null) return connection;
        synchronized (this)
        {
            if (connection != null) return connection;
            connection = factory.create(host.getIp(), host.getPort());
            return connection;
        }
    }

    SimpleRPCClient setHost(Host host)
    {
        this.host = host;
        return this;
    }

    SimpleRPCClient setMaxUsage(int maxUsage)
    {
        this.maxUsage = maxUsage;
        return this;
    }

    private static class ClosableFeedback implements Feedback
    {
        private final Connection connection;
        private final Feedback feedback;

        private ClosableFeedback(Connection connection, Feedback feedback)
        {
            this.connection = connection;
            this.feedback = feedback;
        }

        @Override
        public void onResponse(RPCResponse response) throws Exception
        {
            try
            {
                feedback.onResponse(response);
            }
            finally
            {
                connection.close();
            }
        }

        @Override
        public void onResponse(RPCException e) throws Exception
        {
            try
            {
                feedback.onResponse(e);
            }
            finally
            {
                connection.close();
            }
        }
    }
}
