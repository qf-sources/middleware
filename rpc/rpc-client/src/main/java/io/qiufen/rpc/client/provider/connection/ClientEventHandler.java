package io.qiufen.rpc.client.provider.connection;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.context.RPCContext;
import io.qiufen.rpc.client.spi.context.RPCContextProvider;
import io.qiufen.rpc.client.spi.dispatcher.RPCContextDispatcher;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import io.qiufen.rpc.common.net.Command;
import io.qiufen.rpc.common.net.DataPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
@ChannelHandler.Sharable
public class ClientEventHandler extends ChannelInboundHandlerAdapter
{
    private static final Logger log = LoggerFactory.getLogger(ClientEventHandler.class);

    private final List<NetEventListener> netEventListenerList;

    private final RPCContextProvider rpcContextProvider;
    private final RPCContextDispatcher rpcContextDispatcher;
    private final ClientNetService clientNetService;

    ClientEventHandler(ProviderContext providerContext)
    {
        this.rpcContextProvider = providerContext.getProvider(RPCContextProvider.class);
        this.rpcContextDispatcher = providerContext.getProvider(RPCContextDispatcher.class);
        this.clientNetService = providerContext.getProvider(ClientNetService.class);

        this.netEventListenerList = new ArrayList<NetEventListener>();
    }

    @Override
    public final void channelActive(ChannelHandlerContext ctx) throws Exception
    {
        Channel channel = ctx.channel();
        onConnect(channel);
        this.rpcContextDispatcher.init(channel);
        super.channelActive(ctx);
    }

    @Override
    public final void channelRead(final ChannelHandlerContext ctx, Object msg)
    {
        final DataPacket packet;
        try
        {
            packet = DataPacket.fromBuffer(((ByteBuf) msg).retain());
        }
        finally
        {
            ReferenceCountUtil.release(msg);
        }
        log.debug("Receive data packet{length:{},cmd:{},sequence:{}}", packet.getLength(), packet.getCmd(),
                packet.getSequence());

        try
        {
            Channel connection = ctx.channel();
            if (packet.getCmd() == Command.READY.getCode())
            {
                //链接就绪
                log.info("Connection prepared success:{}", connection);
                onReady(connection);
            }
            else if (packet.getCmd() == Command.END.getCode())
            {
                //关闭链接
                ctx.close();
                log.info("Connection ended success:{}", connection);
            }
            else if (packet.getCmd() == Command.RESPONSE.getCode())
            {
                RPCContext rpcContext = rpcContextProvider.provide(connection, packet.retain());
                rpcContextDispatcher.dispatch(rpcContext);
            }
            else
            {
                log.info("Unknown cmd:{}, ignore!", packet.getCmd());
            }
        }
        finally
        {
            packet.release();
        }
    }

    @Override
    public final void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    {
        log.warn(cause.toString(), cause);
    }

    @Override
    public final void channelInactive(ChannelHandlerContext ctx) throws Exception
    {
        Channel channel = ctx.channel();

        clientNetService.disconnect(channel);
        onDisconnect(channel);

        rpcContextDispatcher.close(channel);

        super.channelInactive(ctx);
    }

    void addListener(NetEventListener listener)
    {
        this.netEventListenerList.add(listener);
    }

    private void onConnect(Channel connection)
    {
        if (CollectionUtil.isEmpty(netEventListenerList))
        {
            return;
        }
        InetSocketAddress localAddress = (InetSocketAddress) connection.localAddress();
        InetSocketAddress remoteAddress = (InetSocketAddress) connection.remoteAddress();
        Host local = new Host(localAddress.getAddress().getHostAddress(), localAddress.getPort());
        Host remote = new Host(remoteAddress.getAddress().getHostAddress(), remoteAddress.getPort());
        for (NetEventListener listener : netEventListenerList)
        {
            try
            {
                listener.onConnect(local, remote);
            }
            catch (Throwable e)
            {
                log.error(e.toString(), e);
            }
        }

        for (NetEventListener listener : netEventListenerList)
        {
            try
            {
                listener.onConnect(connection);
            }
            catch (Throwable e)
            {
                log.error(e.toString(), e);
            }
        }
    }

    private void onDisconnect(Channel connection)
    {
        if (CollectionUtil.isEmpty(netEventListenerList))
        {
            return;
        }
        InetSocketAddress localAddress = (InetSocketAddress) connection.localAddress();
        InetSocketAddress remoteAddress = (InetSocketAddress) connection.remoteAddress();
        Host local = new Host(localAddress.getAddress().getHostAddress(), localAddress.getPort());
        Host remote = new Host(remoteAddress.getAddress().getHostAddress(), remoteAddress.getPort());
        for (NetEventListener listener : netEventListenerList)
        {
            try
            {
                listener.onDisconnect(local, remote);
                listener.onDisconnect(connection);
            }
            catch (Throwable e)
            {
                log.error(e.toString(), e);
            }
        }
    }

    private void onReady(Channel connection)
    {
        if (CollectionUtil.isEmpty(netEventListenerList))
        {
            return;
        }
        for (NetEventListener listener : netEventListenerList)
        {
            try
            {
                listener.onReady(connection);
            }
            catch (Throwable e)
            {
                log.error(e.toString(), e);
            }
        }
    }
}