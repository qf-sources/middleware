package io.qiufen.rpc.client.provider.route;

import io.qiufen.common.lang.Numbers;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rpc.client.spi.exception.ClientSideException;
import io.qiufen.rpc.client.spi.pool.ConnectionPool;
import io.qiufen.rpc.client.spi.pool.ConnectionPoolProvider;
import io.qiufen.rpc.client.spi.route.ClientRoute;
import io.qiufen.rpc.client.spi.route.ClientRouteTable;
import io.qiufen.rpc.client.spi.route.LoadBalancePolicy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * 网络客户端路由表
 */
class DefaultClientRouteTable implements ClientRouteTable
{
    private final List<NetEventListener> netEventListenerList;

    private final ProviderContext providerContext;

    private final ConnectionPoolProvider connectionPoolProvider;

    /**
     * 路由列表
     */
    private final List<ClientRoute> routes = new ArrayList<ClientRoute>();

    private final LoadBalancePolicy loadBalancePolicy;

    DefaultClientRouteTable(ProviderContext providerContext)
    {
        this.providerContext = providerContext;
        this.netEventListenerList = new ArrayList<NetEventListener>();

        this.connectionPoolProvider = providerContext.getProvider(ConnectionPoolProvider.class);

        this.loadBalancePolicy = providerContext.getProvider(LoadBalancePolicy.class);
    }

    /**
     * 命中客户端路由
     */
    public ClientRoute hitRoute() throws ClientSideException
    {
        int size = routes.size();
        if (routes.isEmpty()) throw new ClientSideException("None available client route");
        long unSize = Numbers.toUnsigned(this.loadBalancePolicy.use());
        int n = (int) (unSize % size);
        return routes.get(n);
    }

    public void addListener(NetEventListener listener)
    {
        this.netEventListenerList.add(listener);
        for (ClientRoute route : routes)
        {
            route.getConnectionPool().addListener(listener);
        }
    }

    public void setHost(Set<Host> hostSet)
    {
        synchronized (routes)
        {
            Iterator<ClientRoute> iterator = routes.iterator();
            while (iterator.hasNext())
            {
                ClientRoute route = iterator.next();
                Host host = route.getHost();
                if (hostSet.contains(host))
                {
                    hostSet.remove(host);
                    continue;//已经存在
                }
                iterator.remove();
                route.close();
            }
            for (Host host : hostSet)
            {
                addRoute(host);
            }
        }
    }

    @Override
    public int getRouteCount()
    {
        return routes.size();
    }

    private void addRoute(Host host)
    {
        ConnectionPool connectionPool = connectionPoolProvider.provide(providerContext, host);
        for (NetEventListener listener : netEventListenerList)
        {
            connectionPool.addListener(listener);
        }
        for (int i = 0; i < host.getWeight(); i++)
        {
            routes.add(new ClientRoute(host, connectionPool));
        }
    }
}
