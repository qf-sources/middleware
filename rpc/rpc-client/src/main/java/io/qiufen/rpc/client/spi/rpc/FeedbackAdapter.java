package io.qiufen.rpc.client.spi.rpc;

import io.qiufen.rpc.common.exception.RPCException;

public class FeedbackAdapter implements Feedback
{
    @Override
    public void onResponse(RPCResponse response) throws Exception
    {
    }

    @Override
    public void onResponse(RPCException e) throws Exception
    {
        throw e;
    }
}
