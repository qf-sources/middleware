package io.qiufen.rpc.client.provider.rpc;

import io.netty.buffer.ByteBuf;
import io.qiufen.rpc.client.spi.rpc.RPCRequest;
import io.qiufen.rpc.common.rpc.BufferWriter;

import java.util.List;

class DefaultRPCRequest implements RPCRequest
{
    private final List<ByteBuf> sendingUri;
    private final List<ByteBuf> sendingHeader;
    private final List<ByteBuf> sendingData;

    DefaultRPCRequest(List<ByteBuf> sendingUri, List<ByteBuf> sendingHeader, List<ByteBuf> sendingData)
    {
        this.sendingUri = sendingUri;
        this.sendingHeader = sendingHeader;
        this.sendingData = sendingData;
    }

    @Override
    public <T> void writeUri(BufferWriter<T> writer, T t)
    {
        sendingUri.add((ByteBuf) writer.to(t));
    }

    @Override
    public <T> void writeHeader(BufferWriter<T> writer, T t)
    {
        sendingHeader.add((ByteBuf) writer.to(t));
    }

    @Override
    public <T> void writeData(BufferWriter<T> writer, T t)
    {
        sendingData.add((ByteBuf) writer.to(t));
    }
}
