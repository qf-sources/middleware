package io.qiufen.rpc.client.spi.context;

import io.netty.channel.Channel;
import io.qiufen.rpc.client.spi.rpc.RPCResponse;
import io.qiufen.rpc.common.net.DataPacket;

import java.io.Closeable;

public interface RPCContext extends Closeable
{
    Channel getChannel();

    DataPacket getPacket();

    RPCResponse getResponse();
}
