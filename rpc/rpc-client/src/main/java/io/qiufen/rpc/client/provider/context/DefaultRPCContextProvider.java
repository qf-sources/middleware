package io.qiufen.rpc.client.provider.context;

import io.netty.channel.Channel;
import io.qiufen.rpc.client.spi.context.RPCContext;
import io.qiufen.rpc.client.spi.context.RPCContextProvider;
import io.qiufen.rpc.common.net.DataPacket;

public class DefaultRPCContextProvider implements RPCContextProvider
{
    public static final RPCContextProvider INSTANCE = new DefaultRPCContextProvider();

    private DefaultRPCContextProvider()
    {
    }

    @Override
    public RPCContext provide(Channel channel, DataPacket packet)
    {
        return new DefaultRPCContext(channel, packet);
    }
}
