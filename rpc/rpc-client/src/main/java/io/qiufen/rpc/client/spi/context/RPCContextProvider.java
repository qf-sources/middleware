package io.qiufen.rpc.client.spi.context;

import io.netty.channel.Channel;
import io.qiufen.common.provider.Provider;
import io.qiufen.rpc.common.net.DataPacket;

public interface RPCContextProvider extends Provider
{
    RPCContext provide(Channel channel, DataPacket packet);
}
