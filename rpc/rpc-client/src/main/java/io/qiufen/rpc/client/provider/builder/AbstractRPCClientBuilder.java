package io.qiufen.rpc.client.provider.builder;

import io.qiufen.common.provider.DefaultProviderContext;
import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.provider.connection.DefaultConnectionProvider;
import io.qiufen.rpc.client.provider.context.DefaultRPCContextProvider;
import io.qiufen.rpc.client.provider.infrastructure.DefaultClientNetService;
import io.qiufen.rpc.client.provider.pool.DefaultConnectionPoolProvider;
import io.qiufen.rpc.client.provider.rpc.DefaultRPCRequestProvider;
import io.qiufen.rpc.client.spi.RPCClientConfig;
import io.qiufen.rpc.client.spi.connection.ConnectionProvider;
import io.qiufen.rpc.client.spi.context.RPCContextProvider;
import io.qiufen.rpc.client.spi.dispatcher.RPCContextDispatcher;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import io.qiufen.rpc.client.spi.pool.ConnectionPoolProvider;
import io.qiufen.rpc.client.spi.rpc.RPCRequestProvider;

public abstract class AbstractRPCClientBuilder
{
    protected final DefaultProviderContext providerContext = new DefaultProviderContext();

    protected final RPCClientConfig clientConfig = defaultNetConfig();

    private ConnectionPoolProvider connectionPoolProvider;

    private ConnectionProvider connectionProvider;

    private RPCRequestProvider rpcRequestProvider;

    private RPCContextProvider rpcContextProvider;

    private RPCContextDispatcher rpcContextDispatcher;

    private ClientNetService clientNetService;

    private volatile boolean init = false;

    protected AbstractRPCClientBuilder()
    {
    }

    public RPCClientConfig getClientConfig()
    {
        return clientConfig;
    }

    public AbstractRPCClientBuilder setConnectionPoolProvider(ConnectionPoolProvider connectionPoolProvider)
    {
        this.connectionPoolProvider = connectionPoolProvider;
        return this;
    }

    public AbstractRPCClientBuilder setConnectionProvider(ConnectionProvider connectionProvider)
    {
        this.connectionProvider = connectionProvider;
        return this;
    }

    public AbstractRPCClientBuilder setRpcRequestProvider(RPCRequestProvider rpcRequestProvider)
    {
        this.rpcRequestProvider = rpcRequestProvider;
        return this;
    }

    public AbstractRPCClientBuilder setRpcContextProvider(RPCContextProvider rpcContextProvider)
    {
        this.rpcContextProvider = rpcContextProvider;
        return this;
    }

    public AbstractRPCClientBuilder setRpcContextDispatcher(RPCContextDispatcher rpcContextDispatcher)
    {
        this.rpcContextDispatcher = rpcContextDispatcher;
        return this;
    }

    public AbstractRPCClientBuilder setClientNetService(ClientNetService clientNetService)
    {
        this.clientNetService = clientNetService;
        return this;
    }

    public RPCClient build()
    {
        if (init)
        {
            throw new IllegalStateException("Rpc client has been built.");
        }

        init = true;

        providerContext.setProvider(RPCClientConfig.class, clientConfig);

        if (connectionPoolProvider == null) connectionPoolProvider = DefaultConnectionPoolProvider.INSTANCE;
        providerContext.setProvider(ConnectionPoolProvider.class, connectionPoolProvider);

        if (connectionProvider == null) connectionProvider = DefaultConnectionProvider.INSTANCE;
        providerContext.setProvider(ConnectionProvider.class, connectionProvider);

        if (rpcRequestProvider == null) rpcRequestProvider = DefaultRPCRequestProvider.INSTANCE;
        providerContext.setProvider(RPCRequestProvider.class, rpcRequestProvider);

        if (rpcContextProvider == null) rpcContextProvider = DefaultRPCContextProvider.INSTANCE;
        providerContext.setProvider(RPCContextProvider.class, rpcContextProvider);

        if (rpcContextDispatcher == null) throw new IllegalArgumentException("Need rpc context dispatcher.");
        providerContext.setProvider(RPCContextDispatcher.class, rpcContextDispatcher);

        if (clientNetService == null)
        {
            clientNetService = new DefaultClientNetService(Runtime.getRuntime().availableProcessors() << 1);
        }
        providerContext.setProvider(ClientNetService.class, clientNetService);

        return build0();
    }

    private RPCClientConfig defaultNetConfig()
    {
        int cpus = Runtime.getRuntime().availableProcessors();

        RPCClientConfig config = new RPCClientConfig();
        int initConnections = cpus >> 1;
        config.setInitConnections(Math.max(initConnections, 1));
        config.setMaxConnections(cpus << 2);
        config.setReadTimeoutMills(500);
        return config;
    }

    protected abstract RPCClient build0();
}
