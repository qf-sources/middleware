package io.qiufen.rpc.client.spi.exception;

import io.qiufen.rpc.common.exception.EndpointException;

public class ClientSideException extends EndpointException
{
    public ClientSideException(Exception e)
    {
        super(e);
    }

    public ClientSideException(String message)
    {
        super(message);
    }
}
