package io.qiufen.rpc.client.provider.context;

import io.qiufen.rpc.client.spi.rpc.RPCResponse;
import io.qiufen.rpc.common.net.DataPacket;
import io.qiufen.rpc.common.rpc.BufferLoader;

class DefaultRPCResponse implements RPCResponse
{
    private final DataPacket packet;

    DefaultRPCResponse(DataPacket packet)
    {
        this.packet = packet;
    }

    @Override
    public <T> T getHeader(BufferLoader<T> loader)
    {
        return loader.to(packet.getHeader().retain());
    }

    @Override
    public <T> T getData(BufferLoader<T> loader)
    {
        return loader.to(packet.getData().retain());
    }
}
