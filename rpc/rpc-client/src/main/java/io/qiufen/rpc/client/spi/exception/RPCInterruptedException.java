package io.qiufen.rpc.client.spi.exception;

import io.qiufen.rpc.common.exception.RPCException;

public class RPCInterruptedException extends RPCException
{
    public RPCInterruptedException(String message)
    {
        super(message);
    }
}
