package io.qiufen.rpc.client.spi.connection;

import io.netty.channel.Channel;
import io.qiufen.rpc.client.spi.Host;

public class NetEventListenerAdapter implements NetEventListener
{
    @Override
    public void onDisconnect(Host local, Host remote) throws Exception
    {

    }

    @Override
    public void onConnect(Host local, Host remote) throws Exception
    {

    }

    @Override
    public void onConnect(Channel channel) throws Exception
    {

    }

    @Override
    public void onDisconnect(Channel channel) throws Exception
    {

    }

    @Override
    public void onReady(Channel channel) throws Exception
    {

    }
}
