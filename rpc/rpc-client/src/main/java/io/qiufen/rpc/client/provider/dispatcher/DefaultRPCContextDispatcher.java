package io.qiufen.rpc.client.provider.dispatcher;

import io.netty.channel.Channel;
import io.netty.channel.ChannelConfig;
import io.qiufen.common.concurrency.executor.spi.Command;
import io.qiufen.common.io.IOUtil;
import io.qiufen.rpc.client.provider.connection.FeedbackRegistry;
import io.qiufen.rpc.client.spi.context.RPCContext;
import io.qiufen.rpc.client.spi.dispatcher.RPCContextDispatcher;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

public class DefaultRPCContextDispatcher implements RPCContextDispatcher
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultRPCContextDispatcher.class);

    private final Executor executor;
    private final int minWaterMarkerPerChannel;
    private final int maxWaterMarkerPerChannel;

    private final Map<Channel, AtomicInteger> waterMarkerMap;

    public DefaultRPCContextDispatcher(Executor executor, int minWaterMarkerPerChannel, int maxWaterMarkerPerChannel)
    {
        this.executor = executor;
        this.minWaterMarkerPerChannel = minWaterMarkerPerChannel;
        this.maxWaterMarkerPerChannel = maxWaterMarkerPerChannel;

        this.waterMarkerMap = new ConcurrentHashMap<Channel, AtomicInteger>();
    }

    @Override
    public void init(Channel channel)
    {
        waterMarkerMap.put(channel, new AtomicInteger(0));
    }

    @Override
    public void dispatch(RPCContext context)
    {
        try
        {
            addWaterMarker(context);
            Command command = new RPCContextCommand(context);
            executor.execute(command);
        }
        catch (Exception e)
        {
            LOGGER.error("Reject rpc context:{}.", context);
            doException(context, e);
            doCompleted(context);
        }
    }

    @Override
    public void close(Channel channel)
    {
        waterMarkerMap.remove(channel);
    }

    private void doException(RPCContext context, Exception exception)
    {
        LOGGER.error("context:{},exception:{}", context, exception);
    }

    private void doCompleted(RPCContext context)
    {
        try
        {
            removeWaterMarker(context);
        }
        finally
        {
            IOUtil.close(context);
        }
    }

    private void addWaterMarker(RPCContext context)
    {
        Channel channel = context.getChannel();
        ChannelConfig config = channel.config();
        AtomicInteger waterMarker = waterMarkerMap.get(channel);
        if (waterMarker != null && waterMarker.addAndGet(
                context.getPacket().getLength()) >= maxWaterMarkerPerChannel && config.isAutoRead())
        {
            LOGGER.debug("Reach high water marker:{},channel:{}, set auto read to false.", maxWaterMarkerPerChannel,
                    channel);
            config.setAutoRead(false);
        }
    }

    private void removeWaterMarker(RPCContext context)
    {
        Channel channel = context.getChannel();
        ChannelConfig config = channel.config();
        AtomicInteger waterMarker = waterMarkerMap.get(channel);
        if (waterMarker != null && waterMarker.addAndGet(
                -context.getPacket().getLength()) <= minWaterMarkerPerChannel && !config.isAutoRead())
        {
            LOGGER.debug("Reach low water marker:{},channel:{}, set auto read to true.", minWaterMarkerPerChannel,
                    channel);
            config.setAutoRead(true);
        }
    }

    private class RPCContextCommand extends Command
    {
        private final FeedbackRegistry registry = FeedbackRegistry.INSTANCE;

        private final RPCContext context;

        private RPCContextCommand(RPCContext context)
        {
            this.context = context;
        }

        @Override
        protected void doExecute() throws Exception
        {
            Feedback feedback = registry.getFeedback(context.getChannel(), context.getPacket().getSequence());
            if (feedback == null) return;
            feedback.onResponse(context.getResponse());
        }

        @Override
        protected void doException(Throwable throwable)
        {
            DefaultRPCContextDispatcher.this.doException(context, (Exception) throwable);
        }

        @Override
        protected void doFinally()
        {
            doCompleted(context);
        }
    }
}
