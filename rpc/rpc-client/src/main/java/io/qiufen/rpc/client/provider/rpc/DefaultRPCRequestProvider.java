package io.qiufen.rpc.client.provider.rpc;

import io.netty.buffer.ByteBuf;
import io.qiufen.rpc.client.spi.rpc.RPCRequest;
import io.qiufen.rpc.client.spi.rpc.RPCRequestProvider;

import java.util.List;

public class DefaultRPCRequestProvider implements RPCRequestProvider<ByteBuf>
{
    public static final DefaultRPCRequestProvider INSTANCE = new DefaultRPCRequestProvider();

    private DefaultRPCRequestProvider()
    {
    }

    @Override
    public RPCRequest create(List<ByteBuf> sendingUri, List<ByteBuf> sendingHeader, List<ByteBuf> sendingData)
    {
        return new DefaultRPCRequest(sendingUri, sendingHeader, sendingData);
    }
}
