package io.qiufen.rpc.client.provider.pool;

import io.qiufen.rpc.client.spi.connection.Connection;
import io.qiufen.rpc.client.spi.exception.ClientSideException;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;
import org.apache.commons.pool2.ObjectPool;

class PooledConnection implements Connection
{
    private final ObjectPool<PooledConnection> pool;
    private final Connection originConnection;

    PooledConnection(ObjectPool<PooledConnection> pool, Connection originConnection)
    {
        this.pool = pool;
        this.originConnection = originConnection;
    }

    Connection getOriginConnection()
    {
        return originConnection;
    }

    @Override
    public void send(RPCRequestSetter setter, long readTimeout, Feedback feedback) throws ClientSideException
    {
        this.originConnection.send(setter, readTimeout, feedback);
    }

    @Override
    public void close() throws ClientSideException
    {
        try
        {
            this.pool.returnObject(this);
        }
        catch (ClientSideException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new ClientSideException(e);
        }
    }

    @Override
    public boolean isClosed()
    {
        return this.originConnection.isClosed();
    }
}