package io.qiufen.rpc.client.spi;

/**
 * Created by Administrator on 2018/11/10.
 */
public class Host
{
    private final String ip;
    private final int port;

    /**
     * 权重，必须>=1
     */
    private final int weight;

    public Host(String ip, int port)
    {
        this(ip, port, 1);
    }

    public Host(String ip, int port, int weight)
    {
        this.ip = ip;
        this.port = port;

        if (weight < 1) throw new IllegalArgumentException("Weight must be larger or equal then 1");
        this.weight = weight;
    }

    public String getIp()
    {
        return ip;
    }

    public int getPort()
    {
        return port;
    }

    public int getWeight()
    {
        return weight;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Host host = (Host) o;
        return port == host.port && !(ip != null ? !ip.equals(host.ip) : host.ip != null);

    }

    @Override
    public int hashCode()
    {
        int result = ip != null ? ip.hashCode() : 0;
        result = 31 * result + port;
        return result;
    }

    @Override
    public String toString()
    {
        return "Host{" + "ip='" + ip + '\'' + ", port=" + port + ", weight=" + weight + '}';
    }
}
