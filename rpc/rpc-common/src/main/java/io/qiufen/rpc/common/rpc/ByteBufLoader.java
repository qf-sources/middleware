package io.qiufen.rpc.common.rpc;

import io.netty.buffer.ByteBuf;

public class ByteBufLoader implements BufferLoader<ByteBuf>
{
    public static final ByteBufLoader INSTANCE = new ByteBufLoader();

    private ByteBufLoader()
    {
    }

    @Override
    public ByteBuf to(Object input)
    {
        return (ByteBuf) input;
    }
}
