package io.qiufen.rpc.common.rpc;

import io.netty.buffer.ByteBuf;

public class ByteBufWriter implements BufferWriter<ByteBuf>
{
    public static final ByteBufWriter INSTANCE = new ByteBufWriter();

    private ByteBufWriter()
    {
    }

    @Override
    public Object to(ByteBuf input)
    {
        return input;
    }
}
