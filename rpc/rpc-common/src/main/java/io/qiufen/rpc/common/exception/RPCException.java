package io.qiufen.rpc.common.exception;

/**
 * 服务调用异常
 */
public class RPCException extends RuntimeException
{
    public RPCException()
    {
    }

    public RPCException(String message)
    {
        super(message);
    }
}
