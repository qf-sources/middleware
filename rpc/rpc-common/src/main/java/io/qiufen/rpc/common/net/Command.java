package io.qiufen.rpc.common.net;

/**
 * 指令枚举
 */
public enum Command
{
    PREPARE((byte) 0x01, "准备链接"),
    READY((byte) 0x02, "链接就绪"),
    END((byte) 0x03, "结束链接"),
    REQUEST((byte) 0x04, "请求处理数据"),
    RESPONSE((byte) 0x05, "响应处理数据");

    /**
     * 指令编码
     */
    private byte code;

    /**
     * 指令描述
     */
    private String desc;

    Command(byte code, String desc)
    {
        this.code = code;
        this.desc = desc;
    }

    public byte getCode()
    {
        return code;
    }

    public String getDesc()
    {
        return desc;
    }
}
