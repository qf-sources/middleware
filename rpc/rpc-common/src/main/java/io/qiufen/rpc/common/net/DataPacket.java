package io.qiufen.rpc.common.net;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.CompositeByteBuf;
import io.netty.buffer.Unpooled;

/**
 * 通信数据报文
 */
public final class DataPacket
{
    /**
     * 指令
     */
    private final byte cmd;

    /**
     * 报文序列：0x00000000 ~ 0xffffffff
     */
    private final int sequence;

    private final ByteBuf uri;

    private final ByteBuf header;

    /**
     * 用户数据
     */
    private final ByteBuf data;

    private transient int length;

    public DataPacket(byte cmd, int sequence, ByteBuf uri, ByteBuf header, ByteBuf data)
    {
        this.cmd = cmd;
        length += 1;

        this.sequence = sequence;
        length += 4;

        this.uri = uri;
        length += 2 + uri.readableBytes();

        this.header = header;
        length += 4 + header.readableBytes();

        this.data = data;
        length += 4 + data.readableBytes();
    }

    public static DataPacket fromBuffer(ByteBuf buf)
    {
        try
        {
            byte cmd = buf.readByte();
            int seq = buf.readInt();
            int uriLength = buf.readShort();
            int headerLength = buf.readInt();
            int dataLength = buf.readInt();

            ByteBuf uri = Unpooled.EMPTY_BUFFER;
            ByteBuf header = Unpooled.EMPTY_BUFFER;
            ByteBuf data = Unpooled.EMPTY_BUFFER;
            if (uriLength > 0) uri = buf.readRetainedSlice(uriLength);
            if (headerLength > 0) header = buf.readRetainedSlice(headerLength);
            if (dataLength > 0) data = buf.readRetainedSlice(dataLength);

            return new DataPacket(cmd, seq, uri, header, data);
        }
        finally
        {
            buf.release();
        }
    }

    public byte getCmd()
    {
        return cmd;
    }

    public int getSequence()
    {
        return sequence;
    }

    public ByteBuf getUri()
    {
        return uri;
    }

    public ByteBuf getHeader()
    {
        return header;
    }

    public ByteBuf getData()
    {
        return data;
    }

    public int getLength()
    {
        return length;
    }

    public ByteBuf toBuffer()
    {
        try
        {
            ByteBufAllocator allocator = ByteBufAllocator.DEFAULT;
            int uriLength = uri.readableBytes();
            int headerLength = header.readableBytes();
            int dataLength = data.readableBytes();
            CompositeByteBuf buf = allocator.compositeBuffer(5)
                    .addComponent(true, allocator.buffer(5).writeByte(cmd).writeInt(sequence))
                    .addComponent(true,
                            allocator.buffer(10).writeShort(uriLength).writeInt(headerLength).writeInt(dataLength));
            if (uriLength > 0) buf.addComponent(true, uri.retain());
            if (headerLength > 0) buf.addComponent(true, header.retain());
            if (dataLength > 0) buf.addComponent(true, data.retain());
            return buf;
        }
        finally
        {
            release();
        }
    }

    public void release()
    {
        uri.release();
        header.release();
        data.release();
    }

    public DataPacket retain()
    {
        uri.retain();
        header.retain();
        data.retain();
        return this;
    }
}
