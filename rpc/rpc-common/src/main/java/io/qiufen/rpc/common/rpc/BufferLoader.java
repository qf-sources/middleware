package io.qiufen.rpc.common.rpc;

public interface BufferLoader<T>
{
    T to(Object input);
}
