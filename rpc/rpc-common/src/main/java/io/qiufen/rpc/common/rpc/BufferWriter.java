package io.qiufen.rpc.common.rpc;

public interface BufferWriter<T>
{
    Object to(T input);
}
