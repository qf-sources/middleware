package io.qiufen.rpc.common.exception;

public class EndpointException extends Exception
{
    public EndpointException()
    {
    }

    public EndpointException(Throwable cause)
    {
        super(cause);
    }

    public EndpointException(String message) {super(message);}
}
