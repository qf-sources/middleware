package io.qiufen.rsi.client.spi.subscriber;

import io.qiufen.common.provider.Provider;
import io.qiufen.rsi.client.spi.rpc.RPCClientContext;

public interface ServiceContextProvider extends Provider
{
    ServiceContext provide(RPCClientContext rpcClientContext, ServiceSubscriber subscriber, ServiceInfo serviceInfo);
}
