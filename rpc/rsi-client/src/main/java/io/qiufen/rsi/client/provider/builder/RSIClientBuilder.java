package io.qiufen.rsi.client.provider.builder;

import io.qiufen.common.provider.DefaultProviderContext;
import io.qiufen.rpc.client.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.client.provider.infrastructure.DefaultClientNetService;
import io.qiufen.rpc.client.spi.RPCClientConfig;
import io.qiufen.rpc.client.spi.dispatcher.RPCContextDispatcher;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import io.qiufen.rsi.client.api.RSIClient;
import io.qiufen.rsi.client.api.RSIClientConfig;
import io.qiufen.rsi.client.provider.invocation.DefaultInvokerProvider;
import io.qiufen.rsi.client.provider.proxy.DefaultInvocationHandlerProvider;
import io.qiufen.rsi.client.provider.rpc.DefaultRPCClientContextProvider;
import io.qiufen.rsi.client.provider.subscriber.DefaultServiceContextProvider;
import io.qiufen.rsi.client.spi.invocation.InvokerProvider;
import io.qiufen.rsi.client.spi.proxy.InvocationHandlerProvider;
import io.qiufen.rsi.client.spi.rpc.RPCClientContextProvider;
import io.qiufen.rsi.client.spi.subscriber.ServiceContextProvider;
import io.qiufen.rsi.common.protocol.Protocols;

import java.util.concurrent.Executors;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/11/9 19:58
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class RSIClientBuilder
{
    private final DefaultProviderContext providerContext = new DefaultProviderContext();

    private final RSIClientConfig rsiClientConfig = defaultRSIClientConfig();

    private final RPCClientConfig rpcClientConfig = defaultRPCClientConfig();

    private ServiceContextProvider serviceContextProvider;

    private InvocationHandlerProvider invocationHandlerProvider;

    private InvokerProvider invokerProvider;

    private RPCClientContextProvider rpcClientContextProvider;

    private ClientNetService clientNetService;

    private RPCContextDispatcher rpcContextDispatcher;

    public RSIClientConfig getRsiClientConfig()
    {
        return rsiClientConfig;
    }

    public RPCClientConfig getRpcClientConfig()
    {
        return rpcClientConfig;
    }

    public RSIClientBuilder setServiceContextProvider(ServiceContextProvider serviceContextProvider)
    {
        this.serviceContextProvider = serviceContextProvider;
        return this;
    }

    public RSIClientBuilder setInvocationHandlerProvider(InvocationHandlerProvider invocationHandlerProvider)
    {
        this.invocationHandlerProvider = invocationHandlerProvider;
        return this;
    }

    public RSIClientBuilder setInvokerProvider(InvokerProvider invokerProvider)
    {
        this.invokerProvider = invokerProvider;
        return this;
    }

    public RSIClientBuilder setRpcClientContextProvider(RPCClientContextProvider rpcClientContextProvider)
    {
        this.rpcClientContextProvider = rpcClientContextProvider;
        return this;
    }

    public RSIClientBuilder setClientNetService(ClientNetService clientNetService)
    {
        this.clientNetService = clientNetService;
        return this;
    }

    public RSIClientBuilder setRpcContextDispatcher(RPCContextDispatcher rpcContextDispatcher)
    {
        this.rpcContextDispatcher = rpcContextDispatcher;
        return this;
    }

    public RSIClient build()
    {
        if (serviceContextProvider == null) serviceContextProvider = DefaultServiceContextProvider.INSTANCE;
        providerContext.setProvider(ServiceContextProvider.class, serviceContextProvider);

        if (invocationHandlerProvider == null) invocationHandlerProvider = DefaultInvocationHandlerProvider.INSTANCE;
        providerContext.setProvider(InvocationHandlerProvider.class, invocationHandlerProvider);

        if (invokerProvider == null) invokerProvider = DefaultInvokerProvider.INSTANCE;
        providerContext.setProvider(InvokerProvider.class, invokerProvider);

        if (rpcClientContextProvider == null) rpcClientContextProvider = DefaultRPCClientContextProvider.INSTANCE;
        providerContext.setProvider(RPCClientContextProvider.class, rpcClientContextProvider);

        providerContext.setProvider(RSIClientConfig.class, rsiClientConfig);
        providerContext.setProvider(RPCClientConfig.class, rpcClientConfig);

        int cpus = Runtime.getRuntime().availableProcessors();
        if (clientNetService == null) clientNetService = new DefaultClientNetService(cpus << 1);
        providerContext.setProvider(ClientNetService.class, clientNetService);

        if (rpcContextDispatcher == null)
        {
            rpcContextDispatcher = new DefaultRPCContextDispatcher(Executors.newFixedThreadPool(cpus << 1), 1024 * 512,
                    1024 * 1024);
        }
        providerContext.setProvider(RPCContextDispatcher.class, rpcContextDispatcher);

        return new DefaultRSIClient(providerContext);
    }

    private RSIClientConfig defaultRSIClientConfig()
    {
        RSIClientConfig config = new RSIClientConfig();
        config.setProtocol(Protocols.V1);
        return config;
    }

    private RPCClientConfig defaultRPCClientConfig()
    {
        int cpus = Runtime.getRuntime().availableProcessors();
        RPCClientConfig config = new RPCClientConfig();
        config.setInitConnections(0);
        config.setMaxConnections(cpus << 2);
        config.setReadTimeoutMills(100);
        return config;
    }
}
