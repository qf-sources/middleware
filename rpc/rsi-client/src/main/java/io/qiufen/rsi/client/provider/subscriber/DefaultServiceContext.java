package io.qiufen.rsi.client.provider.subscriber;

import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rsi.client.spi.exception.NoneInstanceException;
import io.qiufen.rsi.client.spi.exception.NoneMethodException;
import io.qiufen.rsi.client.spi.rpc.RPCClientContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceInfo;
import io.qiufen.rsi.client.spi.subscriber.ServiceMethodInfo;
import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class DefaultServiceContext implements ServiceContext
{
    private final ServiceInfo serviceInfo;

    private final ServiceSubscriber subscriber;

    private final RPCClientContext rpcClientContext;

    private final Map<Method, ServiceMethodInfo> methodInfoMap;

    private RPCClient rpcClient;

    DefaultServiceContext(RPCClientContext rpcClientContext, ServiceSubscriber subscriber, ServiceInfo serviceInfo)
    {
        this.rpcClientContext = rpcClientContext;
        this.subscriber = subscriber;
        this.serviceInfo = serviceInfo;

        this.methodInfoMap = new HashMap<Method, ServiceMethodInfo>();
    }

    @Override
    public ServiceInfo getServiceInfo()
    {
        return this.serviceInfo;
    }

    @Override
    public String getMethodName(Method method)
    {
        return method.getName();
    }

    @Override
    public void addMethod(ServiceMethodInfo methodInfo)
    {
        if (methodInfoMap.containsKey(methodInfo.getMethod()))
        {
            throw new RuntimeException("Duplication method:" + methodInfo.getMethod());
        }
        this.methodInfoMap.put(methodInfo.getMethod(), methodInfo);
    }

    @Override
    public ServiceMethodInfo getMethodInfo(Method method) throws NoneMethodException
    {
        ServiceMethodInfo info = methodInfoMap.get(method);
        if (info == null) throw new NoneMethodException(subscriber, method.getDeclaringClass(), method);
        return info;
    }

    @Override
    public void setRPCClient(String rpcClientId, Set<Host> hostSet)
    {
        this.rpcClient = rpcClientContext.initRPCClient(rpcClientId, hostSet);
    }

    @Override
    public RPCClient getRPCClient() throws NoneInstanceException
    {
        if (this.rpcClient == null) throw new NoneInstanceException(this.subscriber, null);
        return this.rpcClient;
    }

    @Override
    public void removeMethod(Method method)
    {
        this.methodInfoMap.remove(method);
    }

    @Override
    public boolean hasMethod(Method method)
    {
        return methodInfoMap.containsKey(method);
    }

    @Override
    public RPCClientContext getRPCClientContext()
    {
        return this.rpcClientContext;
    }
}
