package io.qiufen.rsi.client.spi.exception;

import io.qiufen.rpc.common.exception.EndpointException;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/11/10 14:49
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class RSIClientException extends EndpointException
{
    public RSIClientException()
    {
    }

    public RSIClientException(String message)
    {
        super(message);
    }
}
