package io.qiufen.rsi.client.spi.invocation;

import io.qiufen.common.promise.api.Future;
import io.qiufen.rsi.client.spi.proxy.InvocationResult;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;

import java.lang.reflect.Method;

public interface Invoker
{
    Future<InvocationResult> invoke(ServiceContext serviceContext, Method method, Object[] args) throws Exception;
}
