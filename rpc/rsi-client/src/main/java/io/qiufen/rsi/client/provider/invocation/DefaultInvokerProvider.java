package io.qiufen.rsi.client.provider.invocation;

import io.qiufen.rsi.client.spi.invocation.Invoker;
import io.qiufen.rsi.client.spi.invocation.InvokerProvider;
import io.qiufen.rsi.common.protocol.Protocol;

public class DefaultInvokerProvider implements InvokerProvider
{
    public static final DefaultInvokerProvider INSTANCE = new DefaultInvokerProvider();

    private DefaultInvokerProvider()
    {
    }

    @Override
    public Invoker provide(Protocol protocol)
    {
        return new DefaultInvoker(protocol);
    }
}
