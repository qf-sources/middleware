package io.qiufen.rsi.client.spi.exception;

import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;

import java.lang.reflect.Method;

public class NoneMethodException extends RSIClientException
{
    private final ServiceSubscriber subscriber;
    private final Class<?> interClass;
    private final Method method;

    public NoneMethodException(ServiceSubscriber subscriber, Class<?> interClass, Method method)
    {
        this.subscriber = subscriber;
        this.interClass = interClass;
        this.method = method;
    }

    public Class<?> getInterClass()
    {
        return interClass;
    }

    public Method getMethod()
    {
        return method;
    }

    @Override
    public String getMessage()
    {
        return toString();
    }

    @Override
    public String toString()
    {
        return "NoneMethodException{" + "subscriber=" + subscriber + ", interClass=" + interClass + ", method=" + method + '}';
    }
}
