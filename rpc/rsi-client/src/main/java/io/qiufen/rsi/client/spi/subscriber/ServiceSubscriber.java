package io.qiufen.rsi.client.spi.subscriber;

import java.lang.reflect.Method;

/**
 * 服务订阅者
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface ServiceSubscriber
{
    void onSubscribe(ServiceContext serviceContext, String serviceURI, Class<?> serviceClass, Method[] methods);

    boolean isReady();
}
