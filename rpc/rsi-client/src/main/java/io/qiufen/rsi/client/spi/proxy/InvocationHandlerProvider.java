package io.qiufen.rsi.client.spi.proxy;

import io.qiufen.common.provider.Provider;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;

import java.lang.reflect.InvocationHandler;

public interface InvocationHandlerProvider extends Provider
{
    InvocationHandler provide(ProviderContext providerContext, ServiceContext serviceContext);
}
