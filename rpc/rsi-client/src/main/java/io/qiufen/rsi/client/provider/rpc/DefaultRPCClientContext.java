package io.qiufen.rsi.client.provider.rpc;

import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.provider.builder.MultipleRouteRPCClient;
import io.qiufen.rpc.client.provider.builder.MultipleRouteRPCClientBuilder;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.RPCClientConfig;
import io.qiufen.rpc.client.spi.dispatcher.RPCContextDispatcher;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import io.qiufen.rsi.client.spi.rpc.RPCClientContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

class DefaultRPCClientContext implements RPCClientContext
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultRPCClientContext.class);

    private final RPCContextDispatcher rpcContextDispatcher;
    private final ClientNetService clientNetService;
    private final Map<String, MultipleRouteRPCClient> clientMap;

    private final RPCClientConfig rpcClientConfig;

    DefaultRPCClientContext(ProviderContext providerContext)
    {
        this.rpcClientConfig = providerContext.getProvider(RPCClientConfig.class);

        this.clientMap = new ConcurrentHashMap<String, MultipleRouteRPCClient>();
        this.rpcContextDispatcher = providerContext.getProvider(RPCContextDispatcher.class);
        this.clientNetService = providerContext.getProvider(ClientNetService.class);
    }

    @Override
    public RPCClient initRPCClient(String id, Set<Host> hostSet)
    {
        MultipleRouteRPCClient client = this.clientMap.get(id);
        if (client == null)
        {
            if (LOGGER.isInfoEnabled())
            {
                LOGGER.info("Create new rpc client, rpcClientId:{}", id);
            }
            MultipleRouteRPCClientBuilder builder = new MultipleRouteRPCClientBuilder();
            builder.getClientConfig().setInitConnections(rpcClientConfig.getInitConnections());
            builder.getClientConfig().setMaxConnections(rpcClientConfig.getMaxConnections());
            builder.getClientConfig().setReadTimeoutMills(rpcClientConfig.getReadTimeoutMills());
            client = (MultipleRouteRPCClient) builder.setRpcContextDispatcher(rpcContextDispatcher)
                    .setClientNetService(clientNetService)
                    .build();
            this.clientMap.put(id, client);
        }
        if (LOGGER.isInfoEnabled())
        {
            LOGGER.info("Set new route for rpc client, rpcClientId:{}, host set:{}", id, hostSet);
        }
        client.updateRoute(hostSet);
        return client;
    }
}
