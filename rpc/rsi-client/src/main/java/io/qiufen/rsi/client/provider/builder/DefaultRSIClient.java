package io.qiufen.rsi.client.provider.builder;

import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rsi.client.api.RSIClient;
import io.qiufen.rsi.client.api.RSIClientConfig;
import io.qiufen.rsi.client.spi.exception.RSIClientException;
import io.qiufen.rsi.client.spi.proxy.InvocationHandlerProvider;
import io.qiufen.rsi.client.spi.rpc.RPCClientContext;
import io.qiufen.rsi.client.spi.rpc.RPCClientContextProvider;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceContextProvider;
import io.qiufen.rsi.client.spi.subscriber.ServiceInfo;
import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

/**
 * 服务代理客户端
 */
class DefaultRSIClient implements RSIClient
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultRSIClient.class);

    private final Map<String, Object> proxies = new IdentityHashMap<String, Object>();

    private final List<NetEventListener> listeners;

    private final InvocationHandlerProvider invocationHandlerProvider;

    private final ProviderContext providerContext;
    private final ServiceContextProvider serviceContextProvider;
    private final ServiceSubscriber subscriber;

    private final RPCClientContext rpcClientContext;

    DefaultRSIClient(ProviderContext providerContext)
    {
        RSIClientConfig config = providerContext.getProvider(RSIClientConfig.class);
        if (config.getSubscriber() == null)
        {
            throw new IllegalArgumentException("Need service subscriber.");
        }
        if (config.getProtocol() == null)
        {
            throw new IllegalArgumentException("Need protocol.");
        }

        this.listeners = new ArrayList<NetEventListener>();

        this.providerContext = providerContext;
        this.serviceContextProvider = providerContext.getProvider(ServiceContextProvider.class);
        this.invocationHandlerProvider = providerContext.getProvider(InvocationHandlerProvider.class);

        this.subscriber = config.getSubscriber();
        this.rpcClientContext = providerContext.getProvider(RPCClientContextProvider.class).provide(providerContext);
    }

    /**
     * 订阅服务
     *
     * @param serviceURI   uri
     * @param serviceClass 服务接口
     */
    public <T> T subscribeService(String serviceURI, Class<T> serviceClass) throws RSIClientException
    {
        Object proxy = this.proxies.get(serviceURI);
        if (proxy == null)
        {
            proxy = createServiceProxy(serviceURI, serviceClass);
            this.proxies.put(serviceURI, proxy);
            return serviceClass.cast(proxy);
        }
        throw new RSIClientException("Service has subscribed, class:" + serviceClass);
    }

    public <T> T getService(String serviceURI, Class<T> serviceClass) throws RSIClientException
    {
        Object proxy = this.proxies.get(serviceURI);
        if (proxy == null)
        {
            throw new RSIClientException("No such service registered, class:" + serviceClass);
        }
        return serviceClass.cast(proxy);
    }

    /**
     * 添加客户端监听器
     *
     * @param listener 监听器
     */
    public RSIClient addListener(NetEventListener listener)
    {
        listeners.add(listener);
        return this;
    }

    private Object createServiceProxy(String serviceURI, Class<?> serviceClass)
    {
        LOGGER.info("Subscribe service,uri:{},serviceClass:{}", serviceURI, serviceClass.getName());
        ServiceInfo serviceInfo = new ServiceInfo(serviceURI, serviceClass);
        ServiceContext serviceContext = serviceContextProvider.provide(rpcClientContext, subscriber, serviceInfo);
        InvocationHandler handler = invocationHandlerProvider.provide(providerContext, serviceContext);
        Object proxy = Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{serviceClass}, handler);
        subscriber.onSubscribe(serviceContext, serviceURI, serviceClass, serviceClass.getMethods());
        return proxy;
    }
}
