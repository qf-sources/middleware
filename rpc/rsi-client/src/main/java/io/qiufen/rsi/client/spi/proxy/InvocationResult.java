package io.qiufen.rsi.client.spi.proxy;

public class InvocationResult
{
    private final Object value;

    public InvocationResult(Object value)
    {
        this.value = value;
    }

    public Object getValue()
    {
        return value;
    }
}
