package io.qiufen.rsi.client.provider.proxy;

import io.qiufen.common.promise.api.Future;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.common.exception.RPCException;
import io.qiufen.rsi.client.api.RSIClientConfig;
import io.qiufen.rsi.client.provider.builder.async.Async;
import io.qiufen.rsi.client.provider.builder.async.AsyncContext;
import io.qiufen.rsi.client.spi.exception.NoneMethodException;
import io.qiufen.rsi.client.spi.invocation.Invoker;
import io.qiufen.rsi.client.spi.invocation.InvokerProvider;
import io.qiufen.rsi.client.spi.proxy.InvocationResult;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutionException;

/**
 * 接口逻辑实现处理器
 */
class DefaultInvocationHandler implements InvocationHandler
{
    private static final AsyncContext ASYNC_CONTEXT = Async.getContext();

    private final ServiceSubscriber subscriber;
    private final ServiceContext serviceContext;
    private final Invoker invoker;

    DefaultInvocationHandler(ProviderContext providerContext, ServiceContext serviceContext)
    {
        this.serviceContext = serviceContext;
        RSIClientConfig config = providerContext.getProvider(RSIClientConfig.class);
        this.subscriber = config.getSubscriber();

        this.invoker = providerContext.getProvider(InvokerProvider.class).provide(config.getProtocol());
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
    {
        if (subscriber.isReady())
        {
            Future<InvocationResult> future;
            try
            {
                future = invoker.invoke(serviceContext, method, args);
            }
            catch (NoneMethodException e)
            {
                if (isObjectMethod(method))
                {
                    return method.invoke(serviceContext.getServiceInfo(), args);
                }

                throw e;
            }

            if (ASYNC_CONTEXT.async())
            {
                ASYNC_CONTEXT.bindFuture(future);
                return returnDefault(method);
            }
            else
            {
                InvocationResult result;
                try
                {
                    result = future.get();
                }
                catch (ExecutionException e)
                {
                    Throwable throwable = e.getCause();
                    if (throwable == null)
                    {
                        throw e;
                    }
                    throw throwable;
                }
                return result.getValue();
            }
        }

        if (isObjectMethod(method))
        {
            return method.invoke(serviceContext.getServiceInfo(), args);
        }

        throw new RPCException("RSIClient isn't ready.");
    }

    private boolean isObjectMethod(Method method)
    {
        return method.getDeclaringClass() == Object.class;
    }

    private Object returnDefault(Method method)
    {
        Class<?> returnType = method.getReturnType();
        if (returnType.isPrimitive())
        {
            if (returnType == boolean.class) return false;
            if (returnType == byte.class) return (byte) 0;
            if (returnType == short.class) return (short) 0;
            if (returnType == int.class) return 0;
            if (returnType == long.class) return 0L;
            if (returnType == float.class) return 0.0f;
            if (returnType == double.class) return 0.0;
        }
        return null;
    }
}

