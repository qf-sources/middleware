package io.qiufen.rsi.client.api;

import io.qiufen.common.promise.api.Future;

/**
 * todo come soon
 */
public interface ServiceLocator
{
    <T> Future<T> call();

    <T> Future<T> call(Object arg);

    <T> Future<T> call(Object... args);
}
