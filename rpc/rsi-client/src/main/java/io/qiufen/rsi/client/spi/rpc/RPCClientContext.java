package io.qiufen.rsi.client.spi.rpc;

import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.spi.Host;

import java.util.Set;

public interface RPCClientContext
{
    RPCClient initRPCClient(String id, Set<Host> hostSet);
}
