package io.qiufen.rsi.client.provider.builder.async;

import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.api.Future;
import io.qiufen.common.promise.spi.Resolved;
import io.qiufen.rsi.client.spi.async.Operation;
import io.qiufen.rsi.client.spi.proxy.InvocationResult;

/**
 * todo instead of ServiceLocator
 */
@Deprecated
public class Async
{
    private static final AsyncContext CONTEXT = new AsyncContext();

    public static Future<InvocationResult> run(Operation operation)
    {
        CONTEXT.setAsync();
        try
        {
            operation.invoke();
            return CONTEXT.getFuture();
        }
        finally
        {
            CONTEXT.clear();
        }
    }

    public static <T> Future<T> runAndGetValue(Operation operation)
    {
        return run(operation).then(new GetValueResponse<T>());
    }

    public static AsyncContext getContext()
    {
        return CONTEXT;
    }

    private static class GetValueResponse<T> implements Resolved<InvocationResult, T>
    {
        private GetValueResponse() {}

        @Override
        public void onResolved(Context<T> context, InvocationResult value)
        {
            context.resolve((T) value.getValue());
        }
    }
}
