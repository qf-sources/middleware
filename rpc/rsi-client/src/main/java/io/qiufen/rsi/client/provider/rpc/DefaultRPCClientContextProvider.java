package io.qiufen.rsi.client.provider.rpc;

import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rsi.client.spi.rpc.RPCClientContext;
import io.qiufen.rsi.client.spi.rpc.RPCClientContextProvider;

public class DefaultRPCClientContextProvider implements RPCClientContextProvider
{
    public static final RPCClientContextProvider INSTANCE = new DefaultRPCClientContextProvider();

    private DefaultRPCClientContextProvider()
    {
    }

    @Override
    public RPCClientContext provide(ProviderContext providerContext)
    {
        return new DefaultRPCClientContext(providerContext);
    }
}
