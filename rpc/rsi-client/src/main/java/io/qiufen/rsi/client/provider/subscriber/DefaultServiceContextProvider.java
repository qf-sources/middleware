package io.qiufen.rsi.client.provider.subscriber;

import io.qiufen.rsi.client.spi.rpc.RPCClientContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceContextProvider;
import io.qiufen.rsi.client.spi.subscriber.ServiceInfo;
import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;

public class DefaultServiceContextProvider implements ServiceContextProvider
{
    public static final ServiceContextProvider INSTANCE = new DefaultServiceContextProvider();

    private DefaultServiceContextProvider() {}

    @Override
    public ServiceContext provide(RPCClientContext rpcClientContext, ServiceSubscriber subscriber,
            ServiceInfo serviceInfo)
    {
        return new DefaultServiceContext(rpcClientContext, subscriber, serviceInfo);
    }
}
