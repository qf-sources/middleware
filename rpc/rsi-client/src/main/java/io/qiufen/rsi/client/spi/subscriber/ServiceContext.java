package io.qiufen.rsi.client.spi.subscriber;

import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rsi.client.spi.exception.NoneInstanceException;
import io.qiufen.rsi.client.spi.exception.NoneMethodException;
import io.qiufen.rsi.client.spi.rpc.RPCClientContext;

import java.lang.reflect.Method;
import java.util.Set;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface ServiceContext
{
    ServiceInfo getServiceInfo();

    String getMethodName(Method method);

    void addMethod(ServiceMethodInfo methodInfo);

    ServiceMethodInfo getMethodInfo(Method method) throws NoneMethodException;

    void setRPCClient(String rpcClientId, Set<Host> hostSet);

    RPCClient getRPCClient() throws NoneInstanceException;

    void removeMethod(Method method);

    boolean hasMethod(Method method);

    RPCClientContext getRPCClientContext();
}
