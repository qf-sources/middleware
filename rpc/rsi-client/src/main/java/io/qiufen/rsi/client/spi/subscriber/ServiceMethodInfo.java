package io.qiufen.rsi.client.spi.subscriber;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/3 10:40
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ServiceMethodInfo
{
    private final String rpcURI;
    private final Class<?> serviceClass;
    private final Method method;

    private final Type[] argTypes;
    private final Type returnType;

    private final ByteBuf rpcURIBuf;

    public ServiceMethodInfo(String rpcURI, Class<?> serviceClass, Method method)
    {
        this.rpcURI = rpcURI;
        this.serviceClass = serviceClass;
        this.method = method;

        this.argTypes = method.getGenericParameterTypes();
        this.returnType = method.getGenericReturnType();

        byte[] bytes = rpcURI.getBytes();
        this.rpcURIBuf = Unpooled.unreleasableBuffer(ByteBufAllocator.DEFAULT.buffer(bytes.length).writeBytes(bytes));
    }

    public String getRpcURI()
    {
        return rpcURI;
    }

    public ByteBuf getRpcURIBuf() {return rpcURIBuf.duplicate();}

    public Class<?> getServiceClass()
    {
        return serviceClass;
    }

    public Method getMethod()
    {
        return method;
    }

    public Type[] getArgTypes()
    {
        return argTypes;
    }

    public Type getReturnType()
    {
        return returnType;
    }

    @Override
    public String toString()
    {
        return "ServiceMethodInfo{" + "rpcURI=" + rpcURI + ", serviceClass=" + serviceClass + ", method=" + method + ", argTypes=" + Arrays.toString(
                argTypes) + ", returnType=" + returnType + '}';
    }
}
