package io.qiufen.rsi.client.provider.invocation;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.qiufen.common.lang.TypeReference;
import io.qiufen.common.promise.api.Future;
import io.qiufen.common.promise.api.Promise;
import io.qiufen.common.promise.provider.Promises;
import io.qiufen.rpc.client.api.RPCClient;
import io.qiufen.rpc.client.spi.rpc.Feedback;
import io.qiufen.rpc.client.spi.rpc.RPCRequest;
import io.qiufen.rpc.client.spi.rpc.RPCRequestSetter;
import io.qiufen.rpc.client.spi.rpc.RPCResponse;
import io.qiufen.rpc.common.exception.RPCException;
import io.qiufen.rpc.common.rpc.ByteBufLoader;
import io.qiufen.rpc.common.rpc.ByteBufWriter;
import io.qiufen.rsi.client.spi.invocation.Invoker;
import io.qiufen.rsi.client.spi.proxy.InvocationResult;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceMethodInfo;
import io.qiufen.rsi.common.exception.RSIServiceException;
import io.qiufen.rsi.common.invocation.Constants;
import io.qiufen.rsi.common.protocol.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

class DefaultInvoker implements Invoker
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultInvoker.class);

    private static final Type TYPE = new TypeReference<List<String>>() {}.getType();

    private final Protocol protocol;

    DefaultInvoker(Protocol protocol)
    {
        this.protocol = protocol;
    }

    @Override
    public Future<InvocationResult> invoke(ServiceContext serviceContext, Method method, Object[] args) throws Exception
    {
        ServiceMethodInfo methodInfo = serviceContext.getMethodInfo(method);
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Use method info:{}", methodInfo);
        }
        RPCClient rpcClient = serviceContext.getRPCClient();
        RPCRequestSetter requestSetter = new RequestSetter(methodInfo, args);
        Promise<InvocationResult> promise = Promises.create();
        Feedback feedback = new DefaultFeedback(methodInfo, promise);
        rpcClient.send(requestSetter, feedback);
        return promise.future();
    }

    private void setRequest(RPCRequest request, ServiceMethodInfo methodInfo, Object[] args)
    {
        request.writeUri(ByteBufWriter.INSTANCE, methodInfo.getRpcURIBuf());

        // headers 请求头区保留
        //HashMap<String, String> headers = context.attribute(AttributeKeys.HEADER_REQUEST);
        //protocol.toBytes(dataBuf, Constants.TYPE_HEADER, headers);
        //request.writeHeader();

        ByteBuf dataBuf = Unpooled.EMPTY_BUFFER;
        try
        {
            Type[] types = methodInfo.getArgTypes();
            if (types.length > 0)
            {
                dataBuf = ByteBufAllocator.DEFAULT.buffer();
                for (int i = 0; i < types.length; i++)
                {
                    protocol.toBytes(dataBuf, types[i], args[i]);
                }
            }
            request.writeData(ByteBufWriter.INSTANCE, dataBuf.retain());
        }
        finally
        {
            dataBuf.release();
        }
    }

    private class RequestSetter implements RPCRequestSetter
    {
        private final ServiceMethodInfo methodInfo;
        private final Object[] args;

        private RequestSetter(ServiceMethodInfo methodInfo, Object[] args)
        {
            this.methodInfo = methodInfo;
            this.args = args;
        }

        @Override
        public void set(RPCRequest request)
        {
            setRequest(request, methodInfo, args);
        }
    }

    private class DefaultFeedback implements Feedback
    {
        private final ServiceMethodInfo info;
        private final Promise<InvocationResult> promise;

        private DefaultFeedback(ServiceMethodInfo info, Promise<InvocationResult> promise)
        {
            this.info = info;
            this.promise = promise;
        }

        @Override
        public void onResponse(RPCResponse response)
        {
            decodeResponse(info, response);
        }

        @Override
        public void onResponse(RPCException e)
        {
            promise.reject(e);
        }

        private void decodeResponse(ServiceMethodInfo info, RPCResponse response)
        {
            //        ByteBuf header = response.getHeader(ByteBufLoader.INSTANCE);
            HashMap<String, String> headers = null;
            //        try
            //        {
            //            headers = protocol.fromBytes(header, Constants.TYPE_HEADER);
            //        }
            //        finally
            //        {
            //            header.release();
            //        }

            ByteBuf data = response.getData(ByteBufLoader.INSTANCE);
            Object result;
            try
            {
                result = deserializeData(data, info);
            }
            catch (RSIServiceException e)
            {
                promise.reject(e);
                return;
            }

            promise.resolve(new InvocationResult(result));
        }

        private Object deserializeData(ByteBuf dataBuf, ServiceMethodInfo info) throws RSIServiceException
        {
            try
            {
                if (dataBuf.readableBytes() <= 0) throw new RPCException("None data returned");
                byte flag = dataBuf.readByte();
                if (flag == Constants.SUCCESS)
                {
                    //无可读数据，返回NULL
                    if (dataBuf.readableBytes() <= 0) return null;

                    return protocol.fromBytes(dataBuf, info.getReturnType());
                }
                else if (flag == Constants.FAILURE)
                {
                    doRPCException(protocol, info, dataBuf);
                }
                throw new RPCException("Unrecognized flag:" + flag);
            }
            finally
            {
                dataBuf.release();
            }
        }

        private void doRPCException(Protocol protocol, ServiceMethodInfo info, ByteBuf dataBuf)
        {
            //code,message,extra
            List<String> array = protocol.fromBytes(dataBuf, TYPE);
            int len;
            if (array == null || (len = array.size()) <= 0) throw new RPCException("Unknown exception.");

            RSIServiceException exception = new RSIServiceException(array.get(0));
            if (len > 1) exception.setErrorMessage(array.get(1));
            if (len > 2) exception.setArgs(array.get(2));
            exception.setServiceClass(info.getServiceClass());
            exception.setMethod(info.getMethod());
            throw exception;
        }
    }
}
