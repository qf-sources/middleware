package io.qiufen.rsi.client.spi.subscriber;

public class ServiceInfo
{
    private final String serviceURI;
    private final Class<?> serviceClass;

    public ServiceInfo(String serviceURI, Class<?> serviceClass)
    {
        this.serviceURI = serviceURI;
        this.serviceClass = serviceClass;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceInfo that = (ServiceInfo) o;

        if (serviceURI != null ? !serviceURI.equals(that.serviceURI) : that.serviceURI != null) return false;
        return serviceClass != null ? serviceClass.equals(that.serviceClass) : that.serviceClass == null;
    }

    @Override
    public int hashCode()
    {
        int result = serviceURI != null ? serviceURI.hashCode() : 0;
        result = 31 * result + (serviceClass != null ? serviceClass.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return "ServiceInfo{" + "serviceURI='" + serviceURI + '\'' + ", serviceClass=" + serviceClass + '}';
    }
}
