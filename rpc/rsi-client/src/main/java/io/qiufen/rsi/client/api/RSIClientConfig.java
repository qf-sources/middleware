package io.qiufen.rsi.client.api;

import io.qiufen.common.provider.Provider;
import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;
import io.qiufen.rsi.common.protocol.Protocol;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/20 19:48
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class RSIClientConfig implements Provider
{
    /**
     * 协议层实现
     */
    private Protocol protocol;

    private ServiceSubscriber subscriber;

    public Protocol getProtocol()
    {
        return protocol;
    }

    public void setProtocol(Protocol protocol)
    {
        this.protocol = protocol;
    }

    public ServiceSubscriber getSubscriber()
    {
        return subscriber;
    }

    public void setSubscriber(ServiceSubscriber subscriber)
    {
        this.subscriber = subscriber;
    }
}
