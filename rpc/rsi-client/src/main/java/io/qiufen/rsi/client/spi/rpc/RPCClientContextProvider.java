package io.qiufen.rsi.client.spi.rpc;


import io.qiufen.common.provider.Provider;
import io.qiufen.common.provider.ProviderContext;

public interface RPCClientContextProvider extends Provider
{
    RPCClientContext provide(ProviderContext providerContext);
}
