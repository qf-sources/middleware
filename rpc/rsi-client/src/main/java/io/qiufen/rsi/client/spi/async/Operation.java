package io.qiufen.rsi.client.spi.async;

public interface Operation
{
    void invoke();
}
