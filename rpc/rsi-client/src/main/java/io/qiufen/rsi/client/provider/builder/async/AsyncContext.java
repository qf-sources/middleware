package io.qiufen.rsi.client.provider.builder.async;

import io.qiufen.common.promise.api.Future;
import io.qiufen.rsi.client.spi.proxy.InvocationResult;

public class AsyncContext
{
    private final ThreadLocal<Boolean> flag = new ThreadLocal<Boolean>();
    private final ThreadLocal<Future<InvocationResult>> local = new ThreadLocal<Future<InvocationResult>>();

    AsyncContext()
    {
    }

    public void bindFuture(Future<InvocationResult> future)
    {
        local.set(future);
    }

    void setAsync()
    {
        this.flag.set(true);
    }

    public boolean async()
    {
        Boolean flag = this.flag.get();
        return flag != null && flag;
    }

    public Future<InvocationResult> getFuture()
    {
        return local.get();
    }

    void clear()
    {
        flag.remove();
        local.remove();
    }
}
