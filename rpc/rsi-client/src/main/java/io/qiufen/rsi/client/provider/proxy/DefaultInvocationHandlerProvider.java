package io.qiufen.rsi.client.provider.proxy;

import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rsi.client.spi.proxy.InvocationHandlerProvider;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;

import java.lang.reflect.InvocationHandler;

public class DefaultInvocationHandlerProvider implements InvocationHandlerProvider
{
    public static final DefaultInvocationHandlerProvider INSTANCE = new DefaultInvocationHandlerProvider();

    private DefaultInvocationHandlerProvider()
    {
    }

    @Override
    public InvocationHandler provide(ProviderContext providerContext, ServiceContext serviceContext)
    {
        return new DefaultInvocationHandler(providerContext, serviceContext);
    }
}
