package io.qiufen.rsi.client.api;

import io.qiufen.rpc.client.spi.connection.NetEventListener;
import io.qiufen.rsi.client.spi.exception.RSIClientException;

/**
 * 远程服务调用客户端
 */
public interface RSIClient
{
    /**
     * 订阅服务
     *
     * @param serviceURI   uri
     * @param serviceClass 服务接口
     */
    <T> T subscribeService(String serviceURI, Class<T> serviceClass) throws RSIClientException;

    /**
     * 取得订阅服务
     *
     * @param serviceURI   uri
     * @param serviceClass 服务接口
     */
    <T> T getService(String serviceURI, Class<T> serviceClass) throws RSIClientException;

    /**
     * 添加客户端监听器
     *
     * @param listener 监听器
     */
    RSIClient addListener(NetEventListener listener);
}
