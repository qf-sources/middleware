package io.qiufen.rsi.client.spi.exception;

import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;

public class NoneInstanceException extends RSIClientException
{
    private final ServiceSubscriber subscriber;
    private final String serverName;

    public NoneInstanceException(ServiceSubscriber subscriber, String serverName)
    {
        this.subscriber = subscriber;
        this.serverName = serverName;
    }

    public String getServerName()
    {
        return serverName;
    }

    @Override
    public String getMessage()
    {
        return toString();
    }

    @Override
    public String toString()
    {
        return "{" + "subscriber=" + subscriber + ", serverName='" + serverName + '\'' + '}';
    }
}
