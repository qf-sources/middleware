package io.qiufen.rsi.client.spi.invocation;

import io.qiufen.common.provider.Provider;
import io.qiufen.rsi.common.protocol.Protocol;

public interface InvokerProvider extends Provider
{
    Invoker provide(Protocol protocol);
}
