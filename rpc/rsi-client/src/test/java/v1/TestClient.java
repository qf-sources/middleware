package v1;

import io.netty.util.ResourceLeakDetector;
import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.spi.Rejected;
import io.qiufen.common.promise.spi.Resolved;
import io.qiufen.rpc.client.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.client.provider.infrastructure.SharedClientNetService;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.dispatcher.RPCContextDispatcher;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import io.qiufen.rpc.common.exception.RPCException;
import io.qiufen.rsi.client.api.RSIClient;
import io.qiufen.rsi.client.provider.builder.RSIClientBuilder;
import io.qiufen.rsi.client.provider.builder.async.Async;
import io.qiufen.rsi.client.spi.async.Operation;
import io.qiufen.rsi.client.spi.exception.RSIClientException;
import io.qiufen.rsi.client.spi.proxy.InvocationResult;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceMethodInfo;
import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test0.UserRSIService;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class TestClient
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TestClient.class);

    static int concurrent = 500;
    static int loop = 500000;
    static ClientNetService clientNetService = new SharedClientNetService(
            Runtime.getRuntime().availableProcessors() << 1);
    static RPCContextDispatcher rpcContextDispatcher = new DefaultRPCContextDispatcher(Executors.newFixedThreadPool(8),
            1024, 1024 * 512);

    public static void main(String[] args) throws RSIClientException, InterruptedException
    {
        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);

        List<test0.UserRSIService> serviceList = new ArrayList<test0.UserRSIService>();
        for (int i = 0; i < 1; i++)
        {
            serviceList.add(create(50000 + i));
        }

        final String uuid = UUID.randomUUID().toString();
        final Semaphore semaphore = new Semaphore(0);
        final boolean[] hasFailure = {false};

        int locks = 0;
        Executor executor = Executors.newFixedThreadPool(concurrent);
        long p0 = System.currentTimeMillis();
        for (int i = 0; i < loop; i++)
        {
            for (final UserRSIService userRSIService : serviceList)
            {
                executor.execute(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Async.run(new Operation()
                        {
                            @Override
                            public void invoke()
                            {
                                userRSIService.getDate(uuid);
                            }
                        }).then(new Resolved<InvocationResult, Object>()
                        {
                            @Override
                            public void onResolved(Context context, InvocationResult o) throws Exception
                            {
                                System.out.println(o.getValue());
                            }
                        }, new Rejected<RPCException, Object>()
                        {
                            @Override
                            public void onRejected(Context<Object> context, RPCException e) throws Exception
                            {
                                e.printStackTrace();
                            }
                        });
                    }
                });
                locks++;
            }
        }

        semaphore.acquire(locks);
        long p1 = System.currentTimeMillis();
        System.out.println((p1 - p0) + "," + hasFailure[0]);
    }

    static test0.UserRSIService create(int port) throws RSIClientException
    {
        RSIClientBuilder builder = new RSIClientBuilder();
        builder.getRsiClientConfig().setSubscriber(new Subscriber1(port));
        builder.getRpcClientConfig().setReadTimeoutMills(10000);
        builder.getRpcClientConfig().setInitConnections(concurrent);
        RSIClient client = builder.setClientNetService(clientNetService)
                .setRpcContextDispatcher(rpcContextDispatcher)
                .build();
        return client.subscribeService("appCode:serverName:/user", UserRSIService.class);
    }
}

class Subscriber1 implements ServiceSubscriber
{
    private final int port;

    Subscriber1(int port)
    {
        this.port = port;
    }

    @Override
    public void onSubscribe(ServiceContext serviceContext, String serviceURI, Class<?> serviceClass, Method[] methods)
    {
        //uri = appCode:serverName:/user  + method
        //rpc client id = appCode:serverName
        //method uri = /user/test

        //uri -> {rpc client + rpc uri}
        String[] array = serviceURI.split(":/");
        for (Method method : methods)
        {
            serviceContext.addMethod(
                    new ServiceMethodInfo('/' + array[1] + '/' + serviceContext.getMethodName(method), serviceClass,
                            method));
        }

        serviceContext.setRPCClient(array[0], Collections.singleton(new Host("localhost", port)));
    }

    @Override
    public boolean isReady()
    {
        return true;
    }
}