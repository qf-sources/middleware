package test0;

import io.netty.util.ResourceLeakDetector;
import io.qiufen.common.concurrency.executor.spi.Command;
import io.qiufen.rpc.client.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rsi.client.api.RSIClient;
import io.qiufen.rsi.client.provider.builder.RSIClientBuilder;
import io.qiufen.rsi.client.spi.exception.RSIClientException;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceMethodInfo;
import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.LockSupport;

public class TestRSIClient0
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TestRSIClient0.class);

    static int users = 1000;

    static int loop = 5000000;

    static AtomicInteger total = new AtomicInteger(0);
    static AtomicInteger thought = new AtomicInteger(0);
    static AtomicLong totalTimes = new AtomicLong(0);

    static AtomicInteger r0_10 = new AtomicInteger(0), r10_50 = new AtomicInteger(0), r50_100 = new AtomicInteger(
            0), r100 = new AtomicInteger(0);

    static long end;

    public static void main(String[] args) throws RSIClientException, InterruptedException
    {
        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);

        new Thread()
        {
            final long start = System.currentTimeMillis();

            @Override
            public void run()
            {
                while (true)
                {
                    LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(1));
                    int count = total.get();
                    long times = totalTimes.get();
                    int th = thought.get();
                    thought.addAndGet(-th);
                    if (count <= 0)
                    {
                        continue;
                    }
                    System.out.println(
                            "total:" + count + ",r0_10:" + r0_10 + ",r10_50:" + r10_50 + ",r50_100:" + r50_100 + ",r100:" + r100 + ",avg cost:" + (times / count) + ",thought:" + th);

                    if (total.get() == loop && end > start)
                    {
                        System.out.println("avg thought:" + (loop / ((end - start) / 1000)));
                    }
                }
            }
        }.start();

        final UserRSIService userRSIService = create(50000);

        final String uuid = UUID.randomUUID().toString();
        //        final Semaphore semaphore = new Semaphore(0);
        //        final boolean[] hasFailure = {false};

        //        final AsyncCallback<Date> callback = new AsyncCallbackAdapter<Date>()
        //        {
        //            @Override
        //            public void onComplete(HashMap<String, String> headers, Date result)
        //            {
        //                semaphore.release(1);
        //            }
        //
        //            @Override
        //            public void onRPCException(RPCException e)
        //            {
        //                semaphore.release(1);
        //                if (!hasFailure[0])
        //                {
        //                    e.printStackTrace();
        //                    hasFailure[0] = true;
        //                }
        //            }
        //        };

        //        int locks = 0;
        Executor executor = Executors.newFixedThreadPool(users);
        for (int i = 0; i < loop; i++)
        {
            executor.execute(new Command()
            {
                long p0 = 0;

                @Override
                public void doExecute()
                {
                    p0 = System.currentTimeMillis();
                    //                        Async.run(new Operation()
                    //                        {
                    //                            @Override
                    //                            public void invoke()
                    //                            {
                    //                                userRSIService.getDate(uuid);
                    //                            }
                    //                        }, callback);
                    userRSIService.getDate(uuid);
                }

                @Override
                protected void doFinally()
                {
                    long p1 = System.currentTimeMillis();
                    long time = p1 - p0;
                    totalTimes.addAndGet(time);
                    long t = total.incrementAndGet();
                    if (t == loop)
                    {
                        end = System.currentTimeMillis();
                    }
                    thought.incrementAndGet();
                    if (time >= 100)
                    {
                        r100.incrementAndGet();
                        return;
                    }
                    if (time >= 50)
                    {
                        r50_100.incrementAndGet();
                        return;
                    }
                    if (time >= 10)
                    {
                        r10_50.incrementAndGet();
                        return;
                    }
                    r0_10.incrementAndGet();
                }
            });
            //                locks++;
        }

        //        semaphore.acquire(locks);
    }

    static UserRSIService create(int port) throws RSIClientException
    {
        RSIClientBuilder builder = new RSIClientBuilder();
        builder.getRsiClientConfig().setSubscriber(new Subscriber1(port));
        builder.getRpcClientConfig().setInitConnections(users);
        builder.getRpcClientConfig().setMaxConnections(users);
        builder.getRpcClientConfig().setReadTimeoutMills(100000);
        builder.setRpcContextDispatcher(
                new DefaultRPCContextDispatcher(Executors.newFixedThreadPool(1000), 1024 * 512, 1024 * 1024));
        RSIClient client = builder.build();
        return client.subscribeService("appCode:serverName:/user", UserRSIService.class);
    }
}

class Subscriber1 implements ServiceSubscriber
{
    private final int port;

    Subscriber1(int port)
    {
        this.port = port;
    }

    @Override
    public void onSubscribe(ServiceContext serviceContext, String serviceURI, Class<?> serviceClass, Method[] methods)
    {
        //uri = appCode:serverName:/user  + method
        //rpc client id = appCode:serverName
        //method uri = /user/test

        //uri -> {rpc client + rpc uri}
        String[] array = serviceURI.split(":/");
        for (Method method : methods)
        {
            serviceContext.addMethod(
                    new ServiceMethodInfo('/' + array[1] + '/' + serviceContext.getMethodName(method), serviceClass,
                            method));
        }

        serviceContext.setRPCClient(array[0], Collections.singleton(new Host("localhost", port)));
    }

    @Override
    public boolean isReady()
    {
        return true;
    }
}