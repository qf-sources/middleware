package test1;

import io.netty.util.ResourceLeakDetector;
import io.qiufen.common.promise.api.Context;
import io.qiufen.common.promise.spi.Resolved;
import io.qiufen.rpc.client.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.client.provider.infrastructure.SharedClientNetService;
import io.qiufen.rpc.client.spi.Host;
import io.qiufen.rpc.client.spi.dispatcher.RPCContextDispatcher;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import io.qiufen.rsi.client.api.RSIClient;
import io.qiufen.rsi.client.provider.builder.RSIClientBuilder;
import io.qiufen.rsi.client.provider.builder.async.Async;
import io.qiufen.rsi.client.spi.async.Operation;
import io.qiufen.rsi.client.spi.exception.RSIClientException;
import io.qiufen.rsi.client.spi.subscriber.ServiceContext;
import io.qiufen.rsi.client.spi.subscriber.ServiceMethodInfo;
import io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber;
import io.qiufen.rsi.common.protocol.Protocols;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

public class TestClient
{
    static AtomicLong count = new AtomicLong(0);
    static ClientNetService clientNetService = new SharedClientNetService(
            Runtime.getRuntime().availableProcessors() << 1);
    static RPCContextDispatcher rpcContextDispatcher = new DefaultRPCContextDispatcher(Executors.newFixedThreadPool(8),
            1024, 1024 * 512);

    public static void main(String[] args) throws RSIClientException, InterruptedException
    {
        new Timer().scheduleAtFixedRate(new TimerTask()
        {
            @Override
            public void run()
            {
                long c = count.get();
                count.addAndGet(-c);
                System.out.println("count = " + c);
            }
        }, 1000, 1000);

        ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.ADVANCED);

        final FirstRSIService service = create(8080);

        final String uuid = UUID.randomUUID().toString();
        final int price = 888222;
        final FirstDTO adding = new FirstDTO();
        adding.setId(987654);
        adding.setName("特价商品000001");
        adding.setPrice(3423);

        while (true)
        {
//            FirstDTO dto = service.get(uuid);
////            System.out.println(dto);
//            count.incrementAndGet();

            Async.runAndGetValue(new Operation()
            {
                @Override
                public void invoke()
                {
                    service.get(uuid);
                }
            }).then(new Resolved<Object, Object>()
            {
                @Override
                public void onResolved(Context<Object> context, Object object) throws Exception
                {
                    count.incrementAndGet();
                    context.resolve(object);
                }
            });
        }
    }

    static FirstRSIService create(int port) throws RSIClientException
    {
        RSIClientBuilder builder = new RSIClientBuilder();
        builder.getRsiClientConfig().setSubscriber(new Subscriber1(port));
        builder.getRsiClientConfig().setProtocol(Protocols.JSON);
        builder.getRpcClientConfig().setReadTimeoutMills(10000);
        RSIClient client = builder.setClientNetService(clientNetService)
                .setRpcContextDispatcher(rpcContextDispatcher)
                .build();
        return client.subscribeService("appCode:serverName:/first", FirstRSIService.class);
    }
}

class Subscriber1 implements ServiceSubscriber
{
    private final int port;

    Subscriber1(int port)
    {
        this.port = port;
    }

    @Override
    public void onSubscribe(ServiceContext serviceContext, String serviceURI, Class<?> serviceClass, Method[] methods)
    {
        //uri = appCode:serverName:/user  + method
        //rpc client id = appCode:serverName
        //method uri = /user/test

        //uri -> {rpc client + rpc uri}
        String[] array = serviceURI.split(":/");
        for (Method method : methods)
        {
            serviceContext.addMethod(
                    new ServiceMethodInfo('/' + array[1] + '/' + serviceContext.getMethodName(method), serviceClass,
                            method));
        }

        serviceContext.setRPCClient(array[0], Collections.singleton(new Host("localhost", port)));
    }

    @Override
    public boolean isReady()
    {
        return true;
    }
}