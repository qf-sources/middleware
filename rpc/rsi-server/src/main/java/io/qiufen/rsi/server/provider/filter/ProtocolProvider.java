package io.qiufen.rsi.server.provider.filter;

import io.qiufen.rsi.common.protocol.Protocol;

public interface ProtocolProvider
{
    Protocol getProtocol();

    void setProtocol(Protocol protocol);
}
