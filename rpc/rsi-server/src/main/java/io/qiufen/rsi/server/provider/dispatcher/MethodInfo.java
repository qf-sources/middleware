package io.qiufen.rsi.server.provider.dispatcher;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/11/11 10:26
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class MethodInfo
{
    private final Object service;
    private final Class<?> serviceClass;
    private final Method method;

    private final Type[] argTypes;
    private final Type returnType;

    MethodInfo(Object service, Class<?> serviceClass, Method method)
    {
        this.service = service;
        this.serviceClass = serviceClass;
        this.method = method;

        this.argTypes = method.getGenericParameterTypes();
        this.returnType = method.getGenericReturnType();
    }

    public Object getService()
    {
        return service;
    }

    public Class<?> getServiceClass()
    {
        return serviceClass;
    }

    public Method getMethod()
    {
        return method;
    }

    public Type[] getArgTypes()
    {
        return argTypes;
    }

    public Type getReturnType()
    {
        return returnType;
    }
}
