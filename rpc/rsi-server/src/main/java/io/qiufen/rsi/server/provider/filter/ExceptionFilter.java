package io.qiufen.rsi.server.provider.filter;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.qiufen.common.lang.Strings;
import io.qiufen.common.lang.TypeReference;
import io.qiufen.rpc.common.rpc.ByteBufWriter;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilter;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;
import io.qiufen.rsi.common.invocation.Constants;
import io.qiufen.rsi.common.protocol.Protocol;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public abstract class ExceptionFilter implements RPCServiceFilter, ProtocolProvider
{
    private static final Type TYPE = new TypeReference<List<String>>() {}.getType();

    private Protocol protocol;

    //todo 抽出到response接口，作为failure方法
    protected void doFailure(RPCServiceResponse response, RSIServiceExceptionInfo info)
    {
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        List<String> list = new ArrayList<String>();
        list.add(info.getErrorCode());
        if (Strings.isNotEmpty(info.getErrorMessage())) list.add(info.getErrorMessage());
        if (Strings.isNotEmpty(info.getArgs())) list.add(info.getArgs());

        protocol.toBytes(buf, byte.class, Constants.FAILURE);
        protocol.toBytes(buf, TYPE, list);
        response.writeData(ByteBufWriter.INSTANCE, buf);
    }

    @Override
    public Protocol getProtocol()
    {
        return protocol;
    }

    @Override
    public void setProtocol(Protocol protocol)
    {
        this.protocol = protocol;
    }
}
