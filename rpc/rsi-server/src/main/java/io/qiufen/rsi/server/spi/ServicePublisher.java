package io.qiufen.rsi.server.spi;

import java.lang.reflect.Method;

/**
 * 服务提供者
 *
 * @author Ruzheng Zhang
 * @date 2020/11/10 17:39
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface ServicePublisher
{
    String toMethodURI(String serviceURI, Class<?> serviceClass, Method method);

    void onProvide(ServiceInfo serviceInfo) throws Throwable;
}
