package io.qiufen.rsi.server.api;

import io.qiufen.common.provider.Provider;
import io.qiufen.rsi.common.protocol.Protocol;
import io.qiufen.rsi.server.spi.ServicePublisher;

import java.util.concurrent.Executor;

/**
 * [功能描述]
 *
 * @author Forany
 * @date 2020/8/23 23:23
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class RSIServerConfig implements Provider
{
    private String serverName;

    private Executor executor;

    private Protocol protocol;

    private ServicePublisher servicePublisher;

    private int minWaterMarkerPerChannel;

    private int maxWaterMarkerPerChannel;

    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public Executor getExecutor()
    {
        return executor;
    }

    public void setExecutor(Executor executor)
    {
        this.executor = executor;
    }

    public Protocol getProtocol()
    {
        return protocol;
    }

    public void setProtocol(Protocol protocol)
    {
        this.protocol = protocol;
    }

    public ServicePublisher getServicePublisher()
    {
        return servicePublisher;
    }

    public void setServicePublisher(ServicePublisher servicePublisher)
    {
        this.servicePublisher = servicePublisher;
    }

    public int getMinWaterMarkerPerChannel()
    {
        return minWaterMarkerPerChannel;
    }

    public void setMinWaterMarkerPerChannel(int minWaterMarkerPerChannel)
    {
        this.minWaterMarkerPerChannel = minWaterMarkerPerChannel;
    }

    public int getMaxWaterMarkerPerChannel()
    {
        return maxWaterMarkerPerChannel;
    }

    public void setMaxWaterMarkerPerChannel(int maxWaterMarkerPerChannel)
    {
        this.maxWaterMarkerPerChannel = maxWaterMarkerPerChannel;
    }
}
