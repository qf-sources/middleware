package io.qiufen.rsi.server.provider.filter;

import io.qiufen.common.filter.FilterChain;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilterContext;
import io.qiufen.rsi.common.exception.RSIServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RSIServiceExceptionFilter extends ExceptionFilter
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RSIServiceExceptionFilter.class);

    @Override
    public void doFilter(FilterChain chain, RPCServiceFilterContext context) throws Throwable
    {
        try
        {
            chain.doFilter(context);
        }
        catch (RSIServiceException e)
        {
            LOGGER.error(e.toString());
            RSIServiceExceptionInfo info = new RSIServiceExceptionInfo(e.getErrorCode(), e.getErrorMessage(),
                    e.getArgs());
            doFailure(context.getResponse(), info);
        }
    }
}
