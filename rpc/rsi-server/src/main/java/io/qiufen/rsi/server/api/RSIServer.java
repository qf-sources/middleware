package io.qiufen.rsi.server.api;

import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilter;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/11/11 9:43
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface RSIServer
{
    RSIServerConfig getConfig();

    void addService(String serviceURI, Object service, Class<?>... serviceClasses) throws ServerSideException;

    void addRPCFilter(RPCServiceFilter filter);
}
