package io.qiufen.rsi.server.provider.builder;

import io.qiufen.common.collection.ArrayUtil;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.rpc.server.api.RPCServer;
import io.qiufen.rpc.server.api.RPCServerConfig;
import io.qiufen.rpc.server.provider.builder.RPCServerBuilder;
import io.qiufen.rpc.server.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.server.provider.router.PathMatchedRPCServiceRouter;
import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.infrastructure.ServerNetService;
import io.qiufen.rpc.server.spi.router.RPCServiceRouter;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilter;
import io.qiufen.rsi.server.api.RSIServer;
import io.qiufen.rsi.server.api.RSIServerConfig;
import io.qiufen.rsi.server.provider.dispatcher.RSIDispatcher;
import io.qiufen.rsi.server.provider.filter.ProtocolProvider;
import io.qiufen.rsi.server.spi.ServiceInfo;
import io.qiufen.rsi.server.spi.ServicePublisher;

import java.lang.reflect.Method;

/**
 * 服务管理器
 */
class DefaultRSIServer implements RSIServer
{
    private final RSIServerConfig config;

    private final RPCServiceRouter<String> router;

    private final ServicePublisher servicePublisher;

    private final RPCServer rpcServer;

    private final ServerNetService serverNetService;

    private final RPCServerConfig serverConfig;

    DefaultRSIServer(ProviderContext providerContext) throws InterruptedException
    {
        this.config = providerContext.getProvider(RSIServerConfig.class);
        if (config == null) throw new IllegalArgumentException("Need rsi server config.");

        this.servicePublisher = config.getServicePublisher();
        this.serverNetService = providerContext.getProvider(ServerNetService.class);

        this.router = new PathMatchedRPCServiceRouter();
        this.serverConfig = providerContext.getProvider(RPCServerConfig.class);
        this.rpcServer = this.initRPCServer();
    }

    @Override
    public RSIServerConfig getConfig()
    {
        return config;
    }

    public void addService(String serviceURI, Object service, Class<?>... serviceClasses) throws ServerSideException
    {
        try
        {
            serviceClasses = interClasses(service, serviceClasses);
            for (Class<?> serviceClass : serviceClasses)
            {
                if ("javassist.util.proxy.ProxyObject".equals(serviceClass.getName())) continue;//排除特殊接口

                for (Method method : serviceClass.getMethods())
                {
                    String methodURI = servicePublisher.toMethodURI(serviceURI, serviceClass, method);
                    router.addRoute(methodURI, new RSIDispatcher(config, service, serviceClass, method));
                    triggerProvider(serviceURI, serviceClass, method, methodURI);
                }
            }
        }
        catch (ServerSideException e)
        {
            throw e;
        }
        catch (Throwable e)
        {
            throw new ServerSideException(e);
        }
    }

    @Override
    public void addRPCFilter(RPCServiceFilter filter)
    {
        router.addFilter(filter);
        if (filter instanceof ProtocolProvider)
        {
            ((ProtocolProvider) filter).setProtocol(config.getProtocol());
        }
    }

    private Class<?>[] interClasses(Object service, Class<?>... classes)
    {
        Class<?> serviceClass = service.getClass();
        if (ArrayUtil.isEmpty(classes))
        {
            classes = serviceClass.getInterfaces();
            if (ArrayUtil.isEmpty(classes))
            {
                throw new IllegalArgumentException("Service object not implements any interface.");
            }
            return classes;
        }
        for (Class<?> clazz : classes)
        {
            if (!clazz.isInterface())
            {
                throw new IllegalArgumentException("Found class:" + clazz + " is not an interface.");
            }
            if (!clazz.isAssignableFrom(serviceClass))
            {
                throw new IllegalArgumentException("Found class not implemented by interface:" + clazz);
            }
        }
        return classes;
    }

    private RPCServer initRPCServer() throws InterruptedException
    {
        RPCServerBuilder builder = new RPCServerBuilder();
        builder.getConfig().setHost(serverConfig.getHost());
        builder.getConfig().setPort(serverConfig.getPort());
        return builder.setServerNetService(serverNetService)
                .setRpcContextDispatcher(new DefaultRPCContextDispatcher(config.getExecutor(), router,
                        config.getMinWaterMarkerPerChannel(), config.getMaxWaterMarkerPerChannel()))
                .build();
    }

    private void triggerProvider(String serviceURI, Class<?> serviceClass, Method method, String methodURI)
            throws Throwable
    {
        ServiceInfo info = new ServiceInfo();
        info.setServerName(this.config.getServerName());
        RPCServerConfig serverConfig = this.rpcServer.getConfig();
        info.setServiceURI(serviceURI);
        info.setServiceClass(serviceClass);
        info.setMethod(method);
        info.setMethodURI(methodURI);
        info.setHost(serverConfig.getHost());
        info.setPort(serverConfig.getPort());
        servicePublisher.onProvide(info);
    }
}
