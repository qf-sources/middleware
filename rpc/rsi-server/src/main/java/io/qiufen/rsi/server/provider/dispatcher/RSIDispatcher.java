package io.qiufen.rsi.server.provider.dispatcher;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.qiufen.common.lang.Arrays;
import io.qiufen.common.lang.JsonObject;
import io.qiufen.rpc.common.rpc.ByteBufLoader;
import io.qiufen.rpc.common.rpc.ByteBufWriter;
import io.qiufen.rpc.server.spi.rpc.RPCService;
import io.qiufen.rpc.server.spi.rpc.RPCServiceRequest;
import io.qiufen.rpc.server.spi.rpc.RPCServiceResponse;
import io.qiufen.rsi.common.invocation.Constants;
import io.qiufen.rsi.common.protocol.Protocol;
import io.qiufen.rsi.server.api.RSIServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class RSIDispatcher implements RPCService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RSIDispatcher.class);

    private final Protocol protocol;
    private final Object service;
    private final Method method;
    private final MethodInfo methodInfo;

    public RSIDispatcher(RSIServerConfig config, Object service, Class<?> serviceClass, Method method)
    {
        this.protocol = config.getProtocol();
        this.service = service;
        this.method = method;
        this.methodInfo = new MethodInfo(service, serviceClass, method);
    }

    @Override
    public void doService(RPCServiceRequest request, RPCServiceResponse response) throws Throwable
    {
        Object[] args = deserializeArgs(request.getData(ByteBufLoader.INSTANCE), methodInfo);
        Object result;
        try
        {
            result = method.invoke(service, args);
        }
        catch (InvocationTargetException e)
        {
            throw e.getTargetException();
        }
        response.writeData(ByteBufWriter.INSTANCE, serializeData(result, methodInfo));
    }

    /**
     * 反序列化参数列表
     */
    private Object[] deserializeArgs(ByteBuf dataBuf, MethodInfo methodInfo)
    {
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Start deserialize arguments,data byte length:{}", dataBuf.readableBytes());
        }
        try
        {
            Type[] argTypes = methodInfo.getArgTypes();
            if (argTypes.length == 0) return Arrays.EMPTY;

            Object[] args = new Object[argTypes.length];
            for (int i = 0; i < argTypes.length; i++)
            {
                args[i] = protocol.fromBytes(dataBuf, argTypes[i]);
            }
            if (LOGGER.isDebugEnabled())
            {
                LOGGER.debug("Final arguments:{}", JsonObject.toJsonString(args));
            }
            return args;
        }
        finally
        {
            dataBuf.release();
        }
    }

    /**
     * 序列化结果
     * todo 抽出到response接口，作为success方法
     */
    private ByteBuf serializeData(Object result, MethodInfo methodInfo)
    {
        ByteBuf dataBuf = ByteBufAllocator.DEFAULT.buffer();
        //            HashMap<String, String> headers = context.attribute(AttributeKeys.HEADER_RESPONSE);
        //            protocol.toBytes(dataBuf, Constants.TYPE_HEADER, headers);

        dataBuf.writeByte(Constants.SUCCESS);
        //NULL，不需要写任何数据
        //NOT NULL，继续写数据
        if (result != null) protocol.toBytes(dataBuf, methodInfo.getReturnType(), result);
        return dataBuf;
    }
}
