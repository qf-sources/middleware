package io.qiufen.rsi.server.provider.filter;

public class RSIServiceExceptionInfo
{
    private final String errorCode;
    private final String errorMessage;
    private final String args;

    public RSIServiceExceptionInfo(String errorCode, String errorMessage, String args)
    {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.args = args;
    }

    public String getErrorCode()
    {
        return errorCode;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public String getArgs()
    {
        return args;
    }
}
