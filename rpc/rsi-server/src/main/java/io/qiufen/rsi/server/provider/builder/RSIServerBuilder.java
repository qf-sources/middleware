package io.qiufen.rsi.server.provider.builder;

import io.qiufen.common.provider.DefaultProviderContext;
import io.qiufen.rpc.server.api.RPCServerConfig;
import io.qiufen.rpc.server.provider.infrastructure.DefaultServerNetService;
import io.qiufen.rpc.server.spi.infrastructure.ServerNetService;
import io.qiufen.rsi.common.protocol.Protocols;
import io.qiufen.rsi.server.api.RSIServer;
import io.qiufen.rsi.server.api.RSIServerConfig;

import java.util.concurrent.Executors;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/11/13 15:41
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class RSIServerBuilder
{
    private final DefaultProviderContext providerContext = new DefaultProviderContext();

    private final RSIServerConfig rsiServerConfig = defaultRSIServiceConfig();

    private final RPCServerConfig rpcServerConfig = defaultRPCServerConfig();

    private ServerNetService serverNetService;

    public RSIServerConfig getRsiServiceConfig()
    {
        return rsiServerConfig;
    }

    public RPCServerConfig getRpcServerConfig()
    {
        return rpcServerConfig;
    }

    public RSIServerBuilder setServerNetService(ServerNetService serverNetService)
    {
        this.serverNetService = serverNetService;
        return this;
    }

    public RSIServer build() throws InterruptedException
    {
        if (serverNetService == null)
        {
            int cpus = Runtime.getRuntime().availableProcessors();
            int boss = cpus < 2 ? 1 : cpus >> 1;
            int workers = cpus << 2;
            serverNetService = new DefaultServerNetService(boss, workers);
        }
        providerContext.setProvider(ServerNetService.class, serverNetService);

        providerContext.setProvider(RSIServerConfig.class, rsiServerConfig);
        providerContext.setProvider(RPCServerConfig.class, rpcServerConfig);

        return new DefaultRSIServer(providerContext);
    }

    private RSIServerConfig defaultRSIServiceConfig()
    {
        RSIServerConfig config = new RSIServerConfig();
        config.setProtocol(Protocols.V1);
        config.setMinWaterMarkerPerChannel(1024 * 512);
        config.setMaxWaterMarkerPerChannel(1024 * 1024);
        config.setExecutor(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() << 1));
        return config;
    }

    private RPCServerConfig defaultRPCServerConfig()
    {
        return new RPCServerConfig();
    }
}
