package io.qiufen.rsi.server.spi;

import java.lang.reflect.Method;

public class ServiceInfo
{
    private String serverName;
    private String serviceURI;
    private Class<?> serviceClass;
    private Method method;
    private String methodURI;
    private String host;
    private int port;

    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public String getServiceURI()
    {
        return serviceURI;
    }

    public void setServiceURI(String serviceURI)
    {
        this.serviceURI = serviceURI;
    }

    public Class<?> getServiceClass()
    {
        return serviceClass;
    }

    public void setServiceClass(Class<?> serviceClass)
    {
        this.serviceClass = serviceClass;
    }

    public Method getMethod()
    {
        return method;
    }

    public void setMethod(Method method)
    {
        this.method = method;
    }

    public String getMethodURI()
    {
        return methodURI;
    }

    public void setMethodURI(String methodURI)
    {
        this.methodURI = methodURI;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    @Override
    public String toString()
    {
        return "ServiceInfo{" + "serverName='" + serverName + '\'' + ", serviceURI='" + serviceURI + '\'' + ", serviceClass=" + serviceClass + ", method=" + method + ", methodURI='" + methodURI + '\'' + ", host='" + host + '\'' + ", port=" + port + '}';
    }
}
