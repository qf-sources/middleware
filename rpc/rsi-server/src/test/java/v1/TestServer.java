package v1;

import io.qiufen.common.filter.FilterChain;
import io.qiufen.rpc.server.api.RPCServerConfig;
import io.qiufen.rpc.server.provider.infrastructure.SharedServerNetService;
import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.infrastructure.ServerNetService;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilter;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilterContext;
import io.qiufen.rsi.server.api.RSIServer;
import io.qiufen.rsi.server.api.RSIServerConfig;
import io.qiufen.rsi.server.provider.builder.RSIServerBuilder;
import io.qiufen.rsi.server.provider.filter.ExceptionFilter;
import io.qiufen.rsi.server.provider.filter.RSIServiceExceptionFilter;
import io.qiufen.rsi.server.provider.filter.RSIServiceExceptionInfo;
import io.qiufen.rsi.server.spi.ServiceInfo;
import io.qiufen.rsi.server.spi.ServicePublisher;
import test0.UserDTO;
import test0.UserRSIService;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TestServer
{
    static ServerNetService serverNetService = new SharedServerNetService(2, 8);
    static Executor executor = Executors.newFixedThreadPool(8);

    public static void main(String[] args) throws InterruptedException, ServerSideException
    {
        for (int i = 0; i < 1; i++)
        {
            create(50000 + i);
        }
    }

    static void create(int port) throws ServerSideException, InterruptedException
    {
        RSIServerBuilder builder = new RSIServerBuilder();
        final RSIServerConfig config = builder.getRsiServiceConfig();
        config.setServerName("test");
        config.setServicePublisher(new S());
        config.setExecutor(executor);
        RPCServerConfig serverConfig = builder.getRpcServerConfig();
        serverConfig.setPort(port);
        RSIServer service = builder.setServerNetService(serverNetService).build();
        service.addService("/user", new UserRSIService()
        {
            @Override
            public UserDTO getDate(String name)
            {
                return new UserDTO(name, new Date());
            }
        });
        service.addRPCFilter(new ExceptionFilter()
        {
            @Override
            public void doFilter(FilterChain chain, RPCServiceFilterContext context) throws Throwable
            {
                try
                {
                    chain.doFilter(context);
                }
                catch (RuntimeException e)
                {
                    doFailure(context.getResponse(), new RSIServiceExceptionInfo("0001", e.getMessage(), ""));
                }
            }
        });
        service.addRPCFilter(new RSIServiceExceptionFilter());
        service.addRPCFilter(new RPCServiceFilter()
        {
            @Override
            public void doFilter(FilterChain chain, RPCServiceFilterContext context) throws Throwable
            {
                chain.doFilter(context);
            }
        });
    }
}

class S implements ServicePublisher
{
    @Override
    public String toMethodURI(String serviceURI, Class<?> serviceClass, Method method)
    {
        return serviceURI + '/' + method.getName();
    }

    @Override
    public void onProvide(ServiceInfo serviceInfo)
    {
        System.out.println(serviceInfo);
    }
}