package v1;

import java.util.Date;

public interface UserRSIService
{
    Date getDate(String name);
}
