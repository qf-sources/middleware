package test1;

public interface FirstRSIService
{
    FirstDTO get(String uuid);

    long add(FirstDTO adding);
}
