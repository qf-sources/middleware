package test1;

import io.qiufen.rpc.server.api.RPCServerConfig;
import io.qiufen.rpc.server.provider.infrastructure.DefaultServerNetService;
import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.infrastructure.ServerNetService;
import io.qiufen.rsi.common.protocol.Protocols;
import io.qiufen.rsi.server.api.RSIServer;
import io.qiufen.rsi.server.api.RSIServerConfig;
import io.qiufen.rsi.server.provider.builder.RSIServerBuilder;
import io.qiufen.rsi.server.spi.ServiceInfo;
import io.qiufen.rsi.server.spi.ServicePublisher;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TestServer1
{
    static ServerNetService serverNetService = new DefaultServerNetService(2, 8);
    static Executor executor = Executors.newFixedThreadPool(8);

    public static void main(String[] args) throws InterruptedException, ServerSideException
    {
        create(8080);
    }

    static void create(int port) throws ServerSideException, InterruptedException
    {
        RSIServerBuilder builder = new RSIServerBuilder();
        final RSIServerConfig config = builder.getRsiServiceConfig();
        config.setServerName("test");
        config.setServicePublisher(new S());
        config.setProtocol(Protocols.JSON);
        config.setExecutor(executor);
        RPCServerConfig serverConfig = builder.getRpcServerConfig();
        serverConfig.setPort(port);
        RSIServer service = builder.setServerNetService(serverNetService).build();
        service.addService("/first", new FirstRSIService()
        {
            @Override
            public FirstDTO get(String uuid)
            {
                FirstDTO dto = new FirstDTO();
                dto.setId(9898989);
                dto.setName("双十一大促商品aaaaaa");
                dto.setPrice(998);
                return dto;
            }

            @Override
            public long add(FirstDTO adding)
            {
                return 0;
            }
        });
    }
}

class S implements ServicePublisher
{
    @Override
    public String toMethodURI(String serviceURI, Class<?> serviceClass, Method method)
    {
        return serviceURI + '/' + method.getName();
    }

    @Override
    public void onProvide(ServiceInfo serviceInfo)
    {
        System.out.println(serviceInfo);
    }
}