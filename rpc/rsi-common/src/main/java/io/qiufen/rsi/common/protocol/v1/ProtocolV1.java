package io.qiufen.rsi.common.protocol.v1;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.Protocol;
import io.qiufen.rsi.common.protocol.v1.provider.TypeProtocolRegistry;

import java.lang.reflect.Type;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
@SuppressWarnings("unchecked")
public class ProtocolV1 implements Protocol
{
    public ProtocolV1()
    {
    }

    @Override
    public void toBytes(ByteBuf buf, Type type, Object value)
    {
        TypeProtocolRegistry.find(type).toBytes(buf, value);
    }

    @Override
    public <T> T fromBytes(ByteBuf buf, Type type)
    {
        return (T) TypeProtocolRegistry.find(type).fromBytes(buf);
    }
}
