package io.qiufen.rsi.common.protocol.v1.spi;

import java.lang.reflect.Type;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/18 9:13
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface TypeProtocolBuilder<T>
{
    TypeProtocol<T> build(Type type);
}
