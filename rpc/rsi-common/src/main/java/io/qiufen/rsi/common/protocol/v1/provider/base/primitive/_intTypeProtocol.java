package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import java.nio.ByteBuffer;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/18 9:15
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class _intTypeProtocol extends NumberTypeProtocol<Integer>
{
    public static final _intTypeProtocol instance = new _intTypeProtocol();

    private _intTypeProtocol()
    {
    }

    @Override
    protected byte[] toBytes(Integer num)
    {
        byte[] bytes = new byte[4];
        ByteBuffer.wrap(bytes).putInt(num);
        return bytes;
    }

    @Override
    protected Integer read(byte[] bytes)
    {
        return ByteBuffer.wrap(bytes).getInt();
    }

    @Override
    protected int fullLength()
    {
        return 4;
    }
}
