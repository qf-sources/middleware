package io.qiufen.rsi.common.protocol.json;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.qiufen.common.lang.JsonObject;
import io.qiufen.rsi.common.protocol.Protocol;

import java.lang.reflect.Type;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ProtocolJson implements Protocol
{
    public ProtocolJson()
    {
    }

    @Override
    public void toBytes(ByteBuf buf, Type type, Object value)
    {
        byte[] bytes = JsonObject.toJsonBytes(value);
        buf.writeInt(bytes.length).writeBytes(bytes);
    }

    @Override
    public <T> T fromBytes(ByteBuf buf, Type type)
    {
        int length = buf.readInt();
        return JsonObject.to(new ByteBufInputStream(buf.readSlice(length)), type);
    }
}
