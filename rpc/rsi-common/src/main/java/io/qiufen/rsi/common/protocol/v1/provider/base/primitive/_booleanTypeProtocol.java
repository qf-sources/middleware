package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import io.netty.buffer.ByteBuf;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class _booleanTypeProtocol extends ByteLengthTypeProtocol<Boolean> implements PrimitiveTypeProtocol<Boolean>
{
    public static final _booleanTypeProtocol instance = new _booleanTypeProtocol();

    private static final byte FLAG_TRUE = 1;

    private _booleanTypeProtocol()
    {
    }

    @Override
    public Boolean fromBytes(ByteBuf buf, Byte head)
    {
        return (head & FLAG_TRUE) == FLAG_TRUE;
    }

    @Override
    public void toBytes(ByteBuf buf, Boolean input, Byte head)
    {
        byte _head = head;
        if (input)
        {
            _head |= FLAG_TRUE;
        }
        writeHead(buf, _head);
    }
}
