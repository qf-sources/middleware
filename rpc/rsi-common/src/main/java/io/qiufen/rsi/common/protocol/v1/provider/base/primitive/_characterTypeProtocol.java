package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import io.netty.buffer.ByteBuf;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class _characterTypeProtocol implements PrimitiveTypeProtocol<Character>
{
    public static final _characterTypeProtocol instance = new _characterTypeProtocol();

    private _characterTypeProtocol()
    {
    }

    @Override
    public void toBytes(ByteBuf buf, Character value)
    {
        buf.writeChar(value);
    }

    @Override
    public Character fromBytes(ByteBuf buf)
    {
        return buf.readChar();
    }
}
