package io.qiufen.rsi.common.invocation;

import io.qiufen.common.lang.TypeReference;

import java.lang.reflect.Type;
import java.util.HashMap;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/24 14:07
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Constants
{
    public static final byte SUCCESS = (byte) 0x00;
    public static final byte FAILURE = (byte) 0x01;

    public static final Type TYPE_HEADER = new TypeReference<HashMap<String, String>>() {}.getType();
}
