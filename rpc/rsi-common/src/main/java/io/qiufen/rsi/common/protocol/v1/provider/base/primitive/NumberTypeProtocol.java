package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import io.netty.buffer.ByteBuf;

abstract class NumberTypeProtocol<T> extends ByteLengthTypeProtocol<T> implements PrimitiveTypeProtocol<T>
{
    private static final byte MASK_LENGTH = Byte.MAX_VALUE;

    public final void toBytes(ByteBuf buf, T input, Byte head)
    {
        byte _head = head;
        byte[] numBytes = toBytes(input);
        int index = 0, length = fullLength();
        for (; index < length; index++)
        {
            if (numBytes[index] != 0) break;
        }
        int remain = length - index;
        if (remain <= 0)
        {
            writeHead(buf, _head);
        }
        else
        {
            _head |= remain;
            writeHead(buf, _head);
            buf.writeBytes(numBytes, index, remain);
        }
    }

    public final T fromBytes(ByteBuf buf, Byte head)
    {
        byte _head = head;
        int realLength = _head & MASK_LENGTH;
        int fullLength = fullLength();
        byte[] bytes = new byte[fullLength];
        if (realLength > 0)
        {
            buf.readBytes(bytes, fullLength - realLength, realLength);
        }
        return read(bytes);
    }

    protected abstract byte[] toBytes(T num);

    protected abstract T read(byte[] bytes);

    protected abstract int fullLength();
}
