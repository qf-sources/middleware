package io.qiufen.rsi.common.protocol.v1.provider.generic;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ListTypeProtocol extends CollectionTypeProtocol<List>
{
    public ListTypeProtocol(Type type)
    {
        super(type);
    }

    @Override
    protected List newInstance(Class<List> clazz)
    {
        if (clazz.isInterface())
        {
            return new ArrayList();
        }
        try
        {
            return clazz.newInstance();
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }
}