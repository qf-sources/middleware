package io.qiufen.rsi.common.protocol.v1.provider.base.box;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.base.primitive._characterTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class CharacterTypeProtocol implements TypeProtocol<Character>
{
    public static final CharacterTypeProtocol instance = new CharacterTypeProtocol();

    private static final _characterTypeProtocol _CHARACTER_TYPE_PROTOCOL = _characterTypeProtocol.instance;

    private CharacterTypeProtocol()
    {
    }

    @Override
    public void toBytes(ByteBuf buf, Character input)
    {
        if (input == null)
        {
            buf.writeBoolean(false);
        }
        else
        {
            _CHARACTER_TYPE_PROTOCOL.toBytes(buf, input);
        }
    }

    @Override
    public Character fromBytes(ByteBuf buf)
    {
        if (buf.readBoolean())
        {
            return _CHARACTER_TYPE_PROTOCOL.fromBytes(buf);
        }
        return null;
    }
}
