package io.qiufen.rsi.common.protocol;

import io.netty.buffer.ByteBuf;

import java.lang.reflect.Type;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface Protocol
{
    void toBytes(ByteBuf buf, Type type, Object value);

    <T> T fromBytes(ByteBuf buf, Type type);
}
