package io.qiufen.rsi.common.protocol.v1.provider.base;

import io.netty.buffer.ByteBuf;
import io.qiufen.common.lang.CharSequences;
import io.qiufen.rsi.common.protocol.v1.provider.ReferenceTypeProtocol;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/18 9:15
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class StringTypeProtocol extends ReferenceTypeProtocol<String>
{
    public static final StringTypeProtocol instance = new StringTypeProtocol();

    private static final Charset CHAR_SET = Charset.forName("utf-8");

    private StringTypeProtocol()
    {
        super(false);
    }

    @Override
    protected void toBytes0(ByteBuf buf, String input, int _head)
    {
        if (input.isEmpty())
        {
            writeHead(buf, _head);
            return;
        }

        ByteBuffer byteBuffer = CharSequences.toBytes(CHAR_SET, input);
        _head |= (byteBuffer.limit() << 1);
        writeHead(buf, _head);
        buf.writeBytes(byteBuffer);
    }

    @Override
    protected String fromBytes0(ByteBuf buf, int _head)
    {
        int len = _head >> 1;
        if (len <= 0) return "";

        byte[] bytes = new byte[len];
        buf.readBytes(bytes);
        return new String(bytes, CHAR_SET);
    }
}