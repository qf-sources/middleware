package io.qiufen.rsi.common.protocol.v1.provider.enu;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.TypeProtocolRegistry;
import io.qiufen.rsi.common.protocol.v1.spi.EnumValue;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/18 9:15
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class EnumTypeProtocol<V> implements TypeProtocol<EnumValue<V>>
{
    private final EnumValue<V>[] enumValues;
    private final Type valueType;
    private final TypeProtocol<V> valueTypeProtocol;

    public EnumTypeProtocol(Class<? extends EnumValue<V>> enumClass)
    {
        this.enumValues = enumClass.getEnumConstants();
        this.valueType = parseValueType(enumClass);
        this.valueTypeProtocol = TypeProtocolRegistry.find(valueType);
    }

    @Override
    public void toBytes(ByteBuf buf, EnumValue<V> value)
    {
        valueTypeProtocol.toBytes(buf, value == null ? null : value.value());
    }

    @Override
    public EnumValue<V> fromBytes(ByteBuf buf)
    {
        V value = valueTypeProtocol.fromBytes(buf);
        if (value == null) return null;

        for (EnumValue<V> enumValue : enumValues)
        {
            if (value.equals(enumValue.value())) return enumValue;
        }
        throw new IllegalArgumentException(
                "Not found enum value:" + value + ", type:" + ((Class<?>) valueType).getName());
    }

    private Type parseValueType(Class<?> enumClass)
    {
        Type[] interfaceTypes = enumClass.getGenericInterfaces();
        for (Type interfaceType : interfaceTypes)
        {
            if (interfaceType instanceof ParameterizedType)
            {
                ParameterizedType parameterizedType = (ParameterizedType) interfaceType;
                if (parameterizedType.getRawType() == EnumValue.class)
                {
                    return parameterizedType.getActualTypeArguments()[0];
                }
            }
        }
        throw new IllegalArgumentException("Not support " + EnumValue.class.getName());
    }
}
