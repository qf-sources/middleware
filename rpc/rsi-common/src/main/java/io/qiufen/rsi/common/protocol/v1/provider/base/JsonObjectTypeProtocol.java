package io.qiufen.rsi.common.protocol.v1.provider.base;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.qiufen.common.lang.JsonObject;
import io.qiufen.rsi.common.protocol.v1.provider.ReferenceTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

public class JsonObjectTypeProtocol extends ReferenceTypeProtocol<JsonObject>
{
    public static final TypeProtocol<JsonObject> instance = new JsonObjectTypeProtocol();

    private JsonObjectTypeProtocol()
    {
        super(true);
    }

    @Override
    protected void toBytes0(ByteBuf buf, JsonObject input, int _head)
    {
        int headWriterIndex = buf.writerIndex();
        writeHead(buf, _head);
        int begin = buf.readableBytes();
        input.toJsonBytes(new ByteBufOutputStream(buf));
        int end = buf.readableBytes();
        int writerIndex = buf.writerIndex();

        int length = end - begin;
        _head |= length << 1;
        buf.writerIndex(headWriterIndex);
        writeHead(buf, _head);

        buf.writerIndex(writerIndex);
    }

    @Override
    protected JsonObject fromBytes0(ByteBuf buf, int _head)
    {
        int len = _head >> 1;
        return JsonObject.of(new ByteBufInputStream(buf.readSlice(len)));
    }
}
