package io.qiufen.rsi.common.protocol.v1.provider.base.box;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.spi.HeadFieldTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
abstract class BoxTypeProtocol<H, T> implements TypeProtocol<T>
{
    private final HeadFieldTypeProtocol<H, T> headFieldTypeProtocol;

    BoxTypeProtocol(HeadFieldTypeProtocol<H, T> numberTypeProtocol)
    {
        headFieldTypeProtocol = numberTypeProtocol;
    }

    @Override
    public final void toBytes(ByteBuf buf, T input)
    {
        H head = headFieldTypeProtocol.defaultHead();
        if (input == null)
        {
            headFieldTypeProtocol.writeHead(buf, head);
            return;
        }
        head = setNotNull(head);
        headFieldTypeProtocol.toBytes(buf, input, head);
    }

    @Override
    public final T fromBytes(ByteBuf buf)
    {
        H head = headFieldTypeProtocol.readHead(buf);
        return notNull(head) ? headFieldTypeProtocol.fromBytes(buf, head) : null;
    }

    protected abstract H setNotNull(H head);

    protected abstract boolean notNull(H head);
}
