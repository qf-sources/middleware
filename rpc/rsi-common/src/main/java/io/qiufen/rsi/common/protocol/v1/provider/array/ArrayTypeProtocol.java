package io.qiufen.rsi.common.protocol.v1.provider.array;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.ReferenceTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.provider.TypeProtocolRegistry;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

import java.lang.reflect.Array;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/18 17:53
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ArrayTypeProtocol extends ReferenceTypeProtocol<Object>
{
    private final Class<?> elementType;
    private final TypeProtocol elementTypeProtocol;

    public ArrayTypeProtocol(Class<?> type)
    {
        super(false);
        this.elementType = type.getComponentType();
        this.elementTypeProtocol = TypeProtocolRegistry.find(elementType);
    }

    @Override
    protected void toBytes0(ByteBuf buf, Object input, int _head)
    {
        int len = Array.getLength(input);
        _head |= len << 1;
        writeHead(buf, _head);

        for (int i = 0; i < len; i++)
        {
            elementTypeProtocol.toBytes(buf, Array.get(input, i));
        }
    }

    @Override
    protected Object fromBytes0(ByteBuf buf, int _head)
    {
        int size = _head >> 1;
        Object result = Array.newInstance(elementType, size);
        for (int i = 0; i < size; i++)
        {
            Array.set(result, i, elementTypeProtocol.fromBytes(buf));
        }
        return result;
    }
}
