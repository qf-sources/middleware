package io.qiufen.rsi.common.protocol.v1.provider.generic;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.TypeProtocolRegistry;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
abstract class CollectionTypeProtocol<C extends Collection> extends GenericTypeProtocol<C>
{
    private final TypeProtocol elementTypeProtocol;

    CollectionTypeProtocol(Type type)
    {
        super(type);
        Type elementType = ((ParameterizedType) type).getActualTypeArguments()[0];
        this.elementTypeProtocol = TypeProtocolRegistry.find(elementType);
    }

    @Override
    protected int length(C collection)
    {
        return collection.size();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void write(ByteBuf buf, C collection)
    {
        for (Object o : collection)
        {
            elementTypeProtocol.toBytes(buf, o);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void read(ByteBuf buf, C collection, int size)
    {
        for (int i = 0; i < size; i++)
        {
            collection.add(elementTypeProtocol.fromBytes(buf));
        }
    }
}