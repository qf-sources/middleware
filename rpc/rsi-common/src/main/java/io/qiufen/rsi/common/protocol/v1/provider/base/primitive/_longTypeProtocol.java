package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import java.nio.ByteBuffer;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class _longTypeProtocol extends NumberTypeProtocol<Long>
{
    public static final _longTypeProtocol instance = new _longTypeProtocol();

    private _longTypeProtocol()
    {
    }

    @Override
    protected byte[] toBytes(Long num)
    {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putLong(num);
        return bytes;
    }

    @Override
    protected Long read(byte[] bytes)
    {
        return ByteBuffer.wrap(bytes).getLong();
    }

    @Override
    protected int fullLength()
    {
        return 8;
    }
}