package io.qiufen.rsi.common.exception;

import io.qiufen.rpc.common.exception.RPCException;

import java.lang.reflect.Method;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class RSIServiceException extends RPCException
{
    private String errorCode;
    private String errorMessage;
    private String args;
    private Class<?> serviceClass;
    private Method method;

    public RSIServiceException(String errorCode)
    {
        this.errorCode = errorCode;
    }

    public RSIServiceException(String errorCode, String errorMessage)
    {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public RSIServiceException(String errorCode, String errorMessage, String args)
    {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.args = args;
    }

    public String getErrorCode()
    {
        return errorCode;
    }

    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getArgs()
    {
        return args;
    }

    public void setArgs(String args)
    {
        this.args = args;
    }

    public Class<?> getServiceClass()
    {
        return serviceClass;
    }

    public void setServiceClass(Class<?> serviceClass)
    {
        this.serviceClass = serviceClass;
    }

    public Method getMethod()
    {
        return method;
    }

    public void setMethod(Method method)
    {
        this.method = method;
    }

    @Override
    public Throwable fillInStackTrace()
    {
        return null;
    }

    @Override
    public StackTraceElement[] getStackTrace()
    {
        return null;
    }

    @Override
    public String getMessage()
    {
        return toString();
    }

    @Override
    public String toString()
    {
        return "{" + "errorCode='" + errorCode + '\'' + ", errorMessage='" + errorMessage + '\'' + ", args='" + args + '\'' + ", serviceClass=" + serviceClass + ", method=" + method + '}';
    }
}
