package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class _floatTypeProtocol extends NumberTypeProtocol<Float>
{
    public static final _floatTypeProtocol instance = new _floatTypeProtocol();

    private _floatTypeProtocol()
    {
    }

    @Override
    protected byte[] toBytes(Float num)
    {
        byte[] bytes = new byte[4];
        ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).putFloat(num);
        return bytes;
    }

    @Override
    protected Float read(byte[] bytes)
    {
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getFloat();
    }

    @Override
    protected int fullLength()
    {
        return 4;
    }
}
