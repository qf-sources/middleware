package io.qiufen.rsi.common.protocol.v1.provider.generic;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class SetTypeProtocol extends CollectionTypeProtocol<Set>
{
    public SetTypeProtocol(Type type)
    {
        super(type);
    }

    @Override
    protected Set newInstance(Class<Set> clazz)
    {
        if (clazz.isInterface())
        {
            return new HashSet();
        }
        try
        {
            return clazz.newInstance();
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }
}