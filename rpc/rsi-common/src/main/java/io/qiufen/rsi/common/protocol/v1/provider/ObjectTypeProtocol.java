package io.qiufen.rsi.common.protocol.v1.provider;

import io.netty.buffer.ByteBuf;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ObjectTypeProtocol extends ReferenceTypeProtocol<Object>
{
    private final Class<?> clazz;

    public ObjectTypeProtocol(Class<?> clazz)
    {
        super(true);
        this.clazz = clazz;
    }

    @Override
    protected void toBytes0(ByteBuf buf, Object input, int _head)
    {
        int headWriterIndex = buf.writerIndex();
        writeHead(buf, _head);
        int begin = buf.readableBytes();
        Protocol.toBuffer(buf, input);
        int end = buf.readableBytes();
        int writerIndex = buf.writerIndex();

        int length = end - begin;
        _head |= length << 1;
        buf.writerIndex(headWriterIndex);
        writeHead(buf, _head);

        buf.writerIndex(writerIndex);
    }

    @Override
    protected Object fromBytes0(ByteBuf buf, int _head)
    {
        int len = _head >> 1;
        return Protocol.fromBuffer(buf.readSlice(len), clazz);
    }
}