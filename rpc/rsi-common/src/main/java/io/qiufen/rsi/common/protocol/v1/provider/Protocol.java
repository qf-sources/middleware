package io.qiufen.rsi.common.protocol.v1.provider;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.*;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public abstract class Protocol
{
    private Protocol()
    {
    }

    public static byte[] toBytes(Object object)
    {
        ByteBuf buf = toBuffer(object);
        try
        {
            return ByteBufUtil.getBytes(buf);
        }
        finally
        {
            buf.release();
        }
    }

    public static ByteBuf toBuffer(Object object)
    {
        Class<?> clazz = object.getClass();
        FieldMeta[] fieldMetas = ProtocolMetaCache.getMeta(clazz);
        if (fieldMetas.length <= 0) return Unpooled.EMPTY_BUFFER;

        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        try
        {
            toBuffer0(buf, fieldMetas, object);
        }
        catch (RuntimeException e)
        {
            buf.release();
            throw e;
        }
        return buf;
    }

    public static void toBuffer(ByteBuf buf, Object object)
    {
        Class<?> clazz = object.getClass();
        FieldMeta[] fieldMetas = ProtocolMetaCache.getMeta(clazz);
        if (fieldMetas.length <= 0) return;

        toBuffer0(buf, fieldMetas, object);
    }

    public static <T> T fromBuffer(ByteBuf buf, Class<T> clazz)
    {
        FieldMeta[] fieldMetas = ProtocolMetaCache.getMeta(clazz);
        if (fieldMetas.length <= 0) return null;

        T object;
        try
        {
            object = clazz.newInstance();
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }

        for (FieldMeta fieldMeta : fieldMetas)
        {
            if (!buf.isReadable())
            {
                break;
            }
            TypeProtocol typeProtocol = TypeProtocolRegistry.find(fieldMeta.getType());
            Object value = typeProtocol.fromBytes(buf);
            try
            {
                fieldMeta.getSetter().invoke(object, value);
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
        return object;
    }

    public static <T> T fromBytes(byte[] bytes, Class<T> clazz)
    {
        ByteBuf buf = Unpooled.wrappedBuffer(bytes);
        try
        {
            return fromBuffer(buf, clazz);
        }
        finally
        {
            buf.release();
        }
    }

    @SuppressWarnings("unchecked")
    private static void toBuffer0(ByteBuf buf, FieldMeta[] fieldMetas, Object object)
    {
        for (FieldMeta fieldMeta : fieldMetas)
        {
            TypeProtocol typeProtocol = TypeProtocolRegistry.find(fieldMeta.getType());
            Object value;
            try
            {
                value = fieldMeta.getGetter().invoke(object);
            }
            catch (RuntimeException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
            typeProtocol.toBytes(buf, value);
        }
    }
}

class ProtocolMetaCache
{
    private static final Map<Class<?>, FieldMeta[]> cache = new IdentityHashMap<Class<?>, FieldMeta[]>();

    static FieldMeta[] getMeta(Class<?> clazz)
    {
        FieldMeta[] metas = cache.get(clazz);
        if (null != metas)
        {
            return metas;
        }
        synchronized (cache)
        {
            metas = cache.get(clazz);
            if (null != metas)
            {
                return metas;
            }
            metas = init(clazz);
            cache.put(clazz, metas);
        }
        return cache.get(clazz);
    }

    private static FieldMeta[] init(Class<?> clazz)
    {
        PropertyDescriptor[] descriptors;
        try
        {
            descriptors = Introspector.getBeanInfo(clazz).getPropertyDescriptors();
        }
        catch (IntrospectionException e)
        {
            throw new RuntimeException(e);
        }

        List<Field> fields = new ArrayList<Field>();
        searchField(clazz, fields);
        List<FieldMeta> list = new ArrayList<FieldMeta>();
        for (Field field : fields)
        {
            for (PropertyDescriptor descriptor : descriptors)
            {
                if (!descriptor.getName().equals(field.getName()))
                {
                    continue;
                }
                list.add(
                        new FieldMeta(field.getGenericType(), descriptor.getReadMethod(), descriptor.getWriteMethod()));
                break;
            }
        }
        FieldMeta[] fieldMetas = new FieldMeta[list.size()];
        if (CollectionUtil.isNotEmpty(list))
        {
            list.toArray(fieldMetas);
            list.clear();
        }
        return fieldMetas;
    }

    private static void searchField(Class<?> clazz, List<Field> fields)
    {
        Class<?> superClass = clazz.getSuperclass();
        if (superClass != null)
        {
            searchField(superClass, fields);
        }
        Collections.addAll(fields, clazz.getDeclaredFields());
    }
}

class FieldMeta
{
    private final Type type;
    private final Method getter;
    private final Method setter;

    FieldMeta(Type type, Method getter, Method setter)
    {
        this.type = type;
        this.getter = getter;
        this.setter = setter;
    }

    Type getType()
    {
        return type;
    }

    Method getGetter()
    {
        return getter;
    }

    Method getSetter()
    {
        return setter;
    }
}