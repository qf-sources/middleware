package io.qiufen.rsi.common.protocol.v1.provider.base.box;

import io.qiufen.rsi.common.protocol.v1.provider.base.primitive._longTypeProtocol;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class LongTypeProtocol extends ByteLengthBoxTypeProtocol<Long>
{
    public static final LongTypeProtocol instance = new LongTypeProtocol();

    private LongTypeProtocol()
    {
        super(_longTypeProtocol.instance);
    }
}