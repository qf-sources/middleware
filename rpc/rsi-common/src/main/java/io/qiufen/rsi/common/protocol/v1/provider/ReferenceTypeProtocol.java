package io.qiufen.rsi.common.protocol.v1.provider;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.base.primitive._intTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.HeadFieldTypeProtocol;

public abstract class ReferenceTypeProtocol<T> extends HeadFieldTypeProtocol<Integer, T>
{
    private static final int FLAG_NTN = 1;

    private final boolean fixedHeadLength;
    private final _intTypeProtocol intTypeProtocol;

    protected ReferenceTypeProtocol(boolean fixedHeadLength)
    {
        this.fixedHeadLength = fixedHeadLength;
        this.intTypeProtocol = _intTypeProtocol.instance;
    }

    @Override
    public final void toBytes(ByteBuf buf, T input, Integer head)
    {
        int _head = head;
        if (input == null)
        {
            writeHead(buf, _head);
            return;
        }

        _head |= FLAG_NTN;
        toBytes0(buf, input, _head);
    }

    @Override
    public final T fromBytes(ByteBuf buf, Integer head)
    {
        int _head = head;
        if ((_head & FLAG_NTN) == FLAG_NTN)
        {
            return fromBytes0(buf, _head);
        }

        return null;
    }

    @Override
    public final Integer defaultHead()
    {
        return 0;
    }

    @Override
    public final void writeHead(ByteBuf buf, Integer head)
    {
        if (fixedHeadLength)
        {
            buf.writeInt(head);
        }
        else
        {
            intTypeProtocol.toBytes(buf, head);
        }
    }

    @Override
    public final Integer readHead(ByteBuf buf)
    {
        return fixedHeadLength ? buf.readInt() : intTypeProtocol.fromBytes(buf);
    }

    protected abstract void toBytes0(ByteBuf buf, T input, int head);

    protected abstract T fromBytes0(ByteBuf buf, int head);
}
