package io.qiufen.rsi.common.protocol.v1.spi;

public interface EnumValue<V>
{
    V value();
}
