package io.qiufen.rsi.common.protocol.v1.provider.base;

import io.netty.buffer.ByteBuf;
import io.qiufen.common.lang.BitSets;
import io.qiufen.rsi.common.protocol.v1.provider.base.box.LongTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

import java.util.BitSet;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class BitSetTypeProtocol implements TypeProtocol<BitSet>
{
    public static final BitSetTypeProtocol instance = new BitSetTypeProtocol();

    private final LongTypeProtocol longTypeProtocol;

    private BitSetTypeProtocol()
    {
        this.longTypeProtocol = LongTypeProtocol.instance;
    }

    @Override
    public void toBytes(ByteBuf buf, BitSet value)
    {
        longTypeProtocol.toBytes(buf, BitSets.toLong(value));
    }

    @Override
    public BitSet fromBytes(ByteBuf buf)
    {
        Long value = longTypeProtocol.fromBytes(buf);
        return BitSets.toBitSet(value);
    }
}
