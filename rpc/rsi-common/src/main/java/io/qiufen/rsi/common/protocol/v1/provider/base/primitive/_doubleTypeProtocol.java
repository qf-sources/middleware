package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class _doubleTypeProtocol extends NumberTypeProtocol<Double>
{
    public static final _doubleTypeProtocol instance = new _doubleTypeProtocol();

    private _doubleTypeProtocol()
    {
    }

    @Override
    protected byte[] toBytes(Double num)
    {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).putDouble(num);
        return bytes;
    }

    @Override
    protected Double read(byte[] bytes)
    {
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getDouble();
    }

    @Override
    protected int fullLength()
    {
        return 8;
    }
}
