package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.spi.HeadFieldTypeProtocol;

abstract class ByteLengthTypeProtocol<T> extends HeadFieldTypeProtocol<Byte, T>
{
    @Override
    public final Byte defaultHead()
    {
        return 0;
    }

    @Override
    public final void writeHead(ByteBuf buf, Byte head)
    {
        buf.writeByte(head);
    }

    @Override
    public final Byte readHead(ByteBuf buf)
    {
        return buf.readByte();
    }
}
