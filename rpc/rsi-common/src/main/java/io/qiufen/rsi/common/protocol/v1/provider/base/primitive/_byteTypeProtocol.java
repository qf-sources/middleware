package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import io.netty.buffer.ByteBuf;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class _byteTypeProtocol implements PrimitiveTypeProtocol<Byte>
{
    public static final _byteTypeProtocol instance = new _byteTypeProtocol();

    private _byteTypeProtocol()
    {
    }

    @Override
    public void toBytes(ByteBuf buf, Byte input)
    {
        buf.writeByte(input);
    }

    @Override
    public Byte fromBytes(ByteBuf buf)
    {
        return buf.readByte();
    }
}
