package io.qiufen.rsi.common.protocol.v1.provider.generic;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.ReferenceTypeProtocol;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
abstract class GenericTypeProtocol<C> extends ReferenceTypeProtocol<C>
{
    private final Class<C> clazz;

    @SuppressWarnings("unchecked")
    GenericTypeProtocol(Type genericType)
    {
        super(false);
        this.clazz = (Class<C>) ((ParameterizedType) genericType).getRawType();
    }

    @Override
    protected void toBytes0(ByteBuf buf, C input, int head)
    {
        head |= length(input) << 1;
        writeHead(buf, head);
        write(buf, input);
    }

    @Override
    protected C fromBytes0(ByteBuf buf, int head)
    {
        int length = head >> 1;
        C c = newInstance(clazz);
        read(buf, c, length);
        return c;
    }

    protected abstract int length(C value);

    protected abstract void write(ByteBuf buf, C value);

    protected abstract void read(ByteBuf buf, C value, int length);

    protected abstract C newInstance(Class<C> clazz);
}