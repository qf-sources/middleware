package io.qiufen.rsi.common;

import io.qiufen.common.lang.Strings;

import java.lang.reflect.Method;

public class Util
{
    public static String toMethodURI(String serviceURI, Method method)
    {
        Alias aliasAnn = method.getAnnotation(Alias.class);
        String methodName;
        if (aliasAnn == null || Strings.isEmpty(aliasAnn.value()))
        {
            methodName = method.getName();
        }
        else
        {
            methodName = aliasAnn.value();
        }
        return serviceURI + '/' + methodName;
    }
}
