package io.qiufen.rsi.common.protocol.v1.provider.base.box;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.base.primitive._byteTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ByteTypeProtocol implements TypeProtocol<Byte>
{
    public static final ByteTypeProtocol instance = new ByteTypeProtocol();

    private static final _byteTypeProtocol _BYTE_TYPE_PROTOCOL = _byteTypeProtocol.instance;

    private ByteTypeProtocol()
    {
    }

    @Override
    public void toBytes(ByteBuf buf, Byte input)
    {
        if (input == null)
        {
            buf.writeBoolean(false);
        }
        else
        {
            buf.writeBoolean(true);
            _BYTE_TYPE_PROTOCOL.toBytes(buf, input);
        }
    }

    @Override
    public Byte fromBytes(ByteBuf buf)
    {
        return buf.readBoolean() ? _BYTE_TYPE_PROTOCOL.fromBytes(buf) : null;
    }
}
