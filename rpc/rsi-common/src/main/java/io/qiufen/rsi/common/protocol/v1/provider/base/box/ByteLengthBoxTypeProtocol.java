package io.qiufen.rsi.common.protocol.v1.provider.base.box;

import io.qiufen.rsi.common.protocol.v1.spi.HeadFieldTypeProtocol;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
abstract class ByteLengthBoxTypeProtocol<T> extends BoxTypeProtocol<Byte, T>
{
    private static final byte FLAG_NTN = (byte) 0x80;

    ByteLengthBoxTypeProtocol(HeadFieldTypeProtocol<Byte, T> headFieldTypeProtocol)
    {
        super(headFieldTypeProtocol);
    }

    @Override
    protected final Byte setNotNull(Byte head)
    {
        byte _head = head;
        _head |= FLAG_NTN;
        return _head;
    }

    @Override
    protected final boolean notNull(Byte head)
    {
        return (head & FLAG_NTN) == FLAG_NTN;
    }
}
