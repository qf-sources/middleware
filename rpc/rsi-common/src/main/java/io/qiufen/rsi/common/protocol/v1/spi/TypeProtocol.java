package io.qiufen.rsi.common.protocol.v1.spi;

import io.netty.buffer.ByteBuf;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/18 9:13
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface TypeProtocol<T>
{
    void toBytes(ByteBuf buf, T input);

    T fromBytes(ByteBuf buf);
}