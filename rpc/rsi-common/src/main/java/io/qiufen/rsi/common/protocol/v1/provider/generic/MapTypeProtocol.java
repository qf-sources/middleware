package io.qiufen.rsi.common.protocol.v1.provider.generic;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.TypeProtocolRegistry;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class MapTypeProtocol extends GenericTypeProtocol<Map>
{
    private final TypeProtocol keyTypeProtocol;
    private final TypeProtocol valueTypeProtocol;

    public MapTypeProtocol(Type type)
    {
        super(type);
        Type[] argTypes = ((ParameterizedType) type).getActualTypeArguments();
        this.keyTypeProtocol = TypeProtocolRegistry.find(argTypes[0]);
        this.valueTypeProtocol = TypeProtocolRegistry.find(argTypes[1]);
    }

    @Override
    protected int length(Map map)
    {
        return map.size();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void write(ByteBuf buf, Map map)
    {
        for (Object o : map.entrySet())
        {
            Map.Entry entry = (Map.Entry) o;
            keyTypeProtocol.toBytes(buf, entry.getKey());
            valueTypeProtocol.toBytes(buf, entry.getValue());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void read(ByteBuf buf, Map map, int size)
    {
        for (int i = 0; i < size; i++)
        {
            Object key = keyTypeProtocol.fromBytes(buf);
            Object value = valueTypeProtocol.fromBytes(buf);
            map.put(key, value);
        }
    }

    @Override
    protected Map newInstance(Class<Map> clazz)
    {
        if (clazz.isInterface())
        {
            return new HashMap();
        }
        try
        {
            return clazz.newInstance();
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }
}