package io.qiufen.rsi.common.protocol.v1.provider.base;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.TypeProtocolRegistry;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

import java.math.BigInteger;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class BigIntegerTypeProtocol implements TypeProtocol<BigInteger>
{
    public static final BigIntegerTypeProtocol instance = new BigIntegerTypeProtocol();

    private final TypeProtocol<byte[]> arrayTypeProtocol;

    private BigIntegerTypeProtocol()
    {
        this.arrayTypeProtocol = TypeProtocolRegistry.find(byte[].class);
    }

    @Override
    public void toBytes(ByteBuf buf, BigInteger value)
    {
        arrayTypeProtocol.toBytes(buf, value == null ? null : value.toByteArray());
    }

    @Override
    public BigInteger fromBytes(ByteBuf buf)
    {
        byte[] bytes = arrayTypeProtocol.fromBytes(buf);
        return bytes == null ? null : new BigInteger(bytes);
    }
}
