package io.qiufen.rsi.common.protocol.v1.provider.base.box;

import io.qiufen.rsi.common.protocol.v1.provider.base.primitive._doubleTypeProtocol;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class DoubleTypeProtocol extends ByteLengthBoxTypeProtocol<Double>
{
    public static final DoubleTypeProtocol instance = new DoubleTypeProtocol();

    private DoubleTypeProtocol()
    {
        super(_doubleTypeProtocol.instance);
    }
}
