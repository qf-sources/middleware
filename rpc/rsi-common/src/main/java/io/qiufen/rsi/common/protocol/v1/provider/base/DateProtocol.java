package io.qiufen.rsi.common.protocol.v1.provider.base;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.base.box.LongTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

import java.util.Date;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/8/18 9:14
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class DateProtocol implements TypeProtocol<Date>
{
    public static final DateProtocol instance = new DateProtocol();

    private static final LongTypeProtocol LONG_TYPE_PROTOCOL = LongTypeProtocol.instance;

    private DateProtocol()
    {
    }

    @Override
    public void toBytes(ByteBuf buf, Date date)
    {
        LONG_TYPE_PROTOCOL.toBytes(buf, null == date ? null : date.getTime());
    }

    @Override
    public Date fromBytes(ByteBuf buf)
    {
        Long timestamp = LONG_TYPE_PROTOCOL.fromBytes(buf);
        if (null == timestamp)
        {
            return null;
        }
        return new Date(timestamp);
    }
}