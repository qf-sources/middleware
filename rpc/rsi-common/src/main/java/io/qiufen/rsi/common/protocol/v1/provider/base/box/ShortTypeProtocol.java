package io.qiufen.rsi.common.protocol.v1.provider.base.box;

import io.qiufen.rsi.common.protocol.v1.provider.base.primitive._shortTypeProtocol;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ShortTypeProtocol extends ByteLengthBoxTypeProtocol<Short>
{
    public static final ShortTypeProtocol instance = new ShortTypeProtocol();

    private ShortTypeProtocol()
    {
        super(_shortTypeProtocol.instance);
    }
}
