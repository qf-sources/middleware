package io.qiufen.rsi.common.protocol.v1.provider.base;

import io.netty.buffer.ByteBuf;
import io.qiufen.rsi.common.protocol.v1.provider.base.primitive._intTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class BigDecimalTypeProtocol implements TypeProtocol<BigDecimal>
{
    public static final BigDecimalTypeProtocol instance = new BigDecimalTypeProtocol();

    private final BigIntegerTypeProtocol bigIntegerTypeProtocol;
    private final _intTypeProtocol intTypeProtocol;

    private BigDecimalTypeProtocol()
    {
        this.bigIntegerTypeProtocol = BigIntegerTypeProtocol.instance;
        this.intTypeProtocol = _intTypeProtocol.instance;
    }

    @Override
    public void toBytes(ByteBuf buf, BigDecimal value)
    {
        if (value == null)
        {
            bigIntegerTypeProtocol.toBytes(buf, null);
        }
        else
        {
            bigIntegerTypeProtocol.toBytes(buf, value.unscaledValue());
            intTypeProtocol.toBytes(buf, value.scale());
        }
    }

    @Override
    public BigDecimal fromBytes(ByteBuf buf)
    {
        BigInteger bigInteger = bigIntegerTypeProtocol.fromBytes(buf);
        if (bigInteger == null)
        {
            return null;
        }

        int scale = intTypeProtocol.fromBytes(buf);
        return new BigDecimal(bigInteger, scale);
    }
}
