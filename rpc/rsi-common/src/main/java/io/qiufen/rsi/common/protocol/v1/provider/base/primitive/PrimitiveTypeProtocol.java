package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;

public interface PrimitiveTypeProtocol<P> extends TypeProtocol<P> {}
