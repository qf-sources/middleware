package io.qiufen.rsi.common.protocol.v1.provider.base.primitive;

import java.nio.ByteBuffer;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class _shortTypeProtocol extends NumberTypeProtocol<Short>
{
    public static final _shortTypeProtocol instance = new _shortTypeProtocol();

    private _shortTypeProtocol()
    {
    }

    @Override
    protected byte[] toBytes(Short num)
    {
        byte[] numBytes = new byte[2];
        ByteBuffer.wrap(numBytes).putShort(num);
        return numBytes;
    }

    @Override
    protected Short read(byte[] bytes)
    {
        return ByteBuffer.wrap(bytes).getShort();
    }

    @Override
    protected int fullLength()
    {
        return 2;
    }
}
