package io.qiufen.rsi.common.protocol.v1.provider;

import io.qiufen.common.lang.JsonObject;
import io.qiufen.rsi.common.protocol.v1.provider.array.ArrayTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.provider.base.*;
import io.qiufen.rsi.common.protocol.v1.provider.base.box.*;
import io.qiufen.rsi.common.protocol.v1.provider.base.primitive.*;
import io.qiufen.rsi.common.protocol.v1.provider.enu.EnumTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.provider.generic.ListTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.provider.generic.MapTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.provider.generic.SetTypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.EnumValue;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocol;
import io.qiufen.rsi.common.protocol.v1.spi.TypeProtocolBuilder;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class TypeProtocolRegistry
{
    private static final Map<Type, TypeProtocol> CACHE_FIND = new IdentityHashMap<Type, TypeProtocol>();
    private static final Map<Class, TypeProtocol> simpleTypeProtocolMap = new IdentityHashMap<Class, TypeProtocol>();
    private static final Map<Class, TypeProtocolBuilder> genericTypeProtocolMap = new IdentityHashMap<Class, TypeProtocolBuilder>();

    static
    {
        //---------------------- 简单类型 ------------------------
        simpleTypeProtocolMap.put(boolean.class, _booleanTypeProtocol.instance);
        simpleTypeProtocolMap.put(byte.class, _byteTypeProtocol.instance);
        simpleTypeProtocolMap.put(short.class, _shortTypeProtocol.instance);
        simpleTypeProtocolMap.put(int.class, _intTypeProtocol.instance);
        simpleTypeProtocolMap.put(long.class, _longTypeProtocol.instance);
        simpleTypeProtocolMap.put(float.class, _floatTypeProtocol.instance);
        simpleTypeProtocolMap.put(double.class, _doubleTypeProtocol.instance);
        simpleTypeProtocolMap.put(char.class, _characterTypeProtocol.instance);

        simpleTypeProtocolMap.put(Boolean.class, BooleanTypeProtocol.instance);
        simpleTypeProtocolMap.put(Byte.class, ByteTypeProtocol.instance);
        simpleTypeProtocolMap.put(Short.class, ShortTypeProtocol.instance);
        simpleTypeProtocolMap.put(Integer.class, IntTypeProtocol.instance);
        simpleTypeProtocolMap.put(Long.class, LongTypeProtocol.instance);
        simpleTypeProtocolMap.put(Float.class, FloatTypeProtocol.instance);
        simpleTypeProtocolMap.put(Double.class, DoubleTypeProtocol.instance);
        simpleTypeProtocolMap.put(Character.class, CharacterTypeProtocol.instance);

        simpleTypeProtocolMap.put(String.class, StringTypeProtocol.instance);
        simpleTypeProtocolMap.put(Date.class, DateProtocol.instance);
        simpleTypeProtocolMap.put(BigDecimal.class, BigDecimalTypeProtocol.instance);
        simpleTypeProtocolMap.put(BigInteger.class, BigIntegerTypeProtocol.instance);
        simpleTypeProtocolMap.put(JsonObject.class, JsonObjectTypeProtocol.instance);
        simpleTypeProtocolMap.put(BitSet.class, BitSetTypeProtocol.instance);

        //---------------------- 泛型 ------------------------
        genericTypeProtocolMap.put(List.class, new TypeProtocolBuilder()
        {
            @Override
            public TypeProtocol build(Type type)
            {
                return new ListTypeProtocol(type);
            }
        });
        genericTypeProtocolMap.put(ArrayList.class, genericTypeProtocolMap.get(List.class));
        genericTypeProtocolMap.put(LinkedList.class, genericTypeProtocolMap.get(List.class));
        genericTypeProtocolMap.put(Vector.class, genericTypeProtocolMap.get(List.class));

        genericTypeProtocolMap.put(Set.class, new TypeProtocolBuilder()
        {
            @Override
            public TypeProtocol build(Type type)
            {
                return new SetTypeProtocol(type);
            }
        });
        genericTypeProtocolMap.put(HashSet.class, genericTypeProtocolMap.get(Set.class));
        genericTypeProtocolMap.put(LinkedHashSet.class, genericTypeProtocolMap.get(Set.class));

        genericTypeProtocolMap.put(Map.class, new TypeProtocolBuilder()
        {
            @Override
            public TypeProtocol build(Type type)
            {
                return new MapTypeProtocol(type);
            }
        });
        genericTypeProtocolMap.put(HashMap.class, genericTypeProtocolMap.get(Map.class));
        genericTypeProtocolMap.put(LinkedHashMap.class, genericTypeProtocolMap.get(Map.class));
        genericTypeProtocolMap.put(Hashtable.class, genericTypeProtocolMap.get(Map.class));
    }

    public static <T> void addSimpleTypeProtocol(Class<T> clazz, TypeProtocol<T> typeProtocol)
    {
        simpleTypeProtocolMap.put(clazz, typeProtocol);
    }

    public static <T> void addGenericTypeProtocol(Class<T> clazz, TypeProtocolBuilder<T> factory)
    {
        genericTypeProtocolMap.put(clazz, factory);
    }

    public static <T> TypeProtocol<T> find(Type type)
    {
        TypeProtocol typeProtocol = CACHE_FIND.get(type);
        if (null != typeProtocol)
        {
            return typeProtocol;
        }
        synchronized (CACHE_FIND)
        {
            typeProtocol = CACHE_FIND.get(type);
            if (null != typeProtocol)
            {
                return typeProtocol;
            }
            typeProtocol = find0(type);
            CACHE_FIND.put(type, typeProtocol);
        }
        return typeProtocol;
    }

    private static TypeProtocol<?> find0(Type type)
    {
        //简单类型
        if (type instanceof Class)
        {
            //优先匹配简单类型
            TypeProtocol<?> typeProtocol = simpleTypeProtocolMap.get(type);
            if (null != typeProtocol)
            {
                return typeProtocol;
            }
            Class<?> clazz = (Class<?>) type;
            //匹配数组类型
            if (clazz.isArray())
            {
                return new ArrayTypeProtocol(clazz);
            }
            //匹配枚举类型
            if (clazz.isEnum() && EnumValue.class.isAssignableFrom(clazz))
            {
                return new EnumTypeProtocol(clazz);
            }
            //匹配Object类型
            return new ObjectTypeProtocol((Class<?>) type);
        }

        //泛型
        Type type0 = ((ParameterizedType) type).getRawType();
        if (type0 instanceof Class)
        {
            TypeProtocolBuilder<?> builder = genericTypeProtocolMap.get(type0);
            if (null == builder)
            {
                throw new UnsupportedOperationException("Not support type:" + type0);
            }
            return builder.build(type);
        }

        throw new UnsupportedOperationException("Not support type:" + type);
    }
}
