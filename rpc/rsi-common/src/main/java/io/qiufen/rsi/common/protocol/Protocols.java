package io.qiufen.rsi.common.protocol;

import io.qiufen.rsi.common.protocol.json.ProtocolJson;
import io.qiufen.rsi.common.protocol.v1.ProtocolV1;

/**
 * [功能描述]
 *
 * @author Forany
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Protocols
{
    /**
     * 可应用于生产
     */
    public static final Protocol JSON = new ProtocolJson();

    /**
     * 可应用于生产
     */
    public static final Protocol V1 = new ProtocolV1();
}
