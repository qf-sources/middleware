package io.qiufen.rsi.common.protocol.v1.spi;

import io.netty.buffer.ByteBuf;

public abstract class HeadFieldTypeProtocol<H, T> implements TypeProtocol<T>
{
    @Override
    public final void toBytes(ByteBuf buf, T input)
    {
        toBytes(buf, input, defaultHead());
    }

    @Override
    public final T fromBytes(ByteBuf buf)
    {
        return fromBytes(buf, readHead(buf));
    }

    public abstract H defaultHead();

    public abstract void writeHead(ByteBuf buf, H head);

    public abstract H readHead(ByteBuf buf);

    public abstract void toBytes(ByteBuf buf, T input, H head);

    public abstract T fromBytes(ByteBuf buf, H head);
}
