package protocol.v1;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.qiufen.rsi.common.protocol.Protocols;
import protocol.Bean;

public class Test01
{
    static int loops = 20;
    static long times = 1000000;

    public static void main(String[] args)
    {
        ByteBuf buf = Unpooled.buffer();
        Bean bean0 = new Bean()
        {{
            setBytes(new byte[]{1, 2, 3});
        }};
        System.out.println("bean = " + bean0);
        Protocols.V1.toBytes(buf, Bean.class, bean0);
        System.out.println("buf.readableBytes() = " + buf.readableBytes());
        Bean bean1 = Protocols.V1.fromBytes(buf, Bean.class);
        System.out.println("bean = " + bean1);
        System.out.println("equal = " + (bean1.toString().equals(bean0.toString())));

        t();
    }

    static void t()
    {
        for (int i = 0; i < loops; i++)
        {
            test();
        }
    }

    static void test()
    {
        Bean bean = new Bean()
        {{
            setBytes(new byte[]{1, 2, 3});
        }};

        long sum = 0;
        for (long i = 0; i < times; i++)
        {
            ByteBuf buf = Unpooled.buffer();
            long p0 = System.nanoTime();
            Protocols.V1.toBytes(buf, Bean.class, bean);
            bean = Protocols.V1.fromBytes(buf, Bean.class);
            long p1 = System.nanoTime();
            sum += p1 - p0;
            buf.release();
        }
        System.out.println("avg = " + sum / times);
    }
}
