package protocol;

import io.qiufen.common.lang.JsonObject;

import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;

public class Bean
{
    private byte[] bytes;

    private Boolean a1 = new Random().nextBoolean();

    private Byte a2 = (byte) new Random().nextInt();

    private Short a3 = (short) new Random().nextInt();

    private Integer a4 = new Random().nextInt();

    private Long a5 = new Random().nextLong();

    private Float a6 = new Random().nextFloat();

    private Double a7 = new Random().nextDouble();

    private BigDecimal a8 = BigDecimal.valueOf(new Random().nextDouble());

    private String a9 = UUID.randomUUID().toString();

    private boolean a10 = new Random().nextBoolean();

    private byte a11 = (byte) new Random().nextInt();

    private short a12 = (short) new Random().nextInt();

    private int a13 = new Random().nextInt();

    private long a14 = new Random().nextLong();

    private float a15 = new Random().nextFloat();

    private double a16 = new Random().nextDouble();

    private A18 a18 = A18.B;

    public byte[] getBytes()
    {
        return bytes;
    }

    public void setBytes(byte[] bytes)
    {
        this.bytes = bytes;
    }

    public Boolean getA1()
    {
        return a1;
    }

    public void setA1(Boolean a1)
    {
        this.a1 = a1;
    }

    public Byte getA2()
    {
        return a2;
    }

    public void setA2(Byte a2)
    {
        this.a2 = a2;
    }

    public Short getA3()
    {
        return a3;
    }

    public void setA3(Short a3)
    {
        this.a3 = a3;
    }

    public Integer getA4()
    {
        return a4;
    }

    public void setA4(Integer a4)
    {
        this.a4 = a4;
    }

    public Long getA5()
    {
        return a5;
    }

    public void setA5(Long a5)
    {
        this.a5 = a5;
    }

    public Float getA6()
    {
        return a6;
    }

    public void setA6(Float a6)
    {
        this.a6 = a6;
    }

    public Double getA7()
    {
        return a7;
    }

    public void setA7(Double a7)
    {
        this.a7 = a7;
    }

    public BigDecimal getA8()
    {
        return a8;
    }

    public void setA8(BigDecimal a8)
    {
        this.a8 = a8;
    }

    public String getA9()
    {
        return a9;
    }

    public void setA9(String a9)
    {
        this.a9 = a9;
    }

    public boolean isA10()
    {
        return a10;
    }

    public void setA10(boolean a10)
    {
        this.a10 = a10;
    }

    public byte getA11()
    {
        return a11;
    }

    public void setA11(byte a11)
    {
        this.a11 = a11;
    }

    public short getA12()
    {
        return a12;
    }

    public void setA12(short a12)
    {
        this.a12 = a12;
    }

    public int getA13()
    {
        return a13;
    }

    public void setA13(int a13)
    {
        this.a13 = a13;
    }

    public long getA14()
    {
        return a14;
    }

    public void setA14(long a14)
    {
        this.a14 = a14;
    }

    public float getA15()
    {
        return a15;
    }

    public void setA15(float a15)
    {
        this.a15 = a15;
    }

    public double getA16()
    {
        return a16;
    }

    public void setA16(double a16)
    {
        this.a16 = a16;
    }

    public A18 getA18()
    {
        return a18;
    }

    public void setA18(A18 a18)
    {
        this.a18 = a18;
    }

    @Override
    public String toString()
    {
        return JsonObject.toJsonString(this);
    }
}