package protocol;

import io.qiufen.rsi.common.protocol.v1.spi.EnumValue;

import java.util.UUID;

public enum A18 implements EnumValue<String>
{
    A(UUID.randomUUID().toString()),
    B(UUID.randomUUID().toString());

    private final String uuid;

    A18(String uuid) {this.uuid = uuid;}

    @Override
    public String value()
    {
        return uuid;
    }
}
