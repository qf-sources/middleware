package io.qiufen.rsi.module.client.provider.feature;

import io.qiufen.module.kernel.spi.value.Value;
import io.qiufen.rsi.client.spi.rpc.RPCClientContextProvider;
import io.qiufen.rsi.module.client.spi.ServiceSubscriber;

public class RSIClientDeclarationOptions
{
    private final RSIClientDeclaration declaration;

    RSIClientDeclarationOptions(RSIClientDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public RSIClientDeclarationOptions serviceSubscriber(Class<? extends ServiceSubscriber> type)
    {
        this.declaration.setServiceSubscriberClass(type);
        return this;
    }

    public RSIClientDeclarationOptions id(Object clientId)
    {
        this.declaration.setClientId(clientId);
        return this;
    }

    public RSIClientDeclarationOptions rpcClientContext(Class<? extends RPCClientContextProvider> type)
    {
        this.declaration.setRpcClientContextProviderClass(type);
        return this;
    }

    public RSIClientDeclarationOptions executorService(Value executorService)
    {
        this.declaration.setExecutorService(executorService);
        return this;
    }
}
