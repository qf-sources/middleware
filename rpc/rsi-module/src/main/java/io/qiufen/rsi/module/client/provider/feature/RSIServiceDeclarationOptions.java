package io.qiufen.rsi.module.client.provider.feature;

public class RSIServiceDeclarationOptions
{
    private final RSIServiceDeclaration declaration;

    RSIServiceDeclarationOptions(RSIServiceDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public RSIServiceDeclarationOptions id(Object id)
    {
        this.declaration.setId(id);
        return this;
    }

    public RSIServiceDeclarationOptions to(Object clientId)
    {
        this.declaration.setClientId(clientId);
        return this;
    }
}
