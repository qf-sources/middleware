package io.qiufen.rsi.module.server.provider.feature;

public class RSIServiceDeclarationOptions
{
    private final RSIServiceDeclaration declaration;

    RSIServiceDeclarationOptions(RSIServiceDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public RSIServiceDeclarationOptions to(Object serverId)
    {
        this.declaration.setServerId(serverId);
        return this;
    }
}
