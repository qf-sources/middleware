package io.qiufen.rsi.module.client.provider.feature;

import java.util.List;

public class RSIClientBinder
{
    private final List<RSIClientDeclaration> clientDeclarations;
    private final List<RSIServiceDeclaration> serviceDeclarations;

    RSIClientBinder(List<RSIClientDeclaration> clientDeclarations, List<RSIServiceDeclaration> serviceDeclarations)
    {
        this.clientDeclarations = clientDeclarations;
        this.serviceDeclarations = serviceDeclarations;
    }

    public RSIClientDeclarationOptions bindClient()
    {
        RSIClientDeclaration declaration = new RSIClientDeclaration();
        this.clientDeclarations.add(declaration);
        return new RSIClientDeclarationOptions(declaration);
    }

    public RSIServiceDeclarationOptions bindService(String uri, Class<?> type)
    {
        RSIServiceDeclaration declaration = new RSIServiceDeclaration();
        declaration.setUri(uri);
        declaration.setType(type);
        this.serviceDeclarations.add(declaration);
        return new RSIServiceDeclarationOptions(declaration);
    }
}
