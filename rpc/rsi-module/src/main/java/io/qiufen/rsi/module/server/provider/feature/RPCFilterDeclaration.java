package io.qiufen.rsi.module.server.provider.feature;

public class RPCFilterDeclaration
{
    private Object objectId;
    private Class<?> type;

    private Object serverId;

    RPCFilterDeclaration(Object objectId)
    {
        this.objectId = objectId;
    }

    RPCFilterDeclaration(Class<?> type)
    {
        this.type = type;
    }

    public RPCFilterDeclaration to(Object serverId)
    {
        this.serverId = serverId;
        return this;
    }

    Object getObjectId()
    {
        return objectId;
    }

    Class<?> getType()
    {
        return type;
    }

    Object getServerId()
    {
        return serverId;
    }
}
