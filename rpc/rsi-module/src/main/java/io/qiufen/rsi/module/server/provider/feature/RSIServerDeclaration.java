package io.qiufen.rsi.module.server.provider.feature;

import io.qiufen.module.kernel.spi.value.Value;
import io.qiufen.rsi.common.protocol.Protocol;
import io.qiufen.rsi.server.spi.ServicePublisher;

public class RSIServerDeclaration
{
    private Value port;
    private Value host;
    private Object serverId;
    private Class<? extends ServicePublisher> servicePublisherClass;
    private Value executorService;
    private Protocol protocol;

    Value getPort()
    {
        return port;
    }

    void setPort(Value port)
    {
        this.port = port;
    }

    Value getHost()
    {
        return host;
    }

    void setHost(Value host)
    {
        this.host = host;
    }

    Object getServerId()
    {
        return serverId;
    }

    void setServerId(Object serverId)
    {
        this.serverId = serverId;
    }

    public Class<? extends ServicePublisher> getServicePublisherClass()
    {
        return servicePublisherClass;
    }

    public void setServicePublisherClass(Class<? extends ServicePublisher> servicePublisherClass)
    {
        this.servicePublisherClass = servicePublisherClass;
    }

    public Value getExecutorService()
    {
        return executorService;
    }

    public void setExecutorService(Value executorService)
    {
        this.executorService = executorService;
    }

    public Protocol getProtocol()
    {
        return protocol;
    }

    public void setProtocol(Protocol protocol)
    {
        this.protocol = protocol;
    }
}
