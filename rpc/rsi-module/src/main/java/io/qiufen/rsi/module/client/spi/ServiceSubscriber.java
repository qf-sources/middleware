package io.qiufen.rsi.module.client.spi;

public interface ServiceSubscriber extends io.qiufen.rsi.client.spi.subscriber.ServiceSubscriber
{
    void onPostModule();
}
