package io.qiufen.rsi.module.client.provider.feature;

import io.qiufen.module.kernel.spi.value.Value;
import io.qiufen.rsi.client.spi.rpc.RPCClientContextProvider;
import io.qiufen.rsi.module.client.spi.ServiceSubscriber;

public class RSIClientDeclaration
{
    private Object clientId;
    private Class<? extends ServiceSubscriber> serviceSubscriberClass;
    private Class<? extends RPCClientContextProvider> rpcClientContextProviderClass;
    private Value executorService;

    Object getClientId()
    {
        return clientId;
    }

    void setClientId(Object clientId)
    {
        this.clientId = clientId;
    }

    Class<? extends ServiceSubscriber> getServiceSubscriberClass()
    {
        return serviceSubscriberClass;
    }

    void setServiceSubscriberClass(Class<? extends ServiceSubscriber> serviceSubscriberClass)
    {
        this.serviceSubscriberClass = serviceSubscriberClass;
    }

    Class<? extends RPCClientContextProvider> getRpcClientContextProviderClass()
    {
        return rpcClientContextProviderClass;
    }

    void setRpcClientContextProviderClass(Class<? extends RPCClientContextProvider> rpcClientContextProviderClass)
    {
        this.rpcClientContextProviderClass = rpcClientContextProviderClass;
    }

    Value getExecutorService()
    {
        return executorService;
    }

    void setExecutorService(Value executorService)
    {
        this.executorService = executorService;
    }
}
