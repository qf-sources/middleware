package io.qiufen.rsi.module.server.provider.feature;

public class RSIServiceDeclaration
{
    private String uri;

    private Object objectId;
    private Class<?> type;

    private Object serverId;

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    public Object getObjectId()
    {
        return objectId;
    }

    public void setObjectId(Object objectId)
    {
        this.objectId = objectId;
    }

    public Class<?> getType()
    {
        return type;
    }

    public void setType(Class<?> type)
    {
        this.type = type;
    }

    public Object getServerId()
    {
        return serverId;
    }

    public void setServerId(Object serverId)
    {
        this.serverId = serverId;
    }
}
