package io.qiufen.rsi.module.client.provider.feature;

public class RSIServiceDeclaration
{
    private String uri;

    private Class<?> type;

    private Object clientId;

    private Object id;

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    public Class<?> getType()
    {
        return type;
    }

    public void setType(Class<?> type)
    {
        this.type = type;
    }

    public Object getClientId()
    {
        return clientId;
    }

    public void setClientId(Object clientId)
    {
        this.clientId = clientId;
    }

    public Object getId()
    {
        return id;
    }

    public void setId(Object id)
    {
        this.id = id;
    }
}
