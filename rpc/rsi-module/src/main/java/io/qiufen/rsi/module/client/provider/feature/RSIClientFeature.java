package io.qiufen.rsi.module.client.provider.feature;

import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

@Handler(RSIClientFeatureHandler.class)
public interface RSIClientFeature extends Feature
{
    void bind(RSIClientBinder binder);
}
