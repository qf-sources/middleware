package io.qiufen.rsi.module.server.provider.feature;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;
import io.qiufen.module.kernel.spi.feature.FeatureHandlerAdapter;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import io.qiufen.rpc.server.api.RPCServerConfig;
import io.qiufen.rpc.server.provider.infrastructure.SharedServerNetService;
import io.qiufen.rpc.server.spi.ServerSideException;
import io.qiufen.rpc.server.spi.infrastructure.ServerNetService;
import io.qiufen.rpc.server.spi.rpc.RPCServiceFilter;
import io.qiufen.rsi.server.api.RSIServer;
import io.qiufen.rsi.server.api.RSIServerConfig;
import io.qiufen.rsi.server.provider.builder.RSIServerBuilder;
import io.qiufen.rsi.server.spi.ServicePublisher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RSIServerFeatureHandler extends FeatureHandlerAdapter<RSIServerFeature>
{
    private static final String DEFAULT_ID = "default";
    ServerNetService serverNetService = new SharedServerNetService(2, 8);
    private List<RSIServerDeclaration> serverDeclarations;
    private List<RPCFilterDeclaration> rpcFilterDeclarations;
    private List<RSIServiceDeclaration> serviceDeclarations;
    private FacadeFinder facadeFinder;
    private Map<Object, RSIServer> rsiServerMap;

    @Override
    public void initFeature(ContextFactory context, FacadeRegistry facadeRegistry)
    {
        this.facadeFinder = facadeRegistry;
        this.rsiServerMap = new HashMap<Object, RSIServer>();
    }

    @Override
    public void doHandle(RSIServerFeature feature, ContextFactory context)
    {
        serverDeclarations = new ArrayList<RSIServerDeclaration>();
        rpcFilterDeclarations = new ArrayList<RPCFilterDeclaration>();
        serviceDeclarations = new ArrayList<RSIServiceDeclaration>();
        feature.bind(new RSIServerBinder(serverDeclarations, rpcFilterDeclarations, serviceDeclarations));
    }

    @Override
    public void postModule(RSIServerFeature feature, ContextFactory contextFactory)
            throws ServerSideException, InterruptedException
    {
        initServer(contextFactory);
        initFilter(contextFactory);
        initService(contextFactory);

        serverDeclarations.clear();
        serverDeclarations = null;
        rpcFilterDeclarations.clear();
        rpcFilterDeclarations = null;
        serviceDeclarations.clear();
        serviceDeclarations = null;
    }

    private void initServer(ContextFactory contextFactory) throws InterruptedException
    {
        if (CollectionUtil.isEmpty(serverDeclarations)) return;

        for (RSIServerDeclaration declaration : serverDeclarations)
        {
            Object serverId = declaration.getServerId();
            serverId = serverId == null ? DEFAULT_ID : serverId;

            RSIServerBuilder builder = new RSIServerBuilder();
            final RSIServerConfig config = builder.getRsiServiceConfig();
            config.setServerName(serverId.toString());
            if (declaration.getServicePublisherClass() != null)
            {
                ServicePublisher publisher = (ServicePublisher) getObject(contextFactory, null,
                        declaration.getServicePublisherClass(), ObjectScope.SINGLETON);
                config.setServicePublisher(publisher);
            }

            if (declaration.getExecutorService() == null)
            {
                config.setExecutor(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() << 4));
            }
            else
            {
                config.setExecutor(declaration.getExecutorService().to(facadeFinder, ExecutorService.class));
            }

            RPCServerConfig serverConfig = builder.getRpcServerConfig();
            if (declaration.getPort() != null)
            {
                serverConfig.setPort(declaration.getPort().to(facadeFinder, int.class));
            }
            if (declaration.getProtocol() != null)
            {
                config.setProtocol(declaration.getProtocol());
            }
            RSIServer rsiServer = builder.setServerNetService(serverNetService).build();
            this.rsiServerMap.put(serverId, rsiServer);
        }
    }

    private void initFilter(ContextFactory context)
    {
        if (CollectionUtil.isEmpty(rpcFilterDeclarations)) return;

        for (RPCFilterDeclaration declaration : rpcFilterDeclarations)
        {
            RSIServer rsiServer = getServer(declaration.getServerId());
            Object object = getObject(context, declaration.getObjectId(), declaration.getType());
            rsiServer.addRPCFilter((RPCServiceFilter) object);
        }
    }

    private void initService(ContextFactory context) throws ServerSideException
    {
        if (CollectionUtil.isEmpty(serviceDeclarations)) return;

        for (RSIServiceDeclaration declaration : serviceDeclarations)
        {
            RSIServer rsiServer = getServer(declaration.getServerId());
            Object object = getObject(context, declaration.getObjectId(), declaration.getType());
            rsiServer.addService(declaration.getUri(), object);
        }
    }

    private RSIServer getServer(Object serverId)
    {
        serverId = serverId == null ? DEFAULT_ID : serverId;
        RSIServer rsiServer = this.rsiServerMap.get(serverId);
        if (rsiServer == null) throw new RuntimeException("RSI server not bind,server id:" + serverId);
        return rsiServer;
    }

    private Object getObject(ContextFactory context, Object objectId, Class<?> type)
    {
        return getObject(context, objectId, type, ObjectScope.ONCE);
    }

    private Object getObject(ContextFactory context, Object objectId, Class<?> type, ObjectScope scope)
    {
        ObjectContext objectContext = context.getContext(ObjectContext.class);
        //1.先按照ID匹配
        if (objectId != null) return objectContext.getObject(objectId);

        //2.其次按照类型匹配
        if (objectContext.hasDeclaration(type)) return objectContext.getObject(type);

        //3.创建一个
        ClassDeclaration declaration = new ClassDeclaration(type);
        declaration.setScope(scope);
        objectContext.addDeclaration(declaration);
        return objectContext.getObject(declaration.getIdList()[0]);
    }
}
