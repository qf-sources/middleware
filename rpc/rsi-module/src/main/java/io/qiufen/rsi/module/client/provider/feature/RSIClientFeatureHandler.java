package io.qiufen.rsi.module.client.provider.feature;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;
import io.qiufen.module.kernel.spi.feature.FeatureHandlerAdapter;
import io.qiufen.module.kernel.spi.value.Value;
import io.qiufen.module.object.api.declaration.ClassDeclaration;
import io.qiufen.module.object.api.declaration.ObjectDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.spi.declaration.ObjectScope;
import io.qiufen.rpc.client.provider.dispatcher.DefaultRPCContextDispatcher;
import io.qiufen.rpc.client.provider.infrastructure.SharedClientNetService;
import io.qiufen.rpc.client.spi.dispatcher.RPCContextDispatcher;
import io.qiufen.rpc.client.spi.infrastructure.ClientNetService;
import io.qiufen.rsi.client.api.RSIClient;
import io.qiufen.rsi.client.provider.builder.RSIClientBuilder;
import io.qiufen.rsi.client.spi.exception.RSIClientException;
import io.qiufen.rsi.client.spi.rpc.RPCClientContextProvider;
import io.qiufen.rsi.module.client.spi.ServiceSubscriber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RSIClientFeatureHandler extends FeatureHandlerAdapter<RSIClientFeature>
{
    private static final String DEFAULT_ID = "default";
    private final List<ServiceSubscriber> subscribers = new ArrayList<ServiceSubscriber>();
    ClientNetService clientNetService = new SharedClientNetService(8);
    private List<RSIClientDeclaration> clientDeclarations;
    private List<RSIServiceDeclaration> serviceDeclarations;
    private Map<Object, RSIClient> rsiClientMap;
    private FacadeFinder facadeFinder;

    @Override
    public void initFeature(ContextFactory context, FacadeRegistry facadeRegistry)
    {
        this.rsiClientMap = new HashMap<Object, RSIClient>();
        this.facadeFinder = facadeRegistry;
    }

    @Override
    public void doHandle(RSIClientFeature feature, ContextFactory context)
    {
        clientDeclarations = new ArrayList<RSIClientDeclaration>();
        serviceDeclarations = new ArrayList<RSIServiceDeclaration>();
        feature.bind(new RSIClientBinder(clientDeclarations, serviceDeclarations));
    }

    @Override
    public void postModule(RSIClientFeature feature, ContextFactory contextFactory) throws RSIClientException
    {
        initClient(contextFactory);
        initService(contextFactory);

        for (ServiceSubscriber subscriber : subscribers)
        {
            subscriber.onPostModule();
        }

        clientDeclarations.clear();
        clientDeclarations = null;
        serviceDeclarations.clear();
        serviceDeclarations = null;
    }

    private void initClient(ContextFactory contextFactory)
    {
        if (CollectionUtil.isEmpty(clientDeclarations)) return;

        for (RSIClientDeclaration declaration : clientDeclarations)
        {
            Object clientId = declaration.getClientId();
            clientId = clientId == null ? DEFAULT_ID : clientId;

            RSIClientBuilder builder = new RSIClientBuilder();
            if (declaration.getServiceSubscriberClass() != null)
            {
                ServiceSubscriber subscriber = (ServiceSubscriber) getObject(contextFactory, null,
                        declaration.getServiceSubscriberClass(), ObjectScope.ONCE);
                subscribers.add(subscriber);
                builder.getRsiClientConfig().setSubscriber(subscriber);
            }
            if (declaration.getRpcClientContextProviderClass() != null)
            {
                RPCClientContextProvider provider = (RPCClientContextProvider) getObject(contextFactory, null,
                        declaration.getRpcClientContextProviderClass(), ObjectScope.ONCE);
                builder.setRpcClientContextProvider(provider);
            }
            builder.getRpcClientConfig().setReadTimeoutMills(10000);
            builder.getRpcClientConfig().setInitConnections(10);
            Value executorServiceValue = declaration.getExecutorService();
            ExecutorService executorService;
            if (executorServiceValue == null)
            {
                executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() << 1);
            }
            else
            {
                executorService = executorServiceValue.to(facadeFinder, ExecutorService.class);
            }
            RPCContextDispatcher rpcContextDispatcher = new DefaultRPCContextDispatcher(executorService, 1024,
                    1024 * 512);
            RSIClient client = builder.setClientNetService(clientNetService)
                    .setRpcContextDispatcher(rpcContextDispatcher)
                    .build();
            this.rsiClientMap.put(clientId, client);
        }
    }

    private void initService(ContextFactory context) throws RSIClientException
    {
        if (CollectionUtil.isEmpty(serviceDeclarations))
        {
            return;
        }
        ObjectContext objectContext = context.getContext(ObjectContext.class);
        for (RSIServiceDeclaration declaration : serviceDeclarations)
        {
            RSIClient rsiClient = getClient(declaration.getClientId());
            Object service = rsiClient.subscribeService(declaration.getUri(), declaration.getType());
            ObjectDeclaration objectDeclaration = new ObjectDeclaration(service);
            objectDeclaration.setScope(ObjectScope.SINGLETON);
            if (declaration.getId() != null) objectDeclaration.setIdList(new Object[]{declaration.getId()});
            objectContext.addDeclaration(objectDeclaration);
        }
    }

    private RSIClient getClient(Object clientId)
    {
        clientId = clientId == null ? DEFAULT_ID : clientId;
        RSIClient rsiClient = this.rsiClientMap.get(clientId);
        if (rsiClient == null)
        {
            throw new RuntimeException("RSI client not bind,client id:" + clientId);
        }
        return rsiClient;
    }

    private Object getObject(ContextFactory context, Object objectId, Class<?> type, ObjectScope scope)
    {
        ObjectContext objectContext = context.getContext(ObjectContext.class);
        //1.先按照ID匹配
        if (objectId != null)
        {
            return objectContext.getObject(objectId);
        }

        //2.其次按照类型匹配
        if (objectContext.hasDeclaration(type))
        {
            return objectContext.getObject(type);
        }
        //3.创建一个
        ClassDeclaration declaration = new ClassDeclaration(type);
        declaration.setScope(scope);
        objectContext.addDeclaration(declaration);
        return objectContext.getObject(type);
    }
}
