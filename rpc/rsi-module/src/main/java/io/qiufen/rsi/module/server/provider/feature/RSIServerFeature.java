package io.qiufen.rsi.module.server.provider.feature;


import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

@Handler(RSIServerFeatureHandler.class)
public interface RSIServerFeature extends Feature
{
    void bind(RSIServerBinder binder);
}
