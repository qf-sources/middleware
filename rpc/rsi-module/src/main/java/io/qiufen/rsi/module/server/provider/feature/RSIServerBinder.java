package io.qiufen.rsi.module.server.provider.feature;

import io.qiufen.rpc.server.spi.rpc.RPCServiceFilter;

import java.util.List;

public class RSIServerBinder
{
    private final List<RSIServerDeclaration> serverDeclarations;
    private final List<RPCFilterDeclaration> rpcFilterDeclarations;
    private final List<RSIServiceDeclaration> serviceDeclarations;

    RSIServerBinder(List<RSIServerDeclaration> serverDeclarations, List<RPCFilterDeclaration> rpcFilterDeclarations,
            List<RSIServiceDeclaration> serviceDeclarations)
    {
        this.serverDeclarations = serverDeclarations;
        this.rpcFilterDeclarations = rpcFilterDeclarations;
        this.serviceDeclarations = serviceDeclarations;
    }

    public RSIServerDeclarationOptions bindServer()
    {
        RSIServerDeclaration declaration = new RSIServerDeclaration();
        this.serverDeclarations.add(declaration);
        return new RSIServerDeclarationOptions(declaration);
    }

    public RPCFilterDeclaration bindRPCFilter(Object objectId)
    {
        RPCFilterDeclaration declaration = new RPCFilterDeclaration(objectId);
        this.rpcFilterDeclarations.add(declaration);
        return declaration;
    }

    public RPCFilterDeclaration bindRPCFilter(Class<? extends RPCServiceFilter> type)
    {
        RPCFilterDeclaration declaration = new RPCFilterDeclaration(type);
        this.rpcFilterDeclarations.add(declaration);
        return declaration;
    }

    public RSIServiceDeclarationOptions bindService(String uri, Object objectId)
    {
        RSIServiceDeclaration declaration = new RSIServiceDeclaration();
        declaration.setUri(uri);
        declaration.setObjectId(objectId);
        this.serviceDeclarations.add(declaration);
        return new RSIServiceDeclarationOptions(declaration);
    }

    public RSIServiceDeclarationOptions bindService(String uri, Class<?> type)
    {
        RSIServiceDeclaration declaration = new RSIServiceDeclaration();
        declaration.setUri(uri);
        declaration.setType(type);
        this.serviceDeclarations.add(declaration);
        return new RSIServiceDeclarationOptions(declaration);
    }
}
