package io.qiufen.rsi.module.server.provider.feature;

import io.qiufen.module.kernel.provider.value.SimpleValue;
import io.qiufen.module.kernel.spi.value.Value;
import io.qiufen.rsi.common.protocol.Protocol;
import io.qiufen.rsi.server.spi.ServicePublisher;

public class RSIServerDeclarationOptions
{
    private final RSIServerDeclaration declaration;

    RSIServerDeclarationOptions(RSIServerDeclaration declaration)
    {
        this.declaration = declaration;
    }

    public RSIServerDeclarationOptions port(int port)
    {
        return port(new SimpleValue(port));
    }

    public RSIServerDeclarationOptions port(Value port)
    {
        this.declaration.setPort(port);
        return this;
    }

    public RSIServerDeclarationOptions host(String host) {return host(new SimpleValue(host));}

    public RSIServerDeclarationOptions host(Value host)
    {
        this.declaration.setHost(host);
        return this;
    }

    public RSIServerDeclarationOptions servicePublisher(Class<? extends ServicePublisher> type)
    {
        this.declaration.setServicePublisherClass(type);
        return this;
    }

    public RSIServerDeclarationOptions id(Object serverId)
    {
        this.declaration.setServerId(serverId);
        return this;
    }

    public RSIServerDeclarationOptions executorService(Value value)
    {
        this.declaration.setExecutorService(value);
        return this;
    }

    public RSIServerDeclarationOptions protocol(Protocol protocol)
    {
        this.declaration.setProtocol(protocol);
        return this;
    }
}
