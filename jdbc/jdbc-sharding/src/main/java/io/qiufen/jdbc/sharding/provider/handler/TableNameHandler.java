package io.qiufen.jdbc.sharding.provider.handler;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.lang.Strings;
import io.qiufen.common.provider.ProviderHolder;
import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;
import io.qiufen.jdbc.mapper.spi.handler.StatementContext;
import io.qiufen.jdbc.mapper.spi.handler.StatementContextFactory;
import io.qiufen.jdbc.sharding.provider.datasource.HitContext;
import io.qiufen.jdbc.sharding.provider.parser.Sharding;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.statement.update.Update;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableNameHandler implements StatementContextFactory
{
    private final HitContext hitContext = HitContext.getInstance();

    @Override
    public StatementContext create(MappedStatement mappedStatement, Object parameter)
    {
        StatementContext context = ProviderHolder.next(mappedStatement, parameter);

        Sharding sharding = hitContext.getSharding();
        String sql;
        try
        {
            sql = convertTableName(sharding, context.getJdbcSql());
        }
        catch (JSQLParserException e)
        {
            throw new SessionException(e);
        }
        return new StatementContext(context.getStatementScope(), sql, context.getJdbcParameters());
    }

    @Override
    public void clean(StatementScope statementScope)
    {
        ProviderHolder.next(statementScope);
    }

    protected String convertTableName(Sharding sharding, String sql) throws JSQLParserException
    {
        Statement statement;
        statement = CCJSqlParserUtil.parse(sql);
        Map<String, String> names = new HashMap<String, String>();
        for (int i = 0; i < sharding.getFromNameList().size(); i++)
        {
            names.put(sharding.getFromNameList().get(i), sharding.getToNameList().get(i));
        }
        doStatement(statement, names);
        return statement.toString();
    }

    private void doStatement(Statement statement, Map<String, String> names)
    {
        if (statement instanceof Select)
        {
            doSelect(((Select) statement).getSelectBody(), names);
        }
        else if (statement instanceof Insert)
        {
            doInsert((Insert) statement, names);
        }
        else if (statement instanceof Update)
        {
            doUpdate((Update) statement, names);
        }
        else if (statement instanceof Delete)
        {
            doDelete((Delete) statement, names);
        }
        else
        {
            throw new UnsupportedOperationException("Not support type:" + statement.getClass().getName());
        }
    }

    private void doSelect(SelectBody selectBody, Map<String, String> names)
    {
        if (selectBody instanceof PlainSelect)
        {
            doSelect((PlainSelect) selectBody, names);
        }
        else if (selectBody instanceof SetOperationList)
        {
            List<SelectBody> selectBodies = ((SetOperationList) selectBody).getSelects();
            for (SelectBody body : selectBodies)
            {
                doSelect(body, names);
            }
        }
        else
        {
            throw new UnsupportedOperationException("Not support type:" + selectBody.getClass().getName());
        }
    }

    private void doInsert(Insert insert, Map<String, String> names)
    {
        Table table = insert.getTable();
        setTableName(table, names);

        Select select = insert.getSelect();
        if (null != select)
        {
            SelectBody selectBody = select.getSelectBody();
            doSelect(selectBody, names);
        }
    }

    private void doUpdate(Update update, Map<String, String> names)
    {
        List<Expression> expressions = update.getExpressions();
        if (CollectionUtil.isNotEmpty(expressions))
        {
            for (Expression expression : expressions)
            {
                doExpression(expression, names);
            }
        }
        setTableName(update.getTable(), names);
    }

    private void doDelete(Delete delete, Map<String, String> names)
    {
        Expression where = delete.getWhere();
        if (null != where)
        {
            if (where instanceof BinaryExpression)
            {
                doExpression(((BinaryExpression) where).getLeftExpression(), names);
                doExpression(((BinaryExpression) where).getRightExpression(), names);
            }
        }
        setTableName(delete.getTable(), names);
    }

    private void doSelect(PlainSelect plainSelect, Map<String, String> names)
    {
        //SELECT语句
        for (SelectItem selectItem : plainSelect.getSelectItems())
        {
            if (selectItem instanceof SelectExpressionItem)
            {
                doExpression(((SelectExpressionItem) selectItem).getExpression(), names);
            }
        }

        //FROM语句
        doFrom(plainSelect.getFromItem(), plainSelect, names);

        //JOIN语句
        List<Join> joins = plainSelect.getJoins();
        if (CollectionUtil.isNotEmpty(joins))
        {
            for (Join join : joins)
            {
                doFrom(join.getRightItem(), plainSelect, names);
            }
        }
    }

    private void doFrom(FromItem fromItem, PlainSelect plainSelect, Map<String, String> names)
    {
        if (null == fromItem)
        {
            return;
        }

        if (fromItem instanceof SubSelect)
        {
            doSelect(((SubSelect) fromItem).getSelectBody(), names);
        }
        else if (fromItem instanceof Table)
        {
            Table table = ((Table) fromItem);
            Expression where = plainSelect.getWhere();
            if (null != where)
            {
                if (where instanceof BinaryExpression)
                {
                    doExpression(((BinaryExpression) where).getLeftExpression(), names);
                    doExpression(((BinaryExpression) where).getRightExpression(), names);
                }
            }
            setTableName(table, names);
        }
        else
        {
            throw new UnsupportedOperationException("Not support type:" + fromItem.getClass().getName());
        }
    }

    private void doExpression(Expression expression, Map<String, String> names)
    {
        if (expression instanceof SubSelect)
        {
            doSelect(((SubSelect) expression).getSelectBody(), names);
        }
    }

    private void setTableName(Table table, Map<String, String> names)
    {
        String toName = names.get(table.getName());
        if (Strings.isEmpty(toName))
        {
            return;
        }
        table.setName(toName);
    }
}
