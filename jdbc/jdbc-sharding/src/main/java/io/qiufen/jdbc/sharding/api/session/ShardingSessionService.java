package io.qiufen.jdbc.sharding.api.session;

import io.qiufen.jdbc.mapper.api.session.Session;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/9 15:32
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface ShardingSessionService
{
    Session hit(String scope, String id, Object argument);

    Session hit(String id, Object argument);

    void unHit(String scope);

    void unHit();
}
