package io.qiufen.jdbc.sharding.provider.parser;

import java.util.HashMap;
import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/12 17:13
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ShardingConfiguration
{
    private final Map<String, Rule> statementRuleMap = new HashMap<String, Rule>();
    private final Map<String, Rule> namespaceRuleMap = new HashMap<String, Rule>();

    public Map<String, Rule> getStatementRuleMap()
    {
        return statementRuleMap;
    }

    public Map<String, Rule> getNamespaceRuleMap()
    {
        return namespaceRuleMap;
    }

    @Override
    public String toString()
    {
        return "ShardingConfiguration{" + "statementRuleMap=" + statementRuleMap + ", namespaceRuleMap=" + namespaceRuleMap + '}';
    }
}
