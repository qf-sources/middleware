package io.qiufen.jdbc.sharding.provider.datasource;

import io.qiufen.jdbc.sharding.provider.parser.Sharding;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/10 14:32
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class HitContext
{
    private static final HitContext instance = new HitContext();

    private final ThreadLocal<Sharding> local = new ThreadLocal<Sharding>();

    public static HitContext getInstance()
    {
        return instance;
    }

    public Sharding getSharding()
    {
        return local.get();
    }

    public void setSharding(Sharding sharding)
    {
        this.local.set(sharding);
    }

    public void clear()
    {
        local.remove();
    }
}
