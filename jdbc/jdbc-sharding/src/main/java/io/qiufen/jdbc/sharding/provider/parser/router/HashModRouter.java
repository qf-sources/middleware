package io.qiufen.jdbc.sharding.provider.parser.router;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/9 19:41
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class HashModRouter implements Router
{
    private static final Logger log = LoggerFactory.getLogger(HashModRouter.class);

    private int count;

    @Override
    public String calc(Object obj, String paramName)
    {
        if (obj == null)
        {
            return "0";
        }
        if (obj instanceof Map)
        {
            Object value = ((Map) obj).get(paramName);
            if (value == null)
            {
                return "0";
            }
            return calc(value);
        }

        Method method;
        try
        {
            method = new PropertyDescriptor(paramName, obj.getClass()).getReadMethod();
        }
        catch (IntrospectionException e)
        {
            log.debug("Can't find property:{}", paramName);
            return calc(obj);
        }
        if (method == null)
        {
            log.debug("Can't read property:{}", paramName);
            return calc(obj);
        }
        try
        {
            return calc(method.invoke(obj));
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
        catch (InvocationTargetException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    private String calc(Object value)
    {
        int mod = value.hashCode() % count;
        return String.valueOf(mod < 0 ? -mod : mod);
    }
}
