package io.qiufen.jdbc.sharding.provider.parser;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/12 9:52
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Namespace
{
    private final String paramName;
    private final String value;

    public Namespace(String paramName, String value)
    {
        this.paramName = paramName;
        this.value = value;
    }

    public String getParamName()
    {
        return paramName;
    }

    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return "Namespace{" + "paramName='" + paramName + '\'' + ", value='" + value + '\'' + '}';
    }
}
