package io.qiufen.jdbc.sharding.provider.parser.router;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/19 10:02
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class EqualRouter implements Router
{
    private String eq;

    @Override
    public String calc(Object obj, String paramName)
    {
        return eq;
    }

    public void setEq(String eq)
    {
        this.eq = eq;
    }
}
