package io.qiufen.jdbc.sharding.provider.builder;

import io.qiufen.common.io.IOUtil;
import io.qiufen.common.io.resource.StreamResource;
import io.qiufen.common.provider.ProviderContext;
import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.spi.handler.StatementContextFactory;
import io.qiufen.jdbc.sharding.api.session.ShardingSessionService;
import io.qiufen.jdbc.sharding.provider.handler.TableNameHandler;
import io.qiufen.jdbc.sharding.provider.parser.ShardingConfiguration;
import io.qiufen.jdbc.sharding.provider.parser.ShardingRuleParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/14 19:37
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ShardingSessionServiceBuilder
{
    private static final Logger log = LoggerFactory.getLogger(ShardingSessionServiceBuilder.class);

    private StreamResource streamResource;
    private SessionFactory sessionFactory;

    public ShardingSessionServiceBuilder setResource(StreamResource resource)
    {
        this.streamResource = resource;
        return this;
    }

    public ShardingSessionServiceBuilder setSessionFactory(SessionFactory factory)
    {
        this.sessionFactory = factory;
        return this;
    }

    public ShardingSessionService build() throws Exception
    {
        if (log.isInfoEnabled())
        {
            log.info("Use resource:{}", streamResource);
        }

        InputStream stream = streamResource.open();
        ShardingConfiguration configuration;
        try
        {
            configuration = ShardingRuleParser.parse(stream);
        }
        finally
        {
            IOUtil.close(stream);
        }

        ProviderContext providerContext = sessionFactory.getProviderContext();
        providerContext.addDecorator(StatementContextFactory.class, new TableNameHandler());

        return new DefaultShardingSessionService(sessionFactory, configuration);
    }
}
