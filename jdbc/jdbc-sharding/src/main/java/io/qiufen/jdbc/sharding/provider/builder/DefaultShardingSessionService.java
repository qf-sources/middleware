package io.qiufen.jdbc.sharding.provider.builder;

import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.sharding.api.session.ShardingSessionService;
import io.qiufen.jdbc.sharding.provider.datasource.HitContext;
import io.qiufen.jdbc.sharding.provider.parser.Rule;
import io.qiufen.jdbc.sharding.provider.parser.Sharding;
import io.qiufen.jdbc.sharding.provider.parser.ShardingConfiguration;
import io.qiufen.jdbc.sharding.provider.parser.router.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/10 9:31
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class DefaultShardingSessionService implements ShardingSessionService
{
    private static final Logger log = LoggerFactory.getLogger(DefaultShardingSessionService.class);

    private static final String SCOPE_DEFAULT = "default";

    private final HitContext hitContext = HitContext.getInstance();

    private final Map<String, Rule> statementRuleMap;
    private final Map<String, Rule> namespaceRuleMap;

    /**
     * <scope-<dataSource-session>>
     */
    private final ThreadLocal<Map<String, Map<String, Session>>> local;

    private final SessionFactory factory;


    DefaultShardingSessionService(SessionFactory factory, ShardingConfiguration configuration)
    {
        this.statementRuleMap = configuration.getStatementRuleMap();
        this.namespaceRuleMap = configuration.getNamespaceRuleMap();

        this.local = new ThreadLocal<Map<String, Map<String, Session>>>();
        this.factory = factory;
    }

    @Override
    public Session hit(String scope, String id, Object argument)
    {
        Sharding sharding = getSharding(id, argument);
        hitContext.setSharding(sharding);
        if (log.isDebugEnabled())
        {
            log.debug("Hit sharding:{}", sharding);
        }

        Map<String, Map<String, Session>> scopes = local.get();
        if (scopes == null)
        {
            scopes = new HashMap<String, Map<String, Session>>();
            local.set(scopes);
        }

        Map<String, Session> sessions = scopes.get(scope);
        if (sessions == null)
        {
            sessions = new HashMap<String, Session>();
            scopes.put(scope, sessions);
        }

        String dsName = sharding == null ? null : sharding.getDataSourceName();
        Session session = sessions.get(dsName);
        if (session != null && !session.isClosed())
        {
            return session;
        }
        session = factory.openSession();
        sessions.put(dsName, session);
        return session;
    }

    @Override
    public Session hit(String id, Object argument)
    {
        return hit("default", id, argument);
    }

    @Override
    public void unHit(String scope)
    {
        hitContext.clear();
        Map<String, Map<String, Session>> scopes = local.get();
        if (scopes == null)
        {
            return;
        }
        scopes.remove(scope);
        if (scopes.isEmpty())
        {
            local.remove();
        }
    }

    @Override
    public void unHit()
    {
        unHit(SCOPE_DEFAULT);
    }

    private Sharding getSharding(String id, Object argument)
    {
        if (log.isDebugEnabled())
        {
            log.debug("Try hit statement:{}...", id);
        }
        Rule rule = statementRuleMap.get(id);
        if (rule != null)
        {
            Router router = rule.getRouter();
            String paramName = rule.getStatement().getParamName();
            String eq = router.calc(argument, paramName);
            return rule.getShardingMap().get(eq);
        }

        String ns = id;
        int index = id.lastIndexOf('.');
        if (index > 0)
        {
            ns = id.substring(0, index);
        }
        if (log.isDebugEnabled())
        {
            log.debug("Try hit namespace:{}...", ns);
        }
        rule = namespaceRuleMap.get(ns);
        if (rule != null)
        {
            Router router = rule.getRouter();
            String paramName = rule.getNamespace().getParamName();
            String eq = router.calc(argument, paramName);
            return rule.getShardingMap().get(eq);
        }

        return null;
    }
}
