package io.qiufen.jdbc.sharding.provider.parser.router;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/12 9:29
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface Router
{
    String calc(Object obj, String paramName);
}
