package io.qiufen.jdbc.sharding.provider.datasource;

import io.qiufen.jdbc.sharding.provider.parser.Sharding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.HashMap;
import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/9 15:30
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class RoutableDataSource implements DataSource
{
    private static final Logger log = LoggerFactory.getLogger(RoutableDataSource.class);

    private final HitContext hitContext = HitContext.getInstance();

    private final Map<String, DataSource> routes = new HashMap<String, DataSource>();

    private final DataSource defaultDataSource;

    public RoutableDataSource(DataSource defaultDataSource)
    {
        this.defaultDataSource = defaultDataSource;
    }

    @Override
    public Connection getConnection() throws SQLException
    {
        return getDataSource().getConnection();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException
    {
        return getDataSource().getConnection(username, password);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException
    {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException
    {

    }

    @Override
    public int getLoginTimeout() throws SQLException
    {
        return 0;
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException
    {

    }

    @Override
    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException
    {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException
    {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException
    {
        return false;
    }

    public void add(String name, DataSource dataSource)
    {
        routes.put(name, dataSource);
    }

    private DataSource getDataSource()
    {
        Sharding sharding = hitContext.getSharding();
        if (sharding == null)
        {
            if (log.isDebugEnabled())
            {
                log.debug("Hit none sharding,use default data source.");
            }
            return defaultDataSource;
        }
        String name = sharding.getDataSourceName();
        DataSource dataSource = routes.get(name);
        if (null == dataSource)
        {
            throw new RuntimeException("no such data source,name:" + name);
        }
        if (log.isDebugEnabled())
        {
            log.debug("Use data source:{}", name);
        }
        return dataSource;
    }
}
