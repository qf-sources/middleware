package io.qiufen.jdbc.sharding.provider.parser;

import io.qiufen.jdbc.sharding.provider.parser.router.Router;

import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/12 10:07
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Rule
{
    private final Router router;
    private final Map<String, Sharding> shardingMap;
    private final Namespace namespace;
    private final Statement statement;

    public Rule(Router router, Map<String, Sharding> shardingMap, Namespace namespace)
    {
        this.router = router;
        this.shardingMap = shardingMap;
        this.namespace = namespace;
        this.statement = null;
    }

    public Rule(Router router, Map<String, Sharding> shardingMap, Statement statement)
    {
        this.router = router;
        this.shardingMap = shardingMap;
        this.namespace = null;
        this.statement = statement;
    }

    public Router getRouter()
    {
        return router;
    }

    public Map<String, Sharding> getShardingMap()
    {
        return shardingMap;
    }

    public Namespace getNamespace()
    {
        return namespace;
    }

    public Statement getStatement()
    {
        return statement;
    }

    @Override
    public String toString()
    {
        return "Rule{" + "router=" + router + ", shardingMap=" + shardingMap + ", namespace=" + namespace + ", statement=" + statement + '}';
    }
}
