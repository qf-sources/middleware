package io.qiufen.jdbc.sharding.provider.parser;

import io.qiufen.jdbc.sharding.provider.parser.router.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.beans.*;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/12 17:11
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ShardingRuleParser
{
    private static final Logger log = LoggerFactory.getLogger(ShardingRuleParser.class);

    public static ShardingConfiguration parse(InputStream stream) throws Exception
    {
        ShardingConfiguration configuration = new ShardingConfiguration();

        XPath xPath = XPathFactory.newInstance().newXPath();
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        builder.setEntityResolver(new EntityResolver()
        {
            @Override
            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
            {
                return new InputSource(
                        getClass().getClassLoader().getResourceAsStream("META-INF/sql-map-sharding.dtd"));
            }
        });
        Document document = builder.parse(stream);
        NodeList ruleNodes = (NodeList) xPath.evaluate("/rules/rule", document, XPathConstants.NODESET);
        for (int i = 0; i < ruleNodes.getLength(); i++)
        {
            Node ruleNode = ruleNodes.item(i);
            Node routerNode = (Node) xPath.evaluate("router", ruleNode, XPathConstants.NODE);
            Router router = parseRouter(xPath, routerNode);

            Map<String, Sharding> shardingMap = new HashMap<String, Sharding>();
            NodeList shardingNodes = (NodeList) xPath.evaluate("shardings/sharding", ruleNode, XPathConstants.NODESET);
            parseShardings(xPath, shardingNodes, shardingMap);

            List<Namespace> namespaces = new ArrayList<Namespace>();
            NodeList namespaceNodes = (NodeList) xPath.evaluate("binding/namespace", ruleNode, XPathConstants.NODESET);
            parseNamespaces(xPath, namespaceNodes, namespaces);
            for (Namespace namespace : namespaces)
            {
                configuration.getNamespaceRuleMap().put(namespace.getValue(), new Rule(router, shardingMap, namespace));
            }

            List<Statement> statements = new ArrayList<Statement>();
            NodeList statementNodes = (NodeList) xPath.evaluate("binding/statement", ruleNode, XPathConstants.NODESET);
            parseStatements(xPath, statementNodes, statements);
            for (Statement statement : statements)
            {
                configuration.getStatementRuleMap().put(statement.getValue(), new Rule(router, shardingMap, statement));
            }
        }
        if (log.isInfoEnabled())
        {
            log.info("Sharding configuration:{}", configuration);
        }
        return configuration;
    }

    private static Router parseRouter(XPath xPath, Node routerNode)
            throws XPathExpressionException, ClassNotFoundException, IllegalAccessException, InstantiationException,
            IntrospectionException, NoSuchFieldException, InvocationTargetException
    {
        String className = xPath.evaluate("@class", routerNode);
        Class clazz = Class.forName(className);
        Router router = (Router) clazz.newInstance();
        PropertyDescriptor[] descriptors = Introspector.getBeanInfo(clazz).getPropertyDescriptors();
        NodeList params = (NodeList) xPath.evaluate("param", routerNode, XPathConstants.NODESET);
        for (int i = 0; i < params.getLength(); i++)
        {
            Node param = params.item(i);
            String name = xPath.evaluate("@name", param);
            String value = xPath.evaluate("@value", param);
            PropertyDescriptor property = null;
            for (PropertyDescriptor descriptor : descriptors)
            {
                if (descriptor.getName().equals(name))
                {
                    property = descriptor;
                    break;
                }
            }

            if (property == null)
            {
                throw new NoSuchFieldException("no field:" + name);
            }

            PropertyEditor propertyEditor = PropertyEditorManager.findEditor(property.getPropertyType());
            propertyEditor.setAsText(value);
            property.getWriteMethod().invoke(router, propertyEditor.getValue());
        }
        return router;
    }

    private static void parseShardings(XPath xPath, NodeList shardingNodes, Map<String, Sharding> shardingMap)
            throws XPathExpressionException
    {
        for (int i = 0; i < shardingNodes.getLength(); i++)
        {
            Node route = shardingNodes.item(i);
            String eq = xPath.evaluate("@eq", route);
            String dataSourceName = xPath.evaluate("@dataSource", route);
            NodeList tables = (NodeList) xPath.evaluate("table", route, XPathConstants.NODESET);
            NodeList tableIncrements = (NodeList) xPath.evaluate("table-increment", route, XPathConstants.NODESET);
            if (tables.getLength() <= 0 && tableIncrements.getLength() <= 0)
            {
                shardingMap.put(eq, new Sharding(dataSourceName, null, null));
            }
            else
            {
                parseTables(xPath, tables, dataSourceName, shardingMap);
                parseTableIncrements(xPath, tableIncrements, dataSourceName, shardingMap);
            }
        }
    }

    private static void parseTables(XPath xPath, NodeList tables, String dataSourceName,
            Map<String, Sharding> shardingMap) throws XPathExpressionException
    {
        for (int i = 0; i < tables.getLength(); i++)
        {
            Node table = tables.item(i);
            String eq = xPath.evaluate("@eq", table);
            NodeList names = (NodeList) xPath.evaluate("name", table, XPathConstants.NODESET);
            List<String> fromNameList = new ArrayList<String>();
            List<String> toNameList = new ArrayList<String>();
            for (int j = 0; j < names.getLength(); j++)
            {
                Node name = names.item(j);
                fromNameList.add(xPath.evaluate("@from", name));
                toNameList.add(xPath.evaluate("@to", name));
            }
            shardingMap.put(eq, new Sharding(dataSourceName, fromNameList, toNameList));
        }
    }

    private static void parseTableIncrements(XPath xPath, NodeList tableIncrements, String dataSourceName,
            Map<String, Sharding> shardingMap) throws XPathExpressionException
    {
        for (int i = 0; i < tableIncrements.getLength(); i++)
        {
            Node table = tableIncrements.item(i);
            int count = Integer.parseInt(xPath.evaluate("@count", table));
            int eqStart = Integer.parseInt(xPath.evaluate("@eqStart", table));
            int toStart = Integer.parseInt(xPath.evaluate("@toStart", table));
            for (int i1 = 0; i1 < count; i1++)
            {
                String eq = String.valueOf(eqStart + i1);
                int to = toStart + i1;
                NodeList names = (NodeList) xPath.evaluate("name", table, XPathConstants.NODESET);
                List<String> fromNameList = new ArrayList<String>();
                List<String> toNameList = new ArrayList<String>();
                for (int j = 0; j < names.getLength(); j++)
                {
                    Node name = names.item(j);
                    fromNameList.add(xPath.evaluate("@from", name));
                    toNameList.add(xPath.evaluate("@to", name) + to);
                }
                shardingMap.put(eq, new Sharding(dataSourceName, fromNameList, toNameList));
            }
        }
    }

    private static void parseNamespaces(XPath xPath, NodeList namespaceNodes, List<Namespace> namespaces)
            throws XPathExpressionException
    {
        for (int i = 0; i < namespaceNodes.getLength(); i++)
        {
            Node namespaceNode = namespaceNodes.item(i);
            String paramName = xPath.evaluate("@paramName", namespaceNode);
            String value = xPath.evaluate("@value", namespaceNode);
            namespaces.add(new Namespace(paramName, value));
        }
    }

    private static void parseStatements(XPath xPath, NodeList statementNodes, List<Statement> statements)
            throws XPathExpressionException
    {
        for (int i = 0; i < statementNodes.getLength(); i++)
        {
            Node statementNode = statementNodes.item(i);
            String paramName = xPath.evaluate("@paramName", statementNode);
            String value = xPath.evaluate("@value", statementNode);
            statements.add(new Statement(paramName, value));
        }
    }
}
