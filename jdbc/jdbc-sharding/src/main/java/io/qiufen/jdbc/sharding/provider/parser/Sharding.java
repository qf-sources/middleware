package io.qiufen.jdbc.sharding.provider.parser;

import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/10 9:29
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Sharding
{
    private final String dataSourceName;
    private final List<String> fromNameList;
    private final List<String> toNameList;

    public Sharding(String dataSourceName, List<String> fromNameList, List<String> toNameList)
    {
        this.dataSourceName = dataSourceName;
        this.fromNameList = fromNameList;
        this.toNameList = toNameList;
    }

    public String getDataSourceName()
    {
        return dataSourceName;
    }

    public List<String> getFromNameList()
    {
        return fromNameList;
    }

    public List<String> getToNameList()
    {
        return toNameList;
    }

    @Override
    public String toString()
    {
        return "Sharding{" + "dataSourceName='" + dataSourceName + '\'' + ", fromNameList=" + fromNameList + ", toNameList=" + toNameList + '}';
    }
}
