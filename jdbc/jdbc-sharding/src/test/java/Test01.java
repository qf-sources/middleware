import io.qiufen.common.io.resource.ClassPathResource;
import io.qiufen.common.io.resource.StreamResource;
import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.api.transaction.Transaction;
import io.qiufen.jdbc.mapper.api.transaction.TransactionDefinition;
import io.qiufen.jdbc.mapper.provider.builder.SessionFactoryBuilder;
import io.qiufen.jdbc.sharding.api.session.ShardingSessionService;
import io.qiufen.jdbc.sharding.provider.builder.ShardingSessionServiceBuilder;
import io.qiufen.jdbc.sharding.provider.datasource.RoutableDataSource;
import org.apache.tomcat.jdbc.pool.DataSource;

import java.util.HashSet;
import java.util.Set;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/28 10:27
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Test01
{
    public static void main(String[] args) throws Exception
    {
        query();
    }

    private static void query() throws Exception
    {
        DataSource dataSource0 = new DataSource();
        dataSource0.setUrl("jdbc:mysql://10.27.132.70:3306/scfrsit");
        dataSource0.setDriverClassName("org.mariadb.jdbc.Driver");
        dataSource0.setUsername("selffabu");
        dataSource0.setPassword("JcHmiuANwa");
        DataSource dataSource1 = new DataSource();
        dataSource1.setUrl("jdbc:mysql://10.27.132.73:3306/scfrsit2");
        dataSource1.setDriverClassName("org.mariadb.jdbc.Driver");
        dataSource1.setUsername("selffabu");
        dataSource1.setPassword("BONvzoEAiU");

        RoutableDataSource routableDataSource = new RoutableDataSource(dataSource0);
        routableDataSource.add("ds_0", dataSource0);
        routableDataSource.add("ds_1", dataSource1);

        StreamResource resource = new ClassPathResource("config.xml");
        SessionFactory factory = new SessionFactoryBuilder().setResource(resource)
                .setDataSource(routableDataSource)
                .build();
        resource = new ClassPathResource("sharding.xml");
        ShardingSessionService hitService = new ShardingSessionServiceBuilder().setResource(resource)
                .setSessionFactory(factory)
                .build();

        Set<Session> sessionSet = new HashSet<Session>();

        for (int i = 0; i < 1000; i++)
        {
            Session session = hitService.hit("test.query", i);

            if (!sessionSet.contains(session))
            {
                Transaction transaction = session.createTransaction(new TransactionDefinition(1, false));
                transaction.begin();
                sessionSet.add(session);
            }

            session.select("test.query", i);

            //            transaction.commit();
            //            session.close();
        }

        for (Session session : sessionSet)
        {
            session.getTransaction().commit();
            session.close();
        }

        hitService.unHit();

        Thread.sleep(3600000);
    }
}
