package io.qiufen.jdbc.module.provider.simple;

import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.api.transaction.Transaction;
import io.qiufen.jdbc.mapper.api.transaction.TransactionDefinition;
import io.qiufen.jdbc.module.api.Transactional;
import jakarta.inject.Inject;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.LinkedList;

/**
 * Created by Administrator on 2017/12/16.
 */
public class SimpleTransactionInterceptor implements MethodInterceptor
{
    private final Logger log = LoggerFactory.getLogger(SimpleTransactionInterceptor.class);

    private final SessionFactory sessionFactory;
    private final SimpleSessionHolder holder;

    @Inject
    public SimpleTransactionInterceptor(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
        this.holder = SimpleSessionHolder.getInstance(sessionFactory);
    }

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable
    {
        Object target = methodInvocation.getThis();
        Method method = methodInvocation.getMethod();
        Object[] args = methodInvocation.getArguments();
        doBefore(target, method, args);
        try
        {
            Object result = methodInvocation.proceed();
            doAfter(target, method, args, result);
            return result;
        }
        catch (Exception e)
        {
            doThrowing(target, method, args, e);
            throw e;
        }
    }

    private void doBefore(Object target, Method methodInfo, Object[] args) throws NoSuchMethodException
    {
        Transactional transactional = getTransactional(target, methodInfo);
        if (null == transactional)
        {
            return;
        }
        addSession(transactional, methodInfo);
    }

    private void doAfter(Object target, Method methodInfo, Object[] args, Object returnObj) throws NoSuchMethodException
    {
        Transactional transactional = getTransactional(target, methodInfo);
        if (null == transactional)
        {
            return;
        }
        SimpleSessionScope scope = removeSession();
        if (null == scope)
        {
            return;
        }
        Session session = scope.getSession();
        switch (transactional.propagation())
        {
            case REQUIRED:
            {
                if (scope.isRefer())
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("To up scope...");
                    }
                }
                else
                {
                    try
                    {
                        session.getTransaction().commit();
                    }
                    finally
                    {
                        session.close();
                    }
                }
                break;
            }
            case REQUIRED_NEW:
            {
                try
                {
                    session.getTransaction().commit();
                }
                finally
                {
                    session.close();
                }
                break;
            }
            case NONE:
            {
                //ignore
            }
        }
    }

    private void doThrowing(Object target, Method methodInfo, Object[] args, Throwable throwable)
            throws NoSuchMethodException
    {
        Transactional transactional = getTransactional(target, methodInfo);
        if (null == transactional)
        {
            return;
        }
        SimpleSessionScope scope = removeSession();
        if (null == scope)
        {
            return;
        }
        Session session = scope.getSession();
        switch (transactional.propagation())
        {
            case REQUIRED:
            {
                if (scope.isRefer())
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("To up scope...");
                    }
                }
                else
                {
                    try
                    {
                        session.getTransaction().rollback();
                    }
                    finally
                    {
                        session.close();
                    }
                }
                break;
            }
            case REQUIRED_NEW:
            {
                try
                {
                    session.getTransaction().rollback();
                }
                finally
                {
                    session.close();
                }
                break;
            }
            case NONE:
            {
                //ignore
            }
        }
    }

    private Transactional getTransactional(Object target, Method method) throws NoSuchMethodException
    {
        Method implMethod = target.getClass().getMethod(method.getName(), method.getParameterTypes());
        Transactional transactional = implMethod.getAnnotation(Transactional.class);
        if (null != transactional)
        {
            return transactional;
        }
        transactional = method.getAnnotation(Transactional.class);
        if (null != transactional)
        {
            return transactional;
        }
        return target.getClass().getAnnotation(Transactional.class);
    }

    private void addSession(Transactional transactional, Method methodInfo)
    {
        LinkedList<SimpleSessionScope> scopes = holder.get();
        if (null == scopes)
        {
            scopes = new LinkedList<SimpleSessionScope>();
            holder.set(scopes);
        }
        SimpleSessionScope scope = null;
        switch (transactional.propagation())
        {
            case REQUIRED:
            {
                Session session;
                if (scopes.isEmpty() || (session = scopes.peekLast().getSession()) == null)
                {
                    session = sessionFactory.openSession();
                    scope = new SimpleSessionScope(methodInfo, false, session);
                    Transaction transaction = session.createTransaction(
                            new TransactionDefinition(transactional.isolation(), transactional.readonly()));
                    transaction.begin();
                }
                else
                {
                    scope = new SimpleSessionScope(methodInfo, true, session);
                }
                break;
            }
            case REQUIRED_NEW:
            {
                Session session = sessionFactory.openSession();
                scope = new SimpleSessionScope(methodInfo, false, session);
                Transaction transaction = session.createTransaction(
                        new TransactionDefinition(transactional.isolation(), transactional.readonly()));
                transaction.begin();
                break;
            }
            case NONE:
            {
                scope = new SimpleSessionScope(methodInfo, false, null);
                break;
            }
            case NOT_SUPPORT:
            {
                if (!scopes.isEmpty() && null != scopes.peekLast().getSession())
                {
                    throw new RuntimeException("Not support transaction.");
                }
                break;
            }
        }
        scopes.offer(scope);
    }

    private SimpleSessionScope removeSession()
    {
        LinkedList<SimpleSessionScope> scopes = holder.get();
        if (scopes.isEmpty())
        {
            return null;
        }
        return scopes.pollLast();
    }
}
