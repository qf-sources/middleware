package io.qiufen.jdbc.module.provider.feature;

import io.qiufen.module.kernel.spi.feature.Binder;
import io.qiufen.module.object.api.declaration.BuilderDeclaration;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.ArrayList;
import java.util.List;

public class JdbcBinder extends Binder
{
    private List<Declaration> declarations;

    public void bind(String resource)
    {
        bind(resource, (Object) null);
    }

    public void bind(String resource, Object sessionFactoryId)
    {
        try
        {
            bind(resource, Util.getMapperClass(resource), sessionFactoryId);
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public void bind(String resource, Class<?> mapperClass)
    {
        bind(resource, mapperClass, null);
    }

    public void bind(String resource, Class<?> mapperClass, Object sessionFactoryId)
    {
        if (!mapperClass.isInterface())
        {
            throw new IllegalArgumentException("Mapper class not an interface,class:" + mapperClass.getName());
        }
        BuilderDeclaration declaration = new BuilderDeclaration(mapperClass,
                new MapperBuilder(resource, mapperClass, sessionFactoryId));
        declaration.setLazy(false);
        useMapperDeclarations().add(declaration);
    }

    private List<Declaration> useMapperDeclarations()
    {
        return declarations == null ? (declarations = new ArrayList<Declaration>()) : declarations;
    }

    public List<Declaration> getMapperDeclarations()
    {
        return declarations;
    }

    @Override
    protected void clear()
    {
        if (declarations != null)
        {
            declarations.clear();
            declarations = null;
        }
    }
}
