package io.qiufen.jdbc.module.provider.simple;

import io.qiufen.jdbc.mapper.api.session.Session;

import java.lang.reflect.Method;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/16 10:13
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class SimpleSessionScope
{
    private final Method methodInfo;
    private final boolean isRefer;
    private final Session session;

    public SimpleSessionScope(Method methodInfo, boolean isRefer, Session session)
    {
        this.methodInfo = methodInfo;
        this.isRefer = isRefer;
        this.session = session;
    }

    public boolean isRefer()
    {
        return isRefer;
    }

    public Session getSession()
    {
        return session;
    }

    @Override
    public String toString()
    {
        return "SimpleSessionScope{" + "methodInfo=" + methodInfo + ", isRefer=" + isRefer + ", session=" + session + '}';
    }
}