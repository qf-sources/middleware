package io.qiufen.jdbc.module.provider.feature;

import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.spi.handler.RowHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.IdentityHashMap;
import java.util.Map;

class MapperInvocation implements InvocationHandler
{
    private final Map<Method, Dispatcher> methodDispatcherCache = new IdentityHashMap<Method, Dispatcher>();

    private final SessionFactory factory;
    private final Class<?> mapperClass;

    MapperInvocation(SessionFactory factory, Class<?> mapperClass)
    {
        this.factory = factory;
        this.mapperClass = mapperClass;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Exception
    {
        return getMethodDispatcher(mapperClass, method).invoke(factory.openSession(), args);
    }

    private Dispatcher getMethodDispatcher(Class<?> mapperClass, Method method)
    {
        Dispatcher dispatcher = methodDispatcherCache.get(method);
        if (dispatcher == null)
        {
            synchronized (methodDispatcherCache)
            {
                dispatcher = methodDispatcherCache.get(method);
                if (dispatcher == null)
                {
                    Class<?>[] types = method.getParameterTypes();
                    for (int i = 0; i < types.length; i++)
                    {
                        if (RowHandler.class.isAssignableFrom(types[i]))
                        {
                            dispatcher = new SelectDispatcher(mapperClass, method, i);
                            break;
                        }
                    }
                    if (dispatcher == null)
                    {
                        dispatcher = new MapperDispatcher(mapperClass, method, factory);
                    }
                    methodDispatcherCache.put(method, dispatcher);
                }
            }
        }
        return dispatcher;
    }
}

