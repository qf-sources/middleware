package io.qiufen.jdbc.module.provider.simple;

import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.provider.config.xml.SqlMapFacade;

import java.sql.Connection;
import java.util.LinkedList;

/**
 * session factory
 */
public class StatefulSessionFactory implements SessionFactory
{
    private final SessionFactory sessionFactory;
    private final SimpleSessionHolder holder;

    public StatefulSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
        this.holder = SimpleSessionHolder.getInstance(sessionFactory);
    }

    @Override
    public Session openSession()
    {
        LinkedList<SimpleSessionScope> scopes = holder.get();
        if (null == scopes || scopes.isEmpty())
        {
            return sessionFactory.openSession();
        }
        Session session = scopes.peekLast().getSession();
        if (null == session)
        {
            return sessionFactory.openSession();
        }
        return session;
    }

    @Override
    public Session openSession(Connection userConnection)
    {
        return sessionFactory.openSession(userConnection);
    }

    @Override
    public void destroy()
    {
        sessionFactory.destroy();
    }

    @Override
    public SqlMapFacade getSqlMapFacade()
    {
        return sessionFactory.getSqlMapFacade();
    }
}
