package io.qiufen.jdbc.module.api;

/**
 * 事务传播行为
 *
 * @version 1.0
 * @since 1.0
 */
public enum Propagation
{
    /**
     * 需要事务，如果当前出于任意一个事务当中则使用当前事务，如果没有则创建一个新事务
     */
    REQUIRED,

    /**
     * 需要创建一个新事务
     */
    REQUIRED_NEW,

    /**
     * 不需要事务
     */
    NONE,

    /**
     * 不支持事务
     */
    NOT_SUPPORT
}
