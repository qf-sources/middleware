package io.qiufen.jdbc.module.provider.feature;

import io.qiufen.module.kernel.spi.feature.Feature;
import io.qiufen.module.kernel.spi.feature.Handler;

@Handler(JdbcFeatureHandler.class)
public interface JdbcFeature extends Feature
{
    void bind(JdbcBinder binder);
}
