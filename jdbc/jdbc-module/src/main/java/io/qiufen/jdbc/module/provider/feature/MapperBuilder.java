package io.qiufen.jdbc.module.provider.feature;

import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletException;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.object.api.declaration.Builder;
import io.qiufen.module.object.api.facade.ObjectFacade;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Proxy;
import java.net.URL;

public class MapperBuilder implements Builder
{
    private final String resource;
    private final Class<?> mapperClass;
    private final Object sessionFactoryId;

    private FacadeFinder finder;

    MapperBuilder(String resource, Class<?> mapperClass, Object sessionFactoryId)
    {
        this.resource = resource;
        this.mapperClass = mapperClass;
        this.sessionFactoryId = sessionFactoryId;
    }

    public void setFinder(FacadeFinder finder)
    {
        this.finder = finder;
    }

    @Override
    public Object build() throws NodeletException, IOException
    {
        SessionFactory factory = getSessionFactory(finder);
        addSqlMap(factory);
        return createMapper(factory);
    }

    private SessionFactory getSessionFactory(FacadeFinder finder)
    {
        ObjectFacade facade = finder.getFacade(ObjectFacade.class);
        SessionFactory factory;
        if (this.sessionFactoryId == null)
        {
            factory = facade.getObject(SessionFactory.class);
        }
        else
        {
            factory = facade.getObject(sessionFactoryId, SessionFactory.class);
        }
        return factory;
    }

    private void addSqlMap(SessionFactory factory) throws NodeletException, IOException
    {
        URL url = this.mapperClass.getClassLoader().getResource(resource);
        if (url == null) throw new FileNotFoundException("Resource:" + resource);
        factory.getSqlMapFacade().addSqlMap(url);
    }

    private Object createMapper(SessionFactory factory)
    {
        return Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?>[]{mapperClass},
                new MapperInvocation(factory, mapperClass));
    }
}

