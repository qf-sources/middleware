package io.qiufen.jdbc.module.provider.sharding;

import io.qiufen.jdbc.module.api.Propagation;
import io.qiufen.jdbc.module.api.Transactional;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/17 16:51
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Util
{
    public static boolean needTx(Transactional transactional)
    {
        if (transactional == null)
        {
            return false;
        }
        Propagation propagation = transactional.propagation();
        return propagation != Propagation.NONE && transactional.propagation() != Propagation.NOT_SUPPORT;
    }
}
