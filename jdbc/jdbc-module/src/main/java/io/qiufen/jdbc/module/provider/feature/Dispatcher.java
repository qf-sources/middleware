package io.qiufen.jdbc.module.provider.feature;

import io.qiufen.common.collection.ArrayUtil;
import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.module.api.Param;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

abstract class Dispatcher
{
    private final Annotation[][] annList;

    Dispatcher(Method method)
    {
        this.annList = method.getParameterAnnotations();
    }

    abstract Object invoke(Session session, Object[] args) throws Exception;

    Object matchParam(Object[] args)
    {
        if (ArrayUtil.isEmpty(args)) return null;

        Map<String, Object> map = null;
        for (int i = 0; i < annList.length; i++)
        {
            Annotation[] annotations = annList[i];
            for (Annotation annotation : annotations)
            {
                if (annotation.annotationType() == Param.class)
                {
                    if (map == null) map = new HashMap<String, Object>();
                    map.put(((Param) annotation).value(), args[i]);
                }
            }
        }
        if (map != null) return map;

        if (args.length >= 1) return args[0];

        return null;
    }
}
