package io.qiufen.jdbc.module.provider.simple;

import io.qiufen.jdbc.mapper.api.session.SessionFactory;

import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by Administrator on 2017/12/17.
 */
public class SimpleSessionHolder
{
    private static final Map<SessionFactory, SimpleSessionHolder> holderMap;

    static
    {
        holderMap = new IdentityHashMap<SessionFactory, SimpleSessionHolder>();
    }

    private final ThreadLocal<LinkedList<SimpleSessionScope>> local = new ThreadLocal<LinkedList<SimpleSessionScope>>();

    private SimpleSessionHolder()
    {
    }

    public static SimpleSessionHolder getInstance(SessionFactory sessionFactory)
    {
        SimpleSessionHolder holder = holderMap.get(sessionFactory);
        if (holder != null)
        {
            return holder;
        }
        synchronized (SimpleSessionHolder.class)
        {
            holder = holderMap.get(sessionFactory);
            if (holder != null)
            {
                return holder;
            }
            holder = new SimpleSessionHolder();
            holderMap.put(sessionFactory, holder);
        }
        return holder;
    }

    public LinkedList<SimpleSessionScope> get()
    {
        return local.get();
    }

    public void set(LinkedList<SimpleSessionScope> scopes)
    {
        local.set(scopes);
    }
}
