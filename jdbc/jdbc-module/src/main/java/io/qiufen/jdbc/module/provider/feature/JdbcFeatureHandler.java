package io.qiufen.jdbc.module.provider.feature;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.module.kernel.spi.context.ContextFactory;
import io.qiufen.module.kernel.spi.facade.FacadeFinder;
import io.qiufen.module.kernel.spi.facade.FacadeRegistry;
import io.qiufen.module.kernel.spi.feature.BindableFeatureHandler;
import io.qiufen.module.object.api.declaration.BuilderDeclaration;
import io.qiufen.module.object.provider.context.ObjectContext;
import io.qiufen.module.object.spi.declaration.Declaration;

import java.util.List;

public class JdbcFeatureHandler extends BindableFeatureHandler<JdbcBinder, JdbcFeature>
{
    private FacadeFinder finder;

    @Override
    public void initFeature(ContextFactory contextFactory, FacadeRegistry facadeRegistry)
    {
        finder = facadeRegistry;
    }

    @Override
    protected JdbcBinder initBinder(JdbcFeature feature, ContextFactory contextFactory)
    {
        return new JdbcBinder();
    }

    @Override
    protected void doHandle(JdbcFeature feature, ContextFactory contextFactory, JdbcBinder binder)
    {
        feature.bind(binder);
        List<Declaration> declarationList = binder.getMapperDeclarations();
        if (CollectionUtil.isNotEmpty(declarationList))
        {
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            for (Declaration declaration : declarationList)
            {
                ((MapperBuilder) ((BuilderDeclaration) declaration).getBuilder()).setFinder(finder);
                objectContext.addDeclaration(declaration);
            }
        }
    }

    @Override
    protected void postModule(JdbcFeature feature, ContextFactory contextFactory, JdbcBinder binder)
    {
        List<Declaration> declarationList = binder.getMapperDeclarations();
        if (CollectionUtil.isNotEmpty(declarationList))
        {
            ObjectContext objectContext = contextFactory.getContext(ObjectContext.class);
            for (Declaration declaration : declarationList)
            {
                objectContext.build(declaration);
            }
        }
    }
}
