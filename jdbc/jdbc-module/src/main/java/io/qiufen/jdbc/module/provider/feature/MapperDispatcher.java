package io.qiufen.jdbc.module.provider.feature;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

@SuppressWarnings({"rawtypes", "unchecked"})
class MapperDispatcher extends Dispatcher
{
    private final MappedStatement mappedStatement;
    private final ReturnHandler returnHandler;

    MapperDispatcher(Class<?> mapperClass, Method method, SessionFactory factory)
    {
        super(method);
        CTString id = CTString.wrap(mapperClass.getName() + '.' + method.getName());
        mappedStatement = factory.getSqlMapFacade().getMappedStatement(id);

        Class<?> returnType = method.getReturnType();
        if (returnType == void.class || Void.class.isAssignableFrom(returnType))
        {
            this.returnHandler = VoidReturnHandler.INSTANCE;
        }
        else if (Collection.class.isAssignableFrom(returnType))
        {
            // 集合类型
            this.returnHandler = new CollectionReturnHandler(returnType);
        }
        else if (returnType.isArray())
        {
            // 数组类型
            this.returnHandler = new ArrayReturnHandler(returnType);
        }
        else
        {
            this.returnHandler = SimpleResultHandler.INSTANCE;
        }
    }

    @Override
    Object invoke(Session session, Object[] args) throws Exception
    {
        Object param = matchParam(args);
        Object result = session.execute(mappedStatement, param);
        if (result == null) return null;
        return returnHandler.handle(result);
    }

    interface ReturnHandler
    {
        Object handle(Object result) throws Exception;
    }

    private static class VoidReturnHandler implements ReturnHandler
    {
        private static final ReturnHandler INSTANCE = new VoidReturnHandler();

        @Override
        public Object handle(Object result)
        {
            return null;
        }
    }

    private static class CollectionReturnHandler implements ReturnHandler
    {
        private final Class<?> returnType;
        private final CollectionFactory collectionFactory;

        private CollectionReturnHandler(Class<?> returnType)
        {
            this.returnType = returnType;
            this.collectionFactory = matchCollectionFactory(returnType);
        }

        @Override
        public Object handle(Object result) throws Exception
        {
            Collection collection;
            if (result instanceof Collection)
            {
                if (this.returnType.isAssignableFrom(result.getClass()))
                {
                    return result;
                }

                Collection coll = (Collection) result;
                collection = collectionFactory.create(coll.size());
                collection.addAll(coll);
            }
            else if (result instanceof Object[])
            {
                Object[] array = (Object[]) result;
                collection = collectionFactory.create(array.length);
                Collections.addAll(collection, array);
            }
            else if (result.getClass().isArray())
            {
                int length = Array.getLength(result);
                collection = collectionFactory.create(length);
                for (int i = 0; i < length; i++)
                {
                    collection.add(Array.get(result, i));
                }
            }
            else
            {
                collection = collectionFactory.create(1);
                collection.add(result);
            }
            return collection;
        }

        private interface CollectionFactory
        {
            Collection create(int initialCapacity) throws Exception;
        }

        private static class ArrayListFactory implements CollectionFactory
        {
            @Override
            public Collection create(int initialCapacity)
            {
                return new ArrayList(initialCapacity);
            }
        }

        private static class LinkedListFactory implements CollectionFactory
        {
            @Override
            public Collection create(int initialCapacity)
            {
                return new LinkedList();
            }
        }

        private static class HashSetFactory implements CollectionFactory
        {
            @Override
            public Collection create(int initialCapacity)
            {
                return new HashSet(initialCapacity);
            }
        }

        private static class DefaultCollectionFactory implements CollectionFactory
        {
            private final Class<?> type;

            private DefaultCollectionFactory(Class<?> type) {this.type = type;}

            @Override
            public Collection create(int initialCapacity) throws InstantiationException, IllegalAccessException
            {
                return (Collection) type.newInstance();
            }
        }

        private static final CollectionFactory FACTORY_ARRAY = new ArrayListFactory();
        private static final CollectionFactory FACTORY_LINKED = new LinkedListFactory();
        private static final CollectionFactory FACTORY_HASH = new HashSetFactory();

        private CollectionFactory matchCollectionFactory(Class<?> type)
        {
            int modifiers = type.getModifiers();
            if (Modifier.isInterface(modifiers))
            {
                if (List.class.isAssignableFrom(type)) return FACTORY_ARRAY;
                if (Set.class.isAssignableFrom(type)) return FACTORY_HASH;
                if (Queue.class.isAssignableFrom(type)) return FACTORY_LINKED;
                throw new RuntimeException("Can't create collection for type:" + type);
            }
            return new DefaultCollectionFactory(type);
        }
    }

    private static class ArrayReturnHandler implements ReturnHandler
    {
        private final Class<?> returnType;

        private ArrayReturnHandler(Class<?> returnType) {this.returnType = returnType;}

        @Override
        public Object handle(Object result)
        {
            return fillIntoArray(result, returnType.getComponentType());
        }

        private Object fillIntoArray(Object result, Class<?> componentType)
        {
            if (result instanceof Collection)
            {
                Collection collection = (Collection) result;
                Object array = Array.newInstance(componentType, collection.size());
                int i = 0;
                for (Object o : collection)
                {
                    Array.set(array, i++, o);
                }
                return array;
            }

            if (result.getClass().isArray())
            {
                return result;
            }

            Object array = Array.newInstance(componentType, 1);
            Array.set(array, 0, result);
            return array;
        }
    }

    private static class SimpleResultHandler implements ReturnHandler
    {
        private static final ReturnHandler INSTANCE = new SimpleResultHandler();

        @Override
        public Object handle(Object result)
        {
            // 单对象类型
            if (result instanceof Collection)
            {
                Collection collection = (Collection) result;
                int size = collection.size();
                if (size == 0) return null;
                if (size == 1)
                {
                    Iterator iterator = collection.iterator();
                    return iterator.hasNext() ? iterator.next() : null;
                }
                throw new RuntimeException("None unique object returned.");
            }
            return result;
        }
    }
}
