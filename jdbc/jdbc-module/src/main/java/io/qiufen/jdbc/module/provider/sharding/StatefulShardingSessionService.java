package io.qiufen.jdbc.module.provider.sharding;

import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.api.transaction.Transaction;
import io.qiufen.jdbc.mapper.api.transaction.TransactionDefinition;
import io.qiufen.jdbc.module.api.Transactional;
import io.qiufen.jdbc.sharding.api.session.ShardingSessionService;

import java.util.LinkedList;

/**
 * session factory
 */
public class StatefulShardingSessionService implements ShardingSessionService
{
    private final ShardingSessionService shardingSessionService;
    private final ShardingSessionHolder holder;

    public StatefulShardingSessionService(ShardingSessionService shardingSessionService)
    {
        this.shardingSessionService = shardingSessionService;
        this.holder = ShardingSessionHolder.getInstance(shardingSessionService);
    }

    @Override
    public Session hit(String scope, String id, Object argument)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Session hit(String id, Object argument)
    {
        LinkedList<ShardingSessionScope> scopes = holder.get();
        if (null == scopes || scopes.isEmpty())
        {
            return shardingSessionService.hit(id, argument);
        }

        ShardingSessionScope sessionScope = scopes.peekLast();
        String scopeId = sessionScope.getId();
        Transactional transactional = sessionScope.getTransactional();
        //无需事务
        if (!Util.needTx(transactional))
        {
            return shardingSessionService.hit(scopeId, id, argument);
        }

        //需要事务
        //使用引用层事务
        ShardingSessionScope referSessionScope = sessionScope.getReferSessionScope();
        if (referSessionScope != null)
        {
            String referScopeId = referSessionScope.getId();
            Session session = shardingSessionService.hit(referScopeId, id, argument);
            Transaction transaction = session.getTransaction();
            if (transaction == null)
            {
                transaction = session.createTransaction(
                        new TransactionDefinition(transactional.isolation(), transactional.readonly()));
                transaction.begin();
                referSessionScope.getSessions().add(session);
            }
            return session;
        }
        //
        Session session = shardingSessionService.hit(scopeId, id, argument);
        Transaction transaction = session.getTransaction();
        if (transaction == null)
        {
            transaction = session.createTransaction(
                    new TransactionDefinition(transactional.isolation(), transactional.readonly()));
            transaction.begin();
            sessionScope.getSessions().add(session);
        }

        return session;
    }

    @Override
    public void unHit(String scope)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void unHit()
    {
        LinkedList<ShardingSessionScope> scopes = holder.get();
        if (null == scopes || scopes.isEmpty())
        {
            shardingSessionService.unHit();
            return;
        }

        ShardingSessionScope sessionScope = scopes.peekLast();
        //无需事务
        if (!Util.needTx(sessionScope.getTransactional()))
        {
            shardingSessionService.unHit(sessionScope.getId());
        }
        //需要事务，需要在拦截点与事务统一处理
    }
}
