package io.qiufen.jdbc.module.api;

import java.lang.annotation.*;
import java.sql.Connection;

/**
 * 标识事务
 *
 * @version 1.0
 * @since 1.0
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Transactional
{
    /**
     * 事务传播行为
     *
     * @return 传播行为
     */
    Propagation propagation() default Propagation.REQUIRED;

    /**
     * 事务隔离级别
     *
     * @return 事务隔离级别
     * @see java.sql.Connection
     */
    int isolation() default Connection.TRANSACTION_READ_UNCOMMITTED;

    /**
     * 是否只读事务
     *
     * @return 是否只读
     */
    boolean readonly() default false;
}
