package io.qiufen.jdbc.module.provider.sharding;

import io.qiufen.jdbc.sharding.api.session.ShardingSessionService;

import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by Administrator on 2017/12/17.
 */
public class ShardingSessionHolder
{
    private static final Map<ShardingSessionService, ShardingSessionHolder> holderMap;

    static
    {
        holderMap = new IdentityHashMap<ShardingSessionService, ShardingSessionHolder>();
    }

    private final ThreadLocal<LinkedList<ShardingSessionScope>> local = new ThreadLocal<LinkedList<ShardingSessionScope>>();

    private ShardingSessionHolder()
    {
    }

    public static ShardingSessionHolder getInstance(ShardingSessionService shardingSessionService)
    {
        ShardingSessionHolder holder = holderMap.get(shardingSessionService);
        if (holder != null)
        {
            return holder;
        }
        synchronized (ShardingSessionHolder.class)
        {
            holder = holderMap.get(shardingSessionService);
            if (holder != null)
            {
                return holder;
            }
            holder = new ShardingSessionHolder();
            holderMap.put(shardingSessionService, holder);
        }
        return holder;
    }

    public LinkedList<ShardingSessionScope> get()
    {
        return local.get();
    }

    public void set(LinkedList<ShardingSessionScope> scopes)
    {
        local.set(scopes);
    }
}
