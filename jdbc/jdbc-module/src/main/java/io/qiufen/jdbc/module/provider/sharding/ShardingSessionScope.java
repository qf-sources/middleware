package io.qiufen.jdbc.module.provider.sharding;

import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.module.api.Transactional;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/10/16 10:13
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class ShardingSessionScope
{
    private final Set<Session> sessions = new HashSet<Session>();

    private final String id;
    private final Method methodInfo;
    private final Transactional transactional;
    private final ShardingSessionScope referSessionScope;

    public ShardingSessionScope(Method methodInfo, Transactional transactional, ShardingSessionScope referSessionScope)
    {
        this.methodInfo = methodInfo;
        this.referSessionScope = referSessionScope;
        this.transactional = transactional;
        this.id = methodInfo.toString();
    }

    public String getId()
    {
        return id;
    }

    public Method getMethodInfo()
    {
        return methodInfo;
    }

    public Transactional getTransactional()
    {
        return transactional;
    }

    public ShardingSessionScope getReferSessionScope()
    {
        return referSessionScope;
    }

    public Set<Session> getSessions()
    {
        return sessions;
    }

    @Override
    public String toString()
    {
        return "ShardingSessionScope{" + "methodInfo=" + methodInfo + '}';
    }
}