package io.qiufen.jdbc.module.provider.feature;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.spi.handler.RowHandler;

import java.lang.reflect.Method;

class SelectDispatcher extends Dispatcher
{
    private final CTString id;
    private final int index;

    SelectDispatcher(Class<?> mapperClass, Method method, int index)
    {
        super(method);
        this.index = index;
        id = CTString.wrap(mapperClass.getName() + '.' + method.getName());
    }

    @Override
    Object invoke(Session session, Object[] args)
    {
        Object param = matchParam(args);
        session.select(id, param, (RowHandler<Object>) args[index]);
        return null;
    }
}
