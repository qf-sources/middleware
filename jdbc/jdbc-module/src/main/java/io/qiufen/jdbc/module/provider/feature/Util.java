package io.qiufen.jdbc.module.provider.feature;

import io.qiufen.common.io.IOUtil;
import io.qiufen.common.lang.Strings;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;

public class Util
{
    private static final SAXParserFactory FACTORY = SAXParserFactory.newInstance();

    static
    {
        try
        {
            FACTORY.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    static Class<?> getMapperClass(String resource) throws Exception
    {
        SAXParser parser = FACTORY.newSAXParser();
        XMLReader reader = parser.getXMLReader();
        D d = new D();
        reader.setContentHandler(d);

        InputStream stream = Util.class.getClassLoader().getResourceAsStream(resource);
        try
        {
            reader.parse(new InputSource(stream));
        }
        catch (InterruptException e)
        {
            //ignore
        }
        finally
        {
            IOUtil.close(stream);
        }
        String namespace = d.getNamespace();
        if (Strings.isEmpty(namespace))
        {
            throw new RuntimeException("None namespace defined for mapper class!");
        }
        return Class.forName(namespace);
    }

    private static class D extends DefaultHandler
    {
        private String namespace;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes)
        {
            if ("sqlMap".equals(qName))
            {
                namespace = attributes.getValue("namespace");
                throw new InterruptException();
            }
        }

        private String getNamespace()
        {
            return namespace;
        }
    }

    private static class InterruptException extends RuntimeException
    {
        @Override
        public Throwable fillInStackTrace()
        {
            return null;
        }
    }
}
