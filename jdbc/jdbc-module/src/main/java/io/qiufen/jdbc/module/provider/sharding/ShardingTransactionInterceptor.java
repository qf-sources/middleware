package io.qiufen.jdbc.module.provider.sharding;

import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.module.api.Transactional;
import io.qiufen.jdbc.sharding.api.session.ShardingSessionService;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.Set;

/**
 * Created by Administrator on 2017/12/16.
 */
public class ShardingTransactionInterceptor implements MethodInterceptor
{
    private final Logger log = LoggerFactory.getLogger(ShardingTransactionInterceptor.class);

    private final ShardingSessionService shardingSessionService;
    private final ShardingSessionHolder holder;

    public ShardingTransactionInterceptor(ShardingSessionService shardingSessionService)
    {
        this.shardingSessionService = shardingSessionService;
        this.holder = ShardingSessionHolder.getInstance(shardingSessionService);
    }

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable
    {
        Object target = methodInvocation.getMethod();
        Method method = methodInvocation.getMethod();
        Object[] args = methodInvocation.getArguments();
        doBefore(target, method, args);
        try
        {
            Object result = methodInvocation.proceed();
            doAfter(target, method, args, result);
            return result;
        }
        catch (Exception e)
        {
            doThrowing(target, method, args, e);
            throw e;
        }
    }

    public void doBefore(Object target, Method methodInfo, Object[] args)
    {
        Transactional transactional = getTransactional(target, methodInfo);
        if (null == transactional)
        {
            return;
        }
        addSession(transactional, methodInfo);
    }

    public void doAfter(Object target, Method methodInfo, Object[] args, Object returnObj)
    {
        Transactional transactional = getTransactional(target, methodInfo);
        if (null == transactional)
        {
            return;
        }
        ShardingSessionScope sessionScope = removeSession();
        if (null == sessionScope)
        {
            return;
        }
        Set<Session> sessions = sessionScope.getSessions();
        switch (transactional.propagation())
        {
            case REQUIRED:
            {
                if (sessionScope.getReferSessionScope() != null)
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("To up scope...");
                    }
                }
                else
                {
                    commit(sessions, sessionScope.getId());
                }
                break;
            }
            case REQUIRED_NEW:
            {
                commit(sessions, sessionScope.getId());
                break;
            }
            case NONE:
            {
                //ignore
            }
        }
    }

    public void doThrowing(Object target, Method methodInfo, Object[] args, Throwable throwable)
    {
        Transactional transactional = getTransactional(target, methodInfo);
        if (null == transactional)
        {
            return;
        }
        ShardingSessionScope sessionScope = removeSession();
        if (null == sessionScope)
        {
            return;
        }
        Set<Session> sessions = sessionScope.getSessions();
        switch (transactional.propagation())
        {
            case REQUIRED:
            {
                if (sessionScope.getReferSessionScope() != null)
                {
                    if (log.isDebugEnabled())
                    {
                        log.debug("To up scope...");
                    }
                }
                else
                {
                    rollback(sessions, sessionScope.getId());
                }
                break;
            }
            case REQUIRED_NEW:
            {
                rollback(sessions, sessionScope.getId());
                break;
            }
            case NONE:
            {
                //ignore
            }
        }
    }

    private Transactional getTransactional(Object target, Method methodInfo)
    {
        Transactional transactional = methodInfo.getAnnotation(Transactional.class);
        if (null != transactional)
        {
            return transactional;
        }
        return target.getClass().getAnnotation(Transactional.class);
    }

    private void commit(Set<Session> sessions, String scope)
    {
        try
        {
            for (Session session : sessions)
            {
                session.getTransaction().commit();
            }
        }
        finally
        {
            close(sessions, scope);
        }
    }

    private void rollback(Set<Session> sessions, String scope)
    {
        try
        {
            for (Session session : sessions)
            {
                session.getTransaction().rollback();
            }
        }
        finally
        {
            close(sessions, scope);
        }
    }

    private void close(Set<Session> sessions, String scope)
    {
        for (Session session : sessions)
        {
            try
            {
                session.close();
            }
            catch (Exception e)
            {
                log.error(e.toString(), e);
            }
            finally
            {
                shardingSessionService.unHit(scope);
            }
        }
    }

    private void addSession(Transactional transactional, Method methodInfo)
    {
        LinkedList<ShardingSessionScope> sessionScopes = holder.get();
        if (null == sessionScopes)
        {
            sessionScopes = new LinkedList<ShardingSessionScope>();
            holder.set(sessionScopes);
        }
        ShardingSessionScope sessionScope = null;
        switch (transactional.propagation())
        {
            case REQUIRED:
            {
                //无上层嵌套
                if (sessionScopes.isEmpty())
                {
                    sessionScope = new ShardingSessionScope(methodInfo, transactional, null);
                }
                else
                {
                    ShardingSessionScope outerSessionScope = sessionScopes.peekLast();
                    //上层有事务
                    if (Util.needTx(outerSessionScope.getTransactional()))
                    {
                        //引用类型
                        if (outerSessionScope.getReferSessionScope() == null)
                        {
                            sessionScope = new ShardingSessionScope(methodInfo, transactional, outerSessionScope);
                        }
                        else
                        {
                            sessionScope = new ShardingSessionScope(methodInfo, transactional,
                                    outerSessionScope.getReferSessionScope());
                        }
                    }
                    //上层无事务
                    else
                    {
                        sessionScope = new ShardingSessionScope(methodInfo, transactional, null);
                    }
                }
                break;
            }
            case REQUIRED_NEW:
            {
                sessionScope = new ShardingSessionScope(methodInfo, transactional, null);
                break;
            }
            case NONE:
            {
                sessionScope = new ShardingSessionScope(methodInfo, null, null);
                break;
            }
            case NOT_SUPPORT:
            {
                if (!sessionScopes.isEmpty() && Util.needTx(sessionScopes.peekLast().getTransactional()))
                {
                    throw new RuntimeException("Not support transaction.");
                }
                break;
            }
        }
        sessionScopes.offer(sessionScope);
    }

    private ShardingSessionScope removeSession()
    {
        LinkedList<ShardingSessionScope> scopes = holder.get();
        if (scopes.isEmpty())
        {
            return null;
        }
        return scopes.pollLast();
    }
}
