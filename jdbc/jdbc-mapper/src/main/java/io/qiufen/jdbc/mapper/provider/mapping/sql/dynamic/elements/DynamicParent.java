package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.jdbc.mapper.provider.mapping.sql.SqlChild;

public interface DynamicParent
{
    void addChild(SqlChild child);
}
