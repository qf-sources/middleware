package io.qiufen.jdbc.mapper.provider.session.context;

import io.qiufen.common.lang.Numbers;
import io.qiufen.jdbc.mapper.api.transaction.Transaction;
import io.qiufen.jdbc.mapper.api.transaction.TransactionDefinition;
import io.qiufen.jdbc.mapper.api.transaction.TransactionException;
import io.qiufen.jdbc.mapper.api.transaction.TransactionStateException;
import io.qiufen.jdbc.mapper.provider.scope.SessionScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

public class TransactionSessionContext implements SessionContext, Transaction
{
    private static final Logger log = LoggerFactory.getLogger(TransactionSessionContext.class);

    private static final AtomicInteger INDEX = new AtomicInteger(0);

    private final int id = INDEX.getAndIncrement();

    private final TransactionDefinition definition;
    private final SessionScope sessionScope;
    private final SessionScope transactionSessionScope;

    private TxState state = TxState.CREATED;

    private OldState oldState;

    public TransactionSessionContext(SessionContext sessionContext, TransactionDefinition definition)
    {
        this.definition = definition;
        try
        {
            this.sessionScope = sessionContext.createSessionScope();
        }
        catch (SQLException e)
        {
            throw new TransactionException(e);
        }
        this.transactionSessionScope = new TransactionSessionScope(this.sessionScope);
    }

    @Override
    public SessionScope createSessionScope()
    {
        return this.transactionSessionScope;
    }

    @Override
    public void close() throws SQLException
    {
        switch (state)
        {
            case CREATED:
                state = TxState.CLOSED;
                break;
            case STARTED:
                try
                {
                    rollback();
                }
                finally
                {
                    close0();
                    state = TxState.CLOSED;
                }
                break;
            case COMMIT:
            case ROLLBACK:
                close0();
                state = TxState.CLOSED;
                break;
            case CLOSED:
                break;
            default:
        }
        if (log.isDebugEnabled())
        {
            log.debug("Transaction closed:{}", getHexId());
        }
    }

    public Transaction begin() throws TransactionException
    {
        if (state != TxState.CREATED)
        {
            throw new TransactionStateException("Current state is " + state.name() + ", Not allow start transaction.");
        }
        if (log.isDebugEnabled())
        {
            log.debug("Start transaction:{}", getHexId());
        }
        try
        {
            Connection connection = sessionScope.getConnection();
            this.oldState = new OldState(connection.getAutoCommit(), connection.getTransactionIsolation(),
                    connection.isReadOnly());

            connection.setAutoCommit(false);
            connection.setTransactionIsolation(definition.getIsolation());
            connection.setReadOnly(definition.isReadOnly());
            this.state = TxState.STARTED;
        }
        catch (SQLException e)
        {
            throw new TransactionException(e);
        }
        return this;
    }

    public Transaction commit() throws TransactionException
    {
        if (state != TxState.STARTED && state != TxState.COMMIT && state != TxState.ROLLBACK)
        {
            throw new TransactionStateException("Current state is " + state.name() + ", Not allow commit transaction.");
        }
        if (log.isDebugEnabled())
        {
            log.debug("Commit transaction:{}", getHexId());
        }
        try
        {
            sessionScope.getConnection().commit();
            this.state = TxState.COMMIT;
        }
        catch (SQLException e)
        {
            throw new TransactionException(e);
        }
        return this;
    }

    public Transaction rollback() throws TransactionException
    {
        if (state != TxState.STARTED && state != TxState.COMMIT && state != TxState.ROLLBACK)
        {
            throw new TransactionStateException(
                    "Current state is " + state.name() + ", Not allow rollback transaction.");
        }
        if (log.isDebugEnabled())
        {
            log.debug("Rollback transaction:{}", getHexId());
        }
        try
        {
            sessionScope.getConnection().rollback();
            this.state = TxState.ROLLBACK;
        }
        catch (SQLException e)
        {
            throw new TransactionException(e);
        }
        return this;
    }

    public TransactionDefinition getDefinition()
    {
        return this.definition;
    }

    public int getId()
    {
        return this.id;
    }

    public String getHexId()
    {
        return Numbers.toFullHex(this.id);
    }

    private void close0() throws SQLException
    {
        try
        {
            Connection connection = sessionScope.getConnection();
            connection.setAutoCommit(oldState.isAutoCommit());
            connection.setTransactionIsolation(oldState.getIsolation());
            connection.setReadOnly(oldState.isReadOnly());

            this.oldState = null;
        }
        finally
        {
            sessionScope.close();
        }
    }

    private enum TxState
    {
        CREATED,
        STARTED,
        COMMIT,
        ROLLBACK,
        CLOSED
    }

    private static class TransactionSessionScope implements SessionScope
    {
        private final Connection connection;

        private TransactionSessionScope(SessionScope sessionScope)
        {
            this.connection = sessionScope.getConnection();
        }

        public Connection getConnection()
        {
            return connection;
        }

        public void close()
        {
        }
    }

    private static class OldState
    {
        private final boolean autoCommit;
        private final int isolation;
        private final boolean readOnly;

        OldState(boolean autoCommit, int isolation, boolean readOnly)
        {
            this.autoCommit = autoCommit;
            this.isolation = isolation;
            this.readOnly = readOnly;
        }

        private boolean isAutoCommit()
        {
            return autoCommit;
        }

        private int getIsolation()
        {
            return isolation;
        }

        private boolean isReadOnly()
        {
            return readOnly;
        }
    }
}
