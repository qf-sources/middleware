package io.qiufen.jdbc.mapper.provider.mapping.sql;

import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

public interface Sql
{
    void applyParameter(StatementScope statementScope, Object parameterObject);
}
