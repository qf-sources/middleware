package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.provider.common.utils.SimpleDateFormatter;
import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * SQL Date implementation of TypeHandler
 */
class SqlDateTypeHandler extends BaseTypeHandler implements TypeHandler
{
    private static final String DATE_FORMAT = "yyyy/MM/dd";

    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setDate(i, (java.sql.Date) parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        Object sqlDate = rs.getDate(columnName);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return sqlDate;
        }
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        Object sqlDate = rs.getDate(columnIndex);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return sqlDate;
        }
    }

    public Object valueOf(String s)
    {
        return SimpleDateFormatter.format(DATE_FORMAT, s);
    }

}
