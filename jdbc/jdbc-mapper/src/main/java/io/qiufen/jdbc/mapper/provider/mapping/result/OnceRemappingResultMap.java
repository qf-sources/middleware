package io.qiufen.jdbc.mapper.provider.mapping.result;

import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * An automatic result map for simple stuff
 */
public class OnceRemappingResultMap extends AutoResultMap
{
    public OnceRemappingResultMap(SqlMapContext sqlMapContext)
    {
        super(sqlMapContext);
    }

    public Object[] getResults(StatementScope statementScope, ResultSet rs) throws SQLException
    {
        if (getResultMappings() == null)
        {
            synchronized (this)
            {
                if (getResultMappings() == null)
                {
                    initialize(rs);
                }
            }
        }
        return super.getResults(statementScope, rs);
    }

    @Override
    public Object setResultObjectValues(StatementScope statementScope, ResultSet rs) throws SQLException
    {
        if (getResultMappings() == null)
        {
            synchronized (this)
            {
                if (getResultMappings() == null)
                {
                    initialize(rs);
                }
            }
        }
        return super.setResultObjectValues(statementScope, rs);
    }
}

