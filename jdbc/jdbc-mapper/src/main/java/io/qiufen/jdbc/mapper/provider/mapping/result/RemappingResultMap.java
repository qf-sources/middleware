package io.qiufen.jdbc.mapper.provider.mapping.result;

import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * An automatic result map for simple stuff
 */
public class RemappingResultMap extends AutoResultMap
{
    private byte initialized = 0;

    public RemappingResultMap(SqlMapContext sqlMapContext)
    {
        super(sqlMapContext);
    }

    @Override
    public ResultMap prepare()
    {
        RemappingResultMap resultMap = new RemappingResultMap(getSqlMapContext());
        resultMap.setId(getId());
        resultMap.setResultClass(getResultClass());
        return resultMap;
    }

    public Object[] getResults(StatementScope statementScope, ResultSet rs) throws SQLException
    {
        initialize(rs);
        return super.getResults(statementScope, rs);
    }

    @Override
    public Object setResultObjectValues(StatementScope statementScope, ResultSet rs) throws SQLException
    {
        initialize(rs);
        return super.setResultObjectValues(statementScope, rs);
    }

    @Override
    void initialize(ResultSet rs)
    {
        if (this.initialized == 0)
        {
            super.initialize(rs);
            this.initialized = 1;
        }
    }
}

