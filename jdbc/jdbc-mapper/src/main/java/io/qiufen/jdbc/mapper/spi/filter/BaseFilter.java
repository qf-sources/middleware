package io.qiufen.jdbc.mapper.spi.filter;

import io.qiufen.common.filter.Context;
import io.qiufen.common.filter.Filter;

public interface BaseFilter<C extends Context> extends Filter<C> {}
