package io.qiufen.jdbc.mapper.provider.mapping.sql.raw;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

/**
 * A non-executable SQL container simply for
 * communicating raw SQL around the framework.
 */
public final class RawSql implements Sql
{
    private final CTString sql;

    public RawSql(CTString sql)
    {
        this.sql = sql;
    }

    @Override
    public void applyParameter(StatementScope statementScope, Object parameterObject)
    {
        throw new RuntimeException("Method not implemented on RawSql.");
    }

    public CTString getSql()
    {
        return sql;
    }
}
