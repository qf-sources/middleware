package io.qiufen.jdbc.mapper.api.session;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.api.transaction.Transaction;
import io.qiufen.jdbc.mapper.api.transaction.TransactionDefinition;
import io.qiufen.jdbc.mapper.api.transaction.TransactionException;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;
import io.qiufen.jdbc.mapper.spi.handler.RowHandler;

import java.util.Collection;
import java.util.List;

public interface Session
{
    int insert(CTString id, Object parameterObject) throws SessionException;

    int[] insert(CTString id, Collection collection) throws SessionException;

    int update(CTString id, Object parameterObject) throws SessionException;

    int[] update(CTString id, Collection collection) throws SessionException;

    int delete(CTString id, Object parameterObject) throws SessionException;

    int[] delete(CTString id, Collection collection) throws SessionException;

    <T> T selectOne(CTString id, Object parameterObject) throws SessionException;

    Object selectOne(CTString id, Object parameterObject, Object resultObject) throws SessionException;

    <T> List<T> select(CTString id, Object parameterObject) throws SessionException;

    <T> void select(CTString id, Object parameterObject, RowHandler<T> rowHandler) throws SessionException;

    /**
     * 创建事务支持
     * 当使用UserConnection抛出：UnsupportedTransactionException
     * 尝试重复创建则抛出：DuplicationTransactionException
     *
     * @param definition 事务定义
     * @return 事务对象
     */
    Transaction createTransaction(TransactionDefinition definition) throws TransactionException;

    /**
     * 获取当前事务对象
     * 当使用UserConnection抛出：UnsupportedTransactionException
     *
     * @return 事务对象
     */
    Transaction getTransaction() throws TransactionException;

    void close() throws SessionException;

    boolean isClosed();

    Object execute(CTString id, Object parameterObject) throws SessionException;

    Object execute(MappedStatement mappedStatement, Object parameterObject) throws SessionException;
}

