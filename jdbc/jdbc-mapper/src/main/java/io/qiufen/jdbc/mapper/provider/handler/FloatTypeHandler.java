package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Float implementation of TypeHandler
 */
class FloatTypeHandler extends BaseTypeHandler implements TypeHandler
{
    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setFloat(i, (Float) parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        float f = rs.getFloat(columnName);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return f;
        }
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        float f = rs.getFloat(columnIndex);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return f;
        }
    }

    public Object valueOf(String s)
    {
        return Float.valueOf(s);
    }

}
