package io.qiufen.jdbc.mapper.provider.exchange.accessplan;

import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;

/**
 * Access plan for working with beans
 */
class ComplexAccessPlan extends BaseAccessPlan
{
    private static final Probe PROBE = ProbeFactory.getProbe();

    ComplexAccessPlan(Class<?> clazz, String[] propertyNames)
    {
        super(clazz, propertyNames);
    }

    public void setProperties(Object object, Object[] values)
    {
        for (int i = 0; i < propertyNames.length; i++)
        {
            PROBE.setObject(object, propertyNames[i], values[i]);
        }
    }

    @Override
    public void setProperty(Object object, int index, Object value)
    {
        PROBE.setObject(object, propertyNames[index], value);
    }

    public Object[] getProperties(Object object)
    {
        Object[] values = new Object[propertyNames.length];
        for (int i = 0; i < propertyNames.length; i++)
        {
            values[i] = PROBE.getObject(object, propertyNames[i]);
        }
        return values;
    }

    @Override
    public Object getProperty(Object object, int index)
    {
        return PROBE.getObject(object, propertyNames[index]);
    }
}
