package io.qiufen.jdbc.mapper.provider.mapping.sql.simple;

import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

public final class SimpleDynamicSql implements Sql
{
    public static final char SYMBOL = '$';

    private final CTString sqlStatement;
    private final SqlMapContext sqlMapContext;

    public SimpleDynamicSql(SqlMapContext sqlMapContext, CTString sqlStatement)
    {
        this.sqlStatement = sqlStatement;
        this.sqlMapContext = sqlMapContext;
    }

    public static boolean isSimpleDynamicSql(CharSequence sql)
    {
        return CharSequences.indexOf(sql, SimpleDynamicSql.SYMBOL, 0) > -1;
    }

    public static StringBuilder processDynamicElements(SqlMapContext sqlMapContext, CTString sqlSegment,
            Object parameterObject)
    {
        StringBuilder sqlSegmentBuffer = new StringBuilder(sqlSegment.length()).append(sqlSegment);
        CharSequences.replaceArgs(sqlSegmentBuffer, SYMBOL, SYMBOL, parameterObject,
                sqlMapContext.getDynamicElementParser());
        return sqlSegmentBuffer;
    }

    public static StringBuilder processDynamicElements(SqlMapContext sqlMapContext, String sqlSegment,
            Object parameterObject)
    {
        StringBuilder sqlSegmentBuffer = new StringBuilder(sqlSegment.length()).append(sqlSegment);
        CharSequences.replaceArgs(sqlSegmentBuffer, SYMBOL, SYMBOL, parameterObject,
                sqlMapContext.getDynamicElementParser());
        return sqlSegmentBuffer;
    }

    @Override
    public void applyParameter(StatementScope statementScope, Object parameterObject)
    {
        CharSequence sql = processDynamicElements(sqlMapContext, sqlStatement, parameterObject);
        statementScope.setSql(sql);
    }
}
