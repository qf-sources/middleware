package io.qiufen.jdbc.mapper.provider.session;

import java.sql.ResultSet;
import java.sql.SQLException;

interface ResultSetHandler
{
    void handle(ResultSet resultSet) throws SQLException;
}
