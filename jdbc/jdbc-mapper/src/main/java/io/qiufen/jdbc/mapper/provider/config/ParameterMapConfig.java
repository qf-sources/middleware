package io.qiufen.jdbc.mapper.provider.config;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.handler.extension.CustomTypeHandler;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.DefaultParameterMapBuilder;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.extension.TypeHandlerCallback;

import java.util.ArrayList;
import java.util.List;

public class ParameterMapConfig
{
    private final SqlMapContext sqlMapContext;
    private final Class<?> parameterClass;
    private final List<ParameterMapping> parameterMappingList;
    private final DefaultParameterMapBuilder builder;

    public ParameterMapConfig(SqlMapContext sqlMapContext, CTString id, Class<?> parameterClass)
    {
        this.sqlMapContext = sqlMapContext;
        this.parameterClass = parameterClass;
        this.builder = new DefaultParameterMapBuilder(sqlMapContext).setId(id).setParameterClass(parameterClass);

        this.parameterMappingList = new ArrayList<ParameterMapping>();
    }

    public void addParameterMapping(String propertyName, Class<?> javaClass, String jdbcType, String nullValue,
            Object typeHandlerImpl)
    {
        TypeHandler handler;
        if (typeHandlerImpl != null)
        {
            if (typeHandlerImpl instanceof TypeHandlerCallback)
            {
                handler = new CustomTypeHandler((TypeHandlerCallback) typeHandlerImpl);
            }
            else if (typeHandlerImpl instanceof TypeHandler)
            {
                handler = (TypeHandler) typeHandlerImpl;
            }
            else
            {
                throw new RuntimeException(
                        "The class '" + typeHandlerImpl + "' is not a valid implementation of TypeHandler or TypeHandlerCallback");
            }
        }
        else
        {
            handler = sqlMapContext.resolveTypeHandler(parameterClass, propertyName, javaClass, jdbcType);
        }
        ParameterMapping mapping = new ParameterMapping();
        mapping.setPropertyName(propertyName);
        mapping.setJdbcTypeName(jdbcType);
        mapping.setNullValue(nullValue);
        mapping.setTypeHandler(handler);
        mapping.setJavaType(javaClass);
        parameterMappingList.add(mapping);
    }

    public void finish()
    {
        ParameterMapping[] parameterMappings = new ParameterMapping[parameterMappingList.size()];
        parameterMappingList.toArray(parameterMappings);
        builder.setParameterMappings(parameterMappings);
        sqlMapContext.addParameterMap(builder.build());
        parameterMappingList.clear();
    }
}
