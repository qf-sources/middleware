package io.qiufen.jdbc.mapper.api.session;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/23 19:31
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class SessionException extends RuntimeException
{
    public SessionException(String message)
    {
        super(message);
    }

    public SessionException(Throwable cause)
    {
        super(cause);
    }

    public SessionException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
