package io.qiufen.jdbc.mapper.provider.common.beans;

import io.qiufen.common.bean.ClassUtil;
import javassist.*;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * This class represents a cached set of class definition information that
 * allows for easy mapping between property names and getter/setter methods.
 */
public class ClassInfo
{
    private static final Map<Class<?>, ClassInfo> CACHE = new IdentityHashMap<Class<?>, ClassInfo>();

    private static final CtClass[] EMPTY = {};
    private static final Map<Class<?>, Class<?>> PRIMITIVES = new IdentityHashMap<Class<?>, Class<?>>();

    static
    {
        PRIMITIVES.put(byte.class, Byte.class);
        PRIMITIVES.put(short.class, Short.class);
        PRIMITIVES.put(int.class, Integer.class);
        PRIMITIVES.put(long.class, Long.class);
        PRIMITIVES.put(float.class, Float.class);
        PRIMITIVES.put(double.class, Double.class);
        PRIMITIVES.put(boolean.class, Boolean.class);
        PRIMITIVES.put(char.class, Character.class);
    }

    private final Class<?> clazz;
    private final Map<String, Property> properties = new HashMap<String, Property>();

    private Constructor constructor;
    private String[] readablePropertyNames;
    private String[] writeablePropertyNames;

    private ClassInfo(Class<?> clazz)
    {
        this.clazz = clazz;

        ClassPool classPool = new ClassPool();
        classPool.appendSystemPath();

        addDefaultConstructor(classPool);
        addReadAndWrites(clazz, classPool);
    }

    /**
     * Gets an instance of ClassInfo for the specified class.
     *
     * @param clazz The class for which to lookup the method cache.
     * @return The method cache for the class
     */
    public static ClassInfo getInstance(Class<?> clazz)
    {
        ClassInfo cached = CACHE.get(clazz);
        if (null != cached)
        {
            return cached;
        }
        synchronized (CACHE)
        {
            cached = CACHE.get(clazz);
            if (cached != null)
            {
                return cached;
            }
            cached = new ClassInfo(clazz);
            CACHE.put(clazz, cached);
        }
        return cached;
    }

    private void addDefaultConstructor(ClassPool classPool)
    {
        boolean hasDefault = false;
        java.lang.reflect.Constructor<?>[] constructors = clazz.getConstructors();
        for (java.lang.reflect.Constructor<?> constructor1 : constructors)
        {
            if (constructor1.getParameterTypes().length == 0)
            {
                hasDefault = true;
                break;
            }
        }
        if (!hasDefault)
        {
            return;
        }

        this.constructor = createConstructor(classPool);
    }

    private void addReadAndWrites(Class<?> clazz, ClassPool classPool)
    {
        PropertyDescriptor[] descriptors;
        try
        {
            descriptors = Introspector.getBeanInfo(clazz).getPropertyDescriptors();
        }
        catch (IntrospectionException e)
        {
            throw new RuntimeException(e);
        }
        for (int i = 0; i < descriptors.length; i++)
        {
            PropertyDescriptor descriptor = descriptors[i];
            String name = descriptor.getName();
            if ("class".equals(name))
            {
                continue;
            }

            Property property = createProperty(classPool, descriptor, i);
            properties.put(name, property);
        }
    }

    public Object instantiateClass()
    {
        if (constructor == null)
        {
            throw new RuntimeException(
                    "Error instantiating class.  There is no default constructor for class " + clazz.getName());
        }

        try
        {
            return constructor.newInstance();
        }
        catch (Throwable e)
        {
            throw new RuntimeException("Error instantiating class. Cause: " + e, e);
        }
    }

    public Property getProperty(String propertyName)
    {
        Property property = properties.get(propertyName);
        if (property == null)
        {
            throw new ProbeException(
                    "There is no property named '" + propertyName + "' in class '" + clazz.getName() + "'");
        }
        return property;
    }

    /**
     * Gets the type for a property setter
     *
     * @param propertyName - the name of the property
     * @return The Class of the propery setter
     */
    public Class<?> getSetterType(String propertyName)
    {
        return getProperty(propertyName).getType();
    }

    /**
     * Gets the type for a property getter
     *
     * @param propertyName - the name of the property
     * @return The Class of the propery getter
     */
    public Class<?> getGetterType(String propertyName)
    {
        return getProperty(propertyName).getType();
    }

    /**
     * Gets an array of the readable properties for an object
     *
     * @return The array
     */
    public String[] getReadablePropertyNames()
    {
        return readablePropertyNames;
    }

    /**
     * Gets an array of the writeable properties for an object
     *
     * @return The array
     */
    public String[] getWriteablePropertyNames()
    {
        return writeablePropertyNames;
    }

    /**
     * Check to see if a class has a writeable property by name
     *
     * @param propertyName - the name of the property to check
     * @return True if the object has a writeable property by the name
     */
    public boolean hasWritableProperty(String propertyName)
    {
        return getProperty(propertyName).isWritable();
    }

    /**
     * Check to see if a class has a readable property by name
     *
     * @param propertyName - the name of the property to check
     * @return True if the object has a readable property by the name
     */
    public boolean hasReadableProperty(String propertyName)
    {
        return getProperty(propertyName).isReadable();
    }

    @SuppressWarnings("unchecked")
    private Class<? extends Property> makePropertyClass(PropertyDescriptor propertyDescriptor, ClassPool classPool,
            int propertyIndex) throws NotFoundException, CannotCompileException
    {
        String className = clazz.getName();
        String propertyName = propertyDescriptor.getName();
        String implClassName = className + "_GP" + propertyIndex;
        CtClass implClass = classPool.makeClass(implClassName, toCtClass(BaseProperty.class, classPool));

        //1.constructors
        CtConstructor ctConstructor = new CtConstructor(new CtClass[]{toCtClass(Class.class, classPool)}, implClass);
        ctConstructor.setBody("{super($1);}");
        implClass.addConstructor(ctConstructor);

        CtMethod getNameMethod = new CtMethod(toCtClass(String.class, classPool), "getName", EMPTY, implClass);
        getNameMethod.setBody("{return \"" + propertyName + "\";}");
        implClass.addMethod(getNameMethod);

        CtClass objType = toCtClass(Object.class, classPool);
        Method getter = propertyDescriptor.getReadMethod();
        Class<?> propertyType = propertyDescriptor.getPropertyType();
        List<String> readableList = new ArrayList<String>();
        //2.getters
        if (getter != null)
        {
            CtMethod getMethod = new CtMethod(objType, "get", new CtClass[]{objType}, implClass);
            String expression = "((" + className + ") $1)." + getter.getName() + "()";
            if (propertyType.isPrimitive())
            {
                //like, new Integer(value)
                expression = "new " + PRIMITIVES.get(propertyType).getName() + "(" + expression + ")";
            }
            getMethod.setBody("{return " + expression + ";}");
            implClass.addMethod(getMethod);

            CtMethod isReadable = new CtMethod(toCtClass(boolean.class, classPool), "isReadable", EMPTY, implClass);
            isReadable.setBody("{return true;}");
            implClass.addMethod(isReadable);
            readableList.add(propertyName);
        }

        //3.setters
        List<String> writableList = new ArrayList<String>();
        Method setter = propertyDescriptor.getWriteMethod();
        if (setter != null)
        {
            CtMethod setMethod = new CtMethod(CtClass.voidType, "set", new CtClass[]{objType, objType}, implClass);
            String expression;
            if (propertyType.isPrimitive())
            {
                //like, ((Integer) value).intValue()
                expression = "(" + "(" + PRIMITIVES.get(propertyType)
                        .getName() + ") $2" + ")." + propertyType.getName() + "Value()";
            }
            else
            {
                expression = "(" + ClassUtil.getClassName(propertyType) + ") $2";
            }
            setMethod.setBody("{((" + className + ") $1)." + setter.getName() + "(" + expression + ");}");
            implClass.addMethod(setMethod);

            CtMethod isWritable = new CtMethod(toCtClass(boolean.class, classPool), "isWritable", EMPTY, implClass);
            isWritable.setBody("{return true;}");
            implClass.addMethod(isWritable);
            writableList.add(propertyName);
        }

        readablePropertyNames = new String[readableList.size()];
        readableList.toArray(readablePropertyNames);
        writeablePropertyNames = new String[writableList.size()];
        writableList.toArray(writeablePropertyNames);

        return (Class<? extends Property>) implClass.toClass();
    }

    @SuppressWarnings("unchecked")
    private Class<? extends Constructor> makeConstructorClass(ClassPool classPool)
            throws NotFoundException, CannotCompileException
    {
        String className = clazz.getName();
        String implClassName = className + "_GC";
        CtClass implClass = classPool.makeClass(implClassName);
        implClass.setInterfaces(new CtClass[]{toCtClass(Constructor.class, classPool)});
        CtMethod newInstance = new CtMethod(toCtClass(clazz, classPool), "newInstance", EMPTY, implClass);
        newInstance.setBody("{return new " + className + "();}");
        implClass.addMethod(newInstance);

        return (Class<? extends Constructor>) implClass.toClass();
    }

    private CtClass toCtClass(Class<?> clazz, ClassPool classPool) throws NotFoundException
    {
        return classPool.getCtClass(clazz.getName());
    }

    private Constructor createConstructor(ClassPool classPool)
    {
        Class<? extends Constructor> implClass;
        try
        {
            implClass = makeConstructorClass(classPool);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }

        Constructor constructor;
        try
        {
            constructor = implClass.newInstance();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return constructor;
    }

    private Property createProperty(ClassPool classPool, PropertyDescriptor descriptor, int propertyIndex)
    {
        Class<? extends Property> implClass;
        try
        {
            implClass = makePropertyClass(descriptor, classPool, propertyIndex);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }

        Property property;
        try
        {
            property = (Property) implClass.getConstructors()[0].newInstance(descriptor.getPropertyType());
        }
        catch (InvocationTargetException e)
        {
            throw new RuntimeException(e.getTargetException());
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return property;
    }
}
