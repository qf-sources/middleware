package io.qiufen.jdbc.mapper.provider.mapping.parameter;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import io.qiufen.jdbc.mapper.provider.exchange.DataExchange;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class AbstractParameterMap implements ParameterMap
{
    private final SqlMapContext sqlMapContext;

    private CTString id;
    private Class<?> parameterClass;

    private DataExchange dataExchange;

    public AbstractParameterMap(SqlMapContext sqlMapContext)
    {
        this.sqlMapContext = sqlMapContext;
    }

    public SqlMapContext getSqlMapContext()
    {
        return sqlMapContext;
    }

    @Override
    public CTString getId()
    {
        return id;
    }

    void setId(CTString id)
    {
        this.id = id;
    }

    @Override
    public Class<?> getParameterClass()
    {
        return parameterClass;
    }

    void setParameterClass(Class<?> parameterClass)
    {
        this.parameterClass = parameterClass;
    }

    @Override
    public Object[] getParameterObjectValues(StatementScope statementScope, Object parameterObject)
    {
        return dataExchange.getData(statementScope, this, parameterObject);
    }

    /**
     * @param ps         ps
     * @param parameters parameters
     */
    public void setParameters(StatementScope statementScope, PreparedStatement ps, Object[] parameters)
            throws SQLException
    {
        int len = this.countParameterMapping();
        if (len > 0)
        {
            for (int i = 0; i < len; i++)
            {
                this.getParameterMapping(i).setMappingValue(ps, i + 1, parameters[i]);
            }
        }
    }

    @Override
    public void setParameters(StatementScope statementScope, PreparedStatement ps, Object parameterObject)
            throws SQLException
    {
        if (countParameterMapping() > 0)
        {
            ParameterMappingValue parameterMappingValue = statementScope.getParameterMappingValue();
            parameterMappingValue.start();
            dataExchange.setData(statementScope, this, ps, parameterObject);
            parameterMappingValue.end();
        }
    }

    protected void initDataExchange()
    {
        dataExchange = sqlMapContext.getDataExchangeFactory().getDataExchangeForClass(parameterClass);
        dataExchange.initialize(this);
    }
}
