package io.qiufen.jdbc.mapper.provider.filter;

import io.qiufen.common.filter.FilterChain;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMappingValue;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMapping;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMappingValue;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;
import io.qiufen.jdbc.mapper.provider.session.ResultValue;
import io.qiufen.jdbc.mapper.spi.filter.StatementScopeFilter;
import io.qiufen.jdbc.mapper.spi.filter.StatementScopeFilterContext;
import org.slf4j.Logger;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class LoggingFilter implements StatementScopeFilter
{
    @Override
    public void doFilter(FilterChain filterChain, StatementScopeFilterContext statementScopeFilterContext)
            throws Exception
    {
        StatementScope statementScope = statementScopeFilterContext.getStatementScope();
        Logger logger = statementScope.getStatement().getLogger();
        if (logger.isDebugEnabled())
        {
            logger.debug("<=SQL:{}", statementScope.getSql());
            statementScope.setParameterMappingValue(new ParameterMappingLogger(logger));
            statementScope.setResultMappingValue(new ResultMappingLogger(logger, statementScope));
            statementScope.setResultValue(new ResultLogger(logger));
        }
        filterChain.doFilter(statementScopeFilterContext);
    }

    private static class BaseLogger
    {
        final Logger logger;

        private BaseLogger(Logger logger) {this.logger = logger;}
    }

    private static class ParameterMappingLogger extends BaseLogger implements ParameterMappingValue
    {
        private final StringBuilder builder = new StringBuilder();

        private ParameterMappingLogger(Logger logger)
        {
            super(logger);
        }

        @Override
        public void onValue(ParameterMapping mapping, Object value)
        {
            builder.append(',');
            if (value == null)
            {
                builder.append("null");
                return;
            }
            builder.append('(').append(value.getClass().getSimpleName()).append(')').append(value);
        }

        @Override
        public void start() {}

        @Override
        public void end()
        {
            logger.debug("<=Params:{}", builder.substring(1));
            builder.setLength(0);
        }
    }

    private static class ResultMappingLogger extends BaseLogger implements ResultMappingValue
    {
        private final List<Object> builder = new ArrayList<Object>();
        private final StatementScope statementScope;
        private boolean printHeader = false;

        private ResultMappingLogger(Logger logger, StatementScope statementScope)
        {
            super(logger);
            this.statementScope = statementScope;
        }

        @Override
        public void onValue(ResultMapping mapping, Object value)
        {
            builder.add(value);
        }

        @Override
        public void start(ResultSet rs)
        {
            if (printHeader)
            {
                return;
            }
            printHeader = true;
            printResultHeaders(logger, statementScope.getResultMap());
        }

        @Override
        public void end()
        {
            logger.debug("=>Result values:{}", builder);
            builder.clear();
        }

        private void printResultHeaders(Logger logger, ResultMap resultMap)
        {
            StringBuilder builder = new StringBuilder();
            ResultMapping[] mappings = resultMap.getResultMappings();
            for (ResultMapping mapping : mappings)
            {
                builder.append(',').append(mapping.getColumnName());
            }
            logger.debug("=>Result headers:{}", builder.substring(1));
        }
    }

    private static class ResultLogger extends BaseLogger implements ResultValue
    {
        private ResultLogger(Logger logger)
        {
            super(logger);
        }

        @Override
        public void onValue(int rows)
        {
            logger.debug("=>Influenced rows:{}", rows);
        }

        @Override
        public void onValue(int[] rows)
        {
            logger.debug("=>Influenced rows:{}", rows);
        }
    }
}
