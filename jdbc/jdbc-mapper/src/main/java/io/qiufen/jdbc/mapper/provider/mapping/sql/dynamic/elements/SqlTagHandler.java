package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.common.lang.StringBuilder;

public interface SqlTagHandler
{
    // BODY TAG
    int SKIP_BODY = 0;
    int INCLUDE_BODY = 1;
    int REPEAT_BODY = 2;

    int doStartFragment(SqlTagContext ctx, SqlTag tag, Object parameterObject);

    int doEndFragment(SqlTagContext ctx, SqlTag tag, Object parameterObject, StringBuilder bodyContent);

    void doPrepend(SqlTagContext ctx, SqlTag tag, Object parameterObject, StringBuilder bodyContent);
}
