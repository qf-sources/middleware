package io.qiufen.jdbc.mapper.provider.statement;

public enum StatementType
{
    UNKNOWN,
    INSERT,
    UPDATE,
    DELETE,
    SELECT
}
