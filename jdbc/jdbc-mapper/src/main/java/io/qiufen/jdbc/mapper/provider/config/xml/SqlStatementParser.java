package io.qiufen.jdbc.mapper.provider.config.xml;

import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.Strings;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.common.resources.Resources;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletUtils;
import io.qiufen.jdbc.mapper.provider.config.MappedStatementConfig;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import java.util.Properties;

public class SqlStatementParser
{
    private static final Logger log = LoggerFactory.getLogger(SqlStatementParser.class);

    private final XmlParserState state;

    public SqlStatementParser(XmlParserState state)
    {
        this.state = state;
    }

    public void parseGeneralStatement(Node node, MappedStatement statement)
            throws ClassNotFoundException, NoSuchFieldException, SqlMapConfigException
    {
        // get attributes
        Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
        String id = attributes.getProperty("id");
        String parameterMapName = state.applyNamespace(attributes.getProperty("parameterMap"));
        String parameterClassName = attributes.getProperty("parameterClass");
        String resultMapName = attributes.getProperty("resultMap");
        String resultClassName = attributes.getProperty("resultClass");
        String resultSetType = attributes.getProperty("resultSetType");
        String fetchSize = attributes.getProperty("fetchSize");
        String allowRemapping = attributes.getProperty("remapResults");
        String timeout = attributes.getProperty("timeout");

        log.debug("Parsing statement:{}...", id);

        if (state.isUseStatementNamespaces())
        {
            id = state.applyNamespace(id);
        }

        CTString ctResultMapName = null;
        if (resultMapName != null)
        {
            resultMapName = state.getFirstToken(resultMapName);
            resultMapName = state.applyNamespace(resultMapName);
            ctResultMapName = CTString.of(resultMapName);
        }

        if (resultClassName != null)
        {
            resultClassName = state.getFirstToken(resultClassName);
        }

        Class<?> parameterClass = resolveClass(parameterClassName);
        Class<?> resultClass = resolveClass(resultClassName);

        Integer timeoutInt = timeout == null ? null : Integer.valueOf(timeout);
        Integer fetchSizeInt = fetchSize == null ? null : Integer.valueOf(fetchSize);
        boolean allowRemappingBool = "true".equals(allowRemapping);

        MappedStatementConfig statementConf = state.getSqlMapContext()
                .newMappedStatementConfig(CTString.of(id), statement, new XMLSqlSource(state, node), parameterMapName,
                        parameterClass, ctResultMapName, resultClass, resultSetType, fetchSizeInt, allowRemappingBool,
                        timeoutInt);
        setGeneratedKeyConfig(attributes, node, statementConf);
    }

    private Class<?> resolveClass(String resultClassName) throws ClassNotFoundException
    {
        if (resultClassName != null)
        {
            return Resources.classForName(
                    state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(resultClassName));
        }
        return null;
    }

    private void setGeneratedKeyConfig(Properties attributes, Node node, MappedStatementConfig config)
            throws ClassNotFoundException, NoSuchFieldException, SqlMapConfigException
    {
        if (!"insert".equals(node.getNodeName()))
        {
            return;
        }

        String property = attributes.getProperty("keyProperty");
        if (Strings.isEmpty(property)) return;

        String column = attributes.getProperty("keyColumn");
        String javaTypeStr = attributes.getProperty("keyJavaType");
        Class<?> parameterClass = config.getMappedStatement().getParameterClass();
        Class<?> javaType;
        if (Strings.isEmpty(javaTypeStr))
        {
            javaType = parameterClass.getDeclaredField(property).getType();
        }
        else
        {
            javaType = Resources.classForName(
                    state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(javaTypeStr));
        }
        config.setSelectKeyStatement(property, column, javaType);
    }
}
