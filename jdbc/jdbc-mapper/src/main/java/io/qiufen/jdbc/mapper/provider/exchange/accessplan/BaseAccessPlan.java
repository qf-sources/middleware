package io.qiufen.jdbc.mapper.provider.exchange.accessplan;

import io.qiufen.jdbc.mapper.provider.common.beans.ClassInfo;
import io.qiufen.jdbc.mapper.provider.common.beans.Property;

/**
 * Base implementation of the AccessPlan interface
 */
public abstract class BaseAccessPlan implements AccessPlan
{
    protected String[] propertyNames;
    protected ClassInfo info;

    BaseAccessPlan(Class<?> clazz, String[] propertyNames)
    {
        this.propertyNames = propertyNames;
        info = ClassInfo.getInstance(clazz);
    }

    protected Property[] getProperties(String[] propertyNames)
    {
        Property[] properties = new Property[propertyNames.length];
        for (int i = 0; i < propertyNames.length; i++)
        {
            properties[i] = info.getProperty(propertyNames[i]);
        }
        return properties;
    }
}
