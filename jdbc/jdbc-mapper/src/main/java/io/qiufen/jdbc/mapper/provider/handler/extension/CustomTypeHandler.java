package io.qiufen.jdbc.mapper.provider.handler.extension;

import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.extension.ParameterSetter;
import io.qiufen.jdbc.mapper.spi.handler.extension.ResultGetter;
import io.qiufen.jdbc.mapper.spi.handler.extension.TypeHandlerCallback;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Custom type handler for adding a TypeHandlerCallback
 * todo 研究一下
 */
public class CustomTypeHandler extends BaseTypeHandler implements TypeHandler
{
    private final TypeHandlerCallback callback;

    /**
     * Constructor to provide a TypeHandlerCallback instance
     *
     * @param callback - the TypeHandlerCallback instance
     */
    public CustomTypeHandler(TypeHandlerCallback callback)
    {
        this.callback = callback;
    }

    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ParameterSetter setter = new ParameterSetterImpl(ps, i);
        callback.setParameter(setter, parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        ResultGetter getter = new ResultGetterImpl(rs, columnName);
        return callback.getResult(getter);
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        ResultGetter getter = new ResultGetterImpl(rs, columnIndex);
        return callback.getResult(getter);
    }

    public Object valueOf(String s)
    {
        return callback.valueOf(s);
    }

}
