package io.qiufen.jdbc.mapper.provider.mapping.result;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import io.qiufen.jdbc.mapper.provider.exchange.DataExchange;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Basic implementation of ResultMap interface
 */
public class ResultMap
{
    protected SqlMapContext sqlMapContext;

    private CTString id;
    private Class<?> resultClass;
    private ResultMapping[] resultMappings;
    private DataExchange dataExchange;

    public ResultMap(SqlMapContext sqlMapContext)
    {
        this.sqlMapContext = sqlMapContext;
    }

    public SqlMapContext getSqlMapContext()
    {
        return sqlMapContext;
    }

    public CTString getId()
    {
        return id;
    }

    /**
     * Setter for the ID
     *
     * @param id - the new ID
     */
    public void setId(CTString id)
    {
        this.id = id;
    }

    public Class<?> getResultClass()
    {
        return resultClass;
    }

    /**
     * Setter for the result class (what the results will be mapped into)
     *
     * @param resultClass - the result class
     */
    public void setResultClass(Class<?> resultClass)
    {
        this.resultClass = resultClass;
    }

    public ResultMapping[] getResultMappings()
    {
        return resultMappings;
    }

    /**
     * Setter for a list of the individual ResultMapping objects
     */
    public void setResultMappings(ResultMapping[] resultMappings)
    {
        this.resultMappings = resultMappings;
        this.dataExchange = sqlMapContext.getDataExchangeFactory().getDataExchangeForClass(resultClass);
        this.dataExchange.initialize(this);
    }

    public ResultMap prepare() {return this;}

    public void setResultMappingList(List<ResultMapping> resultMappings)
    {
        ResultMapping[] array = new ResultMapping[resultMappings.size()];
        this.setResultMappings(resultMappings.toArray(array));
    }

    /**
     * Read a row from a resultset and map results to an array.
     *
     * @param statementScope scope of the request
     * @param rs             ResultSet to read from
     * @return row read as an array of column values.
     */
    public Object[] getResults(StatementScope statementScope, ResultSet rs) throws SQLException
    {
        ResultMapping[] mappings = getResultMappings();
        Object[] columnValues = new Object[mappings.length];
        for (int i = 0; i < mappings.length; i++)
        {
            columnValues[i] = mappings[i].getMappingValue(rs);
        }
        return columnValues;
    }

    public Object setResultObjectValues(StatementScope statementScope, ResultSet rs) throws SQLException
    {
        ResultMappingValue resultMappingValue = statementScope.getResultMappingValue();
        resultMappingValue.start(rs);
        Object obj = dataExchange.setData(statementScope, this, rs);
        resultMappingValue.end();
        return obj;
    }
}
