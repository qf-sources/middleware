package io.qiufen.jdbc.mapper.provider.config;

import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;

public interface SqlSource
{
    Sql getSql(MappedStatement statement);
}
