package io.qiufen.jdbc.mapper.provider.config;

import io.qiufen.common.filter.FilterRegistry;
import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;
import io.qiufen.jdbc.mapper.api.session.MSNotFoundException;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;
import io.qiufen.jdbc.mapper.provider.exchange.DataExchangeFactory;
import io.qiufen.jdbc.mapper.provider.handler.TypeHandlerRegistry;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultObjectFactory;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;
import io.qiufen.jdbc.mapper.spi.filter.StatementScopeFilter;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.util.HashMap;
import java.util.Map;

public class SqlMapContext
{
    private static final Probe PROBE = ProbeFactory.getProbe();

    private final Map<CTString, MappedStatement> mappedStatements;
    private final Map<CTString, ParameterMap> parameterMaps;
    private final Map<CTString, ResultMap> resultMaps;

    private final TypeHandlerRegistry typeHandlerRegistry;
    private final DataExchangeFactory dataExchangeFactory;
    private final DynamicElementParser dynamicElementParser;
    private final FilterRegistry filterRegistry = new FilterRegistry("MapperFilterRegistry");
    private boolean useColumnLabel = true;
    private ResultObjectFactory resultObjectFactory;
    private Integer defaultStatementTimeout;

    public SqlMapContext() throws SqlMapConfigException
    {
        this.mappedStatements = new HashMap<CTString, MappedStatement>();
        this.parameterMaps = new HashMap<CTString, ParameterMap>();
        this.resultMaps = new HashMap<CTString, ResultMap>();

        this.typeHandlerRegistry = new TypeHandlerRegistry();
        this.dataExchangeFactory = new DataExchangeFactory(typeHandlerRegistry);

        this.dynamicElementParser = new DynamicElementParser(this);
    }

    public Integer getDefaultStatementTimeout()
    {
        return defaultStatementTimeout;
    }

    public void setDefaultStatementTimeout(int defaultTimeout)
    {
        defaultStatementTimeout = defaultTimeout;
    }

    public ParameterMapConfig newParameterMapConfig(CTString id, Class<?> parameterClass)
    {
        return new ParameterMapConfig(this, id, parameterClass);
    }

    public ResultMapConfig newResultMapConfig(CTString id, Class<?> resultClass, CTString extended)
            throws SqlMapConfigException
    {
        return new ResultMapConfig(this, id, resultClass, extended);
    }

    public MappedStatementConfig newMappedStatementConfig(CTString id, MappedStatement statement, SqlSource processor,
            String parameterMapName, Class<?> parameterClass, CTString resultMapName, Class<?> resultClass,
            String resultSetType, Integer fetchSize, boolean allowRemapping, Integer timeout)
            throws SqlMapConfigException
    {
        return new MappedStatementConfig(this, id, statement, processor, parameterMapName, parameterClass,
                resultMapName, resultClass, resultSetType, fetchSize, allowRemapping, timeout, defaultStatementTimeout);
    }

    public void finalizeSqlMapConfig()
    {
    }

    /**
     * Getter for the DataExchangeFactory
     *
     * @return - the DataExchangeFactory
     */
    public DataExchangeFactory getDataExchangeFactory()
    {
        return dataExchangeFactory;
    }

    /**
     * Getter for the TypeHandlerFactory
     *
     * @return - the TypeHandlerFactory
     */
    public TypeHandlerRegistry getTypeHandlerRegistry()
    {
        return typeHandlerRegistry;
    }

    public boolean isUseColumnLabel()
    {
        return useColumnLabel;
    }

    public void setUseColumnLabel(boolean useColumnLabel)
    {
        this.useColumnLabel = useColumnLabel;
    }

    /**
     * Add a result map
     *
     * @param map - the result map to add
     */
    public void addResultMap(ResultMap map)
    {
        resultMaps.put(map.getId(), map);
    }

    /**
     * Get a result map by ID
     *
     * @param id - the ID
     * @return - the result map
     */
    public ResultMap getResultMap(CTString id) throws SqlMapConfigException
    {
        ResultMap map = resultMaps.get(id);
        if (map == null)
        {
            throw new SqlMapConfigException("There is no result map named " + id + " in this SqlMap.");
        }
        return map;
    }

    /**
     * Add a parameter map
     *
     * @param map - the map to add
     */
    public void addParameterMap(ParameterMap map)
    {
        parameterMaps.put(map.getId(), map);
    }

    /**
     * Get a parameter map by ID
     *
     * @param id - the ID
     * @return - the parameter map
     */
    public ParameterMap getParameterMap(CTString id) throws SqlMapConfigException
    {
        ParameterMap map = parameterMaps.get(id);
        if (map == null)
        {
            throw new SqlMapConfigException("There is no parameter map named " + id + " in this SqlMap.");
        }
        return map;
    }

    public ResultObjectFactory getResultObjectFactory()
    {
        return resultObjectFactory;
    }

    public void setResultObjectFactory(ResultObjectFactory resultObjectFactory)
    {
        this.resultObjectFactory = resultObjectFactory;
    }

    /**
     * Add a mapped statement
     *
     * @param ms - the mapped statement to add
     */
    public void addMappedStatement(MappedStatement ms) throws SqlMapConfigException
    {
        if (mappedStatements.containsKey(ms.getId()))
        {
            throw new SqlMapConfigException("There is already a statement named " + ms.getId() + " in this SqlMap.");
        }
        mappedStatements.put(ms.getId(), ms);
    }

    public MappedStatement getMappedStatement(CTString id) throws MSNotFoundException
    {
        MappedStatement mappedStatement = this.mappedStatements.get(id);
        if (mappedStatement == null)
        {
            throw new MSNotFoundException("There is no mapped statement named " + id + " in this SqlMap.");
        }
        return mappedStatement;
    }

    public CharSequences.VariableParser<Object> getDynamicElementParser() {return dynamicElementParser;}

    public void addFilter(StatementScopeFilter filter)
    {
        this.filterRegistry.addFilter(filter);
    }

    public FilterRegistry getFilterRegistry() {return this.filterRegistry;}

    TypeHandler resolveTypeHandler(Class<?> clazz, String propertyName, Class<?> javaType, String jdbcType,
            boolean useSetterToResolve)
    {
        TypeHandler handler;
        if (clazz == null)
        {
            // Unknown
            handler = typeHandlerRegistry.getUnkownTypeHandler();
        }
        else if (java.util.Map.class.isAssignableFrom(clazz))
        {
            // Map
            if (javaType == null)
            {
                handler = typeHandlerRegistry.getUnkownTypeHandler(); //BUG 1012591 - typeHandlerFactory.getTypeHandler(java.lang.Object.class, jdbcType);
            }
            else
            {
                handler = typeHandlerRegistry.getTypeHandler(javaType, jdbcType);
            }
        }
        else if (typeHandlerRegistry.getTypeHandler(clazz, jdbcType) != null)
        {
            // Primitive
            handler = typeHandlerRegistry.getTypeHandler(clazz, jdbcType);
        }
        else
        {
            // JavaBean
            if (javaType == null)
            {
                Class<?> type;
                if (useSetterToResolve)
                {
                    type = PROBE.getPropertyTypeForSetter(clazz, propertyName);
                }
                else
                {
                    type = PROBE.getPropertyTypeForGetter(clazz, propertyName);
                }
                handler = typeHandlerRegistry.getTypeHandler(type, jdbcType);
            }
            else
            {
                handler = typeHandlerRegistry.getTypeHandler(javaType, jdbcType);
            }
        }
        return handler;
    }

    TypeHandler resolveTypeHandler(Class<?> clazz, String propertyName, Class<?> javaType, String jdbcType)
    {
        return resolveTypeHandler(clazz, propertyName, javaType, jdbcType, false);
    }

    private static class DynamicElementParser implements CharSequences.VariableParser<Object>
    {
        private static final char[] EMPTY = new char[0];

        private static final Probe PROBE = ProbeFactory.getProbe();
        private final SqlMapContext sqlMapContext;

        public DynamicElementParser(SqlMapContext sqlMapContext)
        {
            this.sqlMapContext = sqlMapContext;
        }

        @Override
        public int parse(Object parameterObject, CharSequences.Var var, CharSequences.Resolver resolver)
        {
            if (parameterObject == null)
            {
                return resolver.resolve(EMPTY, StringBuilder.COPIER_CHAR_ARRAY);
            }

            Object obj;
            if (sqlMapContext.getTypeHandlerRegistry().hasTypeHandler(parameterObject.getClass()))
            {
                obj = parameterObject;
            }
            else
            {
                obj = PROBE.getObject(parameterObject, var);
            }
            if (obj == null)
            {
                return resolver.resolve(EMPTY, StringBuilder.COPIER_CHAR_ARRAY);
            }
            String value = obj.toString();
            return resolver.resolve(value, StringBuilder.COPIER_STRING);
        }
    }
}
