package io.qiufen.jdbc.mapper.provider.session;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.session.context.SessionContext;
import io.qiufen.jdbc.mapper.provider.statement.InsertStatement;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;
import io.qiufen.jdbc.mapper.provider.statement.SelectStatement;
import io.qiufen.jdbc.mapper.spi.handler.RowHandler;

import java.util.Collection;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/25 10:35
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
@SuppressWarnings("rawtypes")
class DefaultSession extends AbstractSession
{
    private final MappedStatementService mappedStatementService;

    DefaultSession(SessionContext sessionContext, MappedStatementService mappedStatementService)
    {
        super(sessionContext);
        this.mappedStatementService = mappedStatementService;
    }

    @Override
    public int insert(CTString id, Object parameterObject) throws SessionException
    {
        checkSessionState();
        try
        {
            return mappedStatementService.insert((InsertStatement) mappedStatementService.getMappedStatement(id),
                    parameterObject, getSessionContext());
        }
        catch (SessionException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SessionException(e);
        }
    }

    @Override
    public int[] insert(CTString id, Collection collection) throws SessionException
    {
        checkSessionState();
        try
        {
            return mappedStatementService.batchInsert((InsertStatement) mappedStatementService.getMappedStatement(id),
                    collection, getSessionContext());
        }
        catch (SessionException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SessionException(e);
        }
    }

    @Override
    public int update(CTString id, Object parameterObject) throws SessionException
    {
        checkSessionState();
        try
        {
            return mappedStatementService.update(mappedStatementService.getMappedStatement(id), parameterObject,
                    getSessionContext());
        }
        catch (SessionException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SessionException(e);
        }
    }

    @Override
    public int[] update(CTString id, Collection collection) throws SessionException
    {
        checkSessionState();
        try
        {
            return mappedStatementService.batchUpdate(mappedStatementService.getMappedStatement(id), collection,
                    getSessionContext());
        }
        catch (SessionException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SessionException(e);
        }
    }

    @Override
    public int delete(CTString id, Object parameterObject) throws SessionException
    {
        checkSessionState();
        try
        {
            return mappedStatementService.update(mappedStatementService.getMappedStatement(id), parameterObject,
                    getSessionContext());
        }
        catch (SessionException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SessionException(e);
        }
    }

    @Override
    public int[] delete(CTString id, Collection collection) throws SessionException
    {
        checkSessionState();
        try
        {
            return mappedStatementService.batchUpdate(mappedStatementService.getMappedStatement(id), collection,
                    getSessionContext());
        }
        catch (SessionException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SessionException(e);
        }
    }

    @Override
    public void select(CTString id, Object parameterObject, RowHandler rowHandler) throws SessionException
    {
        checkSessionState();
        try
        {
            mappedStatementService.select((SelectStatement) mappedStatementService.getMappedStatement(id),
                    parameterObject, getSessionContext(), rowHandler);
        }
        catch (SessionException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SessionException(e);
        }
    }

    @Override
    public Object execute(CTString id, Object parameterObject) throws SessionException
    {
        checkSessionState();
        try
        {
            return mappedStatementService.execute(mappedStatementService.getMappedStatement(id), parameterObject,
                    getSessionContext());
        }
        catch (SessionException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SessionException(e);
        }
    }

    @Override
    public Object execute(MappedStatement mappedStatement, Object parameterObject) throws SessionException
    {
        try
        {
            return mappedStatementService.execute(mappedStatement, parameterObject, getSessionContext());
        }
        catch (SessionException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new SessionException(e);
        }
    }
}