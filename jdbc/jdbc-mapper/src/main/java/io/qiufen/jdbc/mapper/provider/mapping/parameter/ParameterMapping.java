package io.qiufen.jdbc.mapper.provider.mapping.parameter;

import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.common.resources.Resources;
import io.qiufen.jdbc.mapper.provider.handler.JdbcTypeRegistry;
import io.qiufen.jdbc.mapper.provider.handler.extension.CustomTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

public class ParameterMapping
{
    private String propertyName;
    private TypeHandler typeHandler;
    private int jdbcType;
    private String jdbcTypeName;
    private String nullValue;
    private Class<?> javaType;

    public String getNullValue()
    {
        return nullValue;
    }

    public void setNullValue(String nullValue)
    {
        this.nullValue = nullValue;
    }

    public String getPropertyName()
    {
        return propertyName;
    }

    public void setPropertyName(String propertyName)
    {
        this.propertyName = propertyName;
    }

    public TypeHandler getTypeHandler()
    {
        return typeHandler;
    }

    public void setTypeHandler(TypeHandler typeHandler)
    {
        this.typeHandler = typeHandler;
    }

    public Class<?> getJavaType()
    {
        return javaType;
    }

    public void setJavaType(Class<?> javaType)
    {
        this.javaType = javaType;
    }

    public String getJavaTypeName()
    {
        return javaType == null ? null : javaType.getName();
    }

    public void setJavaTypeName(String javaTypeName)
    {
        try
        {
            if (javaTypeName == null)
            {
                this.javaType = null;
            }
            else
            {
                this.javaType = Resources.classForName(javaTypeName);
            }
        }
        catch (ClassNotFoundException e)
        {
            throw new SessionException("Error setting javaType property of ParameterMap.  Cause: " + e, e);
        }
    }

    public int getJdbcType()
    {
        return jdbcType;
    }

    public String getJdbcTypeName()
    {
        return jdbcTypeName;
    }

    public void setJdbcTypeName(String jdbcTypeName)
    {
        this.jdbcTypeName = jdbcTypeName;
        this.jdbcType = JdbcTypeRegistry.getType(jdbcTypeName);
    }

    public void setMappingValue(PreparedStatement ps, int parameterIndex, Object parameter) throws SQLException
    {
        Object value = parameter;
        // Apply Null Value
        String nullValueString = getNullValue();
        if (nullValueString != null)
        {
            TypeHandler handler = getTypeHandler();
            if (handler.equals(value, nullValueString))
            {
                value = null;
            }
        }

        // Set Parameter
        TypeHandler typeHandler = getTypeHandler();
        if (value == null)
        {
            if (typeHandler instanceof CustomTypeHandler)
            {
                typeHandler.setParameter(ps, parameterIndex, null, getJdbcTypeName());
            }
            else
            {
                int jdbcType = getJdbcType();
                ps.setNull(parameterIndex, jdbcType == JdbcTypeRegistry.UNKNOWN_TYPE ? Types.OTHER : jdbcType);
            }
        }
        else
        {
            typeHandler.setParameter(ps, parameterIndex, value, getJdbcTypeName());
        }
    }
}
