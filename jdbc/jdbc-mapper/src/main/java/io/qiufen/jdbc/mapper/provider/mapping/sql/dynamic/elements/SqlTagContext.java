package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;

import java.util.*;

public class SqlTagContext
{
    private final List<ParameterMapping> parameterMappings;
    private final Map<Object, Object> attributes;
    private final LinkedList<RemoveFirstPrependMarker> removeFirstPrependStack;
    private final LinkedList<IterateContext> iterateContextStack;
    private final SqlMapContext sqlMapContext;

    public SqlTagContext(SqlMapContext sqlMapContext)
    {
        parameterMappings = new ArrayList<ParameterMapping>();
        attributes = new HashMap<Object, Object>();
        removeFirstPrependStack = new LinkedList<RemoveFirstPrependMarker>();
        iterateContextStack = new LinkedList<IterateContext>();
        this.sqlMapContext = sqlMapContext;
    }

    public void setAttribute(Object key, Object value)
    {
        attributes.put(key, value);
    }

    public Object getAttribute(Object key)
    {
        return attributes.get(key);
    }

    public void addParameterMapping(ParameterMapping mapping)
    {
        parameterMappings.add(mapping);
    }

    public List<ParameterMapping> getParameterMappings()
    {
        return parameterMappings;
    }

    public boolean isEmptyRemoveFirtPrepend()
    {
        return removeFirstPrependStack.size() <= 0;
    }

    /**
     * examine the value of the top RemoveFirstPrependMarker object on the stack.
     *
     * @return was the first prepend removed
     */
    public boolean peekRemoveFirstPrependMarker(SqlTag sqlTag)
    {
        RemoveFirstPrependMarker removeFirstPrepend = removeFirstPrependStack.get(1);
        return removeFirstPrepend.isRemoveFirstPrepend();
    }

    /**
     * pop the first RemoveFirstPrependMarker once the recursion is on it's way out
     * of the recursion loop and return it's internal value.
     *
     * @param tag
     */
    public void popRemoveFirstPrependMarker(SqlTag tag)
    {

        RemoveFirstPrependMarker removeFirstPrepend = removeFirstPrependStack.getFirst();

        if (tag == removeFirstPrepend.getSqlTag())
        {
            removeFirstPrependStack.removeFirst();
        }
    }

    /**
     * push a new RemoveFirstPrependMarker object with the specified internal state
     *
     * @param tag
     */
    public void pushRemoveFirstPrependMarker(SqlTag tag)
    {

        if (tag.getHandler() instanceof DynamicTagHandler)
        {
            // this was added to retain default behavior
            if (tag.isPrependAvailable())
            {
                removeFirstPrependStack.addFirst(new RemoveFirstPrependMarker(tag, true));
            }
            else
            {
                removeFirstPrependStack.addFirst(new RemoveFirstPrependMarker(tag, false));
            }
        }
        else if ("true".equals(tag.getRemoveFirstPrepend()) || "iterate".equals(tag.getRemoveFirstPrepend()))
        {
            // you must be specific about the removal otherwise it
            // will function as ibatis has always functioned and add
            // the prepend
            removeFirstPrependStack.addFirst(new RemoveFirstPrependMarker(tag, true));
        }
        else if (!tag.isPrependAvailable() && !"true".equals(tag.getRemoveFirstPrepend()) && !"iterate".equals(
                tag.getRemoveFirstPrepend()) && tag.getParent() != null)
        {
            // if no prepend or removeFirstPrepend is specified
            // we need to look to the parent tag for default values
            if ("true".equals(tag.getParent().getRemoveFirstPrepend()) || "iterate".equals(
                    tag.getParent().getRemoveFirstPrepend()))
            {
                removeFirstPrependStack.addFirst(new RemoveFirstPrependMarker(tag, true));
            }
        }
        else
        {
            removeFirstPrependStack.addFirst(new RemoveFirstPrependMarker(tag, false));
        }

    }

    /**
     * set a new internal state for top RemoveFirstPrependMarker object
     */
    public void disableRemoveFirstPrependMarker()
    {
        removeFirstPrependStack.get(1).setRemoveFirstPrepend(false);
    }

    public void reEnableRemoveFirstPrependMarker()
    {
        removeFirstPrependStack.get(0).setRemoveFirstPrepend(true);
    }

    /**
     * iterate context is stored here for nested dynamic tags in
     * the body of the iterate tag
     *
     * @param iterateContext
     */
    public void pushIterateContext(IterateContext iterateContext)
    {
        iterateContextStack.addFirst(iterateContext);
    }

    /**
     * iterate context is removed here from the stack when iterate tag is finished being
     * processed
     *
     * @return the top element of the context stack
     */
    public IterateContext popIterateContext()
    {
        IterateContext retVal = null;
        if (!iterateContextStack.isEmpty())
        {
            retVal = iterateContextStack.removeFirst();
        }
        return retVal;
    }

    /**
     * iterate context is removed here from the stack when iterate tag is finished being
     * processed
     *
     * @return the top element on the context stack
     */
    public IterateContext peekIterateContext()
    {
        IterateContext retVal = null;
        if (!iterateContextStack.isEmpty())
        {
            retVal = iterateContextStack.getFirst();
        }
        return retVal;
    }

    public SqlMapContext getSqlMapContext()
    {
        return sqlMapContext;
    }
}

/**
 * This inner class i used strictly to house whether the
 * removeFirstPrepend has been used in a particular nested
 * situation.
 *
 * @author Brandon Goodin
 */
class RemoveFirstPrependMarker
{
    private final SqlTag tag;
    private boolean removeFirstPrepend;

    /**
     *
     */
    public RemoveFirstPrependMarker(SqlTag tag, boolean removeFirstPrepend)
    {
        this.removeFirstPrepend = removeFirstPrepend;
        this.tag = tag;
    }

    /**
     * @return Returns the removeFirstPrepend.
     */
    public boolean isRemoveFirstPrepend()
    {
        return removeFirstPrepend;
    }

    /**
     * @param removeFirstPrepend The removeFirstPrepend to set.
     */
    public void setRemoveFirstPrepend(boolean removeFirstPrepend)
    {
        this.removeFirstPrepend = removeFirstPrepend;
    }

    /**
     * @return Returns the sqlTag.
     */
    public SqlTag getSqlTag()
    {
        return tag;
    }

}