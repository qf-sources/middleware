package io.qiufen.jdbc.mapper.provider.mapping.parameter;

public interface ParameterMappingValue
{
    ParameterMappingValue EMPTY = new ParameterMappingValue()
    {
        @Override
        public void onValue(ParameterMapping mapping, Object value) {}

        @Override
        public void start() {}

        @Override
        public void end() {}
    };

    void onValue(ParameterMapping mapping, Object value);

    void start();

    void end();
}
