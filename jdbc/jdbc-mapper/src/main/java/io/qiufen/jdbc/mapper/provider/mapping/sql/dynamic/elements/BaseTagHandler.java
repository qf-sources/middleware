package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;
import io.qiufen.jdbc.mapper.provider.mapping.sql.simple.SimpleDynamicSql;

public abstract class BaseTagHandler implements SqlTagHandler
{
    public int doStartFragment(SqlTagContext ctx, SqlTag tag, Object parameterObject)
    {
        ctx.pushRemoveFirstPrependMarker(tag);
        return INCLUDE_BODY;
    }

    public int doEndFragment(SqlTagContext ctx, SqlTag tag, Object parameterObject, StringBuilder bodyContent)
    {
        if (tag.isCloseAvailable() && !(tag.getHandler() instanceof IterateTagHandler))
        {
            if (CharSequences.isNotBlank(bodyContent))
            {
                appendCloseAttr(ctx, tag, parameterObject, bodyContent);
            }
        }
        return INCLUDE_BODY;
    }

    public void doPrepend(SqlTagContext ctx, SqlTag tag, Object parameterObject, StringBuilder bodyContent)
    {
        if (tag.isOpenAvailable() && !(tag.getHandler() instanceof IterateTagHandler))
        {
            if (CharSequences.isNotBlank(bodyContent))
            {
                appendOpenAttr(ctx, tag, parameterObject, bodyContent);
            }
        }

        if (tag.isPrependAvailable())
        {
            if (bodyContent.length() > 0)
            {
                if (tag.getParent() != null && ctx.peekRemoveFirstPrependMarker(tag))
                {
                    ctx.disableRemoveFirstPrependMarker();
                }
                else
                {
                    bodyContent.insert(0, " ", StringBuilder.COPIER_STRING);
                    String prependAttr = tag.getPrependAttr();
                    if (tag.isSimpleDynamicPrependAttr())
                    {
                        bodyContent.insert(0,
                                SimpleDynamicSql.processDynamicElements(ctx.getSqlMapContext(), prependAttr,
                                        parameterObject), StringBuilder.COPIER_STRING_BUILDER);
                    }
                    else
                    {
                        bodyContent.insert(0, prependAttr, StringBuilder.COPIER_STRING);
                    }
                }
            }
        }
    }

    protected void appendOpenAttr(SqlTagContext ctx, SqlTag tag, Object parameterObject, StringBuilder bodyContent)
    {
        String openAttr = tag.getOpenAttr();
        if (tag.isSimpleDynamicOpenAttr())
        {
            bodyContent.insert(0,
                    SimpleDynamicSql.processDynamicElements(ctx.getSqlMapContext(), openAttr, parameterObject),
                    StringBuilder.COPIER_STRING_BUILDER);
        }
        else
        {
            bodyContent.insert(0, openAttr, StringBuilder.COPIER_STRING);
        }
    }

    protected void appendCloseAttr(SqlTagContext ctx, SqlTag tag, Object parameterObject, StringBuilder bodyContent)
    {
        String closeAttr = tag.getCloseAttr();
        if (tag.isSimpleDynamicCloseAttr())
        {
            bodyContent.append(
                    SimpleDynamicSql.processDynamicElements(ctx.getSqlMapContext(), closeAttr, parameterObject));
        }
        else
        {
            bodyContent.append(closeAttr);
        }
        bodyContent.append(" ");
    }
}
