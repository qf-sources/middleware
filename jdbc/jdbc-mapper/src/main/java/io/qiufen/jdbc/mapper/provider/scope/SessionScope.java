package io.qiufen.jdbc.mapper.provider.scope;

import java.sql.Connection;
import java.sql.SQLException;

public interface SessionScope
{
    Connection getConnection();

    void close() throws SQLException;
}
