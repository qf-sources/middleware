package io.qiufen.jdbc.mapper.provider.statement;

import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

public class DeleteStatement extends MappedStatement
{
    public StatementType getStatementType()
    {
        return StatementType.DELETE;
    }

    @Override
    public StatementScope initRequest()
    {
        StatementScope statementScope = new StatementScope(this);
        statementScope.setParameterMap(getParameterMap());
        return statementScope;
    }
}
