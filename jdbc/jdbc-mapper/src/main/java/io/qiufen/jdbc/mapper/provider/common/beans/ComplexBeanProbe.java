package io.qiufen.jdbc.mapper.provider.common.beans;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultObjectFactoryUtil;

import java.util.Enumeration;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * StaticBeanProbe provides methods that allow simple, reflective access to
 * JavaBeans style properties.  Methods are provided for all simple types as
 * well as object types.
 * <p/>
 * Examples:
 * <p/>
 * StaticBeanProbe.setObject(object, propertyName, value);
 * <p/>
 * Object value = StaticBeanProbe.getObject(object, propertyName);
 */
public class ComplexBeanProbe extends BaseProbe
{
    protected ComplexBeanProbe() {}

    /**
     * Gets an Object property from a bean
     *
     * @param object The bean
     * @param name   The property name
     * @return The property value (as an Object)
     */
    public Object getObject(Object object, CharSequence name)
    {
        Enumeration<CharSequences.Var> result = CharSequences.split(name, '.');
        Object value = object;
        while (result.hasMoreElements())
        {
            value = getProperty(value, result.nextElement());
            if (value == null) break;
        }
        return value;
    }

    /**
     * Sets the value of a bean property to an Object
     *
     * @param object The bean to change
     * @param name   The name of the property to set
     * @param value  The new value to set
     */
    public void setObject(Object object, String name, Object value)
    {
        Enumeration<CharSequences.Var> result = CharSequences.split(name, '.');
        if (result.hasMoreElements())
        {
            String property = result.nextElement().toString();
            Object child = object;
            while (result.hasMoreElements())
            {
                Class<?> type = getPropertyTypeForSetter(child, property);
                Object parent = child;
                child = getProperty(parent, property);
                if (child == null)
                {
                    if (value == null) return; // don't instantiate child path if value is null
                    try
                    {
                        child = ResultObjectFactoryUtil.createObjectThroughFactory(type);
                        setObject(parent, property, child);
                    }
                    catch (Exception e)
                    {
                        throw new ProbeException(
                                "Cannot set value of property '" + name + "' because '" + property + "' is null and cannot be instantiated on instance of " + type.getName() + ". Cause:" + e,
                                e);
                    }
                }
                property = result.nextElement().toString();
            }
            setProperty(child, property, value);
        }
    }

    /**
     * Returns the class that the setter expects to receive as a parameter when
     * setting a property value.
     *
     * @param object The bean to check
     * @param name   The name of the property
     * @return The type of the property
     */
    public Class<?> getPropertyTypeForSetter(Object object, String name)
    {
        Class<?> type = object.getClass();
        if (object instanceof Class)
        {
            type = getClassPropertyTypeForSetter((Class<?>) object, name);
        }
        else if (object instanceof Map)
        {
            Map map = (Map) object;
            Object value = map.get(name);
            if (value == null)
            {
                type = Object.class;
            }
            else
            {
                type = value.getClass();
            }
        }
        else
        {
            if (name.indexOf('.') > -1)
            {
                StringTokenizer parser = new StringTokenizer(name, ".");
                while (parser.hasMoreTokens())
                {
                    name = parser.nextToken();
                    type = ClassInfo.getInstance(type).getSetterType(name);
                }
            }
            else
            {
                type = ClassInfo.getInstance(type).getSetterType(name);
            }
        }

        return type;
    }

    /**
     * Returns the class that the getter will return when reading a property value.
     *
     * @param object The bean to check
     * @param name   The name of the property
     * @return The type of the property
     */
    public Class<?> getPropertyTypeForGetter(Object object, String name)
    {
        Class<?> type = object.getClass();

        if (object instanceof Class)
        {
            type = getClassPropertyTypeForGetter((Class<?>) object, name);
        }
        else if (object instanceof Map)
        {
            Map map = (Map) object;
            Object value = map.get(name);
            if (value == null)
            {
                type = Object.class;
            }
            else
            {
                type = value.getClass();
            }
        }
        else
        {
            if (name.indexOf('.') > -1)
            {
                StringTokenizer parser = new StringTokenizer(name, ".");
                while (parser.hasMoreTokens())
                {
                    name = parser.nextToken();
                    type = ClassInfo.getInstance(type).getGetterType(name);
                }
            }
            else
            {
                type = ClassInfo.getInstance(type).getGetterType(name);
            }
        }

        return type;
    }

    /**
     * Returns the class that the getter will return when reading a property value.
     *
     * @param type The class to check
     * @param name The name of the property
     * @return The type of the property
     */
    private Class<?> getClassPropertyTypeForGetter(Class<?> type, String name)
    {
        if (name.indexOf('.') > -1)
        {
            StringTokenizer parser = new StringTokenizer(name, ".");
            while (parser.hasMoreTokens())
            {
                name = parser.nextToken();
                type = ClassInfo.getInstance(type).getGetterType(name);
            }
        }
        else
        {
            type = ClassInfo.getInstance(type).getGetterType(name);
        }
        return type;
    }

    /**
     * Returns the class that the setter expects to receive as a parameter when
     * setting a property value.
     *
     * @param type The class to check
     * @param name The name of the property
     * @return The type of the property
     */
    private Class<?> getClassPropertyTypeForSetter(Class<?> type, String name)
    {
        if (name.indexOf('.') > -1)
        {
            StringTokenizer parser = new StringTokenizer(name, ".");
            while (parser.hasMoreTokens())
            {
                name = parser.nextToken();
                type = ClassInfo.getInstance(type).getSetterType(name);
            }
        }
        else
        {
            type = ClassInfo.getInstance(type).getSetterType(name);
        }
        return type;
    }


    /**
     * Checks to see if a bean has a writable property be a given name
     *
     * @param object       The bean to check
     * @param propertyName The property to check for
     * @return True if the property exists and is writable
     */
    public boolean hasWritableProperty(Object object, String propertyName)
    {
        boolean hasProperty = false;
        if (object instanceof Map)
        {
            hasProperty = true;//((Map) object).containsKey(propertyName);
        }
        else
        {
            if (propertyName.indexOf('.') > -1)
            {
                StringTokenizer parser = new StringTokenizer(propertyName, ".");
                Class<?> type = object.getClass();
                while (parser.hasMoreTokens())
                {
                    propertyName = parser.nextToken();
                    type = ClassInfo.getInstance(type).getGetterType(propertyName);
                    hasProperty = ClassInfo.getInstance(type).hasWritableProperty(propertyName);
                }
            }
            else
            {
                hasProperty = ClassInfo.getInstance(object.getClass()).hasWritableProperty(propertyName);
            }
        }
        return hasProperty;
    }

    /**
     * Checks to see if a bean has a readable property be a given name
     *
     * @param object       The bean to check
     * @param propertyName The property to check for
     * @return True if the property exists and is readable
     */
    public boolean hasReadableProperty(Object object, String propertyName)
    {
        boolean hasProperty = false;
        if (object instanceof Map)
        {
            hasProperty = true;//((Map) object).containsKey(propertyName);
        }
        else
        {
            if (propertyName.indexOf('.') > -1)
            {
                StringTokenizer parser = new StringTokenizer(propertyName, ".");
                Class<?> type = object.getClass();
                while (parser.hasMoreTokens())
                {
                    propertyName = parser.nextToken();
                    type = ClassInfo.getInstance(type).getGetterType(propertyName);
                    hasProperty = ClassInfo.getInstance(type).hasReadableProperty(propertyName);
                }
            }
            else
            {
                hasProperty = ClassInfo.getInstance(object.getClass()).hasReadableProperty(propertyName);
            }
        }
        return hasProperty;
    }

    protected Object getProperty(Object object, CharSequence name)
    {
        try
        {
            Object value;
            int openIndex = CharSequences.indexOf(name, '[', 0);
            if (openIndex > -1)
            {
                String name0 = name.subSequence(0, openIndex).toString();
                int start = openIndex + 1;
                int closeIndex = CharSequences.indexOf(name, ']', start);
                int i = Integer.parseInt(name.subSequence(start, closeIndex).toString());
                value = getIndexedProperty(object, name0, i);
            }
            else
            {
                String name0 = name.toString();
                if (object instanceof Map)
                {
                    value = ((Map) object).get(name0);
                }
                else
                {
                    ClassInfo classCache = ClassInfo.getInstance(object.getClass());
                    value = classCache.getProperty(name0).get(object);
                }
            }
            return value;
        }
        catch (ProbeException e)
        {
            throw e;
        }
        catch (Throwable t)
        {
            String msg;
            if (object == null)
            {
                msg = "Could not get property '" + name + "' from null reference.  Cause: ";
            }
            else
            {
                msg = "Could not get property '" + name + "' from " + object.getClass().getName() + ".  Cause: ";
            }
            throw new ProbeException(msg + t, t);
        }
    }

    protected void setProperty(Object object, String name, Object value)
    {
        try
        {
            int openIndex = name.indexOf('[');
            if (openIndex > -1)
            {
                String name0 = name.substring(0, openIndex);
                int start = openIndex + 1;
                int i = Integer.parseInt(name.substring(start, name.indexOf(']', start)));
                setIndexedProperty(object, name0, i, value);
            }
            else
            {
                if (object instanceof Map)
                {
                    ((Map) object).put(name, value);
                }
                else
                {
                    ClassInfo classInfo = ClassInfo.getInstance(object.getClass());
                    classInfo.getProperty(name).set(object, value);
                }
            }
        }
        catch (ProbeException e)
        {
            throw e;
        }
        catch (Throwable t)
        {
            throw new ProbeException(
                    "Could not set property '" + name + "' to value '" + value + "' for " + object.getClass()
                            .getName() + ".  Cause: " + t, t);
        }
    }
}
