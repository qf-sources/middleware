package io.qiufen.jdbc.mapper.provider.session;

import io.qiufen.common.filter.FilterRegistry;
import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.Strings;
import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.api.transaction.Transaction;
import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;
import io.qiufen.jdbc.mapper.provider.config.GeneratedKeyConfig;
import io.qiufen.jdbc.mapper.provider.config.xml.SqlMapFacade;
import io.qiufen.jdbc.mapper.provider.filter.LoggingFilter;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.scope.SessionScope;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;
import io.qiufen.jdbc.mapper.provider.session.context.SessionContext;
import io.qiufen.jdbc.mapper.provider.statement.InsertStatement;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;
import io.qiufen.jdbc.mapper.provider.statement.SelectStatement;
import io.qiufen.jdbc.mapper.spi.filter.StatementScopeFilterContext;
import io.qiufen.jdbc.mapper.spi.handler.RowHandler;
import org.slf4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

@SuppressWarnings({"unchecked", "rawtypes"})
class MappedStatementService
{
    private final SqlMapFacade sqlMapFacade;
    private final DBExecutorService dbExecutorService;
    private final FilterRegistry filterRegistry;

    MappedStatementService(SqlMapFacade sqlMapFacade, DBExecutorService dbExecutorService)
    {
        this.sqlMapFacade = sqlMapFacade;
        this.dbExecutorService = dbExecutorService;
        this.filterRegistry = sqlMapFacade.getFilterRegistry();
        this.filterRegistry.addFilter(new LoggingFilter());
    }

    int update(MappedStatement mappedStatement, Object parameterObject, SessionContext sessionContext) throws Exception
    {
        checkParameter(mappedStatement, parameterObject);
        StatementScope statementScope = mappedStatement.initRequest();
        try
        {
            SessionScope sessionScope = sessionContext.createSessionScope();
            try
            {
                Sql sql = mappedStatement.getSql();
                sql.applyParameter(statementScope, parameterObject);
                filterRegistry.submit(new StatementScopeFilterContext(statementScope, parameterObject));

                String jdbcSql = statementScope.getSql().toString();
                return dbExecutorService.update(statementScope, sessionScope, jdbcSql, parameterObject);
            }
            finally
            {
                sessionScope.close();
            }
        }
        finally
        {
            cleanQuietly(statementScope);
        }
    }

    int[] batchUpdate(MappedStatement mappedStatement, Collection collection, SessionContext sessionContext)
            throws Exception
    {
        if (!(sessionContext instanceof Transaction))
        {
            throw new SessionException("Must in transaction environment!");
        }
        SessionScope sessionScope = sessionContext.createSessionScope();
        try
        {
            CommonStatementBatch batch = dbExecutorService.createBatch(sessionScope);
            try
            {
                for (Object parameterObject : collection)
                {
                    checkParameter(mappedStatement, parameterObject);
                    StatementScope statementScope = mappedStatement.initRequest();
                    Sql sql = mappedStatement.getSql();
                    sql.applyParameter(statementScope, parameterObject);
                    filterRegistry.submit(new StatementScopeFilterContext(statementScope, parameterObject));

                    String jdbcSql = statementScope.getSql().toString();
                    batch.addStatement(statementScope, jdbcSql, parameterObject);
                }
                return batch.execute();
            }
            finally
            {
                batch.close();
            }
        }
        finally
        {
            sessionScope.close();
        }
    }

    int insert(InsertStatement mappedStatement, Object parameterObject, SessionContext sessionContext) throws Exception
    {
        GeneratedKeyConfig keyConfig = mappedStatement.getGeneratedKeyConfig();
        if (keyConfig == null)
        {
            return update(mappedStatement, parameterObject, sessionContext);
        }

        checkParameter(mappedStatement, parameterObject);
        StatementScope statementScope = mappedStatement.initRequest();
        try
        {
            SessionScope sessionScope = sessionContext.createSessionScope();
            try
            {
                Sql sql = mappedStatement.getSql();
                sql.applyParameter(statementScope, parameterObject);
                filterRegistry.submit(new StatementScopeFilterContext(statementScope, parameterObject));

                String jdbcSql = statementScope.getSql().toString();
                return dbExecutorService.updateWithKeys(statementScope, sessionScope, jdbcSql, parameterObject,
                        new DefaultKeyHandler(keyConfig, parameterObject));
            }
            finally
            {
                sessionScope.close();
            }
        }
        finally
        {
            cleanQuietly(statementScope);
        }
    }

    int[] batchInsert(InsertStatement mappedStatement, Collection collection, SessionContext sessionContext)
            throws Exception
    {
        GeneratedKeyConfig keyConfig = mappedStatement.getGeneratedKeyConfig();
        if (keyConfig == null)
        {
            return batchUpdate(mappedStatement, collection, sessionContext);
        }
        if (!(sessionContext instanceof Transaction))
        {
            throw new SessionException("Must in transaction environment!");
        }
        SessionScope sessionScope = sessionContext.createSessionScope();
        try
        {
            KeysStatementBatch batch = dbExecutorService.createBatchWithKeys(sessionScope, keyConfig.getColumn());
            try
            {
                for (Object parameterObject : collection)
                {
                    checkParameter(mappedStatement, parameterObject);
                    StatementScope statementScope = mappedStatement.initRequest();
                    Sql sql = mappedStatement.getSql();
                    sql.applyParameter(statementScope, parameterObject);
                    filterRegistry.submit(new StatementScopeFilterContext(statementScope, parameterObject));

                    String jdbcSql = statementScope.getSql().toString();
                    batch.addStatement(statementScope, jdbcSql, parameterObject,
                            new DefaultKeyHandler(keyConfig, parameterObject));
                }
                return batch.execute();
            }
            finally
            {
                batch.close();
            }
        }
        finally
        {
            sessionScope.close();
        }
    }

    void select(SelectStatement mappedStatement, Object parameterObject, SessionContext sessionContext,
            RowHandler rowHandler) throws Exception
    {
        checkParameter(mappedStatement, parameterObject);
        StatementScope statementScope = mappedStatement.initRequest();
        try
        {
            SessionScope sessionScope = sessionContext.createSessionScope();
            try
            {
                Sql sql = mappedStatement.getSql();
                sql.applyParameter(statementScope, parameterObject);
                filterRegistry.submit(new StatementScopeFilterContext(statementScope, parameterObject));

                String jdbcSql = statementScope.getSql().toString();
                dbExecutorService.select(statementScope, sessionScope, jdbcSql, parameterObject,
                        new DefaultResultSetHandler(statementScope, rowHandler));
            }
            finally
            {
                sessionScope.close();
            }
        }
        finally
        {
            cleanQuietly(statementScope);
        }
    }

    MappedStatement getMappedStatement(CTString id)
    {
        return sqlMapFacade.getMappedStatement(id);
    }

    Object execute(MappedStatement mappedStatement, Object parameterObject, SessionContext sessionContext)
            throws Exception
    {
        switch (mappedStatement.getStatementType())
        {
            case SELECT:
                AbstractSession.DataListRowHandler handler = new AbstractSession.DataListRowHandler();
                select((SelectStatement) mappedStatement, parameterObject, sessionContext, handler);
                return handler.getList();

            case INSERT:
                if (parameterObject instanceof Collection)
                {
                    return batchInsert((InsertStatement) mappedStatement, (Collection) parameterObject, sessionContext);
                }
                return insert((InsertStatement) mappedStatement, parameterObject, sessionContext);

            case UPDATE:
            case DELETE:
                if (parameterObject instanceof Collection)
                {
                    return batchUpdate(mappedStatement, (Collection) parameterObject, sessionContext);
                }
                return update(mappedStatement, parameterObject, sessionContext);

            default:
                throw new SessionException("Unsupported mapped statement,id:" + mappedStatement.getId());
        }
    }

    private void checkParameter(MappedStatement mappedStatement, Object parameterObject)
    {
        Class<?> parameterClass = mappedStatement.getParameterClass();
        if (parameterObject == null || parameterClass == null)
        {
            return;
        }
        if (parameterClass.isAssignableFrom(parameterObject.getClass()))
        {
            return;
        }
        throw new IllegalArgumentException(
                "Invalid parameter object type.  Expected '" + parameterClass.getName() + "' but found '" + parameterObject.getClass()
                        .getName() + "'.");
    }

    private void cleanQuietly(StatementScope statementScope)
    {
        try
        {
            statementScope.cleanup();
        }
        catch (RuntimeException e)
        {
            statementScope.getStatement().getLogger().error(e.toString(), e);
        }
    }

    private static class DefaultKeyHandler implements ResultSetHandler
    {
        private final GeneratedKeyConfig keyConfig;
        private final Object parameter;

        private DefaultKeyHandler(GeneratedKeyConfig keyConfig, Object parameter)
        {
            this.keyConfig = keyConfig;
            this.parameter = parameter;
        }

        @Override
        public void handle(ResultSet resultSet) throws SQLException
        {
            Probe probe = ProbeFactory.getProbe();
            if (Strings.isEmpty(keyConfig.getColumn()))
            {
                Object value = keyConfig.getTypeHandler().getResult(resultSet, 1);
                probe.setObject(parameter, keyConfig.getProperty(), value);
                return;
            }

            if (resultSet.next())
            {
                Object value = keyConfig.getTypeHandler().getResult(resultSet, keyConfig.getColumn());
                probe.setObject(parameter, keyConfig.getProperty(), value);
            }
        }
    }

    private static class DefaultResultSetHandler implements ResultSetHandler
    {
        private final StatementScope statementScope;
        private final RowHandler rowHandler;

        private DefaultResultSetHandler(StatementScope statementScope, RowHandler rowHandler)
        {
            this.statementScope = statementScope;
            this.rowHandler = rowHandler;
        }

        @Override
        public void handle(ResultSet rs) throws SQLException
        {
            Logger logger = statementScope.getStatement().getLogger();
            ResultMap resultMap = statementScope.getResultMap();

            if (resultMap == null)
            {
                logger.warn("None result config, use default.");
                readResultSetDefault(rowHandler, rs);
                return;
            }

            while (rs.next())
            {
                Object object = resultMap.setResultObjectValues(statementScope, rs);
                rowHandler.handleRow(object);
            }
        }

        private void readResultSetDefault(RowHandler rowHandler, ResultSet rs) throws SQLException
        {
            int count = rs.getMetaData().getColumnCount();
            while (rs.next())
            {
                Object[] array = new Object[count];
                for (int i = 1; i <= count; i++)
                {
                    array[i - 1] = rs.getObject(i);
                }
                rowHandler.handleRow(array);
            }
        }
    }
}
