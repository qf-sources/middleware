package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

public class IsEmptyTagHandler extends ConditionalTagHandler
{
    private static final Probe PROBE = ProbeFactory.getProbe();

    public boolean isCondition(SqlTagContext ctx, SqlTag tag, Object parameterObject)
    {
        if (parameterObject == null) return true;

        CharSequence prop = getResolvedProperty(ctx, tag);
        if (prop == null) return false;

        Object value = PROBE.getObject(parameterObject, prop);
        if (value == null) return true;
        if (value instanceof CharSequence) return ((CharSequence) value).length() == 0;
        if (value instanceof Collection) return ((Collection) value).isEmpty();
        if (value instanceof Map) return ((Map) value).isEmpty();
        if (value.getClass().isArray()) return Array.getLength(value) == 0;
        return String.valueOf(value).equals("");
    }
}
