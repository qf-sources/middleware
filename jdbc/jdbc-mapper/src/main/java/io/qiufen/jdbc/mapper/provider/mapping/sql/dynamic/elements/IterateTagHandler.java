package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;
import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;
import io.qiufen.jdbc.mapper.provider.mapping.sql.simple.SimpleDynamicSql;

public class IterateTagHandler extends BaseTagHandler
{
    private static final Probe PROBE = ProbeFactory.getProbe();

    public int doStartFragment(SqlTagContext ctx, SqlTag tag, Object parameterObject)
    {
        IterateContext iterate = (IterateContext) ctx.getAttribute(tag);
        if (iterate == null)
        {
            IterateContext parentIterate = ctx.peekIterateContext();

            ctx.pushRemoveFirstPrependMarker(tag);

            Object collection;
            String prop = tag.getPropertyAttr();
            if (prop != null && !prop.equals(""))
            {
                if (null != parentIterate && parentIterate.isAllowNext())
                {
                    parentIterate.next();
                    parentIterate.setAllowNext(false);
                    if (!parentIterate.hasNext())
                    {
                        parentIterate.setFinal(true);
                    }
                }
                CharSequence prop0 = prop;
                if (parentIterate != null)
                {
                    prop0 = parentIterate.addIndexToTagProperty(
                            new java.lang.StringBuilder(prop.length() + 2).append(prop));
                }
                collection = PROBE.getObject(parameterObject, prop0);
            }
            else
            {
                collection = parameterObject;
            }
            iterate = new IterateContext(collection, tag, parentIterate);

            iterate.setProperty(null == prop ? "" : prop);

            ctx.setAttribute(tag, iterate);
            ctx.pushIterateContext(iterate);
        }
        else if ("iterate".equals(tag.getRemoveFirstPrepend()))
        {
            ctx.reEnableRemoveFirstPrependMarker();
        }

        return iterate.hasNext() ? INCLUDE_BODY : SKIP_BODY;
    }

    public int doEndFragment(SqlTagContext ctx, SqlTag tag, Object parameterObject, StringBuilder bodyContent)
    {
        IterateContext iterate = (IterateContext) ctx.getAttribute(tag);

        if (iterate.hasNext() || iterate.isFinal())
        {
            if (iterate.isAllowNext())
            {
                iterate.next();
            }
            if (CharSequences.isNotBlank(bodyContent))
            {
                // the sub element produced a result.  If it is the first one
                // to produce a result, then we need to add the open
                // text.  If it is not the first to produce a result then
                // we need to add the conjunction text
                if (iterate.someSubElementsHaveContent())
                {
                    if (tag.isConjunctionAvailable())
                    {
                        String attr = tag.getConjunctionAttr();
                        if (tag.isSimpleDynamicConjunctionAttr())
                        {
                            bodyContent.insert(0, SimpleDynamicSql.processDynamicElements(ctx.getSqlMapContext(),
                                    tag.getConjunctionAttr(), parameterObject), StringBuilder.COPIER_STRING_BUILDER);
                        }
                        else
                        {
                            bodyContent.insert(0, attr, StringBuilder.COPIER_STRING);
                        }
                    }
                }
                else
                {
                    // we need to specify that this is the first content
                    // producing element so that the doPrepend method will
                    // add the prepend
                    iterate.setPrependEnabled(true);

                    if (tag.isOpenAvailable())
                    {
                        appendOpenAttr(ctx, tag, parameterObject, bodyContent);
                    }
                }
                iterate.setSomeSubElementsHaveContent(true);
            }

            if (iterate.isLast() && iterate.someSubElementsHaveContent())
            {
                if (tag.isCloseAvailable())
                {
                    appendCloseAttr(ctx, tag, parameterObject, bodyContent);
                }
            }

            iterate.setAllowNext(true);
            return iterate.isFinal() ? super.doEndFragment(ctx, tag, parameterObject, bodyContent) : REPEAT_BODY;
        }
        else
        {
            return super.doEndFragment(ctx, tag, parameterObject, bodyContent);
        }
    }

    public void doPrepend(SqlTagContext ctx, SqlTag tag, Object parameterObject, StringBuilder bodyContent)
    {
        IterateContext iterate = (IterateContext) ctx.getAttribute(tag);
        if (iterate.isPrependEnabled())
        {
            super.doPrepend(ctx, tag, parameterObject, bodyContent);
            iterate.setPrependEnabled(false);  // only do the prepend one time
        }
    }
}

