package io.qiufen.jdbc.mapper.provider.session;

import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.SQLException;

interface StatementBatch
{
    int[] execute() throws SQLException;

    void close();
}

interface CommonStatementBatch extends StatementBatch
{
    void addStatement(StatementScope statementScope, String jdbcSql, Object parameter) throws SQLException;
}

interface KeysStatementBatch extends StatementBatch
{
    void addStatement(StatementScope statementScope, String jdbcSql, Object parameter, ResultSetHandler handler)
            throws SQLException;
}