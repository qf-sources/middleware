package io.qiufen.jdbc.mapper.provider.common.beans;

import java.util.Map;
import java.util.StringTokenizer;

/**
 * StaticBeanProbe provides methods that allow simple, reflective access to
 * JavaBeans style properties.  Methods are provided for all simple types as
 * well as object types.
 * <p/>
 * Examples:
 * <p/>
 * StaticBeanProbe.setObject(object, propertyName, value);
 * <p/>
 * Object value = StaticBeanProbe.getObject(object, propertyName);
 */
public class GenericProbe extends BaseProbe
{
    private static final BaseProbe BEAN_PROBE = new ComplexBeanProbe();

    protected GenericProbe() {}

    /**
     * Gets an object from a Map or bean
     *
     * @param object - the object to probe
     * @param name   - the name of the property (or map entry)
     * @return The value of the property (or map entry)
     */
    public Object getObject(Object object, CharSequence name)
    {
        return BEAN_PROBE.getObject(object, name);
    }

    /**
     * Sets an object in a Map or bean
     *
     * @param object - the object to probe
     * @param name   - the name of the property (or map entry)
     * @param value  - the new value of the property (or map entry)
     */
    public void setObject(Object object, String name, Object value)
    {
        BEAN_PROBE.setObject(object, name, value);
    }

    /**
     * Returns the class that the setter expects to receive as a parameter when
     * setting a property value.
     *
     * @param object - The class to check
     * @param name   - the name of the property
     * @return The type of the property
     */
    public Class<?> getPropertyTypeForSetter(Object object, String name)
    {
        if (object instanceof Class)
        {
            return getClassPropertyTypeForSetter((Class<?>) object, name);
        }
        else
        {
            return BEAN_PROBE.getPropertyTypeForSetter(object, name);
        }
    }

    /**
     * Returns the class that the getter will return when reading a property value.
     *
     * @param object The bean to check
     * @param name   The name of the property
     * @return The type of the property
     * @see Probe#getPropertyTypeForGetter(java.lang.Object, java.lang.String)
     */
    public Class<?> getPropertyTypeForGetter(Object object, String name)
    {
        if (object instanceof Class)
        {
            return getClassPropertyTypeForGetter((Class<?>) object, name);
        }
        else if (name.indexOf('[') > -1)
        {
            return BEAN_PROBE.getIndexedType(object, name);
        }
        else
        {
            return BEAN_PROBE.getPropertyTypeForGetter(object, name);
        }
    }

    /**
     * Checks to see if an object has a writable property by a given name
     *
     * @param object       The bean to check
     * @param propertyName The property to check for
     * @return True if the property exists and is writable
     * @see Probe#hasWritableProperty(java.lang.Object, java.lang.String)
     */
    public boolean hasWritableProperty(Object object, String propertyName)
    {
        return BEAN_PROBE.hasWritableProperty(object, propertyName);
    }

    /**
     * Checks to see if a bean has a readable property by a given name
     *
     * @param object       The bean to check
     * @param propertyName The property to check for
     * @return True if the property exists and is readable
     * @see Probe#hasReadableProperty(java.lang.Object, java.lang.String)
     */
    public boolean hasReadableProperty(Object object, String propertyName)
    {
        return BEAN_PROBE.hasReadableProperty(object, propertyName);
    }

    protected void setProperty(Object object, String property, Object value)
    {
        BEAN_PROBE.setProperty(object, property, value);
    }

    protected Object getProperty(Object object, CharSequence property)
    {
        return BEAN_PROBE.getProperty(object, property);
    }

    private Class<?> getClassPropertyTypeForGetter(Class<?> type, String name)
    {
        if (name.indexOf('.') > -1)
        {
            StringTokenizer parser = new StringTokenizer(name, ".");
            while (parser.hasMoreTokens())
            {
                name = parser.nextToken();
                if (Map.class.isAssignableFrom(type))
                {
                    type = Object.class;
                    break;
                }
                type = ClassInfo.getInstance(type).getGetterType(name);
            }
        }
        else
        {
            type = ClassInfo.getInstance(type).getGetterType(name);
        }

        return type;
    }

    /**
     * Returns the class that the setter expects to receive as a parameter when
     * setting a property value.
     *
     * @param type The class to check
     * @param name The name of the property
     * @return The type of the property
     */
    private Class<?> getClassPropertyTypeForSetter(Class<?> type, String name)
    {

        if (name.indexOf('.') > -1)
        {
            StringTokenizer parser = new StringTokenizer(name, ".");
            while (parser.hasMoreTokens())
            {
                name = parser.nextToken();
                if (Map.class.isAssignableFrom(type))
                {
                    type = Object.class;
                    break;
                }
                type = ClassInfo.getInstance(type).getSetterType(name);
            }
        }
        else
        {
            type = ClassInfo.getInstance(type).getSetterType(name);
        }

        return type;
    }
}
