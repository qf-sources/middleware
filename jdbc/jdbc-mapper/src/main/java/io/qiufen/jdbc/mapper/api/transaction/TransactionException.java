package io.qiufen.jdbc.mapper.api.transaction;

import io.qiufen.jdbc.mapper.api.session.SessionException;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/28 10:31
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class TransactionException extends SessionException
{
    public TransactionException(String message)
    {
        super(message);
    }

    public TransactionException(Throwable cause)
    {
        super(cause);
    }
}
