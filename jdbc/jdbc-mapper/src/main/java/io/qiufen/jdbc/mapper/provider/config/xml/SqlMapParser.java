package io.qiufen.jdbc.mapper.provider.config.xml;

import io.qiufen.common.io.IOUtil;
import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.common.resources.Resources;
import io.qiufen.jdbc.mapper.provider.common.xml.Nodelet;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletException;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletParser;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletUtils;
import io.qiufen.jdbc.mapper.provider.config.ParameterMapConfig;
import io.qiufen.jdbc.mapper.provider.config.ResultMapConfig;
import io.qiufen.jdbc.mapper.provider.statement.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.Properties;

public class SqlMapParser
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SqlMapParser.class);

    private final NodeletParser parser;
    private final XmlParserState state;
    private final SqlStatementParser statementParser;

    public SqlMapParser(XmlParserState state)
    {
        this.parser = new NodeletParser();
        this.state = state;

        parser.setValidation(true);
        parser.setEntityResolver(new SqlMapClasspathEntityResolver());

        statementParser = new SqlStatementParser(this.state);

        addSqlMapNodelets();
        addSqlNodelets();
        addTypeAliasNodelets();
        addParameterMapNodelets();
        addResultMapNodelets();
        addStatementNodelets();

    }

    public void parse(Reader reader) throws NodeletException
    {
        parser.parse(reader);
    }

    public void parse(InputStream inputStream) throws Exception
    {
        parser.parse(inputStream);
    }

    public void parse(URL url) throws NodeletException, IOException
    {
        LOGGER.debug("Loading sql map:{}...", url);
        InputStream stream = url.openStream();
        try
        {
            parser.parse(stream);
        }
        catch (Exception e)
        {
            throw new NodeletException("Parse file failure, url:" + url, e);
        }
        finally
        {
            IOUtil.close(stream);
        }
    }

    private void addSqlMapNodelets()
    {
        parser.addNodelet("/sqlMap", new Nodelet()
        {
            @Override
            public void process(Node node)
            {
                Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                state.setNamespace(attributes.getProperty("namespace"));
            }
        });
    }

    private void addSqlNodelets()
    {
        parser.addNodelet("/sqlMap/sql", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String id = attributes.getProperty("id");
                if (state.isUseStatementNamespaces())
                {
                    id = state.applyNamespace(id);
                }
                if (state.getSqlIncludes().containsKey(id))
                {
                    throw new SqlMapConfigException("Duplicate <sql>-include '" + id + "' found.");
                }
                else
                {
                    state.getSqlIncludes().put(id, node);
                }
            }
        });
    }

    private void addTypeAliasNodelets()
    {
        parser.addNodelet("/sqlMap/typeAlias", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                Properties prop = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String alias = prop.getProperty("alias");
                String type = prop.getProperty("type");
                state.getSqlMapContext().getTypeHandlerRegistry().putTypeAlias(alias, type);
            }
        });
    }

    private void addParameterMapNodelets()
    {
        parser.addNodelet("/sqlMap/parameterMap/end()", new Nodelet()
        {
            @Override
            public void process(Node node)
            {
                state.getParamConfig().finish();
                state.setParamConfig(null);
            }
        });
        parser.addNodelet("/sqlMap/parameterMap", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String id = state.applyNamespace(attributes.getProperty("id"));
                String parameterClassName = attributes.getProperty("class");
                parameterClassName = state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(parameterClassName);
                try
                {
                    ParameterMapConfig paramConf = state.getSqlMapContext()
                            .newParameterMapConfig(CTString.of(id), Resources.classForName(parameterClassName));
                    state.setParamConfig(paramConf);
                }
                catch (Exception e)
                {
                    throw new SqlMapConfigException(
                            "Error configuring ParameterMap.  Could not set ParameterClass.  Cause: " + e, e);
                }
            }
        });
        parser.addNodelet("/sqlMap/parameterMap/parameter", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                Properties childAttributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String propertyName = childAttributes.getProperty("property");
                String jdbcType = childAttributes.getProperty("jdbcType");
                String javaType = childAttributes.getProperty("javaType");
                String nullValue = childAttributes.getProperty("nullValue");
                String mode = childAttributes.getProperty("mode");
                String callback = childAttributes.getProperty("typeHandler");

                callback = state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(callback);
                Object typeHandlerImpl = null;
                if (callback != null)
                {
                    typeHandlerImpl = Resources.instantiate(callback);
                }

                javaType = state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(javaType);
                Class<?> javaClass = null;
                try
                {
                    if (javaType != null && javaType.length() > 0)
                    {
                        javaClass = Resources.classForName(javaType);
                    }
                }
                catch (ClassNotFoundException e)
                {
                    throw new RuntimeException("Error setting javaType on parameter mapping.  Cause: " + e);
                }

                state.getParamConfig()
                        .addParameterMapping(propertyName, javaClass, jdbcType, nullValue, typeHandlerImpl);
            }
        });
    }


    private void addResultMapNodelets()
    {
        parser.addNodelet("/sqlMap/resultMap", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String id = state.applyNamespace(attributes.getProperty("id"));
                String resultClassName = attributes.getProperty("class");
                String extended = state.applyNamespace(attributes.getProperty("extends"));

                resultClassName = state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(resultClassName);
                Class<?> resultClass;
                try
                {
                    resultClass = Resources.classForName(resultClassName);
                }
                catch (Exception e)
                {
                    throw new SqlMapConfigException(
                            "Error configuring Result.  Could not set ResultClass.  Cause: " + e, e);
                }
                CTString ctExtend = extended == null ? null : CTString.of(extended);
                ResultMapConfig resultConf = state.getSqlMapContext()
                        .newResultMapConfig(CTString.of(id), resultClass, ctExtend);
                state.setResultConfig(resultConf);
            }
        });
        parser.addNodelet("/sqlMap/resultMap/end()", new Nodelet()
        {
            @Override
            public void process(Node node)
            {
                state.getResultConfig().finish();
                state.setResultConfig(null);
            }
        });
        parser.addNodelet("/sqlMap/resultMap/result", new Nodelet()
        {
            @Override
            public void process(Node node)
            {
                Properties childAttributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String propertyName = childAttributes.getProperty("property");
                String nullValue = childAttributes.getProperty("nullValue");
                String jdbcType = childAttributes.getProperty("jdbcType");
                String javaType = childAttributes.getProperty("javaType");
                String columnName = childAttributes.getProperty("column");
                String columnIndexProp = childAttributes.getProperty("columnIndex");
                String callback = childAttributes.getProperty("typeHandler");
                String notNullColumn = childAttributes.getProperty("notNullColumn");

                Class<?> javaClass = null;
                try
                {
                    javaType = state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(javaType);
                    if (javaType != null && javaType.length() > 0)
                    {
                        javaClass = Resources.classForName(javaType);
                    }
                }
                catch (ClassNotFoundException e)
                {
                    throw new RuntimeException("Error setting java type on result discriminator mapping.  Cause: " + e);
                }

                Object typeHandlerImpl = null;
                try
                {
                    if (callback != null && callback.length() > 0)
                    {
                        callback = state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(callback);
                        typeHandlerImpl = Resources.instantiate(callback);
                    }
                }
                catch (Exception e)
                {
                    throw new RuntimeException("Error occurred during custom type handler configuration.  Cause: " + e,
                            e);
                }

                Integer columnIndex = null;
                if (columnIndexProp != null)
                {
                    try
                    {
                        columnIndex = Integer.valueOf(columnIndexProp);
                    }
                    catch (Exception e)
                    {
                        throw new RuntimeException("Error parsing column index.  Cause: " + e, e);
                    }
                }

                state.getResultConfig()
                        .addResultMapping(propertyName, columnName, columnIndex, javaClass, jdbcType, nullValue,
                                notNullColumn, typeHandlerImpl);
            }
        });
    }

    protected void addStatementNodelets()
    {
        parser.addNodelet("/sqlMap/statement", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                statementParser.parseGeneralStatement(node, new MappedStatement());
            }
        });
        parser.addNodelet("/sqlMap/insert", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                statementParser.parseGeneralStatement(node, new InsertStatement());
            }
        });
        parser.addNodelet("/sqlMap/update", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                statementParser.parseGeneralStatement(node, new UpdateStatement());
            }
        });
        parser.addNodelet("/sqlMap/delete", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                statementParser.parseGeneralStatement(node, new DeleteStatement());
            }
        });
        parser.addNodelet("/sqlMap/select", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                statementParser.parseGeneralStatement(node, new SelectStatement());
            }
        });
    }
}
