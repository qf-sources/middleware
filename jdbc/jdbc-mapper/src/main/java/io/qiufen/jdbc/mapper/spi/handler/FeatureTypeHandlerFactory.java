package io.qiufen.jdbc.mapper.spi.handler;

public interface FeatureTypeHandlerFactory<F extends Feature>
{
    TypeHandler create(Class<? extends F> featureImplementType);
}
