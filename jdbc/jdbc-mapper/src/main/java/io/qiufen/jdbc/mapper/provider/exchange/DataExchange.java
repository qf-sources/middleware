package io.qiufen.jdbc.mapper.provider.exchange;

import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Interface for exchanging data between a parameter map/result map and the related objects
 */
public interface DataExchange
{
    void initialize(ParameterMap obj);

    void initialize(ResultMap obj);

    /**
     * Gets a data array from a parameter object.
     *
     * @param statementScope  - the scope of the request
     * @param parameterMap    - the parameter map
     * @param parameterObject - the parameter object
     * @return - the objects
     */
    Object[] getData(StatementScope statementScope, ParameterMap parameterMap, Object parameterObject);

    Object setData(StatementScope statementScope, ResultMap resultMap, ResultSet rs) throws SQLException;

    void setData(StatementScope statementScope, ParameterMap parameterMap, PreparedStatement ps, Object parameterObject)
            throws SQLException;
}