package io.qiufen.jdbc.mapper.provider.mapping.parameter;

import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class DefaultParameterMap extends AbstractParameterMap
{
    private static final ParameterMapping[] EMPTY = new ParameterMapping[0];

    private ParameterMapping[] parameterMappings = EMPTY;

    DefaultParameterMap(SqlMapContext sqlMapContext)
    {
        super(sqlMapContext);
    }

    @Override
    public int countParameterMapping()
    {
        return this.parameterMappings.length;
    }

    @Override
    public ParameterMapping getParameterMapping(int index)
    {
        return this.parameterMappings[index];
    }

    void setParameterMappings(ParameterMapping[] parameterMappings)
    {
        if (parameterMappings != null)
        {
            this.parameterMappings = parameterMappings;
        }
        super.initDataExchange();
    }

    @Override
    public VariableParameterMap toVar()
    {
        VariableParameterMap map = new VariableParameterMap(getSqlMapContext());
        map.setId(getId());
        map.setParameterClass(getParameterClass());
        List<ParameterMapping> list = new ArrayList<ParameterMapping>();
        Collections.addAll(list, parameterMappings);
        map.setParameterMappingList(list);
        return map;
    }
}
