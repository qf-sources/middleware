package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.common.lang.Strings;
import io.qiufen.jdbc.mapper.provider.mapping.sql.SqlChild;
import io.qiufen.jdbc.mapper.provider.mapping.sql.simple.SimpleDynamicSql;

import java.util.ArrayList;
import java.util.List;

public class SqlTag implements SqlChild, DynamicParent
{
    private static final char START_INDEX = '[';

    private static final byte FLAG_PREPEND = 1;
    private static final byte FLAG_OPEN = 1 << 1;
    private static final byte FLAG_CLOSE = 1 << 2;
    private static final byte FLAG_CONJUNCTION = 1 << 3;
    private static final byte FLAG_INDEXED_PROPERTY = 1 << 4;

    private final List<SqlChild> children = new ArrayList<SqlChild>(1);
    private String name;
    private SqlTagHandler handler;
    // general attributes
    private String prependAttr;
    private String propertyAttr;
    private String removeFirstPrepend;
    // conditional attributes
    private String comparePropertyAttr;
    private String compareValueAttr;
    // iterate attributes
    private String openAttr;
    private String closeAttr;
    private String conjunctionAttr;
    private SqlTag parent;
    private boolean postParseRequired = false;

    private byte attr = 0;

    private byte simpleDynamicAttr = 0;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public SqlTagHandler getHandler()
    {
        return handler;
    }

    public void setHandler(SqlTagHandler handler)
    {
        this.handler = handler;
    }

    public boolean isPrependAvailable()
    {
        return (this.attr & FLAG_PREPEND) == FLAG_PREPEND;
    }

    public boolean isCloseAvailable()
    {
        return (this.attr & FLAG_CLOSE) == FLAG_CLOSE;
    }

    public boolean isOpenAvailable()
    {
        return (this.attr & FLAG_OPEN) == FLAG_OPEN;
    }

    public boolean isConjunctionAvailable()
    {
        return (this.attr & FLAG_CONJUNCTION) == FLAG_CONJUNCTION;
    }

    public String getPrependAttr()
    {
        return prependAttr;
    }

    public void setPrependAttr(String prependAttr)
    {
        this.prependAttr = prependAttr;
        if (prependAttr != null && prependAttr.length() > 0)
        {
            this.attr |= FLAG_PREPEND;
            if (SimpleDynamicSql.isSimpleDynamicSql(prependAttr))
            {
                this.simpleDynamicAttr |= FLAG_PREPEND;
            }
        }
    }

    public String getPropertyAttr()
    {
        return propertyAttr;
    }

    public void setPropertyAttr(String propertyAttr)
    {
        this.propertyAttr = propertyAttr;
        if (Strings.isNotEmpty(propertyAttr) && propertyAttr.indexOf(START_INDEX) >= 0)
        {
            this.attr |= FLAG_INDEXED_PROPERTY;
        }
    }

    public String getComparePropertyAttr()
    {
        return comparePropertyAttr;
    }

    public void setComparePropertyAttr(String comparePropertyAttr)
    {
        this.comparePropertyAttr = comparePropertyAttr;
    }

    public String getCompareValueAttr()
    {
        return compareValueAttr;
    }

    public void setCompareValueAttr(String compareValueAttr)
    {
        this.compareValueAttr = compareValueAttr;
    }

    public String getOpenAttr()
    {
        return openAttr;
    }

    public void setOpenAttr(String openAttr)
    {
        this.openAttr = openAttr;
        if (openAttr != null && openAttr.length() > 0)
        {
            this.attr |= FLAG_OPEN;
            if (SimpleDynamicSql.isSimpleDynamicSql(openAttr))
            {
                this.simpleDynamicAttr |= FLAG_OPEN;
            }
        }
    }

    public String getCloseAttr()
    {
        return closeAttr;
    }

    public void setCloseAttr(String closeAttr)
    {
        this.closeAttr = closeAttr;
        if (closeAttr != null && closeAttr.length() > 0)
        {
            this.attr |= FLAG_CLOSE;
            if (SimpleDynamicSql.isSimpleDynamicSql(closeAttr))
            {
                this.simpleDynamicAttr |= FLAG_CLOSE;
            }
        }
    }

    public String getConjunctionAttr()
    {
        return conjunctionAttr;
    }

    public void setConjunctionAttr(String conjunctionAttr)
    {
        this.conjunctionAttr = conjunctionAttr;
        if (conjunctionAttr != null && conjunctionAttr.length() > 0)
        {
            this.attr |= FLAG_CONJUNCTION;
            if (SimpleDynamicSql.isSimpleDynamicSql(conjunctionAttr))
            {
                this.simpleDynamicAttr |= FLAG_CONJUNCTION;
            }
        }
    }

    public void addChild(SqlChild child)
    {
        if (child instanceof SqlTag)
        {
            ((SqlTag) child).parent = this;
        }
        children.add(child);
    }

    public List<SqlChild> getChildren()
    {
        return children;
    }

    public SqlTag getParent()
    {
        return parent;
    }

    public String getRemoveFirstPrepend()
    {
        return removeFirstPrepend;
    }

    public void setRemoveFirstPrepend(String removeFirstPrepend)
    {
        this.removeFirstPrepend = removeFirstPrepend;
    }

    /**
     * @return Returns the postParseRequired.
     */
    public boolean isPostParseRequired()
    {
        return postParseRequired;
    }

    /**
     * @param iterateAncestor The postParseRequired to set.
     */
    public void setPostParseRequired(boolean iterateAncestor)
    {
        this.postParseRequired = iterateAncestor;
    }

    public boolean isSimpleDynamicPrependAttr()
    {
        return (this.simpleDynamicAttr & FLAG_PREPEND) == FLAG_PREPEND;
    }

    public boolean isSimpleDynamicOpenAttr()
    {
        return (this.simpleDynamicAttr & FLAG_OPEN) == FLAG_OPEN;
    }

    public boolean isSimpleDynamicCloseAttr()
    {
        return (this.simpleDynamicAttr & FLAG_CLOSE) == FLAG_CLOSE;
    }

    public boolean isSimpleDynamicConjunctionAttr()
    {
        return (this.simpleDynamicAttr & FLAG_CONJUNCTION) == FLAG_CONJUNCTION;
    }

    public boolean isIndexedProperty()
    {
        return (this.attr & FLAG_INDEXED_PROPERTY) == FLAG_INDEXED_PROPERTY;
    }
}
