package io.qiufen.jdbc.mapper.provider.config;

import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;
import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;
import io.qiufen.jdbc.mapper.provider.common.resources.Resources;
import io.qiufen.jdbc.mapper.provider.handler.TypeHandlerRegistry;
import io.qiufen.jdbc.mapper.provider.handler.extension.CustomTypeHandler;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.sql.VariableSqlText;
import io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements.SqlTagContext;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.extension.TypeHandlerCallback;

import java.util.*;

public class InlineParameterMapParser
{
    private static final char PARAMETER_TOKEN = '#';

    private static final Probe probe = ProbeFactory.getProbe();
    private static final char[] Z = {'?'};
    private final Map<String, ParameterMapping> cache = new HashMap<String, ParameterMapping>();

    public VariableSqlText parseInlineParameterMap(TypeHandlerRegistry typeHandlerRegistry, String sqlSegment,
            Class<?> parameterClass)
    {
        StringBuilder sqlSegmentBuffer = new StringBuilder(sqlSegment.length()).append(sqlSegment);
        return parseInlineParameterMap(typeHandlerRegistry, sqlSegmentBuffer, parameterClass);
    }

    public VariableSqlText parseInlineParameterMap(TypeHandlerRegistry typeHandlerRegistry, CTString sqlSegment,
            Class<?> parameterClass)
    {
        StringBuilder sqlSegmentBuffer = new StringBuilder(sqlSegment.length()).append(sqlSegment);
        return parseInlineParameterMap(typeHandlerRegistry, sqlSegmentBuffer, parameterClass);
    }

    private VariableSqlText parseInlineParameterMap(TypeHandlerRegistry typeHandlerRegistry,
            StringBuilder sqlSegmentBuilder, Class<?> parameterClass)
    {
        List<ParameterMapping> mappingList = new ArrayList<ParameterMapping>();
        CharSequences.replaceArgs(sqlSegmentBuilder, PARAMETER_TOKEN, PARAMETER_TOKEN, mappingList,
                new Parser1(typeHandlerRegistry, parameterClass));

        VariableSqlText sqlText = new VariableSqlText();
        sqlText.setText(sqlSegmentBuilder);
        sqlText.setParameterMappingList(mappingList);
        return sqlText;
    }

    public void parseInlineParameterMap(TypeHandlerRegistry typeHandlerRegistry, StringBuilder sqlSegmentBuffer,
            Class<?> parameterClass, SqlTagContext ctx)
    {
        CharSequences.replaceArgs(sqlSegmentBuffer, PARAMETER_TOKEN, PARAMETER_TOKEN, ctx,
                new Parser(typeHandlerRegistry, parameterClass));
    }

    private ParameterMapping newParseMapping(CharSequences.Var token, Class<?> parameterClass,
            TypeHandlerRegistry typeHandlerRegistry)
    {
        // #propertyName,javaType=string,jdbcType=VARCHAR,mode=IN,nullValue=N/A,handler=string,numericScale=2#
        String name = "";
        int i = 0, len = token.length();
        for (; i < len; i++)
        {
            if (token.charAt(i) == ',') break;
        }
        name = token.subSequence(0, i).toString();
        ParameterMapping mapping = this.cache.get(name);
        if (mapping != null) return mapping;
        synchronized (this)
        {
            mapping = this.cache.get(name);
            if (mapping != null) return mapping;
            String str = "";
            if (i < len) str = token.subSequence(i + 1, len).toString();
            StringTokenizer paramParser = new StringTokenizer(str, "=,", false);
            mapping = newParseMapping0(token, name, paramParser, parameterClass, typeHandlerRegistry);
            this.cache.put(name, mapping);
        }
        return mapping;
    }

    private ParameterMapping newParseMapping0(CharSequences.Var token, String name, StringTokenizer paramParser,
            Class<?> parameterClass, TypeHandlerRegistry typeHandlerRegistry)
    {
        ParameterMapping mapping = new ParameterMapping();
        // #propertyName,javaType=string,jdbcType=VARCHAR,mode=IN,nullValue=N/A,handler=string,numericScale=2#
        mapping.setPropertyName(name);
        while (paramParser.hasMoreTokens())
        {
            String field = paramParser.nextToken();
            if (paramParser.hasMoreTokens())
            {
                String value = paramParser.nextToken();
                if ("javaType".equals(field))
                {
                    value = typeHandlerRegistry.resolveAlias(value);
                    mapping.setJavaTypeName(value);
                }
                else if ("jdbcType".equals(field))
                {
                    mapping.setJdbcTypeName(value);
                }
                else if ("nullValue".equals(field))
                {
                    mapping.setNullValue(value);
                }
                else if ("handler".equals(field))
                {
                    try
                    {
                        value = typeHandlerRegistry.resolveAlias(value);
                        Object impl = Resources.instantiate(value);
                        if (impl instanceof TypeHandlerCallback)
                        {
                            mapping.setTypeHandler(new CustomTypeHandler((TypeHandlerCallback) impl));
                        }
                        else if (impl instanceof TypeHandler)
                        {
                            mapping.setTypeHandler((TypeHandler) impl);
                        }
                        else
                        {
                            throw new SessionException(
                                    "The class " + value + " is not a valid implementation of TypeHandler or TypeHandlerCallback");
                        }
                    }
                    catch (SessionException e)
                    {
                        throw e;
                    }
                    catch (Exception e)
                    {
                        throw new SessionException(
                                "Error loading class specified by handler field in " + token + ".  Cause: " + e, e);
                    }
                }
                else
                {
                    throw new SessionException("Unrecognized parameter mapping field: '" + field + "' in " + token);
                }
            }
            else
            {
                throw new SessionException(
                        "Incorrect inline parameter map format (missmatched name=value pairs): " + token);
            }
        }

        if (mapping.getTypeHandler() == null)
        {
            try
            {
                TypeHandler handler = resolveTypeHandler(typeHandlerRegistry, parameterClass, mapping.getPropertyName(),
                        mapping.getJavaTypeName(), mapping.getJdbcTypeName());
                mapping.setTypeHandler(handler);
            }
            catch (Exception e)
            {
                throw new SessionException("Error.  Could not set TypeHandler.  Cause: " + e, e);
            }
        }
        return mapping;
    }

    private TypeHandler resolveTypeHandler(TypeHandlerRegistry typeHandlerRegistry, Class<?> parameterClass,
            String propertyName, String javaType, String jdbcType) throws ClassNotFoundException
    {
        if (parameterClass == null)
        {
            // Unknown
            return typeHandlerRegistry.getUnkownTypeHandler();
        }

        if (Map.class.isAssignableFrom(parameterClass))
        {
            // Map
            if (javaType == null)
            {
                return typeHandlerRegistry.getUnkownTypeHandler(); //BUG 1012591 - typeHandlerFactory.getTypeHandler(java.lang.Object.class, jdbcType);
            }
            javaType = typeHandlerRegistry.resolveAlias(javaType);
            Class<?> javaClass = Resources.classForName(javaType);
            return typeHandlerRegistry.getTypeHandler(javaClass, jdbcType);
        }

        TypeHandler handler;
        // Primitive
        if ((handler = typeHandlerRegistry.getTypeHandler(parameterClass, jdbcType)) != null)
        {
            return handler;
        }

        // JavaBean
        if (javaType == null)
        {
            if (CharSequences.indexOf(propertyName, '[', 0) <= -1)
            {
                Class<?> type = probe.getPropertyTypeForGetter(parameterClass, propertyName);
                handler = typeHandlerRegistry.getTypeHandler(type, jdbcType);
            }
            else
            {
                handler = typeHandlerRegistry.getUnkownTypeHandler();
            }
        }
        else
        {
            javaType = typeHandlerRegistry.resolveAlias(javaType);
            Class<?> javaClass = Resources.classForName(javaType);
            handler = typeHandlerRegistry.getTypeHandler(javaClass, jdbcType);
        }
        return handler;
    }

    abstract class BaseParser<T> implements CharSequences.VariableParser<T>
    {
        private final TypeHandlerRegistry typeHandlerRegistry;
        private final Class<?> parameterClass;

        protected BaseParser(TypeHandlerRegistry typeHandlerRegistry, Class<?> parameterClass)
        {
            this.typeHandlerRegistry = typeHandlerRegistry;
            this.parameterClass = parameterClass;
        }

        @Override
        public int parse(T t, CharSequences.Var var, CharSequences.Resolver resolver)
        {
            char[] value = parse1(t, var);
            return resolver.resolve(value, StringBuilder.COPIER_CHAR_ARRAY);
        }

        private char[] parse1(T t, CharSequences.Var token)
        {
            ParameterMapping mapping = newParseMapping(token, parameterClass, typeHandlerRegistry);
            _t(t, mapping);
            return Z;
        }

        protected abstract void _t(T t, ParameterMapping mapping);
    }

    private class Parser extends BaseParser<SqlTagContext>
    {
        Parser(TypeHandlerRegistry typeHandlerRegistry, Class<?> parameterClass)
        {
            super(typeHandlerRegistry, parameterClass);
        }

        @Override
        protected void _t(SqlTagContext sqlTagContext, ParameterMapping mapping)
        {
            sqlTagContext.addParameterMapping(mapping);
        }
    }

    private class Parser1 extends BaseParser<List<ParameterMapping>>
    {
        Parser1(TypeHandlerRegistry typeHandlerRegistry, Class<?> parameterClass)
        {
            super(typeHandlerRegistry, parameterClass);
        }

        @Override
        protected void _t(List<ParameterMapping> parameterMappings, ParameterMapping mapping)
        {
            parameterMappings.add(mapping);
        }
    }
}