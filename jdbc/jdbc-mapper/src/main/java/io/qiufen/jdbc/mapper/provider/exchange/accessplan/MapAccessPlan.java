package io.qiufen.jdbc.mapper.provider.exchange.accessplan;

import java.util.Map;

/**
 * Access plan for working with Maps
 */
class MapAccessPlan extends BaseAccessPlan
{
    MapAccessPlan(Class<?> clazz, String[] propertyNames)
    {
        super(clazz, propertyNames);
    }

    public void setProperties(Object object, Object[] values)
    {
        Map map = (Map) object;
        for (int i = 0; i < propertyNames.length; i++)
        {
            map.put(propertyNames[i], values[i]);
        }
    }

    @Override
    public void setProperty(Object object, int index, Object value)
    {
        ((Map) object).put(propertyNames[index], value);
    }

    public Object[] getProperties(Object object)
    {
        Object[] values = new Object[propertyNames.length];
        Map map = (Map) object;
        for (int i = 0; i < propertyNames.length; i++)
        {
            values[i] = map.get(propertyNames[i]);
        }
        return values;
    }

    @Override
    public Object getProperty(Object object, int index)
    {
        return ((Map) object).get(propertyNames[index]);
    }
}
