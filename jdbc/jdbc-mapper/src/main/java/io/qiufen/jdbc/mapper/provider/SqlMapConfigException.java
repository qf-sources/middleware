package io.qiufen.jdbc.mapper.provider;

public class SqlMapConfigException extends RuntimeException
{
    public SqlMapConfigException(String message)
    {
        super(message);
    }

    public SqlMapConfigException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
