package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Short implementation of TypeHandler
 */
class ShortTypeHandler extends BaseTypeHandler implements TypeHandler
{

    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setShort(i, (Short) parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        short s = rs.getShort(columnName);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return s;
        }
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        short s = rs.getShort(columnIndex);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return s;
        }
    }

    public Object valueOf(String s)
    {
        return Short.valueOf(s);
    }

}
