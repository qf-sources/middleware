package io.qiufen.jdbc.mapper.spi.filter;

import io.qiufen.common.filter.Context;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

public class StatementScopeFilterContext implements Context
{
    private final StatementScope statementScope;
    private final Object parameterObject;

    public StatementScopeFilterContext(StatementScope statementScope, Object parameterObject)
    {
        this.statementScope = statementScope;
        this.parameterObject = parameterObject;
    }

    public StatementScope getStatementScope()
    {
        return statementScope;
    }

    public Object getParameterObject()
    {
        return parameterObject;
    }
}
