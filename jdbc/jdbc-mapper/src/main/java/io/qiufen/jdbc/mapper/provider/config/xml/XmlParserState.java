package io.qiufen.jdbc.mapper.provider.config.xml;

import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.common.resources.Resources;
import io.qiufen.jdbc.mapper.provider.config.ParameterMapConfig;
import io.qiufen.jdbc.mapper.provider.config.ResultMapConfig;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import org.w3c.dom.Node;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

public class XmlParserState
{
    private final Map<String, Node> sqlIncludes = new HashMap<String, Node>();

    private final SqlMapContext sqlMapContext;

    private Properties globalProps = new Properties();
    private boolean useStatementNamespaces = false;

    private ParameterMapConfig paramConfig;
    private ResultMapConfig resultConfig;

    private String namespace;

    public XmlParserState() throws SqlMapConfigException
    {
        sqlMapContext = new SqlMapContext();
    }

    public SqlMapContext getSqlMapContext()
    {
        return sqlMapContext;
    }

    public Properties getGlobalProps()
    {
        return globalProps;
    }

    public void setGlobalProps(Properties props)
    {
        globalProps = props;
    }

    public boolean isUseStatementNamespaces()
    {
        return useStatementNamespaces;
    }

    public void setUseStatementNamespaces(boolean useStatementNamespaces)
    {
        this.useStatementNamespaces = useStatementNamespaces;
    }

    public Map<String, Node> getSqlIncludes()
    {
        return sqlIncludes;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public String applyNamespace(String id)
    {
        String newId = id;
        if (namespace != null && namespace.length() > 0 && id != null && id.indexOf('.') < 0)
        {
            newId = namespace + "." + id;
        }
        return newId;
    }

    public ParameterMapConfig getParamConfig()
    {
        return paramConfig;
    }

    public void setParamConfig(ParameterMapConfig paramConfig)
    {
        this.paramConfig = paramConfig;
    }

    public ResultMapConfig getResultConfig()
    {
        return resultConfig;
    }

    public void setResultConfig(ResultMapConfig resultConfig)
    {
        this.resultConfig = resultConfig;
    }

    public String getFirstToken(String s)
    {
        return new StringTokenizer(s, ", ", false).nextToken();
    }

    public void setGlobalProperties(String resource, String url)
    {
        try
        {
            Properties props;
            if (resource != null)
            {
                props = Resources.getResourceAsProperties(resource);
            }
            else if (url != null)
            {
                props = Resources.getUrlAsProperties(url);
            }
            else
            {
                throw new RuntimeException(
                        "The " + "properties" + " element requires either a resource or a url attribute.");
            }

            // Merge properties with those passed in programmatically
            props.putAll(globalProps);
            globalProps = props;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Error loading properties.  Cause: " + e, e);
        }
    }
}
