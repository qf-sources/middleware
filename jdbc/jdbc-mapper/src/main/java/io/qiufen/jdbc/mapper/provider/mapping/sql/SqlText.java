package io.qiufen.jdbc.mapper.provider.mapping.sql;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.sql.simple.SimpleDynamicSql;

public final class SqlText implements SqlChild
{
    private final CTString text;
    private final boolean postParseRequired;

    private final ParameterMapping[] parameterMappings;

    private final boolean simpleDynamic;

    public SqlText(CTString text, boolean postParseRequired, ParameterMapping[] parameterMappings)
    {
        this.text = text;
        this.postParseRequired = postParseRequired;
        this.parameterMappings = parameterMappings;
        this.simpleDynamic = SimpleDynamicSql.isSimpleDynamicSql(text);
    }

    public CTString getText()
    {
        return text;
    }

    public ParameterMapping[] getParameterMappings()
    {
        return parameterMappings;
    }

    public boolean isPostParseRequired()
    {
        return postParseRequired;
    }

    public boolean isSimpleDynamic()
    {
        return simpleDynamic;
    }
}
