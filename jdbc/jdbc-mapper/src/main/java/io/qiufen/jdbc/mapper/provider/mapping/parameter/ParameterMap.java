package io.qiufen.jdbc.mapper.provider.mapping.parameter;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface ParameterMap
{
    CTString getId();

    Class<?> getParameterClass();

    int countParameterMapping();

    ParameterMapping getParameterMapping(int index);

    Object[] getParameterObjectValues(StatementScope statementScope, Object parameterObject);

    void setParameters(StatementScope statementScope, PreparedStatement ps, Object parameterObject) throws SQLException;

    VariableParameterMap toVar();
}
