package io.qiufen.jdbc.mapper.provider.session;

import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.provider.config.xml.SqlMapFacade;
import io.qiufen.jdbc.mapper.provider.session.context.DataSourceSessionContext;
import io.qiufen.jdbc.mapper.provider.session.context.SessionContext;
import io.qiufen.jdbc.mapper.provider.session.context.UserConnectionSessionContext;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/25 10:38
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class DefaultSessionFactory implements SessionFactory
{
    private final MappedStatementService mappedStatementService;

    private final SqlMapFacade sqlMapFacade;
    private final SessionContext sessionContext;

    DefaultSessionFactory(SqlMapFacade sqlMapFacade, DataSource dataSource)
    {
        this.sqlMapFacade = sqlMapFacade;
        this.sessionContext = new DataSourceSessionContext(dataSource);

        this.mappedStatementService = new MappedStatementService(sqlMapFacade, new DBExecutorService());
    }

    @Override
    public Session openSession()
    {
        return new DefaultSession(sessionContext, mappedStatementService);
    }

    @Override
    public Session openSession(Connection userConnection)
    {
        return new DefaultSession(new UserConnectionSessionContext(userConnection), mappedStatementService);
    }

    @Override
    public void destroy()
    {
        //todo
    }

    @Override
    public SqlMapFacade getSqlMapFacade()
    {
        return this.sqlMapFacade;
    }
}