package io.qiufen.jdbc.mapper.api.transaction;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/23 19:27
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public interface Transaction
{
    Transaction begin() throws TransactionException;

    Transaction commit() throws TransactionException;

    Transaction rollback() throws TransactionException;

    TransactionDefinition getDefinition();
}
