package io.qiufen.jdbc.mapper.provider.session;

public interface ExecutionLifecycle
{
    ExecutionLifecycle EMPTY = new ExecutionLifecycle()
    {
        @Override
        public void onBegin() {}

        @Override
        public void onSuccess() {}

        @Override
        public void onFailure(Exception e) {}
    };

    void onBegin();

    void onSuccess();

    void onFailure(Exception e);
}
