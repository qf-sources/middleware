package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.provider.common.utils.SimpleDateFormatter;
import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Date (and time) implementation of TypeHandler
 */
class DateTypeHandler extends BaseTypeHandler implements TypeHandler
{
    private static final String DATE_FORMAT = "yyyy/MM/dd hh:mm:ss";

    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setTimestamp(i, new java.sql.Timestamp(((Date) parameter).getTime()));
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        java.sql.Timestamp sqlTimestamp = rs.getTimestamp(columnName);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return new java.util.Date(sqlTimestamp.getTime());
        }
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        java.sql.Timestamp sqlTimestamp = rs.getTimestamp(columnIndex);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return new java.util.Date(sqlTimestamp.getTime());
        }
    }

    public Object valueOf(String s)
    {
        return SimpleDateFormatter.format(DATE_FORMAT, s);
    }

}
