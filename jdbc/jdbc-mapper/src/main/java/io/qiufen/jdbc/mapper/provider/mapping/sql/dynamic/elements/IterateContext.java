package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.jdbc.mapper.api.session.SessionException;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author Brandon Goodin
 */
public class IterateContext implements Iterator<Object>
{
    private final IterateContext parent;

    private final Iterator<?> iterator;
    private int index = -1;

    private String property;
    private boolean allowNext = true;

    private boolean isFinal = false;
    private SqlTag tag;

    /**
     * This variable is true if some of the sub elements have
     * actually produced content.  This is used to test
     * whether to add the open and conjunction text to the
     * generated statement.
     * <p/>
     * This variable is used to replace the deprecated and dangerous
     * isFirst method.
     */
    private boolean someSubElementsHaveContent;

    /**
     * This variable is set by the doEndFragment method in IterateTagHandler
     * to specify that the first content producing sub element has happened.
     * The doPrepend method will test the value to know whether or not
     * to process the prepend.
     * <p/>
     * This variable is used to replace the deprecated and dangerous
     * isFirst method.
     */
    private boolean isPrependEnabled;

    public IterateContext(Object collection, SqlTag tag, IterateContext parent)
    {
        this.parent = parent;
        this.tag = tag;
        if (collection instanceof Iterator)
        {
            this.iterator = (Iterator<?>) collection;
        }
        else if (collection instanceof Collection)
        {
            this.iterator = ((Collection<?>) collection).iterator();
        }
        else if (collection instanceof Object[])
        {
            this.iterator = Arrays.asList((Object[]) collection).iterator();
        }
        else
        {
            throw new SessionException("ParameterObject or property was not a Collection, Array or Iterator.");
        }
    }

    public boolean hasNext()
    {
        return iterator != null && iterator.hasNext();
    }

    public Object next()
    {
        index++;
        return iterator.next();
    }

    @Override
    public void remove() {}

    public int getIndex()
    {
        return index;
    }

    public boolean isLast()
    {
        return iterator != null && !iterator.hasNext();
    }

    /**
     * @return Returns the property.
     */
    public String getProperty()
    {
        return property;
    }

    /**
     * This property specifies whether to increment the iterate in
     * the doEndFragment. The ConditionalTagHandler has the ability
     * to increment the IterateContext, so it is neccessary to avoid
     * incrementing in both the ConditionalTag and the IterateTag.
     *
     * @param property The property to set.
     */
    public void setProperty(String property)
    {
        this.property = property;
    }

    /**
     * @return Returns the allowNext.
     */
    public boolean isAllowNext()
    {
        return allowNext;
    }

    /**
     * @param performIterate The allowNext to set.
     */
    public void setAllowNext(boolean performIterate)
    {
        this.allowNext = performIterate;
    }

    /**
     * @return Returns the tag.
     */
    public SqlTag getTag()
    {
        return tag;
    }

    /**
     * @param tag The tag to set.
     */
    public void setTag(SqlTag tag)
    {
        this.tag = tag;
    }

    public boolean isFinal()
    {
        return isFinal;
    }

    /**
     * This attribute is used to mark whether an iterate tag is
     * in it's final iteration. Since the ConditionalTagHandler
     * can increment the iterate the final iterate in the doEndFragment
     * of the IterateTagHandler needs to know it is in it's final iterate.
     */
    public void setFinal(boolean aFinal)
    {
        isFinal = aFinal;
    }

    /**
     * Returns the last property of any bean specified in this IterateContext.
     *
     * @return The last property of any bean specified in this IterateContext.
     */
    public String getEndProperty()
    {
        if (parent == null) return property;
        int parentPropertyIndex = property.indexOf(parent.getProperty());
        if (parentPropertyIndex <= -1) return property;
        int endPropertyIndex1 = property.indexOf(']', parentPropertyIndex);
        int endPropertyIndex2 = property.indexOf('.', parentPropertyIndex);
        return property.substring(parentPropertyIndex + Math.max(endPropertyIndex1, endPropertyIndex2) + 1);
    }

    /**
     * Replaces value of a tag property to match it's value with current iteration and all other iterations.
     *
     * @param tagProperty the property of a TagHandler.
     * @return A Map containing the modified tag property in PROCESS_STRING key and the index where the modification occured in PROCESS_INDEX key.
     */
    protected ProcessResult processTagProperty(StringBuilder tagProperty)
    {
        if (parent == null) return this.addIndex(tagProperty, 0);
        ProcessResult parentResult = parent.processTagProperty(tagProperty);
        return this.addIndex(parentResult.getInput(), parentResult.getIndex());
    }

    /**
     * Replaces value of a tag property to match it's value with current iteration and all other iterations.
     *
     * @param tagProperty the property of a TagHandler.
     * @return The tag property with all "[]" replaced with the correct iteration value.
     */
    public StringBuilder addIndexToTagProperty(StringBuilder tagProperty)
    {
        ProcessResult map = this.processTagProperty(tagProperty);
        return map.getInput();
    }

    /**
     * Adds index value to the first found property matching this Iteration starting at index startIndex.
     *
     * @param input      The input String.
     * @param startIndex The index where search for property begins.
     * @return A Map containing the modified tag property in PROCESS_STRING key and the index where the modification occured in PROCESS_INDEX key.
     */
    protected ProcessResult addIndex(StringBuilder input, int startIndex)
    {
        String endProperty = getEndProperty();
        int endPropertyLength = endProperty.length();
        int start = -1;
        if (endPropertyLength <= 0)
        {
            start = CharSequences.indexOf(input, '[', startIndex);
        }
        else
        {
            int lastIndex = input.length() - 2;
            int index = startIndex;
            while (index <= lastIndex)
            {
                index = input.indexOf(endProperty, index);
                if (index < 0) break;
                index += endPropertyLength;
                if (input.charAt(index) == '[')
                {
                    start = index;
                    break;
                }
            }
        }

        int modificationIndex = 0;
        // Is the iterate property in the tag property at all?
        if (start > -1)
        {
            // Make sure the tag property does not already have a number.
            int position = start + 1;
            if (input.charAt(position) == ']')
            {
                // Add iteration number to property.
                input.insert(position, this.getIndex());
                modificationIndex = position + 2;// 0]，至少增加2个字符
            }
        }
        return new ProcessResult(modificationIndex, input);
    }

    public boolean someSubElementsHaveContent()
    {
        return someSubElementsHaveContent;
    }

    public void setSomeSubElementsHaveContent(boolean someSubElementsHaveContent)
    {
        this.someSubElementsHaveContent = someSubElementsHaveContent;
    }

    public boolean isPrependEnabled()
    {
        return isPrependEnabled;
    }

    public void setPrependEnabled(boolean isPrependEnabled)
    {
        this.isPrependEnabled = isPrependEnabled;
    }
}

class ProcessResult
{
    private final int index;
    private final StringBuilder input;

    ProcessResult(int index, StringBuilder input)
    {
        this.index = index;
        this.input = input;
    }

    int getIndex()
    {
        return index;
    }

    StringBuilder getInput()
    {
        return input;
    }
}