package io.qiufen.jdbc.mapper.provider.config;

import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

/**
 * [功能描述]
 *
 * @author Forany
 * @date 2020/10/2 16:17
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class GeneratedKeyConfig
{
    private String property;
    private String column;
    private Class javaType;
    private TypeHandler typeHandler;

    public String getProperty()
    {
        return property;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public String getColumn()
    {
        return column;
    }

    public void setColumn(String column)
    {
        this.column = column;
    }

    public Class getJavaType()
    {
        return javaType;
    }

    public void setJavaType(Class javaType)
    {
        this.javaType = javaType;
    }

    public TypeHandler getTypeHandler()
    {
        return typeHandler;
    }

    public void setTypeHandler(TypeHandler typeHandler)
    {
        this.typeHandler = typeHandler;
    }
}
