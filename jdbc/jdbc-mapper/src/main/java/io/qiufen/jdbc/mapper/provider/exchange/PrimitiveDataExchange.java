package io.qiufen.jdbc.mapper.provider.exchange;

import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

/**
 * DataExchange implementation for primitive objects
 */
public class PrimitiveDataExchange extends BaseDataExchange implements DataExchange
{
    PrimitiveDataExchange(DataExchangeFactory dataExchangeFactory)
    {
        super(dataExchangeFactory);
    }

    public Object[] getData(StatementScope statementScope, ParameterMap parameterMap, Object parameterObject)
    {
        int len = parameterMap.countParameterMapping();
        Object[] data = new Object[len];
        Arrays.fill(data, parameterObject);
        return data;
    }

    @Override
    public Object setData(StatementScope statementScope, ResultMap resultMap, ResultSet rs) throws SQLException
    {
        return getResultMappingValue(statementScope, resultMap.getResultMappings()[0], rs);
    }

    @Override
    public void setData(StatementScope statementScope, ParameterMap parameterMap, PreparedStatement ps,
            Object parameterObject) throws SQLException
    {
        int len = parameterMap.countParameterMapping();
        for (int i = 0; i < len; i++)
        {
            setParameterValue(statementScope, parameterMap.getParameterMapping(i), ps, i + 1, parameterObject);
        }
    }
}
