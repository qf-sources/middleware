package io.qiufen.jdbc.mapper.provider.statement;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MappedStatement
{
    private Logger logger;

    private CTString id;
    private Sql sql;
    private Class<?> parameterClass;
    private ParameterMap parameterMap;
    private ResultMap resultMap;
    private int resultSetType;
    private Integer fetchSize;
    private int timeout;

    public StatementType getStatementType()
    {
        return StatementType.UNKNOWN;
    }

    public Logger getLogger()
    {
        return logger;
    }

    public CTString getId()
    {
        return id;
    }

    public void setId(CTString id)
    {
        this.id = id;
        logger = LoggerFactory.getLogger(this.id.toString());
    }

    public int getResultSetType()
    {
        return resultSetType;
    }

    public void setResultSetType(int resultSetType)
    {
        this.resultSetType = resultSetType;
    }

    public Integer getFetchSize()
    {
        return fetchSize == null ? Integer.valueOf(0) : fetchSize;
    }

    public void setFetchSize(Integer fetchSize)
    {
        this.fetchSize = fetchSize;
    }

    public Sql getSql()
    {
        return sql;
    }

    public void setSql(Sql sql)
    {
        this.sql = sql;
    }

    public ResultMap getResultMap()
    {
        return resultMap;
    }

    public void setResultMap(ResultMap resultMap)
    {
        this.resultMap = resultMap;
    }

    public ParameterMap getParameterMap()
    {
        return parameterMap;
    }

    public void setParameterMap(ParameterMap parameterMap)
    {
        this.parameterMap = parameterMap;
    }

    public Class<?> getParameterClass()
    {
        return parameterClass;
    }

    public void setParameterClass(Class<?> parameterClass)
    {
        this.parameterClass = parameterClass;
    }

    public StatementScope initRequest()
    {
        StatementScope statementScope = new StatementScope(this);
        statementScope.setParameterMap(parameterMap);
        if (resultMap != null)
        {
            statementScope.setResultMap(resultMap.prepare());
        }
        return statementScope;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }
}
