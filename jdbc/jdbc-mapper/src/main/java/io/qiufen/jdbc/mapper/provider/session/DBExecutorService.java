package io.qiufen.jdbc.mapper.provider.session;

import io.qiufen.common.lang.Strings;
import io.qiufen.jdbc.mapper.provider.scope.SessionScope;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;
import io.qiufen.jdbc.mapper.provider.statement.InsertStatement;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;
import org.slf4j.Logger;

import java.sql.*;
import java.util.*;

class DBExecutorService
{
    private static final int[] EMPTY = new int[0];

    int updateWithKeys(StatementScope statementScope, SessionScope sessionScope, String jdbcSql, Object parameterObject,
            ResultSetHandler resultSetHandler) throws SQLException
    {
        String columnLabel = null;
        MappedStatement mappedStatement = statementScope.getStatement();
        if (mappedStatement instanceof InsertStatement)
        {
            InsertStatement insertStatement = (InsertStatement) mappedStatement;
            columnLabel = insertStatement.getGeneratedKeyConfig().getColumn();
        }

        PreparedStatement statement = createKeysStatement(sessionScope.getConnection(), jdbcSql, columnLabel);
        try
        {
            initStatementParams(statementScope, statement, parameterObject);
            ExecutionLifecycle executionLifecycle = statementScope.getExecutionLifecycle();
            executionLifecycle.onBegin();

            int rows;
            ResultSet resultSet;
            try
            {
                rows = statement.executeUpdate();
                resultSet = statement.getGeneratedKeys();
            }
            catch (SQLException e)
            {
                executionLifecycle.onFailure(e);
                throw e;
            }

            try
            {
                executionLifecycle.onSuccess();
                statementScope.getResultValue().onValue(rows);
                resultSetHandler.handle(resultSet);
            }
            finally
            {
                closeQuietly(statementScope, resultSet);
            }
            return rows;
        }
        finally
        {
            closeQuietly(statementScope, statement);
        }
    }

    int update(StatementScope statementScope, SessionScope sessionScope, String jdbcSql, Object parameterObject)
            throws SQLException
    {
        PreparedStatement statement = createStatement(sessionScope.getConnection(), jdbcSql);
        initStatementParams(statementScope, statement, parameterObject);
        ExecutionLifecycle executionLifecycle = statementScope.getExecutionLifecycle();
        executionLifecycle.onBegin();

        try
        {
            int rows;
            try
            {
                rows = statement.executeUpdate();
            }
            catch (SQLException e)
            {
                executionLifecycle.onFailure(e);
                throw e;
            }

            executionLifecycle.onSuccess();
            statementScope.getResultValue().onValue(rows);
            return rows;
        }
        finally
        {
            closeQuietly(statementScope, statement);
        }
    }

    void select(StatementScope statementScope, SessionScope sessionScope, String jdbcSql, Object parameterObject,
            ResultSetHandler resultSetHandler) throws SQLException
    {
        PreparedStatement statement = createStatement(sessionScope.getConnection(), jdbcSql);
        try
        {
            initStatementParams(statementScope, statement, parameterObject);
            ExecutionLifecycle executionLifecycle = statementScope.getExecutionLifecycle();
            executionLifecycle.onBegin();

            ResultSet resultSet;
            try
            {
                resultSet = statement.executeQuery();
            }
            catch (SQLException e)
            {
                executionLifecycle.onFailure(e);
                throw e;
            }

            try
            {
                executionLifecycle.onSuccess();
                resultSetHandler.handle(resultSet);
            }
            finally
            {
                closeQuietly(statementScope, resultSet);
            }
        }
        finally
        {
            closeQuietly(statementScope, statement);
        }
    }

    KeysStatementBatch createBatchWithKeys(SessionScope sessionScope, String columnLabel)
    {
        return new PrivateKeysStatementBatch(sessionScope, columnLabel);
    }

    CommonStatementBatch createBatch(SessionScope sessionScope)
    {
        return new PrivateCommonStatementBatch(sessionScope);
    }

    private PreparedStatement createStatement(Connection conn, String sql) throws SQLException
    {
        return conn.prepareStatement(sql);
    }

    private PreparedStatement createKeysStatement(Connection conn, String sql, String columnLabel) throws SQLException
    {
        if (Strings.isEmpty(columnLabel))
        {
            return conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        }

        return conn.prepareStatement(sql, new String[]{columnLabel});
    }

    private void initStatementParams(StatementScope statementScope, PreparedStatement statement, Object parameter)
            throws SQLException
    {
        MappedStatement mappedStatement = statementScope.getStatement();
        statement.setQueryTimeout(mappedStatement.getTimeout());
        setParams(statementScope, statement, parameter);
    }

    private void setParams(StatementScope statementScope, PreparedStatement statement, Object parameter)
            throws SQLException
    {
        statementScope.getParameterMap().setParameters(statementScope, statement, parameter);
    }

    private void closeQuietly(StatementScope statementScope, PreparedStatement statement)
    {
        Logger logger = statementScope.getStatement().getLogger();
        try
        {
            if (statement == null || statement.isClosed()) return;

            if (logger.isDebugEnabled()) logger.debug("Close statement");
            statement.close();
        }
        catch (Exception e)
        {
            logger.error(e.toString(), e);
        }
    }

    private void closeQuietly(StatementScope statementScope, ResultSet resultSet)
    {
        if (resultSet == null)
        {
            return;
        }
        try
        {
            if (resultSet.isClosed())
            {
                return;
            }
            resultSet.close();
        }
        catch (Exception e)
        {
            statementScope.getStatement().getLogger().error(e.toString(), e);
        }
    }

    private abstract class BaseStatementBatch implements StatementBatch
    {
        final Map<String, PSBatch> batches = new HashMap<String, PSBatch>();
        final List<Integer> rows = new ArrayList<Integer>();

        @Override
        public void close()
        {
            Iterator<Map.Entry<String, PSBatch>> iterator = this.batches.entrySet().iterator();
            while (iterator.hasNext())
            {
                Map.Entry<String, PSBatch> entry = iterator.next();
                PSBatch batch = entry.getValue();
                closeQuietly(batch.getStatementScope(), batch.getPs());
                iterator.remove();
            }
            this.rows.clear();
        }
    }

    private class PrivateCommonStatementBatch extends BaseStatementBatch implements CommonStatementBatch
    {
        private final SessionScope sessionScope;

        private PrivateCommonStatementBatch(SessionScope sessionScope)
        {
            this.sessionScope = sessionScope;
        }

        @Override
        public void addStatement(StatementScope statementScope, String jdbcSql, Object parameter) throws SQLException
        {
            PSBatch batch = this.batches.get(jdbcSql);
            if (batch == null)
            {
                PreparedStatement statement = createStatement(sessionScope.getConnection(), jdbcSql);
                initStatementParams(statementScope, statement, parameter);
                statement.addBatch();
                batch = new PSBatch(statementScope, statement);
                this.batches.put(jdbcSql, batch);
            }
            else
            {
                PreparedStatement statement = batch.getPs();
                DBExecutorService.this.setParams(statementScope, statement, parameter);
                statement.addBatch();
            }

            batch.getGlobalIndexes().add(this.rows.size());
            this.rows.add(0);
        }

        @Override
        public int[] execute() throws SQLException
        {
            if (batches.isEmpty()) return EMPTY;

            for (Map.Entry<String, PSBatch> entry : batches.entrySet())
            {
                PSBatch batch = entry.getValue();
                PreparedStatement statement = batch.getPs();
                StatementScope statementScope = batch.getStatementScope();
                ExecutionLifecycle executionLifecycle = statementScope.getExecutionLifecycle();
                executionLifecycle.onBegin();

                int[] rows;
                try
                {
                    rows = statement.executeBatch();
                }
                catch (SQLException e)
                {
                    executionLifecycle.onFailure(e);
                    throw e;
                }

                executionLifecycle.onSuccess();
                statementScope.getResultValue().onValue(rows);

                List<Integer> globalIndexes = batch.getGlobalIndexes();
                for (int i = 0; i < globalIndexes.size(); i++)
                {
                    this.rows.set(globalIndexes.get(i), rows[i]);
                }
            }

            int len = this.rows.size();
            int[] _rows = new int[len];
            for (int i = 0; i < len; i++)
            {
                _rows[i] = this.rows.get(i);
            }
            return _rows;
        }
    }

    private class PrivateKeysStatementBatch extends BaseStatementBatch implements KeysStatementBatch
    {
        private final SessionScope sessionScope;
        private final String columnLabel;

        private PrivateKeysStatementBatch(SessionScope sessionScope, String columnLabel)
        {
            this.sessionScope = sessionScope;
            this.columnLabel = columnLabel;
        }

        @Override
        public void addStatement(StatementScope statementScope, String jdbcSql, Object parameter,
                ResultSetHandler handler) throws SQLException
        {
            PSBatch batch = this.batches.get(jdbcSql);
            if (batch == null)
            {
                PreparedStatement statement = createKeysStatement(sessionScope.getConnection(), jdbcSql, columnLabel);
                initStatementParams(statementScope, statement, parameter);
                statement.addBatch();
                batch = new PSBatch(statementScope, statement);
                this.batches.put(jdbcSql, batch);
            }
            else
            {
                PreparedStatement statement = batch.getPs();
                DBExecutorService.this.setParams(statementScope, statement, parameter);
                statement.addBatch();
            }
            batch.getGlobalIndexes().add(this.rows.size());
            this.rows.add(0);
            batch.addResultSetHandler(handler);
        }

        @Override
        public int[] execute() throws SQLException
        {
            if (batches.isEmpty()) return EMPTY;

            for (Map.Entry<String, PSBatch> entry : batches.entrySet())
            {
                PSBatch batch = entry.getValue();
                PreparedStatement statement = batch.getPs();
                StatementScope statementScope = batch.getStatementScope();
                ExecutionLifecycle executionLifecycle = statementScope.getExecutionLifecycle();
                executionLifecycle.onBegin();

                int[] rows;
                ResultSet resultSet;
                try
                {
                    rows = statement.executeBatch();
                    resultSet = statement.getGeneratedKeys();
                }
                catch (SQLException e)
                {
                    executionLifecycle.onFailure(e);
                    throw e;
                }

                try
                {
                    executionLifecycle.onSuccess();
                    statementScope.getResultValue().onValue(rows);
                    Iterator<ResultSetHandler> iterator = batch.getHandlers().iterator();
                    if (iterator.hasNext())
                    {
                        iterator.next().handle(resultSet);
                    }
                }
                finally
                {
                    closeQuietly(batch.getStatementScope(), resultSet);
                }

                List<Integer> globalIndexes = batch.getGlobalIndexes();
                for (int i = 0; i < globalIndexes.size(); i++)
                {
                    this.rows.set(globalIndexes.get(i), rows[i]);
                }
            }

            int len = this.rows.size();
            int[] _rows = new int[len];
            for (int i = 0; i < len; i++)
            {
                _rows[i] = this.rows.get(i);
            }
            return _rows;
        }
    }
}
