package io.qiufen.jdbc.mapper.provider.mapping.sql.simple;

import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

public class VariableSimpleDynamicSql implements Sql
{
    private final StringBuilder sqlStatement;
    private final SqlMapContext sqlMapContext;

    public VariableSimpleDynamicSql(SqlMapContext sqlMapContext, StringBuilder sqlStatement)
    {
        this.sqlStatement = sqlStatement;
        this.sqlMapContext = sqlMapContext;
    }

    public static void processDynamicElements(SqlMapContext sqlMapContext, StringBuilder sqlStatement,
            Object parameterObject)
    {
        CharSequences.replaceArgs(sqlStatement, SimpleDynamicSql.SYMBOL, SimpleDynamicSql.SYMBOL, parameterObject,
                sqlMapContext.getDynamicElementParser());
    }

    @Override
    public void applyParameter(StatementScope statementScope, Object parameterObject)
    {
        processDynamicElements(sqlMapContext, sqlStatement, parameterObject);
        statementScope.setSql(sqlStatement);
    }
}