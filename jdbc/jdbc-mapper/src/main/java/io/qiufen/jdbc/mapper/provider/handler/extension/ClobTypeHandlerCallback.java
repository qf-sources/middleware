package io.qiufen.jdbc.mapper.provider.handler.extension;

import io.qiufen.jdbc.mapper.spi.handler.extension.ParameterSetter;
import io.qiufen.jdbc.mapper.spi.handler.extension.ResultGetter;
import io.qiufen.jdbc.mapper.spi.handler.extension.TypeHandlerCallback;

import java.io.StringReader;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Types;

public class ClobTypeHandlerCallback implements TypeHandlerCallback
{
    public Object getResult(ResultGetter getter) throws SQLException
    {
        String value;
        Clob clob = getter.getClob();
        if (!getter.wasNull())
        {
            int size = (int) clob.length();
            value = clob.getSubString(1, size);
        }
        else
        {
            value = null;
        }

        return value;
    }

    public void setParameter(ParameterSetter setter, Object parameter) throws SQLException
    {
        String s = (String) parameter;
        if (s != null)
        {
            StringReader reader = new StringReader(s);
            setter.setCharacterStream(reader, s.length());
        }
        else
        {
            setter.setNull(Types.CLOB);
        }
    }

    public Object valueOf(String s)
    {
        return s;
    }

}
