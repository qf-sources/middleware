package io.qiufen.jdbc.mapper.provider.config;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.handler.TypeHandlerRegistry;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.DefaultParameterMapBuilder;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.result.AutoResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.OnceRemappingResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.RemappingResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.mapping.sql.VariableSqlText;
import io.qiufen.jdbc.mapper.provider.mapping.sql._static.StaticSql;
import io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.DynamicSql;
import io.qiufen.jdbc.mapper.provider.mapping.sql.raw.RawSql;
import io.qiufen.jdbc.mapper.provider.mapping.sql.simple.SimpleDynamicSql;
import io.qiufen.jdbc.mapper.provider.statement.InsertStatement;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.ResultSet;
import java.util.List;

public class MappedStatementConfig
{
    private final InlineParameterMapParser inlineParameterMapParser = new InlineParameterMapParser();

    private final SqlMapContext sqlMapContext;
    private final TypeHandlerRegistry typeHandlerRegistry;
    private final MappedStatement mappedStatement;
    private final MappedStatement rootStatement;

    MappedStatementConfig(SqlMapContext sqlMapContext, CTString id, MappedStatement statement, SqlSource processor,
            String parameterMapName, Class<?> parameterClass, CTString resultMapName, Class<?> resultClass,
            String resultSetType, Integer fetchSize, boolean allowRemapping, Integer timeout,
            Integer defaultStatementTimeout) throws SqlMapConfigException
    {
        this.sqlMapContext = sqlMapContext;
        this.typeHandlerRegistry = sqlMapContext.getTypeHandlerRegistry();
        statement.setId(id);

        // set parameter class either from attribute or from map (make sure to match)
        if (parameterMapName == null)
        {
            statement.setParameterClass(parameterClass);
        }
        else
        {
            ParameterMap parameterMap = this.sqlMapContext.getParameterMap(CTString.wrap(parameterMapName));
            statement.setParameterMap(parameterMap);
            statement.setParameterClass(parameterMap.getParameterClass());
        }

        // process SQL statement, including inline parameter maps
        Sql sql = processor.getSql(statement);
        setSqlForStatement(statement, sql);

        if (resultMapName == null)
        {
            if (resultClass != null)
            {
                AutoResultMap resultMap;
                if (allowRemapping)
                {
                    resultMap = new RemappingResultMap(sqlMapContext);
                }
                else
                {
                    resultMap = new OnceRemappingResultMap(sqlMapContext);
                }
                resultMap.setId(CTString.of(statement.getId(), "-AutoResultMap"));
                resultMap.setResultClass(resultClass);
                statement.setResultMap(resultMap);
            }
        }
        else
        {
            statement.setResultMap(this.sqlMapContext.getResultMap(resultMapName));
        }

        if (resultSetType != null)
        {
            if ("FORWARD_ONLY".equals(resultSetType))
            {
                statement.setResultSetType(ResultSet.TYPE_FORWARD_ONLY);
            }
            else if ("SCROLL_INSENSITIVE".equals(resultSetType))
            {
                statement.setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE);
            }
            else if ("SCROLL_SENSITIVE".equals(resultSetType))
            {
                statement.setResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);
            }
        }
        if (fetchSize != null)
        {
            statement.setFetchSize(fetchSize);
        }
        statement.setTimeout(defaultStatementTimeout == null ? 0 : defaultStatementTimeout);
        if (timeout != null)
        {
            statement.setTimeout(timeout);
        }
        mappedStatement = statement;
        rootStatement = statement;
        this.sqlMapContext.addMappedStatement(mappedStatement);
    }

    public void setSelectKeyStatement(String property, String column, Class<?> javaType) throws SqlMapConfigException
    {
        if (rootStatement instanceof InsertStatement)
        {
            InsertStatement insertStatement = ((InsertStatement) rootStatement);
            GeneratedKeyConfig config = new GeneratedKeyConfig();
            config.setProperty(property);
            config.setColumn(column);
            config.setJavaType(javaType);
            TypeHandler typeHandler = typeHandlerRegistry.getTypeHandler(config.getJavaType());
            if (typeHandler == null)
            {
                typeHandler = typeHandlerRegistry.getUnkownTypeHandler();
            }
            config.setTypeHandler(typeHandler);
            insertStatement.setGeneratedKeyConfig(config);
        }
        else
        {
            throw new SqlMapConfigException("It is not an InsertStatement.");
        }
    }

    private void setSqlForStatement(MappedStatement statement, Sql sql)
    {
        if (sql instanceof DynamicSql)
        {
            statement.setSql(sql);
        }
        else
        {
            applyInlineParameterMap(statement, ((RawSql) sql).getSql());
        }
    }

    private void applyInlineParameterMap(MappedStatement statement, CTString sqlStatement)
    {
        CharSequence charSequence = sqlStatement;
        if (statement.getParameterMap() == null)
        {
            DefaultParameterMapBuilder builder = new DefaultParameterMapBuilder(sqlMapContext).setId(
                            CTString.of(statement.getId(), "-InlineParameterMap"))
                    .setParameterClass(statement.getParameterClass());
            VariableSqlText variableSqlText = inlineParameterMapParser.parseInlineParameterMap(
                    sqlMapContext.getTypeHandlerRegistry(), sqlStatement, statement.getParameterClass());
            charSequence = variableSqlText.getText();
            List<ParameterMapping> parameterMappingList = variableSqlText.getParameterMappingList();
            if (CollectionUtil.isNotEmpty(parameterMappingList))
            {
                ParameterMapping[] parameterMappings = new ParameterMapping[parameterMappingList.size()];
                parameterMappingList.toArray(parameterMappings);
                builder.setParameterMappings(parameterMappings);
            }
            statement.setParameterMap(builder.build());
        }
        //非动态sql，可将收尾空白字符去除
        charSequence = charSequence.toString().trim();
        Sql sql;
        if (SimpleDynamicSql.isSimpleDynamicSql(charSequence))
        {
            sql = new SimpleDynamicSql(sqlMapContext, CTString.of(charSequence));
        }
        else
        {
            sql = new StaticSql(CTString.of(charSequence));
        }
        statement.setSql(sql);
    }

    public MappedStatement getMappedStatement()
    {
        return mappedStatement;
    }
}
