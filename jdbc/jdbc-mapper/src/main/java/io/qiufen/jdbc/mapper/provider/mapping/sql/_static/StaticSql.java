package io.qiufen.jdbc.mapper.provider.mapping.sql._static;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

public final class StaticSql implements Sql
{
    private final CTString sqlStatement;

    public StaticSql(CTString sqlStatement)
    {
        this.sqlStatement = sqlStatement;
    }

    @Override
    public void applyParameter(StatementScope statementScope, Object parameterObject)
    {
        statementScope.setSql(this.sqlStatement);
    }
}
