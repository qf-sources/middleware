package io.qiufen.jdbc.mapper.provider.exchange.accessplan;

import java.util.Map;

/**
 * Factory to get an accesss plan appropriate for an object
 */
public class AccessPlanFactory
{
    private AccessPlanFactory() {}

    /**
     * Creates an access plan for working with a bean
     *
     * @return An access plan
     */
    public static AccessPlan getAccessPlan(Class<?> clazz, String[] propertyNames)
    {
        AccessPlan plan;
        if (isComplex(clazz, propertyNames))
        {
            plan = new ComplexAccessPlan(clazz, propertyNames);
        }
        else if (Map.class.isAssignableFrom(clazz))
        {
            plan = new MapAccessPlan(clazz, propertyNames);
        }
        else
        {
            try
            {
                plan = new PropertyAccessPlan(clazz, propertyNames);
            }
            catch (Throwable t)
            {
                plan = new ComplexAccessPlan(clazz, propertyNames);
            }
        }
        return plan;
    }

    private static boolean isComplex(Class<?> clazz, String[] propertyNames)
    {
        if (clazz == null || propertyNames == null || propertyNames.length == 0)
        {
            return true;
        }
        for (String propertyName : propertyNames)
        {
            if (propertyName.indexOf('[') > -1 || propertyName.indexOf('.') > -1)
            {
                return true;
            }
        }
        return false;
    }
}
