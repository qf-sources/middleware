package io.qiufen.jdbc.mapper.provider.statement;

import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

public class UpdateStatement extends MappedStatement
{
    public StatementType getStatementType()
    {
        return StatementType.UPDATE;
    }

    @Override
    public StatementScope initRequest()
    {
        StatementScope statementScope = new StatementScope(this);
        statementScope.setParameterMap(getParameterMap());
        return statementScope;
    }
}

