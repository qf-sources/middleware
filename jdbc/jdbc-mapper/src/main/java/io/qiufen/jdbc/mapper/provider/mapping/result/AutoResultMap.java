package io.qiufen.jdbc.mapper.provider.mapping.result;

import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.common.beans.ClassInfo;
import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

/**
 * An automatic result map for simple stuff
 */
public class AutoResultMap extends ResultMap
{
    AutoResultMap(SqlMapContext sqlMapContext)
    {
        super(sqlMapContext);
    }

    void initialize(ResultSet rs)
    {
        if (getResultClass() == null)
        {
            throw new SessionException(
                    "The automatic ResultMap named " + this.getId() + " had a null result class (not allowed).");
        }
        else if (Map.class.isAssignableFrom(getResultClass()))
        {
            initializeMapResults(rs);
        }
        else if (getSqlMapContext().getTypeHandlerRegistry().getTypeHandler(getResultClass()) != null)
        {
            initializePrimitiveResults(rs);
        }
        else
        {
            initializeBeanResults(rs);
        }
    }

    private void initializeBeanResults(ResultSet rs)
    {
        try
        {
            ClassInfo classInfo = ClassInfo.getInstance(getResultClass());
            String[] propertyNames = classInfo.getWriteablePropertyNames();

            Map<String, String> propertyMap = new HashMap<String, String>();
            for (String propertyName : propertyNames)
            {
                propertyMap.put(propertyName.toUpperCase(Locale.ENGLISH), propertyName);
            }

            List<ResultMapping> resultMappingList = new ArrayList<ResultMapping>();
            ResultSetMetaData rsmd = rs.getMetaData();
            Probe probe = ProbeFactory.getProbe(this.getResultClass());
            for (int i = 0, n = rsmd.getColumnCount(); i < n; i++)
            {
                String columnName = getColumnIdentifier(rsmd, i + 1);
                String upperColumnName = columnName.toUpperCase(java.util.Locale.ENGLISH);
                String matchedProp = propertyMap.get(upperColumnName);
                Class<?> type;
                if (matchedProp == null)
                {
                    type = probe.getPropertyTypeForSetter(this.getResultClass(), columnName);
                }
                else
                {
                    type = classInfo.getSetterType(matchedProp);
                }
                if (type != null || matchedProp != null)
                {
                    ResultMapping resultMapping = new ResultMapping();
                    resultMapping.setPropertyName(matchedProp != null ? matchedProp : columnName);
                    resultMapping.setColumnName(columnName);
                    resultMapping.setColumnIndex(i + 1);
                    resultMapping.setTypeHandler(
                            getSqlMapContext().getTypeHandlerRegistry().getTypeHandler(type)); //map SQL to JDBC type
                    resultMappingList.add(resultMapping);
                }
            }
            setResultMappingList(resultMappingList);
            resultMappingList.clear();
        }
        catch (Exception e)
        {
            throw new SessionException("Error automapping columns. Cause: " + e);
        }
    }

    private void initializeMapResults(ResultSet rs)
    {
        try
        {
            ResultSetMetaData rsmd = rs.getMetaData();
            int n = rsmd.getColumnCount();
            ResultMapping[] resultMappingList = new ResultMapping[n];
            for (int i = 0; i < n; i++)
            {
                String columnName = getColumnIdentifier(rsmd, i + 1);
                ResultMapping resultMapping = new ResultMapping();
                resultMapping.setPropertyName(columnName);
                resultMapping.setColumnName(columnName);
                resultMapping.setColumnIndex(i + 1);
                resultMapping.setTypeHandler(getSqlMapContext().getTypeHandlerRegistry().getTypeHandler(Object.class));
                resultMappingList[i] = resultMapping;
            }
            this.setResultMappings(resultMappingList);
        }
        catch (Exception e)
        {
            throw new SessionException("Error automapping columns. Cause: " + e);
        }
    }

    private void initializePrimitiveResults(ResultSet rs)
    {
        try
        {
            ResultSetMetaData rsmd = rs.getMetaData();
            String columnName = getColumnIdentifier(rsmd, 1);
            ResultMapping resultMapping = new ResultMapping();
            resultMapping.setPropertyName(columnName);
            resultMapping.setColumnName(columnName);
            resultMapping.setColumnIndex(1);
            resultMapping.setTypeHandler(getSqlMapContext().getTypeHandlerRegistry().getTypeHandler(getResultClass()));
            setResultMappings(new ResultMapping[]{resultMapping});
        }
        catch (Exception e)
        {
            throw new SessionException("Error automapping columns. Cause: " + e);
        }
    }

    private String getColumnIdentifier(ResultSetMetaData rsmd, int i) throws SQLException
    {
        return sqlMapContext.isUseColumnLabel() ? rsmd.getColumnLabel(i) : rsmd.getColumnName(i);
    }
}

