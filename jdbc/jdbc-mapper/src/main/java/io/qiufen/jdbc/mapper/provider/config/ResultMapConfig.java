package io.qiufen.jdbc.mapper.provider.config;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.handler.extension.CustomTypeHandler;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMapping;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.extension.TypeHandlerCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ResultMapConfig
{
    private final SqlMapContext sqlMapContext;
    private final ResultMap resultMap;
    private final List<ResultMapping> resultMappingList;
    private int resultMappingIndex;

    ResultMapConfig(SqlMapContext sqlMapContext, CTString id, Class<?> resultClass, CTString extendsResultMap)
            throws SqlMapConfigException
    {
        this.sqlMapContext = sqlMapContext;
        this.resultMap = new ResultMap(sqlMapContext);
        this.resultMappingList = new ArrayList<ResultMapping>();
        resultMap.setId(id);
        resultMap.setResultClass(resultClass);
        if (extendsResultMap != null)
        {
            ResultMap extendedResultMap = sqlMapContext.getResultMap(extendsResultMap);
            ResultMapping[] resultMappings = extendedResultMap.getResultMappings();
            Collections.addAll(resultMappingList, resultMappings);
        }
        resultMappingIndex = resultMappingList.size();
    }

    public void addResultMapping(String propertyName, String columnName, Integer columnIndex, Class<?> javaClass,
            String jdbcType, String nullValue, String notNullColumn, Object impl)
    {
        TypeHandler handler;
        if (impl != null)
        {
            if (impl instanceof TypeHandlerCallback)
            {
                handler = new CustomTypeHandler((TypeHandlerCallback) impl);
            }
            else if (impl instanceof TypeHandler)
            {
                handler = (TypeHandler) impl;
            }
            else
            {
                throw new SqlMapConfigException(
                        "The class '" + impl + "' is not a valid implementation of TypeHandler or TypeHandlerCallback");
            }
        }
        else
        {
            handler = sqlMapContext.resolveTypeHandler(resultMap.getResultClass(), propertyName, javaClass, jdbcType,
                    true);
        }

        if (handler == null)
        {
            throw new SqlMapConfigException(
                    "No type handler could be found to map the property '" + propertyName + "' to the column '" + columnName + "'.  One or both of the types, or the combination of types is not supported.");
        }

        ResultMapping mapping = new ResultMapping();
        mapping.setPropertyName(propertyName);
        mapping.setColumnName(columnName);
        mapping.setJdbcTypeName(jdbcType);
        mapping.setTypeHandler(handler);
        mapping.setNullValue(nullValue);
        mapping.setNotNullColumn(notNullColumn);
        mapping.setJavaType(javaClass);
        if (columnIndex == null)
        {
            resultMappingIndex++;
            mapping.setColumnIndex(resultMappingIndex);
        }
        else
        {
            mapping.setColumnIndex(columnIndex);
        }
        resultMappingList.add(mapping);
    }

    public void finish()
    {
        resultMap.setResultMappingList(resultMappingList);
        sqlMapContext.addResultMap(resultMap);

        resultMappingList.clear();
    }
}
