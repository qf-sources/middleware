package io.qiufen.jdbc.mapper.provider.config.xml;

import io.qiufen.common.filter.FilterRegistry;
import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.api.session.MSNotFoundException;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletException;
import io.qiufen.jdbc.mapper.provider.config.*;
import io.qiufen.jdbc.mapper.provider.handler.TypeHandlerRegistry;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;

import java.io.IOException;
import java.net.URL;

public abstract class SqlMapFacade
{
    private final SqlMapContext context;

    SqlMapFacade(SqlMapContext context)
    {
        this.context = context;
    }

    public void setDefaultStatementTimeout(int defaultTimeout)
    {
        this.context.setDefaultStatementTimeout(defaultTimeout);
    }

    public ParameterMapConfig newParameterMapConfig(CTString id, Class<?> parameterClass)
    {
        return this.context.newParameterMapConfig(id, parameterClass);
    }

    public ResultMapConfig newResultMapConfig(CTString id, Class<?> resultClass, CTString extended)
            throws SqlMapConfigException
    {
        return this.context.newResultMapConfig(id, resultClass, extended);
    }

    public MappedStatementConfig newMappedStatementConfig(CTString id, MappedStatement statement, SqlSource processor,
            String parameterMapName, Class<?> parameterClass, CTString resultMapName, Class<?> resultClass,
            String resultSetType, Integer fetchSize, boolean allowRemapping, Integer timeout)
            throws SqlMapConfigException
    {
        return this.context.newMappedStatementConfig(id, statement, processor, parameterMapName, parameterClass,
                resultMapName, resultClass, resultSetType, fetchSize, allowRemapping, timeout);
    }

    public void finalizeSqlMapConfig()
    {
        this.context.finalizeSqlMapConfig();
    }

    /**
     * Getter for the TypeHandlerFactory
     *
     * @return - the TypeHandlerFactory
     */
    public TypeHandlerRegistry getTypeHandlerRegistry()
    {
        return this.context.getTypeHandlerRegistry();
    }

    public boolean isUseColumnLabel()
    {
        return this.context.isUseColumnLabel();
    }

    public void setUseColumnLabel(boolean useColumnLabel)
    {
        this.context.setUseColumnLabel(useColumnLabel);
    }

    /**
     * Get a result map by ID
     *
     * @param id - the ID
     * @return - the result map
     */
    public ResultMap getResultMap(CTString id) throws SqlMapConfigException
    {
        return this.context.getResultMap(id);
    }

    /**
     * Get a parameter map by ID
     *
     * @param id - the ID
     * @return - the parameter map
     */
    public ParameterMap getParameterMap(CTString id) throws SqlMapConfigException
    {
        return this.context.getParameterMap(id);
    }

    public MappedStatement getMappedStatement(CTString id) throws MSNotFoundException
    {
        return this.context.getMappedStatement(id);
    }

    public FilterRegistry getFilterRegistry()
    {
        return context.getFilterRegistry();
    }

    public abstract void addSqlMap(URL url) throws NodeletException, IOException;
}
