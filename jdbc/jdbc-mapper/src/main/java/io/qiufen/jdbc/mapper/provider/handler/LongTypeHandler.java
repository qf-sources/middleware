package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Long implementation of TypeHandler
 */
class LongTypeHandler extends BaseTypeHandler implements TypeHandler
{
    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setLong(i, (Long) parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        long l = rs.getLong(columnName);
        return rs.wasNull() ? null : l;
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        long l = rs.getLong(columnIndex);
        return rs.wasNull() ? null : l;
    }

    public Object valueOf(String s)
    {
        return Long.valueOf(s);
    }

}
