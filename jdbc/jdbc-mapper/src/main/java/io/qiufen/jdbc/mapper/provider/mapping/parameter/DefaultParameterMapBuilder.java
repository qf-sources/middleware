package io.qiufen.jdbc.mapper.provider.mapping.parameter;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;

public class DefaultParameterMapBuilder
{
    private final SqlMapContext sqlMapContext;
    private CTString id;
    private Class<?> parameterClass;
    private ParameterMapping[] parameterMappings;

    public DefaultParameterMapBuilder(SqlMapContext sqlMapContext)
    {
        this.sqlMapContext = sqlMapContext;
    }

    public DefaultParameterMapBuilder setId(CTString id)
    {
        this.id = id;
        return this;
    }

    public DefaultParameterMapBuilder setParameterClass(Class<?> parameterClass)
    {
        this.parameterClass = parameterClass;
        return this;
    }

    public DefaultParameterMapBuilder setParameterMappings(ParameterMapping[] parameterMappings)
    {
        this.parameterMappings = parameterMappings;
        return this;
    }

    public DefaultParameterMap build()
    {
        DefaultParameterMap map = new DefaultParameterMap(sqlMapContext);
        map.setId(id);
        map.setParameterClass(parameterClass);
        map.setParameterMappings(parameterMappings);
        return map;
    }
}
