package io.qiufen.jdbc.mapper.provider.config.xml;

import io.qiufen.common.lang.Strings;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.common.resources.Resources;
import io.qiufen.jdbc.mapper.provider.common.xml.Nodelet;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletException;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletParser;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletUtils;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.Properties;

public class SqlMapConfigParser
{
    private static final Logger log = LoggerFactory.getLogger(SqlMapConfigParser.class);

    protected final NodeletParser parser = new NodeletParser();
    private final XmlParserState state;

    private boolean usingStreams = false;

    public SqlMapConfigParser() throws SqlMapConfigException
    {
        parser.setValidation(true);
        parser.setEntityResolver(new SqlMapClasspathEntityResolver());
        state = new XmlParserState();

        addSqlMapConfigNodelets();
        addGlobalPropNodelets();
        addSettingsNodelets();
        addTypeAliasNodelets();
        addTypeHandlerNodelets();
        addSqlMapNodelets();
        addResultObjectFactoryNodelets();
    }

    public SqlMapFacade parse(Reader reader, Properties props)
    {
        if (props != null)
        {
            state.setGlobalProps(props);
        }
        return parse(reader);
    }

    public SqlMapFacade parse(Reader reader)
    {
        try
        {
            usingStreams = false;
            parser.parse(reader);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Error occurred.  Cause: " + e, e);
        }
        return createFacade();
    }

    public SqlMapFacade parse(InputStream inputStream, Properties props) throws Exception
    {
        if (props != null)
        {
            state.setGlobalProps(props);
        }
        return parse(inputStream);
    }

    public SqlMapFacade parse(InputStream inputStream) throws Exception
    {
        usingStreams = true;
        parser.parse(inputStream);
        return createFacade();
    }

    private void addSqlMapConfigNodelets()
    {
        parser.addNodelet("/sqlMapConfig/end()", new Nodelet()
        {
            @Override
            public void process(Node node)
            {
                state.getSqlMapContext().finalizeSqlMapConfig();
            }
        });
    }

    private void addGlobalPropNodelets()
    {
        parser.addNodelet("/sqlMapConfig/properties", new Nodelet()
        {
            @Override
            public void process(Node node)
            {
                Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String resource = attributes.getProperty("resource");
                String url = attributes.getProperty("url");
                state.setGlobalProperties(resource, url);
            }
        });
    }

    private void addSettingsNodelets()
    {
        parser.addNodelet("/sqlMapConfig/settings", new Nodelet()
        {
            @Override
            public void process(Node node)
            {
                Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                SqlMapContext config = state.getSqlMapContext();

                String useColumnLabelAttr = attributes.getProperty("useColumnLabel");
                boolean useColumnLabel = (useColumnLabelAttr == null || "true".equals(useColumnLabelAttr));
                config.setUseColumnLabel(useColumnLabel);

                String defaultTimeoutAttr = attributes.getProperty("defaultStatementTimeout");
                if (Strings.isNotEmpty(defaultTimeoutAttr))
                {
                    config.setDefaultStatementTimeout(Integer.parseInt(defaultTimeoutAttr));
                }

                String useStatementNamespacesAttr = attributes.getProperty("useStatementNamespaces");
                boolean useStatementNamespaces = "true".equals(useStatementNamespacesAttr);
                state.setUseStatementNamespaces(useStatementNamespaces);
            }
        });
    }

    private void addTypeAliasNodelets()
    {
        parser.addNodelet("/sqlMapConfig/typeAlias", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                Properties prop = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String alias = prop.getProperty("alias");
                String type = prop.getProperty("type");
                state.getSqlMapContext().getTypeHandlerRegistry().putTypeAlias(alias, type);
            }
        });
    }

    private void addTypeHandlerNodelets()
    {
        parser.addNodelet("/sqlMapConfig/typeHandler", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                Properties prop = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String jdbcType = prop.getProperty("jdbcType");
                String javaType = prop.getProperty("javaType");
                String callback = prop.getProperty("callback");

                javaType = state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(javaType);
                callback = state.getSqlMapContext().getTypeHandlerRegistry().resolveAlias(callback);

                state.getSqlMapContext()
                        .getTypeHandlerRegistry()
                        .register(Resources.classForName(javaType), jdbcType, Resources.instantiate(callback));
            }
        });
    }

    protected void addSqlMapNodelets()
    {
        parser.addNodelet("/sqlMapConfig/sqlMap", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());

                String resource = attributes.getProperty("resource");
                String url = attributes.getProperty("url");

                if (usingStreams)
                {
                    InputStream inputStream;
                    if (resource != null)
                    {
                        log.debug("Loading sql map:{}...", resource);
                        inputStream = Resources.getResourceAsStream(resource);
                    }
                    else if (url != null)
                    {
                        log.debug("Loading sql map:{}...", url);
                        inputStream = Resources.getUrlAsStream(url);
                    }
                    else
                    {
                        throw new SqlMapConfigException(
                                "The <sqlMap> element requires either a resource or a url attribute.");
                    }
                    new SqlMapParser(state).parse(inputStream);
                }
                else
                {
                    Reader reader;
                    if (resource != null)
                    {
                        log.debug("Loading sql map:{}...", resource);
                        reader = Resources.getResourceAsReader(resource);
                    }
                    else if (url != null)
                    {
                        log.debug("Loading sql map:{}...", url);
                        reader = Resources.getUrlAsReader(url);
                    }
                    else
                    {
                        throw new SqlMapConfigException(
                                "The <sqlMap> element requires either a resource or a url attribute.");
                    }

                    new SqlMapParser(state).parse(reader);
                }
            }
        });
    }

    private void addResultObjectFactoryNodelets()
    {
        parser.addNodelet("/sqlMapConfig/resultObjectFactory", new Nodelet()
        {
            @Override
            public void process(Node node) throws Exception
            {
                Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String type = attributes.getProperty("type");

                ResultObjectFactory rof;
                try
                {
                    rof = (ResultObjectFactory) Resources.instantiate(type);
                    state.getSqlMapContext().setResultObjectFactory(rof);
                }
                catch (Exception e)
                {
                    throw new SqlMapConfigException("Error instantiating resultObjectFactory: " + type, e);
                }
            }
        });
        parser.addNodelet("/sqlMapConfig/resultObjectFactory/property", new Nodelet()
        {
            @Override
            public void process(Node node)
            {
                Properties attributes = NodeletUtils.parseAttributes(node, state.getGlobalProps());
                String name = attributes.getProperty("name");
                String value = NodeletUtils.parsePropertyTokens(attributes.getProperty("value"),
                        state.getGlobalProps());
                state.getSqlMapContext().getResultObjectFactory().setProperty(name, value);
            }
        });
    }

    private SqlMapFacade createFacade()
    {
        return new SqlMapFacade(state.getSqlMapContext())
        {
            @Override
            public void addSqlMap(URL url) throws NodeletException, IOException
            {
                new SqlMapParser(state).parse(url);
            }
        };
    }
}
