package io.qiufen.jdbc.mapper.provider.mapping.result;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultMappingValue
{
    ResultMappingValue EMPTY = new ResultMappingValue()
    {
        @Override
        public void onValue(ResultMapping mapping, Object value) {}

        @Override
        public void start(ResultSet rs) {}

        @Override
        public void end() {}
    };

    void onValue(ResultMapping mapping, Object value);

    void start(ResultSet rs) throws SQLException;

    void end();
}
