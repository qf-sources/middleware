package io.qiufen.jdbc.mapper.api.transaction;

/**
 * 事务重复
 *
 * @author Ruzheng Zhang
 * @date 2020/9/28 10:33
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class DuplicationTransactionException extends TransactionException
{
    public DuplicationTransactionException(String message)
    {
        super(message);
    }
}
