package io.qiufen.jdbc.mapper.provider.scope;

import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMappingValue;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMapping;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMappingValue;
import io.qiufen.jdbc.mapper.provider.session.ExecutionLifecycle;
import io.qiufen.jdbc.mapper.provider.session.ResultValue;

import java.sql.ResultSet;
import java.sql.SQLException;

class MergeParameterMappingValue implements ParameterMappingValue
{
    private final ParameterMappingValue first;
    private final ParameterMappingValue second;

    MergeParameterMappingValue(ParameterMappingValue first, ParameterMappingValue second)
    {
        this.first = first;
        this.second = second;
    }

    @Override
    public void onValue(ParameterMapping mapping, Object value)
    {
        first.onValue(mapping, value);
        second.onValue(mapping, value);
    }

    @Override
    public void start()
    {
        first.start();
        second.start();
    }

    @Override
    public void end()
    {
        first.end();
        second.end();
    }
}

class MergeResultMappingValue implements ResultMappingValue
{
    private final ResultMappingValue first;
    private final ResultMappingValue second;

    MergeResultMappingValue(ResultMappingValue first, ResultMappingValue second)
    {
        this.first = first;
        this.second = second;
    }

    @Override
    public void onValue(ResultMapping mapping, Object value)
    {
        first.onValue(mapping, value);
        second.onValue(mapping, value);
    }

    @Override
    public void start(ResultSet rs) throws SQLException
    {
        first.start(rs);
        second.start(rs);
    }

    @Override
    public void end()
    {
        first.end();
        second.end();
    }
}

class MergeResultValue implements ResultValue
{
    private final ResultValue first;
    private final ResultValue second;

    MergeResultValue(ResultValue first, ResultValue second)
    {
        this.first = first;
        this.second = second;
    }

    @Override
    public void onValue(int rows)
    {
        first.onValue(rows);
        second.onValue(rows);
    }

    @Override
    public void onValue(int[] rows)
    {
        first.onValue(rows);
        second.onValue(rows);
    }
}

class MergeExecutionLifecycle implements ExecutionLifecycle
{
    private final ExecutionLifecycle first;
    private final ExecutionLifecycle second;

    MergeExecutionLifecycle(ExecutionLifecycle first, ExecutionLifecycle second)
    {
        this.first = first;
        this.second = second;
    }

    @Override
    public void onBegin()
    {
        first.onBegin();
        second.onBegin();
    }

    @Override
    public void onSuccess()
    {
        first.onSuccess();
        second.onSuccess();
    }

    @Override
    public void onFailure(Exception e)
    {
        first.onFailure(e);
        second.onFailure(e);
    }
}
