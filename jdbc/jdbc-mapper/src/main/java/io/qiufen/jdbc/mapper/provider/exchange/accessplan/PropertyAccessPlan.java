package io.qiufen.jdbc.mapper.provider.exchange.accessplan;

import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.common.beans.Property;

/**
 * Property access plan (for working with beans)
 */
class PropertyAccessPlan extends BaseAccessPlan
{
    protected Property[] properties;

    PropertyAccessPlan(Class<?> clazz, String[] propertyNames)
    {
        super(clazz, propertyNames);
        properties = getProperties(propertyNames);
    }

    public void setProperties(Object object, Object[] values)
    {
        for (int i = 0; i < propertyNames.length; i++)
        {
            setProperty(object, i, values[i]);
        }
    }

    @Override
    public void setProperty(Object object, int index, Object value)
    {
        try
        {
            properties[index].set(object, value);
        }
        catch (Throwable t)
        {
            throw new SessionException(
                    "Error setting property '" + properties[index].getName() + "' of '" + object + "'.  Cause: " + t,
                    t);
        }
    }

    public Object[] getProperties(Object object)
    {
        int i = 0;
        Object[] values = new Object[propertyNames.length];
        try
        {
            for (i = 0; i < propertyNames.length; i++)
            {
                values[i] = properties[i].get(object);
            }
        }
        catch (Throwable t)
        {
            throw new SessionException(
                    "Error getting property '" + properties[i].getName() + "' of '" + object + "'.  Cause: " + t, t);
        }
        return values;
    }

    @Override
    public Object getProperty(Object object, int index)
    {
        try
        {
            return properties[index].get(object);
        }
        catch (Throwable t)
        {
            throw new SessionException(
                    "Error getting property '" + properties[index].getName() + "' of '" + object + "'.  Cause: " + t,
                    t);
        }
    }
}
