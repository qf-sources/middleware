package io.qiufen.jdbc.mapper.provider.exchange.accessplan;

/**
 * An interface to make access to resources consistent, regardless of type.
 */
public interface AccessPlan
{
    /**
     * Sets all of the properties of a bean
     *
     * @param object - the bean
     * @param values - the property values
     */
    void setProperties(Object object, Object[] values);

    void setProperty(Object object, int index, Object value);

    /**
     * Gets all of the properties of a bean
     *
     * @param object - the bean
     * @return the properties
     */
    Object[] getProperties(Object object);

    Object getProperty(Object object, int index);
}
