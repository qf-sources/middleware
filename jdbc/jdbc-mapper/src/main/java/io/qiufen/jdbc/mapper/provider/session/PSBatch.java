package io.qiufen.jdbc.mapper.provider.session;

import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/28 19:49
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
class PSBatch
{
    private final StatementScope statementScope;
    private final PreparedStatement ps;

    private final List<Integer> globalIndexes = new ArrayList<Integer>();

    private List<ResultSetHandler> handlers;

    PSBatch(StatementScope statementScope, PreparedStatement ps)
    {
        this.statementScope = statementScope;
        this.ps = ps;
    }

    StatementScope getStatementScope()
    {
        return statementScope;
    }

    PreparedStatement getPs()
    {
        return ps;
    }

    List<Integer> getGlobalIndexes()
    {
        return globalIndexes;
    }

    void addResultSetHandler(ResultSetHandler handler)
    {
        if (handlers == null)
        {
            handlers = new ArrayList<ResultSetHandler>();
        }
        handlers.add(handler);
    }

    List<ResultSetHandler> getHandlers()
    {
        return handlers;
    }
}