package io.qiufen.jdbc.mapper.provider.exchange;

import io.qiufen.jdbc.mapper.provider.handler.TypeHandlerRegistry;

import java.util.List;
import java.util.Map;

/**
 * Factory for DataExchange objects
 */
public class DataExchangeFactory
{
    private final TypeHandlerRegistry typeHandlerRegistry;
    private final DataExchange listDataExchange;
    private final DataExchange mapDataExchange;
    private final DataExchange primitiveDataExchange;
    private final DataExchange complexDataExchange;

    /**
     * Constructor for the factory
     *
     * @param typeHandlerRegistry - a type handler factory for the factory
     */
    public DataExchangeFactory(TypeHandlerRegistry typeHandlerRegistry)
    {
        this.typeHandlerRegistry = typeHandlerRegistry;
        this.listDataExchange = new ListDataExchange(this);
        this.mapDataExchange = new ComplexDataExchange(this);
        this.primitiveDataExchange = new PrimitiveDataExchange(this);
        this.complexDataExchange = new ComplexDataExchange(this);
    }

    /**
     * Getter for the type handler factory
     *
     * @return - the type handler factory
     */
    public TypeHandlerRegistry getTypeHandlerFactory()
    {
        return typeHandlerRegistry;
    }

    /**
     * Get a DataExchange object for the passed in Class
     *
     * @param clazz - the class to get a DataExchange object for
     * @return - the DataExchange object
     */
    public DataExchange getDataExchangeForClass(Class<?> clazz)
    {
        DataExchange dataExchange;
        if (clazz == null)
        {
            dataExchange = complexDataExchange;
        }
        else if (List.class.isAssignableFrom(clazz))
        {
            dataExchange = listDataExchange;
        }
        else if (Map.class.isAssignableFrom(clazz))
        {
            dataExchange = mapDataExchange;
        }
        else if (typeHandlerRegistry.getTypeHandler(clazz) != null)
        {
            dataExchange = primitiveDataExchange;
        }
        else
        {
            dataExchange = new JavaBeanDataExchange(this);
        }
        return dataExchange;
    }

}
