package io.qiufen.jdbc.mapper.api.transaction;

/**
 * 事务定义
 */
public class TransactionDefinition
{
    private final int isolation;
    private final boolean readOnly;

    public TransactionDefinition(int isolation, boolean readOnly)
    {
        this.isolation = isolation;
        this.readOnly = readOnly;
    }

    public int getIsolation()
    {
        return isolation;
    }

    public boolean isReadOnly()
    {
        return readOnly;
    }
}
