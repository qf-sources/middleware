package io.qiufen.jdbc.mapper.provider.exchange;

import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMapping;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Base implementation for the DataExchange interface
 */
public abstract class BaseDataExchange implements DataExchange
{
    private final DataExchangeFactory dataExchangeFactory;

    protected BaseDataExchange(DataExchangeFactory dataExchangeFactory)
    {
        this.dataExchangeFactory = dataExchangeFactory;
    }

    @Override
    public void initialize(ParameterMap obj) {}

    @Override
    public void initialize(ResultMap obj) {}

    /**
     * Getter for the factory that created this object
     *
     * @return - the factory
     */
    public DataExchangeFactory getDataExchangeFactory()
    {
        return dataExchangeFactory;
    }

    protected void setParameterValue(StatementScope scope, ParameterMapping mapping, PreparedStatement ps,
            int parameterIndex, Object value) throws SQLException
    {
        mapping.setMappingValue(ps, parameterIndex, value);
        scope.getParameterMappingValue().onValue(mapping, value);
    }

    protected Object getResultMappingValue(StatementScope scope, ResultMapping mapping, ResultSet rs)
            throws SQLException
    {
        Object value = mapping.getMappingValue(rs);
        scope.getResultMappingValue().onValue(mapping, value);
        return value;
    }
}
