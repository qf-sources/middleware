package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Integer Decimal implementation of TypeHandler
 */
class IntegerTypeHandler extends BaseTypeHandler implements TypeHandler
{
    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setInt(i, (Integer) parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        int i = rs.getInt(columnName);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return i;
        }
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        int i = rs.getInt(columnIndex);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return i;
        }
    }

    public Object valueOf(String s)
    {
        return Integer.valueOf(s);
    }
}
