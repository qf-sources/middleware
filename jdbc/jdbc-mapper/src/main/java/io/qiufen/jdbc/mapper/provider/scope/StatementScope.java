package io.qiufen.jdbc.mapper.provider.scope;

import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMappingValue;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMappingValue;
import io.qiufen.jdbc.mapper.provider.session.ExecutionLifecycle;
import io.qiufen.jdbc.mapper.provider.session.ResultValue;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;

/**
 * Request based implementation of Scope interface
 */
public class StatementScope
{
    private final MappedStatement statement;

    private CharSequence sql;
    private ParameterMap parameterMap;
    private ResultMap resultMap;
    private ParameterMappingValue parameterMappingValue = ParameterMappingValue.EMPTY;
    private ResultMappingValue resultMappingValue = ResultMappingValue.EMPTY;
    private ResultValue resultValue = ResultValue.EMPTY;

    private ExecutionLifecycle executionLifecycle = ExecutionLifecycle.EMPTY;

    public StatementScope(MappedStatement statement)
    {
        this.statement = statement;
    }

    public CharSequence getSql()
    {
        return sql;
    }

    public void setSql(CharSequence sql)
    {
        this.sql = sql;
    }

    /**
     * Get the statement for the request
     *
     * @return - the statement
     */
    public MappedStatement getStatement()
    {
        return statement;
    }

    /**
     * Get the parameter map for the request
     *
     * @return - the parameter map
     */
    public ParameterMap getParameterMap()
    {
        return parameterMap;
    }

    /**
     * Set the parameter map for the request
     *
     * @param parameterMap - the new parameter map
     */
    public void setParameterMap(ParameterMap parameterMap)
    {
        this.parameterMap = parameterMap;
    }

    /**
     * Get the result map for the request
     *
     * @return - the result map
     */
    public ResultMap getResultMap()
    {
        return resultMap;
    }

    /**
     * Set the result map for the request
     *
     * @param resultMap - the result map
     */
    public void setResultMap(ResultMap resultMap)
    {
        this.resultMap = resultMap;
    }

    public ParameterMappingValue getParameterMappingValue()
    {
        return parameterMappingValue;
    }

    public void setParameterMappingValue(ParameterMappingValue parameterMappingValue)
    {
        this.parameterMappingValue = new MergeParameterMappingValue(this.parameterMappingValue, parameterMappingValue);
    }

    public ResultMappingValue getResultMappingValue()
    {
        return resultMappingValue;
    }

    public void setResultMappingValue(ResultMappingValue resultMappingValue)
    {
        this.resultMappingValue = new MergeResultMappingValue(this.resultMappingValue, resultMappingValue);
    }

    public ResultValue getResultValue()
    {
        return resultValue;
    }

    public void setResultValue(ResultValue resultValue)
    {
        this.resultValue = new MergeResultValue(this.resultValue, resultValue);
    }

    public ExecutionLifecycle getExecutionLifecycle()
    {
        return executionLifecycle;
    }

    public void setExecutionLifecycle(ExecutionLifecycle executionLifecycle)
    {
        this.executionLifecycle = new MergeExecutionLifecycle(this.executionLifecycle, executionLifecycle);
    }

    public void cleanup()
    {
        setSql(null);
        setResultMappingValue(null);
        setParameterMap(null);
        setResultMap(null);
    }
}
