package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Object implementation of TypeHandler
 */
class ObjectTypeHandler extends BaseTypeHandler implements TypeHandler
{
    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setObject(i, parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        Object object = rs.getObject(columnName);
        return rs.wasNull() ? null : object;
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        Object object = rs.getObject(columnIndex);
        return rs.wasNull() ? null : object;
    }

    public Object valueOf(String s)
    {
        return s;
    }

}
