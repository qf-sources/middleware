package io.qiufen.jdbc.mapper.provider.exchange;


import io.qiufen.common.lang.Arrays;
import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.exchange.accessplan.AccessPlan;
import io.qiufen.jdbc.mapper.provider.exchange.accessplan.AccessPlanFactory;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMapping;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultObjectFactoryUtil;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * DataExchange implementation for beans
 */
public class JavaBeanDataExchange extends BaseDataExchange implements DataExchange
{
    private AccessPlan resultPlan;
    private AccessPlan parameterPlan;

    protected JavaBeanDataExchange(DataExchangeFactory dataExchangeFactory)
    {
        super(dataExchangeFactory);
    }

    /**
     * Initializes the data exchange instance.
     */
    public void initialize(ParameterMap parameterMap)
    {
        int len;
        if ((len = parameterMap.countParameterMapping()) > 0)
        {
            String[] parameterPropNames = new String[len];
            for (int i = 0; i < parameterPropNames.length; i++)
            {
                parameterPropNames[i] = parameterMap.getParameterMapping(i).getPropertyName();
            }
            parameterPlan = AccessPlanFactory.getAccessPlan(parameterMap.getParameterClass(), parameterPropNames);
        }
    }

    /**
     * Initializes the data exchange instance.
     */
    public void initialize(ResultMap resultMap)
    {
        ResultMapping[] resultMappings = resultMap.getResultMappings();
        if (resultMappings != null && resultMappings.length > 0)
        {
            String[] resultPropNames = new String[resultMappings.length];
            for (int i = 0; i < resultPropNames.length; i++)
            {
                resultPropNames[i] = resultMappings[i].getPropertyName();
            }
            resultPlan = AccessPlanFactory.getAccessPlan(resultMap.getResultClass(), resultPropNames);
        }
    }

    public Object[] getData(StatementScope statementScope, ParameterMap parameterMap, Object parameterObject)
    {
        return parameterPlan == null ? Arrays.EMPTY : parameterPlan.getProperties(parameterObject);
    }

    @Override
    public Object setData(StatementScope statementScope, ResultMap resultMap, ResultSet rs) throws SQLException
    {
        Object object;
        try
        {
            object = ResultObjectFactoryUtil.createObjectThroughFactory(resultMap.getResultClass());
        }
        catch (Exception e)
        {
            throw new SessionException("JavaBeansDataExchange could not instantiate result class.  Cause: " + e, e);
        }

        ResultMapping[] mappings = resultMap.getResultMappings();
        for (int i = 0; i < mappings.length; i++)
        {
            resultPlan.setProperty(object, i, getResultMappingValue(statementScope, mappings[i], rs));
        }
        return object;
    }

    @Override
    public void setData(StatementScope statementScope, ParameterMap parameterMap, PreparedStatement ps,
            Object parameterObject) throws SQLException
    {
        int len = parameterMap.countParameterMapping();
        for (int i = 0; i < len; i++)
        {
            setParameterValue(statementScope, parameterMap.getParameterMapping(i), ps, i + 1,
                    parameterPlan.getProperty(parameterObject, i));
        }
    }
}
