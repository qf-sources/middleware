package io.qiufen.jdbc.mapper.spi.filter;

public interface StatementScopeFilter extends BaseFilter<StatementScopeFilterContext> {}
