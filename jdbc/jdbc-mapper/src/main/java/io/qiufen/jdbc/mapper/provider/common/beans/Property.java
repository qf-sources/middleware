package io.qiufen.jdbc.mapper.provider.common.beans;

public interface Property
{
    String getName();

    Class<?> getType();

    void set(Object object, Object value);

    Object get(Object object);

    boolean isWritable();

    boolean isReadable();
}
