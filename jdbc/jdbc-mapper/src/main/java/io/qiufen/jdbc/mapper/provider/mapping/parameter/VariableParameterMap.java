package io.qiufen.jdbc.mapper.provider.mapping.parameter;

import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;

import java.util.Collections;
import java.util.List;

public class VariableParameterMap extends AbstractParameterMap
{
    private static final List<ParameterMapping> EMPTY = Collections.emptyList();

    private List<ParameterMapping> parameterMappingList = EMPTY;

    public VariableParameterMap(SqlMapContext sqlMapContext)
    {
        super(sqlMapContext);
    }

    @Override
    public void setId(CTString id)
    {
        super.setId(id);
    }

    @Override
    public void setParameterClass(Class<?> parameterClass)
    {
        super.setParameterClass(parameterClass);
    }

    @Override
    public int countParameterMapping()
    {
        return this.parameterMappingList.size();
    }

    @Override
    public ParameterMapping getParameterMapping(int index)
    {
        return this.parameterMappingList.get(index);
    }

    public List<ParameterMapping> getParameterMappingList()
    {
        return parameterMappingList;
    }

    public void setParameterMappingList(List<ParameterMapping> parameterMappingList)
    {
        if (parameterMappingList != null && !parameterMappingList.isEmpty())
        {
            this.parameterMappingList = parameterMappingList;
        }
        initDataExchange();
    }

    @Override
    public VariableParameterMap toVar()
    {
        return this;
    }
}
