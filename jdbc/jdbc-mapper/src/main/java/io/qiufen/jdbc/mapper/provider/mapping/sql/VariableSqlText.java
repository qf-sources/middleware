package io.qiufen.jdbc.mapper.provider.mapping.sql;

import io.qiufen.common.lang.StringBuilder;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;

import java.util.List;

public class VariableSqlText implements SqlChild
{
    private StringBuilder text;

    private List<ParameterMapping> parameterMappingList;

    public CharSequence getText()
    {
        return text;
    }

    public void setText(StringBuilder text)
    {
        this.text = text;
    }

    public List<ParameterMapping> getParameterMappingList()
    {
        return parameterMappingList;
    }

    public void setParameterMappingList(List<ParameterMapping> parameterMappingList)
    {
        this.parameterMappingList = parameterMappingList;
    }
}
