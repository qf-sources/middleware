package io.qiufen.jdbc.mapper.provider.config.xml;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.Strings;
import io.qiufen.jdbc.mapper.provider.common.xml.NodeletUtils;
import io.qiufen.jdbc.mapper.provider.config.InlineParameterMapParser;
import io.qiufen.jdbc.mapper.provider.config.SqlSource;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.mapping.sql.SqlText;
import io.qiufen.jdbc.mapper.provider.mapping.sql.VariableSqlText;
import io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.DynamicSql;
import io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements.*;
import io.qiufen.jdbc.mapper.provider.mapping.sql.raw.RawSql;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.Properties;

public class XMLSqlSource implements SqlSource
{
    private final InlineParameterMapParser parameterMapParser = new InlineParameterMapParser();

    private final XmlParserState state;
    private final Node parentNode;

    public XMLSqlSource(XmlParserState state, Node parentNode)
    {
        this.state = state;
        this.parentNode = parentNode;
    }

    public Sql getSql(MappedStatement statement)
    {
        StringBuilder builder = new StringBuilder();
        DynamicSql dynamic = new DynamicSql(state.getSqlMapContext(), statement);
        boolean isDynamic = parseDynamicTags(parentNode, statement.getParameterClass(), dynamic, builder, false, false);
        return isDynamic ? dynamic : new RawSql(CTString.of(builder));
    }

    private boolean parseDynamicTags(Node node, Class<?> parameterClass, DynamicParent dynamic, StringBuilder sqlBuffer,
            boolean isDynamic, boolean postParseRequired)
    {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++)
        {
            Node child = children.item(i);
            String nodeName = child.getNodeName();
            if (child.getNodeType() == Node.CDATA_SECTION_NODE || child.getNodeType() == Node.TEXT_NODE)
            {
                String data = ((CharacterData) child).getData();
                data = NodeletUtils.parsePropertyTokens(data, state.getGlobalProps()).trim().replaceAll("\\s{2,}", " ");
                if (Strings.isEmpty(data)) continue;

                data += ' ';//末尾补充一个空格
                SqlText sqlText;
                if (postParseRequired)
                {
                    sqlText = new SqlText(CTString.of(data), true, null);
                }
                else
                {
                    VariableSqlText variableSqlText = parameterMapParser.parseInlineParameterMap(
                            state.getSqlMapContext().getTypeHandlerRegistry(), data, parameterClass);
                    List<ParameterMapping> parameterMappingList = variableSqlText.getParameterMappingList();
                    ParameterMapping[] parameterMappings = null;
                    if (CollectionUtil.isNotEmpty(parameterMappingList))
                    {
                        parameterMappings = new ParameterMapping[parameterMappingList.size()];
                        parameterMappingList.toArray(parameterMappings);
                    }
                    sqlText = new SqlText(CTString.of(variableSqlText.getText()), false, parameterMappings);
                }

                dynamic.addChild(sqlText);

                sqlBuffer.append(data);
            }
            else if ("include".equals(nodeName))
            {
                Properties attributes = NodeletUtils.parseAttributes(child, state.getGlobalProps());
                String refid = (String) attributes.get("refid");
                Node includeNode = state.getSqlIncludes().get(refid);
                if (includeNode == null)
                {
                    String nsrefid = state.applyNamespace(refid);
                    includeNode = state.getSqlIncludes().get(nsrefid);
                    if (includeNode == null)
                    {
                        throw new RuntimeException(
                                "Could not find SQL statement to include with refid '" + refid + "'");
                    }
                }
                isDynamic = parseDynamicTags(includeNode, parameterClass, dynamic, sqlBuffer, isDynamic, false);
            }
            else
            {
                SqlTagHandler handler = SqlTagHandlerFactory.getSqlTagHandler(nodeName);
                if (handler != null)
                {
                    isDynamic = true;

                    SqlTag tag = new SqlTag();
                    tag.setName(nodeName);
                    tag.setHandler(handler);

                    Properties attributes = NodeletUtils.parseAttributes(child, state.getGlobalProps());

                    String prepend = attributes.getProperty("prepend");
                    prepend = Strings.isEmpty(prepend) ? prepend : prepend.trim();
                    tag.setPrependAttr(prepend);
                    tag.setPropertyAttr(attributes.getProperty("property"));
                    tag.setRemoveFirstPrepend(attributes.getProperty("removeFirstPrepend"));

                    String open = attributes.getProperty("open");
                    open = Strings.isEmpty(open) ? open : open.trim();
                    tag.setOpenAttr(open);

                    String close = attributes.getProperty("close");
                    close = Strings.isEmpty(close) ? close : close.trim();
                    tag.setCloseAttr(close);

                    tag.setComparePropertyAttr(attributes.getProperty("compareProperty"));
                    tag.setCompareValueAttr(attributes.getProperty("compareValue"));
                    tag.setConjunctionAttr(attributes.getProperty("conjunction"));

                    // an iterate ancestor requires a post parse

                    if (dynamic instanceof SqlTag)
                    {
                        SqlTag parentSqlTag = (SqlTag) dynamic;
                        if (parentSqlTag.isPostParseRequired() || tag.getHandler() instanceof IterateTagHandler)
                        {
                            tag.setPostParseRequired(true);
                        }
                    }
                    else if (dynamic instanceof DynamicSql)
                    {
                        if (tag.getHandler() instanceof IterateTagHandler)
                        {
                            tag.setPostParseRequired(true);
                        }
                    }

                    dynamic.addChild(tag);

                    if (child.hasChildNodes())
                    {
                        isDynamic = parseDynamicTags(child, parameterClass, tag, sqlBuffer, true,
                                tag.isPostParseRequired());
                    }
                }
            }
        }
        return isDynamic;
    }
}
