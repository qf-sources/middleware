package io.qiufen.jdbc.mapper.provider.statement;

import io.qiufen.jdbc.mapper.provider.config.GeneratedKeyConfig;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

public class InsertStatement extends MappedStatement
{
    private GeneratedKeyConfig generatedKeyConfig;

    public StatementType getStatementType()
    {
        return StatementType.INSERT;
    }

    public GeneratedKeyConfig getGeneratedKeyConfig()
    {
        return generatedKeyConfig;
    }

    public void setGeneratedKeyConfig(GeneratedKeyConfig generatedKeyConfig)
    {
        this.generatedKeyConfig = generatedKeyConfig;
    }

    @Override
    public StatementScope initRequest()
    {
        StatementScope statementScope = new StatementScope(this);
        statementScope.setParameterMap(getParameterMap());
        return statementScope;
    }
}
