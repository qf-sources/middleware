package io.qiufen.jdbc.mapper.provider.session;

import io.qiufen.common.collection.CollectionUtil;
import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.Numbers;
import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.api.transaction.DuplicationTransactionException;
import io.qiufen.jdbc.mapper.api.transaction.Transaction;
import io.qiufen.jdbc.mapper.api.transaction.TransactionDefinition;
import io.qiufen.jdbc.mapper.api.transaction.TransactionException;
import io.qiufen.jdbc.mapper.provider.session.context.SessionContext;
import io.qiufen.jdbc.mapper.provider.session.context.TransactionSessionContext;
import io.qiufen.jdbc.mapper.spi.handler.RowHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/30 14:41
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
abstract class AbstractSession implements Session
{
    private static final Logger log = LoggerFactory.getLogger(AbstractSession.class);

    private static final AtomicInteger INDEX = new AtomicInteger();

    private final int id = INDEX.getAndIncrement();

    private SessionContext sessionContext;
    private boolean closed = false;

    AbstractSession(SessionContext sessionContext)
    {
        this.sessionContext = sessionContext;
        if (log.isDebugEnabled()) log.debug("Create session:{}", getHexId());
    }

    @Override
    public Transaction createTransaction(TransactionDefinition definition) throws TransactionException
    {
        checkSessionState();
        if (this.sessionContext instanceof Transaction)
        {
            throw new DuplicationTransactionException("Transaction has created.");
        }
        this.sessionContext = new TransactionSessionContext(this.sessionContext, definition);
        return (Transaction) this.sessionContext;
    }

    @Override
    public Transaction getTransaction() throws TransactionException
    {
        checkSessionState();
        if (this.sessionContext instanceof Transaction)
        {
            return (Transaction) this.sessionContext;
        }
        throw new TransactionException("None transaction created.");
    }

    @Override
    public <T> T selectOne(CTString id, Object parameterObject) throws SessionException
    {
        List<T> list = select(id, parameterObject);
        if (CollectionUtil.isEmpty(list))
        {
            return null;
        }
        return list.get(0);
    }

    @Override
    public Object selectOne(CTString id, Object parameterObject, Object resultObject) throws SessionException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> List<T> select(CTString id, Object parameterObject) throws SessionException
    {
        DataListRowHandler<T> handler = new DataListRowHandler<T>();
        select(id, parameterObject, handler);
        return handler.getList();
    }

    @Override
    public void close() throws SessionException
    {
        if (this.closed)
        {
            return;
        }

        try
        {
            this.sessionContext.close();
        }
        catch (SQLException e)
        {
            throw new SessionException(e);
        }
        this.closed = true;
        if (log.isDebugEnabled()) log.debug("Session closed:{}.", getHexId());
    }

    @Override
    public boolean isClosed()
    {
        return closed;
    }

    public int getId()
    {
        return this.id;
    }

    public String getHexId()
    {
        return Numbers.toFullHex(this.id);
    }

    SessionContext getSessionContext()
    {
        return sessionContext;
    }

    void checkSessionState()
    {
        if (this.closed)
        {
            throw new SessionException("Session is closed.");
        }
    }

    static class DataListRowHandler<T> implements RowHandler<T>
    {
        private final List<T> list = new ArrayList<T>();

        @Override
        public void handleRow(T valueObject)
        {
            list.add(valueObject);
        }

        protected List<T> getList()
        {
            return list;
        }
    }
}
