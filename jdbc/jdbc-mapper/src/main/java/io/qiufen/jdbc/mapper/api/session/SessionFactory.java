package io.qiufen.jdbc.mapper.api.session;

import io.qiufen.jdbc.mapper.provider.config.xml.SqlMapFacade;

import java.sql.Connection;

/**
 * DB会话工厂
 */
public interface SessionFactory
{
    /**
     * 打开DB会话
     */
    Session openSession();

    /**
     * 使用用户链接打开DB会话
     *
     * @param userConnection 用户链接
     */
    Session openSession(Connection userConnection);

    /**
     * 销毁
     */
    void destroy();

    SqlMapFacade getSqlMapFacade();
}