package io.qiufen.jdbc.mapper.provider.session.context;

import io.qiufen.jdbc.mapper.provider.scope.SessionScope;

import java.sql.SQLException;

public interface SessionContext
{
    SessionScope createSessionScope() throws SQLException;

    void close() throws SQLException;
}
