package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Double implementation of TypeHandler
 */
class DoubleTypeHandler extends BaseTypeHandler implements TypeHandler
{
    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setDouble(i, (Double) parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        double d = rs.getDouble(columnName);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return d;
        }
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        double d = rs.getDouble(columnIndex);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return d;
        }
    }

    public Object valueOf(String s)
    {
        return Double.valueOf(s);
    }
}
