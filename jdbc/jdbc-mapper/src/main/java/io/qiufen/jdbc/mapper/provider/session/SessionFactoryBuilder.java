package io.qiufen.jdbc.mapper.provider.session;

import io.qiufen.common.io.IOUtil;
import io.qiufen.common.io.resource.StreamResource;
import io.qiufen.common.provider.DefaultProviderContext;
import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.config.xml.SqlMapConfigParser;
import io.qiufen.jdbc.mapper.provider.config.xml.SqlMapFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;

public class SessionFactoryBuilder
{
    private static final Logger log = LoggerFactory.getLogger(SessionFactoryBuilder.class);

    private final DefaultProviderContext providerContext = new DefaultProviderContext();

    private StreamResource resource;

    private DataSource dataSource;

    public SessionFactoryBuilder setResource(StreamResource resource)
    {
        this.resource = resource;
        return this;
    }

    public SessionFactoryBuilder setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
        return this;
    }

    public SessionFactory build() throws IOException, SqlMapConfigException
    {
        if (log.isInfoEnabled())
        {
            log.info("Use resource:{}", resource);
        }

        InputStream stream = resource.open();
        SqlMapFacade sqlMapFacade;
        try
        {
            sqlMapFacade = new SqlMapConfigParser().parse(stream);
        }
        catch (Exception e)
        {
            throw new SqlMapConfigException("Parse resource failure:" + resource.toString(), e);
        }
        finally
        {
            IOUtil.close(stream);
        }

        return new DefaultSessionFactory(sqlMapFacade, dataSource);
    }
}
