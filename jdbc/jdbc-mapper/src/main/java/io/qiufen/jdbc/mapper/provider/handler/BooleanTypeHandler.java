package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Boolean implementation of TypeHandler
 */
class BooleanTypeHandler extends BaseTypeHandler implements TypeHandler
{

    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setBoolean(i, (Boolean) parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        boolean b = rs.getBoolean(columnName);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return b;
        }
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        boolean b = rs.getBoolean(columnIndex);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return b;
        }
    }

    public Object valueOf(String s)
    {
        return Boolean.valueOf(s);
    }
}
