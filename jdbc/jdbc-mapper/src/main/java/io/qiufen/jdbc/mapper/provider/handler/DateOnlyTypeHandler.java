package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.provider.common.utils.SimpleDateFormatter;
import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Date only implementation of TypeHandler
 */
class DateOnlyTypeHandler extends BaseTypeHandler implements TypeHandler
{
    private static final String DATE_FORMAT = "yyyy/MM/dd";

    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setDate(i, new java.sql.Date(((Date) parameter).getTime()));
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        java.sql.Date sqlDate = rs.getDate(columnName);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return new java.util.Date(sqlDate.getTime());
        }
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        java.sql.Date sqlDate = rs.getDate(columnIndex);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return new java.util.Date(sqlDate.getTime());
        }
    }

    public Object valueOf(String s)
    {
        return SimpleDateFormatter.format(DATE_FORMAT, s);
    }

}
