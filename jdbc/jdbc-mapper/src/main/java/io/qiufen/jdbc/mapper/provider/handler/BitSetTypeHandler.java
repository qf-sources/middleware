package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.common.lang.BitSets;
import io.qiufen.common.lang.Strings;
import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.BitSet;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 17-7-25
 * Time: 下午8:41
 * To change this template use File | Settings | File Templates.
 */
class BitSetTypeHandler extends BaseTypeHandler
{
    @Override
    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setLong(i, BitSets.toLong((BitSet) parameter));
    }

    @Override
    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        return BitSets.toBitSet(rs.getLong(columnName));
    }

    @Override
    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        return BitSets.toBitSet(rs.getLong(columnIndex));
    }

    @Override
    public Object valueOf(String s)
    {
        long features = 0L;
        if (Strings.isNotEmpty(s))
        {
            features = Long.parseLong(s);
        }
        return BitSets.toBitSet(features);
    }
}
