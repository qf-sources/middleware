package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic;

import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.CharSequences;
import io.qiufen.common.lang.StringBuilder;
import io.qiufen.jdbc.mapper.provider.config.InlineParameterMapParser;
import io.qiufen.jdbc.mapper.provider.config.SqlMapContext;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.VariableParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.sql.Sql;
import io.qiufen.jdbc.mapper.provider.mapping.sql.SqlChild;
import io.qiufen.jdbc.mapper.provider.mapping.sql.SqlText;
import io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements.*;
import io.qiufen.jdbc.mapper.provider.mapping.sql.simple.SimpleDynamicSql;
import io.qiufen.jdbc.mapper.provider.mapping.sql.simple.VariableSimpleDynamicSql;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;
import io.qiufen.jdbc.mapper.provider.statement.MappedStatement;

import java.util.ArrayList;
import java.util.List;

public class DynamicSql implements Sql, DynamicParent
{
    private static final IteratePropertyParser PARSER_0 = new IteratePropertyParser('#');
    private static final IteratePropertyParser PARSER_1 = new IteratePropertyParser('$');

    private final InlineParameterMapParser parameterMapParser = new InlineParameterMapParser();
    private final List<SqlChild> children = new ArrayList<SqlChild>(1);

    private final SqlMapContext sqlMapContext;
    private final CTString parameterMapId;

    public DynamicSql(SqlMapContext sqlMapContext, MappedStatement statement)
    {
        this.sqlMapContext = sqlMapContext;
        this.parameterMapId = CTString.wrap(statement.getId(), "-InlineParameterMap");
    }

    @Override
    public void applyParameter(StatementScope statementScope, Object parameterObject)
    {
        process(statementScope, parameterObject);
    }

    @Override
    public void addChild(SqlChild child)
    {
        children.add(child);
    }

    private void process(StatementScope statementScope, Object parameterObject)
    {
        Class<?> parameterClass = statementScope.getStatement().getParameterClass();
        SqlTagContext ctx = new SqlTagContext(sqlMapContext);
        StringBuilder dynSql = new StringBuilder();
        processBodyChildren(ctx, parameterClass, parameterObject, children, dynSql);
        statementScope.setSql(dynSql);

        VariableParameterMap map = new VariableParameterMap(sqlMapContext);
        map.setId(this.parameterMapId);
        map.setParameterClass(parameterClass);
        map.setParameterMappingList(ctx.getParameterMappings());
        statementScope.setParameterMap(map);
    }

    private void processBodyChildren(SqlTagContext ctx, Class<?> parameterClass, Object parameterObject,
            List<SqlChild> localChildren, StringBuilder bodyBuffer)
    {
        for (SqlChild child : localChildren)
        {
            if (child instanceof SqlText)
            {
                processSqlText(ctx, parameterClass, bodyBuffer, (SqlText) child, parameterObject);
            }
            else if (child instanceof SqlTag)
            {
                processSqlTag(ctx, parameterClass, bodyBuffer, (SqlTag) child, parameterObject);
            }
        }
    }

    private void processSqlText(SqlTagContext ctx, Class<?> parameterClass, StringBuilder bodyBuffer, SqlText sqlText,
            Object parameterObject)
    {
        CTString sqlSegment = sqlText.getText();
        boolean simpleDynamic = sqlText.isSimpleDynamic();
        if (sqlText.isPostParseRequired())
        {
            IterateContext iterateContext = ctx.peekIterateContext();
            if (null != iterateContext && iterateContext.isAllowNext())
            {
                iterateContext.next();
                iterateContext.setAllowNext(false);
                if (!iterateContext.hasNext()) iterateContext.setFinal(true);
            }
            StringBuilder sqlSegmentBuffer = new StringBuilder(sqlSegment.length()).append(sqlSegment);
            if (iterateContext != null) iteratePropertyReplace(simpleDynamic, sqlSegmentBuffer, iterateContext);

            parameterMapParser.parseInlineParameterMap(sqlMapContext.getTypeHandlerRegistry(), sqlSegmentBuffer,
                    parameterClass, ctx);

            if (simpleDynamic)
            {
                VariableSimpleDynamicSql.processDynamicElements(sqlMapContext, sqlSegmentBuffer, parameterObject);
            }
            bodyBuffer.append(sqlSegmentBuffer);
        }
        else
        {
            if (simpleDynamic)
            {
                bodyBuffer.append(SimpleDynamicSql.processDynamicElements(sqlMapContext, sqlSegment, parameterObject));
            }
            else
            {
                bodyBuffer.append(sqlSegment);
            }

            ParameterMapping[] mappings = sqlText.getParameterMappings();
            if (mappings != null)
            {
                for (ParameterMapping mapping : mappings)
                {
                    ctx.addParameterMapping(mapping);
                }
            }
        }
    }

    private void processSqlTag(SqlTagContext ctx, Class<?> parameterClass, StringBuilder bodyBuffer, SqlTag tag,
            Object parameterObject)
    {
        SqlTagHandler handler = tag.getHandler();
        int response;
        do
        {
            response = handler.doStartFragment(ctx, tag, parameterObject);
            if (response != SqlTagHandler.SKIP_BODY)
            {
                StringBuilder body = new StringBuilder();
                processBodyChildren(ctx, parameterClass, parameterObject, tag.getChildren(), body);
                response = handler.doEndFragment(ctx, tag, parameterObject, body);
                handler.doPrepend(ctx, tag, parameterObject, body);
                if (response != SqlTagHandler.SKIP_BODY) bodyBuffer.append(body);
            }
        }
        while (response == SqlTagHandler.REPEAT_BODY);

        ctx.popRemoveFirstPrependMarker(tag);

        IterateContext iterateContext = ctx.peekIterateContext();
        if (iterateContext != null && iterateContext.getTag() == tag)
        {
            ctx.setAttribute(iterateContext.getTag(), null);
            ctx.popIterateContext();
        }
    }

    private void iteratePropertyReplace(boolean simpleDynamic, StringBuilder bodyContent, IterateContext iterate)
    {
        CharSequences.replaceArgs(bodyContent, '#', '#', iterate, PARSER_0);
        if (simpleDynamic)
        {
            CharSequences.replaceArgs(bodyContent, '$', '$', iterate, PARSER_1);
        }
    }

    private static class IteratePropertyParser implements CharSequences.VariableParser<IterateContext>
    {
        private final char mapping;

        private IteratePropertyParser(char mapping)
        {
            this.mapping = mapping;
        }

        @Override
        public int parse(IterateContext iterateContext, CharSequences.Var var, CharSequences.Resolver resolver)
        {
            java.lang.StringBuilder result = iterateContext.addIndexToTagProperty(
                    new java.lang.StringBuilder(var.length() + 4).append(var));
            result.insert(0, mapping).append(mapping);
            return resolver.resolve(result, StringBuilder.COPIER_J_STRING_BUILDER);
        }
    }
}