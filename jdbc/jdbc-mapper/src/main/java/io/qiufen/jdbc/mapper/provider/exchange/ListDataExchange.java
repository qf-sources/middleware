package io.qiufen.jdbc.mapper.provider.exchange;

import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMapping;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * DataExchange implementation for List objects
 */
public class ListDataExchange extends BaseDataExchange implements DataExchange
{
    ListDataExchange(DataExchangeFactory dataExchangeFactory)
    {
        super(dataExchangeFactory);
    }

    public Object[] getData(StatementScope statementScope, ParameterMap parameterMap, Object parameterObject)
    {
        int len = parameterMap.countParameterMapping();
        Object[] data = new Object[len];
        for (int i = 0; i < len; i++)
        {
            ParameterMapping mapping = parameterMap.getParameterMapping(i);
            String propName = mapping.getPropertyName();

            // parse on the '.' notation and get nested properties
            String[] propertyArray = propName.split("\\.");

            if (propertyArray.length > 0)
            {
                // iterate list of properties to discover values
                Object tempData = parameterObject;
                for (String aPropertyArray : propertyArray)
                {
                    // is property an array reference
                    int arrayStartIndex = aPropertyArray.indexOf('[');
                    if (arrayStartIndex == -1)
                    {
                        // is a normal property
                        tempData = ProbeFactory.getProbe().getObject(tempData, aPropertyArray);
                    }
                    else
                    {
                        int index = Integer.parseInt(
                                aPropertyArray.substring(arrayStartIndex + 1, aPropertyArray.length() - 1));
                        tempData = ((List) tempData).get(index);
                    }
                }
                data[i] = tempData;
            }
            else
            {
                int index = Integer.parseInt((propName.substring(propName.indexOf('[') + 1, propName.length() - 1)));
                data[i] = ((List) parameterObject).get(index);
            }
        }
        return data;
    }

    @Override
    public Object setData(StatementScope statementScope, ResultMap resultMap, ResultSet rs) throws SQLException
    {
        ResultMapping[] mappings = resultMap.getResultMappings();
        Object[] array = new Object[mappings.length];
        for (ResultMapping mapping : mappings)
        {
            String propName = mapping.getPropertyName();
            int index = Integer.parseInt(propName.substring(1, propName.length() - 1));
            array[index] = getResultMappingValue(statementScope, mapping, rs);
        }
        return Arrays.asList(array);
    }

    @Override
    public void setData(StatementScope statementScope, ParameterMap parameterMap, PreparedStatement ps,
            Object parameterObject) throws SQLException
    {
        int len = parameterMap.countParameterMapping();
        for (int i = 0; i < len; i++)
        {
            ParameterMapping mapping = parameterMap.getParameterMapping(i);
            String propName = mapping.getPropertyName();

            // parse on the '.' notation and get nested properties
            String[] propertyArray = propName.split("\\.");

            Object value;
            if (propertyArray.length > 0)
            {
                // iterate list of properties to discover values
                Object tempData = parameterObject;
                for (String aPropertyArray : propertyArray)
                {
                    // is property an array reference
                    int arrayStartIndex = aPropertyArray.indexOf('[');
                    if (arrayStartIndex == -1)
                    {
                        // is a normal property
                        tempData = ProbeFactory.getProbe().getObject(tempData, aPropertyArray);
                    }
                    else
                    {
                        int index = Integer.parseInt(
                                aPropertyArray.substring(arrayStartIndex + 1, aPropertyArray.length() - 1));
                        tempData = ((List) tempData).get(index);
                    }
                }
                value = tempData;
            }
            else
            {
                int index = Integer.parseInt((propName.substring(propName.indexOf('[') + 1, propName.length() - 1)));
                value = ((List) parameterObject).get(index);
            }
            setParameterValue(statementScope, mapping, ps, i + 1, value);
        }
    }
}
