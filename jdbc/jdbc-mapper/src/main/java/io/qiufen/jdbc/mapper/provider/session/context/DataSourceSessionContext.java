package io.qiufen.jdbc.mapper.provider.session.context;

import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.scope.SessionScope;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceSessionContext implements SessionContext
{
    private final DataSource dataSource;

    public DataSourceSessionContext(DataSource dataSource)
    {
        if (dataSource == null)
        {
            throw new SessionException("Data source is required!");
        }
        this.dataSource = dataSource;
    }

    @Override
    public SessionScope createSessionScope() throws SQLException
    {
        return new DataSourceSessionScope(dataSource);
    }

    @Override
    public void close()
    {
    }

    private static class DataSourceSessionScope implements SessionScope
    {
        private final Connection connection;

        private DataSourceSessionScope(DataSource dataSource) throws SQLException
        {
            this.connection = dataSource.getConnection();
        }

        public Connection getConnection()
        {
            return connection;
        }

        public void close() throws SQLException
        {
            connection.close();
        }
    }

}
