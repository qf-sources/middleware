package io.qiufen.jdbc.mapper.provider.session;

public interface ResultValue
{
    ResultValue EMPTY = new ResultValue()
    {
        @Override
        public void onValue(int rows) {}

        @Override
        public void onValue(int[] rows) {}
    };

    void onValue(int rows);

    void onValue(int[] rows);
}
