package io.qiufen.jdbc.mapper.provider.config.xml;

import io.qiufen.jdbc.mapper.provider.common.resources.Resources;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Offline entity resolver for the iBATIS DTDs
 */
public class SqlMapClasspathEntityResolver implements EntityResolver
{
    private static final String SQL_MAP_CONFIG_DTD = "META-INF/sql-map-config-1.0.xsd";
    private static final String SQL_MAP_DTD = "META-INF/sql-map-1.0.xsd";

    private static final Map<String, String> doctypeMap = new HashMap<String, String>();

    static
    {
        doctypeMap.put("http://jdbc.qiufen.io/sql-map-config".toUpperCase(), SQL_MAP_CONFIG_DTD);
        doctypeMap.put("http://jdbc.qiufen.io/sql-map".toUpperCase(), SQL_MAP_DTD);

        doctypeMap.put("http://jdbc.qiufen.io/sql-map-config-1.0.xsd".toUpperCase(), SQL_MAP_CONFIG_DTD);
        doctypeMap.put("http://jdbc.qiufen.io/sql-map-1.0.xsd".toUpperCase(), SQL_MAP_DTD);
    }

    /**
     * Converts a public DTD into a local one
     *
     * @param publicId Unused but required by EntityResolver interface
     * @param systemId The DTD that is being requested
     * @return The InputSource for the DTD
     * @throws SAXException If anything goes wrong
     */
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException
    {
        if (publicId != null)
        {
            publicId = publicId.toUpperCase();
        }
        if (systemId != null)
        {
            systemId = systemId.toUpperCase();
        }

        InputSource source;
        try
        {
            String path = doctypeMap.get(publicId);
            source = getInputSource(path, null);
            if (source == null)
            {
                path = doctypeMap.get(systemId);
                source = getInputSource(path, null);
            }
        }
        catch (Exception e)
        {
            throw new SAXException(e.toString());
        }
        return source;
    }

    private InputSource getInputSource(String path, InputSource source)
    {
        if (path != null)
        {
            InputStream in;
            try
            {
                in = Resources.getResourceAsStream(path);
                source = new InputSource(in);
            }
            catch (IOException e)
            {
                // ignore, null is ok
            }
        }
        return source;
    }

}
