package io.qiufen.jdbc.mapper.provider.session.context;

import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.scope.SessionScope;

import java.sql.Connection;

public class UserConnectionSessionContext implements SessionContext
{
    private final SessionScope sessionScope;

    public UserConnectionSessionContext(Connection userConnection)
    {
        if (userConnection == null)
        {
            throw new SessionException("User connection is required!");
        }
        this.sessionScope = new UserConnectionSessionScope(userConnection);
    }

    @Override
    public SessionScope createSessionScope()
    {
        return this.sessionScope;
    }

    @Override
    public void close()
    {
    }

    private static class UserConnectionSessionScope implements SessionScope
    {
        private final Connection connection;

        private UserConnectionSessionScope(Connection connection)
        {
            this.connection = connection;
        }

        public Connection getConnection()
        {
            return connection;
        }

        public void close()
        {
        }
    }
}
