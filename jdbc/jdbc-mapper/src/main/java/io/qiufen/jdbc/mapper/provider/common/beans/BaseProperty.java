package io.qiufen.jdbc.mapper.provider.common.beans;

public abstract class BaseProperty implements Property
{
    private final Class<?> type;

    public BaseProperty(Class<?> type) {this.type = type;}

    @Override
    public final Class<?> getType()
    {
        return this.type;
    }

    @Override
    public void set(Object object, Object value)
    {
        throw new UnsupportedOperationException("None setter method found:" + getName());
    }

    @Override
    public Object get(Object object)
    {
        throw new UnsupportedOperationException("None getter method found:" + getName());
    }

    @Override
    public boolean isWritable()
    {
        return false;
    }

    @Override
    public boolean isReadable()
    {
        return false;
    }

    @Override
    public String toString()
    {
        return "{name:" + getName() + "}";
    }
}
