package io.qiufen.jdbc.mapper.provider.common.beans;

public interface Constructor
{
    Object newInstance() throws Throwable;
}
