package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.common.lang.CTString;
import io.qiufen.common.lang.LocalMap;
import io.qiufen.common.lang.Strings;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.handler.extension.BlobTypeHandlerCallback;
import io.qiufen.jdbc.mapper.provider.handler.extension.ClobTypeHandlerCallback;
import io.qiufen.jdbc.mapper.provider.handler.extension.CustomTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.Feature;
import io.qiufen.jdbc.mapper.spi.handler.FeatureTypeHandlerFactory;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.extension.TypeHandlerCallback;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Not much of a suprise, this is a factory class for TypeHandler objects.
 */
@SuppressWarnings("rawtypes")
public class TypeHandlerRegistry
{
    private static final Map<Class<?>, Class<?>> reversePrimitiveMap = new IdentityHashMap<Class<?>, Class<?>>();

    static
    {
        reversePrimitiveMap.put(Byte.class, byte.class);
        reversePrimitiveMap.put(Short.class, short.class);
        reversePrimitiveMap.put(Integer.class, int.class);
        reversePrimitiveMap.put(Long.class, long.class);
        reversePrimitiveMap.put(Float.class, float.class);
        reversePrimitiveMap.put(Double.class, double.class);
        reversePrimitiveMap.put(Boolean.class, boolean.class);
    }

    private final Map<Class<?>, Map<String, TypeHandler>> typeHandlerMap;
    private final TypeHandler unknownTypeHandler = new UnknownTypeHandler(this);
    private final Map<String, CTString> typeAliases = new HashMap<String, CTString>();
    private final Map<Class<?>, Map<String, FeatureTypeHandlerFactory>> featureTypeHandlerFactoryMap;

    /**
     * Default constructor
     */
    public TypeHandlerRegistry() throws SqlMapConfigException
    {
        this.typeHandlerMap = new ConcurrentHashMap<Class<?>, Map<String, TypeHandler>>();

        TypeHandler handler = new BooleanTypeHandler();
        register(Boolean.class, handler);
        register(boolean.class, handler);

        handler = new ByteTypeHandler();
        register(Byte.class, handler);
        register(byte.class, handler);

        handler = new ShortTypeHandler();
        register(Short.class, handler);
        register(short.class, handler);

        handler = new IntegerTypeHandler();
        register(Integer.class, handler);
        register(int.class, handler);

        handler = new LongTypeHandler();
        register(Long.class, handler);
        register(long.class, handler);

        handler = new FloatTypeHandler();
        register(Float.class, handler);
        register(float.class, handler);

        handler = new DoubleTypeHandler();
        register(Double.class, handler);
        register(double.class, handler);

        register(String.class, new StringTypeHandler());
        register(String.class, "CLOB", new CustomTypeHandler(new ClobTypeHandlerCallback()));
        register(String.class, "LONGVARCHAR", new CustomTypeHandler(new ClobTypeHandlerCallback()));

        register(BigDecimal.class, new BigDecimalTypeHandler());

        register(byte[].class, new ByteArrayTypeHandler());
        register(byte[].class, "BLOB", new CustomTypeHandler(new BlobTypeHandlerCallback()));
        register(byte[].class, "LONGVARBINARY", new CustomTypeHandler(new BlobTypeHandlerCallback()));

        register(Object.class, new ObjectTypeHandler());
        register(Object.class, "OBJECT", new ObjectTypeHandler());

        register(Date.class, new DateTypeHandler());
        register(Date.class, "DATE", new DateOnlyTypeHandler());
        register(Date.class, "TIME", new TimeOnlyTypeHandler());

        register(BitSet.class, new BitSetTypeHandler());

        register(java.sql.Date.class, new SqlDateTypeHandler());
        register(java.sql.Time.class, new SqlTimeTypeHandler());
        register(java.sql.Timestamp.class, new SqlTimestampTypeHandler());

        putTypeAlias("string", String.class.getName());
        putTypeAlias("byte", Byte.class.getName());
        putTypeAlias("long", Long.class.getName());
        putTypeAlias("short", Short.class.getName());
        putTypeAlias("int", Integer.class.getName());
        putTypeAlias("integer", Integer.class.getName());
        putTypeAlias("double", Double.class.getName());
        putTypeAlias("float", Float.class.getName());
        putTypeAlias("boolean", Boolean.class.getName());
        putTypeAlias("date", Date.class.getName());
        putTypeAlias("decimal", BigDecimal.class.getName());
        putTypeAlias("object", Object.class.getName());
        putTypeAlias("map", Map.class.getName());
        putTypeAlias("hashmap", HashMap.class.getName());
        putTypeAlias("list", List.class.getName());
        putTypeAlias("arraylist", ArrayList.class.getName());
        putTypeAlias("collection", Collection.class.getName());
        putTypeAlias("iterator", Iterator.class.getName());
        putTypeAlias("cursor", java.sql.ResultSet.class.getName());
        putTypeAlias("map", Map.class.getName());
        putTypeAlias("LocalMap", LocalMap.class.getName());

        this.featureTypeHandlerFactoryMap = new IdentityHashMap<Class<?>, Map<String, FeatureTypeHandlerFactory>>();

    }

    /* Public Methods */

    /**
     * Get a TypeHandler for a class
     *
     * @param type - the class you want a TypeHandler for
     * @return - the handler
     */
    public TypeHandler getTypeHandler(Class<?> type)
    {
        return getTypeHandler(type, null);
    }

    /**
     * Get a TypeHandler for a class and a JDBC type
     *
     * @param type     - the class
     * @param jdbcType - the jdbc type
     * @return - the handler
     */
    @SuppressWarnings("unchecked")
    public TypeHandler getTypeHandler(Class<?> type, String jdbcType)
    {
        TypeHandler handler = getNormalHandler(type, jdbcType);
        if (handler != null)
        {
            return handler;
        }
        synchronized (this)
        {
            handler = getNormalHandler(type, jdbcType);
            if (handler != null)
            {
                return handler;
            }

            //------------match feature-------------
            if (!Feature.class.isAssignableFrom(type))
            {
                return handler;
            }
            for (Map.Entry<Class<?>, Map<String, FeatureTypeHandlerFactory>> entry : featureTypeHandlerFactoryMap.entrySet())
            {
                if (!entry.getKey().isAssignableFrom(type))
                {
                    continue;
                }
                Map<String, FeatureTypeHandlerFactory> factoryMap = entry.getValue();
                FeatureTypeHandlerFactory factory = factoryMap.get(jdbcType);
                if (factory == null)
                {
                    factory = factoryMap.get(null);
                }

                if (factory != null)
                {
                    handler = factory.create(type);
                    register(type, jdbcType, handler);
                }
            }
        }
        return handler;
    }

    private TypeHandler getNormalHandler(Class<?> type, String jdbcType)
    {
        Map<String, TypeHandler> jdbcHandlerMap = typeHandlerMap.get(type);
        TypeHandler handler = null;
        if (jdbcHandlerMap == null)
        {
            return handler;
        }

        handler = jdbcHandlerMap.get(jdbcType);
        if (handler == null)
        {
            handler = jdbcHandlerMap.get(null);
        }
        return handler;
    }

    /**
     * When in doubt, get the "unknown" type handler
     *
     * @return - if I told you, it would not be unknown, would it?
     */
    public TypeHandler getUnkownTypeHandler()
    {
        return unknownTypeHandler;
    }

    /**
     * Tells you if a particular class has a TypeHandler
     *
     * @param type - the class
     * @return - true if there is a TypeHandler
     */
    public boolean hasTypeHandler(Class<?> type)
    {
        return type != null && getTypeHandler(type) != null;
    }

    /**
     * Register (add) a type handler for a class
     *
     * @param type    - the class
     * @param handler - the handler instance
     */
    public void register(Class<?> type, TypeHandler handler)
    {
        register(type, null, handler);
    }

    /**
     * Register (add) a type handler for a class and JDBC type
     *
     * @param type     - the class
     * @param jdbcType - the JDBC type
     * @param handler  - the handler instance
     */
    public void register(Class<?> type, String jdbcType, TypeHandler handler)
    {
        Map<String, TypeHandler> map = typeHandlerMap.get(type);
        if (map == null)
        {
            map = new HashMap<String, TypeHandler>();
            typeHandlerMap.put(type, map);
        }
        map.put(jdbcType, handler);

        if (reversePrimitiveMap.containsKey(type))
        {
            register(reversePrimitiveMap.get(type), jdbcType, handler);
        }
    }

    public <F extends Feature> void register(Class<F> featureType, String jdbcType,
            FeatureTypeHandlerFactory<? extends F> factory)
    {
        Map<String, FeatureTypeHandlerFactory> map = featureTypeHandlerFactoryMap.get(featureType);
        if (map == null)
        {
            map = new HashMap<String, FeatureTypeHandlerFactory>();
            featureTypeHandlerFactoryMap.put(featureType, map);
        }
        map.put(jdbcType, factory);
    }

    @SuppressWarnings("unchecked")
    public void register(Class<?> javaType, String jdbcType, Object handler) throws SqlMapConfigException
    {
        jdbcType = Strings.isEmpty(jdbcType) ? null : jdbcType;
        if (handler instanceof TypeHandler)
        {
            register(javaType, jdbcType, (TypeHandler) handler);
        }
        else if (handler instanceof TypeHandlerCallback)
        {
            register(javaType, jdbcType, new CustomTypeHandler((TypeHandlerCallback) handler));
        }
        else if (handler instanceof FeatureTypeHandlerFactory)
        {
            register((Class<? extends Feature>) javaType, jdbcType, (FeatureTypeHandlerFactory) handler);
        }
        else
        {
            throw new IllegalArgumentException("Not supported handler type:" + handler.getClass().getName());
        }
    }

    /**
     * Lookup an aliased class and return it's REAL name
     *
     * @param string - the alias
     * @return - the REAL name
     */
    public String resolveAlias(String string)
    {
        String key = null;
        if (string != null)
        {
            key = string.toLowerCase();
        }
        String value;
        if (typeAliases.containsKey(key))
        {
            value = typeAliases.get(key).toString();
        }
        else
        {
            value = string;
        }

        return value;
    }

    /**
     * Adds a type alias that is case insensitive.  All of the following String, string, StRiNg will equate to the same alias.
     *
     * @param alias - the alias
     * @param value - the real class name
     */
    public void putTypeAlias(String alias, String value) throws SqlMapConfigException
    {
        String key = null;
        if (alias != null)
        {
            key = alias.toLowerCase();
        }
        CTString ctValue = CTString.of(value);
        if (typeAliases.containsKey(key) && !typeAliases.get(key).equals(ctValue))
        {
            throw new SqlMapConfigException(
                    "Error in XmlSqlMapClientBuilder.  Alias name conflict occurred.  The alias '" + key + "' is already mapped to the value '" + typeAliases.get(
                            alias) + "'.");
        }
        typeAliases.put(key, ctValue);
    }

}
