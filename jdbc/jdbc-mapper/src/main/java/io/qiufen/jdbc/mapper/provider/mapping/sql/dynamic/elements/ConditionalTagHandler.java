package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.common.lang.StringBuilder;
import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;
import io.qiufen.jdbc.mapper.provider.common.utils.SimpleDateFormatter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public abstract class ConditionalTagHandler extends BaseTagHandler
{
    public static final long NOT_COMPARABLE = Long.MIN_VALUE;
    private static final Probe PROBE = ProbeFactory.getProbe();
    private static final String DATE_MASK = "yyyy/MM/dd hh:mm:ss";

    protected abstract boolean isCondition(SqlTagContext ctx, SqlTag tag, Object parameterObject);

    public int doStartFragment(SqlTagContext ctx, SqlTag tag, Object parameterObject)
    {
        ctx.pushRemoveFirstPrependMarker(tag);
        return isCondition(ctx, tag, parameterObject) ? INCLUDE_BODY : SKIP_BODY;
    }

    public int doEndFragment(SqlTagContext ctx, SqlTag tag, Object parameterObject, StringBuilder bodyContent)
    {
        IterateContext iterate = ctx.peekIterateContext();
        if (null != iterate && iterate.isAllowNext())
        {
            iterate.next();
            iterate.setAllowNext(false);
            if (!iterate.hasNext())
            {
                iterate.setFinal(true);
            }
        }

        //iteratePropertyReplace(bodyContent,ctx.peekIterateContext());
        return super.doEndFragment(ctx, tag, parameterObject, bodyContent);
    }

    protected long compare(SqlTagContext ctx, SqlTag tag, Object parameterObject)
    {
        String comparePropertyName = tag.getComparePropertyAttr();
        String compareValue = tag.getCompareValueAttr();

        CharSequence prop = getResolvedProperty(ctx, tag);
        Object value1;
        Class<?> type;

        if (prop == null)
        {
            value1 = parameterObject;
            type = value1 == null ? Object.class : parameterObject.getClass();
        }
        else
        {
            value1 = PROBE.getObject(parameterObject, prop);
            type = PROBE.getPropertyTypeForGetter(parameterObject, prop.toString());
        }

        if (comparePropertyName != null)
        {
            Object value2 = PROBE.getObject(parameterObject, comparePropertyName);
            return compareValues(type, value1, value2);
        }
        else if (compareValue != null)
        {
            return compareValues(type, value1, compareValue);
        }
        else
        {
            throw new RuntimeException("Error comparing in conditional fragment.  Uknown 'compare to' values.");
        }
    }

    protected long compareValues(Class<?> type, Object value1, Object value2)
    {
        long result;
        if (value1 == null || value2 == null)
        {
            result = value1 == value2 ? 0 : NOT_COMPARABLE;
        }
        else
        {
            if (value2.getClass() != type)
            {
                value2 = convertValue(type, value2.toString());
            }
            if (value2 instanceof String && type != String.class)
            {
                value1 = value1.toString();
            }
            if (!(value1 instanceof Comparable && value2 instanceof Comparable))
            {
                value1 = value1.toString();
                value2 = value2.toString();
            }
            result = ((Comparable) value1).compareTo(value2);
        }

        return result;
    }

    protected Object convertValue(Class<?> type, String value)
    {
        if (type == String.class)
        {
            return value;
        }
        else if (type == Byte.class || type == byte.class)
        {
            return Byte.valueOf(value);
        }
        else if (type == Short.class || type == short.class)
        {
            return Short.valueOf(value);
        }
        else if (type == Character.class || type == char.class)
        {
            return value.charAt(0);
        }
        else if (type == Integer.class || type == int.class)
        {
            return Integer.valueOf(value);
        }
        else if (type == Long.class || type == long.class)
        {
            return Long.valueOf(value);
        }
        else if (type == Float.class || type == float.class)
        {
            return Float.valueOf(value);
        }
        else if (type == Double.class || type == double.class)
        {
            return Double.valueOf(value);
        }
        else if (type == Boolean.class || type == boolean.class)
        {
            return Boolean.valueOf(value);
        }
        else if (type == Date.class)
        {
            return SimpleDateFormatter.format(DATE_MASK, value);
        }
        else if (type == BigInteger.class)
        {
            return new BigInteger(value);
        }
        else if (type == BigDecimal.class)
        {
            return new BigDecimal(value);
        }
        else
        {
            return value;
        }
    }

    /**
     * This method will add the proper index values to an indexed property
     * string if we are inside an iterate tag
     */
    protected CharSequence getResolvedProperty(SqlTagContext ctx, SqlTag tag)
    {
        CharSequence prop = tag.getPropertyAttr();
        if (prop == null) return null;

        IterateContext itCtx = ctx.peekIterateContext();
        if (itCtx == null) return prop;

        if (itCtx.isAllowNext())
        {
            itCtx.next();
            itCtx.setAllowNext(false);
            if (!itCtx.hasNext()) itCtx.setFinal(true);
        }
        if (tag.isIndexedProperty())
        {
            prop = itCtx.addIndexToTagProperty(new java.lang.StringBuilder(prop.length() + 2).append(prop));
        }
        return prop;
    }
}
