package io.qiufen.jdbc.mapper.provider.mapping.sql.dynamic.elements;

import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;

public class IsNullTagHandler extends ConditionalTagHandler
{
    private static final Probe PROBE = ProbeFactory.getProbe();

    public boolean isCondition(SqlTagContext ctx, SqlTag tag, Object parameterObject)
    {
        if (parameterObject == null) return true;

        CharSequence prop = getResolvedProperty(ctx, tag);
        if (prop == null) return false;
        return PROBE.getObject(parameterObject, prop) == null;
    }
}
