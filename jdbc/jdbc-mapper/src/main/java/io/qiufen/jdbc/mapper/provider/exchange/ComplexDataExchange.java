package io.qiufen.jdbc.mapper.provider.exchange;

import io.qiufen.jdbc.mapper.api.session.SessionException;
import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;
import io.qiufen.jdbc.mapper.provider.handler.TypeHandlerRegistry;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMap;
import io.qiufen.jdbc.mapper.provider.mapping.parameter.ParameterMapping;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMap;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultMapping;
import io.qiufen.jdbc.mapper.provider.mapping.result.ResultObjectFactoryUtil;
import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

/**
 * A DataExchange implemtation for working with beans
 */
public class ComplexDataExchange extends BaseDataExchange implements DataExchange
{
    private static final Probe PROBE = ProbeFactory.getProbe();

    /**
     * Constructor for the factory
     *
     * @param dataExchangeFactory - the factory
     */
    ComplexDataExchange(DataExchangeFactory dataExchangeFactory)
    {
        super(dataExchangeFactory);
    }

    public Object[] getData(StatementScope statementScope, ParameterMap parameterMap, Object parameterObject)
    {
        if (parameterObject == null) return new Object[1];

        TypeHandlerRegistry typeHandlerRegistry = getDataExchangeFactory().getTypeHandlerFactory();
        int length = parameterMap.countParameterMapping();
        if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass()))
        {
            Object[] data = new Object[length];
            Arrays.fill(data, parameterObject);
            return data;
        }

        Object[] data = new Object[length];
        for (int i = 0; i < length; i++)
        {
            data[i] = PROBE.getObject(parameterObject, parameterMap.getParameterMapping(i).getPropertyName());
        }
        return data;
    }

    @Override
    public Object setData(StatementScope statementScope, ResultMap resultMap, ResultSet rs) throws SQLException
    {
        ResultMapping[] mappings = resultMap.getResultMappings();
        TypeHandlerRegistry typeHandlerRegistry = getDataExchangeFactory().getTypeHandlerFactory();
        if (typeHandlerRegistry.hasTypeHandler(resultMap.getResultClass()))
        {
            return getResultMappingValue(statementScope, mappings[0], rs);
        }

        Object object;
        try
        {
            object = ResultObjectFactoryUtil.createObjectThroughFactory(resultMap.getResultClass());
        }
        catch (Exception e)
        {
            throw new SessionException("JavaBeansDataExchange could not instantiate result class.  Cause: " + e, e);
        }

        for (ResultMapping mapping : mappings)
        {
            PROBE.setObject(object, mapping.getPropertyName(), getResultMappingValue(statementScope, mapping, rs));
        }
        return object;
    }

    @Override
    public void setData(StatementScope statementScope, ParameterMap parameterMap, PreparedStatement ps,
            Object parameterObject) throws SQLException
    {
        TypeHandlerRegistry typeHandlerRegistry = getDataExchangeFactory().getTypeHandlerFactory();
        int length = parameterMap.countParameterMapping();
        if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass()))
        {
            for (int i = 0; i < length; i++)
            {
                setParameterValue(statementScope, parameterMap.getParameterMapping(i), ps, i + 1, parameterObject);
            }
            return;
        }

        for (int i = 0; i < length; i++)
        {
            ParameterMapping mapping = parameterMap.getParameterMapping(i);
            setParameterValue(statementScope, mapping, ps, i + 1,
                    PROBE.getObject(parameterObject, mapping.getPropertyName()));
        }
    }
}
