package io.qiufen.jdbc.mapper.provider.handler;

import io.qiufen.jdbc.mapper.spi.handler.BaseTypeHandler;
import io.qiufen.jdbc.mapper.spi.handler.TypeHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Byte implementation of TypeHandler
 */
class ByteTypeHandler extends BaseTypeHandler implements TypeHandler
{
    public void setParameter(PreparedStatement ps, int i, Object parameter, String jdbcType) throws SQLException
    {
        ps.setByte(i, (Byte) parameter);
    }

    public Object getResult(ResultSet rs, String columnName) throws SQLException
    {
        byte b = rs.getByte(columnName);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return b;
        }
    }

    public Object getResult(ResultSet rs, int columnIndex) throws SQLException
    {
        byte b = rs.getByte(columnIndex);
        if (rs.wasNull())
        {
            return null;
        }
        else
        {
            return b;
        }
    }

    public Object valueOf(String s)
    {
        return Byte.valueOf(s);
    }
}
