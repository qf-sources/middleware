package io.qiufen.jdbc.mapper.provider.statement;

import io.qiufen.jdbc.mapper.provider.scope.StatementScope;

public class SelectStatement extends MappedStatement
{
    public StatementType getStatementType()
    {
        return StatementType.SELECT;
    }

    public StatementScope initRequest()
    {
        StatementScope statementScope = new StatementScope(this);
        statementScope.setParameterMap(getParameterMap());
        statementScope.setResultMap(getResultMap().prepare());
        return statementScope;
    }
}
