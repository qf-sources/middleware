package bean;

import io.qiufen.jdbc.mapper.provider.common.beans.Probe;
import io.qiufen.jdbc.mapper.provider.common.beans.ProbeFactory;

import java.util.Arrays;

public class Test0
{
    static int times = 100;
    static int loops = 1000000;

    public static void main(String[] args)
    {
        Probe probe = ProbeFactory.getProbe();

        for (int i = 0; i < times; i++)
        {
            long sum = 0;
            for (int j = 0; j < loops; j++)
            {
                long p0 = System.nanoTime();
                test(probe);
                long p1 = System.nanoTime();
                sum += p1 - p0;
            }
            System.out.println((i + 1) + " " + sum / loops);
        }
    }

    static void print(Probe probe)
    {
        User user = new User();
        probe.setObject(user, "id", 1000L);
        probe.setObject(user, "order.no", "ORDER_0");
        probe.setObject(user, "proxy.a", "ORDER_proxy_A0");
        probe.setObject(user, "proxy.b", "ORDER_proxy_B0");
        //        probe.setObject(user, "history[0]", "H_ORDER_0");

        System.out.println(probe.getObject(user, "id"));
        System.out.println(probe.getObject(Arrays.asList(user, user), "[0].id"));
        System.out.println(probe.getObject(user, "order.no"));
        System.out.println(probe.getObject(user, "proxy.a"));
        System.out.println(probe.getObject(user, "proxy.b"));
        //        System.out.println(probe.getObject(user, "history[0]"));
    }

    static void test(Probe probe)
    {
        User user = new User();
        probe.setObject(user, "id", 1000L);
        probe.setObject(user, "order.no", "ORDER_0");
        probe.setObject(user, "proxy.a", "ORDER_proxy_A0");
        probe.setObject(user, "proxy.b", "ORDER_proxy_B0");
        //        probe.setObject(user, "history[0]", "H_ORDER_0");

        probe.getObject(user, "id");
        probe.getObject(Arrays.asList(user, user), "[0].id");
        probe.getObject(user, "order.no");
        probe.getObject(user, "proxy.a");
        probe.getObject(user, "proxy.b");
        //        System.out.println(probe.getObject(user, "history[0]"));
    }
}
