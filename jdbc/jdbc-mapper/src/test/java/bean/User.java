package bean;

import java.util.ArrayList;
import java.util.HashMap;

public class User
{
    private Long id;
    private Order order;
    private HashMap<String, String> proxy;
    private ArrayList<String> history;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Order getOrder()
    {
        return order;
    }

    public void setOrder(Order order)
    {
        this.order = order;
    }

    public HashMap<String, String> getProxy()
    {
        return proxy;
    }

    public void setProxy(HashMap<String, String> proxy)
    {
        this.proxy = proxy;
    }

    public ArrayList<String> getHistory()
    {
        return history;
    }

    public void setHistory(ArrayList<String> history)
    {
        this.history = history;
    }
}
