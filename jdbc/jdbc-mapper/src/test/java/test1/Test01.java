package test1;

import io.qiufen.common.io.resource.ClassPathResource;
import io.qiufen.common.io.resource.StreamResource;
import io.qiufen.common.lang.CTString;
import io.qiufen.jdbc.mapper.api.session.Session;
import io.qiufen.jdbc.mapper.api.session.SessionFactory;
import io.qiufen.jdbc.mapper.api.transaction.Transaction;
import io.qiufen.jdbc.mapper.api.transaction.TransactionDefinition;
import io.qiufen.jdbc.mapper.provider.SqlMapConfigException;
import io.qiufen.jdbc.mapper.provider.session.SessionFactoryBuilder;
import io.qiufen.jdbc.mapper.spi.handler.RowHandler;
import org.apache.tomcat.jdbc.pool.DataSource;

import java.io.IOException;
import java.util.*;

/**
 * [功能描述]
 *
 * @author Ruzheng Zhang
 * @date 2020/9/28 10:27
 * @see [相关类/方法](可选)
 * @since [产品/模块版本](可选)
 */
public class Test01
{
    public static void main(String[] args) throws IOException, InterruptedException, SqlMapConfigException
    {
        //        insert();

        query();
    }

    private static void query() throws IOException, InterruptedException, SqlMapConfigException
    {
        DataSource dataSource = new DataSource();
        dataSource.setUrl("jdbc:mysql://192.168.203.134:3307/moodle");
        dataSource.setDriverClassName("org.mariadb.jdbc.Driver");
        dataSource.setUsername("moodle");
        dataSource.setPassword("moodle");

        StreamResource resource = new ClassPathResource("config.xml");
        final SessionFactory factory = new SessionFactoryBuilder().setDataSource(dataSource)
                .setResource(resource)
                .build();

        Session session = factory.openSession();
        for (int i = 0; i < 5; i++)
        {
            session.select(CTString.wrap("test.query"), new HashMap<String, Object>()
            {{
                put("value", "123");
                put("idList", Arrays.asList("1", "2", "3"));
                put("test", "test00");
            }}, new RowHandler<Object>()
            {
                @Override
                public void handleRow(Object valueObject)
                {
                    //                            System.out.println(valueObject);
                }
            });
        }

        long p1 = System.currentTimeMillis();
        session = factory.openSession();
        Transaction transaction = session.createTransaction(new TransactionDefinition(1, false));
        transaction.begin();

        session.select(CTString.wrap("test.query"), new HashMap<String, Object>()
        {{
            //                        put("value", "123");
            put("idList", Arrays.asList("1", "2", "3"));
        }}, new RowHandler<Object>()
        {
            @Override
            public void handleRow(Object valueObject)
            {
                //                            System.out.println(valueObject);
            }
        });

        for (int i = 0; i < 5; i++)
        {
            //        Session session = factory.createSession();
            //        session.select("", "");
            session.select(CTString.wrap("test.query"), new HashMap<String, Object>()
            {{
                //                        put("value", "123");
                put("idList", Arrays.asList("1", "2", "3"));
            }}, new RowHandler<Object>()
            {
                @Override
                public void handleRow(Object valueObject)
                {
                    //                            System.out.println(valueObject);
                }
            });
        }
        transaction.commit();
        session.close();
        long p2 = System.currentTimeMillis();
        System.out.println(p2 - p1);
    }

    private static void insert() throws IOException, SqlMapConfigException
    {
        DataSource dataSource = new DataSource();
        dataSource.setUrl("jdbc:mysql://192.168.203.134:3307/moodle");
        dataSource.setDriverClassName("org.mariadb.jdbc.Driver");
        dataSource.setUsername("moodle");
        dataSource.setPassword("moodle");

        StreamResource resource = new ClassPathResource("config.xml");
        SessionFactory factory = new SessionFactoryBuilder().setDataSource(dataSource).setResource(resource).build();

        Session session = factory.openSession();
        Transaction transaction = session.createTransaction(new TransactionDefinition(1, false));
        try
        {
            transaction.begin();
            List list = new ArrayList();
            for (int i = 0; i < 10000; i++)
            {
                Map<String, Object> param = new HashMap<String, Object>();
                param.put("a", i);
                param.put("b", i);
                list.add(param);
                //                session.insert("test.insert", param);
            }
            int[] results = session.insert(CTString.wrap("test.insert"), list);
            System.out.println(Arrays.toString(results));
            System.out.println(list);

            //            System.out.println(session.select("test.query", null));
            transaction.commit();
        }
        finally
        {
            session.close();
        }
    }
}
